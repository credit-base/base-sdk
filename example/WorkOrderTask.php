<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

$repository = new Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository();
$parentTaskRepository = new Base\Sdk\WorkOrderTask\Repository\ParentTaskRepository();
// $parentTask = new Base\Sdk\WorkOrderTask\Model\ParentTask();
$template = new Base\Sdk\Template\Model\GbTemplate(1);

$userGroupRepository = new Base\Sdk\UserGroup\Repository\UserGroupRepository();
// $parentTasks = $parentTaskRepository->fetchList(array(1,8));
$parentTask = $repository->scenario($repository::FETCH_ONE_MODEL_UN)->fetchOne(57);

$feedbackRecords = array(
    'crew' => 1,  //反馈人
    'userGroup' => 1,  //反馈委办局
    'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
    'templateId' => '',  //目录id
    'reason' => '反馈原因',
    'items' => array(
        array(
            "name" => '主体名称',    //信息项名称
            "identify" => 'ZTMC',    //数据标识
            "type" => 1,    //数据类型
            "length" => '200',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
            "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则
            "remarks" => '信用主体名称',    //备注
        ),
        array(
            "name" => '统一社会信用代码',    //信息项名称
            "identify" => 'TYSHXYDM',    //数据标识
            "type" => 1,    //数据类型
            "length" => '50',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
            "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
            "remarks" => '信用主体代码',    //备注
        )
    )
);

// $parentTask->setTitle('归集地方性红名单信息');
// $parentTask->setDescription('归集地方性红名单信息描述信息');
// $parentTask->setAttachment(array('name' => '国203号文','identify' => 'b900638af896a26de13bc89ce4f64124.pdf'));
// $parentTask->setTemplateType(1);
// $parentTask->setEndTime('2020-01-01');
// $parentTask->setTemplate($template);
// $parentTask->setAssignObjects($assignObjects);

// var_dump($parentTask->assign());

// $parentTask->setReason('原因');
// var_dump($parentTask->end());
// var_dump($parentTask->revoke());
// var_dump($parentTask->confirm());
$parentTask->setFeedbackRecords(array($feedbackRecords));
var_dump($parentTask->feedback());
// var_dump($repository->fetchOne(1)); 
exit();
