<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

$repository = new Base\Sdk\Template\Repository\GbTemplateRepository();
$gbTemplate = new Base\Sdk\Template\Model\GbTemplate();

$gbTemplate = $repository->scenario($repository::FETCH_ONE_MODEL_UN)->fetchOne(1);

$item = array(
    "name" => '自然人登记信息',    //信息项名称
    "identify" => 'ZRRDJXX',    //数据标识
    "type" => '字符型',    //数据类型
    "length" => '200',    //数据长度
    "options" => array(),    //可选范围
    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
    "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
    "maskRule" => array(),    //脱敏规则
    "remarks" => '信用主体名称',    //备注
);

$gbTemplate->setName('z');
$gbTemplate->setIdentify('ZRRDJ');
$gbTemplate->setDescription('目录描述信息');
$gbTemplate->setSubjectCategory(array(1,2));
$gbTemplate->setItems(array($item));
$gbTemplate->setDimension(1);
$gbTemplate->setExchangeFrequency(1);
$gbTemplate->setInfoClassify(1);
$gbTemplate->setInfoCategory(1);

var_dump($repository->fetchOne(1)); 
var_dump($gbTemplate->add());
// var_dump($gbTemplate->edit());
exit();
