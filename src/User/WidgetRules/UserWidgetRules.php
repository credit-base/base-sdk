<?php
namespace Base\Sdk\User\WidgetRules;

use Marmot\Core;

use Respect\Validation\Validator as V;

use Base\Sdk\Common\Persistence\UtilsSession;

class UserWidgetRules
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    const REAL_NAME_MIN_LENGTH = 2;
    const REAL_NAME_MAX_LENGTH = 15;

    public function realName($realName) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::REAL_NAME_MIN_LENGTH,
            self::REAL_NAME_MAX_LENGTH
        )->validate($realName)) {
            Core::setLastError(REAL_NAME_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    /**
     * 身份证号码的校验
     * 1.号码必须是数字格式，且长度必须是15位或18位
     * 2.数字必须在0-9之间的整数
     */
    public function cardId($cardId) : bool
    {
        $reg = "/(^\d{15}$)|(^\d{18}$)|(^\d{17}(^[0-9]|X))$/";
        if (!V::alnum()->noWhitespace()->length(15, 15)->validate($cardId)
            && !V::alnum()->noWhitespace()->length(18, 18)->validate($cardId)) {
            Core::setLastError(CARDID_FORMAT_ERROR);
            return false;
        }

        if (!preg_match($reg, $cardId)) {
            Core::setLastError(CARDID_FORMAT_ERROR);
            return false;
        }
      
        return true;
    }

    /**
     * 手机号校验：长度必须是11位，0-9的整数
     */
    public function cellphone($cellphone) : bool
    {
        $reg = '/^[1][0-9]{10}$/';
        if (!preg_match($reg, $cellphone)) {
            Core::setLastError(CELLPHONE_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    /**
     * 登陆密码的校验
     * 1.密码长度7-21
     * 2.密码必须包含数字、26个英文字母、@#$%等特殊字符
     * 3.英文字符区分大小写
     */
    public function password($password, $pointer = 'password') : bool
    {
        $reg = '/^(?=.*\d)(?=.*[a-zA-Z])(?=.*[~!@#$%^*.()_+-={}:;?,])[\da-zA-Z~!@#$%^*.()_+-={}:;?,]{8,20}$/';
        if (!preg_match($reg, $password)) {
            Core::setLastError(PASSWORD_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    public function confirmPassword($password, $confirmPassword) : bool
    {
        if ($password != $confirmPassword) {
            Core::setLastError(PASSWORD_INCONSISTENCY);
            return false;
        }
        return true;
    }

    public function hash() : bool
    {
        $session = new UtilsSession();
        $hash = $session->get('cryptojs_hash');
        if (empty($hash)) {
            Core::setLastError(HASH_INVALID);
            return false;
        }
        return true;
    }
}
