<?php
namespace Base\Sdk\User\Translator;

use Marmot\Interfaces\ITranslator;
use Base\Sdk\Common\Utils\Mask;

use Base\Sdk\User\Model\User;

use Base\Sdk\Common\Model\IEnableAble;

abstract class UserTranslator implements ITranslator
{
    const STATUS = array(
        IEnableAble::STATUS['ENABLED'] => '启用',
        IEnableAble::STATUS['DISABLED'] => '禁用'
    );

    const STATUS_TYPE = array(
        IEnableAble::STATUS['ENABLED'] => 'success',
        IEnableAble::STATUS['DISABLED'] => 'warning'
    );

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($user, array $keys = array())
    {
        if (!$user instanceof User) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'cellphone',
                'userName',
                'realName',
                'cardId',
                'avatar',
                'createTime',
                'updateTime',
                'status',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($user->getId());
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $user->getCellphone();
        }
        if (in_array('realName', $keys)) {
            $expression['realName'] = $user->getRealName();
        }
        if (in_array('userName', $keys)) {
            $expression['userName'] = $user->getUserName();
        }
        if (in_array('cardId', $keys)) {
            $expression['cardId'] = $user->getCardId();
        }
        if (in_array('avatar', $keys)) {
            $expression['avatar'] = $user->getAvatar();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $user->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $user->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H点i分', $user->getUpdateTime());
        }
        if (in_array('status', $keys)) {
            $expression['status']['id'] = marmot_encode($user->getStatus());
            $expression['status']['name'] = self::STATUS[$user->getStatus()];
            $expression['status']['type'] = self::STATUS_TYPE[$user->getStatus()];
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $user->getStatusTime();
        }

        return $expression;
    }
}
