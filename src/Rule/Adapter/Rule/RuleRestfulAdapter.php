<?php
namespace Base\Sdk\Rule\Adapter\Rule;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;
use Base\Sdk\Common\Model\IModifyStatusAble;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Model\NullRule;

use Base\Sdk\Rule\Translator\RuleRestfulTranslator;

use Base\Sdk\Rule\Adapter\RulesMapErrorsTrait;

class RuleRestfulAdapter extends GuzzleAdapter implements IRuleAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        ModifyStatusAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        RulesMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'RULE_LIST'=>[
            'fields'=>[],
            'include'=>'crew,userGroup,transformationTemplate,sourceTemplate'
        ],
        'RULE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,userGroup,transformationTemplate,sourceTemplate'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new RuleRestfulTranslator();
        $this->resource = 'rules';
        $this->scenario = array();
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = $this->rulesMapErrors();

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullRule());
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function addAction(Rule $rule) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $rule,
            array(
                'rules',
                'crew',
                'transformationTemplate',
                'sourceTemplate',
                'transformationCategory',
                'sourceCategory'
            )
        );
       
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($rule);
            return true;
        }

        return false;
    }

    protected function deletesAction(IModifyStatusAble $modifyStatusAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($modifyStatusAbleObject, array('crew'));
       
        $this->patch(
            $this->getResource().'/'.$modifyStatusAbleObject->getId().'/delete',
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($modifyStatusAbleObject);
            return true;
        }
        return false;
    }

    protected function editAction(Rule $rule) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $rule,
            array(
                'rules',
                'crew',
            )
        );

        $this->patch(
            $this->getResource().'/'.$rule->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($rule);
            return true;
        }

        return false;
    }
}
