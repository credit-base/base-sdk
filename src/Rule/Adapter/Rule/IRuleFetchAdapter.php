<?php
namespace Base\Sdk\Rule\Adapter\Rule;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IRuleFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
