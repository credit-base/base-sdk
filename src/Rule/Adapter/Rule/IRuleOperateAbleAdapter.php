<?php
namespace Base\Sdk\Rule\Adapter\Rule;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

interface IRuleOperateAbleAdapter extends IOperateAbleAdapter, IModifyStatusAbleAdapter
{
    
}
