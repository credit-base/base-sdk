<?php
namespace Base\Sdk\Rule\Adapter\UnAuditRule;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

interface IUnAuditRuleOperateAbleAdapter extends IOperateAbleAdapter, IApplyAbleAdapter, IModifyStatusAbleAdapter
{
    
}
