<?php
namespace Base\Sdk\Rule\Adapter\UnAuditRule;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Model\IModifyStatusAble;

use Base\Sdk\Rule\Model\UnAuditRule;
use Base\Sdk\Rule\Model\NullUnAuditRule;

use Base\Sdk\Rule\Translator\UnAuditRuleRestfulTranslator;

use Base\Sdk\Rule\Adapter\RulesMapErrorsTrait;

class UnAuditRuleRestfulAdapter extends GuzzleAdapter implements IUnAuditRuleAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        ModifyStatusAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        RulesMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDIT_RULE_LIST'=>[
            'fields' => [],
            'include' => 'crew,userGroup,transformationTemplate,sourceTemplate,publishCrew,applyCrew,applyUserGroup'
        ],
        'UN_AUDIT_RULE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,userGroup,transformationTemplate,sourceTemplate,publishCrew,applyCrew,applyUserGroup'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditRuleRestfulTranslator();
        $this->resource = 'unAuditedRules';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = $this->rulesMapErrors();
        
        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullUnAuditRule());
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function addAction(UnAuditRule $unAuditRule) : bool
    {
        unset($unAuditRule);
        return false;
    }

    protected function approveAction(IApplyAble $unAuditRule) : bool
    {
        $data = $this->getTranslator()->objectToArray($unAuditRule, array('applyCrew'));
        
        $this->patch(
            $this->getResource().'/'.$unAuditRule->getId().'/approve',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($unAuditRule);
            return true;
        }
        return false;
    }

    protected function editAction(UnAuditRule $unAuditRule) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditRule,
            array(
                'rules',
                'crew'
            )
        );
     
        $this->patch(
            $this->getResource().'/'.$unAuditRule->getId().'/'.'resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditRule);
            return true;
        }

        return false;
    }

    protected function rejectAction(IApplyAble $unAuditRule) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditRule,
            array(
                'rejectReason',
                'applyCrew'
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$unAuditRule->getId().'/reject',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditRule);
            return true;
        }
        return false;
    }

    protected function revokeAction(IModifyStatusAble $unAuditRule) : bool
    {
        $data = $this->getTranslator()->objectToArray($unAuditRule, array('crew'));
      
        $this->patch(
            $this->getResource().'/'.$unAuditRule->getId().'/revoke',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($unAuditRule);
            return true;
        }
        return false;
    }
}
