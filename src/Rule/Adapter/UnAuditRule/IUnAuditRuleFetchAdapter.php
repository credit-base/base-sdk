<?php
namespace Base\Sdk\Rule\Adapter\UnAuditRule;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IUnAuditRuleFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
