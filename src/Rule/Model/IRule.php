<?php
namespace Base\Sdk\Rule\Model;

class IRule
{
    const CATEGORY = array(
        'BJ_RULE' => 1,
        'GB_RULE' => 2,
        'BASE_RULE' => 3,
        'WBJ_RULE' => 10,
        // 'QZJ_WBJ_RULE'=>20, //勿删，暂未开发，预留
        // 'QZJ_BJ_RULE'=>21,
        // 'QZJ_GB_RULE'=>22,
    );
    
    const RULE_NAME = [
        'TRANSFORMATION_RULE'=>'transformationRule',
        'COMPLETION_RULE' =>'completionRule',
        'COMPARISON_RULE'=>'comparisonRule',
        'DE_DUPLICATION_RULE'=>'deDuplicationRule'
    ];

    const COMPLETION_BASE = [
        'NAME'=>1,
        'IDENTIFY'=>2
    ];

    const COMPLETION_BASE_CN = [
        self::COMPLETION_BASE['NAME'] => '企业名称',
        self::COMPLETION_BASE['IDENTIFY'] => '统一社会信用代码'
    ];

    const COMPARISON_BASE = [
        'NAME'=>1,
        'IDENTIFY'=>2
    ];

    const COMPARISON_BASE_CN = [
        self::COMPARISON_BASE['NAME'] => '企业名称',
        self::COMPARISON_BASE['IDENTIFY'] => '统一社会信用代码'
    ];

    const DE_DUPLICATION_RESULT = [
        'DISCARD'=>1,
        'COVER'=>2
    ];

    const DE_DUPLICATION_RESULT_CN = [
        self::DE_DUPLICATION_RESULT['DISCARD'] => '丢弃',
        self::DE_DUPLICATION_RESULT['COVER'] => '覆盖'
    ];
}
