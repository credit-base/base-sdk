<?php
namespace Base\Sdk\Rule\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Sdk\Common\Model\NullOperateAbleTrait;
use Base\Sdk\Common\Model\NullApplyAbleTrait;
use Base\Sdk\Common\Model\NullModifyStatusAbleTrait;

class NullUnAuditRule extends UnAuditRule implements INull
{
    use NullApplyAbleTrait, NullOperateAbleTrait, NullModifyStatusAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
