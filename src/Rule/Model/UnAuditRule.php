<?php
namespace Base\Sdk\Rule\Model;

use Marmot\Core;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Model\ApplyAbleTrait;
use Base\Sdk\Common\Model\ModifyStatusAbleTrait;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Rule\Repository\UnAuditRuleRepository;

class UnAuditRule extends Rule implements IApplyAble
{
    const OPERATION_TYPE = array(
        'NULL' => 0,
        'ADD' => 1,
        'EDIT' => 2,
        'ENABLED' => 3,
        'DISABLED' => 4,
    );

    use ApplyAbleTrait, ModifyStatusAbleTrait;

    /**
     * [$rejectReason 驳回原因]
     * @var string
     */
    private $rejectReason;
    /**
     * [$operationType 操作类型]
     * @var int
     */
    private $operationType;
    /**
     * [$applyCrew 审核人]
     * @var Crew
     */
    private $applyCrew;
    /**
     * [$applyUserGroup 审核单位]
     * @var UserGroup
     */
    private $applyUserGroup;
    /**
     * [$relationId 关联id]
     * @var int
     */
    private $relationId;
    /**
     * [$publishCrew 发布人]
     * @var Crew
     */
    private $publishCrew;
    
    private $unAuditRuleRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->id = !empty($id) ? $id : 0;
        $this->rejectReason = '';
        $this->operationType = self::OPERATION_TYPE['NULL'];
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->relationId = 0;
        $this->publishCrew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->applyCrew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->applyUserGroup = new UserGroup();
        $this->unAuditRuleRepository = new UnAuditRuleRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->relationId);
        unset($this->applyCrew);
        unset($this->rejectReason);
        unset($this->applyUserGroup);
        unset($this->publishCrew);
        unset($this->operationType);
        unset($this->applyStatus);
        unset($this->unAuditRuleRepository);
    }

    public function setOperationType(int $operationType): void
    {
        $this->operationType = $operationType;
    }

    public function getOperationType(): int
    {
        return $this->operationType;
    }

    public function setApplyStatus(int $applyStatus): void
    {
        $this->applyStatus =
            in_array(
                $applyStatus,
                array_values(self::APPLY_STATUS)
            ) ? $applyStatus : self::APPLY_STATUS['PENDING'];
    }

    public function getApplyStatus(): int
    {
        return $this->applyStatus;
    }

    public function setRejectReason(string $rejectReason): void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason(): string
    {
        return $this->rejectReason;
    }

    public function setRelationId(int $relationId) : void
    {
        $this->relationId = $relationId;
    }

    public function getRelationId() : int
    {
        return $this->relationId;
    }

    public function setPublishCrew(Crew $publishCrew) : void
    {
        $this->publishCrew = $publishCrew;
    }

    public function getPublishCrew() : Crew
    {
        return $this->publishCrew;
    }

    public function setApplyCrew(Crew $applyCrew) : void
    {
        $this->applyCrew = $applyCrew;
    }

    public function getApplyCrew() : Crew
    {
        return $this->applyCrew;
    }

    public function setApplyUserGroup(UserGroup $applyUserGroup) : void
    {
        $this->applyUserGroup = $applyUserGroup;
    }

    public function getApplyUserGroup() : UserGroup
    {
        return $this->applyUserGroup;
    }

    protected function getUnAuditRuleRepository(): UnAuditRuleRepository
    {
        return $this->unAuditRuleRepository;
    }

    /**
     * 通过驳回
     * @return [IApplyAbleAdapter]
     */
    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getUnAuditRuleRepository();
    }

    /**
     * 新增编辑
     * @return [IOperateAbleAdapter]
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getUnAuditRuleRepository();
    }

     /**
     * 撤销
     * @return [IModifyStatusAbleAdapter]
     */
    protected function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter
    {
        return $this->getUnAuditRuleRepository();
    }
}
