<?php
namespace Base\Sdk\Rule\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;
use Marmot\Core;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Model\ModifyStatusAbleTrait;
use Base\Sdk\Common\Model\OperateAbleTrait;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Rule\Repository\RuleRepository;
use Base\Sdk\Template\Model\Template;

class Rule implements IObject, IModifyStatusAble, IOperateAble
{
    use Object, ModifyStatusAbleTrait, OperateAbleTrait;

    const STATUS = [
        'NORMAL' => 0,
        'DELETED' => -2
    ];

    private $id;
    /**
     * [$transformationCategory 目标库类别]
     * @var int
     */
    private $transformationCategory;
    /**
     * [$transformationTemplate 目标资源目录]
     * @var int
     */
    private $transformationTemplate;
    /**
     * [$sourceCategory 来源库类别]
     * @var int
     */
    private $sourceCategory;
    /**
     * [$sourceTemplate 目标资源目录]
     * @var int
     */
    private $sourceTemplate;
    /**
     * [$rules 规则内容]
     * @var array
     */
    private $rules;
    /**
     * [$dataTotal 已转换数据量]
     * @var int
     */
    private $dataTotal;
    /**
     * [$version 版本号]
     * @var int
     */
    private $version;
    /**
     * [$crew 发布人]
     * @var Crew
     */
    private $crew;
    /**
     * [$userGroup 来源委办局]
     * @var UserGroup
     */
    private $userGroup;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->transformationTemplate = new Template();
        $this->sourceTemplate = new Template();
        $this->transformationCategory = 0;
        $this->sourceCategory = 0;
        $this->rules = array();
        $this->version = 0;
        $this->dataTotal = 0;
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->userGroup = new UserGroup();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new RuleRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->transformationTemplate);
        unset($this->sourceTemplate);
        unset($this->transformationCategory);
        unset($this->sourceCategory);
        unset($this->rules);
        unset($this->dataTotal);
        unset($this->version);
        unset($this->status);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status =
            in_array($status, array_values(self::STATUS)) ? $status : self::STATUS['NORMAL'];
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setTransformationTemplate(Template $transformationTemplate): void
    {
        $this->transformationTemplate = $transformationTemplate;
    }

    public function getTransformationTemplate(): Template
    {
        return $this->transformationTemplate;
    }

    public function setSourceTemplate(Template $sourceTemplate): void
    {
        $this->sourceTemplate = $sourceTemplate;
    }

    public function getSourceTemplate(): Template
    {
        return $this->sourceTemplate;
    }

    public function setTransformationCategory(int $transformationCategory): void
    {
        $this->transformationCategory = $transformationCategory;
    }

    public function getTransformationCategory(): int
    {
        return $this->transformationCategory;
    }

    public function setSourceCategory(int $sourceCategory): void
    {
        $this->sourceCategory = $sourceCategory;
    }

    public function getSourceCategory(): int
    {
        return $this->sourceCategory;
    }

    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setRules(array $rules) : void
    {
        $this->rules = $rules;
    }

    public function getRules() : array
    {
        return $this->rules;
    }

    public function setDataTotal(int $dataTotal): void
    {
        $this->dataTotal = $dataTotal;
    }

    public function getDataTotal(): int
    {
        return $this->dataTotal;
    }

    protected function getRepository(): RuleRepository
    {
        return $this->repository;
    }

    protected function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 删除
     * @return [bool]
     */
    public function deletes() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }
        $modifyStatusAdapter = $this->getIModifyStatusAbleAdapter();
        return $modifyStatusAdapter->deletes($this);
    }

    /**
     * 新增编辑
     * @return [IOperateAbleAdapter]
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }
}
