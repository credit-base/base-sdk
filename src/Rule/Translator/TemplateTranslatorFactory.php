<?php
namespace Base\Sdk\Rule\Translator;

use Base\Sdk\Rule\Model\IRule;

class TemplateTranslatorFactory
{
    const RESTFUL_TRANSLATOR_MAPS = [
        IRule::CATEGORY['BJ_RULE']=> 'Base\Sdk\Template\Translator\BjTemplateRestfulTranslator',
        IRule::CATEGORY['GB_RULE']=> 'Base\Sdk\Template\Translator\GbTemplateRestfulTranslator',
        IRule::CATEGORY['WBJ_RULE']=> 'Base\Sdk\Template\Translator\WbjTemplateRestfulTranslator',
        IRule::CATEGORY['BASE_RULE']=> 'Base\Sdk\Template\Translator\BaseTemplateRestfulTranslator',
    ];

    public function getTemplateRestfulTranslator(string $category)
    {
        $restfulTranslator = isset(self::RESTFUL_TRANSLATOR_MAPS[$category])
                                 ? self::RESTFUL_TRANSLATOR_MAPS[$category]
                                 : '';

        return class_exists($restfulTranslator) ? new $restfulTranslator : new NullTemplateTranslator();
    }

    const TRANSLATOR_MAPS = [
        IRule::CATEGORY['BJ_RULE']=> 'Base\Sdk\Template\Translator\BjTemplateTranslator',
        IRule::CATEGORY['GB_RULE']=> 'Base\Sdk\Template\Translator\GbTemplateTranslator',
        IRule::CATEGORY['WBJ_RULE']=> 'Base\Sdk\Template\Translator\WbjTemplateTranslator',
        IRule::CATEGORY['BASE_RULE']=> 'Base\Sdk\Template\Translator\BaseTemplateTranslator',
    ];

    public function getTemplateTranslator(string $category)
    {
        $translator = isset(self::TRANSLATOR_MAPS[$category]) ? self::TRANSLATOR_MAPS[$category] : '';

        return class_exists($translator) ? new $translator : new NullTemplateTranslator();
    }
}
