<?php
namespace Base\Sdk\Rule\Translator;

use Marmot\Core;

use Base\Sdk\Rule\Model\UnAuditRule;
use Base\Sdk\Rule\Model\NullUnAuditRule;

use Base\Sdk\Crew\Model\Crew;

/**
 * @SuppressWarnings(PHPMD)
 */
class UnAuditRuleRestfulTranslator extends RuleRestfulTranslator
{
    public function arrayToObject(array $expression, $unAuditRule = null)
    {
        return $this->translateToObject($expression, $unAuditRule);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $unAuditRule = null)
    {
        if (empty($expression)) {
            return new NullUnAuditRule();
        }
       
        if ($unAuditRule == null) {
            $unAuditRule = new UnAuditRule();
        }

        $unAuditRule = parent::translateToObject($expression, $unAuditRule);
        
        $data = $expression['data'];

        $id = $data['id'];
        $unAuditRule->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['applyStatus'])) {
            $unAuditRule->setApplyStatus($attributes['applyStatus']);
        }

        if (isset($attributes['rejectReason'])) {
            $unAuditRule->setRejectReason($attributes['rejectReason']);
        }

        if (isset($attributes['operationType'])) {
            $unAuditRule->setOperationType($attributes['operationType']);
        }

        if (isset($attributes['relationId'])) {
            $unAuditRule->setRelationId($attributes['relationId']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['publishCrew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['publishCrew']['data']);

            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
            
            $publishCrew = empty($relationships['publishCrew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;

            $unAuditRule->setPublishCrew($publishCrew);
        }

        if (isset($relationships['applyCrew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['applyCrew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);

            $crew = empty($relationships['applyCrew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;

            $unAuditRule->setApplyCrew($crew);
        }
        if (isset($relationships['applyUserGroup']['data'])) {
            $applyUserGroup = $this->changeArrayFormat($relationships['applyUserGroup']['data']);
            $unAuditRule->setApplyUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($applyUserGroup));
        }

        return $unAuditRule;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($unAuditRule, array $keys = array())
    {
        $rule = parent::objectToArray($unAuditRule, $keys);
        
        if (!$unAuditRule instanceof UnAuditRule) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'rejectReason',
                'applyCrew',
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'unAuditedRules'
            )
        );

        $expression['data']['attributes'] = $rule['data']['attributes'];

        $attributes = array();

        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $unAuditRule->getRejectReason();
        }

        $expression['data']['attributes'] = array_merge($attributes, $expression['data']['attributes']);

        $expression['data']['relationships'] =
            isset($rule['data']['relationships']) ? $rule['data']['relationships'] : array();

        if (in_array('applyCrew', $keys)) {
            $expression['data']['relationships']['applyCrew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $unAuditRule->getApplyCrew()->getId()
                )
            );
        }

        return $expression;
    }
}
