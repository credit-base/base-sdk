<?php
namespace Base\Sdk\Rule\Translator;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Sdk\Template\Model\NullWbjTemplate;

class NullTemplateTranslator implements INull
{
    public function arrayToObject(array $expression, $object = null)
    {
        unset($expression);
        unset($object);
        Core::setLastError(RESOURCE_NOT_EXIST);
        return new NullWbjTemplate();
    }

    public function objectToArray($object)
    {
        unset($object);
        Core::setLastError(RESOURCE_NOT_EXIST);
        return array();
    }
}
