<?php
namespace Base\Sdk\Rule\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Rule\Model\UnAuditRule;
use Base\Sdk\Rule\Model\NullUnAuditRule;

class UnAuditRuleTranslator extends RuleTranslator
{
    public function arrayToObject(array $expression, $unAuditRule = null)
    {
        unset($unAuditRule);
        unset($expression);
        return new NullUnAuditRule();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($unAuditRule, array $keys = array())
    {
        $rule = parent::objectToArray($unAuditRule, $keys);

        if (!$unAuditRule instanceof UnAuditRule) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'applyStatus',
                'operationType',
                'relationId',
                'rejectReason',
                'applyCrew' => [],
                'applyUserGroup' => [],
            );
        }

        $expression = array();

        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $unAuditRule->getRejectReason();
        }
        if (in_array('relationId', $keys)) {
            $expression['relationId'] = $unAuditRule->getRelationId();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = [
                'id' => marmot_encode($unAuditRule->getApplyStatus()),
                'name' => IApplyCategory::APPLY_STATUS_CN[$unAuditRule->getApplyStatus()],
                'type' => IApplyCategory::APPLY_STATUS_TAG_TYPE[$unAuditRule->getApplyStatus()]
            ];
        }
        if (in_array('operationType', $keys)) {
            $expression['operationType']= [
                'id' => marmot_encode($unAuditRule->getOperationType()),
                'name' => IApplyCategory::OPERATION_TYPE_CN[$unAuditRule->getOperationType()]
            ];
        }
        if (isset($keys['applyCrew'])) {
            $expression['applyCrew'] = $this->getCrewTranslator()->objectToArray(
                $unAuditRule->getApplyCrew(),
                $keys['applyCrew']
            );
        }
        if (isset($keys['applyUserGroup'])) {
            $expression['applyUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $unAuditRule->getApplyUserGroup(),
                $keys['applyUserGroup']
            );
        }
        
        $expression = array_merge($rule, $expression);

        return $expression;
    }
}
