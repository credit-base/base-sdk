<?php
namespace Base\Sdk\Rule\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Model\NullRule;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

class RuleRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait, TemplateTranslatorTrait;

    public function arrayToObject(array $expression, $rule = null)
    {
        return $this->translateToObject($expression, $rule);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $rule = null)
    {
        if (empty($expression)) {
            return new NullRule();
        }
        
        if ($rule == null) {
            $rule = new Rule();
        }
   
        $data = $expression['data'];

        $id = $data['id'];
        $rule->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['rules'])) {
            $rule->setRules($attributes['rules']);
        }

        if (isset($attributes['dataTotal'])) {
            $rule->setDataTotal($attributes['dataTotal']);
        }

        if (isset($attributes['version'])) {
            $rule->setVersion($attributes['version']);
        }

        if (isset($attributes['transformationCategory'])) {
            $rule->setTransformationCategory($attributes['transformationCategory']);
        }

        if (isset($attributes['sourceCategory'])) {
            $rule->setSourceCategory($attributes['sourceCategory']);
        }

        if (isset($attributes['createTime'])) {
            $rule->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['statusTime'])) {
            $rule->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['updateTime'])) {
            $rule->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['status'])) {
            $rule->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        
        if (isset($relationships['crew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['crew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
           
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;
            $rule->setCrew($crew);
        }

        if (isset($relationships['userGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['userGroup']['data']);
            $rule->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }
        
        if (isset($relationships['transformationTemplate']['data'])) {
            $transformationTemplate = $this->changeArrayFormat($relationships['transformationTemplate']['data']);
            $transformationCategory =
                isset($attributes['transformationCategory']) ? $attributes['transformationCategory'] : 0;
            $templateRestfulTranslator = $this->getTemplateRestfulTranslatorByCategory($transformationCategory);
            $rule->setTransformationTemplate($templateRestfulTranslator->arrayToObject($transformationTemplate));
        }

        if (isset($relationships['sourceTemplate']['data'])) {
            $sourceTemplate = $this->changeArrayFormat($relationships['sourceTemplate']['data']);
            $sourceCategory = isset($attributes['sourceCategory']) ? $attributes['sourceCategory'] : 0;
            $templateRestfulTranslator = $this->getTemplateRestfulTranslatorByCategory($sourceCategory);
            $rule->setSourceTemplate($templateRestfulTranslator->arrayToObject($sourceTemplate));
        }
       
        return $rule;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($rule, array $keys = array())
    {
        if (!$rule instanceof Rule) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'rules',
                'transformationTemplate',
                'sourceTemplate',
                'transformationCategory',
                'sourceCategory',
                'crew'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'rules'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $rule->getId();
        }

        $attributes = array();

        if (in_array('rules', $keys)) {
            $attributes['rules'] = $rule->getRules();
        }
        if (in_array('transformationCategory', $keys)) {
            $attributes['transformationCategory'] = $rule->getTransformationCategory();
        }
        if (in_array('sourceCategory', $keys)) {
            $attributes['sourceCategory'] = $rule->getSourceCategory();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $rule->getCrew()->getId()
                )
            );
        }
        if (in_array('transformationTemplate', $keys)) {
            $expression['data']['relationships']['transformationTemplate']['data'] = array(
                array(
                    'type' => 'transformationTemplates',
                    'id' => $rule->getTransformationTemplate()->getId()
                )
            );
        }
        if (in_array('sourceTemplate', $keys)) {
            $expression['data']['relationships']['sourceTemplate']['data'] = array(
                array(
                    'type' => 'sourceTemplates',
                    'id' => $rule->getSourceTemplate()->getId()
                )
            );
        }
        
        return $expression;
    }
}
