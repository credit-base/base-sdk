<?php
namespace Base\Sdk\Rule\Translator;

trait TemplateTranslatorTrait
{
    protected function getTemplateRestfulTranslatorByCategory(int $type)
    {
        $restfulTranslatorFactory = new TemplateTranslatorFactory();
     
        return $restfulTranslatorFactory->getTemplateRestfulTranslator($type);
    }

    protected function getTemplateTranslatorByCategory(int $type)
    {
        $translatorFactory = new TemplateTranslatorFactory();
        return $translatorFactory->getTemplateTranslator($type);
    }
}
