<?php
namespace Base\Sdk\Rule\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Model\NullRule;

use Base\Sdk\Crew\Translator\CrewTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;

use Base\Sdk\Rule\Model\IRule;

class RuleTranslator implements ITranslator
{
    use TemplateTranslatorTrait;

    const CATEGORY_CN = [
        IRule::CATEGORY['BJ_RULE']=>'本级',
        IRule::CATEGORY['GB_RULE']=>'国标',
        IRule::CATEGORY['BASE_RULE']=>'基础',
        IRule::CATEGORY['WBJ_RULE']=>'委办局',
        // IRule::CATEGORY['QZJ_WBJ_RULE']=>'前置机委办局',
        // IRule::CATEGORY['QZJ_BJ_RULE']=>'前置机本级',
        // IRule::CATEGORY['QZJ_GB_RULE']=>'前置机国标',
    ];

    public function arrayToObject(array $expression, $rule = null)
    {
        unset($rule);
        unset($expression);
        return new NullRule();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

   /**
     *
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($rule, array $keys = array())
    {
        if (!$rule instanceof Rule) {
            return array();
        }
       
        if (empty($keys)) {
            $keys = array(
                'id',
                'rules',
                'dataTotal',
                'version',
                'transformationCategory',
                'sourceCategory',
                'sourceTemplate'=>[],
                'transformationTemplate'=>[],
                'crew'=>[],
                'userGroup'=>[],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($rule->getId());
        }
        if (in_array('rules', $keys)) {
            $expression['rules'] = $rule->getRules();
        }
        if (in_array('dataTotal', $keys)) {
            $expression['dataTotal'] = $rule->getDataTotal();
        }
        if (in_array('version', $keys)) {
            $expression['version'] = $rule->getVersion();
        }
        if (in_array('transformationCategory', $keys)) {
            $expression['transformationCategory'] = [
                'id'=> marmot_encode($rule->getTransformationCategory()),
                'name'=>self::CATEGORY_CN[$rule->getTransformationCategory()]
            ] ;
        }
        if (in_array('sourceCategory', $keys)) {
            $expression['sourceCategory'] = [
                'id'=> marmot_encode($rule->getSourceCategory()),
                'name'=>self::CATEGORY_CN[$rule->getSourceCategory()]
            ] ;
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($rule->getStatus()),
                'name' => IApplyCategory::STATUS_CN[$rule->getStatus()],
                'type' => IApplyCategory::STATUS_TAG_TYPE[$rule->getStatus()]
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $rule->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $rule->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $rule->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $rule->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $rule->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $rule->getCreateTime());
        }
        
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $rule->getCrew(),
                $keys['crew']
            );
        }
        
        if (isset($keys['userGroup'])) {
            $expression['userGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $rule->getUserGroup(),
                $keys['userGroup']
            );
        }
  
        if (isset($keys['sourceTemplate'])) {
            $templateTranslator = $this->getTemplateTranslatorByCategory($rule->getSourceCategory());
            $expression['sourceTemplate'] = $templateTranslator->objectToArray(
                $rule->getSourceTemplate(),
                $keys['sourceTemplate']
            );
        }
        if (isset($keys['transformationTemplate'])) {
            $templateTranslator = $this->getTemplateTranslatorByCategory($rule->getTransformationCategory());
            $expression['transformationTemplate'] = $templateTranslator->objectToArray(
                $rule->getTransformationTemplate(),
                $keys['transformationTemplate']
            );
        }
   
        return $expression;
    }
}
