<?php
namespace Base\Sdk\Rule\Command;

use Marmot\Interfaces\ICommand;

class OperationCommand implements ICommand
{
    public $id;
    
    public $rules;
    
    public function __construct(
        array $rules,
        int $id
    ) {
        $this->id = $id;
        $this->rules = $rules;
    }
}
