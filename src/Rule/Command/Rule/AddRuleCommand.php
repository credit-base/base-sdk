<?php
namespace Base\Sdk\Rule\Command\Rule;

use Marmot\Interfaces\ICommand;

class AddRuleCommand implements ICommand
{
    public $id;
    
    public $rules;

    public $transformationTemplate;

    public $sourceTemplate;

    public $transformationCategory;

    public $sourceCategory;
    
    public function __construct(
        array $rules,
        int $transformationTemplate,
        int $sourceTemplate,
        int $transformationCategory,
        int $sourceCategory,
        int $id = 0
    ) {
        $this->id = $id;
        $this->transformationTemplate = $transformationTemplate;
        $this->sourceTemplate = $sourceTemplate;
        $this->rules = $rules;
        $this->sourceCategory = $sourceCategory;
        $this->transformationCategory = $transformationCategory;
    }
}
