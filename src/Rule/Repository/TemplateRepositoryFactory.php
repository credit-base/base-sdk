<?php
namespace Base\Sdk\Rule\Repository;

use Base\Sdk\Rule\Model\IRule;

class TemplateRepositoryFactory
{
    const REPOSITORY_MAPS = [
        IRule::CATEGORY['BJ_RULE']=> 'Base\Sdk\Template\Repository\BjTemplateRepository',
        IRule::CATEGORY['GB_RULE']=> 'Base\Sdk\Template\Repository\GbTemplateRepository',
        IRule::CATEGORY['WBJ_RULE']=> 'Base\Sdk\Template\Repository\WbjTemplateRepository',
        IRule::CATEGORY['BASE_RULE']=> 'Base\Sdk\Template\Repository\BaseTemplateRepository',
    ];

    public function getTemplateRepository(string $category)
    {
        $repository = isset(self::REPOSITORY_MAPS[$category]) ? self::REPOSITORY_MAPS[$category] : '';

        return class_exists($repository) ? new $repository : false;
    }
}
