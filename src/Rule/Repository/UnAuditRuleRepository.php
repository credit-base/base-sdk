<?php
namespace Base\Sdk\Rule\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\OperateAbleRepositoryTrait;
use Base\Sdk\Common\Repository\ModifyStatusAbleRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\ApplyAbleRepositoryTrait;

use Base\Sdk\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter;
use Base\Sdk\Rule\Adapter\UnAuditRule\IUnAuditRuleAdapter;

class UnAuditRuleRepository extends Repository implements IUnAuditRuleAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        ApplyAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'UN_AUDIT_RULE_LIST';
    const FETCH_ONE_MODEL_UN = 'UN_AUDIT_RULE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new UnAuditRuleRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
