<?php
namespace Base\Sdk\Rule\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\OperateAbleRepositoryTrait;
use Base\Sdk\Common\Repository\ModifyStatusAbleRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;

use Base\Sdk\Rule\Adapter\Rule\RuleRestfulAdapter;
use Base\Sdk\Rule\Adapter\Rule\IRuleAdapter;

class RuleRepository extends Repository implements IRuleAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'RULE_LIST';
    const FETCH_ONE_MODEL_UN = 'RULE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new RuleRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
