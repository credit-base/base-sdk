<?php
namespace Base\Sdk\Rule\CommandHandler\Rule;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\CommandHandler\DeleteCommandHandler;

class DeleteRuleCommandHandler extends DeleteCommandHandler
{
    use RuleCommandHandlerTrait;
    
    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchRule($id);
    }
}
