<?php
namespace Base\Sdk\Rule\CommandHandler\Rule;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Rule\Command\Rule\EditRuleCommand;

class EditRuleCommandHandler implements ICommandHandler
{
    use RuleCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof EditRuleCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $rule = $this->fetchRule($command->id);
        
        $rule->setRules($command->rules);
       
        return $rule->edit();
    }
}
