<?php
namespace Base\Sdk\Rule\CommandHandler\Rule;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Repository\RuleRepository;

use Marmot\Interfaces\ICommand;

trait RuleCommandHandlerTrait
{
    private $rule;

    private $repository;
    
    public function __construct()
    {
        $this->rule = new Rule();
        $this->repository = new RuleRepository();
    }

    public function __destruct()
    {
        unset($this->rule);
        unset($this->repository);
    }

    protected function getRule() : Rule
    {
        return $this->rule;
    }

    protected function getRepository() : RuleRepository
    {
        return $this->repository;
    }
    
    protected function fetchRule(int $id) : Rule
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }
}
