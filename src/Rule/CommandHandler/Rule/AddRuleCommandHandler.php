<?php
namespace Base\Sdk\Rule\CommandHandler\Rule;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Rule\Command\Rule\AddRuleCommand;

use Base\Sdk\Rule\Repository\TemplateRepositoryFactory;

use Base\Sdk\Template\Model\Template;

class AddRuleCommandHandler implements ICommandHandler
{
    use RuleCommandHandlerTrait;

    protected function getTemplateRepositoryByCategory(int $category)
    {
        $repositoryFactory = new TemplateRepositoryFactory();
        return $repositoryFactory->getTemplateRepository($category);
    }
    
    protected function fetchTemplate(int $id, int $category) : Template
    {
        $repository = $this->getTemplateRepositoryByCategory($category);
       
        return $repository->fetchOne($id);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof AddRuleCommand)) {
            throw new \InvalidArgumentException;
        }

        $rule = $this->getRule();

        $rule->setRules($command->rules);
        $rule->setTransformationCategory($command->transformationCategory);
        $rule->setSourceCategory($command->sourceCategory);

        $sourceTemplate = $this->fetchTemplate(
            $command->sourceTemplate,
            $command->sourceCategory
        );
       
        $rule->setSourceTemplate($sourceTemplate);
      
        $transformationTemplate = $this->fetchTemplate(
            $command->transformationTemplate,
            $command->transformationCategory
        );
        
        $rule->setTransformationTemplate($transformationTemplate);

        return $rule->add();
    }
}
