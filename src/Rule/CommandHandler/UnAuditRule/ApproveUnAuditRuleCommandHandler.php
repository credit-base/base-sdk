<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\ApproveCommandHandler;

class ApproveUnAuditRuleCommandHandler extends ApproveCommandHandler
{
    use UnAuditRuleCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditRule($id);
    }
}
