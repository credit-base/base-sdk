<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\Rule\Model\UnAuditRule;
use Base\Sdk\Rule\Repository\UnAuditRuleRepository;

use Marmot\Interfaces\ICommand;

trait UnAuditRuleCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new UnAuditRuleRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : UnAuditRuleRepository
    {
        return $this->repository;
    }
    
    protected function fetchUnAuditRule(int $id) : UnAuditRule
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }
}
