<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Rule\Command\UnAuditRule\EditUnAuditRuleCommand;

class EditUnAuditRuleCommandHandler implements ICommandHandler
{
    use UnAuditRuleCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof EditUnAuditRuleCommand)) {
            throw new \InvalidArgumentException;
        }
     
        $unAuditRule = $this->fetchUnAuditRule($command->id);

        $unAuditRule->setRules($command->rules);

        return $unAuditRule->edit();
    }
}
