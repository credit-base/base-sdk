<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Basecode\Classes\NullCommandHandler;

class UnAuditRuleCommandHandlerFactory implements ICommandHandlerFactory
{
    public $maps;

    public function __construct(array $maps = array())
    {
        $this->maps = $maps;
    }

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);

        $commandHandler = isset($this->maps[$commandClass]) ? $this->maps[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
