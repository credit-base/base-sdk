<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\CommandHandler\RevokeCommandHandler;

class RevokeUnAuditRuleCommandHandler extends RevokeCommandHandler
{
    use UnAuditRuleCommandHandlerTrait;
    
    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchUnAuditRule($id);
    }
}
