<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Base\Sdk\ResourceCatalog\Model\Task\Task;
use Base\Sdk\Template\Model\Template;
use Base\Sdk\ResourceCatalog\Repository\ErrorDataRepository;

class ErrorData implements IObject
{
    use Object;

    const STATUS = [
        'NORMAL' => 0,
        'PROGRAM_EXCEPTION' => 1,
        'STORAGE_EXCEPTION' => 2,
    ];

    const CATEGORY = [
        'BJ'=>1,
        'GB'=>2,
        'WBJ'=>10,
        'QZJ_WBJ'=>20,
        'QZJ_BJ'=>21,
        'QZJ_GB'=>22,
    ];

    const ERROR_TYPE = [
        'MISSING_DATA' => 1,
        'COMPARISON_FAILED' => 2,
        'DUPLICATION_DATA' => 4,
        'FORMAT_VALIDATION_FAILED' => 8,
    ];

    private $id;

    private $category;

    private $template;

    private $itemsData;

    private $errorType;

    private $errorReason;

    private $task;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->category = 0;
        $this->errorType = 0;
        $this->errorReason = array();
        $this->itemsData = array();
        $this->task = new Task();
        $this->template = new Template();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new ErrorDataRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->category);
        unset($this->total);
        unset($this->template);
        unset($this->itemsData);
        unset($this->errorType);
        unset($this->errorReason);
        unset($this->task);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTask(Task $task) : void
    {
        $this->task = $task;
    }

    public function getTask() : Task
    {
        return $this->task;
    }

    public function setCategory(int $category) : void
    {
        $this->category = $category;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setTemplate(Template $template) : void
    {
        $this->template = $template;
    }

    public function getTemplate() : Template
    {
        return $this->template;
    }

    public function setItemsData(array $itemsData) : void
    {
        $this->itemsData = $itemsData;
    }

    public function getItemsData() : array
    {
        return $this->itemsData;
    }

    public function setErrorType(int $errorType) : void
    {
        $this->errorType = $errorType;
    }

    public function getErrorType() : int
    {
        return $this->errorType;
    }

    public function setErrorReason(array $errorReason) : void
    {
        $this->errorReason = $errorReason;
    }

    public function getErrorReason() : array
    {
        return $this->errorReason;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    protected function getRepository() : ErrorDataRepository
    {
        return $this->repository;
    }
}
