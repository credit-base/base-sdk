<?php
namespace Base\Sdk\ResourceCatalog\Model;

class ISearchData
{
    const INFO_CLASSIFY = array(
        'NULL'=>0,
        'XZXK'=>1,
        'XZCF'=>2,
        'HONGMD'=>3,
        'HEIMD'=>4,
        'QT'=>5
    );

    const INFO_CLASSIFY_CN = array(
        self::INFO_CLASSIFY['NULL'] => "",
        self::INFO_CLASSIFY['XZXK'] => "行政许可",
        self::INFO_CLASSIFY['XZCF'] => "行政处罚",
        self::INFO_CLASSIFY['HONGMD'] => "红名单",
        self::INFO_CLASSIFY['HEIMD'] => "黑名单",
        self::INFO_CLASSIFY['QT'] => "其他",
    );

    const INFO_CLASSIFY_LABEL = array(
        self::INFO_CLASSIFY['NULL'] => "",
        self::INFO_CLASSIFY['XZXK'] => "许可",
        self::INFO_CLASSIFY['XZCF'] => "处罚",
        self::INFO_CLASSIFY['HONGMD'] => "红",
        self::INFO_CLASSIFY['HEIMD'] => "黑",
        self::INFO_CLASSIFY['QT'] => "其他",
    );

    const INFO_CLASSIFY_TYPE = array(
        self::INFO_CLASSIFY['NULL'] => "primary",
        self::INFO_CLASSIFY['XZXK'] => "danger",
        self::INFO_CLASSIFY['XZCF'] => "info",
        self::INFO_CLASSIFY['HONGMD'] => "danger",
        self::INFO_CLASSIFY['HEIMD'] => "info",
        self::INFO_CLASSIFY['QT'] => "primary",
    );

    const INFO_CATEGORY = array(
        'NULL'=>0,
        'JCXX' => 1,
        'SHOUXXX' => 2,
        'SHIXXX' => 3,
        'QTXX' => 4
    );

    const INFO_CATEGORY_CN = array(
        self::INFO_CATEGORY['JCXX'] => "基础信息",
        self::INFO_CATEGORY['SHOUXXX'] => "守信信息",
        self::INFO_CATEGORY['SHIXXX'] => "失信信息",
        self::INFO_CATEGORY['QTXX'] => "其他信息"
    );

    const INFO_CATEGORY_LABEL = array(
        self::INFO_CATEGORY['NULL'] => "",
        self::INFO_CATEGORY['JCXX'] => "基础",
        self::INFO_CATEGORY['SHOUXXX'] => "守信",
        self::INFO_CATEGORY['SHIXXX'] => "失信",
        self::INFO_CATEGORY['QTXX'] => "其他"
    );

    const INFO_CATEGORY_TYPE = array(
        self::INFO_CATEGORY['NULL'] => "primary",
        self::INFO_CATEGORY['JCXX'] => "primary",
        self::INFO_CATEGORY['SHOUXXX'] => "success",
        self::INFO_CATEGORY['SHIXXX'] => "danger",
        self::INFO_CATEGORY['QTXX'] => "primary"
    );

    const SUBJECT_CATEGORY = array(
        'FRJFFRZZ' => 1,
        'ZRR' => 2,
        'GTGSH' => 3,
    );

    const SUBJECT_CATEGORY_CN = array(
        self::SUBJECT_CATEGORY['FRJFFRZZ'] => "法人及非法人组织",
        self::SUBJECT_CATEGORY['ZRR'] => "自然人",
        self::SUBJECT_CATEGORY['GTGSH'] => "个体工商户"
    );

    const DIMENSION = array(
        'NULL' => 0,
        'SHGK' => 1,
        'ZWGX' => 2,
        'SQCX' => 3,
    );

    const DIMENSION_CN = array(
        self::DIMENSION['NULL'] => "暂无",
        self::DIMENSION['SHGK'] => "社会公开",
        self::DIMENSION['ZWGX'] => "政务共享",
        self::DIMENSION['SQCX'] => "授权查询"
    );

    const DIMENSION_LABEL = array(
        self::DIMENSION['NULL'] => "无",
        self::DIMENSION['SHGK'] => "社",
        self::DIMENSION['ZWGX'] => "政",
        self::DIMENSION['SQCX'] => "授"
    );

    const DIMENSION_TYPE = array(
        self::DIMENSION['NULL'] => "danger",
        self::DIMENSION['SHGK'] => "primary",
        self::DIMENSION['ZWGX'] => "primary",
        self::DIMENSION['SQCX'] => "primary"
    );

    const DATA_STATUS_CN = [
        0 => '待确认',
        2 => '已确认',
        -2 => '已屏蔽',
        -4 => '已封存',
    ];

    const DATA_STATUS_TYPE = [
        0 => 'warning',
        2 => 'success',
        -2 => 'danger',
        -4 => 'info',
    ];
}
