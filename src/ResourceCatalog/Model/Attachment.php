<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Template\Repository\WbjTemplateRepository;
use Base\Sdk\ResourceCatalog\Repository\AttachmentRepository;

/**
 * @SuppressWarnings(PHPMD.Superglobals)
 */
class Attachment implements IObject
{
    use Object;

    const MAX_FILE_SIZE = 10 * 1024 * 1024; //10MB

    const UPLOAD_DEFAULT_COUNT = 0;

    const ALLOW_FILE_EXTS = array(
        'xls', 'xlsx'
    );
    /**
     * @var int id
     */
    private $id;

    /**
     * @var string 文件名
     */
    protected $name;

    /**
     * @var string 文件上传路径
     */
    protected $path;

    /**
     * @var int 文件大小
     */
    protected $size;

    /**
     * @var string 文件hash
     */
    protected $hash;

    /**
     * @var Crew $crew
     */
    protected $crew;

    /**
     * @var string $source
     */
    protected $source;

    /**
     * @var string $extension
     */
    protected $extension;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->path = Core::$container->get('attachment.upload.path');
        $this->size = 0;
        $this->hash = '';
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = Core::$container->get('time');
        $this->source = '';
        $this->extension = '';
        $this->status = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->path);
        unset($this->size);
        unset($this->hash);
        unset($this->crew);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->status);
        unset($this->source);
        unset($this->extension);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setExtension(string $extension)
    {
        $this->extension = $extension;
    }

    public function getExtension() : string
    {
        return $this->extension;
    }

    public function getPath() : string
    {
        return $this->path;
    }

    public function setSize(int $size)
    {
        $this->size = $size;
    }

    public function getSize() : int
    {
        return $this->size;
    }

    public function setHash(string $hash)
    {
        $this->hash = $hash;
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    public function setSource(string $source)
    {
        $this->source = $source;
    }

    public function getSource() : string
    {
        return $this->source;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function upload(int $templateId, string $prefix = 'attachment') : bool
    {
        if (!$this->validateUploadFile()) {
            return false;
        }
      
        $this->prepare($prefix);

        if (!$this->validateNameFormat($templateId)) {
            return false;
        }

        if (!$this->validate()) {
            return false;
        }

        $destination = $this->generateFilePath();
        $source = $this->getSource();

        return $this->move($source, $destination);
    }

    protected function prepare(string $prefix) : void
    {
        $this->setSize($_FILES[$prefix]['size']);
        $name = explode('.', $_FILES[$prefix]['name']);
        $this->setName($name[0]);
        $this->setExtension($name[1]);
        $this->setSource($_FILES[$prefix]['tmp_name']);
        $this->generateHash();
    }

    protected function generateHash() : void
    {
        $hash = md5(md5_file($this->getSource()).$this->getSize());
        $this->setHash($hash);
    }

    /**
     * 生产文件路径
     * 员工id_hash_资源目录标识_资源目录类型.后缀
     */
    protected function generateFilePath() : string
    {
        return $this->getPath().
        $this->getName().'_'.
        $this->getCrew()->getId().'_'.
        $this->getHash().'.'.
        $this->getExtension();
    }

    protected function move(string $source, string $destination)
    {
        return move_uploaded_file($source, $destination);
    }

    /**
     * 检查上传文件变量是否正确
     */
    protected function validateUploadFile() : bool
    {
        if (empty($_FILES)) {
            Core::setLastError(FILE_IS_EMPTY);
            return false;
        }

        return true;
    }

    protected function validate() : bool
    {
        return $this->validateSize() &&
               $this->validateExtension() &&
               $this->validateContentDuplicate();
    }

    /**
     * 检查文件大小是否超过限制
     */
    protected function validateSize() : bool
    {
        if ($this->size > self::MAX_FILE_SIZE) {
            Core::setLastError(FILE_SIZE_FORMAT_ERROR);
            return false;
        }
        return true;
    }

    /**
     * 检查文件后缀
     */
    protected function validateExtension() : bool
    {
        if (!in_array($this->getExtension(), self::ALLOW_FILE_EXTS)) {
            Core::setLastError(FILE_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    /**
     * 检查文件名格式
     * 资源目录标识_资源目录id_资源目录类型.xlsx
     * 检查资源目录标识 和 资源目录类型是否正确
     */
    protected function validateNameFormat(int $templateId) : bool
    {
        $name = $this->getName();
        $name = explode('_', $name);
    
        $identify = $name[0];
        $id = $name[1];
        $category = $name[2];

        $template = $this->getTemplateById($templateId);
        $templateIdentify = $template->getIdentify();
        $templateCategory = $template->getCategory();

        if ($identify != $templateIdentify) {
            Core::setLastError(FILE_IDENTIFY_FORMAT_ERROR);
            return false;
        }

        if ($category != $templateCategory) {
            Core::setLastError(FILE_CATEGORY_FORMAT_ERROR);
            return false;
        }

        if ($id != $templateId) {
            Core::setLastError(FILE_IDENTIFY_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    /**
     * 检查文件后缀
     * 通过hash查询后端服务层上传记录，检查文件是否有重复
     */
    protected function validateContentDuplicate() : bool
    {
        if ($this->isExitByHash()) {
            Core::setLastError(FILE_IS_UNIQUE);
            return false;
        }
        return true;
    }

    protected function getWbjTemplateRepository() : WbjTemplateRepository
    {
        return new WbjTemplateRepository();
    }
    /**
     * 查询资源目录数据详情
     */
    protected function getTemplateById(int $templateId)
    {
        $data = $this->getWbjTemplateRepository()
                ->scenario(WbjTemplateRepository::FETCH_ONE_MODEL_UN)
                ->fetchOne($templateId);
        
        if ($data instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        return $data;
    }

    protected function getAttachmentRepository() : AttachmentRepository
    {
        return new AttachmentRepository();
    }
    /**
     * 根据hash获取后端服务层上传记录
     */
    protected function isExitByHash() : bool
    {
        $hash = $this->getHash();

        $filter = [];
        $filter['hash'] = $hash;

        list($count, $list) = $this->getAttachmentRepository()
                ->scenario(WbjTemplateRepository::LIST_MODEL_UN)
                ->search($filter, ['id'], PAGE, PAGE);
        unset($list);

        if ($count == self::UPLOAD_DEFAULT_COUNT) {
            return false;
        }

        return true;
    }
}
