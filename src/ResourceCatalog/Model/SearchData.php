<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\Model\EnableAbleTrait;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;

abstract class SearchData implements IEnableAble, IObject
{
    const EXPIRATION_DATE_DEFAULT = 4102329600; //20991231

    const DATA_STATUS = array(
        'CONFIRM' => 0, //待确认
        'ENABLED' => 2, //已确认
        'DISABLED' => -2, //屏蔽
        'DELETED' => -4, //封存
    );

    use EnableAbleTrait,Object;
    /**
     * [$id 主键Id]
     * @var [int]
     */
    protected $id;
    /**
     * [$infoClassify 信息分类]
     * @var [int]
     */
    protected $infoClassify;
    /**
     * [$infoCategory 信息类别]
     * @var [int]
     */
    protected $infoCategory;
    /**
     * [$subjectCategory 主体类别]
     * @var [int]
     */
    protected $subjectCategory;
    /**
     * [$dimension 公开范围]
     * @var [int]
     */
    protected $dimension;
    /**
     * [$name 主体名称]
     * @var [string]
     */
    protected $name;
    /**
     * [$expirationDate 有效期限]
     * @var [int]
     */
    protected $expirationDate;
    /**
     * [$identify 主体标识]
     * @var [string]
     */
    private $identify;
    /**
     * [$crew 发布人]
     * @var [Crew]
     */
    private $crew;
    /**
     * [$template 资源目录]
     * @var [Template]
     */
    private $template;
    /**
     * [$sourceUnit 来源单位]
     * @var [UserGroup]
     */
    private $sourceUnit;
    /**
     * [$itemsData 资源目录数据]
     * @var [ItemsData]
     */
    private $itemsData;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->infoClassify = 0;
        $this->infoCategory = 0;
        $this->status = self::DATA_STATUS['CONFIRM'];
        $this->crew = new Crew();
        $this->sourceUnit = new UserGroup();
        $this->subjectCategory = 0;
        $this->dimension = 0;
        $this->name = '';
        $this->identify = '';
        $this->expirationDate = self::EXPIRATION_DATE_DEFAULT;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->status);
        unset($this->infoClassify);
        unset($this->infoCategory);
        unset($this->crew);
        unset($this->sourceUnit);
        unset($this->subjectCategory);
        unset($this->dimension);
        unset($this->name);
        unset($this->identify);
        unset($this->expirationDate);
        unset($this->template);
        unset($this->itemsData);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    /**
     * @marmot-set-id
     */
    public function setId($id) : void
    {
        $this->id = $id;
    }
    /**
     * @marmot-get-id
     */
    public function getId()
    {
        return $this->id;
    }

    public function setInfoClassify(int $infoClassify) : void
    {
        $this->infoClassify = $infoClassify;
    }

    public function getInfoClassify() : int
    {
        return $this->infoClassify;
    }

    public function setInfoCategory(int $infoCategory) : void
    {
        $this->infoCategory = $infoCategory;
    }

    public function getInfoCategory() : int
    {
        return $this->infoCategory;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setSourceUnit(UserGroup $sourceUnit) : void
    {
        $this->sourceUnit = $sourceUnit;
    }

    public function getSourceUnit() : UserGroup
    {
        return $this->sourceUnit;
    }

    public function setSubjectCategory(int $subjectCategory) : void
    {
        $this->subjectCategory = $subjectCategory;
    }

    public function getSubjectCategory() : int
    {
        return $this->subjectCategory;
    }

    public function setDimension(int $dimension) : void
    {
        $this->dimension = $dimension;
    }

    public function getDimension() : int
    {
        return $this->dimension;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setIdentify(string $identify) : void
    {
        $this->identify = $identify;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }

    public function setExpirationDate(int $expirationDate) : void
    {
        $this->expirationDate = $expirationDate;
    }

    public function getExpirationDate() : int
    {
        return $this->expirationDate;
    }

    public function isConfirm() : bool
    {
        return $this->getStatus() == self::DATA_STATUS['CONFIRM'];
    }

    public function isConfirmed() : bool
    {
        return $this->getStatus() == self::DATA_STATUS['ENABLED'];
    }

    abstract public function setStatus(int $status) : void;

    abstract public function getTemplate();

    abstract public function getItemsData();

    abstract protected function getRepository();
}
