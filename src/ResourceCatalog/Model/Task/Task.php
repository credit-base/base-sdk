<?php
namespace Base\Sdk\ResourceCatalog\Model\Task;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;

class Task implements IObject
{
    use Object;

    const STATUS = [
        'PENDING' => 0,
        'SUCCESS' => 2,
        'FAILURE' => -2,
        'FAILURE_FILE_DOWNLOAD' => -3,
    ];

    const CATEGORY = [
        'BJ'=>1,
        'GB'=>2,
        'WBJ'=>10,
        'QZJ_WBJ'=>20,
        'QZJ_BJ'=>21,
        'QZJ_GB'=>22,
    ];

    private $id;

    private $pid;

    private $total;

    private $successNumber;

    private $failureNumber;

    private $sourceCategory;

    private $sourceTemplate;

    private $targetCategory;

    private $targetTemplate;

    private $targetRule;

    private $crew;

    private $userGroup;

    private $fileName;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->pid = 0;
        $this->total = 0;
        $this->successNumber = 0;
        $this->failureNumber = 0;
        $this->sourceCategory = 0;
        $this->sourceTemplate = 0;
        $this->targetCategory = 0;
        $this->targetTemplate = 0;
        $this->targetRule = 0;
        $this->crew = new Crew();
        $this->userGroup = new UserGroup();
        $this->status = self::STATUS['PENDING'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->fileName = '';
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->pid);
        unset($this->total);
        unset($this->successNumber);
        unset($this->failureNumber);
        unset($this->sourceCategory);
        unset($this->sourceTemplate);
        unset($this->targetCategory);
        unset($this->targetTemplate);
        unset($this->targetRule);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->fileName);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setPid(int $pid) : void
    {
        $this->pid = $pid;
    }

    public function getPid() : int
    {
        return $this->pid;
    }

    public function setTotal(int $total) : void
    {
        $this->total = $total;
    }

    public function getTotal() : int
    {
        return $this->total;
    }

    public function setSuccessNumber(int $successNumber) : void
    {
        $this->successNumber = $successNumber;
    }

    public function getSuccessNumber() : int
    {
        return $this->successNumber;
    }

    public function setFailureNumber(int $failureNumber) : void
    {
        $this->failureNumber = $failureNumber;
    }

    public function getFailureNumber() : int
    {
        return $this->failureNumber;
    }

    public function setSourceCategory(int $sourceCategory) : void
    {
        $this->sourceCategory = $sourceCategory;
    }

    public function getSourceCategory() : int
    {
        return $this->sourceCategory;
    }

    public function setSourceTemplate(int $sourceTemplate) : void
    {
        $this->sourceTemplate = $sourceTemplate;
    }

    public function getSourceTemplate() : int
    {
        return $this->sourceTemplate;
    }

    public function setTargetCategory(int $targetCategory) : void
    {
        $this->targetCategory = $targetCategory;
    }

    public function getTargetCategory() : int
    {
        return $this->targetCategory;
    }

    public function setTargetTemplate(int $targetTemplate) : void
    {
        $this->targetTemplate = $targetTemplate;
    }

    public function getTargetTemplate() : int
    {
        return $this->targetTemplate;
    }

    public function setTargetRule(int $targetRule) : void
    {
        $this->targetRule = $targetRule;
    }

    public function getTargetRule() : int
    {
        return $this->targetRule;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function setFileName(string $fileName) : void
    {
        $this->fileName = $fileName;
    }

    public function getFileName() : string
    {
        return $this->fileName;
    }
}
