<?php
namespace Base\Sdk\ResourceCatalog\Model\Task;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullTask extends Task implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
