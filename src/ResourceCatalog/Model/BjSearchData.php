<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\Model\ModifyStatusAbleTrait;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;

use Base\Sdk\ResourceCatalog\Model\SearchData;
use Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository;
use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\ResourceCatalog\Adapter\BjSearchData\IBjSearchDataAdapter;

class BjSearchData extends SearchData implements IModifyStatusAble
{
    use ModifyStatusAbleTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->status = self::DATA_STATUS['CONFIRM'];
        $this->template = new BjTemplate();
        $this->itemsData = new ItemsData();
        $this->repository = new BjSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->status);
        unset($this->template);
        unset($this->itemsData);
        unset($this->repository);
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setTemplate(BjTemplate $template) : void
    {
        $this->template = $template;
    }

    public function getTemplate() : BjTemplate
    {
        return $this->template;
    }

    public function setItemsData(ItemsData $itemsData) : void
    {
        $this->itemsData = $itemsData;
    }

    public function getItemsData() : ItemsData
    {
        return $this->itemsData;
    }

    protected function getRepository() : IBjSearchDataAdapter
    {
        return $this->repository;
    }

    protected function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * [confirm 确认]
     * @return [type] [bool]
     */
    public function confirm() : bool
    {
        if (!$this->isConfirm()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }

        return $this->getRepository()->confirm($this);
    }

    /**
     * [disable 屏蔽]
     * @return [type] [bool]
     */
    public function disable() : bool
    {
        if (!$this->isConfirmed()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }

        $bjSearchDataAdapter = $this->getIEnableAbleAdapter();
        return $bjSearchDataAdapter->disable($this);
    }

    /**
     * [deletes 封存]
     * @return [type] [bool]
     */
    public function deletes() : bool
    {
        if (!$this->isConfirm()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }

        $bjSearchDataAdapter = $this->getIModifyStatusAbleAdapter();
        return $bjSearchDataAdapter->deletes($this);
    }
}
