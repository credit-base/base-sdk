<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;

use Base\Sdk\ResourceCatalog\Model\SearchData;
use Base\Sdk\ResourceCatalog\Repository\WbjSearchDataRepository;
use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\ResourceCatalog\Adapter\WbjSearchData\IWbjSearchDataAdapter;

class WbjSearchData extends SearchData
{
    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->template = new WbjTemplate();
        $this->itemsData = new ItemsData();
        $this->repository = new WbjSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->status);
        unset($this->template);
        unset($this->itemsData);
        unset($this->repository);
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setTemplate(WbjTemplate $template) : void
    {
        $this->template = $template;
    }

    public function getTemplate() : WbjTemplate
    {
        return $this->template;
    }

    public function setItemsData(ItemsData $itemsData) : void
    {
        $this->itemsData = $itemsData;
    }

    public function getItemsData() : ItemsData
    {
        return $this->itemsData;
    }

    protected function getRepository() : IWbjSearchDataAdapter
    {
        return $this->repository;
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

     /**
     * [disable 屏蔽]
     * @return [type] [bool]
     */
    public function disable() : bool
    {
        if (!$this->isConfirm()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }

        $wbjSearchDataAdapter = $this->getIEnableAbleAdapter();
        return $wbjSearchDataAdapter->disable($this);
    }
}
