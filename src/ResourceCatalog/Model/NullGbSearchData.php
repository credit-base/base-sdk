<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Sdk\Common\Model\NullEnableAbleTrait;
use Base\Sdk\Common\Model\NullModifyStatusAbleTrait;

class NullGbSearchData extends GbSearchData implements INull
{
    use NullEnableAbleTrait, NullModifyStatusAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function confirm() : bool
    {
        return $this->resourceNotExist();
    }
}
