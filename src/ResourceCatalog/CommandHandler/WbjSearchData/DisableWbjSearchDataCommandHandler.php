<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\WbjSearchData;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\CommandHandler\DisableCommandHandler;

class DisableWbjSearchDataCommandHandler extends DisableCommandHandler
{
    use WbjSearchDataCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchWbjSearchData($id);
    }
}
