<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository;

trait GbSearchDataCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new GbSearchDataRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : GbSearchDataRepository
    {
        return $this->repository;
    }
    
    protected function fetchGbSearchData(int $id) : GbSearchData
    {
        return $this->getRepository()->fetchOne($id);
    }
}
