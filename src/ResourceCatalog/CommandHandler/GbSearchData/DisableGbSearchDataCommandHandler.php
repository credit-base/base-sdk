<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\CommandHandler\DisableCommandHandler;

class DisableGbSearchDataCommandHandler extends DisableCommandHandler
{
    use GbSearchDataCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchGbSearchData($id);
    }
}
