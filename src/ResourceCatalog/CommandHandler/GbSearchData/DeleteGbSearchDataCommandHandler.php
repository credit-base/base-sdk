<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\CommandHandler\DeleteCommandHandler;

class DeleteGbSearchDataCommandHandler extends DeleteCommandHandler
{
    use GbSearchDataCommandHandlerTrait;
    
    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchGbSearchData($id);
    }
}
