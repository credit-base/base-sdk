<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\ResourceCatalog\Command\GbSearchData\ConfirmGbSearchDataCommand;

class ConfirmGbSearchDataCommandHandler implements ICommandHandler
{
    use GbSearchDataCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof ConfirmGbSearchDataCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $gbSearchData = $this->fetchGbSearchData($command->id);

        return $gbSearchData->confirm();
    }
}
