<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\CommandHandler\DisableCommandHandler;

class DisableBjSearchDataCommandHandler extends DisableCommandHandler
{
    use BjSearchDataCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchBjSearchData($id);
    }
}
