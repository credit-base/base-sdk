<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Marmot\Basecode\Classes\NullCommandHandler;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;

class BjSearchDataCommandHandlerFactory implements ICommandHandlerFactory
{
    public $maps;

    public function __construct(array $maps = array())
    {
        $this->maps = $maps;
    }

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset($this->maps[$commandClass]) ? $this->maps[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
