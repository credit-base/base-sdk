<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\CommandHandler\DeleteCommandHandler;

class DeleteBjSearchDataCommandHandler extends DeleteCommandHandler
{
    use BjSearchDataCommandHandlerTrait;
    
    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchBjSearchData($id);
    }
}
