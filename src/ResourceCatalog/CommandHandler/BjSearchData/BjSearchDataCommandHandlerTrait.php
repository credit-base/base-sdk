<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

trait BjSearchDataCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new BjSearchDataRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : BjSearchDataRepository
    {
        return $this->repository;
    }
    
    protected function fetchBjSearchData(int $id) : BjSearchData
    {
        return $this->getRepository()->fetchOne($id);
    }
}
