<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\ResourceCatalog\Command\BjSearchData\ConfirmBjSearchDataCommand;

class ConfirmBjSearchDataCommandHandler implements ICommandHandler
{
    use BjSearchDataCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof ConfirmBjSearchDataCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $bjSearchData = $this->fetchBjSearchData($command->id);

        return $bjSearchData->confirm();
    }
}
