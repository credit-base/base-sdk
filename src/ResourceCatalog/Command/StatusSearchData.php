<?php
namespace Base\Sdk\ResourceCatalog\Command;

use Marmot\Interfaces\ICommand;

abstract class StatusSearchData implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
