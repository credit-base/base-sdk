<?php
namespace Base\Sdk\ResourceCatalog\Adapter\GbSearchData;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Model\NullGbSearchData;
use Base\Sdk\ResourceCatalog\Translator\GbSearchDataRestfulTranslator;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;

class GbSearchDataRestfulAdapter extends GuzzleAdapter implements IGbSearchDataAdapter
{
    use FetchAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        ModifyStatusAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'GB_SEARCH_DATA_LIST'=>[
            'fields'=>[],
            'include'=>'crew,sourceUnit,itemsData,template'
        ],
        'GB_SEARCH_DATA_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,sourceUnit,itemsData,template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new GbSearchDataRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'gbSearchData';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullGbSearchData::getInstance());
    }

    //确认
    public function confirm(GbSearchData $gbSearchData) : bool
    {
        $this->patch(
            $this->getResource().'/'.$gbSearchData->getId().'/confirm'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($gbSearchData);
            return true;
        }
        return false;
    }
}
