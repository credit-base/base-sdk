<?php
namespace Base\Sdk\ResourceCatalog\Adapter\GbSearchData;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

interface IGbSearchDataAdapter extends IAsyncAdapter, IFetchAbleAdapter, IEnableAbleAdapter, IModifyStatusAbleAdapter
{
}
