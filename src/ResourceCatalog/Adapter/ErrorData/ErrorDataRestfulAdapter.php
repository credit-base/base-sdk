<?php
namespace Base\Sdk\ResourceCatalog\Adapter\ErrorData;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\ResourceCatalog\Model\NullErrorData;
use Base\Sdk\ResourceCatalog\Translator\ErrorDataRestfulTranslator;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class ErrorDataRestfulAdapter extends GuzzleAdapter implements IErrorDataAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'ERROR_DATA_LIST'=>[
            'fields'=>[],
            'include'=>'task,template'
        ],
        'ERROR_DATA_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'task,template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new ErrorDataRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'errorDatas';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }
    
    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullErrorData::getInstance());
    }
}
