<?php
namespace Base\Sdk\ResourceCatalog\Adapter\ErrorData;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IErrorDataAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
}
