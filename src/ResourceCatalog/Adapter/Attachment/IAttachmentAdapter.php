<?php
namespace Base\Sdk\ResourceCatalog\Adapter\Attachment;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IAttachmentAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
}
