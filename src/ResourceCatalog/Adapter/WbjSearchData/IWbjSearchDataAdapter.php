<?php
namespace Base\Sdk\ResourceCatalog\Adapter\WbjSearchData;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;

interface IWbjSearchDataAdapter extends IAsyncAdapter, IFetchAbleAdapter, IEnableAbleAdapter
{
}
