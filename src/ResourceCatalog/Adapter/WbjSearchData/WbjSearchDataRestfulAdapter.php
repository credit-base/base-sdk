<?php
namespace Base\Sdk\ResourceCatalog\Adapter\WbjSearchData;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\ResourceCatalog\Model\NullWbjSearchData;
use Base\Sdk\ResourceCatalog\Translator\WbjSearchDataRestfulTranslator;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class WbjSearchDataRestfulAdapter extends GuzzleAdapter implements IWbjSearchDataAdapter
{
    use FetchAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'WBJ_SEARCH_DATA_LIST'=>[
            'fields'=>[],
            'include'=>'crew,sourceUnit,itemsData,template'
        ],
        'WBJ_SEARCH_DATA_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,sourceUnit,itemsData,template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new WbjSearchDataRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'wbjSearchData';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullWbjSearchData::getInstance());
    }
}
