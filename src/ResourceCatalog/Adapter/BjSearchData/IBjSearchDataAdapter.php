<?php
namespace Base\Sdk\ResourceCatalog\Adapter\BjSearchData;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

interface IBjSearchDataAdapter extends IAsyncAdapter, IFetchAbleAdapter, IEnableAbleAdapter, IModifyStatusAbleAdapter
{
}
