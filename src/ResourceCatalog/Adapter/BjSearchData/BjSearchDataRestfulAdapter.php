<?php
namespace Base\Sdk\ResourceCatalog\Adapter\BjSearchData;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use Base\Sdk\ResourceCatalog\Model\NullBjSearchData;
use Base\Sdk\ResourceCatalog\Translator\BjSearchDataRestfulTranslator;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;

class BjSearchDataRestfulAdapter extends GuzzleAdapter implements IBjSearchDataAdapter
{
    use FetchAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        ModifyStatusAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'BJ_SEARCH_DATA_LIST'=>[
            'fields'=>[],
            'include'=>'crew,sourceUnit,itemsData,template'
        ],
        'BJ_SEARCH_DATA_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,sourceUnit,itemsData,template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new BjSearchDataRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'bjSearchData';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            100=>[
                'crew'=>PARAMETER_IS_EMPTY,
                'sourceUnit'=>DEPARTMENT_USER_GROUP_IS_EMPTY,
                'template'=>TEMPLATE_ID_FORMAT_ERROR
            ],
            103=>[
                'hash'=>IS_EXISTED_TEMPLATE_PARAMETER_FORMAT_ERROR
            ],
            10=>[
                'subjectCategory'=>SUBJECT_CATEGORY_FORMAT_ERROR,
                ''=>RESOURCE_NOT_EXIST
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullBjSearchData::getInstance());
    }

    //确认
    public function confirm(BjSearchData $bjSearchData) : bool
    {
        $this->patch(
            $this->getResource().'/'.$bjSearchData->getId().'/confirm'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($bjSearchData);
            return true;
        }
        return false;
    }
}
