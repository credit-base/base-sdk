<?php
namespace Base\Sdk\ResourceCatalog\Adapter\Task;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\ResourceCatalog\Model\Task\NullTask;
use Base\Sdk\ResourceCatalog\Translator\TaskRestfulTranslator;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class TaskRestfulAdapter extends GuzzleAdapter implements ITaskAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'TASK_LIST'=>[
            'fields'=>[],
            'include'=>'crew,userGroup'
        ],
        'TASK_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,userGroup'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new TaskRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'tasks';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }
   
    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullTask::getInstance());
    }
}
