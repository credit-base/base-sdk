<?php
namespace Base\Sdk\ResourceCatalog\Adapter\Task;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface ITaskAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
}
