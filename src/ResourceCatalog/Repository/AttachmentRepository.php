<?php
namespace Base\Sdk\ResourceCatalog\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;

use Base\Sdk\ResourceCatalog\Adapter\Attachment\AttachmentRestfulAdapter;
use Base\Sdk\ResourceCatalog\Adapter\Attachment\IAttachmentAdapter;

class AttachmentRepository extends Repository implements IAttachmentAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ATTACHMENT_LIST';
    const FETCH_ONE_MODEL_UN = 'ATTACHMENT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new AttachmentRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
