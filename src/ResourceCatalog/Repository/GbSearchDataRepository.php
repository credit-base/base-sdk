<?php
namespace Base\Sdk\ResourceCatalog\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\ModifyStatusAbleRepositoryTrait;

use Base\Sdk\ResourceCatalog\Adapter\GbSearchData\GbSearchDataMockAdapter;
use Base\Sdk\ResourceCatalog\Adapter\GbSearchData\GbSearchDataRestfulAdapter;
use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Adapter\GbSearchData\IGbSearchDataAdapter;

class GbSearchDataRepository extends Repository implements IGbSearchDataAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        EnableAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'GB_SEARCH_DATA_LIST';
    const FETCH_ONE_MODEL_UN = 'GB_SEARCH_DATA_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new GbSearchDataRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function confirm(GbSearchData $gbSearchData)
    {
        return $this->getAdapter()->confirm($gbSearchData);
    }
}
