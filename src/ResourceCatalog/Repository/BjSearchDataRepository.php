<?php
namespace Base\Sdk\ResourceCatalog\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\ModifyStatusAbleRepositoryTrait;

use Base\Sdk\ResourceCatalog\Adapter\BjSearchData\BjSearchDataMockAdapter;
use Base\Sdk\ResourceCatalog\Adapter\BjSearchData\BjSearchDataRestfulAdapter;
use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use Base\Sdk\ResourceCatalog\Adapter\BjSearchData\IBjSearchDataAdapter;

class BjSearchDataRepository extends Repository implements IBjSearchDataAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        EnableAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'BJ_SEARCH_DATA_LIST';
    const FETCH_ONE_MODEL_UN = 'BJ_SEARCH_DATA_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new BjSearchDataRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function confirm(BjSearchData $bjSearchData)
    {
        return $this->getAdapter()->confirm($bjSearchData);
    }
}
