<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\ResourceCatalog\Model\SearchData;
use Base\Sdk\ResourceCatalog\Model\ISearchData;

abstract class SearchDataTranslator implements ITranslator
{
    protected function getItemsDataTranslator() : ItemsDataTranslator
    {
        return new ItemsDataTranslator();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($searchData, array $keys = array())
    {
        if (!$searchData instanceof SearchData) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'infoClassify',
                'infoCategory',
                'subjectCategory',
                'dimension',
                'name',
                'identify',
                'expirationDate',
                'crew',
                'sourceUnit',
                'itemsData',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($searchData->getId());
        }
        if (in_array('infoClassify', $keys)) {
            $expression['infoClassify']['id'] = marmot_encode($searchData->getInfoClassify());
            $expression['infoClassify']['name'] = ISearchData::INFO_CLASSIFY_CN[$searchData->getInfoClassify()];
            $expression['infoClassify']['label'] = ISearchData::INFO_CLASSIFY_LABEL[$searchData->getInfoClassify()];
            $expression['infoClassify']['type'] = ISearchData::INFO_CLASSIFY_TYPE[$searchData->getInfoClassify()];
        }
        if (in_array('infoCategory', $keys)) {
            $expression['infoCategory']['id'] = marmot_encode($searchData->getInfoCategory());
            $expression['infoCategory']['name'] = ISearchData::INFO_CATEGORY_CN[$searchData->getInfoCategory()];
            $expression['infoCategory']['label'] = ISearchData::INFO_CATEGORY_LABEL[$searchData->getInfoCategory()];
            $expression['infoCategory']['type'] = ISearchData::INFO_CATEGORY_TYPE[$searchData->getInfoCategory()];
        }
        if (in_array('subjectCategory', $keys)) {
            $subjectCategory = $searchData->getSubjectCategory();
            $expression['subjectCategory'] =
                empty($subjectCategory) ? [] : $this->getSubjectCategoryToArray($subjectCategory);
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension']['id'] = marmot_encode($searchData->getDimension());
            $expression['dimension']['name'] = ISearchData::DIMENSION_CN[$searchData->getDimension()];
            $expression['dimension']['label'] = ISearchData::DIMENSION_LABEL[$searchData->getDimension()];
            $expression['dimension']['type']  = ISearchData::DIMENSION_TYPE[$searchData->getDimension()];
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $searchData->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $searchData->getIdentify();
        }
        if (in_array('expirationDate', $keys)) {
            $expression['expirationDate'] = $searchData->getExpirationDate();
            $expression['expirationDateFormat'] = date(
                'Y-m-d',
                $searchData->getExpirationDate()
            );
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $searchData->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $searchData->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $searchData->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $searchData->getStatusTime();
        }

        if (in_array('crew', $keys)) {
            $expression['crew']['id'] = marmot_encode($searchData->getCrew()->getId());
            $expression['crew']['name'] = $searchData->getCrew()->getRealName();
        }

        if (in_array('sourceUnit', $keys)) {
            $expression['sourceUnit']['id'] = marmot_encode($searchData->getSourceUnit()->getId());
            $expression['sourceUnit']['name'] = $searchData->getSourceUnit()->getName();
        }

        if (in_array('itemsData', $keys)) {
            $expression['itemsData'] = $this->getItemsDataTranslator()->objectToArray(
                $searchData->getItemsData()
            );
        }

        if (in_array('status', $keys)) {
            $expression['status']['id'] = marmot_encode($searchData->getStatus());
            $expression['status']['name'] = ISearchData::DATA_STATUS_CN[$searchData->getStatus()];
            $expression['status']['type'] = ISearchData::DATA_STATUS_TYPE[$searchData->getStatus()];
        }

        return $expression;
    }

    public function getSubjectCategoryToArray($subject):array
    {
        $data = [
            'id' => marmot_encode($subject),
            'name' => ISearchData::SUBJECT_CATEGORY_CN[$subject]
        ];
        
        return $data;
    }
}
