<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\ResourceCatalog\Model\ItemsData;
use Base\Sdk\ResourceCatalog\Model\NullItemsData;

class ItemsDataTranslator implements ITranslator
{
    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function arrayToObject(array $expression, $itemsData = null)
    {
        unset($itemsData);
        unset($expression);
        return new NullItemsData();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($itemsData, array $keys = array())
    {
        if (!$itemsData instanceof ItemsData) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'data',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($itemsData->getId());
        }
        if (in_array('data', $keys)) {
            $expression['data'] = $itemsData->getData();
        }

        return $expression;
    }
}
