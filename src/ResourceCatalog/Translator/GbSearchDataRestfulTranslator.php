<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\Template\Translator\GbTemplateRestfulTranslator;

use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Model\NullGbSearchData;
use Base\Sdk\ResourceCatalog\Translator\SearchDataRestfulTranslator;

class GbSearchDataRestfulTranslator extends SearchDataRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getGbTemplateRestfulTranslator(): GbTemplateRestfulTranslator
    {
        return new GbTemplateRestfulTranslator();
    }

    public function arrayToObject(array $expression, $gbSearchData = null)
    {
        return $this->translateToObject($expression, $gbSearchData);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $gbSearchData = null)
    {
        if (empty($expression)) {
            return new NullGbSearchData();
        }

        if ($gbSearchData == null) {
            $gbSearchData = new GbSearchData();
        }

        $gbSearchData = parent::translateToObject($expression, $gbSearchData);

        $data =  $expression['data'];

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        
        if (isset($relationships['template']['data'])) {
            $template = $this->changeArrayFormat($relationships['template']['data']);
            $gbSearchData->setTemplate($this->getGbTemplateRestfulTranslator()->arrayToObject($template));
        }

        return $gbSearchData;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($gbSearchData, array $keys = array())
    {
        unset($gbSearchData);
        unset($keys);
        return array();
    }
}
