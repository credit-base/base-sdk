<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\ResourceCatalog\Model\Task\Task;
use Base\Sdk\ResourceCatalog\Model\Task\NullTask;

use Base\Sdk\Crew\Translator\CrewRestfulTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

class TaskRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    protected function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    public function arrayToObject(array $expression, $task = null)
    {
        return $this->translateToObject($expression, $task);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $task = null)
    {
        if (empty($expression)) {
            return new NullTask();
        }

        if ($task == null) {
            $task = new Task();
        }
        
        $data =  $expression['data'];
        
        $id = $data['id'];
        $task->setId($id);
        
        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['pid'])) {
            $task->setPid($attributes['pid']);
        }
        if (isset($attributes['total'])) {
            $task->setTotal($attributes['total']);
        }
        if (isset($attributes['successNumber'])) {
            $task->setSuccessNumber($attributes['successNumber']);
        }
        if (isset($attributes['failureNumber'])) {
            $task->setFailureNumber($attributes['failureNumber']);
        }
        if (isset($attributes['sourceCategory'])) {
            $task->setSourceCategory($attributes['sourceCategory']);
        }
        if (isset($attributes['sourceTemplate'])) {
            $task->setSourceTemplate($attributes['sourceTemplate']);
        }
        if (isset($attributes['targetCategory'])) {
            $task->setTargetCategory($attributes['targetCategory']);
        }
        if (isset($attributes['targetTemplate'])) {
            $task->setTargetTemplate($attributes['targetTemplate']);
        }
        if (isset($attributes['targetRule'])) {
            $task->setTargetRule($attributes['targetRule']);
        }
        if (isset($attributes['fileName'])) {
            $task->setFileName($attributes['fileName']);
        }
        if (isset($attributes['createTime'])) {
            $task->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $task->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $task->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $task->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        
        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $task->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        if (isset($relationships['userGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['userGroup']['data']);
            $task->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }

        return $task;
    }

    public function objectToArray($task, array $keys = array())
    {
        unset($task);
        unset($keys);
        return array();
    }
}
