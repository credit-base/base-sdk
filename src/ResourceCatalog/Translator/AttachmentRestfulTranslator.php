<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\ResourceCatalog\Model\Attachment;
use Base\Sdk\ResourceCatalog\Model\NullAttachment;

class AttachmentRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $attachment = null)
    {
        return $this->translateToObject($expression, $attachment);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $attachment = null)
    {
        if (empty($expression)) {
            return new NullAttachment();
        }

        if ($attachment == null) {
            $attachment = new Attachment();
        }

        $data =  $expression['data'];
        
        $id = $data['id'];
        $attachment->setId($id);
        
        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $attachment->setName($attributes['name']);
        }
        if (isset($attributes['hash'])) {
            $attachment->setHash($attributes['hash']);
        }
        if (isset($attributes['createTime'])) {
            $attachment->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $attachment->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $attachment->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $attachment->setStatusTime($attributes['statusTime']);
        }
        return $attachment;
    }

    public function objectToArray($attachment, array $keys = array())
    {
        unset($attachment);
        unset($keys);
        return array();
    }
}
