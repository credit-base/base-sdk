<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Base\Sdk\ResourceCatalog\Model\WbjSearchData;
use Base\Sdk\ResourceCatalog\Model\NullWbjSearchData;
use Base\Sdk\ResourceCatalog\Translator\SearchDataTranslator;

use Base\Sdk\Template\Translator\WbjTemplateTranslator;

class WbjSearchDataTranslator extends SearchDataTranslator
{
    const STATUS_CN = [
        0 => '正常',
        -2 => '已屏蔽',
    ];

    const STATUS_TYPE = [
        0 => 'success',
        -2 => 'danger',
    ];

    protected function getWbjTemplateTranslator() : WbjTemplateTranslator
    {
        return new WbjTemplateTranslator();
    }

    public function arrayToObject(array $expression, $wbjSearchData = null)
    {
        unset($wbjSearchData);
        unset($expression);
        return new NullWbjSearchData();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($wbjSearchData, array $keys = array())
    {
        $searchData = parent::objectToArray($wbjSearchData, $keys);

        if (!$wbjSearchData instanceof WbjSearchData) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'template',
                'status',
            );
        }

        $expression = array();

        if (in_array('template', $keys)) {
            $expression['template'] = $this->getWbjTemplateTranslator()->objectToArray(
                $wbjSearchData->getTemplate()
            );
        }

        if (in_array('status', $keys)) {
            $expression['status']['id'] = marmot_encode($wbjSearchData->getStatus());
            $expression['status']['name'] = self::STATUS_CN[$wbjSearchData->getStatus()];
            $expression['status']['type'] = self::STATUS_TYPE[$wbjSearchData->getStatus()];
        }
        
        $expression = array_merge($searchData, $expression);

        return $expression;
    }
}
