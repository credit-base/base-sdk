<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\ResourceCatalog\Model\SearchData;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

abstract class SearchDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator() : CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    protected function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getItemsDataRestfulTranslator() : ItemsDataRestfulTranslator
    {
        return new ItemsDataRestfulTranslator();
    }

    public function arrayToObject(array $expression, $searchData = null)
    {
        return $this->translateToObject($expression, $searchData);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $searchData = null)
    {
        $data =  $expression['data'];
      
        $id = $data['id'];
        
        $searchData->setId($id);
      
        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['infoClassify'])) {
            $searchData->setInfoClassify($attributes['infoClassify']);
        }
        if (isset($attributes['infoCategory'])) {
            $searchData->setInfoCategory($attributes['infoCategory']);
        }
        if (isset($attributes['subjectCategory'])) {
            $searchData->setSubjectCategory($attributes['subjectCategory']);
        }
        if (isset($attributes['dimension'])) {
            $searchData->setDimension($attributes['dimension']);
        }
        if (isset($attributes['name'])) {
            $searchData->setName($attributes['name']);
        }
        if (isset($attributes['identify'])) {
            $searchData->setIdentify($attributes['identify']);
        }
        if (isset($attributes['expirationDate'])) {
            $searchData->setExpirationDate($attributes['expirationDate']);
        }
        if (isset($attributes['createTime'])) {
            $searchData->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $searchData->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $searchData->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $searchData->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        
        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $searchData->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        if (isset($relationships['sourceUnit']['data'])) {
            $sourceUnit = $this->changeArrayFormat($relationships['sourceUnit']['data']);
            $searchData->setSourceUnit($this->getUserGroupRestfulTranslator()->arrayToObject($sourceUnit));
        }

        if (isset($relationships['itemsData']['data'])) {
            $itemsData = $this->changeArrayFormat($relationships['itemsData']['data']);
            $searchData->setItemsData($this->getItemsDataRestfulTranslator()->arrayToObject($itemsData));
        }
        
        return $searchData;
    }

    public function objectToArray($searchData, array $keys = array())
    {
        unset($searchData);
        unset($keys);
        return array();
    }
}
