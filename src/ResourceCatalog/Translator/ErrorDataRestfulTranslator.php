<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\ResourceCatalog\Model\ErrorData;
use Base\Sdk\ResourceCatalog\Model\NullErrorData;

use Base\Sdk\Rule\Translator\TemplateTranslatorTrait;

class ErrorDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait, TemplateTranslatorTrait;

    protected function getTaskRestfulTranslator() : TaskRestfulTranslator
    {
        return new TaskRestfulTranslator();
    }

    public function arrayToObject(array $expression, $errorData = null)
    {
        return $this->translateToObject($expression, $errorData);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $errorData = null)
    {
        if (empty($expression)) {
            return new NullErrorData();
        }
        
        if ($errorData == null) {
            $errorData = new ErrorData();
        }
        
        $data =  $expression['data'];
        
        $id = $data['id'];
        $errorData->setId($id);
       
        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['category'])) {
            $errorData->setCategory($attributes['category']);
        }
        if (isset($attributes['itemsData'])) {
            $errorData->setItemsData($attributes['itemsData']);
        }
        if (isset($attributes['errorType'])) {
            $errorData->setErrorType($attributes['errorType']);
        }
        if (isset($attributes['errorReason'])) {
            $errorData->setErrorReason($attributes['errorReason']);
        }
        if (isset($attributes['createTime'])) {
            $errorData->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $errorData->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $errorData->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $errorData->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        
        if (isset($relationships['task']['data'])) {
            $task = $this->changeArrayFormat($relationships['task']['data']);
            $errorData->setTask($this->getTaskRestfulTranslator()->arrayToObject($task));
        }

        if (isset($relationships['template']['data'])) {
            $template = $this->changeArrayFormat($relationships['template']['data']);
            $category =
            isset($attributes['category']) ? $attributes['category'] : 0;

            $templateRestfulTranslator = $this->getTemplateRestfulTranslatorByCategory($category);
           
            $errorData->setTemplate($templateRestfulTranslator->arrayToObject($template));
        }
        
        return $errorData;
    }

    public function objectToArray($errorData, array $keys = array())
    {
        unset($errorData);
        unset($keys);
        return array();
    }
}
