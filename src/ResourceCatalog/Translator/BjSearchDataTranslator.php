<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use Base\Sdk\ResourceCatalog\Model\NullBjSearchData;
use Base\Sdk\ResourceCatalog\Translator\SearchDataTranslator;

use Base\Sdk\Template\Translator\BjTemplateTranslator;

class BjSearchDataTranslator extends SearchDataTranslator
{
    protected function getBjTemplateTranslator() : BjTemplateTranslator
    {
        return new BjTemplateTranslator();
    }

    public function arrayToObject(array $expression, $bjSearchData = null)
    {
        unset($bjSearchData);
        unset($expression);
        return new NullBjSearchData();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($bjSearchData, array $keys = array())
    {
        $searchData = parent::objectToArray($bjSearchData, $keys);
        
        if (!$bjSearchData instanceof BjSearchData) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'template'=>[],
            );
        }

        $expression = array();
       
        if (isset($keys['template'])) {
            $expression['template'] = $this->getBjTemplateTranslator()->objectToArray(
                $bjSearchData->getTemplate(),
                $keys['template']
            );
        }
      
        $expression = array_merge($searchData, $expression);

        return $expression;
    }
}
