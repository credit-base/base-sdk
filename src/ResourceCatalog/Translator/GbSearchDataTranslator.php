<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Model\NullGbSearchData;
use Base\Sdk\ResourceCatalog\Translator\SearchDataTranslator;

use Base\Sdk\Template\Translator\GbTemplateTranslator;

class GbSearchDataTranslator extends SearchDataTranslator
{
    protected function getGbTemplateTranslator() : GbTemplateTranslator
    {
        return new GbTemplateTranslator();
    }

    public function arrayToObject(array $expression, $gbSearchData = null)
    {
        unset($gbSearchData);
        unset($expression);
        return new NullGbSearchData();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($gbSearchData, array $keys = array())
    {
        $searchData = parent::objectToArray($gbSearchData, $keys);

        if (!$gbSearchData instanceof GbSearchData) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'template',
            );
        }

        $expression = array();

        if (in_array('template', $keys)) {
            $expression['template'] = $this->getGbTemplateTranslator()->objectToArray(
                $gbSearchData->getTemplate()
            );
        }
        
        $expression = array_merge($searchData, $expression);

        return $expression;
    }
}
