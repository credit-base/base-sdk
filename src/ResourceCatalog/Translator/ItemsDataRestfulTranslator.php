<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;
use Base\Sdk\ResourceCatalog\Model\ItemsData;
use Base\Sdk\ResourceCatalog\Model\NullItemsData;

class ItemsDataRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $itemsData = null)
    {
        return $this->translateToObject($expression, $itemsData);
    }

    protected function translateToObject(array $expression, $itemsData = null)
    {
        if (empty($expression)) {
            return new NullItemsData();
        }

        if ($itemsData == null) {
            $itemsData = new ItemsData();
        }

        $data =  $expression['data'];

        $id = $data['id'];
      
        $itemsData->setId($id);
     
        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['data'])) {
            $itemsData->setdata($attributes['data']);
        }
        
        return $itemsData;
    }

    public function objectToArray($itemsData, array $keys = array())
    {
        unset($itemsData);
        unset($keys);
        return array();
    }
}
