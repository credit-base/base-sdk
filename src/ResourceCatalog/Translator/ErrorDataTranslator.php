<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\ResourceCatalog\Model\ErrorData;
use Base\Sdk\ResourceCatalog\Model\NullErrorData;

use Base\Sdk\Rule\Translator\TemplateTranslatorTrait;

class ErrorDataTranslator implements ITranslator
{
    use TemplateTranslatorTrait;

    const STATUS_CN = [
        ErrorData::STATUS['NORMAL'] => '正常',
        ErrorData::STATUS['PROGRAM_EXCEPTION'] => '程序异常',
        ErrorData::STATUS['STORAGE_EXCEPTION'] => '入库异常',
    ];

    const STATUS_TYPE = [
        ErrorData::STATUS['NORMAL'] => 'success',
        ErrorData::STATUS['PROGRAM_EXCEPTION'] => 'danger',
        ErrorData::STATUS['STORAGE_EXCEPTION'] => 'danger',
    ];

    const ERROR_TYPE_CN = [
        ErrorData::ERROR_TYPE['MISSING_DATA'] => '数据缺失',
        ErrorData::ERROR_TYPE['COMPARISON_FAILED'] => '比对失败',
        ErrorData::ERROR_TYPE['DUPLICATION_DATA'] => '数据重复',
        ErrorData::ERROR_TYPE['FORMAT_VALIDATION_FAILED'] => '数据格式错误',
    ];

    protected function getTaskTranslator():TaskTranslator
    {
        return new TaskTranslator();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function arrayToObject(array $expression, $errorData = null)
    {
        unset($errorData);
        unset($expression);
        return new NullErrorData();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($errorData, array $keys = array())
    {
        if (!$errorData instanceof ErrorData) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'category',
                'template',
                'itemsData',
                'errorType',
                'errorReason',
                'taskData',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($errorData->getId());
        }
        if (in_array('category', $keys)) {
            $expression['category'] = $errorData->getCategory();
        }
        if (in_array('itemsData', $keys)) {
            $expression['itemsData'] = $errorData->getItemsData();
        }
        if (in_array('errorType', $keys)) {
            $expression['errorType'] = $errorData->getErrorType();
        }
        if (in_array('errorReason', $keys)) {
            $expression['errorReason'] = $errorData->getErrorReason();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $errorData->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $errorData->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $errorData->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $errorData->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $errorData->getStatusTime();
        }

        if (in_array('status', $keys)) {
            $expression['status']['id'] = marmot_encode($errorData->getStatus());
            $expression['status']['name'] = self::STATUS_CN[$errorData->getStatus()];
            $expression['status']['type'] = self::STATUS_TYPE[$errorData->getStatus()];
        }

        if (in_array('taskData', $keys)) {
            $expression['taskData'] = $this->getTaskTranslator()->objectToArray(
                $errorData->getTask()
            );
        }

        if (in_array('template', $keys)) {
            $templateTranslator = $this->getTemplateTranslatorByCategory($errorData->getCategory());
            $expression['template'] = $templateTranslator->objectToArray(
                $errorData->getTemplate()
            );
        }

        return $expression;
    }
}
