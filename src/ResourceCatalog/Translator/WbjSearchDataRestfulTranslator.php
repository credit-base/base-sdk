<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\Template\Translator\WbjTemplateRestfulTranslator;

use Base\Sdk\ResourceCatalog\Model\WbjSearchData;
use Base\Sdk\ResourceCatalog\Model\NullWbjSearchData;
use Base\Sdk\ResourceCatalog\Translator\SearchDataRestfulTranslator;

class WbjSearchDataRestfulTranslator extends SearchDataRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getWbjTemplateRestfulTranslator(): WbjTemplateRestfulTranslator
    {
        return new WbjTemplateRestfulTranslator();
    }

    public function arrayToObject(array $expression, $wbjSearchData = null)
    {
        return $this->translateToObject($expression, $wbjSearchData);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $wbjSearchData = null)
    {
        if (empty($expression)) {
            return new NullWbjSearchData();
        }

        if ($wbjSearchData == null) {
            $wbjSearchData = new WbjSearchData();
        }

        $wbjSearchData = parent::translateToObject($expression, $wbjSearchData);

        $data =  $expression['data'];

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['template']['data'])) {
            $template = $this->changeArrayFormat($relationships['template']['data']);
            $wbjSearchData->setTemplate($this->getWbjTemplateRestfulTranslator()->arrayToObject($template));
        }

        return $wbjSearchData;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($wbjSearchData, array $keys = array())
    {
        unset($wbjSearchData);
        unset($keys);
        return array();
    }
}
