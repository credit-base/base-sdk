<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\Template\Translator\BjTemplateRestfulTranslator;

use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use Base\Sdk\ResourceCatalog\Model\NullBjSearchData;
use Base\Sdk\ResourceCatalog\Translator\SearchDataRestfulTranslator;

class BjSearchDataRestfulTranslator extends SearchDataRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getBjTemplateRestfulTranslator(): BjTemplateRestfulTranslator
    {
        return new BjTemplateRestfulTranslator();
    }

    public function arrayToObject(array $expression, $bjSearchData = null)
    {
        return $this->translateToObject($expression, $bjSearchData);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $bjSearchData = null)
    {
        if (empty($expression)) {
            return new NullBjSearchData();
        }

        if ($bjSearchData == null) {
            $bjSearchData = new BjSearchData();
        }

        $bjSearchData = parent::translateToObject($expression, $bjSearchData);

        $data =  $expression['data'];

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        
        if (isset($relationships['template']['data'])) {
            $template = $this->changeArrayFormat($relationships['template']['data']);
            $bjSearchData->setTemplate($this->getBjTemplateRestfulTranslator()->arrayToObject($template));
        }
       
        return $bjSearchData;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($bjSearchData, array $keys = array())
    {
        unset($bjSearchData);
        unset($keys);
        return array();
    }
}
