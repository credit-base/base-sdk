<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Core;
use Marmot\Interfaces\ITranslator;

use Base\Sdk\ResourceCatalog\Model\Task\Task;
use Base\Sdk\ResourceCatalog\Model\Task\NullTask;

class TaskTranslator implements ITranslator
{
    const STATUS_CN = [
        Task::STATUS['PENDING'] => '进行中',
        Task::STATUS['SUCCESS'] => '成功',
        Task::STATUS['FAILURE'] => '失败',
        Task::STATUS['FAILURE_FILE_DOWNLOAD'] => '失败',
    ];

    const STATUS_TYPE = [
        Task::STATUS['PENDING'] => 'waring',
        Task::STATUS['SUCCESS'] => 'success',
        Task::STATUS['FAILURE'] => 'danger',
        Task::STATUS['FAILURE_FILE_DOWNLOAD'] => 'danger',
    ];

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function arrayToObject(array $expression, $task = null)
    {
        unset($task);
        unset($expression);
        return new NullTask();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($task, array $keys = array())
    {
        if (!$task instanceof Task) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'pid',
                'total',
                'successNumber',
                'failureNumber',
                'sourceCategory',
                'sourceTemplate',
                'targetCategory',
                'targetTemplate',
                'targetRule',
                'fileName',
                'crew',
                'userGroup',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($task->getId());
        }
        if (in_array('pid', $keys)) {
            $expression['pid'] = $task->getPid();
        }
        if (in_array('total', $keys)) {
            $expression['total'] = $task->getTotal();
        }
        if (in_array('successNumber', $keys)) {
            $expression['successNumber'] = $task->getSuccessNumber();
        }
        if (in_array('failureNumber', $keys)) {
            $expression['failureNumber'] = $task->getFailureNumber();
        }
        if (in_array('sourceCategory', $keys)) {
            $expression['sourceCategory'] = $task->getSourceCategory();
        }
        if (in_array('sourceTemplate', $keys)) {
            $expression['sourceTemplate'] = $task->getSourceTemplate();
        }
        if (in_array('targetCategory', $keys)) {
            $expression['targetCategory'] = $task->getTargetCategory();
        }
        if (in_array('targetTemplate', $keys)) {
            $expression['targetTemplate'] = $task->getTargetTemplate();
        }
        if (in_array('targetRule', $keys)) {
            $expression['targetRule'] = $task->getTargetRule();
        }
        if (in_array('fileName', $keys)) {
            $fileName = $task->getFileName();
            if (!empty($fileName)) {
                $fileName = $this->isFileExit($fileName);
            }
            $expression['fileName'] = $fileName;
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $task->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $task->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $task->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $task->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $task->getStatusTime();
        }

        if (in_array('crew', $keys)) {
            $expression['crew']['id'] = marmot_encode($task->getCrew()->getId());
            $expression['crew']['name'] = $task->getCrew()->getRealName();
        }

        if (in_array('userGroup', $keys)) {
            $expression['userGroup']['id'] = marmot_encode($task->getUserGroup()->getId());
            $expression['userGroup']['name'] = $task->getUserGroup()->getName();
        }

        if (in_array('status', $keys)) {
            $expression['status']['id'] = marmot_encode($task->getStatus());
            $expression['status']['name'] = self::STATUS_CN[$task->getStatus()];
            $expression['status']['type'] = self::STATUS_TYPE[$task->getStatus()];
        }

        return $expression;
    }

    public function isFileExit(string $fileName) : string
    {
        $newFileName = Core::$container->get('attachment.download.path').$fileName;
        if (!file_exists(Core::$container->get('attachment.storage.path').$fileName)) {
            $newFileName = "";
        }

        
        return $newFileName;
    }
}
