<?php
namespace Base\Sdk\Interaction\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\OperateAbleRepositoryTrait;
use Base\Sdk\Common\Repository\ModifyStatusAbleRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;

use Base\Sdk\Interaction\Adapter\Praise\PraiseRestfulAdapter;
use Base\Sdk\Interaction\Adapter\Interaction\IInteractionAdapter;
use Base\Sdk\Interaction\Model\Praise;

class PraiseRepository extends Repository implements IInteractionAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'PRAISE_LIST';
    const FETCH_ONE_MODEL_UN = 'PRAISE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new PraiseRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function accept(Praise $praise) : bool
    {
        return $this->getAdapter()->accept($praise);
    }

    public function publish(Praise $praise) : bool
    {
        return $this->getAdapter()->publish($praise);
    }

    public function unPublish(Praise $praise) : bool
    {
        return $this->getAdapter()->unPublish($praise);
    }
}
