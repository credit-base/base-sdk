<?php
namespace Base\Sdk\Interaction\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\ResubmitAbleRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\ApplyAbleRepositoryTrait;

use Base\Sdk\Interaction\Adapter\UnAuditQa\UnAuditQaRestfulAdapter;
use Base\Sdk\Interaction\Adapter\UnAuditInteraction\IUnAuditInteractionAdapter;

class UnAuditQaRepository extends Repository implements IUnAuditInteractionAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ApplyAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'UN_AUDIT_QA_LIST';
    const FETCH_ONE_MODEL_UN = 'UN_AUDIT_QA_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new UnAuditQaRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
