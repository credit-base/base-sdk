<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\CommandHandler\RevokeCommandHandler;

class RevokeFeedbackCommandHandler extends RevokeCommandHandler
{
    use FeedbackCommandHandlerTrait;
    
    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchFeedback($id);
    }
}
