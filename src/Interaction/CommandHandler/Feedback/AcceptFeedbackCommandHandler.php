<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Feedback\AcceptFeedbackCommand;

use Base\Sdk\Interaction\CommandHandler\ReplaySetItemsTrait;

class AcceptFeedbackCommandHandler implements ICommandHandler
{
    use FeedbackCommandHandlerTrait, ReplaySetItemsTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof AcceptFeedbackCommand)) {
            throw new \InvalidArgumentException;
        }

        $feedback = $this->fetchFeedback($command->id);

        $reply = $this->getReplyObject($feedback, $command->reply);

        $feedback->setReply($reply);
       
        return $feedback->accept();
    }
}
