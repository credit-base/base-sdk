<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Feedback\AddFeedbackCommand;

class AddFeedbackCommandHandler implements ICommandHandler
{
    use FeedbackCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof AddFeedbackCommand)) {
            throw new \InvalidArgumentException;
        }

        $feedback = $this->getFeedback();

        $feedback->setTitle($command->title);
        $feedback->setContent($command->content);

        $acceptUserGroup = $this->fetchUserGroup($command->acceptUserGroupId);
        $feedback->setAcceptUserGroup($acceptUserGroup);
      
        return $feedback->add();
    }
}
