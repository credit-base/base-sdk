<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\Interaction\Repository\FeedbackRepository;

use Base\Sdk\Interaction\CommandHandler\CommonCommandHandlerTrait;

trait FeedbackCommandHandlerTrait
{
    use CommonCommandHandlerTrait;
    
    private $repository;

    private $feedback;
    
    public function __construct()
    {
        $this->repository = new FeedbackRepository();
        $this->feedback = new Feedback();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->feedback);
    }

    protected function getRepository() : FeedbackRepository
    {
        return $this->repository;
    }
    
    protected function fetchFeedback(int $id) : Feedback
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getFeedback():Feedback
    {
        return $this->feedback;
    }
}
