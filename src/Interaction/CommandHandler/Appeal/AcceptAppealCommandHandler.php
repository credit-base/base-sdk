<?php
namespace Base\Sdk\Interaction\CommandHandler\Appeal;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Appeal\AcceptAppealCommand;

use Base\Sdk\Interaction\CommandHandler\ReplaySetItemsTrait;

class AcceptAppealCommandHandler implements ICommandHandler
{
    use AppealCommandHandlerTrait, ReplaySetItemsTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof AcceptAppealCommand)) {
            throw new \InvalidArgumentException;
        }

        $appeal = $this->fetchAppeal($command->id);
        
        $reply = $this->getReplyObject($appeal, $command->reply);
        
        $appeal->setReply($reply);
       
        return $appeal->accept();
    }
}
