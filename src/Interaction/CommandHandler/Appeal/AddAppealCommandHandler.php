<?php
namespace Base\Sdk\Interaction\CommandHandler\Appeal;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Appeal\AddAppealCommand;

class AddAppealCommandHandler implements ICommandHandler
{
    use AppealCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof AddAppealCommand)) {
            throw new \InvalidArgumentException;
        }

        $appeal = $this->getAppeal();

        $appeal->setTitle($command->title);
        $appeal->setContent($command->content);
        $appeal->setName($command->name);
        $appeal->setIdentify($command->identify);
        $appeal->setContact($command->contact);
        $appeal->setCertificates($command->certificates);
        $appeal->setImages($command->images);
        $appeal->setType($command->type);

        $acceptUserGroup = $this->fetchUserGroup($command->acceptUserGroupId);
        $appeal->setAcceptUserGroup($acceptUserGroup);
       
        return $appeal->add();
    }
}
