<?php
namespace Base\Sdk\Interaction\CommandHandler\Appeal;

use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\Interaction\Repository\AppealRepository;

use Base\Sdk\Interaction\CommandHandler\CommonCommandHandlerTrait;

trait AppealCommandHandlerTrait
{
    use CommonCommandHandlerTrait;
    
    private $appeal;

    private $repository;
    
    public function __construct()
    {
        $this->appeal = new Appeal();
        $this->repository = new AppealRepository();
    }

    public function __destruct()
    {
        unset($this->appeal);
        unset($this->repository);
    }

    protected function getRepository() : AppealRepository
    {
        return $this->repository;
    }
    
    protected function fetchAppeal(int $id) : Appeal
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getAppeal():Appeal
    {
        return $this->appeal;
    }
}
