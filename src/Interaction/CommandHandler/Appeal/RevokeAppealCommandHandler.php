<?php
namespace Base\Sdk\Interaction\CommandHandler\Appeal;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\CommandHandler\RevokeCommandHandler;

class RevokeAppealCommandHandler extends RevokeCommandHandler
{
    use AppealCommandHandlerTrait;
    
    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchAppeal($id);
    }
}
