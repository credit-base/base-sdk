<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditAppeal;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\RejectCommandHandler;

class RejectUnAuditAppealCommandHandler extends RejectCommandHandler
{
    use UnAuditAppealCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditAppeal($id);
    }
}
