<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditAppeal;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\UnAuditAppeal\ResubmitUnAuditAppealCommand;

use Base\Sdk\Interaction\CommandHandler\ReplaySetItemsTrait;

class ResubmitUnAuditAppealCommandHandler implements ICommandHandler
{
    use UnAuditAppealCommandHandlerTrait, ReplaySetItemsTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitUnAuditAppealCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditAppeal = $this->fetchUnAuditAppeal($command->id);

        $reply = $this->getReplyObject($unAuditAppeal, $command->reply);

        $unAuditAppeal->setReply($reply);
       
        return $unAuditAppeal->resubmit();
    }
}
