<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditAppeal;

use Base\Sdk\Interaction\Model\UnAuditAppeal;
use Base\Sdk\Interaction\Repository\UnAuditAppealRepository;

trait UnAuditAppealCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new UnAuditAppealRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : UnAuditAppealRepository
    {
        return $this->repository;
    }
    
    protected function fetchUnAuditAppeal(int $id) : UnAuditAppeal
    {
        return $this->getRepository()->fetchOne($id);
    }
}
