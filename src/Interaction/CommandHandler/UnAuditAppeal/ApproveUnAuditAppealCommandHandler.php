<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditAppeal;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\ApproveCommandHandler;

class ApproveUnAuditAppealCommandHandler extends ApproveCommandHandler
{
    use UnAuditAppealCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditAppeal($id);
    }
}
