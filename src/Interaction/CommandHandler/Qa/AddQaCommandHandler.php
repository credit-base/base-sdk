<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Qa\AddQaCommand;

class AddQaCommandHandler implements ICommandHandler
{
    use QaCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof AddQaCommand)) {
            throw new \InvalidArgumentException;
        }

        $qaObject = $this->getQa();

        $qaObject->setTitle($command->title);
        $qaObject->setContent($command->content);

        $acceptUserGroup = $this->fetchUserGroup($command->acceptUserGroupId);
        $qaObject->setAcceptUserGroup($acceptUserGroup);
       
        return $qaObject->add();
    }
}
