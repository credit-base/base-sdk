<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\CommandHandler\RevokeCommandHandler;

class RevokeQaCommandHandler extends RevokeCommandHandler
{
    use QaCommandHandlerTrait;
    
    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchQa($id);
    }
}
