<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Repository\QaRepository;

use Base\Sdk\Interaction\CommandHandler\CommonCommandHandlerTrait;

trait QaCommandHandlerTrait
{
    use CommonCommandHandlerTrait;
    
    private $qaObjectObject;

    private $repository;
    
    public function __construct()
    {
        $this->qaObject = new Qa();
        $this->repository = new QaRepository();
    }

    public function __destruct()
    {
        unset($this->qaObject);
        unset($this->repository);
    }

    protected function getRepository() : QaRepository
    {
        return $this->repository;
    }
    
    protected function fetchQa(int $id) : Qa
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getQa():Qa
    {
        return $this->qaObject;
    }
}
