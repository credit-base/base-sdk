<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Qa\UnPublishQaCommand;

class UnPublishQaCommandHandler implements ICommandHandler
{
    use QaCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof UnPublishQaCommand)) {
            throw new \InvalidArgumentException;
        }

        $praise = $this->fetchQa($command->id);
       
        return $praise->unPublish();
    }
}
