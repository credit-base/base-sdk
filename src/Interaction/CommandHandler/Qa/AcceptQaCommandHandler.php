<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Qa\AcceptQaCommand;

use Base\Sdk\Interaction\CommandHandler\ReplaySetItemsTrait;

class AcceptQaCommandHandler implements ICommandHandler
{
    use QaCommandHandlerTrait, ReplaySetItemsTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof AcceptQaCommand)) {
            throw new \InvalidArgumentException;
        }

        $complaint = $this->fetchQa($command->id);

        $reply = $this->getReplyObject($complaint, $command->reply);

        $complaint->setReply($reply);
       
        return $complaint->accept();
    }
}
