<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Qa\PublishQaCommand;

class PublishQaCommandHandler implements ICommandHandler
{
    use QaCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof PublishQaCommand)) {
            throw new \InvalidArgumentException;
        }

        $praise = $this->fetchQa($command->id);
       
        return $praise->publish();
    }
}
