<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditFeedback;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\RejectCommandHandler;

class RejectUnAuditFeedbackCommandHandler extends RejectCommandHandler
{
    use UnAuditFeedbackCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditFeedback($id);
    }
}
