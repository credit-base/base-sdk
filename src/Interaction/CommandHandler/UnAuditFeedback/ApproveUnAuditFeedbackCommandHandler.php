<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditFeedback;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\ApproveCommandHandler;

class ApproveUnAuditFeedbackCommandHandler extends ApproveCommandHandler
{
    use UnAuditFeedbackCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditFeedback($id);
    }
}
