<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditFeedback;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\UnAuditFeedback\ResubmitUnAuditFeedbackCommand;

use Base\Sdk\Interaction\CommandHandler\ReplaySetItemsTrait;

class ResubmitUnAuditFeedbackCommandHandler implements ICommandHandler
{
    use UnAuditFeedbackCommandHandlerTrait, ReplaySetItemsTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitUnAuditFeedbackCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditFeedback = $this->fetchUnAuditFeedback($command->id);

        $reply = $this->getReplyObject($unAuditFeedback, $command->reply);

        $unAuditFeedback->setReply($reply);
       
        return $unAuditFeedback->resubmit();
    }
}
