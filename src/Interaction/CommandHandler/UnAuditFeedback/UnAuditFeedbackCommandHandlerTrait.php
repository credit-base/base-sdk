<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditFeedback;

use Base\Sdk\Interaction\Model\UnAuditFeedback;
use Base\Sdk\Interaction\Repository\UnAuditFeedbackRepository;

trait UnAuditFeedbackCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new UnAuditFeedbackRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : UnAuditFeedbackRepository
    {
        return $this->repository;
    }
    
    protected function fetchUnAuditFeedback(int $id) : UnAuditFeedback
    {
        return $this->getRepository()->fetchOne($id);
    }
}
