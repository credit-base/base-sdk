<?php
namespace Base\Sdk\Interaction\CommandHandler\Complaint;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Complaint\AddComplaintCommand;

class AddComplaintCommandHandler implements ICommandHandler
{
    use ComplaintCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof AddComplaintCommand)) {
            throw new \InvalidArgumentException;
        }

        $complaint = $this->getComplaint();

        $complaint->setTitle($command->title);
        $complaint->setContent($command->content);
        $complaint->setName($command->name);
        $complaint->setIdentify($command->identify);
        $complaint->setContact($command->contact);
        $complaint->setSubject($command->subject);
        $complaint->setImages($command->images);
        $complaint->setType($command->type);

        $acceptUserGroup = $this->fetchUserGroup($command->acceptUserGroupId);
        $complaint->setAcceptUserGroup($acceptUserGroup);
       
        return $complaint->add();
    }
}
