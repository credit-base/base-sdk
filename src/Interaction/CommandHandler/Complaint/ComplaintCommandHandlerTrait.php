<?php
namespace Base\Sdk\Interaction\CommandHandler\Complaint;

use Base\Sdk\Interaction\Model\Complaint;
use Base\Sdk\Interaction\Repository\ComplaintRepository;

use Base\Sdk\Interaction\CommandHandler\CommonCommandHandlerTrait;

trait ComplaintCommandHandlerTrait
{
    use CommonCommandHandlerTrait;
    
    private $complaint;

    private $repository;
    
    public function __construct()
    {
        $this->complaint =new Complaint();
        $this->repository = new ComplaintRepository();
    }

    public function __destruct()
    {
        unset($this->complaint);
        unset($this->repository);
    }

    protected function getRepository() : ComplaintRepository
    {
        return $this->repository;
    }
    
    protected function fetchComplaint(int $id) : Complaint
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getComplaint():Complaint
    {
        return $this->complaint;
    }
}
