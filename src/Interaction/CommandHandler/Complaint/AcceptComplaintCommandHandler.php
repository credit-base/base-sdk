<?php
namespace Base\Sdk\Interaction\CommandHandler\Complaint;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Complaint\AcceptComplaintCommand;

use Base\Sdk\Interaction\CommandHandler\ReplaySetItemsTrait;

class AcceptComplaintCommandHandler implements ICommandHandler
{
    use ComplaintCommandHandlerTrait, ReplaySetItemsTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof AcceptComplaintCommand)) {
            throw new \InvalidArgumentException;
        }

        $complaint = $this->fetchComplaint($command->id);

        $reply = $this->getReplyObject($complaint, $command->reply);

        $complaint->setReply($reply);
       
        return $complaint->accept();
    }
}
