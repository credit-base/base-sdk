<?php
namespace Base\Sdk\Interaction\CommandHandler\Complaint;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\CommandHandler\RevokeCommandHandler;

class RevokeComplaintCommandHandler extends RevokeCommandHandler
{
    use ComplaintCommandHandlerTrait;
    
    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchComplaint($id);
    }
}
