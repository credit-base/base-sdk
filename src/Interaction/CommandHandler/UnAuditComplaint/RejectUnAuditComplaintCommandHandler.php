<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditComplaint;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\RejectCommandHandler;

class RejectUnAuditComplaintCommandHandler extends RejectCommandHandler
{
    use UnAuditComplaintCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditComplaint($id);
    }
}
