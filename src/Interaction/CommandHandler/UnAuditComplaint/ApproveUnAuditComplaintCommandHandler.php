<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditComplaint;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\ApproveCommandHandler;

class ApproveUnAuditComplaintCommandHandler extends ApproveCommandHandler
{
    use UnAuditComplaintCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditComplaint($id);
    }
}
