<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditComplaint;

use Base\Sdk\Interaction\Model\UnAuditComplaint;
use Base\Sdk\Interaction\Repository\UnAuditComplaintRepository;

trait UnAuditComplaintCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new UnAuditComplaintRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : UnAuditComplaintRepository
    {
        return $this->repository;
    }
    
    protected function fetchUnAuditComplaint(int $id) : UnAuditComplaint
    {
        return $this->getRepository()->fetchOne($id);
    }
}
