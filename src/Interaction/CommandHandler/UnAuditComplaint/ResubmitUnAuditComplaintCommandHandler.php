<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditComplaint;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\UnAuditComplaint\ResubmitUnAuditComplaintCommand;

use Base\Sdk\Interaction\CommandHandler\ReplaySetItemsTrait;

class ResubmitUnAuditComplaintCommandHandler implements ICommandHandler
{
    use UnAuditComplaintCommandHandlerTrait, ReplaySetItemsTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitUnAuditComplaintCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditComplaint = $this->fetchUnAuditComplaint($command->id);

        $reply = $this->getReplyObject($unAuditComplaint, $command->reply);

        $unAuditComplaint->setReply($reply);
       
        return $unAuditComplaint->resubmit();
    }
}
