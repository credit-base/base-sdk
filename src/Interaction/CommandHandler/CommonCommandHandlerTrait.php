<?php
namespace Base\Sdk\Interaction\CommandHandler;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;

use Marmot\Interfaces\ICommand;

trait CommonCommandHandlerTrait
{
    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return new UserGroupRepository();
    }

    protected function fetchUserGroup(int $id) : UserGroup
    {
        return $this->getUserGroupRepository()->fetchOne($id);
    }
}
