<?php
namespace Base\Sdk\Interaction\CommandHandler;

trait ReplaySetItemsTrait
{
    protected function getReplyObject($interaction, array $reply)
    {
        $replyObject = $interaction->getReply();
        $replyObject->setContent($reply['content']);
        $replyObject->setImages($reply['images']);
        $replyObject->setAdmissibility($reply['admissibility']);

        return $replyObject;
    }
}
