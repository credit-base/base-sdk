<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Praise\AddPraiseCommand;

class AddPraiseCommandHandler implements ICommandHandler
{
    use PraiseCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof AddPraiseCommand)) {
            throw new \InvalidArgumentException;
        }

        $complaint = $this->getPraise();

        $complaint->setTitle($command->title);
        $complaint->setContent($command->content);
        $complaint->setName($command->name);
        $complaint->setIdentify($command->identify);
        $complaint->setContact($command->contact);
        $complaint->setSubject($command->subject);
        $complaint->setImages($command->images);
        $complaint->setType($command->type);
        
        $acceptUserGroup = $this->fetchUserGroup($command->acceptUserGroupId);
        
        $complaint->setAcceptUserGroup($acceptUserGroup);
       
        return $complaint->add();
    }
}
