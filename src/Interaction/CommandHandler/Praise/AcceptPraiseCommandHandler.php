<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Praise\AcceptPraiseCommand;

use Base\Sdk\Interaction\CommandHandler\ReplaySetItemsTrait;

class AcceptPraiseCommandHandler implements ICommandHandler
{
    use PraiseCommandHandlerTrait, ReplaySetItemsTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof AcceptPraiseCommand)) {
            throw new \InvalidArgumentException;
        }

        $praise = $this->fetchPraise($command->id);

        $reply = $this->getReplyObject($praise, $command->reply);

        $praise->setReply($reply);
       
        return $praise->accept();
    }
}
