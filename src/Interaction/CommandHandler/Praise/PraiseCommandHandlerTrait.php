<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Repository\PraiseRepository;

use Base\Sdk\Interaction\CommandHandler\CommonCommandHandlerTrait;

trait PraiseCommandHandlerTrait
{
    use CommonCommandHandlerTrait;
    
    private $praise;

    private $repository;
    
    public function __construct()
    {
        $this->praise = new Praise();
        $this->repository = new PraiseRepository();
    }

    public function __destruct()
    {
        unset($this->praise);
        unset($this->repository);
    }

    protected function getRepository() : PraiseRepository
    {
        return $this->repository;
    }
    
    protected function fetchPraise(int $id) : Praise
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getPraise():Praise
    {
        return $this->praise;
    }
}
