<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\CommandHandler\RevokeCommandHandler;

class RevokePraiseCommandHandler extends RevokeCommandHandler
{
    use PraiseCommandHandlerTrait;
    
    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchPraise($id);
    }
}
