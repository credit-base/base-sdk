<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Praise\UnPublishPraiseCommand;

class UnPublishPraiseCommandHandler implements ICommandHandler
{
    use PraiseCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof UnPublishPraiseCommand)) {
            throw new \InvalidArgumentException;
        }

        $praise = $this->fetchPraise($command->id);
       
        return $praise->unPublish();
    }
}
