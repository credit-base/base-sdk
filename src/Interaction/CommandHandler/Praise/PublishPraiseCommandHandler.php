<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\Praise\PublishPraiseCommand;

class PublishPraiseCommandHandler implements ICommandHandler
{
    use PraiseCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof PublishPraiseCommand)) {
            throw new \InvalidArgumentException;
        }

        $praise = $this->fetchPraise($command->id);
       
        return $praise->publish();
    }
}
