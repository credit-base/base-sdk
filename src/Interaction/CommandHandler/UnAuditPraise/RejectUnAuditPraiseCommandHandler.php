<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditPraise;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\RejectCommandHandler;

class RejectUnAuditPraiseCommandHandler extends RejectCommandHandler
{
    use UnAuditPraiseCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditPraise($id);
    }
}
