<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditPraise;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\UnAuditPraise\ResubmitUnAuditPraiseCommand;

use Base\Sdk\Interaction\CommandHandler\ReplaySetItemsTrait;

class ResubmitUnAuditPraiseCommandHandler implements ICommandHandler
{
    use UnAuditPraiseCommandHandlerTrait, ReplaySetItemsTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitUnAuditPraiseCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditPraise = $this->fetchUnAuditPraise($command->id);

        $reply = $this->getReplyObject($unAuditPraise, $command->reply);

        $unAuditPraise->setReply($reply);
       
        return $unAuditPraise->resubmit();
    }
}
