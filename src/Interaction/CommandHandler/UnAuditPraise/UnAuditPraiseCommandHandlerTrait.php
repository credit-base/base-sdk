<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditPraise;

use Base\Sdk\Interaction\Model\UnAuditPraise;
use Base\Sdk\Interaction\Repository\UnAuditPraiseRepository;

trait UnAuditPraiseCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new UnAuditPraiseRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : UnAuditPraiseRepository
    {
        return $this->repository;
    }
    
    protected function fetchUnAuditPraise(int $id) : UnAuditPraise
    {
        return $this->getRepository()->fetchOne($id);
    }
}
