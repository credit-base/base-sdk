<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditPraise;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\ApproveCommandHandler;

class ApproveUnAuditPraiseCommandHandler extends ApproveCommandHandler
{
    use UnAuditPraiseCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditPraise($id);
    }
}
