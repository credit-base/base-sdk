<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditQa;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Interaction\Command\UnAuditQa\ResubmitUnAuditQaCommand;

use Base\Sdk\Interaction\CommandHandler\ReplaySetItemsTrait;

class ResubmitUnAuditQaCommandHandler implements ICommandHandler
{
    use UnAuditQaCommandHandlerTrait, ReplaySetItemsTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitUnAuditQaCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditQa = $this->fetchUnAuditQa($command->id);

        $reply = $this->getReplyObject($unAuditQa, $command->reply);

        $unAuditQa->setReply($reply);
       
        return $unAuditQa->resubmit();
    }
}
