<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditQa;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\RejectCommandHandler;

class RejectUnAuditQaCommandHandler extends RejectCommandHandler
{
    use UnAuditQaCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditQa($id);
    }
}
