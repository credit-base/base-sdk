<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditQa;

use Base\Sdk\Interaction\Model\UnAuditQa;
use Base\Sdk\Interaction\Repository\UnAuditQaRepository;

trait UnAuditQaCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new UnAuditQaRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : UnAuditQaRepository
    {
        return $this->repository;
    }
    
    protected function fetchUnAuditQa(int $id) : UnAuditQa
    {
        return $this->getRepository()->fetchOne($id);
    }
}
