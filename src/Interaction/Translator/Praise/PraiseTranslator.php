<?php
namespace Base\Sdk\Interaction\Translator\Praise;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Model\NullPraise;

use Base\Sdk\Interaction\Translator\Interaction\InteractionTranslator;

class PraiseTranslator extends InteractionTranslator
{
    public function arrayToObject(array $expression, $praise = null)
    {
        unset($praise);
        unset($expression);
        return new NullPraise();
    }

    public function objectToArray($praise, array $keys = array())
    {
        $interaction = parent::objectToArray($praise, $keys);

        if (!$praise instanceof Praise) {
            return array();
        }

        $expression = array();
        
        $expression = array_merge($interaction, $expression);
        
        return $expression;
    }
}
