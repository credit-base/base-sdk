<?php
namespace Base\Sdk\Interaction\Translator\Praise;

use Base\Sdk\Interaction\Model\UnAuditPraise;
use Base\Sdk\Interaction\Model\NullUnAuditPraise;

use Base\Sdk\Interaction\Translator\Interaction\UnAuditInteractionRestfulTranslator;

class UnAuditPraiseRestfulTranslator extends UnAuditInteractionRestfulTranslator
{
    protected function translateToObject(array $expression, $unAuditPraise = null)
    {
        if (empty($expression)) {
            return new NullUnAuditPraise();
        }

        if ($unAuditPraise == null) {
            $unAuditPraise = new UnAuditPraise();
        }

        $unAuditPraise = parent::translateToObject($expression, $unAuditPraise);

        $data = $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['praiseType'])) {
            $unAuditPraise->setType($attributes['praiseType']);
        }

        return $unAuditPraise;
    }

    public function objectToArray($unAuditPraise, array $keys = array())
    {
        $unAuditInteraction = parent::objectToArray($unAuditPraise, $keys);

        if (!$unAuditPraise instanceof UnAuditPraise) {
            return array();
        }

        $expression['data'] = $unAuditInteraction['data'];
        $expression['data']['type'] = 'unAuditedPraises';
        
        return $expression;
    }
}
