<?php
namespace Base\Sdk\Interaction\Translator\Praise;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Model\NullPraise;

use Base\Sdk\Interaction\Translator\Interaction\InteractionRestfulTranslator;

class PraiseRestfulTranslator extends InteractionRestfulTranslator
{
    protected function translateToObject(array $expression, $praise = null)
    {
        if (empty($expression)) {
            return new NullPraise();
        }

        if ($praise == null) {
            $praise = new Praise();
        }

        $praise = parent::translateToObject($expression, $praise);

        $data = $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['praiseType'])) {
            $praise->setType($attributes['praiseType']);
        }
        
        return $praise;
    }

    public function objectToArray($praise, array $keys = array())
    {
        $interaction = parent::objectToArray($praise, $keys);

        if (!$praise instanceof Praise) {
            return array();
        }

        $expression['data'] = $interaction['data'];
        $expression['data']['type'] = 'praises';
        
        return $expression;
    }
}
