<?php
namespace Base\Sdk\Interaction\Translator\Praise;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Interaction\Model\UnAuditPraise;
use Base\Sdk\Interaction\Model\NullUnAuditPraise;

use Base\Sdk\Interaction\Translator\Interaction\UnAuditInteractionTranslator;

class UnAuditPraiseTranslator extends UnAuditInteractionTranslator
{
    public function arrayToObject(array $expression, $unAuditPraise = null)
    {
        unset($unAuditPraise);
        unset($expression);
        return new NullUnAuditPraise();
    }

    public function objectToArray($unAuditPraise, array $keys = array())
    {
        $unAuditInteraction = parent::objectToArray($unAuditPraise, $keys);

        if (!$unAuditPraise instanceof UnAuditPraise) {
            return array();
        }

        $expression = array();

        $expression = array_merge($unAuditInteraction, $expression);

        return $expression;
    }
}
