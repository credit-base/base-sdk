<?php
namespace Base\Sdk\Interaction\Translator\Reply;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Model\NullReply;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

class ReplyRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $reply = null)
    {
        return $this->translateToObject($expression, $reply);
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $reply = null)
    {
        if (empty($expression)) {
            return new NullReply();
        }

        if ($reply == null) {
            $reply = new Reply();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $reply->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['content'])) {
            $reply->setContent($attributes['content']);
        }

        if (isset($attributes['images'])) {
            $reply->setImages($attributes['images']);
        }

        if (isset($attributes['admissibility'])) {
            $reply->setAdmissibility($attributes['admissibility']);
        }

        if (isset($attributes['createTime'])) {
            $reply->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['statusTime'])) {
            $reply->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['updateTime'])) {
            $reply->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['status'])) {
            $reply->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['crew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);

            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;

            $reply->setCrew($crew);
        }
        
        return $reply;
    }

    public function objectToArray($reply, array $keys = array())
    {
        if (!$reply instanceof Reply) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'content',
                'images',
                'admissibility',
                'crew'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'replies'
            )
        );

        $attributes = array();

        if (in_array('content', $keys)) {
            $attributes['content'] = $reply->getContent();
        }
        if (in_array('admissibility', $keys)) {
            $attributes['admissibility'] = $reply->getAdmissibility();
        }
        if (in_array('images', $keys)) {
            $attributes['images'] = $reply->getImages();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $reply->getCrew()->getId()
                )
            );
        }
        
        return $expression;
    }
}
