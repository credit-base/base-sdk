<?php
namespace Base\Sdk\Interaction\Translator\Reply;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Model\NullReply;

use Base\Sdk\Crew\Translator\CrewTranslator;

class ReplyTranslator implements ITranslator
{
    const ADMISSIBILITY_CN = array(
        Reply::ADMISSIBILITY['ADMISSIBLE'] => '予以受理',
        Reply::ADMISSIBILITY['INADMISSIBLE'] => '不予受理',
    );

    const ADMISSIBILITY_TYPE = array(
        Reply::ADMISSIBILITY['ADMISSIBLE'] => 'success',
        Reply::ADMISSIBILITY['INADMISSIBLE'] => 'danger',
    );

    public function arrayToObject(array $expression, $reply = null)
    {
        unset($expression);
        unset($reply);
        return new NullReply();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($reply, array $keys = array())
    {
        if (!$reply instanceof Reply) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'content',
                'images',
                'admissibility',
                'status',
                'crew' => [],
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($reply->getId());
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $reply->getContent();
        }
        if (in_array('images', $keys)) {
            $expression['images'] = $reply->getImages();
        }
        if (in_array('admissibility', $keys)) {
            $expression['admissibility'] = [
                'id' => marmot_encode($reply->getAdmissibility()),
                'name'=> self::ADMISSIBILITY_CN[$reply->getAdmissibility()],
                'type'=> self::ADMISSIBILITY_TYPE[$reply->getAdmissibility()],
            ];
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($reply->getStatus()),
                'name' => IApplyCategory::STATUS_CN[$reply->getStatus()],
                'type' => IApplyCategory::STATUS_TAG_TYPE[$reply->getStatus()]
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $reply->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $reply->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $reply->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $reply->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $reply->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $reply->getCreateTime());
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $reply->getCrew(),
                $keys['crew']
            );
        }

        return $expression;
    }
}
