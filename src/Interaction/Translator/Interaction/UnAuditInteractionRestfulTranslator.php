<?php
namespace Base\Sdk\Interaction\Translator\Interaction;

use Marmot\Core;

use Base\Sdk\Interaction\Model\UnAuditInteraction;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

abstract class UnAuditInteractionRestfulTranslator extends InteractionRestfulTranslator
{
    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $unAuditInteraction = null)
    {
        return $this->translateToObject($expression, $unAuditInteraction);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $unAuditInteraction = null)
    {
        $unAuditInteraction = parent::translateToObject($expression, $unAuditInteraction);

        $data =  $expression['data'];
        
        $id = $data['id'];
       
        $unAuditInteraction->setId($id);
      
        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['applyStatus'])) {
            $unAuditInteraction->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($attributes['rejectReason'])) {
            $unAuditInteraction->setRejectReason($attributes['rejectReason']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['applyCrew']['data'])) {
            $applyCrewFormat = $this->changeArrayFormat($relationships['applyCrew']['data']);
            $applyCrewFormat = $this->getCrewRestfulTranslator()->arrayToObject($applyCrewFormat);

            $applyCrew = empty($relationships['applyCrew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $applyCrewFormat;

            $unAuditInteraction->setApplyCrew($applyCrew);
        }

        if (isset($relationships['applyUserGroup']['data'])) {
            $applyUserGroup = $this->changeArrayFormat($relationships['applyUserGroup']['data']);
            $unAuditInteraction->setApplyUserGroup(
                $this->getUserGroupRestfulTranslator()->arrayToObject($applyUserGroup)
            );
        }

        if (isset($relationships['relation']['data'])) {
            $relation = $this->changeArrayFormat($relationships['relation']['data']);
            $unAuditInteraction->setMember($this->getMemberRestfulTranslator()->arrayToObject($relation));
        }

        return $unAuditInteraction;
    }

    public function objectToArray($unAuditInteraction, array $keys = array())
    {
        $interaction = parent::objectToArray($unAuditInteraction, $keys);

        $expression = array();

        if (!$unAuditInteraction instanceof UnAuditInteraction) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'applyCrew',
                'rejectReason',
            );
        }

        $expression['data']['attributes'] = $interaction['data']['attributes'];
        $attributes = array();

        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $unAuditInteraction->getRejectReason();
        }

        $expression['data']['attributes'] = array_merge($attributes, $expression['data']['attributes']);

        $relationships  = array();
        if (in_array('applyCrew', $keys)) {
            $relationships['applyCrew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $unAuditInteraction->getApplyCrew()->getId()
                )
            );
        }
        $interactionRelationships =
            isset($interaction['data']['relationships']) ? $interaction['data']['relationships'] : array();
    
        $expression['data']['relationships'] = array_merge(
            $interactionRelationships,
            $relationships
        );
   
        return $expression;
    }
}
