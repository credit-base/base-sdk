<?php
namespace Base\Sdk\Interaction\Translator\Interaction;

use Base\Sdk\Interaction\Model\UnAuditInteraction;

use Base\Sdk\Common\Model\IApplyCategory;
use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Crew\Translator\CrewTranslator;

abstract class UnAuditInteractionTranslator extends InteractionTranslator
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function objectToArray($unAuditInteraction, array $keys = array())
    {
        $interaction = parent::objectToArray($unAuditInteraction, $keys);

        if (!$unAuditInteraction instanceof UnAuditInteraction) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'applyStatus',
                'rejectReason',
                'applyCrew'=>[],
                'applyUserGroup'=>[],
                'acceptStatus',
            );
        }

        $expression = array();

        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = [
                'id' => marmot_encode($unAuditInteraction->getApplyStatus()),
                'name'=> IApplyCategory::APPLY_STATUS_CN[$unAuditInteraction->getApplyStatus()],
                'type'=> IApplyCategory::APPLY_STATUS_TAG_TYPE[$unAuditInteraction->getApplyStatus()],
            ];
        }

        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $unAuditInteraction->getRejectReason();
        }

        if (isset($keys['applyUserGroup'])) {
            $expression['applyUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $unAuditInteraction->getApplyUserGroup(),
                $keys['applyUserGroup']
            );
        }
        if (isset($keys['applyCrew'])) {
            $expression['applyCrew'] = $this->getCrewTranslator()->objectToArray(
                $unAuditInteraction->getApplyCrew(),
                $keys['applyCrew']
            );
        }
        if (in_array('acceptStatus', $keys)) {
            $acceptStatus = $this->getAcceptStatusFormat(
                $unAuditInteraction->getApplyStatus()
            );

            $expression['acceptStatus'] = [
                'id' => marmot_encode($acceptStatus),
                'name'=> self::ACCEPT_STATUS_CN[$acceptStatus],
                'type'=> self::ACCEPT_STATUS_TYPE[$acceptStatus],
            ];
        }

        $expression = array_merge($interaction, $expression);

        return $expression;
    }

    protected function getAcceptStatusFormat(int $applyStatus)
    {
        $acceptStatus = UnAuditInteraction::ACCEPT_STATUS['PENDING'];
        $applyArray = [IApplyAble::APPLY_STATUS['PENDING'],IApplyAble::APPLY_STATUS['REJECT']];
        if (in_array($applyStatus, $applyArray)) {
            $acceptStatus = UnAuditInteraction::ACCEPT_STATUS['ACCEPTING'];
        }

        if ($applyStatus == IApplyAble::APPLY_STATUS['APPROVE']) {
            $acceptStatus = UnAuditInteraction::ACCEPT_STATUS['COMPLETE'];
        }

        return $acceptStatus;
    }
}
