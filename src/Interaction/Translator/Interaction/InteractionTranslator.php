<?php
namespace Base\Sdk\Interaction\Translator\Interaction;

use Marmot\Interfaces\ITranslator;
use Base\Sdk\Common\Utils\Mask;

use Base\Sdk\Interaction\Model\Interaction;

use Base\Sdk\Member\Translator\MemberTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;

use Base\Sdk\Interaction\Translator\Reply\ReplyTranslator;

abstract class InteractionTranslator implements ITranslator
{
    const STATUS_CN = array(
        Interaction::STATUS['NORMAL'] => '正常',
        Interaction::STATUS['REVOKED'] => '已撤销',
        Interaction::STATUS['PUBLISH'] => '正常',
    );

    const STATUS_TYPE = array(
        Interaction::STATUS['NORMAL'] => 'success',
        Interaction::STATUS['REVOKED'] => 'danger',
        Interaction::STATUS['PUBLISH'] => 'info'
    );

    const ACCEPT_STATUS_CN = array(
        Interaction::ACCEPT_STATUS['PENDING'] => '待受理',
        Interaction::ACCEPT_STATUS['ACCEPTING'] => '受理中',
        Interaction::ACCEPT_STATUS['COMPLETE'] => '受理完成',
    );

    const ACCEPT_STATUS_TYPE = array(
        Interaction::ACCEPT_STATUS['PENDING'] => 'warning',
        Interaction::ACCEPT_STATUS['ACCEPTING'] => 'info',
        Interaction::ACCEPT_STATUS['COMPLETE'] => 'success'
    );

    const TYPE_CN = array(
        Interaction::TYPE['PERSONAL'] => '个人',
        Interaction::TYPE['ENTERPRISE'] => '企业',
        Interaction::TYPE['GOVERNMENT'] => '政府',
    );

    protected function getMemberTranslator():MemberTranslator
    {
        return new MemberTranslator();
    }

    protected function getUserGroupTranslator():UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getReplyTranslator():ReplyTranslator
    {
        return new ReplyTranslator();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($interaction, array $keys = array())
    {
        if (!$interaction instanceof Interaction) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'images',
                'contact',
                'acceptStatus',
                'admissibility',
                'createTime',
                'updateTime',
                'status',
                'statusTime',
                'acceptUserGroup'=>[],
                'member'=>[],
                'reply'=>[],
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($interaction->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $interaction->getTitle();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $interaction->getContent();
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $interaction->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $interaction->getIdentify();
            $expression['identifyMask'] = Mask::mask($interaction->getIdentify(), 4, 10);
        }
        if (in_array('subject', $keys)) {
            $expression['subject'] = $interaction->getSubject();
        }
        if (in_array('type', $keys)) {
            $expression['type'] = [
                'id' => marmot_encode($interaction->getType()),
                'name' => self::TYPE_CN[$interaction->getType()]
                ];
        }
        if (in_array('images', $keys)) {
            $expression['images'] = $interaction->getImages();
        }
        if (in_array('contact', $keys)) {
            $expression['contact'] = $interaction->getContact();
        }
        if (in_array('acceptStatus', $keys)) {
            $expression['acceptStatus'] = [
                'id' => marmot_encode($interaction->getAcceptStatus()),
                'name'=> self::ACCEPT_STATUS_CN[$interaction->getAcceptStatus()],
                'type'=> self::ACCEPT_STATUS_TYPE[$interaction->getAcceptStatus()],
            ];
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $interaction->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $interaction->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $interaction->getUpdateTime());
        }
        if (in_array('status', $keys)) {
            $expression['status']['id'] = marmot_encode($interaction->getStatus());
            $expression['status']['name'] = self::STATUS_CN[$interaction->getStatus()];
            $expression['status']['type'] = self::STATUS_TYPE[$interaction->getStatus()];
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $interaction->getStatusTime();
        }

        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $interaction->getMember(),
                $keys['member']
            );
        }
        if (isset($keys['acceptUserGroup'])) {
            $expression['acceptUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $interaction->getAcceptUserGroup(),
                $keys['acceptUserGroup']
            );
        }
        if (isset($keys['reply'])) {
            $expression['reply'] = $this->getReplyTranslator()->objectToArray(
                $interaction->getReply(),
                $keys['reply']
            );
        }
        if (in_array('admissibility', $keys)) {
            $expression['admissibility'] = $expression['reply']['admissibility'];
        }

        return $expression;
    }
}
