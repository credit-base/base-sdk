<?php
namespace Base\Sdk\Interaction\Translator\Interaction;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\Interaction\Model\Interaction;

use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\Member\Translator\MemberRestfulTranslator;
use Base\Sdk\Interaction\Translator\Reply\ReplyRestfulTranslator;

abstract class InteractionRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getMemberRestfulTranslator(): MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }

    protected function getReplyRestfulTranslator(): ReplyRestfulTranslator
    {
        return new ReplyRestfulTranslator();
    }

    public function arrayToObject(array $expression, $interaction = null)
    {
        return $this->translateToObject($expression, $interaction);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $interaction = null)
    {
        $data =  $expression['data'];
       
        $id = $data['id'];
       
        $interaction->setId($id);
      
        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $interaction->setTitle($attributes['title']);
        }
        if (isset($attributes['content'])) {
            $interaction->setContent($attributes['content']);
        }
        if (isset($attributes['images'])) {
            $interaction->setImages($attributes['images']);
        }
        if (isset($attributes['contact'])) {
            $interaction->setContact($attributes['contact']);
        }
        if (isset($attributes['name'])) {
            $interaction->setName($attributes['name']);
        }
        if (isset($attributes['identify'])) {
            $interaction->setIdentify($attributes['identify']);
        }
        if (isset($attributes['subject'])) {
            $interaction->setSubject($attributes['subject']);
        }
        if (isset($attributes['acceptStatus'])) {
            $interaction->setAcceptStatus($attributes['acceptStatus']);
        }
        if (isset($attributes['createTime'])) {
            $interaction->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $interaction->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $interaction->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $interaction->setStatusTime($attributes['statusTime']);
        }
       
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        
        if (isset($relationships['acceptUserGroup']['data'])) {
            $acceptUserGroup = $this->changeArrayFormat($relationships['acceptUserGroup']['data']);
            $interaction->setAcceptUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($acceptUserGroup));
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $interaction->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }
        
        if (isset($relationships['reply']['data'])) {
            if (isset($expression['included'])) {
                $reply = $this->changeArrayFormat($relationships['reply']['data'], $expression['included']);
            }

            if (!isset($expression['included'])) {
                $reply = $this->changeArrayFormat($relationships['reply']['data']);
            }
            
            $interaction->setReply($this->getReplyRestfulTranslator()->arrayToObject($reply));
        }

        return $interaction;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($interaction, array $keys = array())
    {
        $expression = array();

        if (!$interaction instanceof Interaction) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'contact',
                'images',
                'member',
                'acceptUserGroup',
                'reply',
            );
        }

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $interaction->getId();
        }

        $attributes = array();

        if (in_array('type', $keys)) {
            $attributes['type'] = $interaction->getType();
        }
        if (in_array('title', $keys)) {
            $attributes['title'] = $interaction->getTitle();
        }
        if (in_array('images', $keys)) {
            $attributes['images'] = $interaction->getImages();
        }
        if (in_array('name', $keys)) {
            $attributes['name'] = $interaction->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $interaction->getIdentify();
        }
        if (in_array('subject', $keys)) {
            $attributes['subject'] = $interaction->getSubject();
        }
        if (in_array('contact', $keys)) {
            $attributes['contact'] = $interaction->getContact();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $interaction->getContent();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $interaction->getMember()->getId()
                )
            );
        }
        if (in_array('acceptUserGroup', $keys)) {
            $expression['data']['relationships']['acceptUserGroup']['data'] = array(
                array(
                    'type' => 'userGroups',
                    'id' => $interaction->getAcceptUserGroup()->getId()
                )
            );
        }

        if (in_array('reply', $keys)) {
            $expression['data']['relationships']['reply']['data'] =
                array_values($this->getReplyRestfulTranslator()->objectToArray($interaction->getReply()));
        }

        return $expression;
    }
}
