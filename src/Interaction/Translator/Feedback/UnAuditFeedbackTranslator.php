<?php
namespace Base\Sdk\Interaction\Translator\Feedback;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Interaction\Model\UnAuditFeedback;
use Base\Sdk\Interaction\Model\NullUnAuditFeedback;

use Base\Sdk\Interaction\Translator\Interaction\UnAuditInteractionTranslator;

class UnAuditFeedbackTranslator extends UnAuditInteractionTranslator
{
    public function arrayToObject(array $expression, $unAuditFeedback = null)
    {
        unset($unAuditFeedback);
        unset($expression);
        return new NullUnAuditFeedback();
    }

    public function objectToArray($unAuditFeedback, array $keys = array())
    {
        $unAuditInteraction = parent::objectToArray($unAuditFeedback, $keys);

        if (!$unAuditFeedback instanceof UnAuditFeedback) {
            return array();
        }

        $expression = array();

        $expression = array_merge($unAuditInteraction, $expression);

        return $expression;
    }
}
