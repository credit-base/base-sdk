<?php
namespace Base\Sdk\Interaction\Translator\Feedback;

use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\Interaction\Model\NullFeedback;

use Base\Sdk\Interaction\Translator\Interaction\InteractionRestfulTranslator;

class FeedbackRestfulTranslator extends InteractionRestfulTranslator
{
    protected function translateToObject(array $expression, $feedback = null)
    {
        if (empty($expression)) {
            return new NullFeedback();
        }

        if ($feedback == null) {
            $feedback = new Feedback();
        }

        $feedback = parent::translateToObject($expression, $feedback);
        
        return $feedback;
    }

    public function objectToArray($feedback, array $keys = array())
    {
        $interaction = parent::objectToArray($feedback, $keys);

        if (!$feedback instanceof Feedback) {
            return array();
        }

        $expression['data'] = $interaction['data'];
        $expression['data']['type'] = 'feedbacks';
        
        return $expression;
    }
}
