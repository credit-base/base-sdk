<?php
namespace Base\Sdk\Interaction\Translator\Feedback;

use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\Interaction\Model\NullFeedback;

use Base\Sdk\Interaction\Translator\Interaction\InteractionTranslator;

class FeedbackTranslator extends InteractionTranslator
{
    public function arrayToObject(array $expression, $feedback = null)
    {
        unset($feedback);
        unset($expression);
        return new NullFeedback();
    }

    public function objectToArray($feedback, array $keys = array())
    {
        $interaction = parent::objectToArray($feedback, $keys);

        if (!$feedback instanceof Feedback) {
            return array();
        }
        
        $expression = array();
        
        $expression = array_merge($interaction, $expression);
        
        return $expression;
    }
}
