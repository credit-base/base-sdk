<?php
namespace Base\Sdk\Interaction\Translator\Feedback;

use Base\Sdk\Interaction\Model\UnAuditFeedback;
use Base\Sdk\Interaction\Model\NullUnAuditFeedback;

use Base\Sdk\Interaction\Translator\Interaction\UnAuditInteractionRestfulTranslator;

class UnAuditFeedbackRestfulTranslator extends UnAuditInteractionRestfulTranslator
{
    protected function translateToObject(array $expression, $unAuditFeedback = null)
    {
        if (empty($expression)) {
            return new NullUnAuditFeedback();
        }

        if ($unAuditFeedback == null) {
            $unAuditFeedback = new UnAuditFeedback();
        }

        $unAuditFeedback = parent::translateToObject($expression, $unAuditFeedback);

        return $unAuditFeedback;
    }

    public function objectToArray($unAuditFeedback, array $keys = array())
    {
        $unAuditInteraction = parent::objectToArray($unAuditFeedback, $keys);

        if (!$unAuditFeedback instanceof UnAuditFeedback) {
            return array();
        }

        $expression['data'] = $unAuditInteraction['data'];
        $expression['data']['type'] = 'unAuditedFeedbacks';
        
        return $expression;
    }
}
