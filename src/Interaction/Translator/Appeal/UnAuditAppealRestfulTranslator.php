<?php
namespace Base\Sdk\Interaction\Translator\Appeal;

use Base\Sdk\Interaction\Model\UnAuditAppeal;
use Base\Sdk\Interaction\Model\NullUnAuditAppeal;

use Base\Sdk\Interaction\Translator\Interaction\UnAuditInteractionRestfulTranslator;

class UnAuditAppealRestfulTranslator extends UnAuditInteractionRestfulTranslator
{
    protected function translateToObject(array $expression, $unAuditAppeal = null)
    {
        if (empty($expression)) {
            return new NullUnAuditAppeal();
        }

        if ($unAuditAppeal == null) {
            $unAuditAppeal = new UnAuditAppeal();
        }

        $unAuditAppeal = parent::translateToObject($expression, $unAuditAppeal);

        $data = $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['certificates'])) {
            $unAuditAppeal->setCertificates($attributes['certificates']);
        }

        if (isset($attributes['appealType'])) {
            $unAuditAppeal->setType($attributes['appealType']);
        }

        return $unAuditAppeal;
    }

    public function objectToArray($unAuditAppeal, array $keys = array())
    {
        $unAuditInteraction = parent::objectToArray($unAuditAppeal, $keys);

        if (!$unAuditAppeal instanceof UnAuditAppeal) {
            return array();
        }

        $expression['data'] = $unAuditInteraction['data'];
        $expression['data']['type'] = 'unAuditedAppeals';
        
        return $expression;
    }
}
