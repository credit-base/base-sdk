<?php
namespace Base\Sdk\Interaction\Translator\Appeal;

use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\Interaction\Model\NullAppeal;

use Base\Sdk\Interaction\Translator\Interaction\InteractionRestfulTranslator;

class AppealRestfulTranslator extends InteractionRestfulTranslator
{
    protected function translateToObject(array $expression, $appeal = null)
    {
        if (empty($expression)) {
            return new NullAppeal();
        }

        if ($appeal == null) {
            $appeal = new Appeal();
        }

        $appeal = parent::translateToObject($expression, $appeal);

        $data = $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['certificates'])) {
            $appeal->setCertificates($attributes['certificates']);
        }

        if (isset($attributes['appealType'])) {
            $appeal->setType($attributes['appealType']);
        }
        
        return $appeal;
    }

    public function objectToArray($appeal, array $keys = array())
    {
        $interaction = parent::objectToArray($appeal, $keys);

        if (!$appeal instanceof Appeal) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'certificates',
            );
        }

        $expression['data'] = $interaction['data'];
       
        $expression['data']['type'] = 'appeals';
        
        if (in_array('certificates', $keys)) {
            $expression['data']['attributes']['certificates'] = $appeal->getCertificates();
        }
       
        return $expression;
    }
}
