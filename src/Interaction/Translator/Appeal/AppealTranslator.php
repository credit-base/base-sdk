<?php
namespace Base\Sdk\Interaction\Translator\Appeal;

use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\Interaction\Model\NullAppeal;

use Base\Sdk\Interaction\Translator\Interaction\InteractionTranslator;

class AppealTranslator extends InteractionTranslator
{
    public function arrayToObject(array $expression, $appeal = null)
    {
        unset($appeal);
        unset($expression);
        return new NullAppeal();
    }

    public function objectToArray($appeal, array $keys = array())
    {
        $interaction = parent::objectToArray($appeal, $keys);

        if (!$appeal instanceof Appeal) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'certificates',
            );
        }

        $expression = array();

        if (in_array('certificates', $keys)) {
            $expression['certificates'] = $appeal->getCertificates();
        }
        
        $expression = array_merge($interaction, $expression);
        
        return $expression;
    }
}
