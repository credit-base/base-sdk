<?php
namespace Base\Sdk\Interaction\Translator\Appeal;

use Base\Sdk\Interaction\Model\UnAuditAppeal;
use Base\Sdk\Interaction\Model\NullUnAuditAppeal;

use Base\Sdk\Interaction\Translator\Interaction\UnAuditInteractionTranslator;

class UnAuditAppealTranslator extends UnAuditInteractionTranslator
{
    public function arrayToObject(array $expression, $unAuditAppeal = null)
    {
        unset($unAuditAppeal);
        unset($expression);
        return new NullUnAuditAppeal();
    }

    public function objectToArray($unAuditAppeal, array $keys = array())
    {
        $unAuditInteraction = parent::objectToArray($unAuditAppeal, $keys);

        if (!$unAuditAppeal instanceof UnAuditAppeal) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'certificates',
            );
        }

        $expression = array();

        if (in_array('certificates', $keys)) {
            $expression['certificates'] = $unAuditAppeal->getCertificates();
        }

        $expression = array_merge($unAuditInteraction, $expression);

        return $expression;
    }
}
