<?php
namespace Base\Sdk\Interaction\Translator\Complaint;

use Base\Sdk\Interaction\Model\UnAuditComplaint;
use Base\Sdk\Interaction\Model\NullUnAuditComplaint;

use Base\Sdk\Interaction\Translator\Interaction\UnAuditInteractionTranslator;

class UnAuditComplaintTranslator extends UnAuditInteractionTranslator
{
    public function arrayToObject(array $expression, $unAuditComplaint = null)
    {
        unset($unAuditComplaint);
        unset($expression);
        return new NullUnAuditComplaint();
    }

    public function objectToArray($unAuditComplaint, array $keys = array())
    {
        $unAuditInteraction = parent::objectToArray($unAuditComplaint, $keys);

        if (!$unAuditComplaint instanceof UnAuditComplaint) {
            return array();
        }

        $expression = array();

        $expression = array_merge($unAuditInteraction, $expression);

        return $expression;
    }
}
