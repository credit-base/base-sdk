<?php
namespace Base\Sdk\Interaction\Translator\Complaint;

use Base\Sdk\Interaction\Model\Complaint;
use Base\Sdk\Interaction\Model\NullComplaint;

use Base\Sdk\Interaction\Translator\Interaction\InteractionTranslator;

class ComplaintTranslator extends InteractionTranslator
{
    public function arrayToObject(array $expression, $complaint = null)
    {
        unset($complaint);
        unset($expression);
        return new NullComplaint();
    }

    public function objectToArray($complaint, array $keys = array())
    {
        $interaction = parent::objectToArray($complaint, $keys);

        if (!$complaint instanceof Complaint) {
            return array();
        }
        
        $expression = array();
        
        $expression = array_merge($interaction, $expression);
        
        return $expression;
    }
}
