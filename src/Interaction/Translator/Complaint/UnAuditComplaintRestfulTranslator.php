<?php
namespace Base\Sdk\Interaction\Translator\Complaint;

use Base\Sdk\Interaction\Model\UnAuditComplaint;
use Base\Sdk\Interaction\Model\NullUnAuditComplaint;

use Base\Sdk\Interaction\Translator\Interaction\UnAuditInteractionRestfulTranslator;

class UnAuditComplaintRestfulTranslator extends UnAuditInteractionRestfulTranslator
{
    protected function translateToObject(array $expression, $unAuditComplaint = null)
    {
        if (empty($expression)) {
            return new NullUnAuditComplaint();
        }

        if ($unAuditComplaint == null) {
            $unAuditComplaint = new UnAuditComplaint();
        }

        $unAuditComplaint = parent::translateToObject($expression, $unAuditComplaint);

        $data = $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['complaintType'])) {
            $unAuditComplaint->setType($attributes['complaintType']);
        }

        return $unAuditComplaint;
    }

    public function objectToArray($unAuditComplaint, array $keys = array())
    {
        $unAuditInteraction = parent::objectToArray($unAuditComplaint, $keys);

        if (!$unAuditComplaint instanceof UnAuditComplaint) {
            return array();
        }

        $unAuditInteraction = parent::objectToArray($unAuditComplaint, $keys);

        $expression['data'] = $unAuditInteraction['data'];
        $expression['data']['type'] = 'unAuditedComplaints';
        
        return $expression;
    }
}
