<?php
namespace Base\Sdk\Interaction\Translator\Complaint;

use Base\Sdk\Interaction\Model\Complaint;
use Base\Sdk\Interaction\Model\NullComplaint;

use Base\Sdk\Interaction\Translator\Interaction\InteractionRestfulTranslator;

class ComplaintRestfulTranslator extends InteractionRestfulTranslator
{
    protected function translateToObject(array $expression, $complaint = null)
    {
        if (empty($expression)) {
            return new NullComplaint();
        }

        if ($complaint == null) {
            $complaint = new Complaint();
        }
        
        $complaint = parent::translateToObject($expression, $complaint);

        $data = $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['complaintType'])) {
            $complaint->setType($attributes['complaintType']);
        }
        
        return $complaint;
    }

    public function objectToArray($complaint, array $keys = array())
    {
        $interaction = parent::objectToArray($complaint, $keys);

        if (!$complaint instanceof Complaint) {
            return array();
        }

        $interaction = parent::objectToArray($complaint, $keys);

        $expression['data'] = $interaction['data'];
        $expression['data']['type'] = 'complaints';
        
        return $expression;
    }
}
