<?php
namespace Base\Sdk\Interaction\Translator\Qa;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Model\NullQa;

use Base\Sdk\Interaction\Translator\Interaction\InteractionRestfulTranslator;

class QaRestfulTranslator extends InteractionRestfulTranslator
{
    protected function translateToObject(array $expression, $qaObject = null)
    {
        if (empty($expression)) {
            return new NullQa();
        }

        if ($qaObject == null) {
            $qaObject = new Qa();
        }

        $qaObject = parent::translateToObject($expression, $qaObject);
        
        return $qaObject;
    }

    public function objectToArray($qaObject, array $keys = array())
    {
        $interaction = parent::objectToArray($qaObject, $keys);

        if (!$qaObject instanceof Qa) {
            return array();
        }

        $expression['data'] = $interaction['data'];
        $expression['data']['type'] = 'qas';
        
        return $expression;
    }
}
