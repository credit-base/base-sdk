<?php
namespace Base\Sdk\Interaction\Translator\Qa;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Model\NullQa;

use Base\Sdk\Interaction\Translator\Interaction\InteractionTranslator;

class QaTranslator extends InteractionTranslator
{
    public function arrayToObject(array $expression, $qaObject = null)
    {
        unset($qaObject);
        unset($expression);
        return new NullQa();
    }

    public function objectToArray($qaObject, array $keys = array())
    {
        $interaction = parent::objectToArray($qaObject, $keys);

        if (!$qaObject instanceof Qa) {
            return array();
        }

        $expression = array();

        $expression = array_merge($interaction, $expression);
        
        return $expression;
    }
}
