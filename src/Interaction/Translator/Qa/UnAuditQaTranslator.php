<?php
namespace Base\Sdk\Interaction\Translator\Qa;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Interaction\Model\UnAuditQa;
use Base\Sdk\Interaction\Model\NullUnAuditQa;

use Base\Sdk\Interaction\Translator\Interaction\UnAuditInteractionTranslator;

class UnAuditQaTranslator extends UnAuditInteractionTranslator
{
    public function arrayToObject(array $expression, $unAuditQa = null)
    {
        unset($unAuditQa);
        unset($expression);
        return new NullUnAuditQa();
    }

    public function objectToArray($unAuditQa, array $keys = array())
    {
        if (!$unAuditQa instanceof UnAuditQa) {
            return array();
        }

        $expression = array();

        $unAuditInteraction = parent::objectToArray($unAuditQa, $keys);

        $expression = array_merge($unAuditInteraction, $expression);

        return $expression;
    }
}
