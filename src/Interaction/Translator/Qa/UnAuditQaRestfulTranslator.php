<?php
namespace Base\Sdk\Interaction\Translator\Qa;

use Base\Sdk\Interaction\Model\UnAuditQa;
use Base\Sdk\Interaction\Model\NullUnAuditQa;

use Base\Sdk\Interaction\Translator\Interaction\UnAuditInteractionRestfulTranslator;

class UnAuditQaRestfulTranslator extends UnAuditInteractionRestfulTranslator
{
    protected function translateToObject(array $expression, $unAuditQa = null)
    {
        if (empty($expression)) {
            return new NullUnAuditQa();
        }

        if ($unAuditQa == null) {
            $unAuditQa = new UnAuditQa();
        }

        $unAuditQa = parent::translateToObject($expression, $unAuditQa);

        return $unAuditQa;
    }

    public function objectToArray($unAuditQa, array $keys = array())
    {
        $unAuditInteraction = parent::objectToArray($unAuditQa, $keys);

        if (!$unAuditQa instanceof UnAuditQa) {
            return array();
        }

        $expression['data'] = $unAuditInteraction['data'];
        $expression['data']['type'] = 'unAuditedQas';
        
        return $expression;
    }
}
