<?php
namespace Base\Sdk\Interaction\WidgetRules;

use Respect\Validation\Validator as V;

use Base\Sdk\Common\WidgetRules\WidgetRules;

use Marmot\Core;

use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Model\Interaction;

class InteractionWidgetRules
{
    private static $instance;

    protected function getCommonWidgetRules() : WidgetRules
    {
        return new WidgetRules();
    }

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    const IMAGES_LENGTH = 9;

    public function images($images, $pointer = 'images') : bool
    {
        if (count($images) > self::IMAGES_LENGTH) {
            Core::setLastError(IMAGES_COUNT_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        if (!$this->getCommonWidgetRules()->images($images, $pointer)) {
            return false;
        }

        return true;
    }

    const CONTENT_MIN_LENGTH = 1;
    const CONTENT_MAX_LENGTH = 2000;

    public function content($content, $pointer = 'content') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::CONTENT_MIN_LENGTH,
            self::CONTENT_MAX_LENGTH
        )->validate($content)) {
            Core::setLastError(REPLY_CONTENT_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function admissibility($admissibility, $pointer = 'admissibility') : bool
    {
        if (!in_array($admissibility, Reply::ADMISSIBILITY)) {
            Core::setLastError(REPLY_ADMISSIBILITY_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }
        return true;
    }

    const TITLE_MIN_LENGTH = 1;
    const TITLE_MAX_LENGTH = 50;

    public function title($data, string $pointer = 'title') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::TITLE_MIN_LENGTH,
            self::TITLE_MAX_LENGTH
        )->validate($data)) {
            Core::setLastError(TITLE_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    const SUBJECT_MIN_LENGTH = 2;
    const SUBJECT_MAX_LENGTH = 50;

    public function subject($data, string $pointer = 'subject') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::SUBJECT_MIN_LENGTH,
            self::SUBJECT_MAX_LENGTH
        )->validate($data)) {
            Core::setLastError(SUBJECT_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function identify($identify) : bool
    {
        $reg = '/[a-zA-Z0-9]/';
        if (!V::alnum()->noWhitespace()->length(13, 18)->validate($identify) ||
        !preg_match($reg, $identify)) {
            Core::setLastError(IDENTIFY_FORMAT_ERROR, array('pointer' => 'identify'));
            return false;
        }

        return true;
    }

    public function type($type) : bool
    {
        if (!in_array($type, Interaction::TYPE)) {
            Core::setLastError(INTERACTION_TYPE_FORMAT_ERROR, array('pointer' => 'type'));
            return false;
        }
        
        return true;
    }

    const CONTACT_MIN_LENGTH = 1;
    const CONTACT_MAX_LENGTH = 26;

    public function contact($contact) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::CONTACT_MIN_LENGTH,
            self::CONTACT_MAX_LENGTH
        )->validate($contact)) {
            Core::setLastError(CONTACT_FORMAT_ERROR, array('pointer' => 'contact'));
            return false;
        }

        return true;
    }

    const CERTIFICATES_LENGTH = 2;

    public function certificates($certificates, $pointer = 'certificates') : bool
    {
        if (count($certificates) > self::CERTIFICATES_LENGTH) {
            Core::setLastError(IMAGES_COUNT_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        if (!$this->getCommonWidgetRules()->images($certificates, $pointer)) {
            return false;
        }

        return true;
    }
}
