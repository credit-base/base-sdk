<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditAppeal;

use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Interaction\Model\UnAuditAppeal;
use Base\Sdk\Interaction\Model\NullUnAuditAppeal;

use Base\Sdk\Interaction\Translator\Appeal\UnAuditAppealRestfulTranslator;
use Base\Sdk\Interaction\Adapter\UnAuditInteraction\IUnAuditInteractionAdapter;
use Base\Sdk\Interaction\Adapter\UnAuditInteraction\UnAuditInteractionRestfulAdapterTrait;

class UnAuditAppealRestfulAdapter extends GuzzleAdapter implements IUnAuditInteractionAdapter
{
    use UnAuditInteractionRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDIT_APPEAL_LIST'=>[
            'fields' => [],
            'include' => 'member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation'
        ],
        'UN_AUDIT_APPEAL_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditAppealRestfulTranslator();
        $this->resource = 'unAuditedAppeals';
        $this->scenario = array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullUnAuditAppeal());
    }
}
