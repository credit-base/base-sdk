<?php
namespace Base\Sdk\Interaction\Adapter\Appeal;

use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\Interaction\Model\NullAppeal;

use Base\Sdk\Interaction\Adapter\Interaction\IInteractionAdapter;
use Base\Sdk\Interaction\Adapter\Interaction\InteractionRestfulAdapterTrait;

use Base\Sdk\Interaction\Translator\Appeal\AppealRestfulTranslator;

class AppealRestfulAdapter extends GuzzleAdapter implements IInteractionAdapter
{
    use InteractionRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'APPEAL_LIST'=>[
            'fields'=>[],
            'include'=>'crew,applyCrew,member,acceptUserGroup,reply,reply.crew'
        ],
        'APPEAL_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,applyCrew,member,acceptUserGroup,reply,reply.crew'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new AppealRestfulTranslator();
        $this->resource = 'appeals';
        $this->scenario = array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullAppeal());
    }

    protected function addAction(Appeal $appeal) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $appeal,
            array(
                'title',
                'content',
                'name',
                'identify',
                'certificates',
                'type',
                'contact',
                'images',
                'member',
                'acceptUserGroup'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($appeal);
            return true;
        }

        return false;
    }
}
