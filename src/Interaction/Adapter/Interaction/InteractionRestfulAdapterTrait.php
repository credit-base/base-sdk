<?php
namespace Base\Sdk\Interaction\Adapter\Interaction;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;

trait InteractionRestfulAdapterTrait
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        ModifyStatusAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            100=>[
                'memberId'=>MEMBER_ID_IS_EMPTY,
                'acceptUserGroupId'=>ACCEPT_USER_GROUP_ID_IS_EMPTY,
                'crewId' => CREW_ID_IS_EMPTY
            ],
            101 => [
                'title' => TITLE_FORMAT_ERROR,
                'content' => CONTENT_FORMAT_ERROR,
                'images' => IMAGE_FORMAT_ERROR,
                'imagesCount' => IMAGES_COUNT_FORMAT_ERROR,
                'memberId' => MEMBER_ID_FORMAT_ERROR,
                'acceptUserGroupId' => ACCEPT_USER_GROUP_ID_FORMAT_ERROR,
                'name'=>NAME_FORMAT_ERROR,
                'identify'=>IDENTIFY_FORMAT_ERROR,
                'subject'=>SUBJECT_FORMAT_ERROR,
                'type'=>TYPE_FORMAT_ERROR,
                'contact'=>CONTACT_FORMAT_ERROR,
                'replyContent'=>REPLY_CONTENT_FORMAT_ERROR,
                'replyImages'=>REPLY_IMAGES_FORMAT_ERROR,
                'replyImagesCount'=>REPLY_IMAGES_COUNT_FORMAT_ERROR,
                'replyAdmissibility'=>REPLY_ADMISSIBILITY_FORMAT_ERROR,
                'replyCrew'=>REPLY_CREW_FORMAT_ERROR,
                'certificates'=>CERTIFICATES_FORMAT_ERROR
            ],
            102=>[
                'applyStatus'=>APPLY_STATUE_CAN_NOT_MODIFY,
                'status'=>STATUS_CAN_NOT_MODIFY
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function addAction($object) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $object,
            array(
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'contact',
                'images',
                'member',
                'acceptUserGroup'
            )
        );
      
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($object);
            return true;
        }

        return false;
    }

    protected function editAction($object) : bool
    {
        unset($object);
        return false;
    }

    public function accept($object) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $object,
            array('reply')
        );

        $this->patch(
            $this->getResource().'/'.$object->getId().'/accept',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($object);
            return true;
        }

        return false;
    }

    public function publish($object) : bool
    {
        $this->patch(
            $this->getResource().'/'.$object->getId().'/publish'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($object);
            return true;
        }

        return false;
    }

    public function unPublish($object) : bool
    {
        $this->patch(
            $this->getResource().'/'.$object->getId().'/unPublish'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($object);
            return true;
        }

        return false;
    }
}
