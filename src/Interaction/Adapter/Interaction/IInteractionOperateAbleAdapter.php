<?php
namespace Base\Sdk\Interaction\Adapter\Interaction;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

interface IInteractionOperateAbleAdapter extends IOperateAbleAdapter, IModifyStatusAbleAdapter
{
    
}
