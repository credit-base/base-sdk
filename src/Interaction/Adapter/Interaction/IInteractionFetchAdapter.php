<?php
namespace Base\Sdk\Interaction\Adapter\Interaction;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IInteractionFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
