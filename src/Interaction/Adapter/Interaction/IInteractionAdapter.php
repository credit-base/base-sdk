<?php
namespace Base\Sdk\Interaction\Adapter\Interaction;

interface IInteractionAdapter extends IInteractionFetchAdapter, IInteractionOperateAbleAdapter
{
    
}
