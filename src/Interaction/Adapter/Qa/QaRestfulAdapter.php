<?php
namespace Base\Sdk\Interaction\Adapter\Qa;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Model\NullQa;

use Base\Sdk\Interaction\Adapter\Interaction\IInteractionAdapter;
use Base\Sdk\Interaction\Adapter\Interaction\InteractionRestfulAdapterTrait;

use Base\Sdk\Interaction\Translator\Qa\QaRestfulTranslator;

class QaRestfulAdapter extends GuzzleAdapter implements IInteractionAdapter
{
    use InteractionRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'QA_LIST'=>[
            'fields'=>[],
            'include'=>'crew,applyCrew,member,acceptUserGroup,reply,reply.crew'
        ],
        'QA_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,applyCrew,member,acceptUserGroup,reply,reply.crew'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new QaRestfulTranslator();
        $this->resource = 'qas';
        $this->scenario = array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullQa());
    }

    protected function addAction(Qa $qaObject) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $qaObject,
            array(
                'title',
                'content',
                'member',
                'acceptUserGroup'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($qaObject);
            return true;
        }

        return false;
    }
}
