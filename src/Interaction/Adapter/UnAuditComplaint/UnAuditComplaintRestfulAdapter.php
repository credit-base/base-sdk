<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditComplaint;

use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Interaction\Model\NullUnAuditComplaint;

use Base\Sdk\Interaction\Translator\Complaint\UnAuditComplaintRestfulTranslator;
use Base\Sdk\Interaction\Adapter\UnAuditInteraction\IUnAuditInteractionAdapter;
use Base\Sdk\Interaction\Adapter\UnAuditInteraction\UnAuditInteractionRestfulAdapterTrait;

class UnAuditComplaintRestfulAdapter extends GuzzleAdapter implements IUnAuditInteractionAdapter
{
    use UnAuditInteractionRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDIT_COMPLAINT_LIST'=>[
            'fields' => [],
            'include' => 'member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation'
        ],
        'UN_AUDIT_COMPLAINT_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditComplaintRestfulTranslator();
        $this->resource = 'unAuditedComplaints';
        $this->scenario = array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullUnAuditComplaint());
    }
}
