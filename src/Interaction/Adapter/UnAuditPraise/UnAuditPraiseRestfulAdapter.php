<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditPraise;

use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Interaction\Model\NullUnAuditPraise;
use Base\Sdk\Interaction\Translator\Praise\UnAuditPraiseRestfulTranslator;
use Base\Sdk\Interaction\Adapter\UnAuditInteraction\IUnAuditInteractionAdapter;
use Base\Sdk\Interaction\Adapter\UnAuditInteraction\UnAuditInteractionRestfulAdapterTrait;

class UnAuditPraiseRestfulAdapter extends GuzzleAdapter implements IUnAuditInteractionAdapter
{
    use UnAuditInteractionRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDIT_PRAISE_LIST'=>[
            'fields' => [],
            'include' => 'member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation'
        ],
        'UN_AUDIT_PRAISE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditPraiseRestfulTranslator();
        $this->resource = 'unAuditedPraises';
        $this->scenario = array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullUnAuditPraise());
    }
}
