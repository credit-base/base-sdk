<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditQa;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Interaction\Model\UnAuditQa;
use Base\Sdk\Interaction\Model\NullUnAuditQa;

use Base\Sdk\Interaction\Translator\Qa\UnAuditQaRestfulTranslator;
use Base\Sdk\Interaction\Adapter\UnAuditInteraction\IUnAuditInteractionAdapter;
use Base\Sdk\Interaction\Adapter\UnAuditInteraction\UnAuditInteractionRestfulAdapterTrait;

class UnAuditQaRestfulAdapter extends GuzzleAdapter implements IUnAuditInteractionAdapter
{
    use UnAuditInteractionRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDIT_QA_LIST'=>[
            'fields' => [],
            'include' => 'member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation'
        ],
        'UN_AUDIT_QA_FETCH_ONE'=>[
            'fields'=> [],
            'include'=> 'member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditQaRestfulTranslator();
        $this->resource = 'unAuditedQas';
        $this->scenario = array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullUnAuditQa());
    }
}
