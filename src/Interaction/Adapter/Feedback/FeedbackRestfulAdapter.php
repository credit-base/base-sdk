<?php
namespace Base\Sdk\Interaction\Adapter\Feedback;

use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\Interaction\Model\NullFeedback;

use Base\Sdk\Interaction\Adapter\Interaction\IInteractionAdapter;
use Base\Sdk\Interaction\Adapter\Interaction\InteractionRestfulAdapterTrait;

use Base\Sdk\Interaction\Translator\Feedback\FeedbackRestfulTranslator;

class FeedbackRestfulAdapter extends GuzzleAdapter implements IInteractionAdapter
{
    use InteractionRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'FEEDBACK_LIST'=>[
            'fields'=>[],
            'include'=>'crew,applyCrew,member,acceptUserGroup,reply,reply.crew'
        ],
        'FEEDBACK_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,applyCrew,member,acceptUserGroup,reply,reply.crew'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new FeedbackRestfulTranslator();
        $this->resource = 'feedbacks';
        $this->scenario = array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullFeedback());
    }

    protected function addAction(Feedback $feedback) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $feedback,
            array(
                'title',
                'content',
                'member',
                'acceptUserGroup'
            )
        );
       
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($feedback);
            return true;
        }

        return false;
    }
}
