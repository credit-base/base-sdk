<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditFeedback;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Interaction\Model\UnAuditFeedback;
use Base\Sdk\Interaction\Model\NullUnAuditFeedback;

use Base\Sdk\Interaction\Translator\Feedback\UnAuditFeedbackRestfulTranslator;
use Base\Sdk\Interaction\Adapter\UnAuditInteraction\IUnAuditInteractionAdapter;
use Base\Sdk\Interaction\Adapter\UnAuditInteraction\UnAuditInteractionRestfulAdapterTrait;

class UnAuditFeedbackRestfulAdapter extends GuzzleAdapter implements IUnAuditInteractionAdapter
{
    use UnAuditInteractionRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDIT_FEEDBACK_LIST'=>[
            'fields' => [],
            'include' => 'member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation'
        ],
        'UN_AUDIT_FEEDBACK_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'member,acceptUserGroup,reply,reply.crew,applyUserGroup,applyCrew,relation'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditFeedbackRestfulTranslator();
        $this->resource = 'unAuditedFeedbacks';
        $this->scenario = array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullUnAuditFeedback());
    }
}
