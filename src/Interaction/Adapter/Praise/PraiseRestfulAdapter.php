<?php
namespace Base\Sdk\Interaction\Adapter\Praise;

use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Interaction\Model\NullPraise;

use Base\Sdk\Interaction\Adapter\Interaction\IInteractionAdapter;
use Base\Sdk\Interaction\Adapter\Interaction\InteractionRestfulAdapterTrait;

use Base\Sdk\Interaction\Translator\Praise\PraiseRestfulTranslator;

class PraiseRestfulAdapter extends GuzzleAdapter implements IInteractionAdapter
{
    use InteractionRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'PRAISE_LIST'=>[
            'fields'=>[],
            'include'=>'crew,applyCrew,member,acceptUserGroup,reply,reply.crew'
        ],
        'PRAISE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,applyCrew,member,acceptUserGroup,reply,reply.crew'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new PraiseRestfulTranslator();
        $this->resource = 'praises';
        $this->scenario = array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullPraise());
    }
}
