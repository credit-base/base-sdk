<?php
namespace Base\Sdk\Interaction\Adapter\Complaint;

use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Interaction\Model\NullComplaint;

use Base\Sdk\Interaction\Adapter\Interaction\IInteractionAdapter;
use Base\Sdk\Interaction\Adapter\Interaction\InteractionRestfulAdapterTrait;

use Base\Sdk\Interaction\Translator\Complaint\ComplaintRestfulTranslator;

class ComplaintRestfulAdapter extends GuzzleAdapter implements IInteractionAdapter
{
    use InteractionRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'COMPLAINT_LIST'=>[
            'fields'=>[],
            'include'=>'crew,applyCrew,member,acceptUserGroup,reply,reply.crew'
        ],
        'COMPLAINT_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,applyCrew,member,acceptUserGroup,reply,reply.crew'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new ComplaintRestfulTranslator();
        $this->resource = 'complaints';
        $this->scenario = array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullComplaint());
    }
}
