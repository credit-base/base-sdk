<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditInteraction;

use Base\Sdk\Common\Adapter\IApplyAbleAdapter;
use Base\Sdk\Common\Adapter\IResubmitAbleAdapter;

interface IUnAuditOperateAbleAdapter extends IApplyAbleAdapter, IResubmitAbleAdapter
{
    
}
