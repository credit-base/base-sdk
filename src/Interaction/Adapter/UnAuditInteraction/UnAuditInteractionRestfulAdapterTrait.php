<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditInteraction;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Model\IResubmitAble;

trait UnAuditInteractionRestfulAdapterTrait
{
    use FetchAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            101 => [
                'replyContent'=>REPLY_CONTENT_FORMAT_ERROR,
                'replyImages'=>REPLY_IMAGES_FORMAT_ERROR,
                'replyImagesCount'=>REPLY_IMAGES_COUNT_FORMAT_ERROR,
                'replyAdmissibility'=>REPLY_ADMISSIBILITY_FORMAT_ERROR,
                'replyCrew'=>REPLY_CREW_FORMAT_ERROR,
                'applyCrewId' => PARAMETER_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function approveAction(IApplyAble $unAuditInteraction) : bool
    {
        $data = $this->getTranslator()->objectToArray($unAuditInteraction, array('applyCrew'));
       
        $this->patch(
            $this->getResource().'/'.$unAuditInteraction->getId().'/approve',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($unAuditInteraction);
            return true;
        }
        return false;
    }

    protected function rejectAction(IApplyAble $unAuditInteraction) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditInteraction,
            array(
                'rejectReason',
                'applyCrew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$unAuditInteraction->getId().'/reject',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditInteraction);
            return true;
        }
        return false;
    }

    protected function resubmitAction(IResubmitAble $resubmitAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $resubmitAbleObject,
            array('reply')
        );
      
        $this->patch(
            $this->getResource().'/'.$resubmitAbleObject->getId().'/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($resubmitAbleObject);
            return true;
        }
        return false;
    }
}
