<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditInteraction;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IUnAuditFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
