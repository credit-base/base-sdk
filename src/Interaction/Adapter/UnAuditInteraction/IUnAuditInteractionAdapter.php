<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditInteraction;

interface IUnAuditInteractionAdapter extends IUnAuditFetchAdapter, IUnAuditOperateAbleAdapter
{
    
}
