<?php
namespace Base\Sdk\Interaction\Command\Complaint;

use Base\Sdk\Interaction\Command\AddCommand;

class AddComplaintCommand extends AddCommand
{
    public $subject;
    
    public function __construct(
        string $title,
        string $content,
        string $name,
        string $identify,
        string $contact,
        string $subject,
        array $images,
        int $type,
        int $acceptUserGroupId
    ) {
        parent::__construct(
            $title,
            $content,
            $name,
            $identify,
            $contact,
            $images,
            $type,
            $acceptUserGroupId
        );
        $this->subject = $subject;
    }
}
