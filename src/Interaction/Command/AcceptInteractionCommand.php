<?php
namespace Base\Sdk\Interaction\Command;

use Marmot\Interfaces\ICommand;

class AcceptInteractionCommand implements ICommand
{
    public $id;
    
    public $reply;
    
    public function __construct(
        array $reply,
        int $id
    ) {
        $this->id = $id;
        $this->reply = $reply;
    }
}
