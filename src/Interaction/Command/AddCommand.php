<?php
namespace Base\Sdk\Interaction\Command;

use Marmot\Interfaces\ICommand;

class AddCommand implements ICommand
{
    public $id;
    
    public $title;

    public $content;

    public $name;

    public $identify;

    public $type;

    public $contact;

    public $images;

    public $acceptUserGroupId;
    
    public function __construct(
        string $title,
        string $content,
        string $name,
        string $identify,
        string $contact,
        array $images,
        int $type,
        int $acceptUserGroupId,
        int $id = 0
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->name = $name;
        $this->identify = $identify;
        $this->contact = $contact;
        $this->images = $images;
        $this->type = $type;
        $this->acceptUserGroupId = $acceptUserGroupId;
    }
}
