<?php
namespace Base\Sdk\Interaction\Command;

use Marmot\Interfaces\ICommand;

class PublishInteractionCommand implements ICommand
{
    public $id;
    
    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
