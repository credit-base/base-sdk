<?php
namespace Base\Sdk\Interaction\Command\Appeal;

use Base\Sdk\Interaction\Command\AddCommand;

class AddAppealCommand extends AddCommand
{
    public $certificates;
    
    public function __construct(
        string $title,
        string $content,
        string $name,
        string $identify,
        string $contact,
        array $certificates,
        array $images,
        int $type,
        int $acceptUserGroupId
    ) {
        parent::__construct(
            $title,
            $content,
            $name,
            $identify,
            $contact,
            $images,
            $type,
            $acceptUserGroupId
        );
        $this->certificates = $certificates;
    }
}
