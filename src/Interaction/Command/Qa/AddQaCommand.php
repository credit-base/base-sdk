<?php
namespace Base\Sdk\Interaction\Command\Qa;

use Marmot\Interfaces\ICommand;

class AddQaCommand implements ICommand
{
    public $title;

    public $content;

    public $acceptUserGroupId;
    
    public function __construct(
        string $title,
        string $content,
        int $acceptUserGroupId
    ) {
        $this->title = $title;
        $this->content = $content;
        $this->acceptUserGroupId = $acceptUserGroupId;
    }
}
