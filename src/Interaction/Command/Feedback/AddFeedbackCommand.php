<?php
namespace Base\Sdk\Interaction\Command\Feedback;

use Marmot\Interfaces\ICommand;

class AddFeedbackCommand implements ICommand
{
    public $title;

    public $content;

    public $acceptUserGroupId;
    
    public function __construct(
        string $title,
        string $content,
        int $acceptUserGroupId
    ) {
        $this->title = $title;
        $this->content = $content;
        $this->acceptUserGroupId = $acceptUserGroupId;
    }
}
