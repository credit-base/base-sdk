<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Model\IResubmitAble;
use Base\Sdk\Common\Model\ApplyAbleTrait;
use Base\Sdk\Common\Model\ResubmitAbleTrait;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;
use Base\Sdk\Common\Adapter\IResubmitAbleAdapter;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;

abstract class UnAuditInteraction extends Interaction implements IApplyAble, IResubmitAble
{
    use ApplyAbleTrait, ResubmitAbleTrait;

    private $rejectReason;

    private $id;

    private $applyCrew;

    private $applyUserGroup;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->id = !empty($id) ? $id : 0;
        $this->rejectReason = '';
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->applyCrew = new Crew();
        $this->applyUserGroup = new UserGroup();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->applyCrew);
        unset($this->rejectReason);
        unset($this->applyUserGroup);
        unset($this->applyStatus);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setApplyStatus(int $applyStatus): void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus(): int
    {
        return $this->applyStatus;
    }

    public function setRejectReason(string $rejectReason): void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason(): string
    {
        return $this->rejectReason;
    }

    public function setApplyCrew(Crew $applyCrew) : void
    {
        $this->applyCrew = $applyCrew;
    }

    public function getApplyCrew() : Crew
    {
        return $this->applyCrew;
    }

    public function setApplyUserGroup(UserGroup $applyUserGroup) : void
    {
        $this->applyUserGroup = $applyUserGroup;
    }

    public function getApplyUserGroup() : UserGroup
    {
        return $this->applyUserGroup;
    }

    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIResubmitAbleAdapter() : IResubmitAbleAdapter
    {
        return $this->getRepository();
    }
}
