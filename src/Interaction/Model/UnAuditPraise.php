<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Interaction\Repository\UnAuditPraiseRepository;

class UnAuditPraise extends UnAuditInteraction
{
    private $unAuditPraiseRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->unAuditPraiseRepository = new UnAuditPraiseRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->unAuditPraiseRepository);
    }

    public function getRepository(): UnAuditPraiseRepository
    {
        return $this->unAuditPraiseRepository;
    }
}
