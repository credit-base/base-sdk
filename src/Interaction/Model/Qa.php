<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Interaction\Repository\QaRepository;

class Qa extends Interaction
{
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->repository = new QaRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    public function getRepository(): QaRepository
    {
        return $this->repository;
    }
}
