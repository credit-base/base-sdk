<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Interaction\Repository\UnAuditAppealRepository;

class UnAuditAppeal extends UnAuditInteraction
{
    /**
     * [$certificates 上传身份证/营业执照]
     * @var array
     */
    private $certificates;

    private $unAuditAppealRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->certificates = array();
        $this->unAuditAppealRepository = new UnAuditAppealRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->unAuditAppealRepository);
    }

    public function setCertificates(array $certificates) : void
    {
        $this->certificates = $certificates;
    }

    public function getCertificates() : array
    {
        return $this->certificates;
    }

    public function getRepository(): UnAuditAppealRepository
    {
        return $this->unAuditAppealRepository;
    }
}
