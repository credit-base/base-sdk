<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Interaction\Repository\UnAuditComplaintRepository;

class UnAuditComplaint extends UnAuditInteraction
{
    private $unAuditComplaintRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->unAuditComplaintRepository = new UnAuditComplaintRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->unAuditComplaintRepository);
    }

    public function getRepository(): UnAuditComplaintRepository
    {
        return $this->unAuditComplaintRepository;
    }

    // public function getRepository()
    // {
    //     return false;
    // }
}
