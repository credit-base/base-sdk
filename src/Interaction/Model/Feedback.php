<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Interaction\Repository\FeedbackRepository;

class Feedback extends Interaction
{
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->repository = new FeedbackRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    public function getRepository(): FeedbackRepository
    {
        return $this->repository;
    }
}
