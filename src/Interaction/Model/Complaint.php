<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Interaction\Repository\ComplaintRepository;

class Complaint extends Interaction
{
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->repository = new ComplaintRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    public function getRepository(): ComplaintRepository
    {
        return $this->repository;
    }
}
