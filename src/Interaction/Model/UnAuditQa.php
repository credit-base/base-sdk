<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Interaction\Repository\UnAuditQaRepository;

class UnAuditQa extends UnAuditInteraction
{
    private $unAuditQaRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->unAuditQaRepository = new UnAuditQaRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->unAuditQaRepository);
    }

    public function getRepository(): UnAuditQaRepository
    {
        return $this->unAuditQaRepository;
    }
}
