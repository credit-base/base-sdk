<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Interaction\Repository\UnAuditFeedbackRepository;

class UnAuditFeedback extends UnAuditInteraction
{
    private $unAuditFeedbackRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->unAuditFeedbackRepository = new UnAuditFeedbackRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->unAuditFeedbackRepository);
    }

    public function getRepository(): UnAuditFeedbackRepository
    {
        return $this->unAuditFeedbackRepository;
    }
}
