<?php
namespace Base\Sdk\Interaction\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Sdk\Common\Model\NullApplyAbleTrait;
use Base\Sdk\Common\Model\NullResubmitAbleTrait;

class NullUnAuditFeedback extends UnAuditFeedback implements INull
{
    use NullApplyAbleTrait, NullResubmitAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
