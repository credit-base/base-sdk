<?php
namespace Base\Sdk\Interaction\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Base\Sdk\Crew\Model\Crew;

class Reply implements IObject
{
    use Object;

    const ADMISSIBILITY = [
        'ADMISSIBLE'=>1,
        'INADMISSIBLE'=>2
    ];

    private $id;
    /**
     * [$content 内容]
     * @var string
     */
    private $content;
    /**
     * [$admissibility 受理情况]
     * @var int
     */
    private $admissibility;
    /**
     * [$images 图片]
     * @var array
     */
    private $images;
    /**
     * [$crew 受理人]
     * @var Crew
     */
    private $crew;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->content = '';
        $this->admissibility = self::ADMISSIBILITY['ADMISSIBLE'];
        $this->images = array();
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->content);
        unset($this->admissibility);
        unset($this->images);
        unset($this->status);
        unset($this->crew);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setAdmissibility(int $admissibility): void
    {
        $this->admissibility = $admissibility;
    }

    public function getAdmissibility(): int
    {
        return $this->admissibility;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setImages(array $images): void
    {
        $this->images = $images;
    }

    public function getImages(): array
    {
        return $this->images;
    }

    public function setCrew(Crew $crew): void
    {
        $this->crew = $crew;
    }

    public function getCrew(): Crew
    {
        return $this->crew;
    }
}
