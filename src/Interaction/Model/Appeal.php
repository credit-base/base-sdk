<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Interaction\Repository\AppealRepository;

class Appeal extends Interaction
{
    /**
     * [$certificates 上传身份证/营业执照]
     * @var array
     */
    private $certificates;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->certificates = array();
        $this->repository = new AppealRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->certificates);
        unset($this->repository);
    }

    public function setCertificates(array $certificates) : void
    {
        $this->certificates = $certificates;
    }

    public function getCertificates() : array
    {
        return $this->certificates;
    }

    public function getRepository(): AppealRepository
    {
        return $this->repository;
    }
}
