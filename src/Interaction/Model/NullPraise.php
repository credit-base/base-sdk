<?php
namespace Base\Sdk\Interaction\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Sdk\Common\Model\NullOperateAbleTrait;
use Base\Sdk\Common\Model\NullModifyStatusAbleTrait;

class NullPraise extends Praise implements INull
{
    use NullModifyStatusAbleTrait, NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function accept() : bool
    {
        return $this->resourceNotExist();
    }

    public function publish() : bool
    {
        return $this->resourceNotExist();
    }

    public function unPublish() : bool
    {
        return $this->resourceNotExist();
    }
}
