<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Interaction\Repository\PraiseRepository;

class Praise extends Interaction
{
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->repository = new PraiseRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    public function getRepository(): PraiseRepository
    {
        return $this->repository;
    }
}
