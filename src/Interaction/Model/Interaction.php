<?php
namespace Base\Sdk\Interaction\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Model\ModifyStatusAbleTrait;
use Base\Sdk\Common\Model\OperateAbleTrait;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Member\Model\Member;

abstract class Interaction implements IObject, IModifyStatusAble, IOperateAble
{
    use Object, ModifyStatusAbleTrait, OperateAbleTrait;

    const TYPE = [
        'PERSONAL'=>1,
        'ENTERPRISE'=>2,
        'GOVERNMENT'=>3
    ];

    const ACCEPT_STATUS = [
        'PENDING'=>0,
        'ACCEPTING'=>1,
        'COMPLETE'=>2,
    ];

    const STATUS = [
        'NORMAL' => 0,
        'REVOKED' => -2,
        'PUBLISH'=> 2
    ];

    private $id;
    /**
     * [$title 标题]
     * @var string
     */
    private $title;
    /**
     * [$content 内容]
     * @var string
     */
    private $content;
/**
     * [$name 反馈人真实姓名/企业名称]
     * @var string
     */
    private $name;
    /**
     * [$identify 统一社会信用代码/反馈人身份证号]
     * @var string
     */
    private $identify;
    /**
     * [$title 被反馈主体]
     * @var string
     */
    private $subject;
    /**
     * [$type 类型]
     * @var int
     */
    private $type;
    /**
     * [$contact 联系方式]
     * @var string
     */
    private $contact;
    /**
     * [$images 图片]
     * @var array
     */
    private $images;
    /**
     * [$acceptStatus 受理状态]
     * @var int
     */
    private $acceptStatus;
        /**
     * [$reply 回复信息]
     * @var Reply
     */
    private $reply;
    /**
     * [$member 前台用户]
     * @var Member
     */
    private $member;
    /**
     * [$acceptUserGroup 受理委办局]
     * @var UserGroup
     */
    private $acceptUserGroup;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->content = '';
        $this->name = '';
        $this->identify = '';
        $this->subject = '';
        $this->contact = '';
        $this->type = self::TYPE['PERSONAL'];
        $this->images = array();
        $this->acceptStatus = self::ACCEPT_STATUS['PENDING'];
        $this->acceptUserGroup = new UserGroup();
        $this->reply = new Reply();
        $this->member = Core::$container->has('member') ? Core::$container->get('member') : new Member();
        $this->status = self::STATUS['NORMAL'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->content);
        unset($this->name);
        unset($this->identify);
        unset($this->subject);
        unset($this->type);
        unset($this->status);
        unset($this->images);
        unset($this->contact);
        unset($this->acceptStatus);
        unset($this->reply);
        unset($this->member);
        unset($this->repository);
        unset($this->acceptUserGroup);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status =
            in_array($status, array_values(self::STATUS)) ? $status : self::STATUS['NORMAL'];
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setType(int $type): void
    {
        $this->type =
            in_array($type, array_values(self::TYPE)) ? $type : self::TYPE['PERSONAL'];
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setReply(Reply $reply) : void
    {
        $this->reply = $reply;
    }

    public function getReply() : Reply
    {
        return $this->reply;
    }

    public function setAcceptUserGroup(UserGroup $acceptUserGroup) : void
    {
        $this->acceptUserGroup = $acceptUserGroup;
    }

    public function getAcceptUserGroup() : UserGroup
    {
        return $this->acceptUserGroup;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setImages(array $images) : void
    {
        $this->images = $images;
    }

    public function getImages() : array
    {
        return $this->images;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setIdentify(string $identify): void
    {
        $this->identify = $identify;
    }

    public function getIdentify(): string
    {
        return $this->identify;
    }

    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setContact(string $contact): void
    {
        $this->contact = $contact;
    }

    public function getContact(): string
    {
        return $this->contact;
    }

    public function setAcceptStatus(int $acceptStatus): void
    {
        $this->acceptStatus = $acceptStatus;
    }

    public function getAcceptStatus(): int
    {
        return $this->acceptStatus;
    }

    protected function getIOperateAbleAdapter(): IOperateAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter
    {
        return $this->getRepository();
    }

    public function accept() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }

        return $this->getRepository()->accept($this);
    }

    public function publish() : bool
    {
        if (!$this->isAccept()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }

        return $this->getRepository()->publish($this);
    }

    protected function isPublish() : bool
    {
        return $this->getStatus() == self::STATUS['PUBLISH'];
    }

    protected function isAccept():bool
    {
        return $this->getAcceptStatus() == self::ACCEPT_STATUS['COMPLETE'];
    }

    protected function isPending():bool
    {
        return $this->getAcceptStatus() == self::ACCEPT_STATUS['PENDING'];
    }

    public function unPublish() : bool
    {
        if (!$this->isAccept() && !$this->isPublish()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }

        return $this->getRepository()->unPublish($this);
    }

    abstract protected function getRepository();
}
