<?php
namespace Base\Sdk\UserGroup\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\UserGroup\Model\Department;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;

use Base\Sdk\UserGroup\Command\AddDepartmentCommand;

class AddDepartmentCommandHandler implements ICommandHandler
{
    private $department;

    private $userGroupRepository;

    public function __construct()
    {
        $this->department = new Department();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->department);
    }

    protected function getDepartment() : Department
    {
        return $this->department;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }
    
    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ICommand $command)
    {
        if (!($command instanceof AddDepartmentCommand)) {
            throw new \InvalidArgumentException;
        }

        $department = $this->getDepartment();
        $department->setUserGroup(
            $this->getUserGroupRepository()->fetchOne($command->userGroupId)
        );
        $department->setName($command->name);

        return $department->add();
    }
}
