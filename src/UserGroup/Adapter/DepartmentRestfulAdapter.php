<?php
namespace Base\Sdk\UserGroup\Adapter;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Base\Sdk\UserGroup\Model\Department;
use Base\Sdk\UserGroup\Model\NullDepartment;
use Base\Sdk\UserGroup\Translator\DepartmentRestfulTranslator;

class DepartmentRestfulAdapter extends GuzzleAdapter implements IDepartmentAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
        'DEPARTMENT_LIST'=>[
            'fields' => [
                'departments'=>'name,updateTime,userGroup',
                'userGroups'=>'name'
            ],
            'include' => 'userGroup'
        ],
        'DEPARTMENT_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'userGroup'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new DepartmentRestfulTranslator();
        $this->resource = 'departments';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            100 => [
                'userGroupId'=>DEPARTMENT_USER_GROUP_IS_EMPTY
            ],
            101 => [
                'name'=>DEPARTMENT_NAME_FORMAT_ERROR
            ],
            103 => [
                'departmentName'=>DEPARTMENT_NAME_IS_UNIQUE
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }
    
    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullDepartment());
    }

    protected function addAction(Department $department) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $department,
            array('name', 'userGroup')
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($department);
            return true;
        }

        return false;
    }

    protected function editAction(Department $department) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $department,
            array('name')
        );

        $this->patch(
            $this->getResource().'/'.$department->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($department);
            return true;
        }

        return false;
    }
}
