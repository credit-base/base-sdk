<?php
namespace Base\Sdk\UserGroup\Adapter;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Model\NullUserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

class UserGroupRestfulAdapter extends GuzzleAdapter implements IUserGroupAdapter
{
    use FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'USERGROUP_LIST'=>[
            'fields' => []
        ],
        'USERGROUP_FETCH_ONE'=>[
            'fields' => []
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UserGroupRestfulTranslator();
        $this->resource = 'userGroups';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullUserGroup());
    }
}
