<?php
namespace Base\Sdk\UserGroup\Adapter;

use Base\Sdk\Common\Adapter\OperateAbleMockAdapterTrait;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Utils\MockUserGroupFactory;

class UserGroupMockAdapter implements IUserGroupAdapter
{
    use OperateAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockUserGroupFactory::generateUserGroup($id);
    }

    public function fetchList(array $ids) : array
    {
        $userGroupList = array();

        foreach ($ids as $id) {
            $userGroupList[] = MockUserGroupFactory::generateUserGroup($id);
        }

        return $userGroupList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockUserGroupFactory::generateUserGroup($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $userGroupList = array();

        foreach ($ids as $id) {
            $userGroupList[] = MockUserGroupFactory::generateUserGroup($id);
        }

        return $userGroupList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
