<?php
namespace Base\Sdk\UserGroup\Adapter;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

interface IDepartmentAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperateAbleAdapter
{

}
