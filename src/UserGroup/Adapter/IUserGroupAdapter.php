<?php
namespace Base\Sdk\UserGroup\Adapter;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IUserGroupAdapter extends IFetchAbleAdapter, IAsyncAdapter
{

}
