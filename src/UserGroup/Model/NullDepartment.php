<?php
namespace Base\Sdk\UserGroup\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Sdk\Common\Model\NullOperateAbleTrait;

class NullDepartment extends Department implements INull
{
    use NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
