<?php
namespace Base\Sdk\UserGroup\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Model\OperateAbleTrait;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Repository\DepartmentRepository;

class Department implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    const STATUS_NORMAL = 0; //正常
    
    private $id;

    private $name;

    private $userGroup;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->userGroup = new UserGroup();
        $this->updateTime = 0;
        $this->createTime = 0;
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
        $this->repository = new DepartmentRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->userGroup);
        unset($this->updateTime);
        unset($this->createTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setUserGroup(UserGroup $userGroup): void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup(): UserGroup
    {
        return $this->userGroup;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
    
    protected function getRepository(): DepartmentRepository
    {
        return $this->repository;
    }

    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }

    protected function nameIsExist(): bool
    {
        $sort = ['-updateTime'];

        $filter['unique'] = $this->getName();
        $filter['userGroup'] = $this->getUserGroup()->getId();
        
        if (!empty($this->getId())) {
            $filter['id'] = $this->getId();
        }

        list($count, $departmentList) = $this->getRepository()
            ->scenario(DepartmentRepository::LIST_MODEL_UN)->search($filter, $sort);

        unset($departmentList);

        if (!empty($count)) {
            Core::setLastError(DEPARTMENT_NAME_IS_UNIQUE);
            return false;
        }

        return true;
    }

    protected function addAction(): bool
    {
        return $this->nameIsExist() && $this->getRepository()->add($this);
    }

    protected function editAction(): bool
    {
        return $this->nameIsExist() && $this->getRepository()->edit($this);
    }
}
