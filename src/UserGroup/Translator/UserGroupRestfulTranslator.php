<?php
namespace Base\Sdk\UserGroup\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Model\NullUserGroup;

class UserGroupRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $userGroup = null)
    {
        return $this->translateToObject($expression, $userGroup);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $userGroup = null)
    {
        if (empty($expression)) {
            return new NullUserGroup();
        }

        if ($userGroup == null) {
            $userGroup = new UserGroup();
        }
        
        $data = $expression['data'];

        $id = $data['id'];
        $userGroup->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $userGroup->setName($attributes['name']);
        }
        if (isset($attributes['shortName'])) {
            $userGroup->setShortName($attributes['shortName']);
        }
        if (isset($attributes['updateTime'])) {
            $userGroup->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['createTime'])) {
            $userGroup->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['status'])) {
            $userGroup->setStatus($attributes['status']);
        }

        return $userGroup;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($userGroup, array $keys = array())
    {
        if (!$userGroup instanceof UserGroup) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'shortName',
                'updateTime',
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'userGroups'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $userGroup->getId();
        }
        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $userGroup->getName();
        }
        if (in_array('shortName', $keys)) {
            $attributes['shortName'] = $userGroup->getShortName();
        }
        if (in_array('updateTime', $keys)) {
            $attributes['updateTime'] = $userGroup->getUpdateTime();
        }

        $expression['data']['attributes'] = $attributes;
        
        return $expression;
    }
}
