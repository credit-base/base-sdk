<?php
namespace Base\Sdk\UserGroup\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\UserGroup\Model\Department;
use Base\Sdk\UserGroup\Model\NullDepartment;

use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

class DepartmentRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $department = null)
    {
        return $this->translateToObject($expression, $department);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $department = null)
    {
        if (empty($expression)) {
            return new NullDepartment();
        }

        if ($department == null) {
            $department = new Department();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $department->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $department->setName($attributes['name']);
        }
        
        if (isset($attributes['updateTime'])) {
            $department->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['createTime'])) {
            $department->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['status'])) {
            $department->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['userGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['userGroup']['data']);
            $department->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }

        return $department;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($department, array $keys = array())
    {
        if (!$department instanceof Department) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'userGroup'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'departments'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $department->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $department->getName();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('userGroup', $keys)) {
            $expression['data']['relationships']['userGroup']['data'] = [
                [
                    "type" => "userGroups",
                    "id" => $department->getUserGroup()->getId()
                ]
            ];
        }
        
        return $expression;
    }
}
