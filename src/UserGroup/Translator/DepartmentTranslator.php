<?php
namespace Base\Sdk\UserGroup\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\UserGroup\Model\Department;
use Base\Sdk\UserGroup\Model\NullDepartment;

class DepartmentTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $department = null)
    {
        unset($department);
        unset($expression);
        return new NullDepartment();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($department, array $keys = array())
    {
        if (!$department instanceof Department) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'userGroup',
                'updateTime',
                'createTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($department->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $department->getName();
        }
        if (in_array('userGroup', $keys)) {
            $expression['userGroup'] = [
                'id' => marmot_encode($department->getUserGroup()->getId()),
                'name' => $department->getUserGroup()->getName(),
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $department->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H点i分', $department->getUpdateTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $department->getCreateTime();
        }
        return $expression;
    }
}
