<?php
namespace Base\Sdk\UserGroup\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Base\Sdk\UserGroup\Adapter\IDepartmentAdapter;
use Base\Sdk\UserGroup\Adapter\DepartmentMockAdapter;
use Base\Sdk\UserGroup\Adapter\DepartmentRestfulAdapter;

class DepartmentRepository extends Repository implements IDepartmentAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperateAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'DEPARTMENT_LIST';
    const FETCH_ONE_MODEL_UN = 'DEPARTMENT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new DepartmentRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new DepartmentMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
