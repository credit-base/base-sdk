<?php
namespace Base\Sdk\UserGroup\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\FetchRepositoryTrait;

use Base\Sdk\UserGroup\Adapter\IUserGroupAdapter;
use Base\Sdk\UserGroup\Adapter\UserGroupMockAdapter;
use Base\Sdk\UserGroup\Adapter\UserGroupRestfulAdapter;

class UserGroupRepository extends Repository implements IUserGroupAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'USERGROUP_LIST';
    const FETCH_ONE_MODEL_UN = 'USERGROUP_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new UserGroupRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new UserGroupMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
