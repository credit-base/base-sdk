<?php
namespace Base\Sdk\WebsiteCustomize\WidgetRules;

use Respect\Validation\Validator as V;

use Marmot\Core;

use Base\Sdk\Common\Model\IEnableAble;

use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
*/
class WebsiteCustomizeWidgetRules
{
    public function category($category, $pointer = 'category') : bool
    {
        if (!in_array($category, WebsiteCustomize::CATEGORY)) {
            Core::setLastError(CATEGORY_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function status($status, $pointer = 'status') : bool
    {
        if (!V::numeric()->validate($status) || !in_array($status, WebsiteCustomize::STATUS)) {
            Core::setLastError(STATUS_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    
    
    const CONTENT_IMAGE_TYPE = array(
        'FORMAT_ONE' => 1,
        'FORMAT_TWO' => 2
    );

    const CONTENT_IMAGE_VALIDATE = array(
        self::CONTENT_IMAGE_TYPE['FORMAT_ONE'] => 'validateImageFormatOneExtension',
        self::CONTENT_IMAGE_TYPE['FORMAT_TWO'] => 'validateImageFormatTwoExtension'
    );
    
    const CONTENT_NAME_TYPE = array(
        'FORMAT_ONE' => 1,
        'FORMAT_TWO' => 2,
        'FORMAT_THREE' => 3,
        'FORMAT_FOUR' => 4
    );

    const CONTENT_NAME_VALIDATE = array(
        self::CONTENT_NAME_TYPE['FORMAT_ONE'] => 'validateNameFormatOneExtension',
        self::CONTENT_NAME_TYPE['FORMAT_TWO'] => 'validateNameFormatTwoExtension',
        self::CONTENT_NAME_TYPE['FORMAT_THREE'] => 'validateNameFormatThreeExtension',
        self::CONTENT_NAME_TYPE['FORMAT_FOUR'] => 'validateNameFormatFourExtension'
    );
    
    const CONTENT_URL_TYPE = array(
        'FORMAT_ONE' => 1,
        'FORMAT_TWO' => 2
    );

    //内容分类
    const TYPE = array(
        'NULL' => 0, //默认
        'PV' => 1, //访问量统计
        'FONT' => 2, //字体
        'ACCESSIBLE_READING' => 3, //无障碍阅读
        'SIGN_IN' => 4, //登录
        'SIGN_UP' => 5, //注册
        'USER_CENTER' => 6, //用户中心
        'FOR_THE_RECORD' => 7, //备案号
        'PUBLIC_NETWORK_SECURITY' => 8, //公网安备
        'WEBSITE_IDENTIFY' => 9, //网站标识码
    );
    
    //头部搜索栏数组键名
    const HEADER_SEARCH_KEYS = array(
        'creditInformation', 'personalCreditInformation', 'unifiedSocialCreditCode', 'legalPerson', 'newsTitle'
    );

    //内容图片验证
    protected function validateContentImage($type, $image) : bool
    {
        if (!V::arrayType()->validate($image)) {
            return false;
        }

        if (!isset($image['identify']) || !isset($image['name'])) {
            return false;
        }
        
        $validateImage = isset(self::CONTENT_IMAGE_VALIDATE[$type]) ? self::CONTENT_IMAGE_VALIDATE[$type] : '';

        if (!method_exists($this, $validateImage)) {
            return false;
        }
        
        return $this->$validateImage($image['identify']);
    }

    const  IMAGE_TYPE = ['png','jpg','jpeg'];

    private function validateImageFormatOneExtension($image) : bool
    {
        foreach (self::IMAGE_TYPE as $value) {
            if (V::extension($value)->validate($image)) {
                return true;
            }
        }
        return false;
    }

    /**
     * png图片格式验证
     */
    private function validateImageFormatTwoExtension($image) : bool
    {
        if (!V::extension('png')->validate($image)) {
            return false;
        }

        return true;
    }

    //内容状态验证
    protected function validateContentStatus($status) : bool
    {
        if (!V::numeric()->validate($status) || !in_array($status, IEnableAble::STATUS)) {
            return false;
        }

        return true;
    }

    //内容类型验证
    protected function validateContentType($type) : bool
    {
        if (!V::numeric()->validate($type) || !in_array($type, self::TYPE)) {
            return false;
        }

        return true;
    }

    //内容名称验证
    protected function validateContentName($type, $name) : bool
    {
        $validateName = isset(self::CONTENT_NAME_VALIDATE[$type]) ? self::CONTENT_NAME_VALIDATE[$type] : '';

        if (!method_exists($this, $validateName)) {
            return false;
        }
        
        return $this->$validateName($name);
    }

    /**
     * 内容名称验证
     * 长度限制为2-10
     */
    const FORMAT_ONE_NAME_MIN_LENGTH = 2;
    const FORMAT_ONE_NAME_MAX_LENGTH = 10;

    private function validateNameFormatOneExtension($name) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_ONE_NAME_MIN_LENGTH,
            self::FORMAT_ONE_NAME_MAX_LENGTH
        )->validate($name)) {
            return false;
        }

        return true;
    }

    /**
     * 内容名称验证
     * 长度限制为2-3
     */
    const FORMAT_TWO_NAME_MIN_LENGTH = 2;
    const FORMAT_TWO_NAME_MAX_LENGTH = 3;

    private function validateNameFormatTwoExtension($name) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_TWO_NAME_MIN_LENGTH,
            self::FORMAT_TWO_NAME_MAX_LENGTH
        )->validate($name)) {
            return false;
        }

        return true;
    }

    /**
     * 内容名称验证
     * 长度限制为2-8
     */
    const FORMAT_THREE_NAME_MIN_LENGTH = 2;
    const FORMAT_THREE_NAME_MAX_LENGTH = 8;

    private function validateNameFormatThreeExtension($name) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_THREE_NAME_MIN_LENGTH,
            self::FORMAT_THREE_NAME_MAX_LENGTH
        )->validate($name)) {
            return false;
        }

        return true;
    }

    /**
     * 内容名称验证
     * 长度限制为2-20
     */
    const FORMAT_FOUR_NAME_MIN_LENGTH = 2;
    const FORMAT_FOUR_NAME_MAX_LENGTH = 20;

    private function validateNameFormatFourExtension($name) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_FOUR_NAME_MIN_LENGTH,
            self::FORMAT_FOUR_NAME_MAX_LENGTH
        )->validate($name)) {
            return false;
        }

        return true;
    }
    
    /**
     * 内容url验证
     */
    const CONTENT_URL_VALIDATE = array(
        self::CONTENT_URL_TYPE['FORMAT_ONE'] => 'validateUrlFormatOneExtension',
        self::CONTENT_URL_TYPE['FORMAT_TWO'] => 'validateUrlFormatTwoExtension'
    );

    protected function validateContentUrl($type, $url) : bool
    {
        $validateUrl = isset(self::CONTENT_URL_VALIDATE[$type]) ? self::CONTENT_URL_VALIDATE[$type] : '';
      
        if (!method_exists($this, $validateUrl)) {
            return false;
        }
        
        return $this->$validateUrl($url);
    }

    const FORMAT_ONE_URL_MIN_LENGTH = 0;
    const FORMAT_ONE_URL_MAX_LENGTH = 200;

    private function validateUrlFormatOneExtension($url) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_ONE_URL_MIN_LENGTH,
            self::FORMAT_ONE_URL_MAX_LENGTH
        )->validate($url)) {
            return false;
        }

        return true;
    }

    const FORMAT_TWO_URL_MIN_LENGTH = 1;
    const FORMAT_TWO_URL_MAX_LENGTH = 200;

    private function validateUrlFormatTwoExtension($url) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::FORMAT_TWO_URL_MIN_LENGTH,
            self::FORMAT_TWO_URL_MAX_LENGTH
        )->validate($url)) {
            return false;
        }

        return true;
    }

    //背景置灰
    const MEMORIAL_STATUS = array(
        'NO' => 0, //否 默认
        'YES' => 1 // 是
    );

    const VALIDATE_CONTENT = array(
        WebsiteCustomize::CATEGORY['HOME_PAGE'] => 'validateHomePageContent',
    );

    public function content($category, $content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(CONTENT_FORMAT_ERROR, array('pointer'=>'content'));
            return false;
        }
       
        $validateContent = isset(self::VALIDATE_CONTENT[$category]) ? self::VALIDATE_CONTENT[$category] : '';
      
        if (!method_exists($this, $validateContent)) {
            Core::setLastError(CONTENT_KEYS_FORMAT_ERROR, array('pointer'=>'content'));
            return false;
        }

        return $this->$validateContent($content);
    }

    protected function validateHomePageContent(array $content) : bool
    {
        return $this->homePageContentKeysExist($content)
            && $this->homePageContentKeysRequired($content)
            && $this->homePageFormat($content);
    }

    /**
     * 验证内容数组的全部键名存在
     */

    //内容数组的全部键名
    const HOME_PAGE_CONTENT_KEYS = array(
        'memorialStatus', 'theme', 'headerBarLeft', 'headerBarRight', 'headerBg', 'logo', 'headerSearch', 'nav',
        'centerDialog', 'animateWindow', 'leftFloatCard', 'frameWindow', 'rightToolBar', 'specialColumn',
        'relatedLinks', 'footerBanner', 'organizationGroup', 'silhouette', 'partyAndGovernmentOrgans',
        'governmentErrorCorrection', 'footerNav', 'footerTwo', 'footerThree'
    );

    protected function homePageContentKeysExist(array $content) : bool
    {
        $homePageContentKeys = array_keys($content);
        foreach ($homePageContentKeys as $value) {
            if (!in_array($value, self::HOME_PAGE_CONTENT_KEYS)) {
                Core::setLastError(CONTENT_KEYS_FORMAT_ERROR, array('pointer'=>'content-keys'));
                return false;
            }
        }
        
        return true;
    }

    /**
     * 验证数组中必填项存在
     */
    //内容数组中必填项的键名
    const HOME_PAGE_CONTENT_REQUIRED_KEYS = array(
        'memorialStatus', 'theme', 'headerBarLeft', 'headerBarRight', 'headerBg', 'logo', 'headerSearch', 'nav',
        'rightToolBar', 'footerNav', 'footerThree'
    );

    protected function homePageContentKeysRequired(array $content) : bool
    {
        $homePageContentKeys = array_keys($content);
        $requiredKeys = self::HOME_PAGE_CONTENT_REQUIRED_KEYS;

        foreach ($requiredKeys as $value) {
            if (!in_array($value, $homePageContentKeys)) {
                Core::setLastError(CONTENT_REQUIRED_KEYS_FORMAT_ERROR, array('pointer'=>'content-requiredKeys'));
                return false;
            }
        }
        
        return true;
    }

    //验证数组格式
    protected function homePageFormat(array $content) : bool
    {
        foreach ($content as $key => $value) {
            if (!$this->validateHomePageContentFragment($key, $value)) {
                return false;
            }
        }
       
        return true;
    }

    /**
     * 数组内容验证
     */

    //内容的各数组的校验方法
    const VALIDATE_HOME_PAGE_CONTENT_FRAGMENT = array(
        'memorialStatus' => 'validateMemorialStatus',
        'theme' => 'validateTheme',
        'headerBarLeft' => 'validateHeaderBarLeft',
        'headerBarRight' => 'validateHeaderBarRight',
        'headerBg' => 'validateHeaderBg',
        'logo' => 'validateLogo',
        'headerSearch' => 'validateHeaderSearch',
        'nav' => 'validateNav',
        'centerDialog' => 'validateCenterDialog',
        'animateWindow' => 'validateAnimateWindow',
        'leftFloatCard' => 'validateLeftFloatCard',
        'frameWindow' => 'validateFrameWindow',
        'rightToolBar' => 'validateRightToolBar',
        'specialColumn' => 'validateSpecialColumn',
        'relatedLinks' => 'validateRelatedLinks',
        'footerBanner' => 'validateFooterBanner',
        'organizationGroup' => 'validateOrganizationGroup',
        'silhouette' => 'validateSilhouette',
        'partyAndGovernmentOrgans' => 'validatePartyAndGovernmentOrgans',
        'governmentErrorCorrection' => 'validateGovernmentErrorCorrection',
        'footerNav' => 'validateFooterNav',
        'footerTwo' => 'validateFooterTwo',
        'footerThree' => 'validateFooterThree',
    );

    protected function validateHomePageContentFragment(string $key, $content) : bool
    {
        $validateContent = isset(
            self::VALIDATE_HOME_PAGE_CONTENT_FRAGMENT[$key]
        ) ? self::VALIDATE_HOME_PAGE_CONTENT_FRAGMENT[$key] : '';
       
        if (!method_exists($this, $validateContent)) {
            Core::setLastError(CONTENT_FORMAT_ERROR, array('pointer' => 'content'));
            return false;
        }

        return $this->$validateContent($content);
    }

    //验证背景置灰状态
    protected function validateMemorialStatus($content) : bool
    {
        if (!V::numeric()->validate($content) || !in_array($content, self::MEMORIAL_STATUS)) {
            Core::setLastError(MEMORIAL_STATUS_FORMAT_ERROR, array('pointer' => 'memorialStatus'));
            return false;
        }

        return true;
    }

    //验证页面主题标识
    protected function validateTheme($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(THEME_FORMAT_ERROR, array('pointer'=>'theme'));
            return false;
        }

        if (!isset($content['color']) || !isset($content['image'])) {
            Core::setLastError(THEME_FORMAT_ERROR, array('pointer'=>'theme'));
            return false;
        }

        if (!is_string($content['color'])) {
            Core::setLastError(THEME_COLOR_FORMAT_ERROR, array('pointer'=>'theme-color'));
            return false;
        }

        if (!$this->validateContentImage(
            self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
            $content['image']
        )
        ) {
            Core::setLastError(THEME_IMAGE_FORMAT_ERROR, array('pointer'=>'theme-image'));
            return false;
        }

        return true;
    }

    //头部左侧位置标识页面主题标识
    protected function validateHeaderBarLeft($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(HEADER_BAR_LEFT_FORMAT_ERROR, array('pointer'=>'headerBarLeft'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['name']) || !isset($value['status']) || !isset($value['url']) || !isset($value['type'])) {
                Core::setLastError(HEADER_BAR_LEFT_FORMAT_ERROR, array('pointer'=>'headerBarLeft'));
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['name']
            )
            ) {
                Core::setLastError(HEADER_BAR_LEFT_NAME_FORMAT_ERROR, array('pointer'=>'headerBarLeft-name'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(HEADER_BAR_LEFT_STATUS_FORMAT_ERROR, array('pointer'=>'headerBarLeft-status'));
                return false;
            }

            if (!$this->validateContentType($value['type'])) {
                Core::setLastError(HEADER_BAR_LEFT_TYPE_FORMAT_ERROR, array('pointer'=>'headerBarLeft-type'));
                return false;
            }

            if (!empty($value['url'])) {
                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_ONE'],
                    $value['url']
                )) {
                    Core::setLastError(HEADER_BAR_LEFT_URL_FORMAT_ERROR, array('pointer'=>'headerBarLeft-url'));
                    return false;
                }
            }

            if ($value['type'] == self::TYPE['NULL']) {
                if (empty($value['url'])) {
                    Core::setLastError(HEADER_BAR_LEFT_URL_FORMAT_ERROR, array('pointer'=>'headerBarLeft-url'));
                    return false;
                }
            }
        }

        return true;
    }

    //头部右侧位置标识页面主题标识
    protected function validateHeaderBarRight($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(HEADER_BAR_RIGHT_FORMAT_ERROR, array('pointer'=>'headerBarRight'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['name']) || !isset($value['status']) || !isset($value['url']) || !isset($value['type'])) {
                Core::setLastError(HEADER_BAR_RIGHT_FORMAT_ERROR, array('pointer'=>'headerBarRight'));
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['name']
            )
            ) {
                Core::setLastError(HEADER_BAR_RIGHT_NAME_FORMAT_ERROR, array('pointer'=>'headerBarRight-name'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(HEADER_BAR_RIGHT_STATUS_FORMAT_ERROR, array('pointer'=>'headerBarRight-status'));
                return false;
            }

            if (!$this->validateContentType($value['type'])) {
                Core::setLastError(HEADER_BAR_RIGHT_TYPE_FORMAT_ERROR, array('pointer'=>'headerBarRight-type'));
                return false;
            }

            if (!empty($value['url'])) {
                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_ONE'],
                    $value['url']
                )) {
                    Core::setLastError(HEADER_BAR_RIGHT_URL_FORMAT_ERROR, array('pointer'=>'headerBarRight-url'));
                    return false;
                }
            }

            if ($value['type'] == self::TYPE['NULL']) {
                if (empty($value['url'])) {
                    Core::setLastError(HEADER_BAR_RIGHT_URL_FORMAT_ERROR, array('pointer'=>'headerBarRight-url'));
                    return false;
                }
            }
        }

        return true;
    }

    //网站header背景
    protected function validateHeaderBg($content) : bool
    {
        if (!$this->validateContentImage(self::CONTENT_IMAGE_TYPE['FORMAT_TWO'], $content)) {
            Core::setLastError(HEADER_BG_FORMAT_ERROR, array('pointer'=>'headerBg'));
            return false;
        }
        return true;
    }
    
    //网站logo
    protected function validateLogo($content) : bool
    {
        if (!$this->validateContentImage(self::CONTENT_IMAGE_TYPE['FORMAT_TWO'], $content)) {
            Core::setLastError(LOGO_FORMAT_ERROR, array('pointer'=>'logo'));
            return false;
        }
        return true;
    }

    //头部搜索栏位置标识
    protected function validateHeaderSearch($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(HEADER_SEARCH_FORMAT_ERROR, array('pointer'=>'headerSearch'));
            return false;
        }

        foreach ($content as $key => $value) {
            if (!in_array($key, self::HEADER_SEARCH_KEYS)) {
                Core::setLastError(HEADER_SEARCH_FORMAT_ERROR, array('pointer'=>'headerSearch'));
                return false;
            }

            if (!isset($value['status']) || !$this->validateContentStatus($value['status'], 'headerSearch-status')) {
                Core::setLastError(HEADER_SEARCH_STATUS_FORMAT_ERROR, array('pointer'=>'headerSearch-status'));
                return false;
            }
        }
        
        return true;
    }

    //头部导航位置标识
    const NAV_COUNT = 16;

    protected function validateNav($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(NAV_FORMAT_ERROR, array('pointer'=>'nav'));
            return false;
        }

        if (count($content) > self::NAV_COUNT) {
            Core::setLastError(NAV_COUNT_FORMAT_ERROR, array('pointer'=>'nav-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['name']) ||
                !isset($value['status']) ||
                !isset($value['url']) ||
                !isset($value['nav']) ||
                !isset($value['image'])) {
                Core::setLastError(NAV_FORMAT_ERROR, array('pointer'=>'nav'));
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_THREE'],
                $value['name']
            )
            ) {
                Core::setLastError(NAV_NAME_FORMAT_ERROR, array('pointer'=>'nav-name'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(NAV_STATUS_FORMAT_ERROR, array('pointer'=>'nav-status'));
                return false;
            }

            if (!$this->validateNavContentNav($value['nav'])) {
                return false;
            }

            if (!$this->validateContentUrl(self::CONTENT_URL_TYPE['FORMAT_TWO'], $value['url'])) {
                Core::setLastError(NAV_URL_FORMAT_ERROR, array('pointer'=>'nav-url'));
                return false;
            }

            if (!empty($value['image'])) {
                if (!$this->validateContentImage(
                    self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
                    $value['image']
                )) {
                    Core::setLastError(NAV_IMAGE_FORMAT_ERROR, array('pointer'=>'nav-image'));
                    return false;
                }
            }
        }
        
        return true;
    }

    protected function validateNavContentNav($status, $pointer = 'nav-nav') : bool
    {
        if (!V::numeric()->validate($status) || !in_array($status, NAV)) {
            Core::setLastError(STATUS_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    //中间弹框位置标识
    protected function validateCenterDialog($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(CENTER_DIALOG_FORMAT_ERROR, array('pointer'=>'centerDialog'));
            return false;
        }

        if (!isset($content['status']) || !isset($content['image']) || !isset($content['url'])) {
            Core::setLastError(CENTER_DIALOG_FORMAT_ERROR, array('pointer'=>'centerDialog'));
            return false;
        }

        if (!$this->validateContentStatus($content['status'])) {
            Core::setLastError(CENTER_DIALOG_STATUS_FORMAT_ERROR, array('pointer'=>'centerDialog-status'));
            return false;
        }

        if (!$this->validateContentImage(
            self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
            $content['image']
        )) {
            Core::setLastError(CENTER_DIALOG_IMAGE_FORMAT_ERROR, array('pointer'=>'centerDialog-image'));
            return false;
        }

        if (!empty($content['url'])) {
            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $content['url']
            )) {
                Core::setLastError(CENTER_DIALOG_URL_FORMAT_ERROR, array('pointer'=>'centerDialog-url'));
                return false;
            }
        }

        return true;
    }

    //飘窗位置标识
    const ANIMATE_WINDOW_COUNT = 2;
    
    protected function validateAnimateWindow($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(ANIMATE_WINDOW_FORMAT_ERROR, array('pointer'=>'animateWindow'));
            return false;
        }

        if (count($content) > self::ANIMATE_WINDOW_COUNT) {
            Core::setLastError(ANIMATE_WINDOW_COUNT_FORMAT_ERROR, array('pointer'=>'animateWindow-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['image']) || !isset($value['url'])) {
                Core::setLastError(ANIMATE_WINDOW_FORMAT_ERROR, array('pointer'=>'animateWindow'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(ANIMATE_WINDOW_STATUS_FORMAT_ERROR, array('pointer'=>'animateWindow-status'));
                return false;
            }

            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                $value['image']
            )) {
                Core::setLastError(ANIMATE_WINDOW_IMAGE_FORMAT_ERROR, array('pointer'=>'animateWindow-image'));
                return false;
            }

            if (!empty($value['url'])) {
                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_ONE'],
                    $value['url']
                )) {
                    Core::setLastError(ANIMATE_WINDOW_URL_FORMAT_ERROR, array('pointer'=>'animateWindow-url'));
                    return false;
                }
            }
        }

        return true;
    }

    //左侧浮动卡片位置标识
    const LEFT_FLOAT_CARD_COUNT = 5;
    
    protected function validateLeftFloatCard($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(LEFT_FLOAT_CARD_FORMAT_ERROR, array('pointer'=>'leftFloatCard'));
            return false;
        }

        if (count($content) > self::LEFT_FLOAT_CARD_COUNT) {
            Core::setLastError(LEFT_FLOAT_CARD_COUNT_FORMAT_ERROR, array('pointer'=>'leftFloatCard-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['image']) || !isset($value['url'])) {
                Core::setLastError(LEFT_FLOAT_CARD_FORMAT_ERROR, array('pointer'=>'leftFloatCard'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(LEFT_FLOAT_CARD_STATUS_FORMAT_ERROR, array('pointer'=>'leftFloatCard-status'));
                return false;
            }

            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
                $value['image']
            )) {
                Core::setLastError(LEFT_FLOAT_CARD_IMAGE_FORMAT_ERROR, array('pointer'=>'leftFloatCard-image'));
                return false;
            }

            if (!empty($value['url'])) {
                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_ONE'],
                    $value['url']
                )) {
                    Core::setLastError(LEFT_FLOAT_CARD_URL_FORMAT_ERROR, array('pointer'=>'leftFloatCard-url'));
                    return false;
                }
            }
        }

        return true;
    }

    //外链窗口
    protected function validateFrameWindow($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(FRAME_WINDOW_FORMAT_ERROR, array('pointer'=>'frameWindow'));
            return false;
        }

        if (!isset($content['status']) || !isset($content['url'])) {
            Core::setLastError(FRAME_WINDOW_FORMAT_ERROR, array('pointer'=>'frameWindow'));
            return false;
        }

        if (!$this->validateContentStatus($content['status'])) {
            Core::setLastError(FRAME_WINDOW_STATUS_FORMAT_ERROR, array('pointer'=>'frameWindow-status'));
            return false;
        }

        if (!$this->validateContentUrl(
            self::CONTENT_URL_TYPE['FORMAT_TWO'],
            $content['url']
        )) {
            Core::setLastError(FRAME_WINDOW_URL_FORMAT_ERROR, array('pointer'=>'frameWindow-url'));
            return false;
        }

        return true;
    }

    //右侧工具栏分类
    const RIGHT_TOOL_BAR_CATEGORY = array(
        'IMAGE' => 1, //图片
        'LINK' => 2, //链接
        'WRITTEN_WORDS' => 3 //文字
    );
    
    const RIGHT_TOOL_BAR_CATEGORY_VALIDATE = array(
        self::RIGHT_TOOL_BAR_CATEGORY['IMAGE'] => 'validateRightToolBarImage',
        self::RIGHT_TOOL_BAR_CATEGORY['LINK'] => 'validateRightToolBarLink',
        self::RIGHT_TOOL_BAR_CATEGORY['WRITTEN_WORDS'] => 'validateRightToolBarWrittenWords'
    );

    const RIGHT_TOOL_BAR_COUNT = 8;
    //右侧工具栏
    protected function validateRightToolBar($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(RIGHT_TOOL_BAR_FORMAT_ERROR, array('pointer'=>'rightToolBar'));
            return false;
        }

        if (count($content) > self::RIGHT_TOOL_BAR_COUNT) {
            Core::setLastError(RIGHT_TOOL_BAR_COUNT_FORMAT_ERROR, array('pointer'=>'rightToolBar-count'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['name']) || !isset($value['category'])) {
                Core::setLastError(RIGHT_TOOL_BAR_FORMAT_ERROR, array('pointer'=>'rightToolBar'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'], 'rightToolBar-status')) {
                Core::setLastError(RIGHT_TOOL_BAR_STATUS_FORMAT_ERROR, array('pointer'=>'rightToolBar-status'));
                return false;
            }

            if (!$this->validateContentCategory($value['category'])) {
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_TWO'],
                $value['name']
            )) {
                Core::setLastError(RIGHT_TOOL_BAR_NAME_FORMAT_ERROR, array('pointer'=>'rightToolBar-name'));
                return false;
            }
            
            $validate = isset(
                self::RIGHT_TOOL_BAR_CATEGORY_VALIDATE[$value['category']]
            ) ? self::RIGHT_TOOL_BAR_CATEGORY_VALIDATE[$value['category']] : '';
          
            if (!method_exists($this, $validate) || !$this->$validate($value)) {
                return false;
            }
        }
     
        return true;
    }

    //内容右侧工具栏分类验证
    private function validateContentCategory($category) : bool
    {
        if (!V::numeric()->validate($category) || !in_array($category, self::RIGHT_TOOL_BAR_CATEGORY)) {
            Core::setLastError(RIGHT_TOOL_BAR_CATEGORY_FORMAT_ERROR, array('pointer' => 'rightToolBar-category'));
            return false;
        }

        return true;
    }

    const RIGHT_TOOL_BAR_IMAGE_COUNT = 2;

    private function validateRightToolBarImage($content) : bool
    {
        if (!isset($content['images'])) {
            Core::setLastError(RIGHT_TOOL_BAR_IMAGES_FORMAT_ERROR, array('pointer'=>'rightToolBar-images'));
            return false;
        }
        
        $images = $content['images'];

        if (!V::arrayType()->validate($images)) {
            Core::setLastError(RIGHT_TOOL_BAR_IMAGES_FORMAT_ERROR, array('pointer'=>'rightToolBar-images'));
            return false;
        }
        
        if (count($images) > self::RIGHT_TOOL_BAR_IMAGE_COUNT) {
            Core::setLastError(RIGHT_TOOL_BAR_IMAGES_COUNT_FORMAT_ERROR, array('pointer'=>'rightToolBar-images-count'));
            return false;
        }

        foreach ($images as $value) {
            if (!isset($value['title']) || !isset($value['image'])) {
                Core::setLastError(RIGHT_TOOL_BAR_IMAGES_FORMAT_ERROR, array('pointer'=>'rightToolBar-images'));
                return false;
            }
            
            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['title']
            )) {
                Core::setLastError(
                    RIGHT_TOOL_BAR_IMAGES_TITLE_FORMAT_ERROR,
                    array('pointer'=>'rightToolBar-images-title')
                );
                return false;
            }

            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                $value['image']
            )) {
                Core::setLastError(
                    RIGHT_TOOL_BAR_IMAGES_IMAGE_FORMAT_ERROR,
                    array('pointer'=>'rightToolBar-images-image')
                );
                return false;
            }
        }

        return true;
    }

    private function validateRightToolBarLink($content) : bool
    {
        if (!isset($content['url'])
            || !$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_TWO'],
                $content['url']
            )
        ) {
            Core::setLastError(
                RIGHT_TOOL_BAR_URL_FORMAT_ERROR,
                array('pointer'=>'rightToolBar-url')
            );
            return false;
        }

        return true;
    }

    private function validateRightToolBarWrittenWords($content) : bool
    {
        return isset($content['description'])
        && $this->validateRightToolBarDescription($content['description']);
    }

    const RIGHT_TOOL_BAR_DESCRIPTION_MIN_LENGTH = 0;
    const RIGHT_TOOL_BAR_DESCRIPTION_MAX_LENGTH = 200;

    private function validateRightToolBarDescription($description) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::RIGHT_TOOL_BAR_DESCRIPTION_MIN_LENGTH,
            self::RIGHT_TOOL_BAR_DESCRIPTION_MAX_LENGTH
        )->validate($description)) {
            Core::setLastError(
                RIGHT_TOOL_BAR_DESCRIPTION_FORMAT_ERROR,
                array('pointer' => 'rightToolBar-description')
            );
            return false;
        }

        return true;
    }

    //专题专栏
    const SPECIAL_COLUMN_COUNT = 2;
    protected function validateSpecialColumn($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(
                SPECIAL_COLUMN_FORMAT_ERROR,
                array('pointer'=>'specialColumn')
            );
            return false;
        }

        if (count($content) > self::SPECIAL_COLUMN_COUNT) {
            Core::setLastError(
                SPECIAL_COLUMN_COUNT_FORMAT_ERROR,
                array('pointer'=>'specialColumn-count')
            );
            return false;
        }
        
        foreach ($content as $value) {
            if (!isset($value['status'])
                || !isset($value['image'])
                || !isset($value['url'])
            ) {
                Core::setLastError(
                    SPECIAL_COLUMN_FORMAT_ERROR,
                    array('pointer'=>'specialColumn')
                );
                return false;
            }
          
            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(
                    SPECIAL_COLUMN_STATUS_FORMAT_ERROR,
                    array('pointer'=>'specialColumn-status')
                );
                return false;
            }
          
            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                $value['image']
            )) {
                Core::setLastError(
                    SPECIAL_COLUMN_IMAGE_FORMAT_ERROR,
                    array('pointer'=>'specialColumn-image')
                );
                return false;
            }
            
            if (!empty($value['url'])) {
                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_ONE'],
                    $value['url']
                )) {
                    Core::setLastError(
                        SPECIAL_COLUMN_URL_FORMAT_ERROR,
                        array('pointer'=>'specialColumn-url')
                    );
                    return false;
                }
            }
        }
        
        return true;
    }
    
    //相关链接
    const RELATED_LINKS_COUNT = 4;
    protected function validateRelatedLinks($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(
                RELATED_LINKS_FORMAT_ERROR,
                array('pointer'=>'relatedLinks')
            );
            return false;
        }

        if (count($content) > self::RELATED_LINKS_COUNT) {
            Core::setLastError(
                RELATED_LINKS_COUNT_FORMAT_ERROR,
                array('pointer'=>'relatedLinks-count')
            );
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status'])
                || !isset($value['name'])
                || !isset($value['items'])
            ) {
                Core::setLastError(
                    RELATED_LINKS_FORMAT_ERROR,
                    array('pointer'=>'relatedLinks')
                );
                return false;
            }

            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(
                    RELATED_LINKS_STATUS_FORMAT_ERROR,
                    array('pointer' => 'relatedLinks-status')
                );
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_FOUR'],
                $value['name']
            )) {
                Core::setLastError(
                    RELATED_LINKS_NAME_FORMAT_ERROR,
                    array('pointer' => 'relatedLinks-name')
                );
                return false;
            }

            if (!V::arrayType()->validate($value['items'])) {
                Core::setLastError(
                    RELATED_LINKS_ITEMS_FORMAT_ERROR,
                    array('pointer'=>'relatedLinks-items')
                );
                return false;
            }

            foreach ($value['items'] as $item) {
                if (!isset($item['status'])
                    || !isset($item['name'])
                    || !isset($item['url'])
                ) {
                    Core::setLastError(
                        RELATED_LINKS_ITEMS_FORMAT_ERROR,
                        array('pointer'=>'relatedLinks-items')
                    );
                    return false;
                }

                if (!$this->validateContentStatus($item['status'])) {
                    Core::setLastError(
                        RELATED_LINKS_ITEMS_STATUS_FORMAT_ERROR,
                        array('pointer'=>'relatedLinks-items-status')
                    );
                    return false;
                }

                if (!$this->validateContentName(
                    self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                    $item['name']
                )) {
                    Core::setLastError(
                        RELATED_LINKS_ITEMS_NAME_FORMAT_ERROR,
                        array('pointer'=> 'relatedLinks-items-name')
                    );
                    return false;
                }

                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_TWO'],
                    $item['url']
                )) {
                    Core::setLastError(
                        RELATED_LINKS_ITEMS_URL_FORMAT_ERROR,
                        array('pointer'=> 'relatedLinks-items-url')
                    );
                    return false;
                }
            }
        }

        return true;
    }
    
    //底部轮播
    const FOOTER_BANNER_COUNT = 10;
    protected function validateFooterBanner($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(FOOTER_BANNER_FORMAT_ERROR, array('pointer'=>'footerBanner'));
            return false;
        }

        if (count($content) > self::FOOTER_BANNER_COUNT) {
            Core::setLastError(
                FOOTER_BANNER_COUNT_FORMAT_ERROR,
                array('pointer'=>'footerBanner-count')
            );
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status'])
                || !isset($value['image'])
                || !isset($value['url'])
            ) {
                Core::setLastError(
                    FOOTER_BANNER_FORMAT_ERROR,
                    array('pointer'=>'footerBanner')
                );
                return false;
            }

            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(
                    FOOTER_BANNER_STATUS_FORMAT_ERROR,
                    array('pointer'=>'footerBanner-status')
                );
                return false;
            }

            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                $value['image']
            )) {
                Core::setLastError(
                    FOOTER_BANNER_IMAGE_FORMAT_ERROR,
                    array('pointer'=>'footerBanner-image')
                );
                return false;
            }

            if (!empty($value['url'])) {
                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_ONE'],
                    $value['url']
                )) {
                    Core::setLastError(
                        FOOTER_BANNER_URL_FORMAT_ERROR,
                        array('pointer'=>'footerBanner-url')
                    );
                    return false;
                }
            }
        }

        return true;
    }

    //组织列表
    const ORGANIZATION_GROUP_COUNT = 7;
    protected function validateOrganizationGroup($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(
                ORGANIZATION_GROUP_FORMAT_ERROR,
                array('pointer'=>'organizationGroup')
            );
            return false;
        }

        if (count($content) > self::ORGANIZATION_GROUP_COUNT) {
            Core::setLastError(
                ORGANIZATION_GROUP_COUNT_FORMAT_ERROR,
                array('pointer'=>'organizationGroup-count')
            );
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status'])
                || !isset($value['image'])
                || !isset($value['url'])
            ) {
                Core::setLastError(
                    ORGANIZATION_GROUP_FORMAT_ERROR,
                    array('pointer'=>'organizationGroup')
                );
                return false;
            }

            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(
                    ORGANIZATION_GROUP_STATUS_FORMAT_ERROR,
                    array('pointer'=>'organizationGroup-status')
                );
                return false;
            }

            if (!$this->validateContentImage(
                self::CONTENT_IMAGE_TYPE['FORMAT_ONE'],
                $value['image']
            )) {
                Core::setLastError(
                    ORGANIZATION_GROUP_IMAGE_FORMAT_ERROR,
                    array('pointer'=>'organizationGroup-image')
                );
                return false;
            }

            if (!empty($value['url'])) {
                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_ONE'],
                    $value['url']
                )) {
                    Core::setLastError(
                        ORGANIZATION_GROUP_URL_FORMAT_ERROR,
                        array('pointer'=>'organizationGroup-url')
                    );
                    return false;
                }
            }
        }

        return true;
    }

    //剪影
    protected function validateSilhouette($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(
                SILHOUETTE_FORMAT_ERROR,
                array('pointer'=>'silhouette')
            );
            return false;
        }

        if (!isset($content['status'])
            || !isset($content['image'])
            || !isset($content['description'])
        ) {
            Core::setLastError(
                SILHOUETTE_FORMAT_ERROR,
                array('pointer'=>'silhouette')
            );
            return false;
        }

        if (!$this->validateContentStatus($content['status'])) {
            Core::setLastError(
                SILHOUETTE_STATUS_FORMAT_ERROR,
                array('pointer'=>'silhouette-status')
            );
            return false;
        }

        if (!$this->validateContentImage(
            self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
            $content['image']
        )) {
            Core::setLastError(
                SILHOUETTE_IMAGE_FORMAT_ERROR,
                array('pointer'=>'silhouette-image')
            );
            return false;
        }

        if (!empty($content['description'])) {
            return $this->validateSilhouetteDescription($content['description']);
        }

        return true;
    }

    const SILHOUETTE_DESCRIPTION_MIN_LENGTH = 2;
    const SILHOUETTE_DESCRIPTION_MAX_LENGTH = 200;

    private function validateSilhouetteDescription($description) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::SILHOUETTE_DESCRIPTION_MIN_LENGTH,
            self::SILHOUETTE_DESCRIPTION_MAX_LENGTH
        )->validate($description)) {
            Core::setLastError(
                SILHOUETTE_DESCRIPTION_FORMAT_ERROR,
                array('pointer' => 'silhouette-description')
            );
            return false;
        }

        return true;
    }

    //党政机关
    protected function validatePartyAndGovernmentOrgans($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(
                PARTY_AND_GOVERNMENT_ORGANS_FORMAT_ERROR,
                array('pointer'=>'partyAndGovernmentOrgans')
            );
            return false;
        }

        if (!isset($content['status']) || !isset($content['image']) || !isset($content['url'])) {
            Core::setLastError(
                PARTY_AND_GOVERNMENT_ORGANS_FORMAT_ERROR,
                array('pointer'=>'partyAndGovernmentOrgans')
            );
            return false;
        }

        if (!$this->validateContentStatus($content['status'])) {
            Core::setLastError(
                PARTY_AND_GOVERNMENT_ORGANS_STATUS_FORMAT_ERROR,
                array('pointer'=>'partyAndGovernmentOrgans-status')
            );
            return false;
        }

        if (!$this->validateContentImage(
            self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
            $content['image']
        )) {
            Core::setLastError(
                PARTY_AND_GOVERNMENT_ORGANS_IMAGE_FORMAT_ERROR,
                array('pointer'=>'partyAndGovernmentOrgans-image')
            );
            return false;
        }

        if (!empty($content['url'])) {
            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $content['url']
            )) {
                Core::setLastError(
                    PARTY_AND_GOVERNMENT_ORGANS_URL_FORMAT_ERROR,
                    array('pointer'=>'partyAndGovernmentOrgans-url')
                );
                return false;
            }
        }

        return true;
    }

    //政府纠错
    protected function validateGovernmentErrorCorrection($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(
                GOVERNMENT_ERROR_CORRECTION_FORMAT_ERROR,
                array('pointer'=>'governmentErrorCorrection')
            );
            return false;
        }

        if (!isset($content['status']) || !isset($content['image']) || !isset($content['url'])) {
            Core::setLastError(
                GOVERNMENT_ERROR_CORRECTION_FORMAT_ERROR,
                array('pointer'=>'governmentErrorCorrection')
            );
            return false;
        }

        if (!$this->validateContentStatus($content['status'])) {
            Core::setLastError(
                GOVERNMENT_ERROR_CORRECTION_STATUS_FORMAT_ERROR,
                array('pointer'=>'governmentErrorCorrection-status')
            );
            return false;
        }

        if (!$this->validateContentImage(
            self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
            $content['image']
        )) {
            Core::setLastError(
                GOVERNMENT_ERROR_CORRECTION_IMAGE_FORMAT_ERROR,
                array('pointer'=>'governmentErrorCorrection-image')
            );
            return false;
        }

        if (!empty($content['url'])) {
            if (!$this->validateContentUrl(
                self::CONTENT_URL_TYPE['FORMAT_ONE'],
                $content['url']
            )) {
                Core::setLastError(
                    GOVERNMENT_ERROR_CORRECTION_URL_FORMAT_ERROR,
                    array('pointer'=>'governmentErrorCorrection-url')
                );
                return false;
            }
        }

        return true;
    }

    //页脚导航
    protected function validateFooterNav($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(FOOTER_NAV_FORMAT_ERROR, array('pointer'=>'footerNav'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) || !isset($value['name']) || !isset($value['url'])) {
                Core::setLastError(FOOTER_NAV_FORMAT_ERROR, array('pointer'=>'footerNav'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(
                    FOOTER_NAV_STATUS_FORMAT_ERROR,
                    array('pointer'=>'footerNav-status')
                );
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['name']
            )) {
                Core::setLastError(
                    FOOTER_NAV_NAME_FORMAT_ERROR,
                    array('pointer'=>'footerNav-name')
                );
                return false;
            }

            if (!empty($value['url'])) {
                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_ONE'],
                    $value['url']
                )) {
                    Core::setLastError(
                        FOOTER_NAV_URL_FORMAT_ERROR,
                        array('pointer'=>'footerNav-url')
                    );
                    return false;
                }
            }
        }

        return true;
    }

    //页脚第二行内容呈现
    protected function validateFooterTwo($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(FOOTER_TWO_FORMAT_ERROR, array('pointer'=>'footerTwo'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) ||
                !isset($value['name']) ||
                !isset($value['url']) ||
                !isset($value['description'])) {
                Core::setLastError(FOOTER_TWO_FORMAT_ERROR, array('pointer'=>'footerTwo'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(
                    FOOTER_TWO_STATUS_FORMAT_ERROR,
                    array('pointer'=>'footerTwo-status')
                );
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['name']
            )) {
                Core::setLastError(
                    FOOTER_TWO_NAME_FORMAT_ERROR,
                    array('pointer'=>'footerTwo-name')
                );
                return false;
            }

            if (!empty($value['url'])) {
                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_ONE'],
                    $value['url']
                )) {
                    Core::setLastError(
                        FOOTER_TWO_URL_FORMAT_ERROR,
                        array('pointer'=>'footerTwo-url')
                    );
                    return false;
                }
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_FOUR'],
                $value['description']
            )) {
                Core::setLastError(
                    FOOTER_TWO_DESCRIPTION_FORMAT_ERROR,
                    array('pointer'=>'footerTwo-description')
                );
                return false;
            }
        }

        return true;
    }

    //页脚第三行内容呈现
    protected function validateFooterThree($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(FOOTER_THREE_FORMAT_ERROR, array('pointer'=>'footerThree'));
            return false;
        }

        foreach ($content as $value) {
            if (!isset($value['status']) ||
                !isset($value['name']) ||
                !isset($value['url']) ||
                !isset($value['type']) ||
                !isset($value['description'])) {
                Core::setLastError(FOOTER_THREE_FORMAT_ERROR, array('pointer'=>'footerThree'));
                return false;
            }

            if (!$this->validateContentStatus($value['status'])) {
                Core::setLastError(
                    FOOTER_THREE_STATUS_FORMAT_ERROR,
                    array('pointer'=>'footerThree-status')
                );
                return false;
            }

            if (!$this->validateContentType($value['type'])) {
                Core::setLastError(
                    FOOTER_THREE_TYPE_FORMAT_ERROR,
                    array('pointer'=>'footerThree-type')
                );
                return false;
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_ONE'],
                $value['name']
            )) {
                Core::setLastError(
                    FOOTER_THREE_NAME_FORMAT_ERROR,
                    array('pointer'=>'footerThree-name')
                );
                return false;
            }

            if (!empty($value['url'])) {
                if (!$this->validateContentUrl(
                    self::CONTENT_URL_TYPE['FORMAT_ONE'],
                    $value['url']
                )) {
                    Core::setLastError(
                        FOOTER_THREE_URL_FORMAT_ERROR,
                        array('pointer'=>'footerThree-url')
                    );
                    return false;
                }
            }

            if (!$this->validateContentName(
                self::CONTENT_NAME_TYPE['FORMAT_FOUR'],
                $value['description']
            )) {
                Core::setLastError(
                    FOOTER_THREE_DESCRIPTION_FORMAT_ERROR,
                    array('pointer'=>'footerThree-description')
                );
                return false;
            }

            if ($value['type'] == self::TYPE['PUBLIC_NETWORK_SECURITY']) {
                if (isset($value['image']) && !empty($value['image'])) {
                    if (!$this->validateContentImage(
                        self::CONTENT_IMAGE_TYPE['FORMAT_TWO'],
                        $value['image']
                    )) {
                        Core::setLastError(
                            FOOTER_THREE_IMAGE_FORMAT_ERROR,
                            array('pointer'=>'footerThree-image')
                        );
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
