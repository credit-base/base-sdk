<?php
namespace Base\Sdk\WebsiteCustomize\Command\WebsiteCustomize;

use Marmot\Interfaces\ICommand;

class AddWebsiteCustomizeCommand implements ICommand
{
    public $id;
    
    public $category;

    public $content;

    public $status;
    
    public function __construct(
        int $category,
        int $status,
        array $content,
        int $id = 0
    ) {
        $this->category = $category;
        $this->content = $content;
        $this->status = $status;
        $this->id = $id;
    }
}
