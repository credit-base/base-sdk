<?php
namespace Base\Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Base\Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

use Marmot\Interfaces\ICommand;

trait WebsiteCustomizeCommandHandlerTrait
{
    private $websiteCustomize;

    private $repository;
    
    public function __construct()
    {
        $this->websiteCustomize = new WebsiteCustomize();
        $this->repository = new WebsiteCustomizeRepository();
    }

    public function __destruct()
    {
        unset($this->websiteCustomize);
        unset($this->repository);
    }

    protected function getWebsiteCustomize() : WebsiteCustomize
    {
        return $this->websiteCustomize;
    }

    protected function getRepository() : WebsiteCustomizeRepository
    {
        return $this->repository;
    }
    
    protected function fetchWebsiteCustomize(int $id) : WebsiteCustomize
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }
}
