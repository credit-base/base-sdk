<?php
namespace Base\Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand;

class PublishWebsiteCustomizeCommandHandler implements ICommandHandler
{
    use WebsiteCustomizeCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof PublishWebsiteCustomizeCommand)) {
            throw new \InvalidArgumentException;
        }

        $websiteCustomize = $this->fetchWebsiteCustomize($command->id);
       
        return $websiteCustomize->publish();
    }
}
