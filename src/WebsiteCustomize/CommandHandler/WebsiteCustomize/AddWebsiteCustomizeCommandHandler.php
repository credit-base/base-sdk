<?php
namespace Base\Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand;

class AddWebsiteCustomizeCommandHandler implements ICommandHandler
{
    use WebsiteCustomizeCommandHandlerTrait;

    public function executeAction($command)
    {
        if (!($command instanceof AddWebsiteCustomizeCommand)) {
            throw new \InvalidArgumentException;
        }

        $websiteCustomize = $this->getWebsiteCustomize();

        $websiteCustomize->setCategory($command->category);
        $websiteCustomize->setStatus($command->status);
        $websiteCustomize->setContent($command->content);
        
        return $websiteCustomize->add();
    }
}
