<?php
namespace Base\Sdk\WebsiteCustomize\Model;

use Marmot\Core;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Model\OperateAbleTrait;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

class WebsiteCustomize implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    const STATUS = [
        'UNPUBLISHED' => 0,
        'PUBLISHED' => 2
    ];

    const CATEGORY = [
        'HOME_PAGE' => 1,
    ];

    private $id;

    private $version;

    private $category;

    private $content;

    private $crew;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->version = '';
        $this->category = 0;
        $this->content = array();
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->status = self::STATUS['UNPUBLISHED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new WebsiteCustomizeRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->version);
        unset($this->category);
        unset($this->content);
        unset($this->crew);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function setCategory(int $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): int
    {
        return $this->category;
    }

    public function setVersion(string $version): void
    {
        $this->version = $version;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setContent(array $content): void
    {
        $this->content = $content;
    }

    public function getContent(): array
    {
        return $this->content;
    }

    public function getRepository() : WebsiteCustomizeRepository
    {
        return $this->repository;
    }

    /**
     * 新增编辑
     * @return [bool]
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 发布
     * @return [bool]
     */
    public function publish() : bool
    {
        return $this->getRepository()->publish($this);
    }
}
