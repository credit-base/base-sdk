<?php
namespace Base\Sdk\WebsiteCustomize\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Sdk\Common\Model\NullOperateAbleTrait;

class NullWebsiteCustomize extends WebsiteCustomize implements INull
{
    use NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function publish() : bool
    {
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
