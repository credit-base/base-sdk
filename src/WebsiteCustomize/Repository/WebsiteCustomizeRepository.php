<?php
namespace Base\Sdk\WebsiteCustomize\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\OperateAbleRepositoryTrait;
use Base\Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\TopAbleRepositoryTrait;

use Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize\WebsiteCustomizeRestfulAdapter;
use Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter;
use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;

class WebsiteCustomizeRepository extends Repository implements IWebsiteCustomizeAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        TopAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'WEBSITE_CUSTOMIZE_LIST';
    const FETCH_ONE_MODEL_UN = 'WEBSITE_CUSTOMIZE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new WebsiteCustomizeRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function publish(WebsiteCustomize $websiteCustomize) : bool
    {
        return $this->getAdapter()->publish($websiteCustomize);
    }

    public function customize(string $category)
    {
        return $this->getAdapter()->customize($category);
    }
}
