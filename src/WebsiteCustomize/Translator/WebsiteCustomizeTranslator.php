<?php
namespace Base\Sdk\WebsiteCustomize\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Base\Sdk\WebsiteCustomize\Model\NullWebsiteCustomize;

use Base\Sdk\Crew\Translator\CrewTranslator;

class WebsiteCustomizeTranslator implements ITranslator
{
    const STATUS_CN = [
        WebsiteCustomize::STATUS['UNPUBLISHED'] => '未发布',
        WebsiteCustomize::STATUS['PUBLISHED'] => '已发布',
    ];

    const STATUS_TAG_TYPE = [
        WebsiteCustomize::STATUS['UNPUBLISHED'] => 'danger',
        WebsiteCustomize::STATUS['PUBLISHED'] => 'success',
    ];

    public function arrayToObject(array $expression, $websiteCustomize = null)
    {
        unset($websiteCustomize);
        unset($expression);
        return new NullWebsiteCustomize();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }
/**
 * @SuppressWarnings(PHPMD)
 */
    public function objectToArray($websiteCustomize, array $keys = array())
    {
        if (!$websiteCustomize instanceof WebsiteCustomize) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'version',
                'category',
                'content',
                'category',
                'status',
                'crew' => [],
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($websiteCustomize->getId());
        }
        if (in_array('category', $keys)) {
            $expression['category'] = marmot_encode($websiteCustomize->getCategory());
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $websiteCustomize->getContent();
        }
        if (in_array('version', $keys)) {
            $expression['version'] = $websiteCustomize->getVersion();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($websiteCustomize->getStatus()),
                'name' => self::STATUS_CN[$websiteCustomize->getStatus()],
                'type' => self::STATUS_TAG_TYPE[$websiteCustomize->getStatus()]
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $websiteCustomize->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $websiteCustomize->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $websiteCustomize->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $websiteCustomize->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $websiteCustomize->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $websiteCustomize->getCreateTime());
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $websiteCustomize->getCrew(),
                $keys['crew']
            );
        }

        return $expression;
    }
}
