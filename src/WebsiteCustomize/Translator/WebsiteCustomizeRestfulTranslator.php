<?php
namespace Base\Sdk\WebsiteCustomize\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Base\Sdk\WebsiteCustomize\Model\NullWebsiteCustomize;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

class WebsiteCustomizeRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $websiteCustomize = null)
    {
        return $this->translateToObject($expression, $websiteCustomize);
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $websiteCustomize = null)
    {
        if (empty($expression)) {
            return new NullWebsiteCustomize();
        }

        if ($websiteCustomize == null) {
            $websiteCustomize = new WebsiteCustomize();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $websiteCustomize->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['category'])) {
            $websiteCustomize->setCategory($attributes['category']);
        }

        if (isset($attributes['version'])) {
            $websiteCustomize->setVersion($attributes['version']);
        }

        if (isset($attributes['content'])) {
            $websiteCustomize->setContent($attributes['content']);
        }

        if (isset($attributes['createTime'])) {
            $websiteCustomize->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['statusTime'])) {
            $websiteCustomize->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['updateTime'])) {
            $websiteCustomize->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['status'])) {
            $websiteCustomize->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $websiteCustomize->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $websiteCustomize;
    }

    public function objectToArray($websiteCustomize, array $keys = array())
    {
        if (!$websiteCustomize instanceof WebsiteCustomize) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'category',
                'content',
                'status',
                'crew'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'websiteCustomizes'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $websiteCustomize->getId();
        }

        $attributes = array();

        if (in_array('category', $keys)) {
            $attributes['category'] = $websiteCustomize->getCategory();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $websiteCustomize->getContent();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $websiteCustomize->getStatus();
        }
        
        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $websiteCustomize->getCrew()->getId()
                )
            );
        }
        
        return $expression;
    }
}
