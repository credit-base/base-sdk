<?php
namespace Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

interface IWebsiteCustomizeAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperateAbleAdapter
{
    public function customize(string $category);
}
