<?php
namespace Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;

use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Base\Sdk\WebsiteCustomize\Model\NullWebsiteCustomize;

use Base\Sdk\WebsiteCustomize\Translator\WebsiteCustomizeRestfulTranslator;

class WebsiteCustomizeRestfulAdapter extends GuzzleAdapter implements IWebsiteCustomizeAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'WEBSITE_CUSTOMIZE_LIST'=>[
            'fields' => [],
            'include' => 'crews'
        ],
        'WEBSITE_CUSTOMIZE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crews'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new WebsiteCustomizeRestfulTranslator();
        $this->resource = 'websiteCustomizes';
        $this->scenario = array();
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function getMapErrors() : array
    {
        $mapError = [
            100 => [
                'crewId' => 'CREW_IS_EMPTY',
                'websiteCustomizeId' => 'WEBSITE_CUSTOMIZE_NOT_UNIQUER'
            ],
            101 => [
                'content'=>CONTENT_FORMAT_ERROR,
                'content-keys' => CONTENT_KEYS_FORMAT_ERROR,
                'content-requiredKeys' => CONTENT_REQUIRED_KEYS_FORMAT_ERROR,
                'memorialStatus'=>MEMORIAL_STATUS_FORMAT_ERROR,
                'theme' => THEME_FORMAT_ERROR,
                'theme-color' => THEME_COLOR_FORMAT_ERROR,
                'theme-image' => THEME_IMAGE_FORMAT_ERROR,
                'headerBarLeft' => HEADER_BAR_LEFT_FORMAT_ERROR,
                'headerBarLeft-name' => HEADER_BAR_LEFT_NAME_FORMAT_ERROR,
                'headerBarLeft-status' => HEADER_BAR_LEFT_STATUS_FORMAT_ERROR,
                'headerBarLeft-type' => HEADER_BAR_LEFT_TYPE_FORMAT_ERROR,
                'headerBarLeft-url' => HEADER_BAR_LEFT_URL_FORMAT_ERROR,
                'headerBarRight' => HEADER_BAR_RIGHT_FORMAT_ERROR,
                'headerBarRight-name' => HEADER_BAR_RIGHT_NAME_FORMAT_ERROR,
                'headerBarRight-status' => HEADER_BAR_RIGHT_STATUS_FORMAT_ERROR,
                'headerBarRight-type' => HEADER_BAR_RIGHT_TYPE_FORMAT_ERROR,
                'headerBarRight-url' => HEADER_BAR_RIGHT_URL_FORMAT_ERROR,
                'headerBg' => HEADER_BG_FORMAT_ERROR,
                'logo' => LOGO_FORMAT_ERROR,
                'headerSearch' => HEADER_SEARCH_FORMAT_ERROR,
                'headerSearch-status' => HEADER_SEARCH_STATUS_FORMAT_ERROR,
                'nav' => NAV_FORMAT_ERROR,
                'nav-count' => NAV_COUNT_FORMAT_ERROR,
                'nav-name' => NAV_NAME_FORMAT_ERROR,
                'nav-status' => NAV_STATUS_FORMAT_ERROR,
                'nav-url' => NAV_URL_FORMAT_ERROR,
                'nav-image' => NAV_IMAGE_FORMAT_ERROR,
                'centerDialog' => CENTER_DIALOG_FORMAT_ERROR,
                'centerDialog-status' => CENTER_DIALOG_STATUS_FORMAT_ERROR,
                'centerDialog-url' => CENTER_DIALOG_URL_FORMAT_ERROR,
                'centerDialog-image' => CENTER_DIALOG_IMAGE_FORMAT_ERROR,
                'animateWindow' => ANIMATE_WINDOW_FORMAT_ERROR,
                'animateWindow-count' => ANIMATE_WINDOW_COUNT_FORMAT_ERROR,
                'animateWindow-status' => ANIMATE_WINDOW_STATUS_FORMAT_ERROR,
                'animateWindow-image' => ANIMATE_WINDOW_IMAGE_FORMAT_ERROR,
                'animateWindow-url' => ANIMATE_WINDOW_URL_FORMAT_ERROR,
                'leftFloatCard' => LEFT_FLOAT_CARD_FORMAT_ERROR,
                'leftFloatCard-count' => LEFT_FLOAT_CARD_COUNT_FORMAT_ERROR,
                'leftFloatCard-status' => LEFT_FLOAT_CARD_STATUS_FORMAT_ERROR,
                'leftFloatCard-image' => LEFT_FLOAT_CARD_IMAGE_FORMAT_ERROR,
                'leftFloatCard-url' => LEFT_FLOAT_CARD_URL_FORMAT_ERROR,
                'frameWindow' => FRAME_WINDOW_FORMAT_ERROR,
                'frameWindow-status' => FRAME_WINDOW_STATUS_FORMAT_ERROR,
                'frameWindow-url' => FRAME_WINDOW_URL_FORMAT_ERROR,
                'rightToolBar' => RIGHT_TOOL_BAR_FORMAT_ERROR,
                'rightToolBar-count' => RIGHT_TOOL_BAR_COUNT_FORMAT_ERROR,
                'rightToolBar-status' => RIGHT_TOOL_BAR_STATUS_FORMAT_ERROR,
                'rightToolBar-name' => RIGHT_TOOL_BAR_NAME_FORMAT_ERROR,
                'rightToolBar-category' => RIGHT_TOOL_BAR_CATEGORY_FORMAT_ERROR,
                'rightToolBar-images' => RIGHT_TOOL_BAR_IMAGES_FORMAT_ERROR,
                'rightToolBar-images-count' => RIGHT_TOOL_BAR_IMAGES_COUNT_FORMAT_ERROR,
                'rightToolBar-images-title' => RIGHT_TOOL_BAR_IMAGES_TITLE_FORMAT_ERROR,
                'rightToolBar-images-image' => RIGHT_TOOL_BAR_IMAGES_IMAGE_FORMAT_ERROR,
                'rightToolBar-url' => RIGHT_TOOL_BAR_URL_FORMAT_ERROR,
                'rightToolBar-description' => RIGHT_TOOL_BAR_DESCRIPTION_FORMAT_ERROR,
                'specialColumn' => SPECIAL_COLUMN_FORMAT_ERROR,
                'specialColumn-count' => SPECIAL_COLUMN_COUNT_FORMAT_ERROR,
                'specialColumn-status' => SPECIAL_COLUMN_STATUS_FORMAT_ERROR,
                'specialColumn-image' => SPECIAL_COLUMN_IMAGE_FORMAT_ERROR,
                'specialColumn-url' => SPECIAL_COLUMN_URL_FORMAT_ERROR,
                'relatedLinks' => RELATED_LINKS_FORMAT_ERROR,
                'relatedLinks-count' => RELATED_LINKS_COUNT_FORMAT_ERROR,
                'relatedLinks-status' => RELATED_LINKS_STATUS_FORMAT_ERROR,
                'relatedLinks-name' => RELATED_LINKS_NAME_FORMAT_ERROR,
                'relatedLinks-items' => RELATED_LINKS_ITEMS_FORMAT_ERROR,
                'relatedLinks-items-status' => RELATED_LINKS_ITEMS_STATUS_FORMAT_ERROR,
                'relatedLinks-items-name' => RELATED_LINKS_ITEMS_NAME_FORMAT_ERROR,
                'relatedLinks-items-url' => RELATED_LINKS_ITEMS_URL_FORMAT_ERROR,
                'footerBanner' => FOOTER_BANNER_FORMAT_ERROR,
                'footerBanner-count' => FOOTER_BANNER_COUNT_FORMAT_ERROR,
                'footerBanner-status' => FOOTER_BANNER_STATUS_FORMAT_ERROR,
                'footerBanner-image' => FOOTER_BANNER_IMAGE_FORMAT_ERROR,
                'footerBanner-url' => FOOTER_BANNER_URL_FORMAT_ERROR,
                'organizationGroup' => ORGANIZATION_GROUP_FORMAT_ERROR,
                'organizationGroup-count' => ORGANIZATION_GROUP_COUNT_FORMAT_ERROR,
                'organizationGroup-status' => ORGANIZATION_GROUP_STATUS_FORMAT_ERROR,
                'organizationGroup-image' => ORGANIZATION_GROUP_IMAGE_FORMAT_ERROR,
                'organizationGroup-url' => ORGANIZATION_GROUP_URL_FORMAT_ERROR,
                'silhouette' => SILHOUETTE_FORMAT_ERROR,
                'silhouette-status' => SILHOUETTE_STATUS_FORMAT_ERROR,
                'silhouette-image' => SILHOUETTE_IMAGE_FORMAT_ERROR,
                'silhouette-description' => SILHOUETTE_DESCRIPTION_FORMAT_ERROR,
                'partyAndGovernmentOrgans' => PARTY_AND_GOVERNMENT_ORGANS_FORMAT_ERROR,
                'partyAndGovernmentOrgans-status' => PARTY_AND_GOVERNMENT_ORGANS_STATUS_FORMAT_ERROR,
                'partyAndGovernmentOrgans-image' => PARTY_AND_GOVERNMENT_ORGANS_IMAGE_FORMAT_ERROR,
                'partyAndGovernmentOrgans-url' => PARTY_AND_GOVERNMENT_ORGANS_URL_FORMAT_ERROR,
                'governmentErrorCorrection' => GOVERNMENT_ERROR_CORRECTION_FORMAT_ERROR,
                'governmentErrorCorrection-status' => GOVERNMENT_ERROR_CORRECTION_STATUS_FORMAT_ERROR,
                'governmentErrorCorrection-image' => GOVERNMENT_ERROR_CORRECTION_IMAGE_FORMAT_ERROR,
                'governmentErrorCorrection-url' => GOVERNMENT_ERROR_CORRECTION_URL_FORMAT_ERROR,
                'footerNav' => FOOTER_NAV_FORMAT_ERROR,
                'footerNav-status' => FOOTER_NAV_STATUS_FORMAT_ERROR,
                'footerNav-name' => FOOTER_NAV_NAME_FORMAT_ERROR,
                'footerNav-url' => FOOTER_NAV_URL_FORMAT_ERROR,
                'footerTwo' => FOOTER_TWO_FORMAT_ERROR,
                'footerTwo-status' => FOOTER_TWO_STATUS_FORMAT_ERROR,
                'footerTwo-name' => FOOTER_TWO_NAME_FORMAT_ERROR,
                'footerTwo-url' => FOOTER_TWO_URL_FORMAT_ERROR,
                'footerTwo-description' => FOOTER_TWO_DESCRIPTION_FORMAT_ERROR,
                'footerThree' => FOOTER_THREE_FORMAT_ERROR,
                'footerThree-status' => FOOTER_THREE_STATUS_FORMAT_ERROR,
                'footerThree-name' => FOOTER_THREE_NAME_FORMAT_ERROR,
                'footerThree-url' => FOOTER_THREE_URL_FORMAT_ERROR,
                'footerThree-description' => FOOTER_THREE_DESCRIPTION_FORMAT_ERROR,
                'footerThree-type' => FOOTER_THREE_TYPE_FORMAT_ERROR,
                'footerThree-image' => FOOTER_THREE_IMAGE_FORMAT_ERROR,
                'crewId'=>CREW_ID_FORMAT_ERROR,
                'category'=>CATEGORY_FORMAT_ERROR,
                'status'=>STATUS_FORMAT_ERROR,
            ],
            102=>[
                'status'=>STATUS_CAN_NOT_MODIFY
            ],
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullWebsiteCustomize());
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function addAction(WebsiteCustomize $websiteCustomize) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $websiteCustomize,
            array(
                'category',
                'status',
                'content',
                'crew'
            )
        );
       
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($websiteCustomize);
            return true;
        }

        return false;
    }

    protected function editAction(WebsiteCustomize $websiteCustomize) : bool
    {
        unset($websiteCustomize);
        return false;
    }

    public function publish(WebsiteCustomize $websiteCustomize) : bool
    {
        $this->patch(
            $this->getResource().'/'.$websiteCustomize->getId().'/'.'publish'
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($websiteCustomize);
            return true;
        }

        return false;
    }

    public function customize(string $category)
    {
        $this->get(
            $this->getResource().'/'.$category
        );
     
        return $this->isSuccess() ? $this->translateToObject() : NullWebsiteCustomize::getInstance();
    }
}
