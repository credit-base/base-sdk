<?php
/**
    * 1-99 系统错误规范
    * 100-200 通用错误规范
    * 201-300 OA通用错误规范
    * 301-400 门户通用错误规范
    * 401-500 共享通用错误规范
    * 501-600 用户通用错误规范
    * 601-10000 预留错误规范
    * 10001-10100 科室错误规范
    * 10101-10200 员工错误规范
    * 10201-10300 资源目录错误规范
    * 10301-10400 工单任务错误规范
 */

// 1-99 系统错误规范----------------------------------------------------------------

/**
 * csrf 验证失效
 */
define('CSRF_VERIFY_FAILURE', 15);
/**
 * 滑动验证失败
 */
define('AFS_VERIFY_FAILURE', 16);
/**
 * 用户未登录
 */
define('NEED_SIGNIN', 17);
/**
 * 短信发送太频繁
 */
define('SMS_SEND_TOO_QUICK', 18);
/**
 * 哈希无效
 */
define('HASH_INVALID', 19);
/**
 * 用户未登录
 */
define('LOGIN_EXPIRED', 20);
/**
 * 权限未定义
 */
define('PURVIEW_UNDEFINED', 21);
/**
 * 验证码不正确
 */
define('CAPTCHA_ERROR', 22);

// 100-200 通用错误规范----------------------------------------------------------------
/**
 * 数据不能重复
 */
define('PARAMETER_IS_UNIQUE', 100);
/**
 * 数据格式不正确
 */
define('PARAMETER_FORMAT_ERROR', 101);
/**
 * 参数为空
 */
define('PARAMETER_IS_EMPTY', 102);
/**
 * 状态不能操作
 */
define('STATUS_CAN_NOT_MODIFY', 103);
/**
 * 审核状态不能操作
 */
define('APPLY_STATUE_CAN_NOT_MODIFY', 104);

/**
 * 图片格式不正确
 */
define('IMAGE_FORMAT_ERROR', 121);
/**
 * 附件格式不正确
 */
define('ATTACHMENT_FORMAT_ERROR', 122);
/**
 * 姓名格式不正确
 */
define('REAL_NAME_FORMAT_ERROR', 123);
/**
 * 手机号格式不正确
 */
define('CELLPHONE_FORMAT_ERROR', 124);
/**
 * 身份证号格式不正确
 */
define('CARDID_FORMAT_ERROR', 125);
/**
 * 名称格式不正确
 */
define('NAME_FORMAT_ERROR', 126);
/**
 * 标题格式不正确
 */
define('TITLE_FORMAT_ERROR', 127);
/**
 * 描述格式不正确
 */
define('DESCRIPTION_FORMAT_ERROR', 128);
/**
 * 原因格式不正确
 */
define('REASON_FORMAT_ERROR', 129);
/**
 * 来源格式不正确
 */
define('SOURCE_FORMAT_ERROR', 130);
/**
 * 状态格式不正确
 */
define('STATUS_FORMAT_ERROR', 131);
/**
 * 状态非启用
 */
define('STATUS_NOT_ENABLE', 132);
/**
 * 状态非禁用
 */
define('STATUS_NOT_DISABLE', 133);
/**
 * 状态非置顶
 */
define('STATUS_NOT_TOP', 134);
/**
 * 状态已置顶
 */
define('STATUS_NOT_CANCEL_TOP', 135);

// 201-300 OA通用错误规范----------------------------------------------------------------
/**
 * jwt-token 为空
 */
define('JWT_TOKEN_EMPTY', 201);
/**
 * jwt-token 已过期
 */
define('JWT_TOKEN_OVERDUE', 202);
/**
 * jwt-token 验证失败
 */
define('JWT_TOKEN_ERROR', 203);

// 501-600 用户通用错误规范----------------------------------------------------------------

/**
 * 密码格式不正确
 */
define('PASSWORD_FORMAT_ERROR', 501);
/**
 * 旧密码不正确
 */
define('OLD_PASSWORD_INCORRECT', 502);
/**
 * 密码不正确
 */
define('PASSWORD_INCORRECT', 503);
/**
 * 确认密码与密码不一致
 */
define('PASSWORD_INCONSISTENCY', 504);
/**
 * 手机号已存在
 */
define('CELLPHONE_EXIST', 505);
/**
 * 手机号不存在
 */
define('CELLPHONE_NOT_EXIST', 506);
/**
 * 账号已被禁用
 */
define('USER_STATUS_DISABLE', 507);
/**
 * 审核人格式不正确
 */
define('APPLY_CREW_FORMAT_ERROR', 508);
/**
 * 发布人格式不正确
 */
define('CREW_ID_FORMAT_ERROR', 509);

// 10001-10100 科室管理错误规范----------------------------------------------------------------
/**
 * 科室名称格式不正确
 */
define('DEPARTMENT_NAME_FORMAT_ERROR', 10001);
/**
 * 科室名称已经存在
 */
define('DEPARTMENT_NAME_IS_UNIQUE', 10002);
/**
 * 所属委办局为空
 */
define('DEPARTMENT_USER_GROUP_IS_EMPTY', 10003);

// 10101-10200 员工管理错误规范----------------------------------------------------------------
/**
 * 员工类型不存在
 */
define('CREW_CATEGORY_NOT_EXIST', 10101);
/**
 * 员工权限范围格式不正确
 */
define('CREW_PURVIEW_FORMAT_ERROR', 10102);
/**
 * 员工类型权限不足
 */
define('CREW_PURVIEW_HIERARCHY_FORMAT_ERROR', 10103);
/**
 * 该所属科室不属于该所属委办局
 */
define('DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP', 10104);
/**
 * 所属委办局格式不正确
 */
define('CREW_USER_GROUP_FORMAT_ERROR', 10105);
/**
 * 所属科室格式不正确
 */
define('CREW_DEPARTMENT_FORMAT_ERROR', 10106);

// 10201-10300 资源目录错误规范----------------------------------------------------------------
/**
 * 目录名称格式不正确
 */
define('TEMPLATE_NAME_FORMAT_ERROR', 10201);
/**
 * 目录标识格式不正确
 */
define('IDENTIFY_FORMAT_ERROR', 10202);
/**
 * 主体类别格式不正确
 */
define('SUBJECT_CATEGORY_FORMAT_ERROR', 10203);
/**
 * 公开范围格式不正确
 */
define('DIMENSION_FORMAT_ERROR', 10204);
/**
 * 数据长度格式不正确
 */
define('LENGTH_FORMAT_ERROR', 10205);
/**
 * 模板信息格式不正确
 */
define('ITEMS_FORMAT_ERROR', 10206);
/**
 * 信息项名称格式不正确
 */
define('OPTIONS_FORMAT_ERROR', 10207);
/**
 * 是否必填格式不正确
 */
define('IS_NECESSARY_FORMAT_ERROR', 10208);
/**
 * 是否脱敏格式不正确
 */
define('IS_MASKED_FORMAT_ERROR', 10209);
/**
 * 脱敏规则格式不正确
 */
define('MASK_RULE_FORMAT_ERROR', 10210);
/**
 * 备注格式不正确
 */
define('REMARKS_FORMAT_ERROR', 10211);
/**
 * 数据类型格式不正确
 */
define('ITEM_TYPE_FORMAT_ERROR', 10212);
/**
 * 目录标识已存在
 */
define('IDENTIFY_FORMAT_EXIST', 10213);
/**
 * 统一社会信用代码格式错误
 */
define('TYSHXYDM_FORMAT_ERROR', 10214);
/**
 * 信用主体名称格式错误
 */
define('ZTMC_FORMAT_ERROR', 10215);
/**
 * 资源目录类型不存在
 */
define('CATEGORY_NOT_EXIST', 10216);
/**
 * 资源目录信息类别不存在
 */
define('INFO_CATEGORY_NOT_EXIST', 10217);
/**
 * 前置机资源目录已经存在
 */
define('QZJ_TEMPLATE_EXIST', 10218);
/**
 * 信息分类格式不正确
 */
define('INFO_CLASSIFY_FORMAT_ERROR', 10219);
/**
 * 更新频率格式不正确
 */
define('EXCHANGE_FREQUENCY_FORMAT_ERROR', 10220);
/**
 * 来源委办局格式不正确
 */
define('SOURCE_UNIT_FORMAT_ERROR', 10221);
/**
 * 信息项数据长度格式不正确
 */
define('ITEM_LENGTH_FORMAT_ERROR', 10222);
/**
 * 信息项可选范围格式不正确
 */
define('ITEM_OPTIONS_FORMAT_ERROR', 10223);

// 10301-10400 工单任务错误规范----------------------------------------------------------------
/**
 * 工单任务标题格式不正确
 */
define('WORK_ORDER_TASK_TITLE_FORMAT_ERROR', 10301);
/**
 * 工单任务目录类型格式不正确
 */
define('TEMPLATE_TYPE_FORMAT_ERROR', 10302);
/**
 * 终结时间格式不正确
 */
define('END_TIME_FORMAT_ERROR', 10303);
/**
 * 反馈信息格式不正确
 */
define('FEEDBACK_RECORDS_FORMAT_ERROR', 10304);
/**
 * 反馈信息用户格式不正确
 */
define('CREW_FORMAT_ERROR', 10305);
/**
 * 反馈信息委办局格式不正确
 */
define('USER_GROUP_FORMAT_ERROR', 10306);
/**
 * 反馈信息目录格式不正确
 */
define('TEMPLATE_ID_FORMAT_ERROR', 10307);
/**
 * 反馈信息是否已存在目录格式不正确
 */
define('IS_EXISTED_TEMPLATE_PARAMETER_FORMAT_ERROR', 10308);

// 10401-10500 新闻错误规范----------------------------------------------------------------
/**
 * 新闻栏目分类不存在
 */
define('NEWS_TYPE_NOT_EXIST', 10401);
/**
 * 新闻公共类型不存在
 */
define('DIMENSION_NOT_EXIST', 10401);
/**
 * 新闻首页展示状态不存在
 */
define('HOME_PAGE_SHOW_STATUS_NOT_EXIST', 10403);
/**
 * 新闻轮播状态不存在
 */
define('BANNER_STATUS_NOT_EXIST', 10404);

// 10501-10600 前台用户管理模块错误规范----------------------------------------------------------------
/**
 * 邮箱格式不正确
 */
define('EMAIL_FORMAT_ERROR', 10501);
/**
 * 用户名不存在
 */
define('NAME_NOT_EXIT', 10502);
/**
 * 联系地址格式不正确
 */
define('CONTACT_ADDRESS_FORMAT_ERROR', 10503);
/**
 * 密保问题格式不正确
 */
define('SECURITY_QUESTION_FORMAT_ERROR', 10504);
/**
 * 密保答案格式不正确
 */
define('SECURITY_ANSWER_FORMAT_ERROR', 10505);
/**
 * 性别格式不正确
 */
define('GENDER_FORMAT_ERROR', 10506);
/**
 * 用户名已经存在
 */
define('USER_NAME_EXIST', 10507);
/**
 * 邮箱已经存在
 */
define('EMAIL_EXIST', 10508);
/**
 * 用户名格式不正确
 */
define('USER_NAME_FORMAT_ERROR', 10509);
/**
 * 密保答案不正确
 */
define('SECURITY_ANSWER_INCORRECT', 10510);

// 10601-10700 信用刊物模块错误规范----------------------------------------------------------------
/**
 * 年份格式不正确
 */
define('YEAR_FORMAT_ERROR', 10601);

// 10701-10800 信用随手拍模块错误规范----------------------------------------------------------------
/**
 * 附件为空
 */
define('CREDIT_PHOTOGRAPHY_FORMAT_ERROR', 10701);
/**
 * 附件格式不正确
 */
define('CREDIT_PHOTOGRAPHY_ATTACHMENT_FORMAT_ERROR', 10702);

// 10801-10900 规则管理模块错误规范----------------------------------------------------------------
/**
 * 规则格式不正确
 */
define('RULES_FORMAT_ERROR', 10801);
/**
 * 规则名称不存在
 */
define('RULES_NAME_NOT_UNIQUE', 10802);
/**
 * 补全数量不正确
 */
define('COMPLETION_RULES_COUNT_FORMAT_ERROR', 10803);
/**
 * 补全资源目录id格式不正确
 */
define('COMPLETION_RULES_ID_FORMAT_ERROR', 10804);
/**
 * 补全依据不正确
 */
define('COMPLETION_RULES_BASE_FORMAT_ERROR', 10805);
/**
 * 补全信息项不正确
 */
define('COMPLETION_RULES_ITEM_FORMAT_ERROR', 10806);
/**
 * 补全规则格式不正确
 */
define('COMPLETION_RULES_FORMAT_ERROR', 10807);
/**
 * 比对数量不正确
 */
define('COMPARISON_RULES_COUNT_FORMAT_ERROR', 10808);
/**
 *  比对资源目录id格式不正确
 */
define('COMPARISON_RULES_ID_FORMAT_ERROR', 10809);
/**
 * 比对依据不正确
 */
define('COMPARISON_RULES_BASE_FORMAT_ERROR', 10810);
/**
 * 比对信息项不正确
 */
define('COMPARISON_RULES_ITEM_FORMAT_ERROR', 10811);
/**
 * 比对规则格式不正确
 */
define('COMPARISON_RULES_FORMAT_ERROR', 10812);
/**
 * 去重规则格式不正确
 */
define('DE_DUPLICATION_RULES_FORMAT_ERROR', 10813);
/**
 * 去重信息项不正确
 */
define('DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR', 10814);
/**
 * 去重结果不正确
 */
define('DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR', 10815);
/**
 * 转换资源目录信息项不存在
 */
define('TRANSFORMATION_RULES_TRANSFORMATION_NOT_EXIST', 10816);
/**
 * 来源资源目录信息项不存在
 */
define('TRANSFORMATION_RULES_SOURCE_NOT_EXIST', 10817);
/**
 * 目标信息项类型不存在
 */
define('TRANSFORMATION_ITEM_TYPE_NOT_EXIST', 10818);
/**
 * 类型不能转换
 */
define('TYPE_CANNOT_TRANSFORMATION', 10819);
/**
 * 长度不能转换
 */
define('LENGTH_CANNOT_TRANSFORMATION', 10820);
/**
 * 公开范围不能转换
 */
define('DIMENSION_CANNOT_TRANSFORMATION', 10821);
/**
 * 脱敏状态不能转换
 */
define('IS_MASKED_CANNOT_TRANSFORMATION', 10822);
/**
 * 脱敏范围不能转换
 */
define('MASK_RULE_CANNOT_TRANSFORMATION', 10823);
/**
 * 补全依据需要包含身份证号信息项
 */
define('ZRR_NOT_INCLUDE_IDENTIFY', 10824);
/**
 * 补全规则目标资源目录模板待补全信息项不存在
 */
define('COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST', 10825);
/**
 * 补全规则补全信息项来源不存在
 */
define('COMPLETION_RULES_COMPLETION_TEMPLATE_NOT_EXIST', 10826);
/**
 * 补全规则补全信息项不存在
 */
define('COMPLETION_RULES_COMPLETION_TEMPLATE_ITEM_NOT_EXIST', 10827);
/**
 * 比对规则目标资源目录模板待比对信息项不存在
 */
define('COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST', 10828);
/**
 * 比对规则比对信息项来源不存在
 */
define('COMPARISON_RULES_COMPARISON_TEMPLATE_NOT_EXIST', 10829);
/**
 * 比对项为企业名称,比对依据不能选择企业名称信息项
 */
define('COMPARISON_RULES_BASE_CANNOT_NAME', 10830);
/**
 * 比对项为统一社会信用代码/身份证号,那比对依据不能选择统一社会信用代码/身份证号信息项
 */
define('COMPARISON_RULES_BASE_CANNOT_IDENTIFY', 10831);
/**
 * 比对规则比对信息项不存在
 */
define('COMPARISON_RULES_COMPARISON_TEMPLATE_ITEM_NOT_EXIST', 10832);
/**
 * 去重规则去重信息项不存在
 */
define('DE_DUPLICATION_RULES_ITEMS_NOT_EXIT', 10833);
/**
 * 分类格式不正确
 */
define('TYPE_FORMAT_ERROR', 10834);
/**
 * 目标资源目录格式不正确
 */
define('TRANSFORMATION_TEMPLATE_FORMAT_ERROR', 10835);
/**
 * 来源资源目录格式不正确
 */
define('SOURCE_TEMPLATE_FORMAT_ERROR', 10836);
/**
 * 存在未转换和补全的必填项
 */
define('REQUIRED_ITEMS_MAPPING_IS_EXIT', 10837);
/**
 * 目标资源目录为空
 */
define('TRANSFORMATION_TEMPLATE_IS_EMPTY', 10838);
/**
 * 来源资源目录为空
 */
define('SOURCE_TEMPLATE_IS_EMPTY', 10839);
/**
 * 规则数据已经存在
 */
define('RULES_IS_UNIQUE', 10840);
/**
 * 规则数据超出范围
 */
define('RULES_IS_OVER_STEP', 10841);
/**
 * 转换规则格式不正确
 */
define('TRANSFORMATION_RULES_FORMAT_ERROR', 10842);
/**
 * 规则目录类型不正确
 */
define('RULES_CATEGORY_FORMAT_ERROR', 10843);
/**
 * 目标目录类型不正确
 */
define('TRANSFORMATION_CATEGORY_FORMAT_ERROR', 10844);
/**
 * 来源目录类型不正确
 */
define('SOURCE_CATEGORY_FORMAT_ERROR', 10845);

// 10901-11000 数据管理子系统模块错误规范----------------------------------------------------------------
/**
 * 文件格式不正确
 */
define('FILE_FORMAT_ERROR', 10901);
/**
 * 文件大小不正确
 */
define('FILE_SIZE_FORMAT_ERROR', 10902);
/**
 * 文件为空
 */
define('FILE_IS_EMPTY', 10903);
/**
 * 文件已存在
 */
define('FILE_IS_UNIQUE', 10904);
/**
 * 文件名称的资源目录标识不正确
 */
define('FILE_IDENTIFY_FORMAT_ERROR', 10905);
/**
 * 文件名称的资源目录类型不正确
 */
define('FILE_CATEGORY_FORMAT_ERROR', 10906);
/**
 * 员工id为空
 */
define('CREW_IS_EMPTY', 10907);

// 11001-12000 数据管理子系统模块错误规范----------------------------------------------------------------
/**
 * 网站定制数据不存在
 */
define('WEBSITE_CUSTOMIZE_NOT_UNIQUER', 11001);
/**
 * 内容格式不正确
 */
define('CONTENT_FORMAT_ERROR', 11002);
/**
 * 内容数组格式不正确
 */
define('CONTENT_KEYS_FORMAT_ERROR', 11003);
/**
 * 内容必填项格式不正确
 */
define('CONTENT_REQUIRED_KEYS_FORMAT_ERROR', 11004);
/**
 * 内容背景置灰状态格式不正确
 */
define('MEMORIAL_STATUS_FORMAT_ERROR', 11005);
/**
 * 页面主题标识格式不正确
 */
define('THEME_FORMAT_ERROR', 11006);
/**
 * 页面主题标识-主题色格式不正确
 */
define('THEME_COLOR_FORMAT_ERROR', 11007);
/**
 * 页面主题标识-背景图格式不正确
 */
define('THEME_IMAGE_FORMAT_ERROR', 11008);
/**
 * 头部左侧位置标识页面主题标识格式不正确
 */
define('HEADER_BAR_LEFT_FORMAT_ERROR', 11009);
/**
 * 头部左侧位置标识页面主题标识名称格式不正确
 */
define('HEADER_BAR_LEFT_NAME_FORMAT_ERROR', 11010);
/**
 * 头部左侧位置标识页面主题标识状态格式不正确
 */
define('HEADER_BAR_LEFT_STATUS_FORMAT_ERROR', 11011);
/**
 * 头部左侧位置标识页面主题标识类型格式不正确
 */
define('HEADER_BAR_LEFT_TYPE_FORMAT_ERROR', 11012);
/**
 * 头部左侧位置标识页面主题标识链接格式不正确
 */
define('HEADER_BAR_LEFT_URL_FORMAT_ERROR', 11013);
/**
 * 头部右侧位置标识页面主题标识格式不正确
 */
define('HEADER_BAR_RIGHT_FORMAT_ERROR', 11014);
/**
 * 头部右侧位置标识页面主题标识名称格式不正确
 */
define('HEADER_BAR_RIGHT_NAME_FORMAT_ERROR', 11015);
/**
 * 头部右侧位置标识页面主题标识状态格式不正确
 */
define('HEADER_BAR_RIGHT_STATUS_FORMAT_ERROR', 11016);
/**
 * 头部右侧位置标识页面主题标识类型格式不正确
 */
define('HEADER_BAR_RIGHT_TYPE_FORMAT_ERROR', 11017);
/**
 * 头部右侧位置标识页面主题标识链接格式不正确
 */
define('HEADER_BAR_RIGHT_URL_FORMAT_ERROR', 11018);
/**
 * 网站header背景格式不正确
 */
define('HEADER_BG_FORMAT_ERROR', 11019);
/**
 * 网站logo格式不正确
 */
define('LOGO_FORMAT_ERROR', 11020);
/**
 * 头部搜索栏位置标识格式不正确
 */
define('HEADER_SEARCH_FORMAT_ERROR', 11021);
/**
 * 头部搜索栏位置标识状态格式不正确
 */
define('HEADER_SEARCH_STATUS_FORMAT_ERROR', 11022);
/**
 * 头部导航位置标识格式不正确
 */
define('NAV_FORMAT_ERROR', 11023);
/**
 * 头部导航位置标识数量超过限制
 */
define('NAV_COUNT_FORMAT_ERROR', 11024);
/**
 * 头部导航位置标识名称格式不正确
 */
define('NAV_NAME_FORMAT_ERROR', 11025);
/**
 * 头部导航位置标识状态格式不正确
 */
define('NAV_STATUS_FORMAT_ERROR', 11026);
/**
 * 头部导航位置标识链接格式不正确
 */
define('NAV_URL_FORMAT_ERROR', 11027);
/**
 * 头部导航位置标识图片格式不正确
 */
define('NAV_IMAGE_FORMAT_ERROR', 11028);
/**
 * 中间弹框位置标识格式不正确
 */
define('CENTER_DIALOG_FORMAT_ERROR', 11029);
/**
 * 中间弹框位置标识状态格式不正确
 */
define('CENTER_DIALOG_STATUS_FORMAT_ERROR', 11030);
/**
 * 中间弹框位置标识链接格式不正确
 */
define('CENTER_DIALOG_URL_FORMAT_ERROR', 11031);
/**
 * 中间弹框位置标识图片格式不正确
 */
define('CENTER_DIALOG_IMAGE_FORMAT_ERROR', 11032);
/**
 * 飘窗位置标识格式不正确
 */
define('ANIMATE_WINDOW_FORMAT_ERROR', 11033);
/**
 * 飘窗位置标识数量超出限制
 */
define('ANIMATE_WINDOW_COUNT_FORMAT_ERROR', 11034);
/**
 * 飘窗位置标识状态格式不正确
 */
define('ANIMATE_WINDOW_STATUS_FORMAT_ERROR', 11035);
/**
 * 飘窗位置标识图片格式不正确
 */
define('ANIMATE_WINDOW_IMAGE_FORMAT_ERROR', 11036);
/**
 * 飘窗位置标识链接格式不正确
 */
define('ANIMATE_WINDOW_URL_FORMAT_ERROR', 11037);
/**
 * 左侧浮动卡片位置标识格式不正确
 */
define('LEFT_FLOAT_CARD_FORMAT_ERROR', 11038);
/**
 * 左侧浮动卡片位置标识数量超出限制
 */
define('LEFT_FLOAT_CARD_COUNT_FORMAT_ERROR', 11039);
/**
 * 左侧浮动卡片位置标识状态格式不正确
 */
define('LEFT_FLOAT_CARD_STATUS_FORMAT_ERROR', 11040);
/**
 * 左侧浮动卡片位置标识图片格式不正确
 */
define('LEFT_FLOAT_CARD_IMAGE_FORMAT_ERROR', 11041);
/**
 * 左侧浮动卡片位置标识链接格式不正确
 */
define('LEFT_FLOAT_CARD_URL_FORMAT_ERROR', 11042);
/**
 * 外链窗口格式不正确
 */
define('FRAME_WINDOW_FORMAT_ERROR', 11043);
/**
 * 外链窗口状态格式不正确
 */
define('FRAME_WINDOW_STATUS_FORMAT_ERROR', 11044);
/**
 * 外链窗口链接格式不正确
 */
define('FRAME_WINDOW_URL_FORMAT_ERROR', 11045);
/**
 * 右侧工具栏格式不正确
 */
define('RIGHT_TOOL_BAR_FORMAT_ERROR', 11046);
/**
 * 右侧工具栏数量超出限制
 */
define('RIGHT_TOOL_BAR_COUNT_FORMAT_ERROR', 11047);
/**
 * 右侧工具栏状态格式不正确
 */
define('RIGHT_TOOL_BAR_STATUS_FORMAT_ERROR', 11048);
/**
 * 右侧工具栏名称格式不正确
 */
define('RIGHT_TOOL_BAR_NAME_FORMAT_ERROR', 11049);
/**
 * 右侧工具栏分类格式不正确
 */
define('RIGHT_TOOL_BAR_CATEGORY_FORMAT_ERROR', 11050);
/**
 * 右侧工具栏图片格式不正确
 */
define('RIGHT_TOOL_BAR_IMAGES_FORMAT_ERROR', 11051);
/**
 * 右侧工具栏图片数量超出限制
 */
define('RIGHT_TOOL_BAR_IMAGES_COUNT_FORMAT_ERROR', 11052);
/**
 * 右侧工具栏图片标题格式不正确
 */
define('RIGHT_TOOL_BAR_IMAGES_TITLE_FORMAT_ERROR', 11053);
/**
 * 右侧工具栏图片格式不正确
 */
define('RIGHT_TOOL_BAR_IMAGES_IMAGE_FORMAT_ERROR', 11054);
/**
 * 右侧工具栏链接格式不正确
 */
define('RIGHT_TOOL_BAR_URL_FORMAT_ERROR', 11055);
/**
 * 右侧工具栏描述格式不正确
 */
define('RIGHT_TOOL_BAR_DESCRIPTION_FORMAT_ERROR', 11056);
/**
 * 专题专栏格式不正确
 */
define('SPECIAL_COLUMN_FORMAT_ERROR', 11057);
/**
 * 专题专栏数量超出限制
 */
define('SPECIAL_COLUMN_COUNT_FORMAT_ERROR', 11058);
/**
 * 专题专栏状态格式不正确
 */
define('SPECIAL_COLUMN_STATUS_FORMAT_ERROR', 11059);
/**
 * 专题专栏图片格式不正确
 */
define('SPECIAL_COLUMN_IMAGE_FORMAT_ERROR', 11060);
/**
 * 专题专栏链接格式不正确
 */
define('SPECIAL_COLUMN_URL_FORMAT_ERROR', 11061);
/**
 * 相关链接格式不正确
 */
define('RELATED_LINKS_FORMAT_ERROR', 11062);
/**
 * 相关链接数量超出限制
 */
define('RELATED_LINKS_COUNT_FORMAT_ERROR', 11063);
/**
 * 相关链接状态格式不正确
 */
define('RELATED_LINKS_STATUS_FORMAT_ERROR', 11064);
/**
 * 相关链接名称格式不正确
 */
define('RELATED_LINKS_NAME_FORMAT_ERROR', 11065);
/**
 * 相关链接items格式不正确
 */
define('RELATED_LINKS_ITEMS_FORMAT_ERROR', 11066);
/**
 * 相关链接items状态格式不正确
 */
define('RELATED_LINKS_ITEMS_STATUS_FORMAT_ERROR', 11067);
/**
 * 相关链接items名称格式不正确
 */
define('RELATED_LINKS_ITEMS_NAME_FORMAT_ERROR', 11068);
/**
 * 相关链接items链接格式不正确
 */
define('RELATED_LINKS_ITEMS_URL_FORMAT_ERROR', 11069);
/**
 * 底部轮播格式不正确
 */
define('FOOTER_BANNER_FORMAT_ERROR', 11070);
/**
 * 底部轮播数量超出限制
 */
define('FOOTER_BANNER_COUNT_FORMAT_ERROR', 11071);
/**
 * 底部轮播状态格式不正确
 */
define('FOOTER_BANNER_STATUS_FORMAT_ERROR', 11072);
/**
 * 底部轮播图片格式不正确
 */
define('FOOTER_BANNER_IMAGE_FORMAT_ERROR', 11073);
/**
 * 底部轮播链接格式不正确
 */
define('FOOTER_BANNER_URL_FORMAT_ERROR', 11074);
/**
 * 组织列表格式不正确
 */
define('ORGANIZATION_GROUP_FORMAT_ERROR', 11075);
/**
 * 组织列表数量超出限制
 */
define('ORGANIZATION_GROUP_COUNT_FORMAT_ERROR', 11076);
/**
 * 组织列表状态格式不正确
 */
define('ORGANIZATION_GROUP_STATUS_FORMAT_ERROR', 11077);
/**
 * 组织列表图片格式不正确
 */
define('ORGANIZATION_GROUP_IMAGE_FORMAT_ERROR', 11078);
/**
 * 组织列表链接格式不正确
 */
define('ORGANIZATION_GROUP_URL_FORMAT_ERROR', 11079);
/**
 * 剪影格式不正确
 */
define('SILHOUETTE_FORMAT_ERROR', 11080);
/**
 * 剪影状态格式不正确
 */
define('SILHOUETTE_STATUS_FORMAT_ERROR', 11081);
/**
 * 剪影图片格式不正确
 */
define('SILHOUETTE_IMAGE_FORMAT_ERROR', 11082);
/**
 * 剪影描述格式不正确
 */
define('SILHOUETTE_DESCRIPTION_FORMAT_ERROR', 11083);
/**
 * 党政机关格式不正确
 */
define('PARTY_AND_GOVERNMENT_ORGANS_FORMAT_ERROR', 11084);
/**
 * 党政机关状态格式不正确
 */
define('PARTY_AND_GOVERNMENT_ORGANS_STATUS_FORMAT_ERROR', 11085);
/**
 * 党政机关图片格式不正确
 */
define('PARTY_AND_GOVERNMENT_ORGANS_IMAGE_FORMAT_ERROR', 11086);
/**
 * 党政机关链接格式不正确
 */
define('PARTY_AND_GOVERNMENT_ORGANS_URL_FORMAT_ERROR', 11087);
/**
 * 政府纠错格式不正确
 */
define('GOVERNMENT_ERROR_CORRECTION_FORMAT_ERROR', 11088);
/**
 * 政府纠错状态格式不正确
 */
define('GOVERNMENT_ERROR_CORRECTION_STATUS_FORMAT_ERROR', 11089);
/**
 * 政府纠错图片格式不正确
 */
define('GOVERNMENT_ERROR_CORRECTION_IMAGE_FORMAT_ERROR', 11090);
/**
 * 政府纠错链接格式不正确
 */
define('GOVERNMENT_ERROR_CORRECTION_URL_FORMAT_ERROR', 11091);
/**
 * 页脚导航格式不正确
 */
define('FOOTER_NAV_FORMAT_ERROR', 11092);
/**
 * 页脚导航状态格式不正确
 */
define('FOOTER_NAV_STATUS_FORMAT_ERROR', 11093);
/**
 * 页脚导航名称格式不正确
 */
define('FOOTER_NAV_NAME_FORMAT_ERROR', 11094);
/**
 * 页脚导航链接格式不正确
 */
define('FOOTER_NAV_URL_FORMAT_ERROR', 11095);
/**
 * 页脚第二行内容呈现格式不正确
 */
define('FOOTER_TWO_FORMAT_ERROR', 11096);
/**
 * 页脚第二行内容呈现状态格式不正确
 */
define('FOOTER_TWO_STATUS_FORMAT_ERROR', 11097);
/**
 * 页脚第二行内容呈现名称格式不正确
 */
define('FOOTER_TWO_NAME_FORMAT_ERROR', 11098);
/**
 * 页脚第二行内容呈现链接格式不正确
 */
define('FOOTER_TWO_URL_FORMAT_ERROR', 11099);
/**
 * 页脚第二行内容呈现描述格式不正确
 */
define('FOOTER_TWO_DESCRIPTION_FORMAT_ERROR', 11100);
/**
 * 页脚第三行内容呈现格式不正确
 */
define('FOOTER_THREE_FORMAT_ERROR', 11101);
/**
 * 页脚第三行内容呈现状态格式不正确
 */
define('FOOTER_THREE_STATUS_FORMAT_ERROR', 11102);
/**
 * 页脚第三行内容呈现名称格式不正确
 */
define('FOOTER_THREE_NAME_FORMAT_ERROR', 11103);
/**
 * 页脚第三行内容呈现链接格式不正确
 */
define('FOOTER_THREE_URL_FORMAT_ERROR', 11104);
/**
 * 页脚第三行内容呈现描述格式不正确
 */
define('FOOTER_THREE_DESCRIPTION_FORMAT_ERROR', 11105);
/**
 * 页脚第三行内容呈现类型格式不正确
 */
define('FOOTER_THREE_TYPE_FORMAT_ERROR', 11105);
/**
 * 页脚第三行内容呈现图片格式不正确
 */
define('FOOTER_THREE_IMAGE_FORMAT_ERROR', 11106);
/**
* 网站定制分类格式不正确
*/
define('CATEGORY_FORMAT_ERROR', 11107);

// 12001-13000 互动交流模块错误规范----------------------------------------------------------------
/**
 * 前台用户格式不正确
 */
define('MEMBER_ID_FORMAT_ERROR', 12001);
/**
 * 受理委办局格式不正确
 */
define('ACCEPT_USER_GROUP_ID_FORMAT_ERROR', 12002);
/**
 * 图片数量超出限制
 */
define('IMAGES_COUNT_FORMAT_ERROR', 12003);
/**
 * 被表扬主体格式不正确
 */
define('SUBJECT_FORMAT_ERROR', 12004);
/**
 * 联系方式格式不正确
 */
define('CONTACT_FORMAT_ERROR', 12005);
/**
 * 回复内容格式不正确
 */
define('REPLY_CONTENT_FORMAT_ERROR', 12006);
/**
 * 回复图片格式不正确
 */
define('REPLY_IMAGES_FORMAT_ERROR', 12007);
/**
 * 回复图片数量超出限制
 */
define('REPLY_IMAGES_COUNT_FORMAT_ERROR', 12008);
/**
 * 受理情况格式不正确
 */
define('REPLY_ADMISSIBILITY_FORMAT_ERROR', 12009);
/**
 * 受理人格式不正确
 */
define('REPLY_CREW_FORMAT_ERROR', 12010);
/**
 * 前台用户不能为空
 */
define('MEMBER_ID_IS_EMPTY', 12011);
/**
 * 受理委办局不能为空
 */
define('ACCEPT_USER_GROUP_ID_IS_EMPTY', 12012);
/**
 * 受理人不能为空
 */
define('CREW_ID_IS_EMPTY', 12013);
/**
 * 上传身份证/营业执照格式不正确
 */
define('CERTIFICATES_FORMAT_ERROR', 12014);
/**
 * 反馈类型格式不正确
 */
define('INTERACTION_TYPE_FORMAT_ERROR', 10834);
