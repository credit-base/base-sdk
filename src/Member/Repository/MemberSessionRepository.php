<?php
namespace Base\Sdk\Member\Repository;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Adapter\Member\MemberSessionAdapter;

class MemberSessionRepository
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new MemberSessionAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    protected function getAdapter() : MemberSessionAdapter
    {
        return $this->adapter;
    }

    public function save(Member $member) : bool
    {
        return $this->getAdapter()->save($member);
    }

    public function get(int $id)
    {
        return $this->getAdapter()->get($id);
    }

    public function clear(int $id) : bool
    {
        return $this->getAdapter()->del($id);
    }
}
