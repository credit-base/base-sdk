<?php
namespace Base\Sdk\Member\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\OperateAbleRepositoryTrait;
use Base\Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Adapter\Member\IMemberAdapter;
use Base\Sdk\Member\Adapter\Member\MemberMockAdapter;
use Base\Sdk\Member\Adapter\Member\MemberRestfulAdapter;

class MemberRepository extends Repository implements IMemberAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'MEMBER_LIST';
    const FETCH_ONE_MODEL_UN = 'MEMBER_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new MemberRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    protected function getActualAdapter()
    {
        return $this->adapter;
    }

    protected function getMockAdapter()
    {
        return new MemberMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function signIn(Member $member) : bool
    {
        return $this->getAdapter()->signIn($member);
    }
    
    public function resetPassword(Member $member) : bool
    {
        return $this->getAdapter()->resetPassword($member);
    }
    
    public function updatePassword(Member $member) : bool
    {
        return $this->getAdapter()->updatePassword($member);
    }
    
    public function validateSecurity(Member $member) : bool
    {
        return $this->getAdapter()->validateSecurity($member);
    }
}
