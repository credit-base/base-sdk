<?php
namespace Base\Sdk\Member\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Model\NullMember;
use Base\Sdk\Member\Model\SecurityQA;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Common\Utils\Mask;

class MemberTranslator implements ITranslator
{
    const GENDER_CN = array(
        0 => '未知',
        1 => '男',
        2 => '女',
    );

    const CARD_ID_MAX_LENGTH = 18;

    const SECURITY_QUESTION_CN = array(
        SecurityQA::SECURITY_QUESTION['NULL'] => '暂无',
        SecurityQA::SECURITY_QUESTION['QUESTION_NAME'] => '你父亲或母亲的姓名',
        SecurityQA::SECURITY_QUESTION['QUESTION_MOVIE'] => '你喜欢看的电影',
        SecurityQA::SECURITY_QUESTION['QUESTION_FRIEND'] => '你最好朋友的名字',
        SecurityQA::SECURITY_QUESTION['QUESTION_HIGH_SCHOOL'] => '你毕业于哪个高中',
        SecurityQA::SECURITY_QUESTION['QUESTION_COLOR'] => '你最喜欢的颜色'
    );
    
    public function arrayToObject(array $expression, $member = null)
    {
        unset($member);
        unset($expression);
        return new NullMember();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

   /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($member, array $keys = array())
    {
        if (!$member instanceof Member) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'userName',
                'realName',
                'cardId',
                'cellphone',
                'email',
                'contactAddress',
                'securityQuestion',
                'securityAnswer',
                'password',
                'gender',
                'oldPassword',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($member->getId());
        }
        if (in_array('gender', $keys)) {
            $expression['gender'] = $member->getGender();
        }
        if (in_array('userName', $keys)) {
            $expression['userName'] = $member->getUserName();
        }
        if (in_array('realName', $keys)) {
            $expression['realName'] = $member->getRealName();
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $member->getCellphone();
            $expression['cellphoneMask'] = Mask::mask($expression['cellphone'], 4, 3);
        }
        if (in_array('email', $keys)) {
            $expression['email'] = $member->getEmail();
        }
        
        if (in_array('cardId', $keys)) {
            $expression['cardId'] = $member->getCardId();
            $expression['cardIdMask'] =  $this->getCardIdMask($expression['cardId']);
        }
        if (in_array('contactAddress', $keys)) {
            $expression['contactAddress'] = $member->getContactAddress();
        }
        if (in_array('gender', $keys)) {
            $expression['gender'] = [
                'id' => marmot_encode($member->getGender()),
                'name' => self::GENDER_CN[$member->getGender()]
            ];
        }
        if (in_array('securityQuestion', $keys)) {
            $expression['securityQuestion'] = [
                'id' => marmot_encode($member->getSecurityQa()->getId()),
                'name' => array_key_exists(
                    $member->getSecurityQa()->getId(),
                    self::SECURITY_QUESTION_CN
                ) ? self::SECURITY_QUESTION_CN[$member->getSecurityQa()->getId()] : ''
            ];
        }
        if (in_array('securityAnswer', $keys)) {
            $expression['securityAnswer'] = $member->getSecurityQa()->getAnswer();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($member->getStatus()),
                'name' => IApplyCategory::STATUS_CN[$member->getStatus()],
                'type' => IApplyCategory::STATUS_TAG_TYPE[$member->getStatus()]
            ];
        }
        
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $member->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H点i分', $member->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $member->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H点i分', $member->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $member->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H点i分', $member->getCreateTime());
        }

        return $expression;
    }

    private function getCardIdMask(string $cardId) : string
    {
        $cardIdMask = strlen($cardId) == self::CARD_ID_MAX_LENGTH ?
                                        Mask::mask($cardId, 4, 10) :
                                        Mask::mask($cardId, 4, 7);

        return $cardIdMask;
    }
}
