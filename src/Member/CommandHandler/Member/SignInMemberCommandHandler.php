<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Member\Command\Member\SignInMemberCommand;

class SignInMemberCommandHandler implements ICommandHandler
{
    use MemberCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof SignInMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->getMember();
        $member->setUserName($command->userName);
        $member->setPassword($command->password);

        return $member->signIn();
    }
}
