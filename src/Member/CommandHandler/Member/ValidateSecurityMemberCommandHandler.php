<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Member\Command\Member\ValidateSecurityMemberCommand;

class ValidateSecurityMemberCommandHandler implements ICommandHandler
{
    use MemberCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ValidateSecurityMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->fetchMember($command->id);
        $member->getSecurityQA()->setAnswer($command->securityAnswer);

        return $member->validateSecurity();
    }
}
