<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Model\SecurityQA;
use Base\Sdk\Member\Repository\MemberRepository;

trait MemberCommandHandlerTrait
{
    private $member;

    private $repository;
    
    public function __construct()
    {
        $this->member = new Member();
        $this->repository = new MemberRepository();
    }

    public function __destruct()
    {
        unset($this->member);
        unset($this->repository);
    }
    
    protected function getMember() : Member
    {
        return $this->member;
    }

    protected function getRepository() : MemberRepository
    {
        return $this->repository;
    }
    
    protected function fetchMember(int $id) : Member
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getSecurityQA() : SecurityQA
    {
        return new SecurityQA;
    }
}
