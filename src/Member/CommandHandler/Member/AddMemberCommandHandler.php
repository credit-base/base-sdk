<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Member\Command\Member\AddMemberCommand;

class AddMemberCommandHandler implements ICommandHandler
{
    use MemberCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof AddMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->getMember();
        $member->setUserName($command->userName);
        $member->setRealName($command->realName);
        $member->setCellphone($command->cellphone);
        $member->setEmail($command->email);
        $member->setCardId($command->cardId);
        $member->setContactAddress($command->contactAddress);
        $member->setPassword($command->password);

        $securityQa = $this->getSecurityQA();
        $securityQa->setId($command->securityQuestion);
        $securityQa->setAnswer($command->securityAnswer);
        $member->setSecurityQa($securityQa);
        
        return $member->add();
    }
}
