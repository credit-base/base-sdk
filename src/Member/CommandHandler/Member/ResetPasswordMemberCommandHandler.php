<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Member\Command\Member\ResetPasswordMemberCommand;

class ResetPasswordMemberCommandHandler implements ICommandHandler
{
    use MemberCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof ResetPasswordMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->getMember();
        $member->setUserName($command->userName);
        $member->getSecurityQA()->setAnswer($command->securityAnswer);
        $member->setPassword($command->password);

        return $member->resetPassword();
    }
}
