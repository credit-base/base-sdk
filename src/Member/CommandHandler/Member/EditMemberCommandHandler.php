<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Member\Command\Member\EditMemberCommand;

class EditMemberCommandHandler implements ICommandHandler
{
    use MemberCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->fetchMember($command->id);
        $member->setGender($command->gender);

        return $member->edit();
    }
}
