<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\CommandHandler\DisableCommandHandler;

class DisableMemberCommandHandler extends DisableCommandHandler
{
    use MemberCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchMember($id);
    }
}
