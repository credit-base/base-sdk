<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Member\Command\Member\UpdatePasswordMemberCommand;

class UpdatePasswordMemberCommandHandler implements ICommandHandler
{
    use MemberCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof UpdatePasswordMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->fetchMember($command->id);
        $member->setPassword($command->password);
        $member->setOldPassword($command->oldPassword);

        return $member->updatePassword();
    }
}
