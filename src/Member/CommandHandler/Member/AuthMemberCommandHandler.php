<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Repository\MemberRepository;
use Base\Sdk\Member\Command\Member\AuthMemberCommand;
use Base\Sdk\Member\Repository\MemberSessionRepository;

class AuthMemberCommandHandler implements ICommandHandler
{
    private $memberSessionRepository;

    private $memberRepository;

    public function __construct()
    {
        $this->memberSessionRepository = new MemberSessionRepository();
        $this->memberRepository = new MemberRepository();
    }

    public function __destruct()
    {
        unset($this->memberSessionRepository);
        unset($this->memberRepository);
    }

    protected function getMemberSessionRepository() : MemberSessionRepository
    {
        return $this->memberSessionRepository;
    }

    protected function getMemberRepository() : MemberRepository
    {
        return $this->memberRepository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AuthMemberCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->getMemberRepository()
                      ->scenario(MemberRepository::FETCH_ONE_MODEL_UN)
                      ->fetchOne($command->id);
        
        $member->setIdentify($command->identify);

        $this->getMemberSessionRepository()->save($member);

        $member = $this->getMemberSessionRepository()->get($command->id);
        return $member->validateIdentify($command->identify) && $this->registerGlobalMember($member);
    }

    private function registerGlobalMember(Member $member) : bool
    {
        Core::$container->set('member', $member);
        return true;
    }
}
