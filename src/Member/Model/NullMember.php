<?php
namespace Base\Sdk\Member\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Sdk\Common\Model\NullOperateAbleTrait;
use Base\Sdk\Common\Model\NullEnableAbleTrait;

class NullMember extends Member implements INull
{
    use NullEnableAbleTrait, NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function resetPassword() : bool
    {
        return $this->resourceNotExist();
    }

    public function updatePassword() : bool
    {
        return $this->resourceNotExist();
    }

    public function validateSecurity() : bool
    {
        return $this->resourceNotExist();
    }

    public function signIn() : bool
    {
        return $this->resourceNotExist();
    }

    public function signOut() : bool
    {
        return $this->resourceNotExist();
    }

    public function validateIdentify(string $identify) : bool
    {
        unset($identify);
        return $this->resourceNotExist();
    }
}
