<?php
namespace Base\Sdk\Member\Model;

use Marmot\Core;
use Marmot\Framework\Classes\Cookie;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Member\Repository\MemberRepository;
use Base\Sdk\Member\Repository\MemberSessionRepository;

use Base\Sdk\User\Model\User;

class Member extends User
{
    private $email;

    private $contactAddress;

    private $securityQa;
    
    private $repository;

    private $cookie;

    private $memberSessionRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->email = '';
        $this->contactAddress = '';
        $this->securityQa = new SecurityQA();
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new MemberRepository();
        $this->cookie = new Cookie();
        $this->memberSessionRepository = new MemberSessionRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->email);
        unset($this->contactAddress);
        unset($this->securityQa);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
        unset($this->cookie);
        unset($this->memberSessionRepository);
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setContactAddress(string $contactAddress): void
    {
        $this->contactAddress = $contactAddress;
    }

    public function getContactAddress(): string
    {
        return $this->contactAddress;
    }

    public function setSecurityQa(SecurityQA $securityQa) : void
    {
        $this->securityQa = $securityQa;
    }

    public function getSecurityQa(): SecurityQA
    {
        return $this->securityQa;
    }

    protected function getRepository(): MemberRepository
    {
        return $this->repository;
    }

    protected function getCookie() : Cookie
    {
        return $this->cookie;
    }

    protected function getMemberSessionRepository() : MemberSessionRepository
    {
        return $this->memberSessionRepository;
    }
    
    protected function isCellphoneExist() : bool
    {
        $filter = array();

        $filter['cellphone'] = $this->getCellphone();
        $sort = ['-updateTime'];

        list($memberList, $count) = $this->getRepository()->search($filter, $sort);
        unset($memberList);
        
        if (!empty($count)) {
            Core::setLastError(CELLPHONE_EXIST);
            return false;
        }

        return true;
    }

    protected function isEmailExist() : bool
    {
        $filter = array();

        $filter['email'] = $this->getEmail();
        $sort = ['-updateTime'];

        list($memberList, $count) = $this->getRepository()->search($filter, $sort);
        unset($memberList);
        
        if (!empty($count)) {
            Core::setLastError(EMAIL_EXIST);
            return false;
        }

        return true;
    }

    protected function isUserNameExist() : bool
    {
        $filter = array();

        $filter['userName'] = $this->getUserName();
        $sort = ['-updateTime'];

        list($memberList, $count) = $this->getRepository()->search($filter, $sort);
        unset($memberList);
        
        if (!empty($count)) {
            Core::setLastError(USER_NAME_EXIST);
            return false;
        }

        return true;
    }

    protected function addAction() : bool
    {
        return $this->isUserNameExist()
            && $this->isCellphoneExist()
            && $this->isEmailExist()
            && $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return $this->getRepository()->edit($this);
    }

    public function resetPassword() : bool
    {
        return $this->getRepository()->resetPassword($this);
    }

    public function updatePassword() : bool
    {
        return $this->getRepository()->updatePassword($this);
    }

    public function validateSecurity() : bool
    {
        return $this->getRepository()->validateSecurity($this);
    }
    
    //用户登录
    public function signIn() : bool
    {
        if ($this->getRepository()->signIn($this)) {
            $this->saveCookie() && $this->saveSession();
            return true;
        }

        return false;
    }

    //储存cookie
    protected function saveCookie() : bool
    {
        $cookie = $this->getCookie();
        $cookie->name = Core::$container->get('cookie.name');
        $cookie->value = $this->getId().':'.$this->generateIdentify();
        $cookie->expire = Core::$container->get('cookie.duration');

        return $cookie->add();
    }

    //储存session
    protected function saveSession() : bool
    {
        if ($this->getMemberSessionRepository()->save($this)) {
            Core::$container->set('member', $this->getMemberSessionRepository()->get($this->getId()));
            return true;
        }

        return false;
    }

    //用户退出
    public function signOut() : bool
    {
        return $this->clearCookie() && $this->clearSession();
    }

    //清除cookie
    protected function clearCookie() : bool
    {
        $cookie = $this->getCookie();
        $cookie->name = Core::$container->get('cookie.name');
        $cookie->value = 0;
        $cookie->expire = -1;

        return $cookie->add();
    }

    //清除session
    protected function clearSession() : bool
    {
        return $this->getMemberSessionRepository()->clear($this->getId());
    }

    //验证登录表示
    public function validateIdentify(string $identify) : bool
    {
        if ($this->getIdentify() != $identify) {
            $this->clearCookie();
            return false;
        }

         return true;
    }
}
