<?php
namespace Base\Sdk\Member\Model;

class SecurityQA
{
    const SECURITY_QUESTION = array(
        'NULL' => 0,
        'QUESTION_NAME' => 1,
        'QUESTION_MOVIE' => 2,
        'QUESTION_FRIEND' => 3,
        'QUESTION_HIGH_SCHOOL' => 4,
        'QUESTION_COLOR' => 5,
    );

    private $id;

    private $answer;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->answer = '';
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->answer);
    }

    public function setId($id) : void
    {
        $this->id = in_array(
            $id,
            self::SECURITY_QUESTION
        ) ? $id : self::SECURITY_QUESTION['NULL'];
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setAnswer(string $answer): void
    {
        $this->answer = $answer;
    }

    public function getAnswer(): string
    {
        return $this->answer;
    }
}
