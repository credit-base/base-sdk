<?php
namespace Base\Sdk\Member\Adapter\Member;

use Base\Sdk\Common\Adapter\EnableAbleMockAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleMockAdapterTrait;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Utils\MockFactory;

class MemberMockAdapter implements IMemberAdapter
{
    use OperateAbleMockAdapterTrait, EnableAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateMember($id);
    }

    public function fetchList(array $ids) : array
    {
        $memberList = array();

        foreach ($ids as $id) {
            $memberList[] = MockFactory::generateMember($id);
        }

        return $memberList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function signIn(Member $member) : bool
    {
        unset($member);
        return true;
    }

    public function updatePassword(Member $member) : bool
    {
        unset($member);
        return true;
    }

    public function resetPassword(Member $member) : bool
    {
        unset($member);
        return true;
    }

    public function validateSecurity(Member $member) : bool
    {
        unset($member);
        return true;
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateMember($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateMember($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
