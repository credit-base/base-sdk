<?php
namespace Base\Sdk\Member\Adapter\Member;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;

use Base\Sdk\Member\Model\Member;

interface IMemberAdapter extends IAsyncAdapter, IFetchAbleAdapter, IEnableAbleAdapter
{
    public function signIn(Member $member) : bool;

    public function resetPassword(Member $member) : bool;

    public function updatePassword(Member $member) : bool;

    public function validateSecurity(Member $member) : bool;
}
