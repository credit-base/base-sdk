<?php
namespace Base\Sdk\Member\Adapter\Member;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Model\NullMember;

use Base\Sdk\Member\Translator\MemberRestfulTranslator;

class MemberRestfulAdapter extends GuzzleAdapter implements IMemberAdapter
{
    use FetchAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'MEMBER_LIST'=>[
            'fields' => []
        ],
        'MEMBER_FETCH_ONE'=>[
            'fields'=>[]
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new MemberRestfulTranslator();
        $this->resource = 'members';
        $this->scenario = array();
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            10 => [
                'userName'=>NAME_NOT_EXIT,
                ''=>RESOURCE_NOT_EXIST
            ],
            101 => [
                'userName' => NAME_FORMAT_ERROR,
                'email' => SOURCE_FORMAT_ERROR,
                'contactAddress' => CONTACT_ADDRESS_FORMAT_ERROR,
                'securityQuestion' => SECURITY_QUESTION_FORMAT_ERROR,
                'securityAnswer' => SECURITY_ANSWER_FORMAT_ERROR,
                'gender' => GENDER_FORMAT_ERROR,
            ],
            102 =>[
                'status' => STATUS_CAN_NOT_MODIFY,
                'signInStatus'=>USER_STATUS_DISABLE,
            ],
            103 =>[
                'userName' => USER_NAME_EXIST,
                'cellphone' => CELLPHONE_EXIST,
                'email' => EMAIL_EXIST,
            ],
            104 =>[
                'securityAnswer' => SECURITY_ANSWER_INCORRECT
            ],
            501 => REAL_NAME_FORMAT_ERROR,
            502 => CELLPHONE_FORMAT_ERROR,
            503 => PASSWORD_FORMAT_ERROR,
            504 => CARDID_FORMAT_ERROR,
            505 =>[
                'password' => PASSWORD_INCORRECT,
                'oldPassword' => OLD_PASSWORD_INCORRECT,
            ],
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullMember::getInstance());
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function addAction(Member $member) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $member,
            array(
                'userName',
                'realName',
                'cardId',
                'cellphone',
                'email',
                'contactAddress',
                'securityAnswer',
                'password',
                'securityQuestion'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($member);
            return true;
        }

        return false;
    }

    protected function editAction(Member $member) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $member,
            array('gender')
        );

        $this->patch(
            $this->getResource().'/'.$member->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($member);
            return true;
        }

        return false;
    }

    public function signIn(Member $member) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $member,
            array(
                'userName','password'
            )
        );

        $this->post(
            $this->getResource().'/signIn',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($member);
            return true;
        }

        return false;
    }

    public function resetPassword(Member $member) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $member,
            array(
                'userName', 'password', 'securityAnswer'
            )
        );

        $this->patch(
            $this->getResource().'/resetPassword',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($member);
            return true;
        }

        return false;
    }

    public function updatePassword(Member $member) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $member,
            array(
                'password', 'oldPassword'
            )
        );

        $this->patch(
            $this->getResource().'/'.$member->getId().'/updatePassword',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($member);
            return true;
        }

        return false;
    }

    public function validateSecurity(Member $member) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $member,
            array(
                'securityAnswer'
            )
        );

        $this->patch(
            $this->getResource().'/'.$member->getId().'/validateSecurity',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($member);
            return true;
        }

        return false;
    }
}
