<?php
namespace Base\Sdk\Member\Adapter\Member\Query;

use Marmot\Framework\Query\DataCacheQuery;

use Base\Sdk\Member\Adapter\Member\Query\Persistence\MemberSessionCache;

class MemberSessionDataCacheQuery extends DataCacheQuery
{
    public function __construct()
    {
        parent::__construct(new MemberSessionCache());
    }
}
