<?php
namespace Base\Sdk\Member\Command\Member;

use Marmot\Interfaces\ICommand;

class EditMemberCommand implements ICommand
{
    public $gender;

    public $id;

    public function __construct(
        int $gender,
        int $id
    ) {
        $this->gender = $gender;
        $this->id = $id;
    }
}
