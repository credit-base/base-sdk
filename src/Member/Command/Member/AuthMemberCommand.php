<?php
namespace Base\Sdk\Member\Command\Member;

use Marmot\Interfaces\ICommand;

class AuthMemberCommand implements ICommand
{
    public $id;

    public $identify;

    public function __construct(
        int $id,
        string $identify
    ) {
        $this->id = $id;
        $this->identify = $identify;
    }
}
