<?php
namespace Base\Sdk\Member\WidgetRules;

use Marmot\Core;

use Respect\Validation\Validator as V;

use Base\Sdk\User\Model\User;
use Base\Sdk\Member\Model\SecurityQA;

class MemberWidgetRules
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    const USER_NAME_MIN_LENGTH = 2;
    const USER_NAME_MAX_LENGTH = 20;

    public function userName($userName) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::USER_NAME_MIN_LENGTH,
            self::USER_NAME_MAX_LENGTH
        )->validate($userName)) {
            Core::setLastError(USER_NAME_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    public function email($email) : bool
    {
        if (!V::email()->validate($email)) {
            Core::setLastError(EMAIL_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    const CONTACT_ADDRESS_MIN_LENGTH = 1;
    const CONTACT_ADDRESS_MAX_LENGTH = 255;

    public function contactAddress($contactAddress) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::CONTACT_ADDRESS_MIN_LENGTH,
            self::CONTACT_ADDRESS_MAX_LENGTH
        )->validate($contactAddress)) {
            Core::setLastError(CONTACT_ADDRESS_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    const SECURITY_ANSWER_MIN_LENGTH = 1;
    const SECURITY_ANSWER_MAX_LENGTH = 30;

    public function securityAnswer($securityAnswer) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::SECURITY_ANSWER_MIN_LENGTH,
            self::SECURITY_ANSWER_MAX_LENGTH
        )->validate($securityAnswer)) {
            Core::setLastError(SECURITY_ANSWER_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    public function securityQuestion($securityQuestion) : bool
    {
        if (!V::numeric()->positive()->validate($securityQuestion)
        || !in_array($securityQuestion, SecurityQA::SECURITY_QUESTION)
        ) {
            Core::setLastError(SECURITY_QUESTION_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    public function gender($gender) : bool
    {
        if (!V::numeric()->positive()->validate($gender)
        || !in_array($gender, User::GENDER)
        ) {
            Core::setLastError(GENDER_FORMAT_ERROR);
            return false;
        }

        return true;
    }
}
