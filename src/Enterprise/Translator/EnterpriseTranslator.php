<?php
namespace Base\Sdk\Enterprise\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Enterprise\Model\Enterprise;
use Base\Sdk\Enterprise\Model\NullEnterprise;

class EnterpriseTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $enterprise = null)
    {
        unset($enterprise);
        unset($expression);
        return new NullEnterprise();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($enterprise, array $keys = array())
    {
        if (!$enterprise instanceof Enterprise) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'unifiedSocialCreditCode',
                'establishmentDate',
                'approvalDate',
                'address',
                'registrationCapital',
                'businessTermStart',
                'businessTermTo',
                'businessScope',
                'registrationAuthority',
                'principal',
                'principalCardId',
                'registrationStatus',
                'enterpriseTypeCode',
                'enterpriseType',
                'industryCategory',
                'industryCode',
                'administrativeArea',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($enterprise->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $enterprise->getName();
        }
        if (in_array('unifiedSocialCreditCode', $keys)) {
            $expression['unifiedSocialCreditCode'] = $enterprise->getUnifiedSocialCreditCode();
        }
        if (in_array('establishmentDate', $keys)) {
            $expression['establishmentDate'] = $enterprise->getEstablishmentDate();
        }
        if (in_array('approvalDate', $keys)) {
            $expression['approvalDate'] = $enterprise->getApprovalDate();
        }
        if (in_array('address', $keys)) {
            $expression['address'] = $enterprise->getAddress();
        }
        if (in_array('registrationCapital', $keys)) {
            $expression['registrationCapital'] = $enterprise->getRegistrationCapital();
        }
        if (in_array('businessTermStart', $keys)) {
            $expression['businessTermStart'] = $enterprise->getBusinessTermStart();
        }
        if (in_array('businessTermTo', $keys)) {
            $expression['businessTermTo'] = $enterprise->getBusinessTermTo();
        }
        if (in_array('businessScope', $keys)) {
            $expression['businessScope'] = $enterprise->getBusinessScope();
        }
        if (in_array('registrationAuthority', $keys)) {
            $expression['registrationAuthority'] = $enterprise->getRegistrationAuthority();
        }
        if (in_array('principal', $keys)) {
            $expression['principal'] = $enterprise->getPrincipal();
        }
        if (in_array('principalCardId', $keys)) {
            $expression['principalCardId'] = $enterprise->getPrincipalCardId();
        }
        if (in_array('registrationStatus', $keys)) {
            $expression['registrationStatus'] = $enterprise->getRegistrationStatus();
        }
        if (in_array('enterpriseTypeCode', $keys)) {
            $expression['enterpriseTypeCode'] = $enterprise->getEnterpriseTypeCode();
        }
        if (in_array('enterpriseType', $keys)) {
            $expression['enterpriseType'] = $enterprise->getEnterpriseType();
        }
        if (in_array('industryCategory', $keys)) {
            $expression['industryCategory'] =
                INDUSTRY_CATEGORY_CN[$enterprise->getIndustryCategory()];
        }
        if (in_array('industryCode', $keys)) {
            $expression['industryCode'] = $enterprise->getIndustryCode();
        }
        if (in_array('administrativeArea', $keys)) {
            $expression['administrativeArea'] =
                ADMINISTRATIVE_AREA_CN[$enterprise->getAdministrativeArea()];
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($enterprise->getStatus()),
                'name' => IApplyCategory::STATUS_CN[$enterprise->getStatus()],
                'type' => IApplyCategory::STATUS_TAG_TYPE[$enterprise->getStatus()]
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $enterprise->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $enterprise->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $enterprise->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $enterprise->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $enterprise->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $enterprise->getCreateTime());
        }

        return $expression;
    }
}
