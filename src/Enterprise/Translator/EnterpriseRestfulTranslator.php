<?php
namespace Base\Sdk\Enterprise\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Enterprise\Model\Enterprise;
use Base\Sdk\Enterprise\Model\NullEnterprise;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

class EnterpriseRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $enterprise = null)
    {
        return $this->translateToObject($expression, $enterprise);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $enterprise = null)
    {
        if (empty($expression)) {
            return new NullEnterprise();
        }

        if ($enterprise == null) {
            $enterprise = new Enterprise();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $enterprise->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $enterprise->setName($attributes['name']);
        }

        if (isset($attributes['unifiedSocialCreditCode'])) {
            $enterprise->setUnifiedSocialCreditCode($attributes['unifiedSocialCreditCode']);
        }

        if (isset($attributes['establishmentDate'])) {
            $enterprise->setEstablishmentDate($attributes['establishmentDate']);
        }

        if (isset($attributes['approvalDate'])) {
            $enterprise->setApprovalDate($attributes['approvalDate']);
        }

        if (isset($attributes['address'])) {
            $enterprise->setAddress($attributes['address']);
        }

        if (isset($attributes['registrationCapital'])) {
            $enterprise->setRegistrationCapital($attributes['registrationCapital']);
        }

        if (isset($attributes['businessTermStart'])) {
            $enterprise->setBusinessTermStart($attributes['businessTermStart']);
        }

        if (isset($attributes['businessTermTo'])) {
            $enterprise->setBusinessTermTo($attributes['businessTermTo']);
        }

        if (isset($attributes['businessScope'])) {
            $enterprise->setBusinessScope($attributes['businessScope']);
        }

        if (isset($attributes['registrationAuthority'])) {
            $enterprise->setRegistrationAuthority($attributes['registrationAuthority']);
        }

        if (isset($attributes['principal'])) {
            $enterprise->setPrincipal($attributes['principal']);
        }

        if (isset($attributes['principalCardId'])) {
            $enterprise->setPrincipalCardId($attributes['principalCardId']);
        }

        if (isset($attributes['registrationStatus'])) {
            $enterprise->setRegistrationStatus($attributes['registrationStatus']);
        }

        if (isset($attributes['enterpriseTypeCode'])) {
            $enterprise->setEnterpriseTypeCode($attributes['enterpriseTypeCode']);
        }

        if (isset($attributes['enterpriseType'])) {
            $enterprise->setEnterpriseType($attributes['enterpriseType']);
        }

        if (isset($attributes['industryCategory'])) {
            $enterprise->setIndustryCategory($attributes['industryCategory']);
        }

        if (isset($attributes['industryCode'])) {
            $enterprise->setIndustryCode($attributes['industryCode']);
        }

        if (isset($attributes['administrativeArea'])) {
            $enterprise->setAdministrativeArea($attributes['administrativeArea']);
        }

        if (isset($attributes['createTime'])) {
            $enterprise->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['statusTime'])) {
            $enterprise->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['updateTime'])) {
            $enterprise->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['status'])) {
            $enterprise->setStatus($attributes['status']);
        }
        
        return $enterprise;
    }

    public function objectToArray($enterprise, array $keys = array())
    {
        unset($enterprise);
        unset($keys);
        return array();
    }
}
