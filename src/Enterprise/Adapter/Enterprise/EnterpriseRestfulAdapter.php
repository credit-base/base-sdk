<?php
namespace Base\Sdk\Enterprise\Adapter\Enterprise;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Base\Sdk\Enterprise\Model\Enterprise;
use Base\Sdk\Enterprise\Model\NullEnterprise;

use Base\Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

class EnterpriseRestfulAdapter extends GuzzleAdapter implements IEnterpriseAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'ENTERPRISE_LIST'=>[
            'fields'=>[],
            'include'=>''
        ],
        'ENTERPRISE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new EnterpriseRestfulTranslator();
        $this->resource = 'enterprises';
        $this->scenario = array();
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullEnterprise());
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }
}
