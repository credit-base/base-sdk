<?php
namespace Base\Sdk\Enterprise\Adapter\Enterprise;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IEnterpriseAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
