<?php
namespace Base\Sdk\Enterprise\Adapter\Enterprise;

use Base\Sdk\Enterprise\Utils\MockFactory;

class EnterpriseMockAdapter implements IEnterpriseAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateEnterprise($id);
    }

    public function fetchList(array $ids) : array
    {
        $enterpriseList = array();

        foreach ($ids as $id) {
            $enterpriseList[] = MockFactory::generateEnterprise($id);
        }

        return $enterpriseList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateEnterprise($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateEnterprise($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
