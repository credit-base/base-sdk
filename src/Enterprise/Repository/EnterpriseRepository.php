<?php
namespace Base\Sdk\Enterprise\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;

use Base\Sdk\Enterprise\Adapter\Enterprise\EnterpriseRestfulAdapter;
use Base\Sdk\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;
use Base\Sdk\Enterprise\Adapter\Enterprise\EnterpriseMockAdapter;

class EnterpriseRepository extends Repository implements IEnterpriseAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ENTERPRISE_LIST';
    const FETCH_ONE_MODEL_UN = 'ENTERPRISE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new EnterpriseRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new EnterpriseMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
