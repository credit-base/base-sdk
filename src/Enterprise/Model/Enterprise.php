<?php
namespace Base\Sdk\Enterprise\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Base\Sdk\Enterprise\Repository\EnterpriseRepository;

/**
 * @SuppressWarnings(PHPMD)
 */
class Enterprise implements IObject
{
    use Object;

    private $id;
    /**
     * [$name 企业名称]
     * @var string
     */
    private $name;
    /**
     * [$unifiedSocialCreditCode 统一社会信用代码]
     * @var string
     */
    private $unifiedSocialCreditCode;
    /**
     * [$establishmentDate 成立日期]
     * @var int
     */
    private $establishmentDate;
    /**
     * [$approvalDate 核准日期]
     * @var int
     */
    private $approvalDate;
    /**
     * [$address 住所/经营场所]
     * @var string
     */
    private $address;
    /**
     * [$registrationCapital 注册资本（万）]
     * @var string
     */
    private $registrationCapital;
    /**
     * [$businessTermStart 营业期限自]
     * @var int
     */
    private $businessTermStart;
    /**
     * [$businessTermTo 营业期限至]
     * @var int
     */
    private $businessTermTo;
    /**
     * [$businessScope 经营范围]
     * @var string
     */
    private $businessScope;
    /**
     * [$registrationAuthority 登记机关]
     * @var string
     */
    private $registrationAuthority;
    /**
     * [$principal 法定代表人/经营者]
     * @var string
     */
    private $principal;
    /**
     * [$principalCardId 法人身份证号]
     * @var string
     */
    private $principalCardId;
    /**
     * [$registrationStatus 登记状态]
     * @var string
     */
    private $registrationStatus;
    /**
     * [$enterpriseTypeCode 企业类型代码]
     * @var string
     */
    private $enterpriseTypeCode;
    /**
     * [$enterpriseType 企业类型]
     * @var string
     */
    private $enterpriseType;
    /**
     * [$industryCategory 行业门类]
     * @var string
     */
    private $industryCategory;
    /**
     * [$industryCode 行业代码]
     * @var string
     */
    private $industryCode;
    /**
     * [$administrativeArea 所属区域]
     * @var int
     */
    private $administrativeArea;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->unifiedSocialCreditCode = '';
        $this->establishmentDate = 0;
        $this->approvalDate = 0;
        $this->address = '';
        $this->registrationCapital = '';
        $this->businessTermStart = 0;
        $this->businessTermTo = 0;
        $this->businessScope = '';
        $this->registrationAuthority = '';
        $this->principal = '';
        $this->principalCardId = '';
        $this->registrationStatus = '';
        $this->enterpriseTypeCode = '';
        $this->enterpriseType = '';
        $this->industryCategory = '';
        $this->industryCode = '';
        $this->administrativeArea = 0;
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new EnterpriseRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->unifiedSocialCreditCode);
        unset($this->establishmentDate);
        unset($this->approvalDate);
        unset($this->address);
        unset($this->registrationCapital);
        unset($this->businessTermStart);
        unset($this->businessTermTo);
        unset($this->businessScope);
        unset($this->registrationAuthority);
        unset($this->principal);
        unset($this->principalCardId);
        unset($this->registrationStatus);
        unset($this->enterpriseTypeCode);
        unset($this->enterpriseType);
        unset($this->industryCategory);
        unset($this->industryCode);
        unset($this->administrativeArea);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setUnifiedSocialCreditCode(string $unifiedSocialCreditCode): void
    {
        $this->unifiedSocialCreditCode = $unifiedSocialCreditCode;
    }

    public function getUnifiedSocialCreditCode(): string
    {
        return $this->unifiedSocialCreditCode;
    }

    public function setEstablishmentDate(int $establishmentDate): void
    {
        $this->establishmentDate = $establishmentDate;
    }

    public function getEstablishmentDate(): int
    {
        return $this->establishmentDate;
    }

    public function setApprovalDate(int $approvalDate): void
    {
        $this->approvalDate = $approvalDate;
    }

    public function getApprovalDate(): int
    {
        return $this->approvalDate;
    }

    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setRegistrationCapital(string $registrationCapital): void
    {
        $this->registrationCapital = $registrationCapital;
    }

    public function getRegistrationCapital(): string
    {
        return $this->registrationCapital;
    }

    public function setBusinessTermStart(int $businessTermStart) : void
    {
        $this->businessTermStart = $businessTermStart;
    }

    public function getBusinessTermStart() : int
    {
        return $this->businessTermStart;
    }

    public function setBusinessTermTo(int $businessTermTo) : void
    {
        $this->businessTermTo = $businessTermTo;
    }

    public function getBusinessTermTo() : int
    {
        return $this->businessTermTo;
    }

    public function setBusinessScope(string $businessScope) : void
    {
        $this->businessScope = $businessScope;
    }

    public function getBusinessScope() : string
    {
        return $this->businessScope;
    }

    public function setRegistrationAuthority(string $registrationAuthority): void
    {
        $this->registrationAuthority = $registrationAuthority;
    }

    public function getRegistrationAuthority(): string
    {
        return $this->registrationAuthority;
    }

    public function setPrincipal(string $principal): void
    {
        $this->principal = $principal;
    }

    public function getPrincipal(): string
    {
        return $this->principal;
    }

    public function setPrincipalCardId(string $principalCardId): void
    {
        $this->principalCardId = $principalCardId;
    }

    public function getPrincipalCardId(): string
    {
        return $this->principalCardId;
    }

    public function setRegistrationStatus(string $registrationStatus): void
    {
        $this->registrationStatus = $registrationStatus;
    }

    public function getRegistrationStatus(): string
    {
        return $this->registrationStatus;
    }

    public function setEnterpriseTypeCode(string $enterpriseTypeCode): void
    {
        $this->enterpriseTypeCode = $enterpriseTypeCode;
    }

    public function getEnterpriseTypeCode(): string
    {
        return $this->enterpriseTypeCode;
    }

    public function setEnterpriseType(string $enterpriseType): void
    {
        $this->enterpriseType = $enterpriseType;
    }

    public function getEnterpriseType(): string
    {
        return $this->enterpriseType;
    }

    public function setIndustryCategory(string $industryCategory): void
    {
        $this->industryCategory = $industryCategory;
    }

    public function getIndustryCategory(): string
    {
        return $this->industryCategory;
    }

    public function setIndustryCode(string $industryCode): void
    {
        $this->industryCode = $industryCode;
    }

    public function getIndustryCode(): string
    {
        return $this->industryCode;
    }

    public function setAdministrativeArea(int $administrativeArea): void
    {
        $this->administrativeArea = $administrativeArea;
    }

    public function getAdministrativeArea(): int
    {
        return $this->administrativeArea;
    }

    protected function getRepository(): EnterpriseRepository
    {
        return $this->repository;
    }
}
