<?php
namespace Base\Sdk\Template\Command\BjTemplate;

use Base\Sdk\Template\Command\TemplateOperationCommand;

class BjTemplateOperationCommand extends TemplateOperationCommand
{
    public $sourceUnit;

    public $gbTemplate;

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function __construct(
        string $name,
        string $identify,
        string $description,
        array $subjectCategory,
        array $items,
        int $sourceUnit,
        int $gbTemplate,
        int $dimension,
        int $exchangeFrequency,
        int $infoClassify,
        int $infoCategory,
        int $id = 0
    ) {
        parent::__construct(
            $name,
            $identify,
            $description,
            $subjectCategory,
            $items,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $id
        );
 
        $this->sourceUnit = $sourceUnit;
        $this->gbTemplate = $gbTemplate;
    }
}
