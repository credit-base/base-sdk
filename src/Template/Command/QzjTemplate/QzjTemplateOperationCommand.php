<?php
namespace Base\Sdk\Template\Command\QzjTemplate;

use Base\Sdk\Template\Command\TemplateOperationCommand;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class QzjTemplateOperationCommand extends TemplateOperationCommand
{
    public $sourceUnit;

    public $category;

    public function __construct(
        string $name,
        string $identify,
        string $description,
        array $subjectCategory,
        array $items,
        int $sourceUnit,
        int $dimension,
        int $exchangeFrequency,
        int $infoClassify,
        int $infoCategory,
        int $category,
        int $id = 0
    ) {
        parent::__construct(
            $name,
            $identify,
            $description,
            $subjectCategory,
            $items,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $id
        );
        $this->sourceUnit = $sourceUnit;
        $this->category = $category;
    }
}
