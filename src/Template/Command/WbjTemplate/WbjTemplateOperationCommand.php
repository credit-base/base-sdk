<?php
namespace Base\Sdk\Template\Command\WbjTemplate;

use Base\Sdk\Template\Command\TemplateOperationCommand;

class WbjTemplateOperationCommand extends TemplateOperationCommand
{
    public $sourceUnit;

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function __construct(
        string $name,
        string $identify,
        string $description,
        array $subjectCategory,
        array $items,
        int $sourceUnit,
        int $dimension,
        int $exchangeFrequency,
        int $infoClassify,
        int $infoCategory,
        int $id = 0
    ) {
        parent::__construct(
            $name,
            $identify,
            $description,
            $subjectCategory,
            $items,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $id
        );
        $this->sourceUnit = $sourceUnit;
    }
}
