<?php
namespace Base\Sdk\Template\Command;

use Marmot\Interfaces\ICommand;

class TemplateOperationCommand implements ICommand
{
    public $id;

    public $name;

    public $identify;

    public $description;

    public $subjectCategory;

    public $dimension;

    public $exchangeFrequency;

    public $infoClassify;

    public $infoCategory;

    public $items;

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function __construct(
        string $name,
        string $identify,
        string $description,
        array $subjectCategory,
        array $items,
        int $dimension,
        int $exchangeFrequency,
        int $infoClassify,
        int $infoCategory,
        int $id = 0
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->identify = $identify;
        $this->description = $description;
        $this->subjectCategory = $subjectCategory;
        $this->items = $items;
        $this->dimension = $dimension;
        $this->exchangeFrequency = $exchangeFrequency;
        $this->infoClassify = $infoClassify;
        $this->infoCategory = $infoCategory;
    }
}
