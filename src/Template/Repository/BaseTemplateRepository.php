<?php
namespace Base\Sdk\Template\Repository;

use Marmot\Core;

use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Base\Sdk\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;
use Base\Sdk\Template\Adapter\BaseTemplate\BaseTemplateRestfulAdapter;

class BaseTemplateRepository implements IBaseTemplateAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperateAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'BASE_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'BASE_TEMPLATE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new BaseTemplateRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    protected function getAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
