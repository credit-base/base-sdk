<?php
namespace Base\Sdk\Template\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\Model\NullWbjTemplate;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

class WbjTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $wbjTemplate = null)
    {
        return $this->translateToObject($expression, $wbjTemplate);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }
    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $wbjTemplate = null)
    {
        if (empty($expression)) {
            return new NullWbjTemplate();
        }

        if ($wbjTemplate == null) {
            $wbjTemplate = new WbjTemplate();
        }
   
        $data = $expression['data'];

        $id = $data['id'];
        $wbjTemplate->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $wbjTemplate->setName($attributes['name']);
        }
        if (isset($attributes['identify'])) {
            $wbjTemplate->setIdentify($attributes['identify']);
        }
        if (isset($attributes['subjectCategory'])) {
            $wbjTemplate->setSubjectCategory($attributes['subjectCategory']);
        }
        if (isset($attributes['dimension'])) {
            $wbjTemplate->setDimension($attributes['dimension']);
        }
        if (isset($attributes['exchangeFrequency'])) {
            $wbjTemplate->setExchangeFrequency($attributes['exchangeFrequency']);
        }
        if (isset($attributes['infoClassify'])) {
            $wbjTemplate->setInfoClassify($attributes['infoClassify']);
        }
        if (isset($attributes['infoCategory'])) {
            $wbjTemplate->setInfoCategory($attributes['infoCategory']);
        }
        if (isset($attributes['description'])) {
            $wbjTemplate->setDescription($attributes['description']);
        }
        if (isset($attributes['items'])) {
            $wbjTemplate->setItems($attributes['items']);
        }
        if (isset($attributes['updateTime'])) {
            $wbjTemplate->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['createTime'])) {
            $wbjTemplate->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['ruleCount'])) {
            $wbjTemplate->setRuleCount($attributes['ruleCount']);
        }
        if (isset($attributes['dataTotal'])) {
            $wbjTemplate->setDataTotal($attributes['dataTotal']);
        }
        if (isset($attributes['category'])) {
            $wbjTemplate->setCategory($attributes['category']);
        }
        if (isset($attributes['userGroupCount'])) {
            $wbjTemplate->setUserGroupCount($attributes['userGroupCount']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['sourceUnit']['data'])) {
            $sourceUnit = $this->changeArrayFormat($relationships['sourceUnit']['data']);
            $wbjTemplate->setSourceUnit($this->getUserGroupRestfulTranslator()->arrayToObject($sourceUnit));
        }

        return $wbjTemplate;
    }
    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($wbjTemplate, array $keys = array())
    {
        if (!$wbjTemplate instanceof WbjTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'templates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $wbjTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $wbjTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $wbjTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $attributes['subjectCategory'] = $wbjTemplate->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $wbjTemplate->getDimension();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $attributes['exchangeFrequency'] = $wbjTemplate->getExchangeFrequency();
        }
        if (in_array('infoClassify', $keys)) {
            $attributes['infoClassify'] = $wbjTemplate->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $attributes['infoCategory'] = $wbjTemplate->getInfoCategory();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $wbjTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $wbjTemplate->getItems();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('sourceUnit', $keys)) {
            $expression['data']['relationships']['sourceUnit']['data'] = [
                [
                    "type" => "userGroups",
                    "id" => $wbjTemplate->getSourceUnit()->getId()
                ]
            ];
        }

        return $expression;
    }
}
