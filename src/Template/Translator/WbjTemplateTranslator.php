<?php
namespace Base\Sdk\Template\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\Model\NullWbjTemplate;

use Base\Sdk\ResourceCatalog\Model\ISearchData;

class WbjTemplateTranslator implements ITranslator
{
    use ConfigureFormatTrait;
    
    public function arrayToObject(array $expression, $wbjTemplate = null)
    {
        unset($wbjTemplate);
        unset($expression);
        return new NullWbjTemplate();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($wbjTemplate, array $keys = array())
    {
        if (!$wbjTemplate instanceof WbjTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'updateTime',
                'createTime',
                'ruleCount',
                'dataTotal'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($wbjTemplate->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $wbjTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $wbjTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $subjectCategory = $wbjTemplate->getSubjectCategory();
            $expression['subjectCategory'] = $this->getSubjectCategoryCn($subjectCategory);
        }
        if (in_array('exchangeFrequency', $keys)) {
            $expression['exchangeFrequency'] = [
                'id' => marmot_encode($wbjTemplate->getExchangeFrequency()),
                'name' => WbjTemplate::EXCHANGE_FREQUENCY_CN[$wbjTemplate->getExchangeFrequency()],
            ];
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = [
                'id' => marmot_encode($wbjTemplate->getDimension()),
                'name' => WbjTemplate::DIMENSION_CN[$wbjTemplate->getDimension()],
                'label' => ISearchData::DIMENSION_LABEL[$wbjTemplate->getDimension()],
                'type' => ISearchData::DIMENSION_TYPE[$wbjTemplate->getDimension()],
            ];
        }
        if (in_array('infoClassify', $keys)) {
            $expression['infoClassify'] = [
                'id' => marmot_encode($wbjTemplate->getInfoClassify()),
                'name' => WbjTemplate::INFO_CLASSIFY_CN[$wbjTemplate->getInfoClassify()],
                'label' => ISearchData::INFO_CLASSIFY_LABEL[$wbjTemplate->getInfoClassify()],
                'type' => ISearchData::INFO_CLASSIFY_TYPE[$wbjTemplate->getInfoClassify()],
            ];
        }
        if (in_array('infoCategory', $keys)) {
            $expression['infoCategory'] = [
                'id' => marmot_encode($wbjTemplate->getInfoCategory()),
                'name' => WbjTemplate::INFO_CATEGORY_CN[$wbjTemplate->getInfoCategory()],
                'label' => ISearchData::INFO_CATEGORY_LABEL[$wbjTemplate->getInfoCategory()],
                'type' => ISearchData::INFO_CATEGORY_TYPE[$wbjTemplate->getInfoCategory()],
            ];
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $wbjTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $expression['items'] = $this->getItemCn($wbjTemplate->getItems());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $wbjTemplate->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $wbjTemplate->getUpdateTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $wbjTemplate->getCreateTime();
        }

        if (in_array('sourceUnit', $keys)) {
            $expression['sourceUnit'] = [
                'id' => marmot_encode($wbjTemplate->getSourceUnit()->getId()),
                'name' => $wbjTemplate->getSourceUnit()->getName(),
            ];
        }
        if (in_array('ruleCount', $keys)) {
            $expression['ruleCount'] = $wbjTemplate->getRuleCount();
        }
        if (in_array('dataTotal', $keys)) {
            $expression['dataTotal'] = $wbjTemplate->getDataTotal();
        }

        return $expression;
    }
}
