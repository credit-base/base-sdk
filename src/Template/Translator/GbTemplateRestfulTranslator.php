<?php
namespace Base\Sdk\Template\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\Model\NullGbTemplate;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

class GbTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $gbTemplate = null)
    {
        return $this->translateToObject($expression, $gbTemplate);
    }
    /**
     * @SuppressWarnings(PHPMD)
    */
    protected function translateToObject(array $expression, $gbTemplate = null)
    {
        if (empty($expression)) {
            return new NullGbTemplate();
        }

        if ($gbTemplate == null) {
            $gbTemplate = new GbTemplate();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $gbTemplate->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $gbTemplate->setName($attributes['name']);
        }
        if (isset($attributes['identify'])) {
            $gbTemplate->setIdentify($attributes['identify']);
        }
        if (isset($attributes['subjectCategory'])) {
            $gbTemplate->setSubjectCategory($attributes['subjectCategory']);
        }
        if (isset($attributes['dimension'])) {
            $gbTemplate->setDimension($attributes['dimension']);
        }
        if (isset($attributes['exchangeFrequency'])) {
            $gbTemplate->setExchangeFrequency($attributes['exchangeFrequency']);
        }
        if (isset($attributes['infoClassify'])) {
            $gbTemplate->setInfoClassify($attributes['infoClassify']);
        }
        if (isset($attributes['infoCategory'])) {
            $gbTemplate->setInfoCategory($attributes['infoCategory']);
        }
        if (isset($attributes['description'])) {
            $gbTemplate->setDescription($attributes['description']);
        }
        if (isset($attributes['items'])) {
            $gbTemplate->setItems($attributes['items']);
        }
        if (isset($attributes['updateTime'])) {
            $gbTemplate->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['createTime'])) {
            $gbTemplate->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['ruleCount'])) {
            $gbTemplate->setRuleCount($attributes['ruleCount']);
        }
        if (isset($attributes['dataTotal'])) {
            $gbTemplate->setDataTotal($attributes['dataTotal']);
        }
        if (isset($attributes['category'])) {
            $gbTemplate->setCategory($attributes['category']);
        }
        if (isset($attributes['userGroupCount'])) {
            $gbTemplate->setUserGroupCount($attributes['userGroupCount']);
        }

        return $gbTemplate;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($gbTemplate, array $keys = array())
    {
        if (!$gbTemplate instanceof GbTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'gbTemplates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $gbTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $gbTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $gbTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $attributes['subjectCategory'] = $gbTemplate->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $gbTemplate->getDimension();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $attributes['exchangeFrequency'] = $gbTemplate->getExchangeFrequency();
        }
        if (in_array('infoClassify', $keys)) {
            $attributes['infoClassify'] = $gbTemplate->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $attributes['infoCategory'] = $gbTemplate->getInfoCategory();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $gbTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $gbTemplate->getItems();
        }

        $expression['data']['attributes'] = $attributes;
        
        return $expression;
    }
}
