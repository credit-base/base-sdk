<?php
namespace Base\Sdk\Template\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Template\Model\QzjTemplate;
use Base\Sdk\Template\Model\NullQzjTemplate;

use Base\Sdk\ResourceCatalog\Model\ISearchData;

class QzjTemplateTranslator implements ITranslator
{
    use ConfigureFormatTrait;
    
    const CATEGORY_CN = [
        QzjTemplate::CATEGORY['QZJ_WBJ'] => '前置机委办局',
        QzjTemplate::CATEGORY['QZJ_BJ'] => '前置机本级',
        QzjTemplate::CATEGORY['QZJ_GB'] => '前置机国标',
    ];
    
    public function arrayToObject(array $expression, $qzjTemplate = null)
    {
        unset($qzjTemplate);
        unset($expression);
        return new NullQzjTemplate();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($qzjTemplate, array $keys = array())
    {
        if (!$qzjTemplate instanceof QzjTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'updateTime',
                'createTime',
                'ruleCount',
                'dataTotal',
                'category'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($qzjTemplate->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $qzjTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $qzjTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $subjectCategory = $qzjTemplate->getSubjectCategory();
            $expression['subjectCategory'] = $this->getSubjectCategoryCn($subjectCategory);
        }
        if (in_array('exchangeFrequency', $keys)) {
            $expression['exchangeFrequency'] = [
                'id' => marmot_encode($qzjTemplate->getExchangeFrequency()),
                'name' => QzjTemplate::EXCHANGE_FREQUENCY_CN[$qzjTemplate->getExchangeFrequency()],
            ];
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = [
                'id' => marmot_encode($qzjTemplate->getDimension()),
                'name' => QzjTemplate::DIMENSION_CN[$qzjTemplate->getDimension()],
                'label' => ISearchData::DIMENSION_LABEL[$qzjTemplate->getDimension()],
                'type' => ISearchData::DIMENSION_TYPE[$qzjTemplate->getDimension()],
            ];
        }
        if (in_array('infoClassify', $keys)) {
            $expression['infoClassify'] = [
                'id' => marmot_encode($qzjTemplate->getInfoClassify()),
                'name' => QzjTemplate::INFO_CLASSIFY_CN[$qzjTemplate->getInfoClassify()],
                'label' => ISearchData::INFO_CLASSIFY_LABEL[$qzjTemplate->getInfoClassify()],
                'type' => ISearchData::INFO_CLASSIFY_TYPE[$qzjTemplate->getInfoClassify()],
            ];
        }
        if (in_array('infoCategory', $keys)) {
            $expression['infoCategory'] = [
                'id' => marmot_encode($qzjTemplate->getInfoCategory()),
                'name' => QzjTemplate::INFO_CATEGORY_CN[$qzjTemplate->getInfoCategory()],
                'label' => ISearchData::INFO_CATEGORY_LABEL[$qzjTemplate->getInfoCategory()],
                'type' => ISearchData::INFO_CATEGORY_TYPE[$qzjTemplate->getInfoCategory()],
            ];
        }
        if (in_array('category', $keys)) {
            $expression['category'] = [
                'id' => marmot_encode($qzjTemplate->getCategory()),
                'name' => self::CATEGORY_CN[$qzjTemplate->getCategory()]
            ];
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $qzjTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $expression['items'] = $this->getItemCn($qzjTemplate->getItems());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $qzjTemplate->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $qzjTemplate->getUpdateTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $qzjTemplate->getCreateTime();
        }

        if (in_array('sourceUnit', $keys)) {
            $expression['sourceUnit'] = [
                'id' => marmot_encode($qzjTemplate->getSourceUnit()->getId()),
                'name' => $qzjTemplate->getSourceUnit()->getName(),
            ];
        }
        if (in_array('ruleCount', $keys)) {
            $expression['ruleCount'] = $qzjTemplate->getRuleCount();
        }
        if (in_array('dataTotal', $keys)) {
            $expression['dataTotal'] = $qzjTemplate->getDataTotal();
        }

        return $expression;
    }
}
