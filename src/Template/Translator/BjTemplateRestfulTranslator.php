<?php
namespace Base\Sdk\Template\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\Template\Model\NullBjTemplate;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\Template\Translator\GbTemplateRestfulTranslator;

class BjTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $bjTemplate = null)
    {
        return $this->translateToObject($expression, $bjTemplate);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getGbTemplateRestfulTranslator(): GbTemplateRestfulTranslator
    {
        return new GbTemplateRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $bjTemplate = null)
    {
        if (empty($expression)) {
            return new NullBjTemplate();
        }

        if ($bjTemplate == null) {
            $bjTemplate = new BjTemplate();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $bjTemplate->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $bjTemplate->setName($attributes['name']);
        }
        if (isset($attributes['identify'])) {
            $bjTemplate->setIdentify($attributes['identify']);
        }
        if (isset($attributes['subjectCategory'])) {
            $bjTemplate->setSubjectCategory($attributes['subjectCategory']);
        }
        if (isset($attributes['exchangeFrequency'])) {
            $bjTemplate->setExchangeFrequency($attributes['exchangeFrequency']);
        }
        if (isset($attributes['dimension'])) {
            $bjTemplate->setDimension($attributes['dimension']);
        }
        if (isset($attributes['description'])) {
            $bjTemplate->setDescription($attributes['description']);
        }
        if (isset($attributes['infoClassify'])) {
            $bjTemplate->setInfoClassify($attributes['infoClassify']);
        }
        if (isset($attributes['infoCategory'])) {
            $bjTemplate->setInfoCategory($attributes['infoCategory']);
        }
        if (isset($attributes['items'])) {
            $bjTemplate->setItems($attributes['items']);
        }
        if (isset($attributes['updateTime'])) {
            $bjTemplate->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['createTime'])) {
            $bjTemplate->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['ruleCount'])) {
            $bjTemplate->setRuleCount($attributes['ruleCount']);
        }
        if (isset($attributes['dataTotal'])) {
            $bjTemplate->setDataTotal($attributes['dataTotal']);
        }
        if (isset($attributes['category'])) {
            $bjTemplate->setCategory($attributes['category']);
        }
        if (isset($attributes['userGroupCount'])) {
            $bjTemplate->setUserGroupCount($attributes['userGroupCount']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['gbTemplate']['data'])) {
            $gbTemplate = $this->changeArrayFormat($relationships['gbTemplate']['data']);
            $bjTemplate->setGbTemplate($this->getGbTemplateRestfulTranslator()->arrayToObject($gbTemplate));
        }
        if (isset($relationships['sourceUnit']['data'])) {
            $sourceUnit = $this->changeArrayFormat($relationships['sourceUnit']['data']);
            $bjTemplate->setSourceUnit($this->getUserGroupRestfulTranslator()->arrayToObject($sourceUnit));
        }

        return $bjTemplate;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($bjTemplate, array $keys = array())
    {
        if (!$bjTemplate instanceof BjTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'infoClassify',
                'sourceUnit',
                'gbTemplate',
                'infoCategory',
                'description',
                'items',
                'exchangeFrequency'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'bjTemplates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $bjTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $bjTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $bjTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $attributes['subjectCategory'] = $bjTemplate->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $bjTemplate->getDimension();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $bjTemplate->getItems();
        }
        if (in_array('infoClassify', $keys)) {
            $attributes['infoClassify'] = $bjTemplate->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $attributes['infoCategory'] = $bjTemplate->getInfoCategory();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $attributes['exchangeFrequency'] = $bjTemplate->getExchangeFrequency();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $bjTemplate->getDescription();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('sourceUnit', $keys)) {
            $expression['data']['relationships']['sourceUnit']['data'] = [
                [
                    "type" => "userGroups",
                    "id" => $bjTemplate->getSourceUnit()->getId()
                ]
            ];
        }

        if (in_array('gbTemplate', $keys)) {
            $expression['data']['relationships']['gbTemplate']['data'] = [
                [
                    "type" => "gbTemplates",
                    "id" => $bjTemplate->getGbTemplate()->getId()
                ]
            ];
        }
        
        return $expression;
    }
}
