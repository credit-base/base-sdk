<?php
namespace Base\Sdk\Template\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Template\Model\BaseTemplate;
use Base\Sdk\Template\Model\NullBaseTemplate;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

class BaseTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $baseTemplate = null)
    {
        return $this->translateToObject($expression, $baseTemplate);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $baseTemplate = null)
    {
        if (empty($expression)) {
            return new NullBaseTemplate();
        }

        if ($baseTemplate == null) {
            $baseTemplate = new BaseTemplate();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $baseTemplate->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $baseTemplate->setName($attributes['name']);
        }
        if (isset($attributes['identify'])) {
            $baseTemplate->setIdentify($attributes['identify']);
        }
        if (isset($attributes['subjectCategory'])) {
            $baseTemplate->setSubjectCategory($attributes['subjectCategory']);
        }
        if (isset($attributes['exchangeFrequency'])) {
            $baseTemplate->setExchangeFrequency($attributes['exchangeFrequency']);
        }
        if (isset($attributes['dimension'])) {
            $baseTemplate->setDimension($attributes['dimension']);
        }
        if (isset($attributes['description'])) {
            $baseTemplate->setDescription($attributes['description']);
        }
        if (isset($attributes['infoClassify'])) {
            $baseTemplate->setInfoClassify($attributes['infoClassify']);
        }
        if (isset($attributes['infoCategory'])) {
            $baseTemplate->setInfoCategory($attributes['infoCategory']);
        }
        if (isset($attributes['items'])) {
            $baseTemplate->setItems($attributes['items']);
        }
        if (isset($attributes['updateTime'])) {
            $baseTemplate->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['createTime'])) {
            $baseTemplate->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['ruleCount'])) {
            $baseTemplate->setRuleCount($attributes['ruleCount']);
        }
        if (isset($attributes['dataTotal'])) {
            $baseTemplate->setDataTotal($attributes['dataTotal']);
        }
        if (isset($attributes['category'])) {
            $baseTemplate->setCategory($attributes['category']);
        }
        if (isset($attributes['userGroupCount'])) {
            $baseTemplate->setUserGroupCount($attributes['userGroupCount']);
        }
        
        return $baseTemplate;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($baseTemplate, array $keys = array())
    {
        if (!$baseTemplate instanceof BaseTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'exchangeFrequency'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'baseTemplates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $baseTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $baseTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $baseTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $attributes['subjectCategory'] = $baseTemplate->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $baseTemplate->getDimension();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $baseTemplate->getItems();
        }
        if (in_array('infoClassify', $keys)) {
            $attributes['infoClassify'] = $baseTemplate->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $attributes['infoCategory'] = $baseTemplate->getInfoCategory();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $attributes['exchangeFrequency'] = $baseTemplate->getExchangeFrequency();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $baseTemplate->getDescription();
        }

        $expression['data']['attributes'] = $attributes;
        
        return $expression;
    }
}
