<?php
namespace Base\Sdk\Template\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\Model\NullGbTemplate;

use Base\Sdk\ResourceCatalog\Model\ISearchData;

class GbTemplateTranslator implements ITranslator
{
    use ConfigureFormatTrait;
    
    public function arrayToObject(array $expression, $gbTemplate = null)
    {
        unset($gbTemplate);
        unset($expression);
        return new NullGbTemplate();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($gbTemplate, array $keys = array())
    {
        if (!$gbTemplate instanceof GbTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'sourceUnit',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'updateTime',
                'createTime',
                'ruleCount',
                'dataTotal',
                'userGroupCount'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($gbTemplate->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $gbTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $gbTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $subjectCategory = $gbTemplate->getSubjectCategory();
            $expression['subjectCategory'] = $this->getSubjectCategoryCn($subjectCategory);
        }
        if (in_array('exchangeFrequency', $keys)) {
            $expression['exchangeFrequency'] = [
                'id' => marmot_encode($gbTemplate->getExchangeFrequency()),
                'name' => GbTemplate::EXCHANGE_FREQUENCY_CN[$gbTemplate->getExchangeFrequency()],
            ];
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = [
                'id' => marmot_encode($gbTemplate->getDimension()),
                'name' => GbTemplate::DIMENSION_CN[$gbTemplate->getDimension()],
                'label' => ISearchData::DIMENSION_LABEL[$gbTemplate->getDimension()],
                'type' => ISearchData::DIMENSION_TYPE[$gbTemplate->getDimension()],
            ];
        }
        if (in_array('infoClassify', $keys)) {
            $expression['infoClassify'] = [
                'id' => marmot_encode($gbTemplate->getInfoClassify()),
                'name' => GbTemplate::INFO_CLASSIFY_CN[$gbTemplate->getInfoClassify()],
                'label' => ISearchData::INFO_CLASSIFY_LABEL[$gbTemplate->getInfoClassify()],
                'type' => ISearchData::INFO_CLASSIFY_TYPE[$gbTemplate->getInfoClassify()],
            ];
        }
        if (in_array('infoCategory', $keys)) {
            $expression['infoCategory'] = [
                'id' => marmot_encode($gbTemplate->getInfoCategory()),
                'name' => GbTemplate::INFO_CATEGORY_CN[$gbTemplate->getInfoCategory()],
                'label' => ISearchData::INFO_CATEGORY_LABEL[$gbTemplate->getInfoCategory()],
                'type' => ISearchData::INFO_CATEGORY_TYPE[$gbTemplate->getInfoCategory()],
            ];
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $gbTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $items = $gbTemplate->getItems();
            $expression['items'] = $this->getItemCn($items);
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $gbTemplate->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $gbTemplate->getUpdateTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $gbTemplate->getCreateTime();
        }
        if (in_array('ruleCount', $keys)) {
            $expression['ruleCount'] = $gbTemplate->getRuleCount();
        }
        if (in_array('dataTotal', $keys)) {
            $expression['dataTotal'] = $gbTemplate->getDataTotal();
        }
        if (in_array('userGroupCount', $keys)) {
            $expression['userGroupCount'] = $gbTemplate->getUserGroupCount();
        }


        return $expression;
    }
}
