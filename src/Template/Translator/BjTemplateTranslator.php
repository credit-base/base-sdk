<?php
namespace Base\Sdk\Template\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\Template\Model\NullBjTemplate;

use Base\Sdk\ResourceCatalog\Model\ISearchData;

class BjTemplateTranslator implements ITranslator
{
    use ConfigureFormatTrait;
    
    public function arrayToObject(array $expression, $bjTemplate = null)
    {
        unset($bjTemplate);
        unset($expression);
        return new NullBjTemplate();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($bjTemplate, array $keys = array())
    {
        if (!$bjTemplate instanceof BjTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'sourceUnit',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'sourceUnit',
                'gbTemplate',
                'updateTime',
                'createTime',
                'items',
                'ruleCount',
                'dataTotal',
                'userGroupCount'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($bjTemplate->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $bjTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $bjTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $subjectCategory = $bjTemplate->getSubjectCategory();
            $expression['subjectCategory'] = $this->getSubjectCategoryCn($subjectCategory);
        }
        if (in_array('infoClassify', $keys)) {
            $expression['infoClassify'] = [
                'id' => marmot_encode($bjTemplate->getInfoClassify()),
                'name' => BjTemplate::INFO_CLASSIFY_CN[$bjTemplate->getInfoClassify()],
                'label' => ISearchData::INFO_CLASSIFY_LABEL[$bjTemplate->getInfoClassify()],
                'type' => ISearchData::INFO_CLASSIFY_TYPE[$bjTemplate->getInfoClassify()],
            ];
        }
        if (in_array('exchangeFrequency', $keys)) {
            $expression['exchangeFrequency'] = [
                'id' => marmot_encode($bjTemplate->getExchangeFrequency()),
                'name' => BjTemplate::EXCHANGE_FREQUENCY_CN[$bjTemplate->getExchangeFrequency()],
            ];
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = [
                'id' => marmot_encode($bjTemplate->getDimension()),
                'name' => BjTemplate::DIMENSION_CN[$bjTemplate->getDimension()],
                'label' => ISearchData::DIMENSION_LABEL[$bjTemplate->getDimension()],
                'type' => ISearchData::DIMENSION_TYPE[$bjTemplate->getDimension()],
            ];
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $bjTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $expression['items'] = $this->getItemCn($bjTemplate->getItems());
        }
        if (in_array('infoCategory', $keys)) {
            $expression['infoCategory'] = [
                'id' => marmot_encode($bjTemplate->getInfoCategory()),
                'name' => BjTemplate::INFO_CATEGORY_CN[$bjTemplate->getInfoCategory()],
                'label' => ISearchData::INFO_CATEGORY_LABEL[$bjTemplate->getInfoCategory()],
                'type' => ISearchData::INFO_CATEGORY_TYPE[$bjTemplate->getInfoCategory()],
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $bjTemplate->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $bjTemplate->getUpdateTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $bjTemplate->getCreateTime();
        }
        if (in_array('gbTemplate', $keys)) {
            $expression['gbTemplate'] = [
                'id' => marmot_encode($bjTemplate->getGbTemplate()->getId()),
                'name' => $bjTemplate->getGbTemplate()->getName(),
            ];
        }
        if (in_array('sourceUnit', $keys)) {
            $expression['sourceUnit'] = [
                'id' => marmot_encode($bjTemplate->getSourceUnit()->getId()),
                'name' => $bjTemplate->getSourceUnit()->getName(),
            ];
        }
        if (in_array('ruleCount', $keys)) {
            $expression['ruleCount'] = $bjTemplate->getRuleCount();
        }
        if (in_array('dataTotal', $keys)) {
            $expression['dataTotal'] = $bjTemplate->getDataTotal();
        }
        if (in_array('userGroupCount', $keys)) {
            $expression['userGroupCount'] = $bjTemplate->getUserGroupCount();
        }

        return $expression;
    }
}
