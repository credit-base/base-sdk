<?php
namespace Base\Sdk\Template\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Template\Model\QzjTemplate;
use Base\Sdk\Template\Model\NullQzjTemplate;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

class QzjTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $qzjTemplate = null)
    {
        return $this->translateToObject($expression, $qzjTemplate);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }
    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $qzjTemplate = null)
    {
        if (empty($expression)) {
            return new NullQzjTemplate();
        }

        if ($qzjTemplate == null) {
            $qzjTemplate = new QzjTemplate();
        }
   
        $data = $expression['data'];

        $id = $data['id'];
        $qzjTemplate->setId($id);
        
        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $qzjTemplate->setName($attributes['name']);
        }
        if (isset($attributes['identify'])) {
            $qzjTemplate->setIdentify($attributes['identify']);
        }
        if (isset($attributes['subjectCategory'])) {
            $qzjTemplate->setSubjectCategory($attributes['subjectCategory']);
        }
        if (isset($attributes['dimension'])) {
            $qzjTemplate->setDimension($attributes['dimension']);
        }
        if (isset($attributes['exchangeFrequency'])) {
            $qzjTemplate->setExchangeFrequency($attributes['exchangeFrequency']);
        }
        if (isset($attributes['infoClassify'])) {
            $qzjTemplate->setInfoClassify($attributes['infoClassify']);
        }
        if (isset($attributes['infoCategory'])) {
            $qzjTemplate->setInfoCategory($attributes['infoCategory']);
        }
        if (isset($attributes['category'])) {
            $qzjTemplate->setCategory($attributes['category']);
        }
        if (isset($attributes['description'])) {
            $qzjTemplate->setDescription($attributes['description']);
        }
        if (isset($attributes['items'])) {
            $qzjTemplate->setItems($attributes['items']);
        }
        if (isset($attributes['updateTime'])) {
            $qzjTemplate->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['createTime'])) {
            $qzjTemplate->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['ruleCount'])) {
            $qzjTemplate->setRuleCount($attributes['ruleCount']);
        }
        if (isset($attributes['dataTotal'])) {
            $qzjTemplate->setDataTotal($attributes['dataTotal']);
        }
        if (isset($attributes['userGroupCount'])) {
            $qzjTemplate->setUserGroupCount($attributes['userGroupCount']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['sourceUnit']['data'])) {
            $sourceUnit = $this->changeArrayFormat($relationships['sourceUnit']['data']);
            $qzjTemplate->setSourceUnit($this->getUserGroupRestfulTranslator()->arrayToObject($sourceUnit));
        }

        return $qzjTemplate;
    }
    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($qzjTemplate, array $keys = array())
    {
        if (!$qzjTemplate instanceof QzjTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'category',
                'description',
                'items',
                'sourceUnit'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'qzjTemplates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $qzjTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $qzjTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $qzjTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $attributes['subjectCategory'] = $qzjTemplate->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $qzjTemplate->getDimension();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $attributes['exchangeFrequency'] = $qzjTemplate->getExchangeFrequency();
        }
        if (in_array('infoClassify', $keys)) {
            $attributes['infoClassify'] = $qzjTemplate->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $attributes['infoCategory'] = $qzjTemplate->getInfoCategory();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $qzjTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $qzjTemplate->getItems();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $qzjTemplate->getCategory();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('sourceUnit', $keys)) {
            $expression['data']['relationships']['sourceUnit']['data'] = [
                [
                    "type" => "userGroups",
                    "id" => $qzjTemplate->getSourceUnit()->getId()
                ]
            ];
        }

        return $expression;
    }
}
