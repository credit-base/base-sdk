<?php
namespace Base\Sdk\Template\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Template\Model\BaseTemplate;
use Base\Sdk\Template\Model\NullBaseTemplate;

use Base\Sdk\ResourceCatalog\Model\ISearchData;

class BaseTemplateTranslator implements ITranslator
{
    use ConfigureFormatTrait;
    
    public function arrayToObject(array $expression, $baseTemplate = null)
    {
        unset($baseTemplate);
        unset($expression);
        return new NullBaseTemplate();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($baseTemplate, array $keys = array())
    {
        if (!$baseTemplate instanceof BaseTemplate) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'sourceUnit',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'sourceUnit',
                'gbTemplate',
                'updateTime',
                'createTime',
                'items',
                'ruleCount',
                'dataTotal',
                'userGroupCount',
                'category'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($baseTemplate->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $baseTemplate->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $baseTemplate->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $subjectCategory = $baseTemplate->getSubjectCategory();
            $expression['subjectCategory'] = $this->getSubjectCategoryCn($subjectCategory);
        }
        if (in_array('infoClassify', $keys)) {
            $expression['infoClassify'] = [
                'id' => marmot_encode($baseTemplate->getInfoClassify()),
                'name' => BaseTemplate::INFO_CLASSIFY_CN[$baseTemplate->getInfoClassify()],
                'label' => ISearchData::INFO_CLASSIFY_LABEL[$baseTemplate->getInfoClassify()],
                'type' => ISearchData::INFO_CLASSIFY_TYPE[$baseTemplate->getInfoClassify()],
            ];
        }
        if (in_array('exchangeFrequency', $keys)) {
            $expression['exchangeFrequency'] = [
                'id' => marmot_encode($baseTemplate->getExchangeFrequency()),
                'name' => BaseTemplate::EXCHANGE_FREQUENCY_CN[$baseTemplate->getExchangeFrequency()],
            ];
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = [
                'id' => marmot_encode($baseTemplate->getDimension()),
                'name' => BaseTemplate::DIMENSION_CN[$baseTemplate->getDimension()],
                'label' => ISearchData::DIMENSION_LABEL[$baseTemplate->getDimension()],
                'type' => ISearchData::DIMENSION_TYPE[$baseTemplate->getDimension()],
            ];
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $baseTemplate->getDescription();
        }
        if (in_array('items', $keys)) {
            $expression['items'] = $this->getItemCn($baseTemplate->getItems());
        }
        if (in_array('infoCategory', $keys)) {
            $expression['infoCategory'] = [
                'id' => marmot_encode($baseTemplate->getInfoCategory()),
                'name' => BaseTemplate::INFO_CATEGORY_CN[$baseTemplate->getInfoCategory()],
                'label' => ISearchData::INFO_CATEGORY_LABEL[$baseTemplate->getInfoCategory()],
                'type' => ISearchData::INFO_CATEGORY_TYPE[$baseTemplate->getInfoCategory()],
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $baseTemplate->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $baseTemplate->getUpdateTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $baseTemplate->getCreateTime();
        }

        if (in_array('ruleCount', $keys)) {
            $expression['ruleCount'] = $baseTemplate->getRuleCount();
        }
        if (in_array('dataTotal', $keys)) {
            $expression['dataTotal'] = $baseTemplate->getDataTotal();
        }
        if (in_array('userGroupCount', $keys)) {
            $expression['userGroupCount'] = $baseTemplate->getUserGroupCount();
        }
        if (in_array('category', $keys)) {
            $expression['category'] = marmot_encode($baseTemplate->getCategory());
        }

        return $expression;
    }
}
