<?php
namespace Base\Sdk\Template\Translator;

use Base\Sdk\Template\Model\Template;

trait ConfigureFormatTrait
{
    protected function getSubjectCategoryCn($subjectCategory)
    {
        $data = array();
        foreach ($subjectCategory as $key => $item) {
            $data[$key]['id'] = marmot_encode($item);
            $data[$key]['name'] = Template::SUBJECT_CATEGORY_CN[$item];
        }

        return $data;
    }
   
    protected function getItemCn($items)
    {
        foreach ($items as $key => $item) {
            $items[$key]['isNecessary']= $this->getIsNecessaryCn($item['isNecessary']);
            $items[$key]['isMasked'] = $this->getIsMaskedCn($item['isMasked']);
            $items[$key]['dimension'] = $this->getDimensionCn($item['dimension']);
            $items[$key]['type'] = $this->getTypeCn($item['type']);
        }

        return $items;
    }
   
    protected function getIsNecessaryCn($isNecessary)
    {
        $data = array(
            'id' => marmot_encode($isNecessary),
            'name' => Template::IS_NECESSARY_CN[$isNecessary]
        );

        return $data;
    }
   
    protected function getIsMaskedCn($isMasked)
    {
        $data = array(
            'id' => marmot_encode($isMasked),
            'name' => Template::IS_MASKED_CN[$isMasked]
        );

        return $data;
    }
    
    protected function getDimensionCn($dimension)
    {
        $data = array(
            'id' => marmot_encode($dimension),
            'name' => Template::DIMENSION_CN[$dimension]
        );

        return $data;
    }
    
    protected function getTypeCn($type)
    {
        $data = array(
            'id' => marmot_encode($type),
            'name' => Template::TYPE_CN[$type]
        );

        return $data;
    }
}
