<?php
namespace Base\Sdk\Template\Model;

use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Model\OperateAbleTrait;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

use Base\Sdk\Template\Repository\WbjTemplateRepository;
use Base\Sdk\UserGroup\Model\UserGroup;

class WbjTemplate extends Template implements IOperateAble
{
    use OperateAbleTrait;

    private $sourceUnit;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->sourceUnit = new UserGroup();
        $this->repository = new WbjTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->sourceUnit);
        unset($this->repository);
    }

    public function getRepository(): WbjTemplateRepository
    {
        return $this->repository;
    }

    public function setSourceUnit(UserGroup $sourceUnit) : void
    {
        $this->sourceUnit = $sourceUnit;
    }

    public function getSourceUnit(): UserGroup
    {
        return $this->sourceUnit;
    }
    
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }
}
