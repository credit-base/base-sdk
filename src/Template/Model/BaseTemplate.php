<?php
namespace Base\Sdk\Template\Model;

use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Model\OperateAbleTrait;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

use Base\Sdk\Template\Repository\BaseTemplateRepository;

class BaseTemplate extends Template implements IOperateAble
{
    use OperateAbleTrait;

    private $repository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->repository = new BaseTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->sourceUnit);
        unset($this->gbTemplate);
        unset($this->repository);
    }

    public function getRepository(): BaseTemplateRepository
    {
        return $this->repository;
    }

    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }
}
