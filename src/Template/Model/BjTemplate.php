<?php
namespace Base\Sdk\Template\Model;

use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Model\OperateAbleTrait;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

use Base\Sdk\Template\Repository\BjTemplateRepository;
use Base\Sdk\UserGroup\Model\UserGroup;

class BjTemplate extends Template implements IOperateAble
{
    use OperateAbleTrait;

    private $sourceUnit;

    private $gbTemplate;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->sourceUnit = new UserGroup();
        $this->gbTemplate = new GbTemplate();
        $this->repository = new BjTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->sourceUnit);
        unset($this->gbTemplate);
        unset($this->repository);
    }

    public function getRepository(): BjTemplateRepository
    {
        return $this->repository;
    }

    public function setSourceUnit(UserGroup $sourceUnit) : void
    {
        $this->sourceUnit = $sourceUnit;
    }

    public function getSourceUnit(): UserGroup
    {
        return $this->sourceUnit;
    }

    public function setGbTemplate(GbTemplate $gbTemplate): void
    {
        $this->gbTemplate = $gbTemplate;
    }

    public function getGbTemplate() : GbTemplate
    {
        return $this->gbTemplate;
    }

    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }
}
