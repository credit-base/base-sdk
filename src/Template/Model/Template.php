<?php
namespace Base\Sdk\Template\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

class Template implements IObject
{
    use Object;

    /**
     * 主体类别
     * @var SUBJECT_CATEGORY['FRJFFRZZ']  法人及非法人组织 1
     * @var SUBJECT_CATEGORY['ZRR']  自然人 2
     * @var SUBJECT_CATEGORY['GTGSH']  个体工商户 3
     */
    const SUBJECT_CATEGORY = array(
        'FRJFFRZZ' => 1,
        'ZRR' => 2,
        'GTGSH' => 3
    );

    const SUBJECT_CATEGORY_CN = array(
        0 => '',
        1 => '法人及非法人组织',
        2 => '自然人',
        3 => '个体工商户'
    );

    /**
     * 公开范围
     * @var DIMENSION['SHGK']  社会公开 1
     * @var DIMENSION['ZWGX']  政务共享 2
     * @var DIMENSION['SQCX']  授权查询 3
     */
    const DIMENSION = array(
        'SHGK' => 1,
        'ZWGX' => 2,
        'SQCX' => 3
    );

    const DIMENSION_CN = array(
        '' => '',
        0 => '',
        1 => '社会公开',
        2 => '政务共享',
        3 => '授权查询'
    );

    /**
     * 数据类型
     * @var TYPE['ZFX']  字符型
     * @var TYPE['RQX']  日期型
     * @var TYPE['ZSX']  整数型
     * @var TYPE['FDX']  浮点型
     * @var TYPE['MJX']  枚举型
     * @var TYPE['JHX']  集合型
     */
    const TYPE = array(
        'ZFX' => 1,
        'RQX' => 2,
        'ZSX' => 3,
        'FDX' => 4,
        'MJX' => 5,
        'JHX' => 6
    );

    const TYPE_CN = array(
        '' => '',
        0 => '',
        self::TYPE['ZFX'] => '字符型',
        self::TYPE['RQX'] => '日期型',
        self::TYPE['ZSX'] => '整数型',
        self::TYPE['FDX'] => '浮点型',
        self::TYPE['MJX'] => '枚举型',
        self::TYPE['JHX'] => '集合型'
    );

    /**
     * 是否必填
     * @var IS_NECESSARY['FOU']  否 0
     * @var IS_NECESSARY['SHI']  是 1，默认
     */
    const IS_NECESSARY = array(
        '' => '',
        'FOU' => 0,
        'SHI' => 1
    );

    const IS_NECESSARY_CN = array(
        '' => '',
        0 => '否',
        1 => '是',
    );

    /**
     * 是否脱敏
     * @var IS_MASKED['FOU']  否 0，默认
     * @var IS_MASKED['SHI']  是 1
     */
    const IS_MASKED = array(
        'FOU' => 0,
        'SHI' => 1
    );

    const IS_MASKED_CN = array(
        '' => '',
        0 => '否',
        1 => '是',
    );

    // 信息分类
    const INFO_CLASSIFY = array(
        'XZXK' => 1,
        'XZCF' => 2,
        'RMD' => 3,
        'HMD' => 4,
        'QT' => 5
    );

    const INFO_CLASSIFY_CN = array(
        0 => '',
        1 => '行政许可',
        2 => '行政处罚',
        3 => '红名单',
        4 => '黑名单',
        5 => '其他'
    );

    // 更新频率
    const EXCHANGE_FREQUENCY = array(
        'SS' => 1,
        'MR' => 2,
        'MZ' => 3,
        'MY' => 4,
        'MJD' => 5,
        'MBN' => 6,
        'MYN' => 7,
        'MLN' => 8,
        'MSN' => 9,
    );

    const EXCHANGE_FREQUENCY_CN = array(
        0 => '',
        1 => '实时',
        2 => '每日',
        3 => '每周',
        4 => '每月',
        5 => '每季度',
        6 => '每半年',
        7 => '每一年',
        8 => '每两年',
        9 => '每三年'
    );
        
    // 信息类别
    const INFO_CATEGORY = array(
        'JCXX' => 1,
        'SXXX' => 2,
        'SXXX' => 3,
        'QTXX' => 4
    );

    const INFO_CATEGORY_CN = array(
        0 => '',
        1 => '基础信息',
        2 => '守信信息',
        3 => '失信信息',
        4 => '其他信息'
    );

    const CATEGORY = [
        'QZJ_WBJ'=>20,
        'QZJ_BJ'=>21,
        'QZJ_GB'=>22,
    ];

    private $id;

    private $name;

    private $identify;

    private $subjectCategory;

    private $dimension;

    private $exchangeFrequency;

    private $infoClassify;

    private $infoCategory;

    private $description;

    private $items;

    private $ruleCount;

    private $dataTotal;

    private $category;

    private $userGroupCount;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->identify = '';
        $this->subjectCategory = array();
        $this->dimension = 0;
        $this->exchangeFrequency = 0;
        $this->infoClassify = 0;
        $this->infoCategory = 0;
        $this->description = '';
        $this->items = array();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->ruleCount = 0;
        $this->dataTotal = 0;
        $this->category = 0;
        $this->userGroupCount = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->identify);
        unset($this->subjectCategory);
        unset($this->dimension);
        unset($this->exchangeFrequency);
        unset($this->infoClassify);
        unset($this->infoCategory);
        unset($this->description);
        unset($this->items);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->ruleCount);
        unset($this->dataTotal);
        unset($this->category);
        unset($this->userGroupCount);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function setExchangeFrequency(int $exchangeFrequency): void
    {
        $this->exchangeFrequency = $exchangeFrequency;
    }

    public function getExchangeFrequency(): int
    {
        return $this->exchangeFrequency;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setIdentify(string $identify): void
    {
        $this->identify = $identify;
    }

    public function getIdentify(): string
    {
        return $this->identify;
    }

    public function setInfoCategory(int $infoCategory): void
    {
        $this->infoCategory = $infoCategory;
    }

    public function getInfoCategory(): int
    {
        return $this->infoCategory;
    }

    public function setInfoClassify(int $infoClassify): void
    {
        $this->infoClassify = $infoClassify;
    }

    public function getInfoClassify(): int
    {
        return $this->infoClassify;
    }

    public function setDimension(int $dimension): void
    {
        $this->dimension = $dimension;
    }

    public function getDimension(): int
    {
        return $this->dimension;
    }

    public function setSubjectCategory(array $subjectCategory): void
    {
        $this->subjectCategory = $subjectCategory;
    }

    public function getSubjectCategory(): array
    {
        return $this->subjectCategory;
    }

    public function setRuleCount(int $ruleCount): void
    {
        $this->ruleCount = $ruleCount;
    }

    public function getRuleCount(): int
    {
        return $this->ruleCount;
    }

    public function setDataTotal(int $dataTotal): void
    {
        $this->dataTotal = $dataTotal;
    }

    public function getDataTotal(): int
    {
        return $this->dataTotal;
    }

    public function setCategory(int $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): int
    {
        return $this->category;
    }

    public function setUserGroupCount(int $userGroupCount): void
    {
        $this->userGroupCount = $userGroupCount;
    }

    public function getUserGroupCount(): int
    {
        return $this->userGroupCount;
    }
}
