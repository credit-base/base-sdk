<?php
namespace Base\Sdk\Template\Model;

use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Model\OperateAbleTrait;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

use Base\Sdk\Template\Repository\GbTemplateRepository;

class GbTemplate extends Template implements IOperateAble
{
    use OperateAbleTrait;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->repository = new GbTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    public function getRepository(): GbTemplateRepository
    {
        return $this->repository;
    }

    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }
}
