<?php
namespace Base\Sdk\Template\WidgetRule;

use Respect\Validation\Validator as V;

use Marmot\Core;
use Base\Sdk\Template\Model\Template;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
*/
class TemplateWidgetRule
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function formatNumeric($data, string $pointer = '') : bool
    {
        if (!is_numeric($data)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }
    
    const NAME_MIN_LENGTH = 1;
    const NAME_MAX_LENGTH = 100;
    public function name($name) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::NAME_MIN_LENGTH,
            self::NAME_MAX_LENGTH
        )->validate($name)) {
            Core::setLastError(TEMPLATE_NAME_FORMAT_ERROR, array('pointer' => 'name'));
            return false;
        }
        return true;
    }

    public function identify($identify) : bool
    {
        $reg = '/^[A-Z][A-Z_]{0,98}[A-Z]$/';
        if (!preg_match($reg, $identify)) {
            Core::setLastError(IDENTIFY_FORMAT_ERROR, array('pointer' => 'identify'));
            return false;
        }
        return true;
    }

    public function subjectCategory($subjectCategory) : bool
    {
        if (empty($subjectCategory) || !is_array($subjectCategory)) {
            Core::setLastError(SUBJECT_CATEGORY_FORMAT_ERROR, array('pointer' => 'subjectCategory'));
            return false;
        }
        if (array_diff($subjectCategory, Template::SUBJECT_CATEGORY)) {
            Core::setLastError(SUBJECT_CATEGORY_FORMAT_ERROR, array('pointer' => 'subjectCategory'));
            return false;
        }
        return true;
    }

    public function dimension($dimension) : bool
    {
        if (!V::intVal()->validate($dimension) || !in_array($dimension, Template::DIMENSION)) {
            Core::setLastError(DIMENSION_FORMAT_ERROR, array('pointer' => 'dimension'));
            return false;
        }
        return true;
    }

    const DESCRIPTION_MIN_LENGTH = 1;
    const DESCRIPTION_MAX_LENGTH = 2000;
    public function description($description) : bool
    {
        if (!empty($description)) {
            if (!V::charset('UTF-8')->stringType()->length(
                self::DESCRIPTION_MIN_LENGTH,
                self::DESCRIPTION_MAX_LENGTH
            )->validate($description)) {
                Core::setLastError(DESCRIPTION_FORMAT_ERROR, array('pointer' => 'description'));
                return false;
            }
        }
        return true;
    }

    const ITEMS_KEYS = array(
        'name',
        'identify',
        'type',
        'length',
        'options',
        'dimension',
        'isNecessary',
        'isMasked',
        'maskRule',
        'remarks'
    );

    public function items($subjectCategory, $items) : bool
    {
        if (empty($items) || !is_array($items)) {
            Core::setLastError(ITEMS_FORMAT_ERROR, array('pointer' => 'items'));
            return false;
        }

        foreach ($items as $each) {
            if (!empty(array_diff(self::ITEMS_KEYS, array_keys($each)))) {
                Core::setLastError(ITEMS_FORMAT_ERROR, array('pointer' => 'items'));
                return false;
            }
            if (!$this->name($each['name'])) {
                return false;
            }
            if (!$this->identify($each['identify'])) {
                return false;
            }
            if (!$this->type($each['type'])) {
                return false;
            }
            if (!$this->length($each['type'], $each['length'])) {
                return false;
            }
            if (!$this->options($each['type'], $each['options'])) {
                return false;
            }
            if (!$this->dimension($each['dimension'])) {
                return false;
            }
            if (!$this->isNecessary($each['isNecessary'])) {
                return false;
            }
            if (!$this->isMasked($each['isMasked'])) {
                return false;
            }
            if (!$this->maskRule($each['isMasked'], $each['maskRule'])) {
                return false;
            }
            if (!$this->remarks($each['remarks'])) {
                return false;
            }

            $identifies[] = $each['identify'];
        }

        if (count(array_unique($identifies)) != count($identifies)) {
            Core::setLastError(ITEMS_FORMAT_ERROR, array('pointer' => 'identify'));
            return false;
        }
        if (!$this->identifyDefine($subjectCategory, $identifies)) {
            return false;
        }

        return true;
    }

    const IDENTIFY_DEFINE = array(
        'ZTMC' => 'ZTMC',
        'ZJHM' => 'ZJHM',
        'TYSHXYDM' => 'TYSHXYDM',
    );
    const FR = array(
        Template::SUBJECT_CATEGORY['FRJFFRZZ'],
        Template::SUBJECT_CATEGORY['GTGSH']
    );
    protected function identifyDefine($subjectCategory, $identifies) : bool
    {
        if (!in_array(self::IDENTIFY_DEFINE['ZTMC'], $identifies)) {
            Core::setLastError(ZTMC_FORMAT_ERROR, array('pointer' => 'ZTMC'));
            return false;
        }
        foreach ($subjectCategory as $value) {
            if ($value == Template::SUBJECT_CATEGORY['ZRR']) {
                if (!in_array(self::IDENTIFY_DEFINE['ZJHM'], $identifies)) {
                    Core::setLastError(CARDID_FORMAT_ERROR, array('pointer' => 'ZJHM'));
                    return false;
                }
            }
            if (in_array($value, self::FR)) {
                if (!in_array(self::IDENTIFY_DEFINE['TYSHXYDM'], $identifies)) {
                    Core::setLastError(TYSHXYDM_FORMAT_ERROR, array('pointer' => 'TYSHXYDM'));
                    return false;
                }
            }
        }
        return true;
    }

    protected function type($type) : bool
    {
        if (!in_array($type, Template::TYPE)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'type'));
            return false;
        }

        return true;
    }

    const TYPE_LENGTH = array(
        Template::TYPE['ZFX'],
        Template::TYPE['ZSX'],
        Template::TYPE['MJX'],
        Template::TYPE['JHX']
    );
    protected function length($type, $length) : bool
    {
        if (in_array($type, self::TYPE_LENGTH)) {
            if (!V::intVal()->validate($length)) {
                Core::setLastError(LENGTH_FORMAT_ERROR, array('pointer' => 'length'));
                return false;
            }
        }
        
        return true;
    }

    const TYPE_IS_ENUM = array(
        Template::TYPE['MJX'],
        Template::TYPE['JHX']
    );
    protected function options($type, $options) : bool
    {
        if (in_array($type, self::TYPE_IS_ENUM)) {
            if (empty($options) || !is_array($options)) {
                Core::setLastError(OPTIONS_FORMAT_ERROR, array('pointer' => 'options'));
                return false;
            }
        }
        if (!in_array($type, self::TYPE_IS_ENUM)) {
            if (!empty($options)) {
                Core::setLastError(OPTIONS_FORMAT_ERROR, array('pointer' => 'options'));
                return false;
            }
        }
        return true;
    }

    protected function isNecessary($isNecessary) : bool
    {
        if (!V::intVal()->validate($isNecessary) || !in_array($isNecessary, Template::IS_NECESSARY)) {
            Core::setLastError(IS_NECESSARY_FORMAT_ERROR, array('pointer' => 'isNecessary'));
            return false;
        }
        return true;
    }

    protected function isMasked($isMasked) : bool
    {
        if (!V::intVal()->validate($isMasked) || !in_array($isMasked, Template::IS_MASKED)) {
            Core::setLastError(IS_MASKED_FORMAT_ERROR, array('pointer' => 'isMasked'));
            return false;
        }
        return true;
    }

    protected function maskRule($isMasked, $maskRule) : bool
    {
        if ($isMasked == Template::IS_MASKED['SHI']) {
            if (empty($maskRule) || !is_array($maskRule)) {
                Core::setLastError(MASK_RULE_FORMAT_ERROR, array('pointer' => 'maskRule'));
                return false;
            }
        }
        if ($isMasked == Template::IS_MASKED['FOU']) {
            if (!empty($maskRule)) {
                Core::setLastError(MASK_RULE_FORMAT_ERROR, array('pointer' => 'maskRule'));
                return false;
            }
        }

        return true;
    }

    const REMARKS_MIN_LENGTH = 1;
    const REMARKS_MAX_LENGTH = 2000;
    public function remarks($remarks) : bool
    {
        if (!empty($remarks)) {
            if (!V::charset('UTF-8')->stringType()->length(
                self::REMARKS_MIN_LENGTH,
                self::REMARKS_MAX_LENGTH
            )->validate($remarks)) {
                Core::setLastError(REMARKS_FORMAT_ERROR, array('pointer' => 'remarks'));
                return false;
            }
        }
        return true;
    }

    public function category($category):bool
    {
        if (!in_array($category, Template::CATEGORY)) {
            Core::setLastError(CATEGORY_NOT_EXIST, array('pointer' => 'category'));
                return false;
        }

        return true;
    }

    public function infoCategory($infoCategory):bool
    {
        if (!in_array($infoCategory, Template::INFO_CATEGORY)) {
            Core::setLastError(INFO_CATEGORY_NOT_EXIST, array('pointer' => 'infoCategory'));
                return false;
        }

        return true;
    }
}
