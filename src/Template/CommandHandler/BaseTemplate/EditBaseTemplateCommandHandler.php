<?php
namespace Base\Sdk\Template\CommandHandler\BaseTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Template\Model\NullBaseTemplate;
use Base\Sdk\Template\Repository\BaseTemplateRepository;
use Base\Sdk\Template\Repository\GbTemplateRepository;
use Base\Sdk\Template\Command\BaseTemplate\EditBaseTemplateCommand;

use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class EditBaseTemplateCommandHandler implements ICommandHandler
{
    private $repository;

    private $baseTemplate;

    public function __construct()
    {
        $this->repository = new BaseTemplateRepository();
        $this->baseTemplate = new NullBaseTemplate();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->userGroupRepository);
        unset($this->gbTemplateRepository);
        unset($this->bjTemplate);
    }

    protected function getRepository() : BaseTemplateRepository
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof EditBaseTemplateCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $this->baseTemplate = $this->getRepository()->fetchOne($command->id);

        $this->baseTemplate->setName($command->name);
        $this->baseTemplate->setIdentify($command->identify);
        $this->baseTemplate->setDescription($command->description);
        $this->baseTemplate->setSubjectCategory($command->subjectCategory);
        $this->baseTemplate->setDimension($command->dimension);
        $this->baseTemplate->setExchangeFrequency($command->exchangeFrequency);
        $this->baseTemplate->setInfoClassify($command->infoClassify);
        $this->baseTemplate->setInfoCategory($command->infoCategory);
        $this->baseTemplate->setItems($command->items);

        return $this->baseTemplate->edit();
    }
}
