<?php
namespace Base\Sdk\Template\CommandHandler\QzjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Template\Model\NullQzjTemplate;
use Base\Sdk\Template\Repository\QzjTemplateRepository;
use Base\Sdk\Template\Command\QzjTemplate\EditQzjTemplateCommand;

use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class EditQzjTemplateCommandHandler implements ICommandHandler
{
    private $repository;

    private $qzjTemplate;

    private $userGroupRepository;

    public function __construct()
    {
        $this->repository = new QzjTemplateRepository();
        $this->userGroupRepository = new UserGroupRepository();
        $this->qzjTemplate = new NullQzjTemplate();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->userGroupRepository);
        unset($this->qzjTemplate);
    }

    protected function getRepository() : QzjTemplateRepository
    {
        return $this->repository;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof EditQzjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $this->qzjTemplate = $this->getRepository()->fetchOne($command->id);

        $this->qzjTemplate->setName($command->name);
        $this->qzjTemplate->setIdentify($command->identify);
        $this->qzjTemplate->setDescription($command->description);
        $this->qzjTemplate->setSubjectCategory($command->subjectCategory);
        $this->qzjTemplate->setDimension($command->dimension);
        $this->qzjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $this->qzjTemplate->setInfoClassify($command->infoClassify);
        $this->qzjTemplate->setInfoCategory($command->infoCategory);
        $this->qzjTemplate->setCategory($command->category);
        $this->qzjTemplate->setItems($command->items);
        $this->qzjTemplate->setSourceUnit(
            $this->getUserGroupRepository()->fetchOne($command->sourceUnit)
        );

        return $this->qzjTemplate->edit();
    }
}
