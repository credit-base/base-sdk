<?php
namespace Base\Sdk\Template\CommandHandler\QzjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Template\Model\QzjTemplate;
use Base\Sdk\Template\Command\QzjTemplate\AddQzjTemplateCommand;

use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class AddQzjTemplateCommandHandler implements ICommandHandler
{
    private $qzjTemplate;

    private $userGroupRepository;

    public function __construct()
    {
        $this->qzjTemplate = new QzjTemplate();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->qzjTemplate);
        unset($this->userGroupRepository);
    }

    protected function getQzjTemplate() : QzjTemplate
    {
        return $this->qzjTemplate;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ICommand $command)
    {
        if (!($command instanceof AddQzjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $qzjTemplate = $this->getQzjTemplate();
 
        $qzjTemplate->setName($command->name);
        $qzjTemplate->setIdentify($command->identify);
        $qzjTemplate->setDescription($command->description);
        $qzjTemplate->setSubjectCategory($command->subjectCategory);
        $qzjTemplate->setItems($command->items);
        $qzjTemplate->setDimension($command->dimension);
        $qzjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $qzjTemplate->setInfoClassify($command->infoClassify);
        $qzjTemplate->setInfoCategory($command->infoCategory);
        $qzjTemplate->setCategory($command->category);
        $qzjTemplate->setSourceUnit(
            $this->getUserGroupRepository()->fetchOne($command->sourceUnit)
        );

        return $qzjTemplate->add();
    }
}
