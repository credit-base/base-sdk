<?php
namespace Base\Sdk\Template\CommandHandler\GbTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Template\Command\GbTemplate\AddGbTemplateCommand;
use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class AddGbTemplateCommandHandler implements ICommandHandler
{
    private $gbTemplate;

    public function __construct()
    {
        $this->gbTemplate = new GbTemplate();
    }

    public function __destruct()
    {
        unset($this->gbTemplate);
    }

    protected function getGbTemplate() : GbTemplate
    {
        return $this->gbTemplate;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ICommand $command)
    {
        if (!($command instanceof AddGbTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $gbTemplate = $this->getGbTemplate();
 
        $gbTemplate->setName($command->name);
        $gbTemplate->setIdentify($command->identify);
        $gbTemplate->setDescription($command->description);
        $gbTemplate->setSubjectCategory($command->subjectCategory);
        $gbTemplate->setItems($command->items);
        $gbTemplate->setDimension($command->dimension);
        $gbTemplate->setExchangeFrequency($command->exchangeFrequency);
        $gbTemplate->setInfoClassify($command->infoClassify);
        $gbTemplate->setInfoCategory($command->infoCategory);

        return $gbTemplate->add();
    }
}
