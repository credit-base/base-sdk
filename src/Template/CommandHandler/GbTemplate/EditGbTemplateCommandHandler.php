<?php
namespace Base\Sdk\Template\CommandHandler\GbTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Template\Model\NullGbTemplate;
use Base\Sdk\Template\Command\GbTemplate\EditGbTemplateCommand;
use Base\Sdk\Template\Repository\GbTemplateRepository;

class EditGbTemplateCommandHandler implements ICommandHandler
{
    private $repository;

    private $gbTemplate;

    public function __construct()
    {
        $this->repository = new GbTemplateRepository();
        $this->gbTemplate = new NullGbTemplate();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->gbTemplate);
    }

    protected function getRepository(): GbTemplateRepository
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof EditGbTemplateCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $this->gbTemplate = $this->getRepository()->fetchOne($command->id);

        $this->gbTemplate->setName($command->name);
        $this->gbTemplate->setIdentify($command->identify);
        $this->gbTemplate->setDescription($command->description);
        $this->gbTemplate->setSubjectCategory($command->subjectCategory);
        $this->gbTemplate->setDimension($command->dimension);
        $this->gbTemplate->setExchangeFrequency($command->exchangeFrequency);
        $this->gbTemplate->setInfoClassify($command->infoClassify);
        $this->gbTemplate->setInfoCategory($command->infoCategory);
        $this->gbTemplate->setItems($command->items);

        return $this->gbTemplate->edit();
    }
}
