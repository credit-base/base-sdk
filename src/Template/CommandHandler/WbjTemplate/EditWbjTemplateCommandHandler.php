<?php
namespace Base\Sdk\Template\CommandHandler\wbjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Template\Model\NullWbjTemplate;
use Base\Sdk\Template\Repository\WbjTemplateRepository;
use Base\Sdk\Template\Command\wbjTemplate\EditWbjTemplateCommand;

use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class EditWbjTemplateCommandHandler implements ICommandHandler
{
    private $repository;

    private $wbjTemplate;

    private $userGroupRepository;

    public function __construct()
    {
        $this->repository = new WbjTemplateRepository();
        $this->userGroupRepository = new UserGroupRepository();
        $this->wbjTemplate = new NullWbjTemplate();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->userGroupRepository);
        unset($this->wbjTemplate);
    }

    protected function getRepository() : WbjTemplateRepository
    {
        return $this->repository;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof EditWbjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $this->wbjTemplate = $this->getRepository()->fetchOne($command->id);

        $this->wbjTemplate->setName($command->name);
        $this->wbjTemplate->setIdentify($command->identify);
        $this->wbjTemplate->setDescription($command->description);
        $this->wbjTemplate->setSubjectCategory($command->subjectCategory);
        $this->wbjTemplate->setDimension($command->dimension);
        $this->wbjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $this->wbjTemplate->setInfoClassify($command->infoClassify);
        $this->wbjTemplate->setInfoCategory($command->infoCategory);
        $this->wbjTemplate->setItems($command->items);
        $this->wbjTemplate->setSourceUnit(
            $this->getUserGroupRepository()->fetchOne($command->sourceUnit)
        );

        return $this->wbjTemplate->edit();
    }
}
