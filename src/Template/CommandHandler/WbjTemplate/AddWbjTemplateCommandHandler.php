<?php
namespace Base\Sdk\Template\CommandHandler\WbjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\Command\WbjTemplate\AddWbjTemplateCommand;

use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class AddWbjTemplateCommandHandler implements ICommandHandler
{
    private $wbjTemplate;

    private $userGroupRepository;

    public function __construct()
    {
        $this->wbjTemplate = new WbjTemplate();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->wbjTemplate);
        unset($this->userGroupRepository);
    }

    protected function getWbjTemplate() : WbjTemplate
    {
        return $this->wbjTemplate;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ICommand $command)
    {
        if (!($command instanceof AddWbjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $wbjTemplate = $this->getWbjTemplate();
 
        $wbjTemplate->setName($command->name);
        $wbjTemplate->setIdentify($command->identify);
        $wbjTemplate->setDescription($command->description);
        $wbjTemplate->setSubjectCategory($command->subjectCategory);
        $wbjTemplate->setItems($command->items);
        $wbjTemplate->setDimension($command->dimension);
        $wbjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $wbjTemplate->setInfoClassify($command->infoClassify);
        $wbjTemplate->setInfoCategory($command->infoCategory);
        $wbjTemplate->setSourceUnit(
            $this->getUserGroupRepository()->fetchOne($command->sourceUnit)
        );

        return $wbjTemplate->add();
    }
}
