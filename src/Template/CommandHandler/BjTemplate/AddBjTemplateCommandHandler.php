<?php
namespace Base\Sdk\Template\CommandHandler\BjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\Template\Command\BjTemplate\AddBjTemplateCommand;
use Base\Sdk\Template\Repository\GbTemplateRepository;

use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class AddBjTemplateCommandHandler implements ICommandHandler
{
    private $bjTemplate;

    private $userGroupRepository;

    public function __construct()
    {
        $this->bjTemplate = new BjTemplate();
        $this->gbTemplateRepository = new GbTemplateRepository();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->bjTemplate);
        unset($this->gbTemplateRepository);
        unset($this->userGroupRepository);
    }

    protected function getBjTemplate() : BjTemplate
    {
        return $this->bjTemplate;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getGbTemplateRepository() : GbTemplateRepository
    {
        return $this->gbTemplateRepository;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ICommand $command)
    {
        if (!($command instanceof AddBjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $bjTemplate = $this->getBjTemplate();
 
        $bjTemplate->setName($command->name);
        $bjTemplate->setIdentify($command->identify);
        $bjTemplate->setDescription($command->description);
        $bjTemplate->setSubjectCategory($command->subjectCategory);
        $bjTemplate->setItems($command->items);
        $bjTemplate->setDimension($command->dimension);
        $bjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $bjTemplate->setInfoClassify($command->infoClassify);
        $bjTemplate->setInfoCategory($command->infoCategory);
        $bjTemplate->setSourceUnit(
            $this->getUserGroupRepository()->fetchOne($command->sourceUnit)
        );
        $bjTemplate->setGbTemplate(
            $this->getGbTemplateRepository()->fetchOne($command->gbTemplate)
        );

        return $bjTemplate->add();
    }
}
