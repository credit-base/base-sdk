<?php
namespace Base\Sdk\Template\CommandHandler\BjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Template\Model\NullBjTemplate;
use Base\Sdk\Template\Repository\BjTemplateRepository;
use Base\Sdk\Template\Repository\GbTemplateRepository;
use Base\Sdk\Template\Command\BjTemplate\EditBjTemplateCommand;

use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class EditBjTemplateCommandHandler implements ICommandHandler
{
    private $repository;

    private $bjTemplate;

    private $userGroupRepository;

    private $gbTemplateRepository;

    public function __construct()
    {
        $this->repository = new BjTemplateRepository();
        $this->userGroupRepository = new UserGroupRepository();
        $this->gbTemplateRepository = new GbTemplateRepository();
        $this->bjTemplate = new NullBjTemplate();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->userGroupRepository);
        unset($this->gbTemplateRepository);
        unset($this->bjTemplate);
    }

    protected function getRepository() : BjTemplateRepository
    {
        return $this->repository;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getGbTemplateRepository() : GbTemplateRepository
    {
        return $this->gbTemplateRepository;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof EditBjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $this->bjTemplate = $this->getRepository()->fetchOne($command->id);

        $this->bjTemplate->setName($command->name);
        $this->bjTemplate->setIdentify($command->identify);
        $this->bjTemplate->setDescription($command->description);
        $this->bjTemplate->setSubjectCategory($command->subjectCategory);
        $this->bjTemplate->setDimension($command->dimension);
        $this->bjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $this->bjTemplate->setInfoClassify($command->infoClassify);
        $this->bjTemplate->setInfoCategory($command->infoCategory);
        $this->bjTemplate->setItems($command->items);
        $this->bjTemplate->setSourceUnit(
            $this->getUserGroupRepository()->fetchOne($command->sourceUnit)
        );
        $this->bjTemplate->setGbTemplate(
            $this->getGbTemplateRepository()->fetchOne($command->gbTemplate)
        );

        return $this->bjTemplate->edit();
    }
}
