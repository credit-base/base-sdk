<?php
namespace Base\Sdk\Template\Adapter\QzjTemplate;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

interface IQzjTemplateAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperateAbleAdapter
{
}
