<?php
namespace Base\Sdk\Template\Adapter\QzjTemplate;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;

use Base\Sdk\Template\Model\QzjTemplate;
use Base\Sdk\Template\Model\NullQzjTemplate;
use Base\Sdk\Template\Translator\QzjTemplateRestfulTranslator;

class QzjTemplateRestfulAdapter extends GuzzleAdapter implements IQzjTemplateAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'QZJ_TEMPLATE_LIST'=>[
            'fields' => [],
            'include' => 'sourceUnit'
        ],
        'QZJ_TEMPLATE_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'sourceUnit'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new QzjTemplateRestfulTranslator();
        $this->resource = 'qzjTemplates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            101=>[
                'qzjTemplate' => QZJ_TEMPLATE_EXIST,
                'name' => TEMPLATE_NAME_FORMAT_ERROR,
                'identify'=>IDENTIFY_FORMAT_ERROR,
                'subjectCategory'=>SUBJECT_CATEGORY_FORMAT_ERROR,
                'dimension' => DIMENSION_FORMAT_ERROR,
                'infoClassify'=>INFO_CLASSIFY_FORMAT_ERROR,
                'infoCategory'=>INFO_CATEGORY_NOT_EXIST,
                'description'=>DESCRIPTION_FORMAT_ERROR,
                'exchangeFrequency'=>EXCHANGE_FREQUENCY_FORMAT_ERROR,
                'sourceUnit'=>SOURCE_UNIT_FORMAT_ERROR,
                'items'=>ITEMS_FORMAT_ERROR,
                'ZTMC' => ZTMC_FORMAT_ERROR,
                'ZJHM' => CARDID_FORMAT_ERROR,
                'TYSHXYDM' => TYSHXYDM_FORMAT_ERROR,
                'type' => ITEM_TYPE_FORMAT_ERROR,
                'length' => ITEM_LENGTH_FORMAT_ERROR,
                'options' => ITEM_OPTIONS_FORMAT_ERROR,
                'isNecessary' => IS_NECESSARY_FORMAT_ERROR,
                'isMasked' => IS_MASKED_FORMAT_ERROR,
                'maskRule' => MASK_RULE_FORMAT_ERROR,
                'remarks' => REMARKS_FORMAT_ERROR
            ],
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullQzjTemplate());
    }

    protected function addAction(QzjTemplate $qzjTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $qzjTemplate,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'category',
                'description',
                'items',
                'sourceUnit'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($qzjTemplate);
            return true;
        }

        return false;
    }

    protected function editAction(QzjTemplate $qzjTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $qzjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'category',
                'description',
                'items',
                'sourceUnit'
            )
        );

        $this->patch(
            $this->getResource().'/'.$qzjTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($qzjTemplate);
            return true;
        }

        return false;
    }
}
