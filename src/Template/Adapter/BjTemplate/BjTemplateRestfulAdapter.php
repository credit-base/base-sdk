<?php
namespace Base\Sdk\Template\Adapter\BjTemplate;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;

use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\Template\Model\NullBjTemplate;
use Base\Sdk\Template\Translator\BjTemplateRestfulTranslator;

class BjTemplateRestfulAdapter extends GuzzleAdapter implements IBjTemplateAdapter
{
    use FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait, OperateAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'BJTEMPLATE_LIST'=>[
            'fields' => [],
            'include' => 'sourceUnit,gbTemplate'
        ],
        'BJTEMPLATE_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'sourceUnit,gbTemplate'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new BjTemplateRestfulTranslator();
        $this->resource = 'bjTemplates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullBjTemplate());
    }

    protected function addAction(BjTemplate $bjTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $bjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'gbTemplate'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($bjTemplate);
            return true;
        }

        return false;
    }

    protected function editAction(BjTemplate $bjTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $bjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'gbTemplate'
            )
        );

        $this->patch(
            $this->getResource().'/'.$bjTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($bjTemplate);
            return true;
        }

        return false;
    }
}
