<?php
namespace Base\Sdk\Template\Adapter\BjTemplate;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

interface IBjTemplateAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperateAbleAdapter
{

}
