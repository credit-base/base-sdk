<?php
namespace Base\Sdk\Template\Adapter\GbTemplate;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\Model\NullGbTemplate;
use Base\Sdk\Template\Translator\GbTemplateRestfulTranslator;

class GbTemplateRestfulAdapter extends GuzzleAdapter implements IGbTemplateAdapter
{
    use CommonMapErrorsTrait, FetchAbleRestfulAdapterTrait,
    AsyncFetchAbleRestfulAdapterTrait, OperateAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'GBTEMPLATE_LIST'=>[
            'fields' => []
        ],
        'GBTEMPLATE_FETCH_ONE'=>[
            'fields' => []
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new GbTemplateRestfulTranslator();
        $this->resource = 'gbTemplates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullGbTemplate());
    }

    protected function addAction(GbTemplate $gbTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $gbTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($gbTemplate);
            return true;
        }

        return false;
    }

    protected function editAction(GbTemplate $gbTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $gbTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items'
            )
        );

        $this->patch(
            $this->getResource().'/'.$gbTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($gbTemplate);
            return true;
        }

        return false;
    }
}
