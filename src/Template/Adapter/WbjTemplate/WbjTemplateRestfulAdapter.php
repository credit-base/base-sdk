<?php
namespace Base\Sdk\Template\Adapter\WbjTemplate;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;

use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\Model\NullWbjTemplate;
use Base\Sdk\Template\Translator\WbjTemplateRestfulTranslator;

class WbjTemplateRestfulAdapter extends GuzzleAdapter implements IWbjTemplateAdapter
{
    use FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait, OperateAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'WBJTEMPLATE_LIST'=>[
            'fields' => [],
            'include' => 'sourceUnit'
        ],
        'WBJTEMPLATE_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'sourceUnit'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new WbjTemplateRestfulTranslator();
        $this->resource = 'templates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullWbjTemplate());
    }

    protected function addAction(WbjTemplate $wbjTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $wbjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($wbjTemplate);
            return true;
        }

        return false;
    }

    protected function editAction(WbjTemplate $wbjTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $wbjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit'
            )
        );

        $this->patch(
            $this->getResource().'/'.$wbjTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($wbjTemplate);
            return true;
        }

        return false;
    }
}
