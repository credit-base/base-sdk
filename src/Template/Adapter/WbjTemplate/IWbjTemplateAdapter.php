<?php
namespace Base\Sdk\Template\Adapter\WbjTemplate;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

interface IWbjTemplateAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperateAbleAdapter
{
}
