<?php
namespace Base\Sdk\Template\Adapter\BaseTemplate;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;

use Base\Sdk\Template\Model\BaseTemplate;
use Base\Sdk\Template\Model\NullBaseTemplate;
use Base\Sdk\Template\Translator\BaseTemplateRestfulTranslator;

class BaseTemplateRestfulAdapter extends GuzzleAdapter implements IBaseTemplateAdapter
{
    use FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait, OperateAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'BASE_TEMPLATE_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'BASE_TEMPLATE_FETCH_ONE'=>[
            'fields' => [],
            'include' => ''
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new BaseTemplateRestfulTranslator();
        $this->resource = 'baseTemplates';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullBaseTemplate());
    }

    protected function addAction(BaseTemplate $baseTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $baseTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($baseTemplate);
            return true;
        }

        return false;
    }

    protected function editAction(BaseTemplate $baseTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $baseTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items'
            )
        );

        $this->patch(
            $this->getResource().'/'.$baseTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($baseTemplate);
            return true;
        }

        return false;
    }
}
