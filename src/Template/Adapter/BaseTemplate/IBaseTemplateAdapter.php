<?php
namespace Base\Sdk\Template\Adapter\BaseTemplate;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

interface IBaseTemplateAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperateAbleAdapter
{

}
