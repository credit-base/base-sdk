<?php
namespace Base\Sdk\Purview\Model;

interface IPurviewAble
{
    const CATEGORY = array(
        'USER_GROUP' => 1,// 委办局
        'CREW' => 2,// 员工
        'GB_TEMPLATE' => 3,// 国标目录管理
        'BJ_TEMPLATE' => 4,// 本级目录管理
        'WBJ_TEMPLATE' => 5,// 委办局目录管理
        'RULE_MANAGE' => 6,// 目录规则管理
        'GB_DATA_MANAGE' => 7,// 国标数据管理
        'BJ_DATA_MANAGEMENT' => 8,// 本级数据管理
        'WBJ_DATA_MANAGE' => 9,// 委办局数据管理
        'EXCHANGE_SHARING_RULE' => 10,// 共享规则管理
        'NEWS' => 11,// 新闻管理
        'UNAUDITED_NEWS' => 12,// 新闻审核
        'MEMBER' => 13,// 前台用户管理
        'JOURNAL' => 14,// 信用刊物管理
        'UNAUDITED_JOURNAL' => 15,// 信用刊物审核
        'CREDIT_PHOTOGRAPHY' => 16,// 信用随手拍
        'QZJ_DATA' => 17,// 前置机数据管理
        'UNAUDITED_RULE_MANAGE' => 18,// 目录规则审核
        'BASE_RULE_MANAGE' => 19,// 基础目录规则管理
        'UNAUDITED_BASE_RULE_MANAGE' => 20,// 基础目录规则审核
        'ENTERPRISE' => 21,// 企业信息
        'QZJ_TEMPLATE' => 22,// 前置机目录管理
        'COMPLAINT' => 23,// 信用投诉管理
        'PRAISE' => 24,// 信用表扬管理
        'QA' => 25,// 信用问答管理
        'APPEAL' => 26,// 异议申诉管理
        'FEEDBACK' => 27,// 问题反馈管理
        'UN_AUDITED_COMPLAINT' => 28,// 信用投诉审核
        'UN_AUDITED_PRAISE' => 29,// 信用表扬审核
        'UN_AUDITED_QA' => 30,// 信用问答审核
        'UN_AUDITED_APPEAL' => 31,// 异议申诉审核
        'UN_AUDITED_FEEDBACK' => 32,// 问题反馈审核
    );
}
