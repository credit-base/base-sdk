<?php
namespace Base\Sdk\Purview\Model;

class PurviewCategoryFactory
{
    const MAPS = array(
        'userGroups'=>IPurviewAble::CATEGORY['USER_GROUP'],
        'departments'=>IPurviewAble::CATEGORY['USER_GROUP'],
        'crews'=>IPurviewAble::CATEGORY['CREW'],
        'gbTemplates'=>IPurviewAble::CATEGORY['GB_TEMPLATE'],
        'bjTemplates'=>IPurviewAble::CATEGORY['BJ_TEMPLATE'],
        'wbjTemplates'=>IPurviewAble::CATEGORY['WBJ_TEMPLATE'],
        'news'=>IPurviewAble::CATEGORY['NEWS'],
        'unAuditedNews'=>IPurviewAble::CATEGORY['UNAUDITED_NEWS'],
        'members'=>IPurviewAble::CATEGORY['MEMBER'],
        'journals'=>IPurviewAble::CATEGORY['JOURNAL'],
        'unAuditedJournals'=>IPurviewAble::CATEGORY['UNAUDITED_JOURNAL'],
        'creditPhotography'=>IPurviewAble::CATEGORY['CREDIT_PHOTOGRAPHY'],
        'gbSearchData'=>IPurviewAble::CATEGORY['GB_DATA_MANAGE'],
        'bjSearchData'=>IPurviewAble::CATEGORY['BJ_DATA_MANAGEMENT'],
        'wbjSearchData'=>IPurviewAble::CATEGORY['WBJ_DATA_MANAGE'],
        'rules'=>IPurviewAble::CATEGORY['RULE_MANAGE'],
        'unAuditedRules'=>IPurviewAble::CATEGORY['UNAUDITED_RULE_MANAGE'],
        'baseRules'=>IPurviewAble::CATEGORY['BASE_RULE_MANAGE'],
        'unAuditedBaseRules'=>IPurviewAble::CATEGORY['UNAUDITED_BASE_RULE_MANAGE'],
        'gbTasks'=>IPurviewAble::CATEGORY['GB_DATA_MANAGE'],
        'bjTasks'=>IPurviewAble::CATEGORY['BJ_DATA_MANAGEMENT'],
        'wbjTasks'=>IPurviewAble::CATEGORY['WBJ_DATA_MANAGE'],
        'errorData'=>IPurviewAble::CATEGORY['WBJ_DATA_MANAGE'],
        'parentTasks'=>IPurviewAble::CATEGORY['WBJ_TEMPLATE'],
        'workOrderTasks'=>IPurviewAble::CATEGORY['WBJ_TEMPLATE'],
        'enterprises'=>IPurviewAble::CATEGORY['ENTERPRISE'],
        'qzjTemplates'=>IPurviewAble::CATEGORY['QZJ_TEMPLATE'],

        'complaints'=>IPurviewAble::CATEGORY['COMPLAINT'],
        'praises'=>IPurviewAble::CATEGORY['PRAISE'],
        'qas'=>IPurviewAble::CATEGORY['QA'],
        'appeals'=>IPurviewAble::CATEGORY['APPEAL'],
        'feedbacks'=>IPurviewAble::CATEGORY['FEEDBACK'],
        'unAuditedComplaints'=>IPurviewAble::CATEGORY['UN_AUDITED_COMPLAINT'],
        'unAuditedPraises'=>IPurviewAble::CATEGORY['UN_AUDITED_PRAISE'],
        'unAuditedQas'=>IPurviewAble::CATEGORY['UN_AUDITED_QA'],
        'unAuditedAppeals'=>IPurviewAble::CATEGORY['UN_AUDITED_APPEAL'],
        'unAuditedFeedbacks'=>IPurviewAble::CATEGORY['UN_AUDITED_FEEDBACK'],
    );

    public static function getCategory(string $resource) : int
    {
        return isset(self::MAPS[$resource]) ? self::MAPS[$resource] : 0;
    }
}
