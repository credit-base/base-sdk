<?php
namespace Base\Sdk\Statistical\Adapter\Statistical;

use Marmot\Interfaces\INull;

use Base\Sdk\Statistical\Model\NullStatistical;
use Base\Sdk\Statistical\Model\Statistical;

class NullStatisticalAdapter implements INull
{
    public function analyse(array $filter = array()) : Statistical
    {
        unset($filter);
        return new NullStatistical();
    }
}
