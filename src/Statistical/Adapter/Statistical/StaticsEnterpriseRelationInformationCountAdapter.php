<?php
namespace Base\Sdk\Statistical\Adapter\Statistical;

use Base\Sdk\Statistical\Model\Statistical;
use Base\Sdk\Statistical\Model\NullStatistical;

class StaticsEnterpriseRelationInformationCountAdapter extends StatisticalRestfulAdapter
{
    public function analyse(array $filter = array()) : Statistical
    {
        $this->get(
            $this->getResource() . '/enterpriseRelationInformationCount',
            array('filter'=>$filter)
        );

        return $this->isSuccess() ? $this->translateToObject() : new NullStatistical();
    }

    public function analyseAsync(array $filter = array())
    {
        return $this->getAsync(
            $this->getResource() . '/enterpriseRelationInformationCount',
            array('filter'=>$filter)
        );
    }
}
