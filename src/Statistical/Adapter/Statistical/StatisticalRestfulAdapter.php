<?php
namespace Base\Sdk\Statistical\Adapter\Statistical;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Base\Sdk\Statistical\Model\NullStatistical;
use Base\Sdk\Statistical\Translator\StatisticalRestfulTranslator;

class StatisticalRestfulAdapter extends GuzzleAdapter implements IStatisticalAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [];

    public function __construct()
    {
        parent::__construct(
            Core::$container->get('baseSdk.url')
        );
        $this->translator = new StatisticalRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'statisticals';
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullStatistical());
    }
}
