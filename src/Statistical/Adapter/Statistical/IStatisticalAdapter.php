<?php
namespace Base\Sdk\Statistical\Adapter\Statistical;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IStatisticalAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
}
