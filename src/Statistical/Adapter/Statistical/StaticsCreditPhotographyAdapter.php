<?php
namespace Base\Sdk\Statistical\Adapter\Statistical;

use Base\Sdk\Statistical\Model\Statistical;
use Base\Sdk\Statistical\Model\NullStatistical;

class StaticsCreditPhotographyAdapter extends StatisticalRestfulAdapter
{
    public function analyse(array $filter = array()) : Statistical
    {
        $this->get('/statisticals/staticsCreditPhotography', array('filter'=>$filter));

        return $this->isSuccess() ? $this->translateToObject() : new NullStatistical();
    }

    public function analyseAsync(array $filter = array())
    {
        return $this->getAsync('/statisticals/staticsCreditPhotograph', array('filter'=>$filter));
    }
}
