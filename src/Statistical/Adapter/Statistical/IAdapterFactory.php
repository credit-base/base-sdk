<?php
namespace Base\Sdk\Statistical\Adapter\Statistical;

use Base\Sdk\Statistical\Adapter\Statistical\NullStatisticalAdapter;

class IAdapterFactory
{
    const MAPS = array(
        'enterpriseRelationInformationCount'=>
            'Base\Sdk\Statistical\Adapter\Statistical\StaticsEnterpriseRelationInformationCountAdapter',
    );

    public function getAdapter(string $type)
    {
        $adapter = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($adapter) ? new $adapter : new NullStatisticalAdapter();
    }
}
