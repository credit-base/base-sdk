<?php
namespace Base\Sdk\Statistical\Translator;

interface IStatisticalTranslator
{
    public function objectToArray($object);
    public function arrayToObject(array $expression, $object = null);
    public function arrayToObjects(array $expression) : array;
}
