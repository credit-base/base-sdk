<?php
namespace Base\Sdk\Statistical\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Statistical\Model\NullStatistical;
use Base\Sdk\Statistical\Model\Statistical;

class StaticsEnterpriseRelationInformationCountTranslator implements ITranslator, IStatisticalTranslator
{
    public function arrayToObject(array $expression, $statistical = null)
    {
        unset($statistical);
        unset($expression);
        return new NullStatistical();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }
    
    public function objectToArray($statistical, array $keys = array())
    {
        if (!$statistical instanceof Statistical) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'result',
            );
        }

        $expression = array();
        
        if (in_array('result', $keys)) {
            $expression = $statistical->getResult();
        }
        
        return $expression;
    }
}
