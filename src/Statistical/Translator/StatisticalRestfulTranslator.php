<?php
namespace Base\Sdk\Statistical\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Statistical\Model\Statistical;
use Base\Sdk\Statistical\Model\NullStatistical;

use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

class StatisticalRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $statistical = null)
    {
        return $this->translateToObject($expression, $statistical);
    }

    protected function translateToObject(array $expression, $statistical = null)
    {
        if (empty($expression)) {
            return new NullStatistical();
        }
       
        if ($statistical == null) {
            $statistical = new Statistical();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $statistical->setId($id);

        if (isset($data['attributes'])) {
            $attributes = $data['attributes'];

            if (isset($attributes['result'])) {
                $statistical->setResult($attributes['result']);
            }
        }
        
        return $statistical;
    }

    public function objectToArray($statistical, array $keys = array())
    {
        unset($statistical);
        unset($keys);
        return array();
    }
}
