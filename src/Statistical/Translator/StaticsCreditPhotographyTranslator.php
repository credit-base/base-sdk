<?php
namespace Base\Sdk\Statistical\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Statistical\Model\NullStatistical;
use Base\Sdk\Statistical\Model\Statistical;

class StaticsCreditPhotographyTranslator implements ITranslator, IStatisticalTranslator
{
    public function arrayToObject(array $expression, $statistical = null)
    {
        unset($statistical);
        unset($expression);
        return new NullStatistical();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }
    
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($statistical, array $keys = array())
    {
        if (!$statistical instanceof Statistical) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'result',
            );
        }

        $expression = array();
        
        $result = $statistical->getResult();
        $pendingTotal = empty($result['pendingTotal']) ? 0 : $result['pendingTotal'];
        $approveTotal = empty($result['approveTotal']) ? 0 : $result['approveTotal'];
        $rejectTotal = empty($result['rejectTotal']) ? 0 : $result['rejectTotal'];

        $expression = [
            'pendingTotal'=> $pendingTotal,
            'approveTotal'=>$approveTotal,
            'rejectTotal'=>$rejectTotal
        ];
        
        return $expression;
    }
}
