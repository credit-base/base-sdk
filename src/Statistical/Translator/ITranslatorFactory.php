<?php
namespace Base\Sdk\Statistical\Translator;

use Base\Sdk\Statistical\Translator\NullStatisticalTranslator;

class ITranslatorFactory
{
    const MAPS = array(
        'enterpriseRelationInformationCount'=>
            'Base\Sdk\Statistical\Translator\StaticsEnterpriseRelationInformationCountTranslator',
    );

    public function getTranslator(string $type)
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : new NullStatisticalTranslator();
    }
}
