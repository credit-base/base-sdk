<?php
namespace Base\Sdk\Common\Command;

use Marmot\Interfaces\ICommand;

abstract class CancelTopCommand implements ICommand
{
    public $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
