<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\ITopAble;

interface ITopAbleAdapter
{
    public function top(ITopAble $topAbleObject) : bool;

    public function cancelTop(ITopAble $topAbleObject) : bool;
}
