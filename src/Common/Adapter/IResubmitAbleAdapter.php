<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IResubmitAble;

interface IResubmitAbleAdapter
{
    public function resubmit(IResubmitAble $resubmitAbleObject) : bool;
}
