<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IApplyAble;

trait ApplyAbleMockAdapterTrait
{
    public function approve(IApplyAble $applyAbleObject) : bool
    {
        unset($applyAbleObject);
        return true;
    }

    public function reject(IApplyAble $applyAbleObject) : bool
    {
        unset($applyAbleObject);
        return true;
    }
}
