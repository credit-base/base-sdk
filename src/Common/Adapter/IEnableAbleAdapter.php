<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IEnableAble;

interface IEnableAbleAdapter
{
    public function enable(IEnableAble $enableAbleObject) : bool;

    public function disable(IEnableAble $enableAbleObject) : bool;
}
