<?php
namespace Base\Sdk\Common\Adapter;

interface IResourcesFetchAbleAdapter
{
    public function fetchOne($id, string $resources);

    public function search(
        string $resources,
        array $filter = array(),
        int $number = 0,
        int $size = 20
    ) : array;
}
