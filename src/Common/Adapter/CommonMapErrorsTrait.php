<?php
namespace Base\Sdk\Common\Adapter;

trait CommonMapErrorsTrait
{
    public function commonMapErrors()
    {
        return [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];
    }
}
