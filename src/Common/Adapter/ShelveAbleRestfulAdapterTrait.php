<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IShelveAble;

trait ShelveAbleRestfulAdapterTrait
{
    abstract protected function getResource() : string;

    public function shelve(IshelveAble $shelveAbleObject) : bool
    {
        return $this->shelveAction($shelveAbleObject);
    }

    protected function shelveAction(IshelveAble $shelveAbleObject) : bool
    {
        $this->patch(
            $this->getResource().'/'.$shelveAbleObject->getId().'/shelve'
        );
        if ($this->isSuccess()) {
            $this->translateToObject($shelveAbleObject);
            return true;
        }
        return false;
    }

    public function offShelve(IshelveAble $shelveAbleObject) : bool
    {
        return $this->offShelveAction($shelveAbleObject);
    }

    protected function offShelveAction(IshelveAble $shelveAbleObject) : bool
    {
        $this->patch(
            $this->getResource().'/'.$shelveAbleObject->getId().'/offShelve'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($shelveAbleObject);
            return true;
        }
        return false;
    }
}
