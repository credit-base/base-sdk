<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IShelveAble;

interface IShelveAbleAdapter
{
    public function shelve(IShelveAble $onShelfAbleObject) : bool;

    public function offShelve(IShelveAble $onShelfAbleObject) : bool;
}
