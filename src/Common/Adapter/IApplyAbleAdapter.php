<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IApplyAble;

interface IApplyAbleAdapter
{
    public function approve(IApplyAble $applyAbleObject) : bool;

    public function reject(IApplyAble $applyAbleObject) : bool;
}
