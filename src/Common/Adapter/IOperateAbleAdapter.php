<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IOperateAble;

interface IOperateAbleAdapter
{
    public function add(IOperateAble $operatAbleObject) : bool;

    public function edit(IOperateAble $operatAbleObject) : bool;
}
