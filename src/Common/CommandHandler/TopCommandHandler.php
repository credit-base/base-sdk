<?php
namespace Base\Sdk\Common\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Common\Model\ITopAble;
use Base\Sdk\Common\Command\TopCommand;

abstract class TopCommandHandler implements ICommandHandler
{
    abstract protected function fetchITopObject($id) : ITopAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(TopCommand $command)
    {
        $this->topAble = $this->fetchITopObject($command->id);
        
        return $this->topAble->top();
    }
}
