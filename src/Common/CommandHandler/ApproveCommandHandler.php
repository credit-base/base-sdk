<?php
namespace Base\Sdk\Common\CommandHandler;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Command\ApproveCommand;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

abstract class ApproveCommandHandler implements ICommandHandler
{
    abstract protected function fetchIApplyObject($id) : IApplyAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ApproveCommand $command)
    {
        $this->approveAble = $this->fetchIApplyObject($command->id);

        return $this->approveAble->approve();
    }
}
