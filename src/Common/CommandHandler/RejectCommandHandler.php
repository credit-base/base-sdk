<?php
namespace Base\Sdk\Common\CommandHandler;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Command\RejectCommand;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

abstract class RejectCommandHandler implements ICommandHandler
{
    abstract protected function fetchIApplyObject($id) : IApplyAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(RejectCommand $command)
    {
        $this->rejectAble = $this->fetchIApplyObject($command->id);

        $this->rejectAble->setRejectReason($command->rejectReason);
        
        return $this->rejectAble->reject();
    }
}
