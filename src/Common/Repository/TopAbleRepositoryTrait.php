<?php
namespace Base\Sdk\Common\Repository;

use Base\Sdk\Common\Model\ITopAble;

trait TopAbleRepositoryTrait
{
    public function top(ITopAble $topAbleObject) : bool
    {
        return $this->getAdapter()->top($topAbleObject);
    }

    public function cancelTop(ITopAble $topAbleObject) : bool
    {
        return $this->getAdapter()->cancelTop($topAbleObject);
    }
}
