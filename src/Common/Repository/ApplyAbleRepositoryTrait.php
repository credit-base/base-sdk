<?php
namespace Base\Sdk\Common\Repository;

use Base\Sdk\Common\Model\IApplyAble;

trait ApplyAbleRepositoryTrait
{
    public function approve(IApplyAble $applyAbleObject) : bool
    {
        return $this->getAdapter()->approve($applyAbleObject);
    }

    public function reject(IApplyAble $applyAbleObject) : bool
    {
        return $this->getAdapter()->reject($applyAbleObject);
    }
}
