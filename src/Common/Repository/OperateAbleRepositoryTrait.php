<?php
namespace Base\Sdk\Common\Repository;

use Base\Sdk\Common\Model\IOperateAble;

trait OperateAbleRepositoryTrait
{
    public function add(IOperateAble $operateAbleObject) : bool
    {
        return $this->getAdapter()->add($operateAbleObject);
    }

    public function edit(IOperateAble $operateAbleObject) : bool
    {
        return $this->getAdapter()->edit($operateAbleObject);
    }
}
