<?php
namespace Base\Sdk\Common\Repository;

use Base\Sdk\Common\Model\IResubmitAble;

trait ResubmitAbleRepositoryTrait
{
    public function resubmit(IResubmitAble $resubmitAbleObject) : bool
    {
        return $this->getAdapter()->resubmit($resubmitAbleObject);
    }
}
