<?php
namespace Base\Sdk\Common\Repository;

use Base\Sdk\Common\Model\IShelveAble;

trait ShelveAbleRepositoryTrait
{
    public function shelve(IShelveAble $shelveAbleObject) : bool
    {
        return $this->getAdapter()->shelve($shelveAbleObject);
    }

    public function offShelve(IShelveAble $shelveAbleObject) : bool
    {
        return $this->getAdapter()->offShelve($shelveAbleObject);
    }
}
