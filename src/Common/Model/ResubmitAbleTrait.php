<?php
namespace Base\Sdk\Common\Model;

use Base\Sdk\Common\Adapter\IResubmitAbleAdapter;

trait ResubmitAbleTrait
{
    public function resubmit() : bool
    {
        return $this->resubmitAction();
    }

    protected function resubmitAction(): bool
    {
        $repository = $this->getIResubmitAbleAdapter();

        return $repository->resubmit($this);
    }

    abstract protected function getIResubmitAbleAdapter() : IResubmitAbleAdapter;
}
