<?php
namespace Base\Sdk\Common\Model;

use Marmot\Core;

use Base\Sdk\Common\Adapter\IApplyAbleAdapter;

trait ApplyAbleTrait
{
    protected $applyStatus;

    public function approve() : bool
    {
        return $this->approveAction();
    }

    protected function approveAction(): bool
    {
        $applyAdapter = $this->getIApplyAbleAdapter();

        return $applyAdapter->approve($this);
    }

    public function reject() : bool
    {
        return $this->rejectAction();
    }

    protected function rejectAction(): bool
    {
        $applyAdapter = $this->getIApplyAbleAdapter();
        return $applyAdapter->reject($this);
    }

    abstract protected function getIApplyAbleAdapter() : IApplyAbleAdapter;
}
