<?php
namespace Base\Sdk\Common\Model;

interface IShelveAble
{
    public function shelve() : bool;
    
    public function offShelve() : bool;
}
