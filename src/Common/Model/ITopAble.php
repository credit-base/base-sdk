<?php
namespace Base\Sdk\Common\Model;

interface ITopAble
{
    const STICK = array(
        'DISABLED' => 0 ,
        'ENABLED' => 2
    );

    public function top() : bool;
    
    public function cancelTop() : bool;
}
