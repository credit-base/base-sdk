<?php
namespace Base\Sdk\Common\Model;

use Base\Sdk\Common\Model\IApplyAble;

class IApplyCategory
{
    const OPERATION_TYPE = array(
        'NULL' => 0,
        'ADD' => 1,
        'EDIT' => 2,
        'ENABLED' => 3,
        'DISABLED' => 4,
        'TOP' => 5,
        'CANCEL_TOP' => 6,
        'MOVE' => 7,
    );

    const STICK_CN = array(
        ITopAble::STICK['DISABLED'] => '未置顶',
        ITopAble::STICK['ENABLED'] => '置顶',
    );

    const STATUS_CN = array(
        IEnableAble::STATUS['DISABLED'] => '禁用',
        IEnableAble::STATUS['ENABLED'] => '启用',
    );

    const OPERATION_TYPE_CN = array(
        self::OPERATION_TYPE['NULL'] => '无',
        self::OPERATION_TYPE['ADD'] => '新增',
        self::OPERATION_TYPE['EDIT'] => '编辑',
        self::OPERATION_TYPE['ENABLED'] => '启用',
        self::OPERATION_TYPE['DISABLED'] => '禁用',
        self::OPERATION_TYPE['TOP'] => '置顶',
        self::OPERATION_TYPE['CANCEL_TOP'] => '取消置顶',
        self::OPERATION_TYPE['MOVE'] => '移动',
        
    );

    const APPLY_STATUS_CN = array(
        IApplyAble::APPLY_STATUS['NOT_SUBMITTED'] => '未提交',
        IApplyAble::APPLY_STATUS['PENDING'] => '待审核',
        IApplyAble::APPLY_STATUS['APPROVE'] => '已通过',
        IApplyAble::APPLY_STATUS['REJECT'] => '已驳回',
        IApplyAble::APPLY_STATUS['REVOKED'] => '已撤销',
    );

    const STATUS_TAG_TYPE = array(
        IEnableAble::STATUS['DISABLED'] => 'danger',
        IEnableAble::STATUS['ENABLED'] => 'success',
    );

    const STICK_TAG_TYPE = array(
        ITopAble::STICK['DISABLED'] => 'info',
        ITopAble::STICK['ENABLED'] => 'success',
    );
    
    const APPLY_STATUS_TAG_TYPE = array(
        IApplyAble::APPLY_STATUS['NOT_SUBMITTED'] => 'warning',
        IApplyAble::APPLY_STATUS['PENDING'] => 'warning',
        IApplyAble::APPLY_STATUS['APPROVE'] => 'success',
        IApplyAble::APPLY_STATUS['REJECT'] => 'danger',
        IApplyAble::APPLY_STATUS['REVOKED'] => 'danger',
    );
}
