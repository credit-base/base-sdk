<?php
namespace Base\Sdk\Common\Model;

use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

use Marmot\Core;

use Base\Sdk\Common\Model\IApplyAble;

trait ModifyStatusAbleTrait
{
    /**
     * 撤销
     * @return bool 是否撤销成功
     */
    public function revoke() : bool
    {
        if (!$this->isPending() || !$this->isNormal()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }
        $modifyStatusAdapter = $this->getIModifyStatusAbleAdapter();
        return $modifyStatusAdapter->revoke($this);
    }

    /**
     * 关闭
     * @return bool 是否关闭成功
     */
    public function close() : bool
    {
        if (!$this->isApprove() || !$this->isNormal()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }
        $modifyStatusAdapter = $this->getIModifyStatusAbleAdapter();
        return $modifyStatusAdapter->close($this);
    }

    /**
     * 删除
     * @return bool 是否删除成功
     */
    public function deletes() : bool
    {
        if (!$this->isReject() && !$this->isRevoked() && !$this->isClosed()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }
        $modifyStatusAdapter = $this->getIModifyStatusAbleAdapter();
        return $modifyStatusAdapter->deletes($this);
    }

    abstract protected function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter;

    public function setStatus(int $status)
    {
        $this->status = in_array($status, array_values(self::MODIFY_STATUS)) ? $status : self::MODIFY_STATUS['NORMAL'];
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::MODIFY_STATUS['NORMAL'];
    }

    public function isRevoked() : bool
    {
        return $this->getStatus() == self::MODIFY_STATUS['REVOKED'];
    }

    public function isClosed() : bool
    {
        return $this->getStatus() == self::MODIFY_STATUS['CLOSED'];
    }

    public function isPending() : bool
    {
        return $this->getApplyStatus() == IApplyAble::APPLY_STATUS['PENDING'];
    }

    public function isApprove() : bool
    {
        return $this->getApplyStatus() == IApplyAble::APPLY_STATUS['APPROVE'];
    }

    public function isReject() : bool
    {
        return $this->getApplyStatus() == IApplyAble::APPLY_STATUS['REJECT'];
    }
}
