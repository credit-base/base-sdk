<?php
namespace Base\Sdk\Common\Model;

use Marmot\Core;

use Base\Sdk\Common\Adapter\ITopAbleAdapter;

trait TopAbleTrait
{
    /**
     * 置顶
     * @return bool 是否置顶成功
     */
    public function top() : bool
    {
        if ($this->isStick()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }

        $topAdapter = $this->getITopAbleAdapter();
        return $topAdapter->top($this);
    }

    /**
     * 取消置顶
     * @return bool 是否取消置顶成功
     */
    public function cancelTop() : bool
    {
        if (!$this->isStick()) {
            Core::setLastError(STATUS_CAN_NOT_MODIFY);
            return false;
        }
        $topAdapter = $this->getITopAbleAdapter();
        return $topAdapter->cancelTop($this);
    }

    abstract protected function getITopAbleAdapter() : ITopAbleAdapter;

    public function setStick(int $stick)
    {
        $this->stick = in_array($stick, array_values(self::STICK)) ? $stick : self::STICK['DISABLED'];
    }

    public function isStick() : bool
    {
        return $this->getStick() == self::STICK['ENABLED'];
    }
}
