<?php
namespace Base\Sdk\Common\Model;

interface IOnShelfAble
{
    const STATUS = array(
        'ENABLED' => 0 ,
        'DISABLED' => -2
    );

    public function onShelf() : bool;
    
    public function offStock() : bool;
}
