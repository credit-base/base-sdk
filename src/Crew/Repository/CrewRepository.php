<?php
namespace Base\Sdk\Crew\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\OperateAbleRepositoryTrait;
use Base\Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;

use Base\Sdk\Crew\Adapter\Crew\CrewMockAdapter;
use Base\Sdk\Crew\Adapter\Crew\CrewRestfulAdapter;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Adapter\Crew\ICrewAdapter;

class CrewRepository extends Repository implements ICrewAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'CREW_LIST';
    const FETCH_ONE_MODEL_UN = 'CREW_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CrewRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new CrewMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function signIn(Crew $crew) : bool
    {
        return $this->getAdapter()->signIn($crew);
    }
}
