<?php
namespace Base\Sdk\Crew\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Server;
use Firebase\JWT\JWT;

use Base\Sdk\Common\Model\NullEnableAbleTrait;
use Base\Sdk\Common\Model\NullOperateAbleTrait;

class Guest extends Crew implements INull
{
    use NullEnableAbleTrait, NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    /**
     * [generateIdentify 生成登录标识]
     * @return [string]  [返回类型]
     */
    public function generateIdentify() : string
    {
        $this->identify = md5(serialize(Server::get('marmot')).Core::$container->get('time'));

        return $this->identify;
    }

    /**
     * 登录
     * @return [bool]
     */
    public function signIn() : bool
    {
        if (!$this->getRepository()->signIn($this)) {
            return false;
        }

        return $this->saveIdentify() && $this->saveSession() && $this->registerGlobalCrew();
    }

    protected function registerGlobalCrew() : bool
    {
        Core::$container->set('crew', $this);

        return true;
    }

    protected function saveIdentify()
    {
        $nowTime = Core::$container->get('time');

        $token = [
            'iss' => Core::$container->get('jwt.iss'), //签发者
            'aud' => Core::$container->get('jwt.aud'), //jwt所面向的用户
            'iat' => $nowTime, //签发时间
            'nbf' => $nowTime + Core::$container->get('jwt.nbf'), //在什么时间之后该jwt才可用
            'exp' => $nowTime + Core::$container->get('jwt.exp'), //过期时间-10min
            'data' => [
                'crewId' => $this->getId(),
                'identify' => $this->generateIdentify()
            ]
        ];

        $jwt = JWT::encode($token, Core::$container->get('jwt.key'));
       
        $res['jwt'] = $jwt;

        Core::$container->set('jwt', $res);

        return true;
    }

    /**
     * 储存session
     */
    public function saveSession() : bool
    {
        if ($this->getCrewSessionRepository()->save($this)) {
            Core::$container->set('crewSession', $this->getCrewSessionRepository()->get($this->getId()));

            return true;
        }

        return false;
    }

    /**
     * 验证登录表示
     */
    public function validateIdentify(string $identify) : bool
    {
        if ($this->getIdentify() != $identify) {
            Core::$container->set('jwt', '');
            return false;
        }

        return true;
    }
}
