<?php
namespace Base\Sdk\Crew\CommandHandler\Crew;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\UserGroup\Repository\DepartmentRepository;
use Base\Sdk\Crew\Command\Crew\AddCrewCommand;

class AddCrewCommandHandler implements ICommandHandler
{
    private $crew;

    private $userGroupRepository;

    private $departmentRepository;

    public function __construct()
    {
        $this->crew = new Crew();
        $this->userGroupRepository = new UserGroupRepository();
        $this->departmentRepository = new DepartmentRepository();
    }

    public function __destruct()
    {
        unset($this->crew);
        unset($this->userGroupRepository);
        unset($this->departmentRepository);
    }

    protected function getCrew() : Crew
    {
        return $this->crew;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getDepartmentRepository() : DepartmentRepository
    {
        return $this->departmentRepository;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    public function executeAction($command)
    {
        if (!($command instanceof AddCrewCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->getCrew();

        $crew->setRealName($command->realName);
        $crew->setCellphone($command->cellphone);
        $crew->setPassword($command->password);
        $crew->setCardId($command->cardId);
        $crew->setCategory($command->category);
        $crew->setUserGroup(
            $this->getUserGroupRepository()->fetchOne($command->userGroupId)
        );
        $crew->setDepartment(
            $this->getDepartmentRepository()->fetchOne($command->departmentId)
        );
        $crew->setPurview($command->purview);

        return $crew->add();
    }
}
