<?php
namespace Base\Sdk\Crew\CommandHandler\Crew;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Crew\Model\Guest;
use Base\Sdk\Crew\Command\Crew\SignInCrewCommand;

class SignInCrewCommandHandler implements ICommandHandler
{
    private $crew;

    public function __construct()
    {
        $this->crew = new Guest();
    }

    public function __destruct()
    {
        unset($this->crew);
    }

    protected function getCrew()
    {
        return $this->crew;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof SignInCrewCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->getCrew();

        $crew->setUserName($command->userName);
        $crew->setPassword($command->password);

        return $crew->signIn();
    }
}
