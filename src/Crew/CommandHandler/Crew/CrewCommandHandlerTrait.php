<?php
namespace Base\Sdk\Crew\CommandHandler\Crew;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

trait CrewCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new CrewRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : CrewRepository
    {
        return $this->repository;
    }
    
    protected function fetchCrew(int $id) : Crew
    {
        return $this->getRepository()->fetchOne($id);
    }
}
