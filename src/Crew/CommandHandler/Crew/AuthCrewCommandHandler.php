<?php
namespace Base\Sdk\Crew\CommandHandler\Crew;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Command\Crew\AuthCrewCommand;
use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\Crew\Repository\CrewSessionRepository;

class AuthCrewCommandHandler implements ICommandHandler
{
    private $crewSessionRepository;

    private $crewRepository;

    public function __construct()
    {
        $this->crewSessionRepository = new CrewSessionRepository();
        $this->crewRepository = new CrewRepository();
    }

    public function __destruct()
    {
        unset($this->crewSessionRepository);
        unset($this->crewRepository);
    }

    protected function getCrewSessionRepository() : CrewSessionRepository
    {
        return $this->crewSessionRepository;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AuthCrewCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->getCrewRepository()->scenario(CrewRepository::FETCH_ONE_MODEL_UN)
                     ->fetchOne($command->id);
    
        $crew->setIdentify($command->identify);

        $this->getCrewSessionRepository()->save($crew);

        $crew = $this->getCrewSessionRepository()->get($command->id);
        return $crew->validateIdentify($command->identify) && $this->registerGlobalCrew($crew);
    }
    
    private function registerGlobalCrew(Crew $crew) : bool
    {
        Core::$container->set('crew', $crew);
        return true;
    }
}
