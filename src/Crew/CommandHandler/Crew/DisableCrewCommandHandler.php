<?php
namespace Base\Sdk\Crew\CommandHandler\Crew;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\CommandHandler\DisableCommandHandler;

class DisableCrewCommandHandler extends DisableCommandHandler
{
    use CrewCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchCrew($id);
    }
}
