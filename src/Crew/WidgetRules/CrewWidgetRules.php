<?php
namespace Base\Sdk\Crew\WidgetRules;

use Marmot\Core;

use Respect\Validation\Validator as V;

use Base\Sdk\Crew\Model\Crew;

class CrewWidgetRules
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function category($category) : bool
    {
        if (!in_array($category, Crew::CATEGORY)) {
            Core::setLastError(CREW_CATEGORY_NOT_EXIST);
            return false;
        }

        return true;
    }

    public function purview($purview) : bool
    {
        if (!is_array($purview) || empty($purview)) {
            Core::setLastError(CREW_PURVIEW_FORMAT_ERROR);
            return false;
        }

        foreach ($purview as $each) {
            if (!V::numeric()->positive()->validate($each)) {
                Core::setLastError(CREW_PURVIEW_FORMAT_ERROR);
                return false;
            }
        }

        return true;
    }

    /**
     * 校验当前登陆的员工是否有分配权限
     * 1.获取当前登陆员工类型
     * 2.员工类型的值越小，权限范围越大
     */
    public function purviewHierarchy($category) : bool
    {
        $crewCategory = Core::$container->get('crew')->getCategory();

        if ($category < $crewCategory) {
            Core::setLastError(CREW_PURVIEW_HIERARCHY_FORMAT_ERROR);
            return false;
        }

        return true;
    }
}
