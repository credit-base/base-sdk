<?php
namespace Base\Sdk\Crew\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Crew\Model\Crew;

use Base\Sdk\UserGroup\Translator\UserGroupTranslator;

class CrewSessionTranslator implements ITranslator
{
    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function arrayToObject(array $expression, $crew = null)
    {
        if ($crew == null) {
            $crew = new Crew($expression['id']);
            //$crew = Crew::create($expression['category']);//预留多态，不要删
        }
        if (isset($expression['id'])) {
            $crew->setId($expression['id']);
        }
        if (isset($expression['cellphone'])) {
            $crew->setCellphone($expression['cellphone']);
        }
        if (isset($expression['userName'])) {
            $crew->setUserName($expression['userName']);
        }
        if (isset($expression['realName'])) {
            $crew->setRealName($expression['realName']);
        }
        if (isset($expression['identify'])) {
            $crew->setIdentify($expression['identify']);
        }
        if (isset($expression['category'])) {
            $crew->setCategory($expression['category']);
        }
        if (isset($expression['purview'])) {
            $crew->setPurview($expression['purview']);
        }
        if (isset($expression['userGroup'])) {
            $crew->setUserGroup($this->getUserGroupTranslator()->arrayToObject(
                $expression['userGroup']
            ));
        }

        return $crew;
    }
    
    public function arrayToObjects(array $expression) : array
    {
        return [
            $expression['id'] => $this->arrayToObject($expression)
        ];
    }

    public function objectToArray($crew, array $keys = array())
    {
        unset($keys);
        $expression = array();

        $expression['id'] = $crew->getId();
        $expression['userName'] = $crew->getUserName();
        $expression['cellphone'] = $crew->getCellphone();
        $expression['realName'] = $crew->getRealName();
        $expression['identify'] = $crew->getIdentify();
        $expression['category'] = $crew->getCategory();
        $expression['purview'] = $crew->getPurview();
    
        $expression['userGroup'] = $this->getUserGroupTranslator()->objectToArray(
            $crew->getUserGroup()
        );

        return $expression;
    }
}
