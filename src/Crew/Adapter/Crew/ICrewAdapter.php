<?php
namespace Base\Sdk\Crew\Adapter\Crew;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;
use Base\Sdk\Crew\Model\Crew;

interface ICrewAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperateAbleAdapter, IEnableAbleAdapter
{
    public function signIn(Crew $crew) : bool;
}
