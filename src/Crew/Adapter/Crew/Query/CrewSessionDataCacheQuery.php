<?php
namespace Base\Sdk\Crew\Adapter\Crew\Query;

use Marmot\Framework\Query\DataCacheQuery;

use Base\Sdk\Crew\Adapter\Crew\Query\Persistence\CrewSessionCache;

class CrewSessionDataCacheQuery extends DataCacheQuery
{
    public function __construct()
    {
        parent::__construct(new CrewSessionCache());
    }
}
