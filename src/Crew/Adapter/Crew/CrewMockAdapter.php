<?php
namespace Base\Sdk\Crew\Adapter\Crew;

use Base\Sdk\Common\Adapter\EnableAbleMockAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleMockAdapterTrait;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Utils\MockFactory;

class CrewMockAdapter implements ICrewAdapter
{
    use OperateAbleMockAdapterTrait, EnableAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateCrew($id);
    }

    public function fetchList(array $ids) : array
    {
        $crewList = array();

        foreach ($ids as $id) {
            $crewList[] = MockFactory::generateCrew($id);
        }

        return $crewList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function signIn(Crew $crew) : bool
    {
        unset($crew);
        return true;
    }

    public function updatePassword(Crew $crew) : bool
    {
        unset($crew);
        return true;
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateCrew($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateCrew($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
