<?php
namespace Base\Sdk\Crew\Command\Crew;

use Marmot\Interfaces\ICommand;

class AuthCrewCommand implements ICommand
{
    public $id;

    public $identify;

    public function __construct(
        int $id,
        string $identify
    ) {
        $this->id = $id;
        $this->identify = $identify;
    }
}
