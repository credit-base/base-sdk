<?php
namespace Base\Sdk\Crew\Command\Crew;

use Marmot\Interfaces\ICommand;

class EditCrewCommand implements ICommand
{
    public $realName;

    public $cardId;

    public $userGroupId;

    public $category;

    public $departmentId;
    
    public $purview;

    public $id;

    public function __construct(
        string $realName,
        string $cardId,
        int $id,
        int $userGroupId = 0,
        int $category = 0,
        int $departmentId = 0,
        array $purview = array()
    ) {
        $this->realName = $realName;
        $this->cardId = $cardId;
        $this->userGroupId = $userGroupId;
        $this->category = $category;
        $this->departmentId = $departmentId;
        $this->purview = $purview;
        $this->id = $id;
    }
}
