<?php
namespace Base\Sdk\News\CommandHandler\News;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\CommandHandler\DisableCommandHandler;
use Base\Sdk\Common\Command\DisableCommand;

class DisableNewsCommandHandler extends DisableCommandHandler
{
    use NewsCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchNews($id);
    }
}
