<?php
namespace Base\Sdk\News\CommandHandler\News;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\News\Command\News\EditNewsCommand;

class EditNewsCommandHandler implements ICommandHandler
{
    use NewsCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof EditNewsCommand)) {
            throw new \InvalidArgumentException;
        }

        $news = $this->fetchNews($command->id);
        
        $news->setTitle($command->title);
        $news->setSource($command->source);
        $news->setBannerImage($command->bannerImage);
        $news->setStatus($command->status);
        $news->setCover($command->cover);
        $news->setAttachments($command->attachments);
        $news->setStick($command->stick);
        $news->setContent($command->content);
        $news->setNewsType($command->newsType);
        $news->setDimension($command->dimension);
        $news->setBannerStatus($command->bannerStatus);
        $news->setHomePageShowStatus($command->homePageShowStatus);
       
        return $news->edit();
    }
}
