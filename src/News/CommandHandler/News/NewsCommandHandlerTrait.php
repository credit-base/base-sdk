<?php
namespace Base\Sdk\News\CommandHandler\News;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Repository\NewsRepository;

use Marmot\Interfaces\ICommand;

trait NewsCommandHandlerTrait
{
    private $news;

    private $repository;
    
    public function __construct()
    {
        $this->news = new News();
        $this->repository = new NewsRepository();
    }

    public function __destruct()
    {
        unset($this->news);
        unset($this->repository);
    }

    protected function getNews() : News
    {
        return $this->news;
    }

    protected function getRepository() : NewsRepository
    {
        return $this->repository;
    }
    
    protected function fetchNews(int $id) : News
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }
}
