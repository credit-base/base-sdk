<?php
namespace Base\Sdk\News\CommandHandler\News;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\News\Command\News\MoveNewsCommand;

class MoveNewsCommandHandler implements ICommandHandler
{
    use NewsCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof MoveNewsCommand)) {
            throw new \InvalidArgumentException;
        }
    
        $news = $this->fetchNews($command->id);
        $news->setNewsType($command->newType);
        
        return $news->move();
    }
}
