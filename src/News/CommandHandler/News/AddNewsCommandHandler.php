<?php
namespace Base\Sdk\News\CommandHandler\News;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\News\Command\News\AddNewsCommand;

class AddNewsCommandHandler implements ICommandHandler
{
    use NewsCommandHandlerTrait;

    public function executeAction($command)
    {
        if (!($command instanceof AddNewsCommand)) {
            throw new \InvalidArgumentException;
        }

        $news = $this->getNews();

        $news->setCover($command->cover);
        $news->setStick($command->stick);
        $news->setTitle($command->title);
        $news->setSource($command->source);
        $news->setStatus($command->status);
        $news->setContent($command->content);
        $news->setNewsType($command->newsType);
        $news->setDimension($command->dimension);
        $news->setAttachments($command->attachments);
        $news->setBannerImage($command->bannerImage);
        $news->setBannerStatus($command->bannerStatus);
        $news->setHomePageShowStatus($command->homePageShowStatus);

        return $news->add();
    }
}
