<?php
namespace Base\Sdk\News\CommandHandler\News;

use Base\Sdk\Common\Model\ITopAble;
use Base\Sdk\Common\CommandHandler\CancelTopCommandHandler;
use Base\Sdk\Common\Command\CancelTopCommand;

class CancelTopNewsCommandHandler extends CancelTopCommandHandler
{
    use NewsCommandHandlerTrait;
    
    protected function fetchITopObject($id) : ITopAble
    {
        return $this->fetchNews($id);
    }
}
