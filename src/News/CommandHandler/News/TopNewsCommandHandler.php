<?php
namespace Base\Sdk\News\CommandHandler\News;

use Base\Sdk\Common\Model\ITopAble;
use Base\Sdk\Common\CommandHandler\TopCommandHandler;
use Base\Sdk\Common\Command\TopCommand;

class TopNewsCommandHandler extends TopCommandHandler
{
    use NewsCommandHandlerTrait;
    
    protected function fetchITopObject($id) : ITopAble
    {
        return $this->fetchNews($id);
    }
}
