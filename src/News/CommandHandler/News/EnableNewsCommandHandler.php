<?php
namespace Base\Sdk\News\CommandHandler\News;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\CommandHandler\EnableCommandHandler;
use Base\Sdk\Common\Command\EnableCommand;

class EnableNewsCommandHandler extends EnableCommandHandler
{
    use NewsCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchNews($id);
    }
}
