<?php
namespace Base\Sdk\News\CommandHandler\UnAuditNews;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\RejectCommandHandler;

use Base\Sdk\Common\Command\RejectCommand;

class RejectUnAuditNewsCommandHandler extends RejectCommandHandler
{
    use UnAuditNewsCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditNews($id);
    }
}
