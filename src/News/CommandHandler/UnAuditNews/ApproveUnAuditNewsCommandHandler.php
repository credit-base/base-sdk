<?php
namespace Base\Sdk\News\CommandHandler\UnAuditNews;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\ApproveCommandHandler;
use Base\Sdk\Common\Command\ApproveCommand;

class ApproveUnAuditNewsCommandHandler extends ApproveCommandHandler
{
    use UnAuditNewsCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditNews($id);
    }
}
