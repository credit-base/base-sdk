<?php
namespace Base\Sdk\News\CommandHandler\UnAuditNews;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\News\Command\UnAuditNews\EditUnAuditNewsCommand;

class EditUnAuditNewsCommandHandler implements ICommandHandler
{
    use UnAuditNewsCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof EditUnAuditNewsCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditNews = $this->fetchUnAuditNews($command->id);

        $unAuditNews->setTitle($command->title);
        $unAuditNews->setSource($command->source);
        $unAuditNews->setCover($command->cover);
        $unAuditNews->setAttachments($command->attachments);
        $unAuditNews->setBannerImage($command->bannerImage);
        $unAuditNews->setContent($command->content);
        $unAuditNews->setNewsType($command->newsType);
        $unAuditNews->setDimension($command->dimension);
        $unAuditNews->setStatus($command->status);
        $unAuditNews->setStick($command->stick);
        $unAuditNews->setBannerStatus($command->bannerStatus);
        $unAuditNews->setHomePageShowStatus($command->homePageShowStatus);
       
        return $unAuditNews->edit();
    }
}
