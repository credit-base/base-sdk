<?php
namespace Base\Sdk\News\CommandHandler\UnAuditNews;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Repository\UnAuditNewsRepository;

use Marmot\Interfaces\ICommand;

trait UnAuditNewsCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new UnAuditNewsRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }
    
    protected function getRepository() : UnAuditNewsRepository
    {
        return $this->repository;
    }
    
    protected function fetchUnAuditNews(int $id) : UnAuditNews
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }
}
