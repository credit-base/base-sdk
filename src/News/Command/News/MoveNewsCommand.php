<?php
namespace Base\Sdk\News\Command\News;

use Marmot\Interfaces\ICommand;

class MoveNewsCommand implements ICommand
{
    public $id;
    
    public $newType;

    public function __construct(
        int $newType,
        int $id
    ) {
        $this->id = $id;
        $this->newType = $newType;
    }
}
