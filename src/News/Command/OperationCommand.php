<?php
namespace Base\Sdk\News\Command;

use Marmot\Interfaces\ICommand;

 /**
   * @SuppressWarnings(PHPMD)
   */
class OperationCommand implements ICommand
{
    public $id;
    
    public $title;

    public $source;

    public $cover;

    public $attachments;

    public $bannerImage;

    public $content;

    public $newsType;

    public $dimension;

    public $status;

    public $stick;

    public $bannerStatus;

    public $homePageShowStatus;
    
    public function __construct(
        string $title,
        string $source,
        string $content,
        int $newsType,
        int $dimension,
        int $status,
        int $stick,
        int $bannerStatus,
        int $homePageShowStatus,
        array $cover = array(),
        array $attachments = array(),
        array $bannerImage = array(),
        int $id = 0
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->source = $source;
        $this->cover = $cover;
        $this->attachments = $attachments;
        $this->bannerImage = $bannerImage;
        $this->content = $content;
        $this->newsType = $newsType;
        $this->dimension = $dimension;
        $this->status = $status;
        $this->stick = $stick;
        $this->bannerStatus = $bannerStatus;
        $this->homePageShowStatus = $homePageShowStatus;
    }
}
