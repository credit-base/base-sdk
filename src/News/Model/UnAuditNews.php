<?php
namespace Base\Sdk\News\Model;

use Marmot\Core;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Model\ApplyAbleTrait;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\News\Repository\UnAuditNewsRepository;

class UnAuditNews extends News implements IApplyAble
{
    use ApplyAbleTrait;

    private $rejectReason;

    private $operationType;

    private $applyInfoType;

    private $applyCrew;

    private $relation;

    private $applyUserGroup;

    private $publishUserGroup;
    
    private $unAuditNewsRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->rejectReason = '';
        $this->applyInfoType = 0;
        $this->relation = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->applyCrew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->applyUserGroup = new UserGroup();
        $this->publishUserGroup = new UserGroup();
        $this->operationType = IApplyCategory::OPERATION_TYPE['NULL'];
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->unAuditNewsRepository = new UnAuditNewsRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->rejectReason);
        unset($this->operationType);
        unset($this->applyInfoType);
        unset($this->applyStatus);
        unset($this->relation);
        unset($this->applyCrew);
        unset($this->applyUserGroup);
        unset($this->publishUserGroup);
        unset($this->unAuditNewsRepository);
    }

    public function setRejectReason(string $rejectReason): void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason(): string
    {
        return $this->rejectReason;
    }

    public function setApplyStatus(int $applyStatus): void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus(): int
    {
        return $this->applyStatus;
    }

    public function setOperationType(int $operationType): void
    {
        $this->operationType = $operationType;
    }

    public function getOperationType(): int
    {
        return $this->operationType;
    }

    public function setApplyInfoType(int $applyInfoType): void
    {
        $this->applyInfoType = $applyInfoType;
    }

    public function getApplyInfoType(): int
    {
        return $this->applyInfoType;
    }

    public function setRelation(Crew $relation) : void
    {
        $this->relation = $relation;
    }

    public function getRelation() : Crew
    {
        return $this->relation;
    }

    public function setApplyCrew(Crew $applyCrew) : void
    {
        $this->applyCrew = $applyCrew;
    }

    public function getApplyCrew() : Crew
    {
        return $this->applyCrew;
    }

    public function setPublishUserGroup(UserGroup $publishUserGroup) : void
    {
        $this->publishUserGroup = $publishUserGroup;
    }

    public function getPublishUserGroup() : UserGroup
    {
        return $this->publishUserGroup;
    }

    //@todo 修改待定 applyUserGroup 属于 applyCrew, 这里违反了约束性
    public function setApplyUserGroup(UserGroup $applyUserGroup) : void
    {
        $this->applyUserGroup = $applyUserGroup;
    }

    public function getApplyUserGroup() : UserGroup
    {
        return $this->applyUserGroup;
    }

    protected function getUnAuditNewsRepository()
    {
        return $this->unAuditNewsRepository;
    }

    /**
     * 审核通过/驳回
     * @return [bool]
     */
    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getUnAuditNewsRepository();
    }

    /**
     * 新增编辑
     * @return [bool]
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getUnAuditNewsRepository();
    }
}
