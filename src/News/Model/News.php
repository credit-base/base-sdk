<?php
namespace Base\Sdk\News\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Model\ITopAble;
use Base\Sdk\Common\Model\EnableAbleTrait;
use Base\Sdk\Common\Model\OperateAbleTrait;
use Base\Sdk\Common\Model\TopAbleTrait;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\ITopAbleAdapter;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\News\Repository\NewsRepository;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class News implements IObject, IEnableAble, IOperateAble, ITopAble
{
    use Object, EnableAbleTrait, OperateAbleTrait, TopAbleTrait;

    const DIMENSION = array(
        'NULL' => 0,
        'SOCIOLOGY' => 1,
        'GOVERNMENT_AFFAIRS' => 2,
    );

    const BANNER_STATUS = array(
        'DISABLED' => 0,
        'ENABLED' => 2,
    );
    
    const HOME_PAGE_SHOW_STATUS = array(
        'DISABLED' => 0,
        'ENABLED' => 2,
    );

    private $id;

    private $title;

    private $source;

    private $cover;
    
    private $description;

    private $attachments;

    private $bannerImage;

    private $content;

    private $newsType;

    private $dimension;

    private $stick;

    private $bannerStatus;

    private $homePageShowStatus;

    private $crew;

    private $userGroup;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->source = '';
        $this->description = '';
        $this->content = '';
        $this->attachments = array();
        $this->cover = array();
        $this->newsType = 0;
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->userGroup = new UserGroup();
        $this->dimension = self::DIMENSION['NULL'];
        $this->status = self::STATUS['ENABLED'];
        $this->stick = self::STICK['DISABLED'];
        $this->bannerStatus = self::BANNER_STATUS['DISABLED'];
        $this->homePageShowStatus = self::HOME_PAGE_SHOW_STATUS['DISABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new NewsRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->source);
        unset($this->description);
        unset($this->attachments);
        unset($this->cover);
        unset($this->bannerImage);
        unset($this->content);
        unset($this->newsType);
        unset($this->dimension);
        unset($this->stick);
        unset($this->bannerStatus);
        unset($this->homePageShowStatus);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status =
            in_array(
                $status,
                array_values(self::STATUS)
            ) ? $status : self::STATUS['ENABLED'];
    }

    public function setDimension(int $dimension): void
    {
        $this->dimension =
            in_array(
                $dimension,
                array_values(self::DIMENSION)
            ) ? $dimension : self::DIMENSION['NULL'];
    }

    public function getDimension(): int
    {
        return $this->dimension;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setNewsType(int $newsType): void
    {
        $this->newsType = $newsType;
    }

    public function getNewsType(): int
    {
        return $this->newsType;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setBannerImage(array $bannerImage) : void
    {
        $this->bannerImage = $bannerImage;
    }

    public function getBannerImage() : array
    {
        return $this->bannerImage;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setStick(int $stick) : void
    {
        $this->stick =
            in_array(
                $stick,
                array_values(self::STICK)
            ) ? $stick : self::STICK['DISABLED'];
    }

    public function getStick() : int
    {
        return $this->stick;
    }

    public function setAttachments(array $attachments): void
    {
        $this->attachments = $attachments;
    }

    public function getAttachments(): array
    {
        return $this->attachments;
    }

    public function setCover(array $cover): void
    {
        $this->cover = $cover;
    }

    public function getCover(): array
    {
        return $this->cover;
    }

    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function setBannerStatus(int $bannerStatus) : void
    {
        $this->bannerStatus =
            in_array(
                $bannerStatus,
                array_values(self::BANNER_STATUS)
            ) ? $bannerStatus : self::BANNER_STATUS['DISABLED'];
    }

    public function getBannerStatus() : int
    {
        return $this->bannerStatus;
    }

    public function setHomePageShowStatus(int $homePageShowStatus) : void
    {
        $this->homePageShowStatus =
            in_array(
                $homePageShowStatus,
                array_values(self::HOME_PAGE_SHOW_STATUS)
            ) ? $homePageShowStatus : self::HOME_PAGE_SHOW_STATUS['DISABLED'];
    }

    public function getHomePageShowStatus() : int
    {
        return $this->homePageShowStatus;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * 启用禁用
     */
    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 新增编辑
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 置顶/取消置顶
     */
    protected function getITopAbleAdapter() : ITopAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 移动
     * @return [bool]
     */
    public function move() : bool
    {
        return $this->getRepository()->move($this);
    }
}
