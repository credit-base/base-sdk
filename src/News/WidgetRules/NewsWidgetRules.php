<?php
namespace Base\Sdk\News\WidgetRules;

use Base\Sdk\Common\WidgetRules\WidgetRules;

use Marmot\Core;

use Base\Sdk\News\Model\News;

class NewsWidgetRules
{
    private static $instance;

    protected function getCommonWidgetRules() : WidgetRules
    {
        return new WidgetRules();
    }

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function newsType($newsType, $pointer = 'newsType') : bool
    {
        if (!in_array($newsType, NEWS_TYPE) || empty($newsType)) {
            Core::setLastError(NEWS_TYPE_NOT_EXIST, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function dimension($dimension, $pointer = 'dimension') : bool
    {
        if (!in_array($dimension, News::DIMENSION) || empty($dimension)) {
            Core::setLastError(DIMENSION_NOT_EXIST, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function homePageShowStatus($homePageShowStatus, $pointer = 'homePageShowStatus') : bool
    {
        if (!in_array($homePageShowStatus, News::HOME_PAGE_SHOW_STATUS)) {
            Core::setLastError(HOME_PAGE_SHOW_STATUS_NOT_EXIST, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function bannerImage($bannerStatus, $bannerImage) : bool
    {
        if (!in_array($bannerStatus, News::BANNER_STATUS)) {
            Core::setLastError(BANNER_STATUS_NOT_EXIST, array('pointer'=>'bannerStatus'));
            return false;
        }

        if ($bannerStatus == News::BANNER_STATUS['ENABLED']) {
            $this->getCommonWidgetRules()->image($bannerImage, 'bannerImage');
        }

        return true;
    }
}
