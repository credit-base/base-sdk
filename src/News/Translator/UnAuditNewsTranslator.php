<?php
namespace Base\Sdk\News\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Model\NullUnAuditNews;

use Base\Sdk\Crew\Translator\CrewTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;

class UnAuditNewsTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $unAuditNews = null)
    {
        unset($unAuditNews);
        unset($expression);
        return new NullUnAuditNews();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($unAuditNews, array $keys = array())
    {
        if (!$unAuditNews instanceof UnAuditNews) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'rejectReason',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'applyStatus',
                'applyInfoType',
                'operationType',
                'relation' =>[],
                'crew' => [],
                'applyCrew' => [],
                'applyUserGroup' => [],
                'publishUserGroup' => [],
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($unAuditNews->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = Filter::dhtmlspecialchars($unAuditNews->getTitle());
        }
        if (in_array('source', $keys)) {
            $expression['source'] = Filter::dhtmlspecialchars($unAuditNews->getSource());
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = Filter::dhtmlspecialchars($unAuditNews->getRejectReason());
        }
        if (in_array('attachments', $keys)) {
            $expression['attachments'] = $unAuditNews->getAttachments();
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $unAuditNews->getCover();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($unAuditNews->getStatus()),
                'name' => IApplyCategory::STATUS_CN[$unAuditNews->getStatus()],
                'type' => IApplyCategory::STATUS_TAG_TYPE[$unAuditNews->getStatus()]
            ];
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = [
                'id' => marmot_encode($unAuditNews->getApplyStatus()),
                'name' => IApplyCategory::APPLY_STATUS_CN[$unAuditNews->getApplyStatus()],
                'type' => IApplyCategory::APPLY_STATUS_TAG_TYPE[$unAuditNews->getApplyStatus()]
            ];
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = [
                'id' => marmot_encode($unAuditNews->getDimension()),
                'name' => INews::DIMENSION_CN[$unAuditNews->getDimension()]
            ];
        }
        if (in_array('newsType', $keys)) {
            $expression['newsType'] = [
                'id' => marmot_encode($unAuditNews->getNewsType()),
                'name' => array_key_exists($unAuditNews->getNewsType(), NEWS_TYPE_CN) ?
                        NEWS_TYPE_CN[$unAuditNews->getNewsType()] :
                        ''
            ];
        }
        if (in_array('applyInfoType', $keys)) {
            $expression['applyInfoType'] = [
                'id' => marmot_encode($unAuditNews->getApplyInfoType()),
                'name' => NEWS_TYPE_CN[$unAuditNews->getApplyInfoType()]
            ];
        }
        if (in_array('operationType', $keys)) {
            $expression['operationType']= [
                'id' => marmot_encode($unAuditNews->getOperationType()),
                'name' => IApplyCategory::OPERATION_TYPE_CN[$unAuditNews->getOperationType()]
            ];
        }
        if (in_array('bannerImage', $keys)) {
            $expression['bannerImage'] = $unAuditNews->getBannerImage();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = Filter::dhtmlspecialchars(str_replace("ℑ", "", $unAuditNews->getContent()));//phpcs:ignore
        }
        if (in_array('stick', $keys)) {
            $expression['stick'] = [
                'id' => marmot_encode($unAuditNews->getStick()),
                'name' => IApplyCategory::STICK_CN[$unAuditNews->getStick()],
                'type' => IApplyCategory::STICK_TAG_TYPE[$unAuditNews->getStick()]
            ];
        }
        if (in_array('bannerStatus', $keys)) {
            $expression['bannerStatus'] = [
                'id' => marmot_encode($unAuditNews->getBannerStatus()),
                'name' => INews::BANNER_STATUS_CN[$unAuditNews->getBannerStatus()],
                'type' => INews::BANNER_STATUS_TAG_TYPE[$unAuditNews->getBannerStatus()]
            ];
        }
        if (in_array('homePageShowStatus', $keys)) {
            $expression['homePageShowStatus'] =[
                'id' => marmot_encode($unAuditNews->getHomePageShowStatus()),
                'name' => INews::HOME_PAGE_SHOW_STATUS_CN[$unAuditNews->getHomePageShowStatus()],
                'type' => INews::HOME_PAGE_SHOW_STATUS_TAG_TYPE[$unAuditNews->getHomePageShowStatus()]
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $unAuditNews->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $unAuditNews->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $unAuditNews->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $unAuditNews->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $unAuditNews->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $unAuditNews->getCreateTime());
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $unAuditNews->getCrew(),
                $keys['crew']
            );
        }
        if (isset($keys['relation'])) {
            $expression['relation'] = $this->getCrewTranslator()->objectToArray(
                $unAuditNews->getRelation(),
                $keys['relation']
            );
        }
        if (isset($keys['applyCrew'])) {
            $expression['applyCrew'] = $this->getCrewTranslator()->objectToArray(
                $unAuditNews->getApplyCrew(),
                $keys['applyCrew']
            );
        }
        if (isset($keys['publishUserGroup'])) {
            $expression['publishUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $unAuditNews->getPublishUserGroup(),
                $keys['publishUserGroup']
            );
        }
        if (isset($keys['applyUserGroup'])) {
            $expression['applyUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $unAuditNews->getApplyUserGroup(),
                $keys['applyUserGroup']
            );
        }

        return $expression;
    }
}
