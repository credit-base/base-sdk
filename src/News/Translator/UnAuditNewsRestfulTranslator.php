<?php
namespace Base\Sdk\News\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Model\NullUnAuditNews;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD)
 */
class UnAuditNewsRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $unAuditNews = null)
    {
        return $this->translateToObject($expression, $unAuditNews);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $unAuditNews = null)
    {
        if (empty($expression)) {
            return new NullUnAuditNews();
        }

        if ($unAuditNews == null) {
            $unAuditNews = new UnAuditNews();
        }
        
        $data = $expression['data'];

        $id = $data['id'];
        $unAuditNews->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $unAuditNews->setTitle($attributes['title']);
        }

        if (isset($attributes['source'])) {
            $unAuditNews->setSource($attributes['source']);
        }

        if (isset($attributes['description'])) {
            $unAuditNews->setDescription($attributes['description']);
        }

        if (isset($attributes['attachments'])) {
            $unAuditNews->setAttachments($attributes['attachments']);
        }

        if (isset($attributes['applyStatus'])) {
            $unAuditNews->setApplyStatus($attributes['applyStatus']);
        }

        if (isset($attributes['rejectReason'])) {
            $unAuditNews->setRejectReason($attributes['rejectReason']);
        }

        if (isset($attributes['operationType'])) {
            $unAuditNews->setOperationType($attributes['operationType']);
        }

        if (isset($attributes['cover'])) {
            $unAuditNews->setCover($attributes['cover']);
        }

        if (isset($attributes['content'])) {
            $unAuditNews->setContent($attributes['content']);
        }

        if (isset($attributes['newsType'])) {
            $unAuditNews->setNewsType($attributes['newsType']);
        }

        if (isset($attributes['dimension'])) {
            $unAuditNews->setDimension($attributes['dimension']);
        }

        if (isset($attributes['bannerImage'])) {
            $unAuditNews->setBannerImage($attributes['bannerImage']);
        }

        if (isset($attributes['bannerStatus'])) {
            $unAuditNews->setBannerStatus($attributes['bannerStatus']);
        }

        if (isset($attributes['homePageShowStatus'])) {
            $unAuditNews->setHomePageShowStatus($attributes['homePageShowStatus']);
        }

        if (isset($attributes['stick'])) {
            $unAuditNews->setStick($attributes['stick']);
        }

        if (isset($attributes['applyInfoType'])) {
            $unAuditNews->setApplyInfoType($attributes['applyInfoType']);
        }

        if (isset($attributes['createTime'])) {
            $unAuditNews->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['updateTime'])) {
            $unAuditNews->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['statusTime'])) {
            $unAuditNews->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['status'])) {
            $unAuditNews->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['relation']['data'])) {
            $relationData = $this->changeArrayFormat($relationships['relation']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($relationData);
           
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;
            $unAuditNews->setRelation($crew);
        }

        if (isset($relationships['applyCrew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['applyCrew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
           
            $applyCrew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;
            $unAuditNews->setApplyCrew($applyCrew);
        }

        if (isset($relationships['applyUserGroup']['data'])) {
            $applyUserGroup = $this->changeArrayFormat($relationships['applyUserGroup']['data']);
            $unAuditNews->setApplyUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($applyUserGroup));
        }

        if (isset($relationships['publishUserGroup']['data'])) {
            $publishUserGroup = $this->changeArrayFormat($relationships['publishUserGroup']['data']);
            $unAuditNews->setPublishUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($publishUserGroup));
        }

        if (isset($relationships['crew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['crew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
           
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;
            $unAuditNews->setCrew($crew);
        }

        return $unAuditNews;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($unAuditNews, array $keys = array())
    {
        if (!$unAuditNews instanceof UnAuditNews) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'rejectReason',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew',
                'applyCrew',
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'unAuditedNews'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $unAuditNews->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $unAuditNews->getTitle();
        }
        if (in_array('source', $keys)) {
            $attributes['source'] = $unAuditNews->getSource();
        }
        if (in_array('attachments', $keys)) {
            $attributes['attachments'] = $unAuditNews->getAttachments();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $unAuditNews->getRejectReason();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $unAuditNews->getStatus();
        }
        if (in_array('dimension', $keys)) {
            $attributes['dimension'] = $unAuditNews->getDimension();
        }
        if (in_array('newsType', $keys)) {
            $attributes['newsType'] = $unAuditNews->getNewsType();
        }
        if (in_array('bannerImage', $keys)) {
            $attributes['bannerImage'] = $unAuditNews->getBannerImage();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $unAuditNews->getContent();
        }
        if (in_array('stick', $keys)) {
            $attributes['stick'] = $unAuditNews->getStick();
        }
        if (in_array('cover', $keys)) {
            $attributes['cover'] = $unAuditNews->getCover();
        }
        if (in_array('bannerStatus', $keys)) {
            $attributes['bannerStatus'] = $unAuditNews->getBannerStatus();
        }
        if (in_array('homePageShowStatus', $keys)) {
            $attributes['homePageShowStatus'] = $unAuditNews->getHomePageShowStatus();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $unAuditNews->getCrew()->getId()
                )
            );
        }

        if (in_array('applyCrew', $keys)) {
            $expression['data']['relationships']['applyCrew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $unAuditNews->getApplyCrew()->getId()
                )
            );
        }
        
        return $expression;
    }
}
