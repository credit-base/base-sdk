<?php
namespace Base\Sdk\News\Translator;

use Base\Sdk\News\Model\News;

class INews
{
    const DIMENSION_CN = array(
        News::DIMENSION['NULL'] => '无',
        News::DIMENSION['SOCIOLOGY'] => '社会公开',
        News::DIMENSION['GOVERNMENT_AFFAIRS'] => '政务共享',
    );

    const BANNER_STATUS_CN = array(
        News::BANNER_STATUS['DISABLED'] => ' 未轮播',
        News::BANNER_STATUS['ENABLED'] => '轮播 ',
    );

    const HOME_PAGE_SHOW_STATUS_CN = array(
        News::HOME_PAGE_SHOW_STATUS['DISABLED'] => '未展示',
        News::HOME_PAGE_SHOW_STATUS['ENABLED'] => '展示',
    );

    const HOME_PAGE_SHOW_STATUS_TAG_TYPE = array(
        News::HOME_PAGE_SHOW_STATUS['DISABLED'] => 'info',
        News::HOME_PAGE_SHOW_STATUS['ENABLED'] => 'success',
    );

    const BANNER_STATUS_TAG_TYPE = array(
        News::BANNER_STATUS['DISABLED'] => 'info',
        News::BANNER_STATUS['ENABLED'] => 'success',
    );
}
