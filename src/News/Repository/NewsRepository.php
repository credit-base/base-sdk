<?php
namespace Base\Sdk\News\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\OperateAbleRepositoryTrait;
use Base\Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\TopAbleRepositoryTrait;

use Base\Sdk\News\Adapter\News\NewsRestfulAdapter;
use Base\Sdk\News\Adapter\News\INewsAdapter;
use Base\Sdk\News\Model\News;

class NewsRepository extends Repository implements INewsAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        TopAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'NEWS_LIST';
    const FETCH_ONE_MODEL_UN = 'NEWS_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new NewsRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function move(News $news) : bool
    {
        return $this->getAdapter()->move($news);
    }
}
