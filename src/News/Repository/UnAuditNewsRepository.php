<?php
namespace Base\Sdk\News\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\OperateAbleRepositoryTrait;
use Base\Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\ApplyAbleRepositoryTrait;

use Base\Sdk\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;
use Base\Sdk\News\Adapter\UnAuditNews\IUnAuditNewsAdapter;

class UnAuditNewsRepository extends Repository implements IUnAuditNewsAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperateAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        ApplyAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'UN_AUDIT_NEWS_LIST';
    const FETCH_ONE_MODEL_UN = 'UN_AUDIT_NEWS_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new UnAuditNewsRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
