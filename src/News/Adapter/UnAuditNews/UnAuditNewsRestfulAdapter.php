<?php
namespace Base\Sdk\News\Adapter\UnAuditNews;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Model\NullUnAuditNews;

use Base\Sdk\News\Translator\UnAuditNewsRestfulTranslator;

class UnAuditNewsRestfulAdapter extends GuzzleAdapter implements IUnAuditNewsAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDIT_NEWS_LIST'=>[
            'fields' => [],
            'include' => 'crew,publishUserGroup,applyUserGroup,applyCrew,relation'
        ],
        'UN_AUDIT_NEWS_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,publishUserGroup,applyUserGroup,applyCrew,relation'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditNewsRestfulTranslator();
        $this->resource = 'unAuditedNews';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            101 => [
                'crewId' => PARAMETER_FORMAT_ERROR,
                'applyCrewId' => PARAMETER_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
                'title' => TITLE_FORMAT_ERROR,
                'source' => SOURCE_FORMAT_ERROR,
                'cover' => IMAGE_FORMAT_ERROR,
                'attachments' => ATTACHMENT_FORMAT_ERROR,
                'content' => PARAMETER_FORMAT_ERROR,
                'dimension' => DIMENSION_NOT_EXIST,
                'homePageShowStatus' => HOME_PAGE_SHOW_STATUS_NOT_EXIST,
                'bannerStatus' => BANNER_STATUS_NOT_EXIST,
                'bannerImage' => IMAGE_FORMAT_ERROR,
                'status' => STATUS_FORMAT_ERROR,
                'stick' => STATUS_FORMAT_ERROR,
                'newsType' => NEWS_TYPE_NOT_EXIST,
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullUnAuditNews());
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function addAction(UnAuditNews $unAuditNews) : bool
    {
        unset($unAuditNews);
        return false;
    }

    protected function editAction(UnAuditNews $unAuditNews) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditNews,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$unAuditNews->getId().'/'.'resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditNews);
            return true;
        }

        return false;
    }

    protected function approveAction(IApplyAble $applyAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($applyAbleObject, array('applyCrew'));
       
        $this->patch(
            $this->getResource().'/'.$applyAbleObject->getId().'/approve',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($applyAbleObject);
            return true;
        }
        return false;
    }

    protected function rejectAction(IApplyAble $applyAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $applyAbleObject,
            array(
                'rejectReason',
                'applyCrew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$applyAbleObject->getId().'/reject',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($applyAbleObject);
            return true;
        }
        return false;
    }
}
