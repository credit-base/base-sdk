<?php
namespace Base\Sdk\News\Adapter\UnAuditNews;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IUnAuditNewsFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
