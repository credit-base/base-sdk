<?php
namespace Base\Sdk\News\Adapter\UnAuditNews;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;

interface IUnAuditNewsOperateAbleAdapter extends IOperateAbleAdapter, IApplyAbleAdapter
{
    
}
