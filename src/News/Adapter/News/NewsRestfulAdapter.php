<?php
namespace Base\Sdk\News\Adapter\News;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\TopAbleRestfulAdapterTrait;
use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\Model\ITopAble;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Model\NullNews;

use Base\Sdk\News\Translator\NewsRestfulTranslator;

class NewsRestfulAdapter extends GuzzleAdapter implements INewsAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        TopAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    
    const SCENARIOS = [
        'NEWS_LIST'=>[
            'fields' => [],
            'include' => 'crews,publishUserGroup'
        ],
        'NEWS_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crews,publishUserGroup'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new NewsRestfulTranslator();
        $this->resource = 'news';
        $this->scenario = array();
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            101 => [
                'title' => TITLE_FORMAT_ERROR,
                'source' => SOURCE_FORMAT_ERROR,
                'cover' => IMAGE_FORMAT_ERROR,
                'attachments' => ATTACHMENT_FORMAT_ERROR,
                'content' => PARAMETER_FORMAT_ERROR,
                'status' => STATUS_FORMAT_ERROR,
                'stick' => STATUS_FORMAT_ERROR,
                'newsType' => NEWS_TYPE_NOT_EXIST,
                'dimension' => DIMENSION_NOT_EXIST,
                'homePageShowStatus' => HOME_PAGE_SHOW_STATUS_NOT_EXIST,
                'bannerStatus' => BANNER_STATUS_NOT_EXIST,
                'bannerImage' => IMAGE_FORMAT_ERROR,
                'crewId' => PARAMETER_FORMAT_ERROR,
                'applyCrewId' => PARAMETER_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullNews());
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function addAction($news) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $news,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($news);
            return true;
        }

        return false;
    }

    protected function editAction(News $news) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $news,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$news->getId(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($news);
            return true;
        }

        return false;
    }

    protected function enableAction(IEnableAble $enableAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($enableAbleObject, array('crew'));

        $this->patch(
            $this->getResource().'/'.$enableAbleObject->getId().'/enable',
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($enableAbleObject);
            return true;
        }
        return false;
    }

    protected function disableAction(IEnableAble $enableAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($enableAbleObject, array('crew'));

        $this->patch(
            $this->getResource().'/'.$enableAbleObject->getId().'/disable',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($enableAbleObject);
            return true;
        }
        return false;
    }

    protected function topAction(ITopAble $topAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($topAbleObject, array('crew'));
        
        $this->patch(
            $this->getResource().'/'.$topAbleObject->getId().'/top',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($topAbleObject);
            return true;
        }
        return false;
    }

    protected function cancelTopAction(ITopAble $topAbleObject) : bool
    {
        $data = $this->getTranslator()->objectToArray($topAbleObject, array('crew'));

        $this->patch(
            $this->getResource().'/'.$topAbleObject->getId().'/cancelTop',
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($topAbleObject);
            return true;
        }
        return false;
    }

    public function move(News $news) : bool
    {
        $data = $this->getTranslator()->objectToArray($news, array('crew'));
    
        $this->patch(
            $this->getResource().'/'.$news->getId().'/'.'move'.'/'.$news->getNewsType(),
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($news);
            return true;
        }

        return false;
    }
}
