<?php
namespace Base\Sdk\News\Adapter\News;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface INewsFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
