<?php
namespace Base\Sdk\News\Adapter\News;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;
use Base\Sdk\Common\Adapter\ITopAbleAdapter;

interface INewsOperateAbleAdapter extends IOperateAbleAdapter, IEnableAbleAdapter, ITopAbleAdapter
{
    
}
