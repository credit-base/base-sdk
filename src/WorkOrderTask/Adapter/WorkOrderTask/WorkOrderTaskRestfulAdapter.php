<?php
namespace Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;
use Base\Sdk\WorkOrderTask\Model\NullWorkOrderTask;
use Base\Sdk\WorkOrderTask\Translator\WorkOrderTaskRestfulTranslator;

class WorkOrderTaskRestfulAdapter extends GuzzleAdapter implements IWorkOrderTaskAdapter
{
    use CommonMapErrorsTrait, FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'WORK_ORDER_TASK_LIST'=>[
            'fields' => [],
            'include' => 'parentTask,parentTask.assignObjects,assignObject,template'
        ],
        'WORK_ORDER_TASK_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'parentTask,parentTask.assignObjects,assignObject,template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new WorkOrderTaskRestfulTranslator();
        $this->resource = 'workOrderTasks';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            101 => [
                'feedbackRecords' => FEEDBACK_RECORDS_FORMAT_ERROR,
                'crew' => CREW_FORMAT_ERROR,
                'userGroup' => USER_GROUP_FORMAT_ERROR,
                'reason' => REASON_FORMAT_ERROR,
                'templateId' => TEMPLATE_ID_FORMAT_ERROR,
                'items' => ITEMS_FORMAT_ERROR,
                'isExistedTemplate' => IS_EXISTED_TEMPLATE_PARAMETER_FORMAT_ERROR,
                'identify' => IDENTIFY_FORMAT_ERROR,
                'ZTMC' => ZTMC_FORMAT_ERROR,
                'TYSHXYDM'=>TYSHXYDM_FORMAT_ERROR,
                'type' => ITEM_TYPE_FORMAT_ERROR,
                'length' => LENGTH_FORMAT_ERROR,
                'isNecessary' => IS_NECESSARY_FORMAT_ERROR,
                'isMasked' => IS_MASKED_FORMAT_ERROR,
                'options' => OPTIONS_FORMAT_ERROR
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullWorkOrderTask());
    }
    
    public function end(WorkOrderTask $workOrderTask) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $workOrderTask,
            array('reason')
        );

        $this->patch(
            $this->getResource().'/'.$workOrderTask->getId().'/end',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($workOrderTask);
            return true;
        }

        return false;
    }

    public function confirm(WorkOrderTask $workOrderTask) : bool
    {
        $this->patch(
            $this->getResource().'/'.$workOrderTask->getId().'/confirm'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($workOrderTask);
            return true;
        }

        return false;
    }

    public function revoke(WorkOrderTask $workOrderTask) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $workOrderTask,
            array('reason')
        );

        $this->patch(
            $this->getResource().'/'.$workOrderTask->getId().'/revoke',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($workOrderTask);
            return true;
        }

        return false;
    }

    public function feedback(WorkOrderTask $workOrderTask) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $workOrderTask,
            array('feedbackRecords')
        );

        $this->patch(
            $this->getResource().'/'.$workOrderTask->getId().'/feedback',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($workOrderTask);
            return true;
        }

        return false;
    }
}
