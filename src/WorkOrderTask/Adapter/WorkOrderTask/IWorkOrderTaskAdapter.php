<?php
namespace Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;

interface IWorkOrderTaskAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
    public function end(WorkOrderTask $workOrderTask) : bool;

    public function confirm(WorkOrderTask $workOrderTask) : bool;
    
    public function revoke(WorkOrderTask $workOrderTask) : bool;

    public function feedback(WorkOrderTask $workOrderTask) : bool;
}
