<?php
namespace Base\Sdk\WorkOrderTask\Adapter\ParentTask;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;
use Base\Sdk\WorkOrderTask\Model\ParentTask;

interface IParentTaskAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
    public function assign(ParentTask $parentTask) : bool;
    
    public function revoke(ParentTask $parentTask) : bool;
}
