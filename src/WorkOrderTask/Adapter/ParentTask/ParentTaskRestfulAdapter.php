<?php
namespace Base\Sdk\WorkOrderTask\Adapter\ParentTask;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\WorkOrderTask\Model\NullParentTask;

use Base\Sdk\WorkOrderTask\Translator\ParentTaskRestfulTranslator;

class ParentTaskRestfulAdapter extends GuzzleAdapter implements IParentTaskAdapter
{
    use CommonMapErrorsTrait, FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'PARENT_TASK_LIST'=>[
            'fields' => [],
            'include' => 'template'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new ParentTaskRestfulTranslator();
        $this->resource = 'parentTasks';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            101 => [
                'title' => TITLE_FORMAT_ERROR,
                'templateType' => TEMPLATE_TYPE_FORMAT_ERROR,
                'description' => DESCRIPTION_FORMAT_ERROR,
                'endTime' => END_TIME_FORMAT_ERROR,
                'reason' => REASON_FORMAT_ERROR,
                'attachment' => ATTACHMENT_FORMAT_ERROR
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullParentTask());
    }

    public function assign(ParentTask $parentTask) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $parentTask,
            array(
                'title',
                'description',
                'attachment',
                'assignObjects',
                'templateType',
                'template',
                'endTime'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($parentTask);
            return true;
        }

        return false;
    }

    public function revoke(ParentTask $parentTask) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $parentTask,
            array('reason')
        );

        $this->patch(
            $this->getResource().'/'.$parentTask->getId().'/revoke',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($parentTask);
            return true;
        }

        return false;
    }
}
