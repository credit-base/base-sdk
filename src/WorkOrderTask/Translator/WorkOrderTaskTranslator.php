<?php
namespace Base\Sdk\WorkOrderTask\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;
use Base\Sdk\WorkOrderTask\Model\NullWorkOrderTask;
use Base\Sdk\WorkOrderTask\Translator\ParentTaskTranslator;

class WorkOrderTaskTranslator implements ITranslator
{
    const TYPE = array(
        WorkOrderTask::STATUS['DQR'] => 'info',
        WorkOrderTask::STATUS['YCX'] => 'warning',
        WorkOrderTask::STATUS['YQR'] => 'success',
        WorkOrderTask::STATUS['GJZ'] => 'warning',
        WorkOrderTask::STATUS['YZJ'] => 'danger'
    );

    public function arrayToObject(array $expression, $workOrderTask = null)
    {
        unset($workOrderTask);
        unset($expression);
        return new NullWorkOrderTask();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    protected function getParentTaskTranslator() : ParentTaskTranslator
    {
        return new ParentTaskTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($workOrderTask, array $keys = array())
    {
        if (!$workOrderTask instanceof WorkOrderTask) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'feedbackRecords',
                'status',
                'parentTask',
                'assignObject',
                'template',
                'reason',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($workOrderTask->getId());
        }
        if (in_array('feedbackRecords', $keys)) {
            $feedbackRecords = $workOrderTask->getFeedbackRecords();
            if (!empty($feedbackRecords)) {
                $feedbackRecords = $this->feedbackRecords($workOrderTask->getFeedbackRecords());
            }
            
            $expression['feedbackRecords'] = $feedbackRecords;
        }
        if (in_array('reason', $keys)) {
            $expression['reason'] = $workOrderTask->getReason();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = array(
                'id' => $workOrderTask->getStatus(),
                'name' => WorkOrderTask::STATUS_CN[$workOrderTask->getStatus()],
                'type' => self::TYPE[$workOrderTask->getStatus()]
            );
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $workOrderTask->getUpdateTime();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $workOrderTask->getCreateTime();
        }
        
        if (in_array('parentTask', $keys)) {
            $expression['parentTask'] = $this->getParentTaskTranslator()->objectToArray(
                $workOrderTask->getParentTask()
            );
        }
        if (in_array('template', $keys)) {
            $templateType = $workOrderTask->getParentTask()->getTemplateType();
            $translator = $this->getTranslatorFactory()->getTranslator($templateType);

            $expression['template'] = $translator->objectToArray(
                $workOrderTask->getTemplate()
            );
        }
        if (in_array('assignObject', $keys)) {
            $expression['assignObject'] = [
                'id' => marmot_encode($workOrderTask->getAssignObject()->getId()),
                'name' => $workOrderTask->getAssignObject()->getName(),
            ];
        }

        return $expression;
    }

    protected function feedbackRecords($feedbackRecords)
    {
        foreach ($feedbackRecords as $key => $item) {
            $feedbackRecords[$key]['items'] = $this->getItemCn($item['items']);
        }

        return $feedbackRecords;
    }

    protected function getItemCn($items)
    {
        foreach ($items as $key => $item) {
            $items[$key]['isNecessary']= $this->getIsNecessaryCn($item['isNecessary']);
            $items[$key]['isMasked'] = $this->getIsMaskedCn($item['isMasked']);
            $items[$key]['dimension'] = $this->getDimensionCn($item['dimension']);
            $items[$key]['type'] = $this->getTypeCn($item['type']);
        }

        return $items;
    }

    protected function getIsNecessaryCn($isNecessary)
    {
        $data = array(
            'id' => marmot_encode($isNecessary),
            'name' => BjTemplate::IS_NECESSARY_CN[$isNecessary]
        );

        return $data;
    }

    protected function getIsMaskedCn($isMasked)
    {
        $data = array(
            'id' => marmot_encode($isMasked),
            'name' => BjTemplate::IS_MASKED_CN[$isMasked]
        );

        return $data;
    }

    protected function getDimensionCn($dimension)
    {
        $data = array(
            'id' => marmot_encode($dimension),
            'name' => BjTemplate::DIMENSION_CN[$dimension]
        );

        return $data;
    }
    
    protected function getTypeCn($type)
    {
        $data = array(
            'id' => marmot_encode($type),
            'name' => BjTemplate::TYPE_CN[$type]
        );

        return $data;
    }
}
