<?php
namespace Base\Sdk\WorkOrderTask\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\WorkOrderTask\Model\NullParentTask;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

class ParentTaskRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    const TYPE_MAPS = array(
        ParentTask::TEMPLATE_TYPE['GB'] => 'gbTemplates',
        ParentTask::TEMPLATE_TYPE['BJ'] => 'bjTemplates'
    );

    public function arrayToObject(array $expression, $parentTask = null)
    {
        return $this->translateToObject($expression, $parentTask);
    }

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $parentTask = null)
    {
        if (empty($expression)) {
            return new NullParentTask();
        }

        if ($parentTask == null) {
            $parentTask = new ParentTask();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $parentTask->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $parentTask->setTitle($attributes['title']);
        }

        if (isset($attributes['description'])) {
            $parentTask->setDescription($attributes['description']);
        }

        if (isset($attributes['attachment'])) {
            $parentTask->setAttachment($attributes['attachment']);
        }

        if (isset($attributes['templateType'])) {
            $parentTask->setTemplateType($attributes['templateType']);
        }

        if (isset($attributes['reason'])) {
            $parentTask->setReason($attributes['reason']);
        }

        if (isset($attributes['ratio'])) {
            $parentTask->setRatio($attributes['ratio']);
        }

        if (isset($attributes['endTime'])) {
            $parentTask->setEndTime($attributes['endTime']);
        }

        if (isset($attributes['updateTime'])) {
            $parentTask->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['status'])) {
            $parentTask->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['template']['data'])) {
            if (isset($expression['included'])) {
                $template = $this->changeArrayFormat($relationships['template']['data'], $expression['included']);
            }
            if (!isset($expression['included'])) {
                $template = $this->changeArrayFormat($relationships['template']['data']);
            }

            $templateRestfulTranslator = $this->getTranslatorFactory()
                ->getRestfulTranslator($attributes['templateType']);
            $parentTask->setTemplate($templateRestfulTranslator->arrayToObject($template));
        }

        return $parentTask;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($parentTask, array $keys = array())
    {
        if (!$parentTask instanceof ParentTask) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'description',
                'attachment',
                'assignObjects',
                'templateType',
                'template',
                'reason',
                'endTime'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'parentTasks'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $parentTask->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $parentTask->getTitle();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $parentTask->getDescription();
        }
        if (in_array('attachment', $keys)) {
            $attributes['attachment'] = $parentTask->getAttachment();
        }
        if (in_array('templateType', $keys)) {
            $attributes['templateType'] = $parentTask->getTemplateType();
        }
        if (in_array('endTime', $keys)) {
            $attributes['endTime'] = $parentTask->getEndTime();
        }
        if (in_array('reason', $keys)) {
            $attributes['reason'] = $parentTask->getReason();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('template', $keys)) {
            $templateType = $attributes['templateType'];
            $type = self::TYPE_MAPS[$templateType];

            $expression['data']['relationships']['template']['data'] = array(
                array(
                    'type' => $type,
                    'id' => $parentTask->getTemplate()->getId()
                )
            );
        }

        if (in_array('assignObjects', $keys)) {
            $assignObjects = $this->getAssignObjectsArray($parentTask->getAssignObjects());
            $expression['data']['relationships']['assignObjects']['data'] = $assignObjects;
        }
        
        return $expression;
    }

    protected function getAssignObjectsArray(array $assignObjects)
    {
        $assignObjectsArray = [];

        foreach ($assignObjects[1] as $assignObject) {
            $assignObjectsArray[] = array(
                    'type' => 'userGroups',
                    'id' => $assignObject->getId()
                );
        }

        return $assignObjectsArray;
    }
}
