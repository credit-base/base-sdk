<?php
namespace Base\Sdk\WorkOrderTask\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\WorkOrderTask\Model\NullParentTask;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;

class ParentTaskTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $parentTask = null)
    {
        unset($parentTask);
        unset($expression);
        return new NullParentTask();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($parentTask, array $keys = array())
    {
        if (!$parentTask instanceof ParentTask) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'ratio',
                'description',
                'attachment',
                'templateType',
                'template',
                'endTime',
                'reason',
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($parentTask->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $parentTask->getTitle();
        }
        if (in_array('ratio', $keys)) {
            $expression['ratio'] = $parentTask->getRatio();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $parentTask->getDescription();
        }
        if (in_array('attachment', $keys)) {
            $expression['attachment'] = $parentTask->getAttachment();
        }
        if (in_array('templateType', $keys)) {
            $expression['templateType'] = array(
                'id' => $parentTask->getTemplateType(),
                'name' => ParentTask::TEMPLATE_TYPE_CN[$parentTask->getTemplateType()]
            );
        }
        if (in_array('reason', $keys)) {
            $expression['reason'] = $parentTask->getReason();
        }
        if (in_array('endTime', $keys)) {
            $expression['endTime'] = $parentTask->getEndTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $parentTask->getStatus();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $parentTask->getUpdateTime();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $parentTask->getCreateTime();
        }
        
        if (in_array('template', $keys)) {
            $translator = $this->getTranslatorFactory()->getTranslator($parentTask->getTemplateType());
            $expression['template'] = $translator->objectToArray(
                $parentTask->getTemplate()
            );
        }

        return $expression;
    }
}
