<?php
namespace Base\Sdk\WorkOrderTask\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;
use Base\Sdk\WorkOrderTask\Model\NullWorkOrderTask;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

class WorkOrderTaskRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $workOrderTask = null)
    {
        return $this->translateToObject($expression, $workOrderTask);
    }

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getParentTaskRestfulTranslator(): ParentTaskRestfulTranslator
    {
        return new ParentTaskRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $workOrderTask = null)
    {
        if (empty($expression)) {
            return new NullWorkOrderTask();
        }

        if ($workOrderTask == null) {
            $workOrderTask = new WorkOrderTask();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $workOrderTask->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['reason'])) {
            $workOrderTask->setReason($attributes['reason']);
        }

        if (isset($attributes['feedbackRecords'])) {
            $workOrderTask->setFeedbackRecords($attributes['feedbackRecords']);
        }

        if (isset($attributes['updateTime'])) {
            $workOrderTask->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['createTime'])) {
            $workOrderTask->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['status'])) {
            $workOrderTask->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['parentTask']['data'])) {
            if (isset($expression['included'])) {
                $parentTaskObject = $this->changeArrayFormat(
                    $relationships['parentTask']['data'],
                    $expression['included']
                );
            }
            if (!isset($expression['included'])) {
                $parentTaskObject = $this->changeArrayFormat($relationships['parentTask']['data']);
            }

            $parentTask = $this->getParentTaskRestfulTranslator()->arrayToObject($parentTaskObject);
            
            $workOrderTask->setParentTask($parentTask);
        }

        if (isset($relationships['template']['data'])) {
            if (isset($expression['included'])) {
                $template = $this->changeArrayFormat($relationships['template']['data'], $expression['included']);
            }
            if (!isset($expression['included'])) {
                $template = $this->changeArrayFormat($relationships['template']['data']);
            }

            $templateType = $workOrderTask->getParentTask()->getTemplateType();

            $templateRestfulTranslator = $this->getTranslatorFactory()
                ->getRestfulTranslator($templateType);
            $workOrderTask->setTemplate($templateRestfulTranslator->arrayToObject($template));
        }
        
        if (isset($relationships['assignObject']['data'])) {
            $assignObject = $this->changeArrayFormat($relationships['assignObject']['data']);
            $workOrderTask->setAssignObject($this->getUserGroupRestfulTranslator()->arrayToObject($assignObject));
        }

        return $workOrderTask;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($workOrderTask, array $keys = array())
    {
        if (!$workOrderTask instanceof WorkOrderTask) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'feedbackRecords',
                'reason'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'workOrderTasks'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $workOrderTask->getId();
        }

        $attributes = array();

        if (in_array('reason', $keys)) {
            $attributes['reason'] = $workOrderTask->getReason();
        }

        if (in_array('feedbackRecords', $keys)) {
            $attributes['feedbackRecords'] = $workOrderTask->getFeedbackRecords();
        }

        $expression['data']['attributes'] = $attributes;
        
        return $expression;
    }
}
