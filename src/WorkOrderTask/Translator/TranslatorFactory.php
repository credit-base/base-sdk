<?php
namespace Base\Sdk\WorkOrderTask\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Classes\NullTranslator;

use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\WorkOrderTask\Translator\NullRestfulTranslator;

class TranslatorFactory
{
    const RESTFUL_MAPS = array(
        ParentTask::TEMPLATE_TYPE['GB'] =>
        'Base\Sdk\Template\Translator\GbTemplateRestfulTranslator',
        ParentTask::TEMPLATE_TYPE['BJ'] =>
        'Base\Sdk\Template\Translator\BjTemplateRestfulTranslator',
    );

    const MAPS = array(
        ParentTask::TEMPLATE_TYPE['GB'] =>
        'Base\Sdk\Template\Translator\GbTemplateTranslator',
        ParentTask::TEMPLATE_TYPE['BJ'] =>
        'Base\Sdk\Template\Translator\BjTemplateTranslator',
    );

    public function getRestfulTranslator($type) : IRestfulTranslator
    {
        $translator = isset(self::RESTFUL_MAPS[$type]) ? self::RESTFUL_MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }

    public function getTranslator($type) : ITranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullTranslator::getInstance();
    }
}
