<?php
namespace Base\Sdk\WorkOrderTask\Repository;

use Marmot\Core;

use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskRestfulAdapter;
use Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;

class WorkOrderTaskRepository implements IWorkOrderTaskAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'WORK_ORDER_TASK_LIST';
    const FETCH_ONE_MODEL_UN = 'WORK_ORDER_TASK_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new WorkOrderTaskRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    protected function getAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    
    public function end(WorkOrderTask $workOrderTask) : bool
    {
        return $this->getAdapter()->end($workOrderTask);
    }

    public function confirm(WorkOrderTask $workOrderTask) : bool
    {
        return $this->getAdapter()->confirm($workOrderTask);
    }

    public function revoke(WorkOrderTask $workOrderTask) : bool
    {
        return $this->getAdapter()->revoke($workOrderTask);
    }

    public function feedback(WorkOrderTask $workOrderTask) : bool
    {
        return $this->getAdapter()->feedback($workOrderTask);
    }
}
