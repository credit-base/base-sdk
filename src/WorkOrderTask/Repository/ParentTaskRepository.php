<?php
namespace Base\Sdk\WorkOrderTask\Repository;

use Marmot\Core;

use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\WorkOrderTask\Adapter\ParentTask\ParentTaskRestfulAdapter;
use Base\Sdk\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use Base\Sdk\WorkOrderTask\Model\ParentTask;

class ParentTaskRepository implements IParentTaskAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'PARENT_TASK_LIST';

    public function __construct()
    {
        $this->adapter = new ParentTaskRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    protected function getAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function assign(ParentTask $parentTask) : bool
    {
        return $this->getAdapter()->assign($parentTask);
    }

    public function revoke(ParentTask $parentTask) : bool
    {
        return $this->getAdapter()->revoke($parentTask);
    }
}
