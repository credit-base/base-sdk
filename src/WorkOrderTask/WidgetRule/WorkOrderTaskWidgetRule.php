<?php
namespace Base\Sdk\WorkOrderTask\WidgetRule;

use Respect\Validation\Validator as V;

use Marmot\Core;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
*/
class WorkOrderTaskWidgetRule
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    const TITLE_MIN_LENGTH = 1;
    const TITLE_MAX_LENGTH = 100;
    public function title($title) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::TITLE_MIN_LENGTH,
            self::TITLE_MAX_LENGTH
        )->validate($title)) {
            Core::setLastError(WORK_ORDER_TASK_TITLE_FORMAT_ERROR, array('pointer' => 'title'));
            return false;
        }
        return true;
    }

    public function formatNumeric($data, string $pointer = '') : bool
    {
        if (!is_numeric($data)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    const DESCRIPTION_MIN_LENGTH = 1;
    const DESCRIPTION_MAX_LENGTH = 2000;
    public function description($description) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::DESCRIPTION_MIN_LENGTH,
            self::DESCRIPTION_MAX_LENGTH
        )->validate($description)) {
            Core::setLastError(DESCRIPTION_FORMAT_ERROR, array('pointer' => 'description'));
            return false;
        }

        return true;
    }

    const REASON_MIN_LENGTH = 1;
    const REASON_MAX_LENGTH = 2000;
    public function reason($reason) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::REASON_MIN_LENGTH,
            self::REASON_MAX_LENGTH
        )->validate($reason)) {
            Core::setLastError(REASON_FORMAT_ERROR, array('pointer' => 'reason'));
            return false;
        }

        return true;
    }

    public function attachments($attachments) : bool
    {
        if (!V::arrayType()->validate($attachments)) {
            Core::setLastError(ATTACHMENT_FORMAT_ERROR, array('pointer' => 'attachment'));
            return false;
        }

        if (!V::extension('pdf')->validate($attachments['identify'])) {
            Core::setLastError(ATTACHMENT_FORMAT_ERROR);
            return false;
        }
        
        return true;
    }
}
