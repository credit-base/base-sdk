<?php
namespace Base\Sdk\WorkOrderTask\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullParentTask extends ParentTask implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function assign() : bool
    {
        return $this->resourceNotExist();
    }

    public function revoke() : bool
    {
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
