<?php
namespace Base\Sdk\WorkOrderTask\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Template\Model\Template;
use Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

class WorkOrderTask implements IObject
{
    use Object;

    const STATUS = array(
        'DQR' => 0,
        'YCX' => 1,
        'YQR' => 2,
        'GJZ' => 3,
        'YZJ' => 4
    );

    const STATUS_CN = array(
        self::STATUS['DQR'] => '待确认',
        self::STATUS['YCX'] => '已撤销',
        self::STATUS['YQR'] => '已确认',
        self::STATUS['GJZ'] => '跟进中',
        self::STATUS['YZJ'] => '已终结'
    );

    /**
     * @var $id
     */
    protected $id;
    /**
     * @var ParentTask $parentTask 父任务
     */
    protected $parentTask;
    /**
     * @var Template $template 指派目录
     */
    protected $template;
    /**
     * @var UserGroup $assignObject 指派对象
     */
    protected $assignObject;
    /**
     * @var $isExistedTemplate 是否已存在目录
     */
    protected $isExistedTemplate;
    /**
     * @var $feedbackRecords 反馈记录
     */
    protected $feedbackRecords;

    protected $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->parentTask = new ParentTask();
        $this->template = new Template();
        $this->assignObject = new UserGroup();
        $this->reason = '';
        $this->feedbackRecords = array();
        $this->isExistedTemplate = 0;
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->repository = new WorkOrderTaskRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->parentTask);
        unset($this->template);
        unset($this->assignObject);
        unset($this->reason);
        unset($this->feedbackRecords);
        unset($this->isExistedTemplate);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function setParentTask(ParentTask $parentTask) : void
    {
        $this->parentTask = $parentTask;
    }

    public function getParentTask() : ParentTask
    {
        return $this->parentTask;
    }

    public function setTemplate($template) : void
    {
        $this->template = $template;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function setAssignObject(UserGroup $assignObject) : void
    {
        $this->assignObject = $assignObject;
    }

    public function getAssignObject() : UserGroup
    {
        return $this->assignObject;
    }

    public function setReason(string $reason): void
    {
        $this->reason = $reason;
    }

    public function getReason(): string
    {
        return $this->reason;
    }

    public function setIsExistedTemplate(int $isExistedTemplate): void
    {
        $this->isExistedTemplate = $isExistedTemplate;
    }

    public function getIsExistedTemplate(): int
    {
        return $this->isExistedTemplate;
    }

    public function getRepository(): WorkOrderTaskRepository
    {
        return $this->repository;
    }

    public function setFeedbackRecords(array $feedbackRecords) : void
    {
        $this->feedbackRecords = $feedbackRecords;
    }

    public function getFeedbackRecords()
    {
        return $this->feedbackRecords;
    }

    public function confirm(): bool
    {
        return $this->getRepository()->confirm($this);
    }

    public function end(): bool
    {
        return $this->getRepository()->end($this);
    }

    public function feedback(): bool
    {
        return $this->getRepository()->feedback($this);
    }

    public function revoke(): bool
    {
        return $this->getRepository()->revoke($this);
    }
}
