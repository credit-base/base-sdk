<?php
namespace Base\Sdk\WorkOrderTask\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Base\Sdk\Template\Model\Template;
use Base\Sdk\WorkOrderTask\Repository\ParentTaskRepository;

class ParentTask implements IObject
{
    const TEMPLATE_TYPE = array(
        'GB' => 1,
        'BJ' => 2
    );

    const TEMPLATE_TYPE_CN = array(
        self::TEMPLATE_TYPE['GB'] => '国标目录',
        self::TEMPLATE_TYPE['BJ'] => '本级目录'
    );

    use Object;
    
    private $id;

    private $title;

    private $ratio;

    private $finishCount;
    
    private $description;

    private $attachment;

    private $assignObjects;

    private $templateType;

    private $template;

    private $endTime;

    private $reason;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->ratio = 0;
        $this->description = '';
        $this->attachment = array();
        $this->assignObjects = array();
        $this->templateType = 0;
        $this->template = new Template();
        $this->reason = '';
        $this->endTime = '0000-00-00';
        $this->finishCount = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->repository = new ParentTaskRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->ratio);
        unset($this->description);
        unset($this->attachment);
        unset($this->assignObjects);
        unset($this->templateType);
        unset($this->template);
        unset($this->reason);
        unset($this->endTime);
        unset($this->finishCount);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function setRatio(int $ratio): void
    {
        $this->ratio = $ratio;
    }

    public function getRatio(): int
    {
        return $this->ratio;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setTemplateType(int $templateType): void
    {
        $this->templateType = $templateType;
    }

    public function getTemplateType(): int
    {
        return $this->templateType;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function setAssignObjects(array $assignObjects) : void
    {
        $this->assignObjects = $assignObjects;
    }

    public function getAssignObjects() : array
    {
        return $this->assignObjects;
    }

    public function setEndTime(string $endTime): void
    {
        $this->endTime = $endTime;
    }

    public function getEndTime(): string
    {
        return $this->endTime;
    }

    public function setFinishCount(int $finishCount) : void
    {
        $this->finishCount = $finishCount;
    }

    public function getFinishCount() : int
    {
        return $this->finishCount;
    }

    public function setAttachment(array $attachment): void
    {
        $this->attachment = $attachment;
    }

    public function getAttachment(): array
    {
        return $this->attachment;
    }

    public function setReason(string $reason): void
    {
        $this->reason = $reason;
    }

    public function getReason(): string
    {
        return $this->reason;
    }

    public function getRepository(): ParentTaskRepository
    {
        return $this->repository;
    }

    public function assign(): bool
    {
        return $this->getRepository()->assign($this);
    }

    public function revoke(): bool
    {
        return $this->getRepository()->revoke($this);
    }
}
