<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand;

class FeedbackWorkOrderTaskCommandHandler implements ICommandHandler
{
    use WorkOrderTaskCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof FeedbackWorkOrderTaskCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $this->workOrderTask = $this->getRepository()->fetchOne($command->id);

        $this->workOrderTask->setFeedbackRecords($command->feedbackRecords);

        return $this->workOrderTask->feedback();
    }
}
