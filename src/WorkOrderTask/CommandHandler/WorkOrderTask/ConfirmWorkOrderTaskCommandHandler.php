<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand;

class ConfirmWorkOrderTaskCommandHandler implements ICommandHandler
{
    use WorkOrderTaskCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof ConfirmWorkOrderTaskCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $this->workOrderTask = $this->getRepository()->fetchOne($command->id);

        return $this->workOrderTask->confirm();
    }
}
