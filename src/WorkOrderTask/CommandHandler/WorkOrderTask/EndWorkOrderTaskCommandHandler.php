<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand;

class EndWorkOrderTaskCommandHandler implements ICommandHandler
{
    use WorkOrderTaskCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof EndWorkOrderTaskCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $this->workOrderTask = $this->getRepository()->fetchOne($command->id);
        $this->workOrderTask->setReason($command->reason);

        return $this->workOrderTask->end();
    }
}
