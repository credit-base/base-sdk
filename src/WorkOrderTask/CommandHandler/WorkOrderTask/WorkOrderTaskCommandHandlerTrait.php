<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use Base\Sdk\WorkOrderTask\Model\NullWorkOrderTask;
use Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

trait WorkOrderTaskCommandHandlerTrait
{
    private $repository;

    private $workOrderTask;

    public function __construct()
    {
        $this->repository = new WorkOrderTaskRepository();
        $this->workOrderTask = new NullWorkOrderTask();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->workOrderTask);
    }

    protected function getRepository(): WorkOrderTaskRepository
    {
        return $this->repository;
    }
}
