<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\ParentTask;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand;

class RevokeParentTaskCommandHandler implements ICommandHandler
{
    use ParentTaskCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof RevokeParentTaskCommand)) {
            throw new \InvalidArgumentException;
        }
        
        $this->workOrderTask = $this->getRepository()->fetchOne($command->id);
        $this->workOrderTask->setReason($command->reason);

        return $this->workOrderTask->revoke();
    }
}
