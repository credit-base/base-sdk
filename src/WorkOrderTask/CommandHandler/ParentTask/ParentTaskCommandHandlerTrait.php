<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\ParentTask;

use Base\Sdk\WorkOrderTask\Model\NullParentTask;
use Base\Sdk\WorkOrderTask\Repository\ParentTaskRepository;

trait ParentTaskCommandHandlerTrait
{
    private $repository;

    private $workOrderTask;

    public function __construct()
    {
        $this->repository = new ParentTaskRepository();
        $this->workOrderTask = new NullParentTask();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->workOrderTask);
    }

    protected function getRepository(): ParentTaskRepository
    {
        return $this->repository;
    }
}
