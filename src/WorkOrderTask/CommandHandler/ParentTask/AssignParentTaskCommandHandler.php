<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\ParentTask;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\WorkOrderTask\Command\ParentTask\AssignParentTaskCommand;

class AssignParentTaskCommandHandler implements ICommandHandler
{
    const MAPS = array(
        ParentTask::TEMPLATE_TYPE['GB'] =>
        'Base\Sdk\Template\Repository\GbTemplateRepository',
        ParentTask::TEMPLATE_TYPE['BJ'] =>
        'Base\Sdk\Template\Repository\BjTemplateRepository',
    );

    private $parentTask;
    private $userGroupRepository;

    public function __construct()
    {
        $this->parentTask = new ParentTask();
        $this->userGroupRepository = new UserGroupRepository();
    }

    public function __destruct()
    {
        unset($this->parentTask);
        unset($this->userGroupRepository);
    }

    protected function getParentTask() : ParentTask
    {
        return $this->parentTask;
    }

    protected function getUserGroupRepository() : UserGroupRepository
    {
        return $this->userGroupRepository;
    }

    protected function getTemplateRepository($templateType)
    {
        $repository = self::MAPS[$templateType];

        return new $repository;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ICommand $command)
    {
        if (!($command instanceof AssignParentTaskCommand)) {
            throw new \InvalidArgumentException;
        }

        $parentTask = $this->getParentTask();

        $parentTask->setTitle($command->title);
        $parentTask->setDescription($command->description);
        $parentTask->setEndTime($command->endTime);
        $parentTask->setAttachment($command->attachment);
        $parentTask->setTemplateType($command->templateType);

        $templateRepository = $this->getTemplateRepository($command->templateType);
        $parentTask->setTemplate($templateRepository->fetchOne($command->template));
        
        $parentTask->setAssignObjects(
            $this->getUserGroupRepository()->fetchList($command->assignObjects)
        );

        return $parentTask->assign();
    }
}
