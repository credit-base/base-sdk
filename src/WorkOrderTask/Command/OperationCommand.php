<?php
namespace Base\Sdk\WorkOrderTask\Command;

use Marmot\Interfaces\ICommand;

class OperationCommand implements ICommand
{
    public $id;
    
    public $reason;
    
    public function __construct(
        int $id,
        string $reason
    ) {
        $this->id = $id;
        $this->reason = $reason;
    }
}
