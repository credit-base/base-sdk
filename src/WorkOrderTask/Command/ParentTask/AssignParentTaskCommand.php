<?php
namespace Base\Sdk\WorkOrderTask\Command\ParentTask;

use Marmot\Interfaces\ICommand;

class AssignParentTaskCommand implements ICommand
{
    public $title;
    
    public $description;

    public $endTime;

    public $attachment;
    
    public $assignObjects;

    public $templateType;
    
    public $template;

    public function __construct(
        string $title,
        string $description,
        string $endTime,
        array $attachment,
        array $assignObjects,
        int $templateType,
        int $template
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->endTime = $endTime;
        $this->attachment = $attachment;
        $this->assignObjects = $assignObjects;
        $this->templateType = $templateType;
        $this->template = $template;
    }
}
