<?php
namespace Base\Sdk\WorkOrderTask\Command\WorkOrderTask;

use Marmot\Interfaces\ICommand;

class FeedbackWorkOrderTaskCommand implements ICommand
{
    public $id;

    public $feedbackRecords;

    public function __construct(
        array $feedbackRecords,
        int $id
    ) {
        $this->feedbackRecords = $feedbackRecords;
        $this->id = $id;
    }
}
