<?php
namespace Base\Sdk\CreditPhotography\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Model\NullCreditPhotography;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;
use Base\Sdk\Member\Translator\MemberRestfulTranslator;

class CreditPhotographyRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $creditPhotography = null)
    {
        return $this->translateToObject($expression, $creditPhotography);
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    protected function getMemberRestfulTranslator(): MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $creditPhotography = null)
    {
        if (empty($expression)) {
            return new NullCreditPhotography();
        }

        if ($creditPhotography == null) {
            $creditPhotography = new CreditPhotography();
        }

        $data = $expression['data'];
        
        $id = $data['id'];
        $creditPhotography->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['description'])) {
            $creditPhotography->setDescription($attributes['description']);
        }

        if (isset($attributes['applyStatus'])) {
            $creditPhotography->setApplyStatus($attributes['applyStatus']);
        }

        if (isset($attributes['attachments'])) {
            $creditPhotography->setAttachments($attributes['attachments']);
        }

        if (isset($attributes['rejectReason'])) {
            $creditPhotography->setRejectReason($attributes['rejectReason']);
        }

        if (isset($attributes['createTime'])) {
            $creditPhotography->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['statusTime'])) {
            $creditPhotography->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['updateTime'])) {
            $creditPhotography->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['status'])) {
            $creditPhotography->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['applyCrew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['applyCrew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);

            $crew = empty($relationships['applyCrew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;
            $creditPhotography->setApplyCrew($crew);
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $creditPhotography->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }
        
        return $creditPhotography;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($creditPhotography, array $keys = array())
    {
        if (!$creditPhotography instanceof CreditPhotography) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'description',
                'attachments',
                'rejectReason',
                'member',
                'applyCrew',
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'creditPhotography'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $creditPhotography->getId();
        }

        $attributes = array();

        if (in_array('description', $keys)) {
            $attributes['description'] = $creditPhotography->getDescription();
        }
        if (in_array('attachments', $keys)) {
            $attributes['attachments'] = $creditPhotography->getAttachments();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $creditPhotography->getRejectReason();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $creditPhotography->getMember()->getId()
                )
            );
        }

        if (in_array('applyCrew', $keys)) {
            $expression['data']['relationships']['applyCrew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $creditPhotography->getApplyCrew()->getId()
                )
            );
        }
        
        return $expression;
    }
}
