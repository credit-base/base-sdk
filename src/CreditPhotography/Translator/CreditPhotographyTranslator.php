<?php
namespace Base\Sdk\CreditPhotography\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Model\NullCreditPhotography;
use Base\Sdk\Crew\Translator\CrewTranslator;
use Base\Sdk\Member\Translator\MemberTranslator;

use Base\Sdk\Common\Model\IApplyCategory;

class CreditPhotographyTranslator implements ITranslator
{
    const STATUS_CN = array(
        CreditPhotography::STATUS['NOMAL'] => '正常',
        CreditPhotography::STATUS['DELETE'] => '删除',
    );

    const STATUS_TAG_TYPE = array(
        CreditPhotography::STATUS['NOMAL'] => 'success',
        CreditPhotography::STATUS['DELETE'] => 'danger',
    );

    public function arrayToObject(array $expression, $creditPhotography = null)
    {
        unset($creditPhotography);
        unset($expression);
        return new NullCreditPhotography();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($creditPhotography, array $keys = array())
    {
        if (!$creditPhotography instanceof CreditPhotography) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'description',
                'attachments',
                'applyStatus',
                'rejectReason',
                'member'=>[],
                'applyCrew'=>[],
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($creditPhotography->getId());
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $creditPhotography->getDescription();
        }
        if (in_array('attachments', $keys)) {
            $expression['attachments'] = $creditPhotography->getAttachments();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $creditPhotography->getRejectReason();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = [
                'id' => marmot_encode($creditPhotography->getApplyStatus()),
                'name' => IApplyCategory::APPLY_STATUS_CN[$creditPhotography->getApplyStatus()],
                'type' => IApplyCategory::APPLY_STATUS_TAG_TYPE[$creditPhotography->getApplyStatus()]
            ];
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($creditPhotography->getStatus()),
                'name' => self::STATUS_CN[$creditPhotography->getStatus()],
                'type' => self::STATUS_TAG_TYPE[$creditPhotography->getStatus()]
            ];
        }
        
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $creditPhotography->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $creditPhotography->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $creditPhotography->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $creditPhotography->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $creditPhotography->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $creditPhotography->getCreateTime());
        }
        if (isset($keys['applyCrew'])) {
            $expression['applyCrew'] = $this->getCrewTranslator()->objectToArray(
                $creditPhotography->getApplyCrew(),
                $keys['applyCrew']
            );
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $creditPhotography->getMember(),
                $keys['member']
            );
        }

        return $expression;
    }
}
