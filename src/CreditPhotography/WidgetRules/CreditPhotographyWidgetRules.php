<?php
namespace Base\Sdk\CreditPhotography\WidgetRules;

use Marmot\Core;
use Respect\Validation\Validator as V;

class CreditPhotographyWidgetRules
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
 
    const VIDEO_SIZE_MAX = 1;
    const IMAGES_SIZE_MAX = 9;
    /**
     * @todo 修改存疑 拆分公私有函数与protected函数, 添加单元测试，放开PHPMD检测
     * 可以先验证公共
     * 1.validateXXXExtension 和 验证Size 可以提成2个私有函数，在用 protected 函数进行封装
     * 2.attachemtns 调度两个 protected 函数
     */
    public function attachments($attachments) : bool
    {
        if (!V::arrayType()->validate($attachments)) {
            Core::setLastError(CREDIT_PHOTOGRAPHY_FORMAT_ERROR, array('pointer'=>'attachments'));
            return false;
        }
        foreach ($attachments as $attachment) {
            if ($this->validateImageExtension($attachment['identify']) &&
                count($attachments) <= self::IMAGES_SIZE_MAX) {
                return true;
            }
            if ($this->validateVideoExtension($attachment['identify'])
                && count($attachments) == self::VIDEO_SIZE_MAX) {
                return true;
            }

            Core::setLastError(CREDIT_PHOTOGRAPHY_ATTACHMENT_FORMAT_ERROR, array('pointer'=>'attachments'));
            return false;
        }

        Core::setLastError(CREDIT_PHOTOGRAPHY_FORMAT_ERROR, array('pointer'=>'attachments'));
        return false;
    }

    const  IMAGE_TYPE = ['png','jpg','jpeg','bmp','gif'];

    private function validateImageExtension(string $image) : bool
    {
        foreach (self::IMAGE_TYPE as $value) {
            if (V::extension($value)->validate($image)) {
                return true;
            }
        }
        
        return false;
    }

    const  VIDEO_TYPE = ['mp4'];

    private function validateVideoExtension(string $video) : bool
    {
        foreach (self::VIDEO_TYPE as $value) {
            if (V::extension($value)->validate($video)) {
                return true;
            }
        }
        return false;
    }

    const DESCRIPTION_MIN_LENGTH = 1;
    const DESCRIPTION_MAX_LENGTH = 200;

    public function description($description, $pointer = 'description') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::DESCRIPTION_MIN_LENGTH,
            self::DESCRIPTION_MAX_LENGTH
        )->validate($description)) {
            Core::setLastError(DESCRIPTION_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function isVideo($image) : bool
    {
        if ($this->validateVideoExtension($image)) {
            return true;
        }

        return false;
    }
}
