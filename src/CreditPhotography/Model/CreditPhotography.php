<?php
namespace Base\Sdk\CreditPhotography\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Model\ApplyAbleTrait;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;

use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Model\OperateAbleTrait;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Member\Model\Member;
use Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository;

class CreditPhotography implements IObject, IApplyAble, IOperateAble
{
    use Object, ApplyAbleTrait, OperateAbleTrait;

    const STATUS = array(
        'NOMAL' => 0,
        'DELETE' => -2,
    );

    private $id;

    private $description;

    private $attachments;

    private $rejectReason;
    
    private $member;

    private $applyCrew;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->description = '';
        $this->rejectReason = '';
        $this->attachments = array();
        $this->member = Core::$container->has('member') ? Core::$container->get('member') : new Member();
        $this->applyCrew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->status = self::STATUS['NOMAL'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new CreditPhotographyRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->description);
        unset($this->attachments);
        unset($this->applyStatus);
        unset($this->rejectReason);
        unset($this->applyCrew);
        unset($this->member);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function setApplyStatus(int $applyStatus): void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus(): int
    {
        return $this->applyStatus;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setAttachments(array $attachments): void
    {
        $this->attachments = $attachments;
    }

    public function getAttachments(): array
    {
        return $this->attachments;
    }

    public function setRejectReason(string $rejectReason): void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason(): string
    {
        return $this->rejectReason;
    }

    public function setApplyCrew(Crew $applyCrew) : void
    {
        $this->applyCrew = $applyCrew;
    }

    public function getApplyCrew() : Crew
    {
        return $this->applyCrew;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function getRepository(): CreditPhotographyRepository
    {
        return $this->repository;
    }

    /**
     * 通过驳回
     */
    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 发布
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 删除
     */
    public function delete(): bool
    {
        return $this->getRepository()->deletes($this);
    }
}
