<?php
namespace Base\Sdk\CreditPhotography\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Sdk\Common\Model\NullOperateAbleTrait;
use Base\Sdk\Common\Model\NullApplyAbleTrait;

class NullCreditPhotography extends CreditPhotography implements INull
{
    use NullApplyAbleTrait, NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
