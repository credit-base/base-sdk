<?php
namespace Base\Sdk\CreditPhotography\Adapter\CreditPhotography;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;

interface ICreditPhotographyAdapter extends ICreditPhotographyFetchAdapter, ICreditPhotographyOperateAbleAdapter
{
    public function deletes(CreditPhotography $creditPhotography) : bool;
}
