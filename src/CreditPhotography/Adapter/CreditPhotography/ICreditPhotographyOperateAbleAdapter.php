<?php
namespace Base\Sdk\CreditPhotography\Adapter\CreditPhotography;

use Base\Sdk\Common\Adapter\IApplyAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

interface ICreditPhotographyOperateAbleAdapter extends IApplyAbleAdapter, IOperateAbleAdapter
{
}
