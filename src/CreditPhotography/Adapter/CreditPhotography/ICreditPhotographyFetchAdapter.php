<?php
namespace Base\Sdk\CreditPhotography\Adapter\CreditPhotography;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface ICreditPhotographyFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
