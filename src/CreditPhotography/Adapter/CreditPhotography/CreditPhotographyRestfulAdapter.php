<?php
namespace Base\Sdk\CreditPhotography\Adapter\CreditPhotography;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Model\NullCreditPhotography;
use Base\Sdk\CreditPhotography\Translator\CreditPhotographyRestfulTranslator;

class CreditPhotographyRestfulAdapter extends GuzzleAdapter implements ICreditPhotographyAdapter
{
    use FetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'CREDIT_PHOTOGRAPHY_LIST'=>[
            'fields' => [],
            'include' => 'member,applyCrew'
        ],
        'CREDIT_PHOTOGRAPHY_FETCH_ONE'=>[
            'fields'=>[],
            'include'=> 'member,applyCrew'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new CreditPhotographyRestfulTranslator();
        $this->resource = 'creditPhotography';
        $this->scenario = array();
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            101 => [
                'description' => DESCRIPTION_FORMAT_ERROR,
                'attachments' => ATTACHMENT_FORMAT_ERROR,
                'memberId' => PARAMETER_FORMAT_ERROR,
                'applyCrewId' => PARAMETER_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
            ],
            102 =>[
                'status' => STATUS_CAN_NOT_MODIFY,
                'applyStatus' => STATUS_CAN_NOT_MODIFY,
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullCreditPhotography());
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function approveAction(IApplyAble $creditPhotography) : bool
    {
        $data = $this->getTranslator()->objectToArray($creditPhotography, array('applyCrew'));
       
        $this->patch(
            $this->getResource().'/'.$creditPhotography->getId().'/approve',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($creditPhotography);
            return true;
        }
        return false;
    }

    protected function rejectAction(IApplyAble $creditPhotography) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $creditPhotography,
            array(
                'rejectReason',
                'applyCrew'
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$creditPhotography->getId().'/reject',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($creditPhotography);
            return true;
        }
        return false;
    }

    protected function addAction(CreditPhotography $creditPhotography) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $creditPhotography,
            array(
                'description',
                'attachments',
                'member'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($creditPhotography);
            return true;
        }

        return false;
    }

    public function deletes(CreditPhotography $creditPhotography) : bool
    {
        $this->delete(
            $this->getResource().'/'.$creditPhotography->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($creditPhotography);
            return true;
        }

        return false;
    }

    protected function editAction(CreditPhotography $creditPhotography) : bool
    {
        unset($creditPhotography);
        return false;
    }
}
