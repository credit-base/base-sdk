<?php
namespace Base\Sdk\CreditPhotography\Command\CreditPhotography;

use Marmot\Interfaces\ICommand;

class AddCreditPhotographyCommand implements ICommand
{
    public $description;

    public $attachments;

    public $id;
    
    public function __construct(
        string $description,
        array $attachments,
        int $id = 0
    ) {
        $this->description = $description;
        $this->attachments = $attachments;
        $this->id = $id;
    }
}
