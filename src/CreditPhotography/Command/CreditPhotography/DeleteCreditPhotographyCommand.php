<?php
namespace Base\Sdk\CreditPhotography\Command\CreditPhotography;

use Marmot\Interfaces\ICommand;

class DeleteCreditPhotographyCommand implements ICommand
{
    public $id;
    
    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}
