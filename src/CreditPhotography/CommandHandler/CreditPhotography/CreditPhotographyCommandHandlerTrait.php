<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository;

trait CreditPhotographyCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new CreditPhotographyRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : CreditPhotographyRepository
    {
        return $this->repository;
    }
    
    protected function fetchCreditPhotography(int $id) : CreditPhotography
    {
        return $this->getRepository()->fetchOne($id);
    }
}
