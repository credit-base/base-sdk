<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\ApproveCommandHandler;
use Base\Sdk\Common\Command\ApproveCommand;

class ApproveCreditPhotographyCommandHandler extends ApproveCommandHandler
{
    use CreditPhotographyCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchCreditPhotography($id);
    }
}
