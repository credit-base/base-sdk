<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\RejectCommandHandler;

use Base\Sdk\Common\Command\RejectCommand;

class RejectCreditPhotographyCommandHandler extends RejectCommandHandler
{
    use CreditPhotographyCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchCreditPhotography($id);
    }
}
