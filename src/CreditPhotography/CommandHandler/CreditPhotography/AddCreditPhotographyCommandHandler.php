<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand;

class AddCreditPhotographyCommandHandler implements ICommandHandler
{
    private $creditPhotography;

    public function __construct()
    {
        $this->creditPhotography = new CreditPhotography();
    }

    public function __destruct()
    {
        unset($this->creditPhotography);
    }

    protected function getCreditPhotography() : CreditPhotography
    {
        return $this->creditPhotography;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddCreditPhotographyCommand)) {
            throw new \InvalidArgumentException;
        }

        $creditPhotography = $this->getCreditPhotography();
        $creditPhotography->setDescription($command->description);
        $creditPhotography->setAttachments($command->attachments);

        return $creditPhotography->add();
    }
}
