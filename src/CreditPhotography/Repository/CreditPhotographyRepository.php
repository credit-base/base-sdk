<?php
namespace Base\Sdk\CreditPhotography\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Base\Sdk\Common\Repository\FetchRepositoryTrait;
use Base\Sdk\Common\Repository\ErrorRepositoryTrait;
use Base\Sdk\Common\Repository\AsyncRepositoryTrait;
use Base\Sdk\Common\Repository\ApplyAbleRepositoryTrait;
use Base\Sdk\Common\Repository\OperateAbleRepositoryTrait;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter;
use Base\Sdk\CreditPhotography\Adapter\CreditPhotography\CreditPhotographyRestfulAdapter;

class CreditPhotographyRepository extends Repository implements ICreditPhotographyAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait,
        ApplyAbleRepositoryTrait,
        OperateAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'CREDIT_PHOTOGRAPHY_LIST';
    const FETCH_ONE_MODEL_UN = 'CREDIT_PHOTOGRAPHY_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CreditPhotographyRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function deletes(CreditPhotography $creditPhotography) : bool
    {
        return $this->getAdapter()->deletes($creditPhotography);
    }
}
