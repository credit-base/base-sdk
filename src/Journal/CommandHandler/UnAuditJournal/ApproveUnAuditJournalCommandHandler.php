<?php
namespace Base\Sdk\Journal\CommandHandler\UnAuditJournal;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\ApproveCommandHandler;
use Base\Sdk\Common\Command\ApproveCommand;

class ApproveUnAuditJournalCommandHandler extends ApproveCommandHandler
{
    use UnAuditJournalCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditJournal($id);
    }
}
