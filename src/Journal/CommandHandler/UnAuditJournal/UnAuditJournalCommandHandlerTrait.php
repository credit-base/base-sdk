<?php
namespace Base\Sdk\Journal\CommandHandler\UnAuditJournal;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Repository\UnAuditJournalRepository;

use Marmot\Interfaces\ICommand;

trait UnAuditJournalCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new UnAuditJournalRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : UnAuditJournalRepository
    {
        return $this->repository;
    }
    
    protected function fetchUnAuditJournal(int $id) : UnAuditJournal
    {
        return $this->getRepository()->fetchOne($id);
    }

    // protected function getCrewRepository() : CrewRepository
    // {
    //     return new CrewRepository();
    // }

    // protected function fetchCrew(int $id) : Crew
    // {
    //     return $this->getCrewRepository()->fetchOne($id);
    // }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }
}
