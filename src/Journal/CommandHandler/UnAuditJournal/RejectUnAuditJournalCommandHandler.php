<?php
namespace Base\Sdk\Journal\CommandHandler\UnAuditJournal;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\CommandHandler\RejectCommandHandler;

use Base\Sdk\Common\Command\RejectCommand;

class RejectUnAuditJournalCommandHandler extends RejectCommandHandler
{
    use UnAuditJournalCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchUnAuditJournal($id);
    }
}
