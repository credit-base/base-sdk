<?php
namespace Base\Sdk\Journal\CommandHandler\UnAuditJournal;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Journal\Command\UnAuditJournal\EditUnAuditJournalCommand;

class EditUnAuditJournalCommandHandler implements ICommandHandler
{
    use UnAuditJournalCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof EditUnAuditJournalCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditJournal = $this->fetchUnAuditJournal($command->id);

        $unAuditJournal->setDescription($command->description);
        $unAuditJournal->setAuthImages($command->authImages);
        $unAuditJournal->setAttachment($command->attachment);
        $unAuditJournal->setTitle($command->title);
        $unAuditJournal->setSource($command->source);
        $unAuditJournal->setCover($command->cover);
        $unAuditJournal->setStatus($command->status);
        $unAuditJournal->setYear($command->year);
       
        return $unAuditJournal->edit();
    }
}
