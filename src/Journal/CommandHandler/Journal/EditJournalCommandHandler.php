<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Journal\Command\Journal\EditJournalCommand;

class EditJournalCommandHandler implements ICommandHandler
{
    use JournalCommandHandlerTrait;

    protected function executeAction($command)
    {
        if (!($command instanceof EditJournalCommand)) {
            throw new \InvalidArgumentException;
        }

        $journal = $this->fetchJournal($command->id);
        
        $journal->setTitle($command->title);
        $journal->setSource($command->source);
        $journal->setDescription($command->description);
        $journal->setAuthImages($command->authImages);
        $journal->setAttachment($command->attachment);
        $journal->setStatus($command->status);
        $journal->setCover($command->cover);
        $journal->setYear($command->year);
       
        return $journal->edit();
    }
}
