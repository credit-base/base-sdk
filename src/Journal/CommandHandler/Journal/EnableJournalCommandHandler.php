<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\CommandHandler\EnableCommandHandler;
use Base\Sdk\Common\Command\EnableCommand;

class EnableJournalCommandHandler extends EnableCommandHandler
{
    use JournalCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchJournal($id);
    }
}
