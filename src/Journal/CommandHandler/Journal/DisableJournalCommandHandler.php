<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\CommandHandler\DisableCommandHandler;
use Base\Sdk\Common\Command\DisableCommand;

class DisableJournalCommandHandler extends DisableCommandHandler
{
    use JournalCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchJournal($id);
    }
}
