<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Repository\JournalRepository;

use Marmot\Interfaces\ICommand;

trait JournalCommandHandlerTrait
{
    private $journal;

    private $repository;
    
    public function __construct()
    {
        $this->journal = new Journal();
        $this->repository = new JournalRepository();
    }

    public function __destruct()
    {
        unset($this->journal);
        unset($this->repository);
    }

    protected function getJournal() : Journal
    {
        return $this->journal;
    }

    protected function getRepository() : JournalRepository
    {
        return $this->repository;
    }
    
    protected function fetchJournal(int $id) : Journal
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }
}
