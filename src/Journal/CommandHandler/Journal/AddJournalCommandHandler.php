<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Marmot\Interfaces\ICommandHandler;

use Base\Sdk\Journal\Command\Journal\AddJournalCommand;

class AddJournalCommandHandler implements ICommandHandler
{
    use JournalCommandHandlerTrait;

    public function executeAction($command)
    {
        if (!($command instanceof AddJournalCommand)) {
            throw new \InvalidArgumentException;
        }

        $journal = $this->getJournal();

        $journal->setCover($command->cover);
        $journal->setYear($command->year);
        $journal->setTitle($command->title);
        $journal->setSource($command->source);
        $journal->setStatus($command->status);
        $journal->setDescription($command->description);
        $journal->setAuthImages($command->authImages);
        $journal->setAttachment($command->attachment);

        return $journal->add();
    }
}
