<?php
namespace Base\Sdk\Journal\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Model\EnableAbleTrait;
use Base\Sdk\Common\Model\OperateAbleTrait;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Journal\Repository\JournalRepository;

class Journal implements IObject, IEnableAble, IOperateAble
{
    use Object, EnableAbleTrait, OperateAbleTrait;

    private $id;

    private $title;

    private $source;

    private $cover;
    
    private $description;

    private $attachment;

    private $authImages;

    private $year;

    private $crew;

    private $userGroup;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->source = '';
        $this->description = '';
        $this->authImages = array();
        $this->attachment = array();
        $this->cover = array();
        $this->year = 0;
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->userGroup = new UserGroup();
        $this->status = self::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new JournalRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->source);
        unset($this->description);
        unset($this->attachment);
        unset($this->cover);
        unset($this->authImages);
        unset($this->status);
        unset($this->year);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setAuthImages(array $authImages) : void
    {
        $this->authImages = $authImages;
    }

    public function getAuthImages() : array
    {
        return $this->authImages;
    }

    public function setAttachment(array $attachment): void
    {
        $this->attachment = $attachment;
    }

    public function getAttachment(): array
    {
        return $this->attachment;
    }

    public function setCover(array $cover): void
    {
        $this->cover = $cover;
    }

    public function getCover(): array
    {
        return $this->cover;
    }

    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getRepository(): JournalRepository
    {
        return $this->repository;
    }

    /**
     * 启用禁用
     */
    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 新增编辑
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }
}
