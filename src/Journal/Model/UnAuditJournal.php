<?php
namespace Base\Sdk\Journal\Model;

use Marmot\Core;
use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Model\ApplyAbleTrait;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Journal\Repository\UnAuditJournalRepository;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

class UnAuditJournal extends Journal implements IApplyAble
{
    const OPERATION_TYPE = array(
        'NULL' => 0,
        'ADD' => 1,
        'EDIT' => 2,
        'ENABLED' => 3,
        'DISABLED' => 4,
    );

    use ApplyAbleTrait;

    private $rejectReason;

    private $id;

    private $operationType;

    private $applyInfoType;

    private $applyCrew;

    private $applyUserGroup;

    private $relation;

    private $publishUserGroup;
    
    private $unAuditJournalRepository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->id = !empty($id) ? $id : 0;
        $this->rejectReason = '';
        $this->applyInfoType = 0;
        $this->operationType = self::OPERATION_TYPE['NULL'];
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->relation = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->publishUserGroup = new UserGroup();
        $this->applyCrew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->applyUserGroup = new UserGroup();
        $this->unAuditJournalRepository = new UnAuditJournalRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->relation);
        unset($this->applyCrew);
        unset($this->rejectReason);
        unset($this->applyUserGroup);
        unset($this->publishUserGroup);
        unset($this->operationType);
        unset($this->applyStatus);
        unset($this->applyInfoType);
        unset($this->unAuditJournalRepository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setOperationType(int $operationType): void
    {
        $this->operationType = $operationType;
    }

    public function getOperationType(): int
    {
        return $this->operationType;
    }

    public function setApplyInfoType(int $applyInfoType): void
    {
        $this->applyInfoType = $applyInfoType;
    }

    public function getApplyInfoType(): int
    {
        return $this->applyInfoType;
    }

    public function setApplyStatus(int $applyStatus): void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus(): int
    {
        return $this->applyStatus;
    }

    public function setRejectReason(string $rejectReason): void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason(): string
    {
        return $this->rejectReason;
    }

    public function setRelation(Crew $relation) : void
    {
        $this->relation = $relation;
    }

    public function getRelation() : Crew
    {
        return $this->relation;
    }

    public function setPublishUserGroup(UserGroup $publishUserGroup) : void
    {
        $this->publishUserGroup = $publishUserGroup;
    }

    public function getPublishUserGroup() : UserGroup
    {
        return $this->publishUserGroup;
    }

    public function setApplyCrew(Crew $applyCrew) : void
    {
        $this->applyCrew = $applyCrew;
    }

    public function getApplyCrew() : Crew
    {
        return $this->applyCrew;
    }

    public function setApplyUserGroup(UserGroup $applyUserGroup) : void
    {
        $this->applyUserGroup = $applyUserGroup;
    }

    public function getApplyUserGroup() : UserGroup
    {
        return $this->applyUserGroup;
    }

    public function getUnAuditJournalRepository(): UnAuditJournalRepository
    {
        return $this->unAuditJournalRepository;
    }

    /**
     * 通过驳回
     * @return [bool]
     */
    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getUnAuditJournalRepository();
    }

    /**
     * 新增编辑
     * @return [bool]
     */
    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getUnAuditJournalRepository();
    }
}
