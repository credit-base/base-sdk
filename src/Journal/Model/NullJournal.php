<?php
namespace Base\Sdk\Journal\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Sdk\Common\Model\NullOperateAbleTrait;
use Base\Sdk\Common\Model\NullEnableAbleTrait;

class NullJournal extends Journal implements INull
{
    use NullEnableAbleTrait, NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
