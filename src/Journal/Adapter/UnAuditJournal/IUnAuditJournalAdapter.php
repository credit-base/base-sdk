<?php
namespace Base\Sdk\Journal\Adapter\UnAuditJournal;

interface IUnAuditJournalAdapter extends IUnAuditJournalFetchAdapter, IUnAuditJournalOperateAbleAdapter
{
    
}
