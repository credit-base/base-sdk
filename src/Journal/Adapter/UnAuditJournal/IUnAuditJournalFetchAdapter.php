<?php
namespace Base\Sdk\Journal\Adapter\UnAuditJournal;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IUnAuditJournalFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
