<?php
namespace Base\Sdk\Journal\Adapter\UnAuditJournal;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;

interface IUnAuditJournalOperateAbleAdapter extends IOperateAbleAdapter, IApplyAbleAdapter
{
    
}
