<?php
namespace Base\Sdk\Journal\Adapter\UnAuditJournal;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Model\NullUnAuditJournal;

use Base\Sdk\Journal\Translator\UnAuditJournalRestfulTranslator;

class UnAuditJournalRestfulAdapter extends GuzzleAdapter implements IUnAuditJournalAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'UN_AUDIT_JOURNAL_LIST'=>[
            'fields' => [],
            'include' => 'crew,publishUserGroup,applyUserGroup,applyCrew,relation'
        ],
        'UN_AUDIT_JOURNAL_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,publishUserGroup,applyUserGroup,applyCrew,relation'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UnAuditJournalRestfulTranslator();
        $this->resource = 'unAuditedJournals';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            101 => [
                'title' => TITLE_FORMAT_ERROR,
                'source' => SOURCE_FORMAT_ERROR,
                'year' => YEAR_FORMAT_ERROR,
                'authImages' => IMAGE_FORMAT_ERROR,
                'cover' => IMAGE_FORMAT_ERROR,
                'attachment' => ATTACHMENT_FORMAT_ERROR,
                'description' => DESCRIPTION_FORMAT_ERROR,
                'status' => STATUS_FORMAT_ERROR,
                'crewId' => PARAMETER_FORMAT_ERROR,
                'applyCrewId' => PARAMETER_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullUnAuditJournal());
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function addAction(UnAuditJournal $unAuditJournal) : bool
    {
        unset($unAuditJournal);
        return false;
    }

    protected function approveAction(IApplyAble $unAuditJournal) : bool
    {
        $data = $this->getTranslator()->objectToArray($unAuditJournal, array('applyCrew'));
       
        $this->patch(
            $this->getResource().'/'.$unAuditJournal->getId().'/approve',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject($unAuditJournal);
            return true;
        }
        return false;
    }

    protected function editAction(UnAuditJournal $unAuditJournal) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditJournal,
            array(
                'title',
                'source',
                'cover',
                'attachment',
                'authImages',
                'year',
                'description',
                'status',
                'crew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$unAuditJournal->getId().'/'.'resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditJournal);
            return true;
        }

        return false;
    }

    protected function rejectAction(IApplyAble $unAuditJournal) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditJournal,
            array(
                'rejectReason',
                'applyCrew'
            )
        );
       
        $this->patch(
            $this->getResource().'/'.$unAuditJournal->getId().'/reject',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditJournal);
            return true;
        }
        return false;
    }
}
