<?php
namespace Base\Sdk\Journal\Adapter\Journal;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;

interface IJournalOperateAbleAdapter extends IOperateAbleAdapter, IEnableAbleAdapter
{
    
}
