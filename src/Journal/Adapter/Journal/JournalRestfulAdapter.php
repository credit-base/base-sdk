<?php
namespace Base\Sdk\Journal\Adapter\Journal;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use Base\Sdk\Common\Adapter\CommonMapErrorsTrait;
use Base\Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Base\Sdk\Common\Adapter\TopAbleRestfulAdapterTrait;
use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\Model\ITopAble;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Model\NullJournal;

use Base\Sdk\Journal\Translator\JournalRestfulTranslator;

class JournalRestfulAdapter extends GuzzleAdapter implements IJournalAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'JOURNAL_LIST'=>[
            'fields'=>[],
            'include'=>'crew,publishUserGroup'
        ],
        'JOURNAL_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew,publishUserGroup'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new JournalRestfulTranslator();
        $this->resource = 'journals';
        $this->scenario = array();
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            101 => [
                'title' => TITLE_FORMAT_ERROR,
                'source' => SOURCE_FORMAT_ERROR,
                'cover' => IMAGE_FORMAT_ERROR,
                'attachment' => ATTACHMENT_FORMAT_ERROR,
                'description' => DESCRIPTION_FORMAT_ERROR,
                'status' => STATUS_FORMAT_ERROR,
                'year' => YEAR_FORMAT_ERROR,
                'authImages' => IMAGE_FORMAT_ERROR,
                'crewId' => PARAMETER_FORMAT_ERROR,
                'applyCrewId' => PARAMETER_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
            ]
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullJournal());
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function addAction(Journal $journal) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $journal,
            array(
                'title',
                'source',
                'cover',
                'attachment',
                'authImages',
                'description',
                'year',
                'status',
                'crew'
            )
        );
     
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($journal);
            return true;
        }

        return false;
    }

    protected function enableAction(IEnableAble $journal) : bool
    {
        $data = $this->getTranslator()->objectToArray($journal, array('crew'));
       
        $this->patch(
            $this->getResource().'/'.$journal->getId().'/enable',
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($journal);
            return true;
        }
        return false;
    }

    protected function editAction(Journal $journal) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $journal,
            array(
                'title',
                'source',
                'cover',
                'attachment',
                'authImages',
                'description',
                'year',
                'status',
                'crew'
            )
        );

        $this->patch(
            $this->getResource().'/'.$journal->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($journal);
            return true;
        }

        return false;
    }

    protected function disableAction(IEnableAble $journal) : bool
    {
        $data = $this->getTranslator()->objectToArray($journal, array('crew'));

        $this->patch(
            $this->getResource().'/'.$journal->getId().'/disable',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($journal);
            return true;
        }
        return false;
    }
}
