<?php
namespace Base\Sdk\Journal\Adapter\Journal;

interface IJournalAdapter extends IJournalFetchAdapter, IJournalOperateAbleAdapter
{
    
}
