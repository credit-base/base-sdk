<?php
namespace Base\Sdk\Journal\Adapter\Journal;

use Marmot\Interfaces\IAsyncAdapter;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

interface IJournalFetchAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    
}
