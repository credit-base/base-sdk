<?php
namespace Base\Sdk\Journal\WidgetRules;

use Respect\Validation\Validator as V;

use Base\Sdk\Common\WidgetRules\WidgetRules;

use Marmot\Core;

class JournalWidgetRules
{
    private static $instance;

    protected function getCommonWidgetRules() : WidgetRules
    {
        return new WidgetRules();
    }

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function attachment($attachment, $pointer = 'attachment') : bool
    {
        if (!V::arrayType()->validate($attachment)) {
            Core::setLastError(ATTACHMENT_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        if (!$this->validateAttachmentExtension($attachment['identify'])) {
            return false;
        }
      
        return true;
    }

    private function validateAttachmentExtension(string $attachment, $pointer = 'attachment') : bool
    {
        if (!V::extension('pdf')->validate($attachment)) {
            Core::setLastError(ATTACHMENT_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    const AUTH_IMAGES_LENGTH = 5;

    public function authImages($authImages, $pointer = 'authImages') : bool
    {
        if (count($authImages) > self::AUTH_IMAGES_LENGTH) {
            Core::setLastError(IMAGE_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        if (!$this->getCommonWidgetRules()->images($authImages, $pointer)) {
            return false;
        }

        return true;
    }

    const YEAR_MIN_LENGTH = 1901;
    const YEAR_MAX_LENGTH = 2155;

    public function year($year, $pointer = 'year') : bool
    {
        if (!V::intVal()->between(
            self::YEAR_MIN_LENGTH,
            self::YEAR_MAX_LENGTH
        )->validate($year)) {
            Core::setLastError(YEAR_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }
}
