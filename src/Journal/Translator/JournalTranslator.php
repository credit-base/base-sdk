<?php
namespace Base\Sdk\Journal\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Model\NullJournal;

use Base\Sdk\Crew\Translator\CrewTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;

class JournalTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $journal = null)
    {
        unset($journal);
        unset($expression);
        return new NullJournal();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

   /**
     *
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($journal, array $keys = array())
    {
        if (!$journal instanceof Journal) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'cover',
                'attachment',
                'content',
                'description',
                'authImages',
                'year',
                'status',
                'crew' => [],
                'userGroup'=> [],
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($journal->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $journal->getTitle();
        }
        if (in_array('source', $keys)) {
            $expression['source'] = $journal->getSource();
        }
        if (in_array('attachment', $keys)) {
            $expression['attachment'] = $journal->getAttachment();
        }
        if (in_array('authImages', $keys)) {
            $expression['authImages'] = $journal->getAuthImages();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $journal->getDescription();
        }
        if (in_array('year', $keys)) {
            $expression['year'] = $journal->getYear();
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $journal->getCover();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($journal->getStatus()),
                'name' => IApplyCategory::STATUS_CN[$journal->getStatus()],
                'type' => IApplyCategory::STATUS_TAG_TYPE[$journal->getStatus()]
            ];
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $journal->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $journal->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $journal->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $journal->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $journal->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $journal->getCreateTime());
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $journal->getCrew(),
                $keys['crew']
            );
        }
        if (isset($keys['userGroup'])) {
            $expression['publishUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $journal->getUserGroup(),
                $keys['userGroup']
            );
        }

        return $expression;
    }
}
