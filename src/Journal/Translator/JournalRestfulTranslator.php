<?php
namespace Base\Sdk\Journal\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Model\NullJournal;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

class JournalRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $journal = null)
    {
        return $this->translateToObject($expression, $journal);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $journal = null)
    {
        if (empty($expression)) {
            return new NullJournal();
        }

        if ($journal == null) {
            $journal = new Journal();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $journal->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $journal->setTitle($attributes['title']);
        }

        if (isset($attributes['description'])) {
            $journal->setDescription($attributes['description']);
        }

        if (isset($attributes['attachment'])) {
            $journal->setAttachment($attributes['attachment']);
        }

        if (isset($attributes['year'])) {
            $journal->setYear($attributes['year']);
        }

        if (isset($attributes['source'])) {
            $journal->setSource($attributes['source']);
        }

        if (isset($attributes['cover'])) {
            $journal->setCover($attributes['cover']);
        }

        if (isset($attributes['authImages'])) {
            $journal->setAuthImages($attributes['authImages']);
        }

        if (isset($attributes['createTime'])) {
            $journal->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['statusTime'])) {
            $journal->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['updateTime'])) {
            $journal->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['status'])) {
            $journal->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['crew']['data']);
            
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
            
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;

            $journal->setCrew($crew);
        }

        if (isset($relationships['publishUserGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['publishUserGroup']['data']);
            $journal->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }
        
        return $journal;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($journal, array $keys = array())
    {
        if (!$journal instanceof Journal) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'cover',
                'attachment',
                'authImages',
                'description',
                'year',
                'status',
                'crew'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'journals'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $journal->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $journal->getTitle();
        }
        if (in_array('source', $keys)) {
            $attributes['source'] = $journal->getSource();
        }
        if (in_array('attachment', $keys)) {
            $attributes['attachment'] = $journal->getAttachment();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $journal->getStatus();
        }
        if (in_array('year', $keys)) {
            $attributes['year'] = $journal->getYear();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $journal->getDescription();
        }
        if (in_array('authImages', $keys)) {
            $attributes['authImages'] = $journal->getAuthImages();
        }
        if (in_array('cover', $keys)) {
            $attributes['cover'] = $journal->getCover();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $journal->getCrew()->getId()
                )
            );
        }
        
        return $expression;
    }
}
