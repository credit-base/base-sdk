<?php
namespace Base\Sdk\Journal\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Model\NullUnAuditJournal;
use Base\Sdk\Common\Translator\RestfulTranslatorTrait;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD)
 */
class UnAuditJournalRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $unAuditJournal = null)
    {
        return $this->translateToObject($expression, $unAuditJournal);
    }

    protected function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }

    protected function getCrewRestfulTranslator(): CrewRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function translateToObject(array $expression, $unAuditJournal = null)
    {
        if (empty($expression)) {
            return new NullUnAuditJournal();
        }

        if ($unAuditJournal == null) {
            $unAuditJournal = new UnAuditJournal();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $unAuditJournal->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $unAuditJournal->setTitle($attributes['title']);
        }

        if (isset($attributes['source'])) {
            $unAuditJournal->setSource($attributes['source']);
        }

        if (isset($attributes['description'])) {
            $unAuditJournal->setDescription($attributes['description']);
        }

        if (isset($attributes['attachment'])) {
            $unAuditJournal->setAttachment($attributes['attachment']);
        }

        if (isset($attributes['applyStatus'])) {
            $unAuditJournal->setApplyStatus($attributes['applyStatus']);
        }

        if (isset($attributes['rejectReason'])) {
            $unAuditJournal->setRejectReason($attributes['rejectReason']);
        }

        if (isset($attributes['operationType'])) {
            $unAuditJournal->setOperationType($attributes['operationType']);
        }

        if (isset($attributes['cover'])) {
            $unAuditJournal->setCover($attributes['cover']);
        }

        if (isset($attributes['year'])) {
            $unAuditJournal->setYear($attributes['year']);
        }

        if (isset($attributes['authImages'])) {
            $unAuditJournal->setAuthImages($attributes['authImages']);
        }

        if (isset($attributes['applyInfoType'])) {
            $unAuditJournal->setApplyInfoType($attributes['applyInfoType']);
        }

        if (isset($attributes['createTime'])) {
            $unAuditJournal->setCreateTime($attributes['createTime']);
        }

        if (isset($attributes['updateTime'])) {
            $unAuditJournal->setUpdateTime($attributes['updateTime']);
        }

        if (isset($attributes['statusTime'])) {
            $unAuditJournal->setStatusTime($attributes['statusTime']);
        }

        if (isset($attributes['status'])) {
            $unAuditJournal->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['crew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
           
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;
            $unAuditJournal->setCrew($crew);
        }

        if (isset($relationships['publishUserGroup']['data'])) {
            $publishUserGroup = $this->changeArrayFormat($relationships['publishUserGroup']['data']);
            $unAuditJournal->setPublishUserGroup(
                $this->getUserGroupRestfulTranslator()->arrayToObject($publishUserGroup)
            );
        }

        if (isset($relationships['applyUserGroup']['data'])) {
            $applyUserGroup = $this->changeArrayFormat($relationships['applyUserGroup']['data']);
            $unAuditJournal->setApplyUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($applyUserGroup));
        }

        if (isset($relationships['applyCrew']['data'])) {
            $crewFormat = $this->changeArrayFormat($relationships['applyCrew']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($crewFormat);
           
            $applyCrew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;
            $unAuditJournal->setApplyCrew($applyCrew);
        }

        if (isset($relationships['relation']['data'])) {
            $relationData = $this->changeArrayFormat($relationships['relation']['data']);
            $crewFormat = $this->getCrewRestfulTranslator()->arrayToObject($relationData);
           
            $crew = empty($relationships['crew']['data']['id'])
            ? (Core::$container->has('crew') ? Core::$container->get('crew') : new Crew())
            : $crewFormat;
            $unAuditJournal->setRelation($crew);
        }

        return $unAuditJournal;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($unAuditJournal, array $keys = array())
    {
        if (!$unAuditJournal instanceof UnAuditJournal) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'cover',
                'attachment',
                'authImages',
                'year',
                'description',
                'rejectReason',
                'status',
                'crew',
                'applyCrew',
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'unAuditedJournals'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $unAuditJournal->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $unAuditJournal->getTitle();
        }
        if (in_array('source', $keys)) {
            $attributes['source'] = $unAuditJournal->getSource();
        }
        if (in_array('attachment', $keys)) {
            $attributes['attachment'] = $unAuditJournal->getAttachment();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $unAuditJournal->getRejectReason();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $unAuditJournal->getStatus();
        }
        if (in_array('year', $keys)) {
            $attributes['year'] = $unAuditJournal->getYear();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $unAuditJournal->getDescription();
        }
        if (in_array('authImages', $keys)) {
            $attributes['authImages'] = $unAuditJournal->getAuthImages();
        }
        if (in_array('cover', $keys)) {
            $attributes['cover'] = $unAuditJournal->getCover();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $unAuditJournal->getCrew()->getId()
                )
            );
        }

        if (in_array('applyCrew', $keys)) {
            $expression['data']['relationships']['applyCrew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $unAuditJournal->getApplyCrew()->getId()
                )
            );
        }
        
        return $expression;
    }
}
