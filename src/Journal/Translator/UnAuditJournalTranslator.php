<?php
namespace Base\Sdk\Journal\Translator;

use Marmot\Interfaces\ITranslator;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Model\NullUnAuditJournal;
use Base\Sdk\Crew\Translator\CrewTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;

class UnAuditJournalTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $unAuditJournal = null)
    {
        unset($unAuditJournal);
        unset($expression);
        return new NullUnAuditJournal();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($unAuditJournal, array $keys = array())
    {
        if (!$unAuditJournal instanceof UnAuditJournal) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'cover',
                'attachment',
                'authImages',
                'rejectReason',
                'description',
                'year',
                'status',
                'applyStatus',
                'applyInfoType',
                'operationType',
                'relation' =>[],
                'crew' => [],
                'applyCrew' => [],
                'applyUserGroup' => [],
                'publishUserGroup' => [],
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($unAuditJournal->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $unAuditJournal->getTitle();
        }
        if (in_array('source', $keys)) {
            $expression['source'] = $unAuditJournal->getSource();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $unAuditJournal->getRejectReason();
        }
        if (in_array('attachment', $keys)) {
            $expression['attachment'] = $unAuditJournal->getAttachment();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $unAuditJournal->getDescription();
        }
        if (in_array('year', $keys)) {
            $expression['year'] = $unAuditJournal->getYear();
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $unAuditJournal->getCover();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = [
                'id' => marmot_encode($unAuditJournal->getStatus()),
                'name' => IApplyCategory::STATUS_CN[$unAuditJournal->getStatus()],
                'type' => IApplyCategory::STATUS_TAG_TYPE[$unAuditJournal->getStatus()]
            ];
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = [
                'id' => marmot_encode($unAuditJournal->getApplyStatus()),
                'name' => IApplyCategory::APPLY_STATUS_CN[$unAuditJournal->getApplyStatus()],
                'type' => IApplyCategory::APPLY_STATUS_TAG_TYPE[$unAuditJournal->getApplyStatus()]
            ];
        }
        if (in_array('operationType', $keys)) {
            $expression['operationType']= [
                'id' => marmot_encode($unAuditJournal->getOperationType()),
                'name' => IApplyCategory::OPERATION_TYPE_CN[$unAuditJournal->getOperationType()]
            ];
        }
        if (in_array('authImages', $keys)) {
            $expression['authImages'] = $unAuditJournal->getAuthImages();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $unAuditJournal->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y年m月d日 H时i分', $unAuditJournal->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $unAuditJournal->getStatusTime();
            $expression['statusTimeFormat'] = date('Y年m月d日 H时i分', $unAuditJournal->getStatusTime());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $unAuditJournal->getCreateTime();
            $expression['createTimeFormat'] = date('Y年m月d日 H时i分', $unAuditJournal->getCreateTime());
        }
        if (isset($keys['crew'])) {
            $expression['crew'] = $this->getCrewTranslator()->objectToArray(
                $unAuditJournal->getCrew(),
                $keys['crew']
            );
        }
        if (isset($keys['relation'])) {
            $expression['relation'] = $this->getCrewTranslator()->objectToArray(
                $unAuditJournal->getRelation(),
                $keys['relation']
            );
        }
        if (isset($keys['applyCrew'])) {
            $expression['applyCrew'] = $this->getCrewTranslator()->objectToArray(
                $unAuditJournal->getApplyCrew(),
                $keys['applyCrew']
            );
        }
        if (isset($keys['publishUserGroup'])) {
            $expression['publishUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $unAuditJournal->getPublishUserGroup(),
                $keys['publishUserGroup']
            );
        }
        if (isset($keys['applyUserGroup'])) {
            $expression['applyUserGroup'] = $this->getUserGroupTranslator()->objectToArray(
                $unAuditJournal->getApplyUserGroup(),
                $keys['applyUserGroup']
            );
        }

        return $expression;
    }
}
