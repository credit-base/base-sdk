<?php
namespace Base\Sdk\Journal\Command;

use Marmot\Interfaces\ICommand;

 /**
   * @SuppressWarnings(PHPMD)
   */
class OperationCommand implements ICommand
{
    public $id;
    
    public $title;

    public $source;

    public $cover;

    public $attachment;

    public $bannerImage;

    public $description;

    public $status;

    public $year;
    
    public function __construct(
        string $title,
        string $source,
        string $description,
        int $status,
        int $year,
        int $id,
        array $cover = array(),
        array $attachment = array(),
        array $authImages = array()
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->source = $source;
        $this->cover = $cover;
        $this->attachment = $attachment;
        $this->authImages = $authImages;
        $this->description = $description;
        $this->status = $status;
        $this->year = $year;
    }
}
