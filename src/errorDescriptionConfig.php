<?php

return array(
    CSRF_VERIFY_FAILURE => array(
        'id'=>CSRF_VERIFY_FAILURE,
        'link'=>'',
        'status'=>403,
        'code'=>'CSRF_VERIFY_FAILURE',
        'title'=>'当前页面信息失效，请刷新重试',
        'detail'=>'当前页面信息失效，请刷新重试',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    AFS_VERIFY_FAILURE => array(
        'id'=>AFS_VERIFY_FAILURE,
        'link'=>'',
        'status'=>403,
        'code'=>'AFS_VERIFY_FAILURE',
        'title'=>'滑动验证失效,请刷新页面',
        'detail'=>'滑动验证失效,请刷新页面',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    NEED_SIGNIN => array(
        'id'=>NEED_SIGNIN,
        'link'=>'',
        'status'=>403,
        'code'=>'NEED_SIGNIN',
        'title'=>'登录状态已失效，请您重新登录',
        'detail'=>'登录状态已失效，请您重新登录',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    SMS_SEND_TOO_QUICK => array(
        'id'=>SMS_SEND_TOO_QUICK,
        'link'=>'',
        'status'=>403,
        'code'=>'SMS_SEND_TOO_QUICK',
        'title'=>'短信发送太频繁,请稍后再试',
        'detail'=>'短信发送太频繁,请稍后再试',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    HASH_INVALID => array(
        'id'=>HASH_INVALID,
        'link'=>'',
        'status'=>403,
        'code'=>'HASH_INVALID',
        'title'=>'哈希无效,请刷新页面',
        'detail'=>'哈希无效,请刷新页面',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    LOGIN_EXPIRED => array(
        'id'=>LOGIN_EXPIRED,
        'link'=>'',
        'status'=>403,
        'code'=>'LOGIN_EXPIRED',
        'title'=>'登录状态已失效，请您重新登录',
        'detail'=>'登录状态已失效，请您重新登录',
        'source'=>array(
        ),
        'meta'=>array()
    ),
    PURVIEW_UNDEFINED => array(
        'id'=>PURVIEW_UNDEFINED,
        'link'=>'',
        'status'=>403,
        'code'=>'PURVIEW_UNDEFINED',
        'title'=>'该账号未分配权限，请联系管理员！',
        'detail'=>'该账号未分配权限，请联系管理员！',
        'source'=>array(
            'pointer'=>'purview'
        ),
        'meta'=>array()
    ),
    CAPTCHA_ERROR => array(
        'id'=>CAPTCHA_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CAPTCHA_ERROR',
        'title'=>'验证码不正确,请重新输入',
        'detail'=>'验证码不正确,请重新输入',
        'source'=>array(
            'pointer'=>'captcha'
        ),
        'meta'=>array()
    ),
    PARAMETER_IS_UNIQUE => array(
        'id' => PARAMETER_IS_UNIQUE,
        'link' => '',
        'status' => 403,
        'code' => 'PARAMETER_IS_UNIQUE',
        'title' => '数据已经存在,请重新输入',
        'detail' => '数据已经存在,请重新输入',
        'source' => array(
            'pointer' => ''
        ),
        'meta' => array()
    ),
    PARAMETER_FORMAT_ERROR => array(
        'id' => PARAMETER_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'PARAMETER_FORMAT_ERROR',
        'title' => '数据格式不正确,请重新输入',
        'detail' => '数据格式不正确,请重新输入',
        'source' => array(
            'pointer' => ''
        ),
        'meta' => array()
    ),
    PARAMETER_IS_EMPTY => array(
        'id'=>PARAMETER_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'PARAMETER_IS_EMPTY',
        'title'=>'数据不能为空,请继续完善信息',
        'detail'=>'数据不能为空,请继续完善信息',
        'source'=>array(
            'pointer'=>''
        ),
        'meta'=>array()
    ),
    STATUS_CAN_NOT_MODIFY => array(
        'id' => STATUS_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_CAN_NOT_MODIFY',
        'title'=>'该状态不能进行此操作,请刷新页面后重新尝试',
        'detail'=>'该状态不能进行此操作,请刷新页面后重新尝试',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    IMAGE_FORMAT_ERROR => array(
        'id'=>IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'IMAGE_FORMAT_ERROR',
        'title'=>'图片格式不正确,应为jpg,jpeg,png格式,请重新上传',
        'detail'=>'图片格式不正确,应为jpg,jpeg,png格式,请重新上传',
        'source'=>array(
            'pointer'=>'image'
        ),
        'meta'=>array()
    ),
    ATTACHMENT_FORMAT_ERROR => array(
        'id'=>ATTACHMENT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ATTACHMENT_FORMAT_ERROR',
        'title'=>'附件格式不正确,应为zip, doc, docx, xls, xlsx, pdf, rar, ppt 格式,请重新上传',
        'detail'=>'附件格式不正确,应为zip, doc, docx, xls, xlsx, pdf, rar, ppt 格式,请重新上传',
        'source'=>array(
            'pointer'=>'attachments'
        ),
        'meta'=>array()
    ),
    REAL_NAME_FORMAT_ERROR => array(
        'id'=>REAL_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'REAL_NAME_FORMAT_ERROR',
        'title'=>'姓名格式不正确,应为2-15位字符,请重新输入',
        'detail'=>'姓名格式不正确,应为2-15位字符,请重新输入',
        'source'=>array(
            'pointer'=>'realName'
        ),
        'meta'=>array()
    ),
    CELLPHONE_FORMAT_ERROR => array(
        'id'=>CELLPHONE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CELLPHONE_FORMAT_ERROR',
        'title'=>'手机号格式不正确,应为11位数字,请重新输入',
        'detail'=>'手机号格式不正确,应为11位数字,请重新输入',
        'source'=>array(
            'pointer'=>'cellphone'
        ),
        'meta'=>array()
    ),
    CARDID_FORMAT_ERROR => array(
        'id'=>CARDID_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CARDID_FORMAT_ERROR',
        'title'=>'身份号格式不正确,应为15或18位数字或数字+大写字母X,请重新输入',
        'detail'=>'身份号格式不正确,应为15或18位数字或数字+大写字母X,请重新输入',
        'source'=>array(
            'pointer'=>'cardId'
        ),
        'meta'=>array()
    ),
    NAME_FORMAT_ERROR => array(
        'id'=>NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NAME_FORMAT_ERROR',
        'title'=>'名称格式不正确,应为2-10位字符,请重新输入',
        'detail'=>'名称格式不正确,应为2-10位字符,请重新输入',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    TITLE_FORMAT_ERROR => array(
        'id'=>TITLE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TITLE_FORMAT_ERROR',
        'title'=>'标题格式不正确,应为5-80位字符,请重新输入',
        'detail'=>'标题格式不正确,应为5-80位字符,请重新输入',
        'source'=>array(
            'pointer'=>'title'
        ),
        'meta'=>array()
    ),
    DESCRIPTION_FORMAT_ERROR => array(
        'id'=>DESCRIPTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'DESCRIPTION_FORMAT_ERROR',
        'title'=>'描述格式不正确,应为1-200位字符,请重新输入',
        'detail'=>'描述格式不正确,应为1-200位字符,请重新输入',
        'source'=>array(
            'pointer'=>''
        ),
        'meta'=>array()
    ),
    REASON_FORMAT_ERROR => array(
        'id'=>REASON_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'REASON_FORMAT_ERROR',
        'title'=>'原因格式不正确,应为1-2000位字符,请重新输入',
        'detail'=>'原因格式不正确,应为1-2000位字符,请重新输入',
        'source'=>array(
            'pointer'=>'reason'
        ),
        'meta'=>array()
    ),
    JWT_TOKEN_EMPTY => array(
        'id'=>JWT_TOKEN_EMPTY,
        'link'=>'',
        'status'=>409,
        'code'=>'JWT_TOKEN_EMPTY',
        'title'=>'jwt-token 为空，您没有访问权限',
        'detail'=>'jwt-token 为空，您没有访问权限',
        'source'=>array(
            'pointer'=>'jwt'
        ),
        'meta'=>array()
    ),
    JWT_TOKEN_OVERDUE => array(
        'id'=>JWT_TOKEN_OVERDUE,
        'link'=>'',
        'status'=>409,
        'code'=>'JWT_TOKEN_OVERDUE',
        'title'=>'jwt-token 已过期，请重新登录',
        'detail'=>'jwt-token 已过期，请重新登录',
        'source'=>array(
            'pointer'=>'jwt'
        ),
        'meta'=>array()
    ),
    JWT_TOKEN_ERROR => array(
        'id'=>JWT_TOKEN_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'JWT_TOKEN_ERROR',
        'title'=>'jwt-token 验证失败,请重新登录',
        'detail'=>'jwt-token 验证失败,请重新登录',
        'source'=>array(
            'pointer'=>'jwt'
        ),
        'meta'=>array()
    ),
    PASSWORD_FORMAT_ERROR => array(
        'id'=>PASSWORD_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'PASSWORD_FORMAT_ERROR',
        'title'=>'密码格式不正确,应为8-20位大写字母+小写字母+数字+特殊字符的组合,请重新输入',
        'detail'=>'密码格式不正确,应为8-20位大写字母+小写字母+数字+特殊字符的组合,请重新输入',
        'source'=>array(
            'pointer'=>'password'
        ),
        'meta'=>array()
    ),
    OLD_PASSWORD_INCORRECT => array(
        'id'=>OLD_PASSWORD_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'OLD_PASSWORD_INCORRECT',
        'title'=>'当前密码不正确,请重新输入',
        'detail'=>'当前密码不正确,请重新输入',
        'source'=>array(
            'pointer'=>'oldPassword'
        ),
        'meta'=>array()
    ),
    PASSWORD_INCORRECT => array(
        'id'=>PASSWORD_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PASSWORD_INCORRECT',
        'title'=>'密码不正确,请重新输入',
        'detail'=>'密码不正确,请重新输入',
        'source'=>array(
            'pointer'=>'password'
        ),
        'meta'=>array()
    ),
    PASSWORD_INCONSISTENCY => array(
        'id'=>PASSWORD_INCONSISTENCY,
        'link'=>'',
        'status'=>403,
        'code'=>'PASSWORD_INCONSISTENCY',
        'title'=>'密码不一致,请重新输入',
        'detail'=>'密码不一致,请重新输入',
        'source'=>array(
            'pointer'=>'password'
        ),
        'meta'=>array()
    ),
    CELLPHONE_EXIST => array(
        'id'=>CELLPHONE_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'CELLPHONE_EXIST',
        'title'=>'手机号已经存在,请重新输入',
        'detail'=>'手机号已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'cellphone'
        ),
        'meta'=>array()
    ),
    CELLPHONE_NOT_EXIST => array(
        'id'=>CELLPHONE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'CELLPHONE_NOT_EXIST',
        'title'=>'该账号不存在，请重新输入',
        'detail'=>'该账号不存在，请重新输入',
        'source'=>array(
            'pointer'=>'cellphone'
        ),
        'meta'=>array()
    ),
    USER_STATUS_DISABLE => array(
        'id'=>USER_STATUS_DISABLE,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_STATUS_DISABLE',
        'title'=>'当前账号已被禁用，请联系管理员查询具体情况',
        'detail'=>'当前账号已被禁用，请联系管理员查询具体情况',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    DEPARTMENT_NAME_FORMAT_ERROR => array(
        'id'=>DEPARTMENT_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'DEPARTMENT_NAME_FORMAT_ERROR',
        'title'=>'科室名称格式不正确,应为2-15位字符,请重新输入',
        'detail'=>'科室名称格式不正确',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    DEPARTMENT_NAME_IS_UNIQUE => array(
        'id'=>DEPARTMENT_NAME_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'DEPARTMENT_NAME_IS_UNIQUE',
        'title'=>'科室名称已经存在,请重新输入',
        'detail'=>'科室名称已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    DEPARTMENT_USER_GROUP_IS_EMPTY => array(
        'id'=>DEPARTMENT_USER_GROUP_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'DEPARTMENT_USER_GROUP_IS_EMPTY',
        'title'=>'所属委办局不能为空,请继续完善信息',
        'detail'=>'所属委办局不能为空,请继续完善信息',
        'source'=>array(
            'pointer'=>'userGroup'
        ),
        'meta'=>array()
    ),
    CREW_CATEGORY_NOT_EXIST => array(
        'id'=>CREW_CATEGORY_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_CATEGORY_NOT_EXIST',
        'title'=>'员工类型不存在,请核对后重新输入',
        'detail'=>'员工类型不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'category'
        ),
        'meta'=>array()
    ),
    CREW_PURVIEW_FORMAT_ERROR => array(
        'id'=>CREW_PURVIEW_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_PURVIEW_FORMAT_ERROR',
        'title'=>'员工权限范围格式不正确,请重新输入',
        'detail'=>'员工权限范围格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'purview'
        ),
        'meta'=>array()
    ),
    CREW_PURVIEW_HIERARCHY_FORMAT_ERROR => array(
        'id'=>CREW_PURVIEW_HIERARCHY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_PURVIEW_HIERARCHY_FORMAT_ERROR',
        'title'=>'员工类型权限不足',
        'detail'=>'员工类型权限不足',
        'source'=>array(
            'pointer'=>'purview'
        ),
        'meta'=>array()
    ),
    DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP => array(
        'id'=>DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP,
        'link'=>'',
        'status'=>403,
        'code'=>'DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP',
        'title'=>'该所属科室不属于该所属委办局,请重新输入',
        'detail'=>'该所属科室不属于该所属委办局,请重新输入',
        'source'=>array(
            'pointer'=>'department'
        ),
        'meta'=>array()
    ),
    CREW_USER_GROUP_FORMAT_ERROR => array(
        'id'=>CREW_USER_GROUP_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_USER_GROUP_FORMAT_ERROR',
        'title'=>'所属委办局格式不正确,请重新输入',
        'detail'=>'所属委办局格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'department'
        ),
        'meta'=>array()
    ),
    CREW_DEPARTMENT_FORMAT_ERROR => array(
        'id'=>CREW_DEPARTMENT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_DEPARTMENT_FORMAT_ERROR',
        'title'=>'所属科室格式不正确,请重新输入',
        'detail'=>'所属科室格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'department'
        ),
        'meta'=>array()
    ),
    TEMPLATE_NAME_FORMAT_ERROR => array(
        'id' => TEMPLATE_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_NAME_FORMAT_ERROR',
        'title'=>'目录名称格式不正确',
        'detail'=>'目录名称长度应为1-100字符',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    IDENTIFY_FORMAT_EXIST => array(
        'id' => IDENTIFY_FORMAT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'IDENTIFY_FORMAT_EXIST',
        'title'=>'目录标识不允许重复',
        'detail'=>'目录标识不允许重复',
        'source'=>array(
            'pointer'=>'identify'
        ),
        'meta'=>array()
    ),
    IDENTIFY_FORMAT_ERROR => array(
        'id' => IDENTIFY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'IDENTIFY_FORMAT_ERROR',
        'title'=>'目录标识格式不正确',
        'detail'=>'目录标识拼音首字母大写，在模板信息内不能重复',
        'source'=>array(
            'pointer'=>'identify'
        ),
        'meta'=>array()
    ),
    SUBJECT_CATEGORY_FORMAT_ERROR => array(
        'id' => SUBJECT_CATEGORY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SUBJECT_CATEGORY_FORMAT_ERROR',
        'title'=>'主体类别格式不正确',
        'detail'=>'主体类别格式不正确',
        'source'=>array(
            'pointer'=>'subjectCategory'
        ),
        'meta'=>array()
    ),
    DIMENSION_FORMAT_ERROR => array(
        'id' => DIMENSION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'DIMENSION_FORMAT_ERROR',
        'title'=>'公开范围格式不正确',
        'detail'=>'公开范围格式不正确',
        'source'=>array(
            'pointer'=>'dimension'
        ),
        'meta'=>array()
    ),
    LENGTH_FORMAT_ERROR => array(
        'id' => LENGTH_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'LENGTH_FORMAT_ERROR',
        'title'=>'模板信息数据长度格式不正确',
        'detail'=>'模板信息数据长度格式不正确',
        'source'=>array(
            'pointer'=>'length'
        ),
        'meta'=>array()
    ),
    ITEMS_FORMAT_ERROR => array(
        'id' => ITEMS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ITEMS_FORMAT_ERROR',
        'title'=>'模板信息格式不正确',
        'detail'=>'模板信息格式不正确',
        'source'=>array(
            'pointer'=>'items'
        ),
        'meta'=>array()
    ),
    OPTIONS_FORMAT_ERROR => array(
        'id' => OPTIONS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'OPTIONS_FORMAT_ERROR',
        'title'=>'模板信息可选范围格式不正确',
        'detail'=>'模板信息可选范围格式不正确',
        'source'=>array(
            'pointer'=>'options'
        ),
        'meta'=>array()
    ),
    IS_NECESSARY_FORMAT_ERROR => array(
        'id' => IS_NECESSARY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'IS_NECESSARY_FORMAT_ERROR',
        'title'=>'模板信息是否必填格式不正确',
        'detail'=>'模板信息是否必填格式不正确',
        'source'=>array(
            'pointer'=>'isNecessary'
        ),
        'meta'=>array()
    ),
    IS_MASKED_FORMAT_ERROR => array(
        'id' => IS_MASKED_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'IS_MASKED_FORMAT_ERROR',
        'title'=>'模板信息是否脱敏格式不正确',
        'detail'=>'模板信息是否脱敏格式不正确',
        'source'=>array(
            'pointer'=>'isMasked'
        ),
        'meta'=>array()
    ),
    MASK_RULE_FORMAT_ERROR => array(
        'id' => MASK_RULE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'MASK_RULE_FORMAT_ERROR',
        'title'=>'模板信息脱敏规则格式不正确',
        'detail'=>'模板信息脱敏规则格式不正确',
        'source'=>array(
            'pointer'=>'maskRule'
        ),
        'meta'=>array()
    ),
    REMARKS_FORMAT_ERROR => array(
        'id' => REMARKS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'REMARKS_FORMAT_ERROR',
        'title'=>'备注格式不正确',
        'detail'=>'备注长度应为1-2000字符',
        'source'=>array(
            'pointer'=>'remarks'
        ),
        'meta'=>array()
    ),
    ITEM_TYPE_FORMAT_ERROR => array(
        'id' => ITEM_TYPE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ITEM_TYPE_FORMAT_ERROR',
        'title'=>'模板信息数据类型格式不正确',
        'detail'=>'模板信息数据类型格式不正确',
        'source'=>array(
            'pointer'=>'type'
        ),
        'meta'=>array()
    ),
    TYSHXYDM_FORMAT_ERROR => array(
        'id' => TYSHXYDM_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TYSHXYDM_FORMAT_ERROR',
        'title'=>'统一社会信用代码格式错误',
        'detail'=>'统一社会信用代码格式错误',
        'source'=>array(
            'pointer'=>'TYSHXYDM'
        ),
        'meta'=>array()
    ),
    ZTMC_FORMAT_ERROR => array(
        'id' => ZTMC_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ZTMC_FORMAT_ERROR',
        'title'=>'信用主体名称格式错误',
        'detail'=>'信用主体名称格式错误',
        'source'=>array(
            'pointer'=>'ZTMC'
        ),
        'meta'=>array()
    ),
    ATTACHMENT_FORMAT_ERROR => array(
        'id' => ATTACHMENT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ATTACHMENT_FORMAT_ERROR',
        'title'=>'附件格式错误',
        'detail'=>'附件格式错误',
        'source'=>array(
            'pointer'=>'attachment'
        ),
        'meta'=>array()
    ),
    WORK_ORDER_TASK_TITLE_FORMAT_ERROR => array(
        'id' => WORK_ORDER_TASK_TITLE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'WORK_ORDER_TASK_TITLE_FORMAT_ERROR',
        'title'=>'标题格式错误',
        'detail'=>'标题格式错误',
        'source'=>array(
            'pointer'=>'title'
        ),
        'meta'=>array()
    ),
    WORK_ORDER_TASK_TITLE_FORMAT_ERROR => array(
        'id' => WORK_ORDER_TASK_TITLE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'WORK_ORDER_TASK_TITLE_FORMAT_ERROR',
        'title'=>'工单任务标题格式不正确',
        'detail'=>'工单任务标题格式不正确',
        'source'=>array(
            'pointer'=>'title'
        ),
        'meta'=>array()
    ),
    TEMPLATE_TYPE_FORMAT_ERROR => array(
        'id' => TEMPLATE_TYPE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_TYPE_FORMAT_ERROR',
        'title'=>'工单任务目录类型格式不正确',
        'detail'=>'工单任务目录类型格式不正确',
        'source'=>array(
            'pointer'=>'templateType'
        ),
        'meta'=>array()
    ),
    END_TIME_FORMAT_ERROR => array(
        'id' => END_TIME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'END_TIME_FORMAT_ERROR',
        'title'=>'终结时间格式不正确',
        'detail'=>'终结时间格式不正确',
        'source'=>array(
            'pointer'=>'endTime'
        ),
        'meta'=>array()
    ),
    FEEDBACK_RECORDS_FORMAT_ERROR => array(
        'id' => FEEDBACK_RECORDS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FEEDBACK_RECORDS_FORMAT_ERROR',
        'title'=>'反馈信息格式不正确',
        'detail'=>'反馈信息格式不正确',
        'source'=>array(
            'pointer'=>'feedbackRecords'
        ),
        'meta'=>array()
    ),
    CREW_FORMAT_ERROR => array(
        'id' => CREW_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_FORMAT_ERROR',
        'title'=>'反馈信息用户格式不正确',
        'detail'=>'反馈信息用户格式不正确',
        'source'=>array(
            'pointer'=>'crew'
        ),
        'meta'=>array()
    ),
    USER_GROUP_FORMAT_ERROR => array(
        'id' => USER_GROUP_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_GROUP_FORMAT_ERROR',
        'title'=>'反馈信息委办局格式不正确',
        'detail'=>'反馈信息委办局格式不正确',
        'source'=>array(
            'pointer'=>'userGroup'
        ),
        'meta'=>array()
    ),
    TEMPLATE_ID_FORMAT_ERROR => array(
        'id' => TEMPLATE_ID_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TEMPLATE_ID_FORMAT_ERROR',
        'title'=>'反馈信息目录格式不正确',
        'detail'=>'反馈信息目录格式不正确',
        'source'=>array(
            'pointer'=>'templateId'
        ),
        'meta'=>array()
    ),
    IS_EXISTED_TEMPLATE_PARAMETER_FORMAT_ERROR => array(
        'id' => IS_EXISTED_TEMPLATE_PARAMETER_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'IS_EXISTED_TEMPLATE_PARAMETER_FORMAT_ERROR',
        'title'=>'反馈信息是否已存在目录格式不正确',
        'detail'=>'反馈信息是否已存在目录格式不正确',
        'source'=>array(
            'pointer'=>'isExistedTemplate'
        ),
        'meta'=>array()
    ),
    NEWS_TYPE_NOT_EXIST => array(
        'id' => NEWS_TYPE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'NEWS_TYPE_NOT_EXIST',
        'title'=>'新闻栏目分类不存在',
        'detail'=>'新闻栏目分类不存在',
        'source'=>array(
            'pointer'=>'newsType'
        ),
        'meta'=>array()
    ),
    DIMENSION_NOT_EXIST => array(
        'id' => DIMENSION_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'DIMENSION_NOT_EXIST',
        'title'=>'新闻公共类型不存在',
        'detail'=>'新闻公共类型不存在',
        'source'=>array(
            'pointer'=>'dimension'
        ),
        'meta'=>array()
    ),
    HOME_PAGE_SHOW_STATUS_NOT_EXIST => array(
        'id' => HOME_PAGE_SHOW_STATUS_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'HOME_PAGE_SHOW_STATUS_NOT_EXIST',
        'title'=>'新闻首页展示状态不存在',
        'detail'=>'新闻首页展示状态不存在',
        'source'=>array(
            'pointer'=>'homePageShowStatus'
        ),
        'meta'=>array()
    ),
    BANNER_STATUS_NOT_EXIST => array(
        'id' => BANNER_STATUS_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'BANNER_STATUS_NOT_EXIST',
        'title'=>'新闻轮播状态不存在',
        'detail'=>'新闻轮播状态不存在',
        'source'=>array(
            'pointer'=>'bannerStatus'
        ),
        'meta'=>array()
    ),
    STATUS_NOT_ENABLE => array(
        'id' => STATUS_NOT_ENABLE,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_NOT_ENABLE',
        'title'=>'状态非启用',
        'detail'=>'状态非启用',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    STATUS_NOT_DISABLE => array(
        'id' => STATUS_NOT_DISABLE,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_NOT_DISABLE',
        'title'=>'状态非禁用',
        'detail'=>'状态非禁用',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    STATUS_NOT_TOP => array(
        'id' => STATUS_NOT_TOP,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_NOT_TOP',
        'title'=>'状态非置顶',
        'detail'=>'状态非置顶',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    STATUS_NOT_CANCEL_TOP => array(
        'id' => STATUS_NOT_CANCEL_TOP,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_NOT_CANCEL_TOP',
        'title'=>'状态已置顶',
        'detail'=>'状态已置顶',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    STATUS_FORMAT_ERROR => array(
        'id' => STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'STATUS_FORMAT_ERROR',
        'title'=>'状态格式不正确',
        'detail'=>'状态格式不正确',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    SOURCE_FORMAT_ERROR => array(
        'id' => SOURCE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SOURCE_FORMAT_ERROR',
        'title'=>'来源格式不正确',
        'detail'=>'来源格式不正确',
        'source'=>array(
            'pointer'=>'source'
        ),
        'meta'=>array()
    ),
    NAME_NOT_EXIT => array(
        'id' => NAME_NOT_EXIT,
        'link'=>'',
        'status'=>403,
        'code'=>'NAME_NOT_EXIT',
        'title'=>'用户名不存在,请核对后重新输入',
        'detail'=>'用户名不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'userName'
        ),
        'meta'=>array()
    ),
    EMAIL_FORMAT_ERROR => array(
        'id' => EMAIL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'EMAIL_FORMAT_ERROR',
        'title'=>'邮箱格式不正确,应为2-30位字符,且必须包含"@"和".",请重新输入',
        'detail'=>'邮箱格式不正确,应为2-30位字符,且必须包含"@"和".",请重新输入',
        'source'=>array(
            'pointer'=>'email'
        ),
        'meta'=>array()
    ),
    CONTACT_ADDRESS_FORMAT_ERROR => array(
        'id' => CONTACT_ADDRESS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CONTACT_ADDRESS_FORMAT_ERROR',
        'title'=>'联系地址格式不正确,应为1-255位字符,请重新输入',
        'detail'=>'联系地址格式不正确,应为1-255位字符,请重新输入',
        'source'=>array(
            'pointer'=>'contactAddress'
        ),
        'meta'=>array()
    ),
    SECURITY_QUESTION_FORMAT_ERROR => array(
        'id' => SECURITY_QUESTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SECURITY_QUESTION_FORMAT_ERROR',
        'title'=>'密保问题格式不正确,请重新输入',
        'detail'=>'密保问题格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'securityQuestion'
        ),
        'meta'=>array()
    ),
    SECURITY_ANSWER_FORMAT_ERROR => array(
        'id' => SECURITY_ANSWER_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SECURITY_ANSWER_FORMAT_ERROR',
        'title'=>'密保答案格式不正确,应为1-30位字符,请重新输入',
        'detail'=>'密保答案格式不正确,应为1-30位字符,请重新输入',
        'source'=>array(
            'pointer'=>'securityAnswer'
        ),
        'meta'=>array()
    ),
    GENDER_FORMAT_ERROR => array(
        'id' => GENDER_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'GENDER_FORMAT_ERROR',
        'title'=>'性别格式不正确,请重新输入',
        'detail'=>'性别格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'email'
        ),
        'meta'=>array()
    ),
    USER_NAME_EXIST => array(
        'id' => USER_NAME_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_NAME_EXIST',
        'title'=>'用户名已经存在,请重新输入',
        'detail'=>'用户名已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'userName'
        ),
        'meta'=>array()
    ),
    EMAIL_EXIST => array(
        'id' => EMAIL_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'EMAIL_EXIST',
        'title'=>'邮箱已经存在,请重新输入',
        'detail'=>'邮箱已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'email'
        ),
        'meta'=>array()
    ),
    USER_NAME_FORMAT_ERROR => array(
        'id' => USER_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_NAME_FORMAT_ERROR',
        'title'=>'用户名格式不正确,应为2-20位字符,请重新输入',
        'detail'=>'用户名格式不正确,应为2-20位字符,请重新输入',
        'source'=>array(
            'pointer'=>'userName'
        ),
        'meta'=>array()
    ),
    SECURITY_ANSWER_INCORRECT => array(
        'id' => SECURITY_ANSWER_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'SECURITY_ANSWER_INCORRECT',
        'title'=>'密保答案不正确,请重新输入',
        'detail'=>'密保答案不正确,请重新输入',
        'source'=>array(
            'pointer'=>'userName'
        ),
        'meta'=>array()
    ),
    YEAR_FORMAT_ERROR => array(
        'id'=> YEAR_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'YEAR_FORMAT_ERROR',
        'title'=>'年份范围不正确,应为1901-2155年之间,请重新输入',
        'detail'=>'年份范围不正确,应为1901-2155年之间,请重新输入',
        'source'=>array(
            'pointer'=>'year'
        ),
        'meta'=>array()
    ),
    CREDIT_PHOTOGRAPHY_FORMAT_ERROR => array(
        'id'=> CREDIT_PHOTOGRAPHY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREDIT_PHOTOGRAPHY_FORMAT_ERROR',
        'title'=>'图片或视频不能为空，请继续完善信息',
        'detail'=>'图片或视频不能为空，请继续完善信息',
        'source'=>array(
            'pointer'=>''
        ),
        'meta'=>array()
    ),
    CREDIT_PHOTOGRAPHY_ATTACHMENT_FORMAT_ERROR => array(
        'id'=> CREDIT_PHOTOGRAPHY_ATTACHMENT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREDIT_PHOTOGRAPHY_ATTACHMENT_FORMAT_ERROR',
        'title'=>'图片或视频数量超过限制，应为9张图片或1个视频',
        'detail'=>'图片或视频数量超过限制，应为9张图片或1个视频',
        'source'=>array(
            'pointer'=>''
        ),
        'meta'=>array()
    ),
    APPLY_CREW_FORMAT_ERROR => array(
        'id' => APPLY_CREW_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'APPLY_CREW_FORMAT_ERROR',
        'title'=>'审核人格式不正确,请重新输入',
        'detail'=>'审核人格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'applyCrewId'
        ),
        'meta'=>array()
    ),
    CREW_ID_FORMAT_ERROR => array(
        'id' => CREW_ID_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_ID_FORMAT_ERROR',
        'title'=>'发布人格式不正确,请重新输入',
        'detail'=>'发布人格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'crewId'
        ),
        'meta'=>array()
    ),
    RULES_FORMAT_ERROR => array(
        'id' => RULES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULES_FORMAT_ERROR',
        'title'=>'规则格式不正确,请重新输入',
        'detail'=>'规则格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'rules'
        ),
        'meta'=>array()
    ),
    RULES_NAME_NOT_UNIQUE => array(
        'id' => RULES_NAME_NOT_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'RULES_NAME_NOT_UNIQUE',
        'title'=>'规则名称不存在,请核对后重新输入',
        'detail'=>'规则名称不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'rulesName'
        ),
        'meta'=>array()
    ),
    COMPLETION_RULES_COUNT_FORMAT_ERROR => array(
        'id' => COMPLETION_RULES_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPLETION_RULES_COUNT_FORMAT_ERROR',
        'title'=>'补全数量不正确,请重新输入',
        'detail'=>'补全数量不正确,请重新输入',
        'source'=>array(
            'pointer'=>'completionRulesCount'
        ),
        'meta'=>array()
    ),
    COMPLETION_RULES_ID_FORMAT_ERROR => array(
        'id' => COMPLETION_RULES_ID_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPLETION_RULES_ID_FORMAT_ERROR',
        'title'=>'补全资源目录id格式不正确,请重新输入',
        'detail'=>'补全资源目录id格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'completionRulesId'
        ),
        'meta'=>array()
    ),
    TYPE_FORMAT_ERROR => array(
        'id' => TYPE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TYPE_FORMAT_ERROR',
        'title'=>'分类格式不正确,请重新输入',
        'detail'=>'分类格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'type'
        ),
        'meta'=>array()
    ),
    TRANSFORMATION_TEMPLATE_FORMAT_ERROR => array(
        'id' => TRANSFORMATION_TEMPLATE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TRANSFORMATION_TEMPLATE_FORMAT_ERROR',
        'title'=>'目标资源目录格式不正确,请重新输入',
        'detail'=>'目标资源目录格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'transformationTemplates'
        ),
        'meta'=>array()
    ),
    SOURCE_TEMPLATE_FORMAT_ERROR => array(
        'id' => SOURCE_TEMPLATE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SOURCE_TEMPLATE_FORMAT_ERROR',
        'title'=>'来源资源目录格式不正确,请重新输入',
        'detail'=>'来源资源目录格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'sourceTemplates'
        ),
        'meta'=>array()
    ),
    REQUIRED_ITEMS_MAPPING_IS_EXIT => array(
        'id' => REQUIRED_ITEMS_MAPPING_IS_EXIT,
        'link'=>'',
        'status'=>403,
        'code'=>'REQUIRED_ITEMS_MAPPING_IS_EXIT',
        'title'=>'存在未转换和补全的必填项,请重新输入',
        'detail'=>'存在未转换和补全的必填项,请重新输入',
        'source'=>array(
            'pointer'=>'requiredItemsMapping'
        ),
        'meta'=>array()
    ),
    TRANSFORMATION_TEMPLATE_IS_EMPTY => array(
        'id' => TRANSFORMATION_TEMPLATE_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'TRANSFORMATION_TEMPLATE_IS_EMPTY',
        'title'=>'目标资源目录为空,请重新输入',
        'detail'=>'目标资源目录为空,请重新输入',
        'source'=>array(
            'pointer'=>'transformationTemplateId'
        ),
        'meta'=>array()
    ),
    SOURCE_TEMPLATE_IS_EMPTY => array(
        'id' => SOURCE_TEMPLATE_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'SOURCE_TEMPLATE_IS_EMPTY',
        'title'=>'来源资源目录为空,请重新输入',
        'detail'=>'来源资源目录为空,请重新输入',
        'source'=>array(
            'pointer'=>'sourceTemplateId'
        ),
        'meta'=>array()
    ),
    RULES_IS_UNIQUE => array(
        'id' => RULES_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'RULES_IS_UNIQUE',
        'title'=>'规则数据已经存在,请重新输入',
        'detail'=>'规则数据已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'rule'
        ),
        'meta'=>array()
    ),
    RULES_IS_OVER_STEP => array(
        'id' => RULES_IS_OVER_STEP,
        'link'=>'',
        'status'=>403,
        'code'=>'RULES_IS_OVER_STEP',
        'title'=>'规则数据超出范围,请重新输入',
        'detail'=>'规则数据超出范围,请重新输入',
        'source'=>array(
            'pointer'=>'rules'
        ),
        'meta'=>array()
    ),
    TRANSFORMATION_RULES_FORMAT_ERROR => array(
        'id' => TRANSFORMATION_RULES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TRANSFORMATION_RULES_FORMAT_ERROR',
        'title'=>'转换规则格式不正确,请重新输入',
        'detail'=>'转换规则格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'transformationRules'
        ),
        'meta'=>array()
    ),
    COMPLETION_RULES_BASE_FORMAT_ERROR => array(
        'id' => COMPLETION_RULES_BASE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPLETION_RULES_BASE_FORMAT_ERROR',
        'title'=>'补全依据不正确,请重新输入',
        'detail'=>'补全依据不正确,请重新输入',
        'source'=>array(
            'pointer'=>'completionRulesBase'
        ),
        'meta'=>array()
    ),
    COMPLETION_RULES_ITEM_FORMAT_ERROR => array(
        'id' => COMPLETION_RULES_ITEM_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPLETION_RULES_ITEM_FORMAT_ERROR',
        'title'=>'补全信息项不正确,请重新输入',
        'detail'=>'补全信息项不正确,请重新输入',
        'source'=>array(
            'pointer'=>'completionRulesItem'
        ),
        'meta'=>array()
    ),
    COMPLETION_RULES_FORMAT_ERROR => array(
        'id' => COMPLETION_RULES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPLETION_RULES_FORMAT_ERROR',
        'title'=>'补全规则格式不正确,请重新输入',
        'detail'=>'补全规则格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'completionRules'
        ),
        'meta'=>array()
    ),
    COMPARISON_RULES_COUNT_FORMAT_ERROR => array(
        'id' => COMPARISON_RULES_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPARISON_RULES_COUNT_FORMAT_ERROR',
        'title'=>'比对数量不正确,请重新输入',
        'detail'=>'比对数量不正确,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesCount'
        ),
        'meta'=>array()
    ),
    COMPARISON_RULES_ID_FORMAT_ERROR => array(
        'id' => COMPARISON_RULES_ID_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPARISON_RULES_ID_FORMAT_ERROR',
        'title'=>'比对资源目录id格式不正确,请重新输入',
        'detail'=>'比对资源目录id格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesId'
        ),
        'meta'=>array()
    ),
    COMPARISON_RULES_BASE_FORMAT_ERROR => array(
        'id' => COMPARISON_RULES_BASE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPARISON_RULES_BASE_FORMAT_ERROR',
        'title'=>'比对依据不正确,请重新输入',
        'detail'=>'比对依据不正确,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesBase'
        ),
        'meta'=>array()
    ),
    COMPARISON_RULES_ITEM_FORMAT_ERROR => array(
        'id' => COMPARISON_RULES_ITEM_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPARISON_RULES_ITEM_FORMAT_ERROR',
        'title'=>'比对信息项不正确,请重新输入',
        'detail'=>'比对信息项不正确,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesItem'
        ),
        'meta'=>array()
    ),
    COMPARISON_RULES_FORMAT_ERROR => array(
        'id' => COMPARISON_RULES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPARISON_RULES_FORMAT_ERROR',
        'title'=>'比对规则格式不正确,请重新输入',
        'detail'=>'比对规则格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRules'
        ),
        'meta'=>array()
    ),
    DE_DUPLICATION_RULES_FORMAT_ERROR => array(
        'id' => DE_DUPLICATION_RULES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'DE_DUPLICATION_RULES_FORMAT_ERROR',
        'title'=>'去重规则格式不正确,请重新输入',
        'detail'=>'去重规则格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'deDuplicationRules'
        ),
        'meta'=>array()
    ),
    DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR => array(
        'id' => DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR',
        'title'=>'去重信息项不正确,请重新输入',
        'detail'=>'去重信息项不正确,请重新输入',
        'source'=>array(
            'pointer'=>'deDuplicationRulesItems'
        ),
        'meta'=>array()
    ),
    DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR => array(
        'id' => DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR',
        'title'=>'去重结果不正确,请重新输入',
        'detail'=>'去重结果不正确,请重新输入',
        'source'=>array(
            'pointer'=>'deDuplicationRulesResult'
        ),
        'meta'=>array()
    ),
    TRANSFORMATION_RULES_TRANSFORMATION_NOT_EXIST => array(
        'id' => TRANSFORMATION_RULES_TRANSFORMATION_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'TRANSFORMATION_RULES_TRANSFORMATION_NOT_EXIST',
        'title'=>'转换资源目录信息项不存在,请核对后重新输入',
        'detail'=>'转换资源目录信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'transformationRulesTransformationNotExist'
        ),
        'meta'=>array()
    ),
    TRANSFORMATION_RULES_SOURCE_NOT_EXIST => array(
        'id' => TRANSFORMATION_RULES_SOURCE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'TRANSFORMATION_RULES_SOURCE_NOT_EXIST',
        'title'=>'来源资源目录信息项不存在,请核对后重新输入',
        'detail'=>'来源资源目录信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'transformationRulesSourceNotExist'
        ),
        'meta'=>array()
    ),
    TRANSFORMATION_ITEM_TYPE_NOT_EXIST => array(
        'id' => TRANSFORMATION_ITEM_TYPE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'TRANSFORMATION_ITEM_TYPE_NOT_EXIST',
        'title'=>'目标信息项类型不存在,请核对后重新输入',
        'detail'=>'目标信息项类型不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'transformationItemTypeNotExist'
        ),
        'meta'=>array()
    ),
    TYPE_CANNOT_TRANSFORMATION => array(
        'id' => TYPE_CANNOT_TRANSFORMATION,
        'link'=>'',
        'status'=>403,
        'code'=>'TYPE_CANNOT_TRANSFORMATION',
        'title'=>'类型不能转换,请重新输入',
        'detail'=>'类型不能转换,请重新输入',
        'source'=>array(
            'pointer'=>'typeCannotTransformation'
        ),
        'meta'=>array()
    ),
    LENGTH_CANNOT_TRANSFORMATION => array(
        'id' => LENGTH_CANNOT_TRANSFORMATION,
        'link'=>'',
        'status'=>403,
        'code'=>'LENGTH_CANNOT_TRANSFORMATION',
        'title'=>'长度不能转换,请重新输入',
        'detail'=>'长度不能转换,请重新输入',
        'source'=>array(
            'pointer'=>'lengthCannotTransformation'
        ),
        'meta'=>array()
    ),
    DIMENSION_CANNOT_TRANSFORMATION => array(
        'id' => DIMENSION_CANNOT_TRANSFORMATION,
        'link'=>'',
        'status'=>403,
        'code'=>'DIMENSION_CANNOT_TRANSFORMATION',
        'title'=>'公开范围不能转换,请重新输入',
        'detail'=>'公开范围不能转换,请重新输入',
        'source'=>array(
            'pointer'=>'dimensionCannotTransformation'
        ),
        'meta'=>array()
    ),
    IS_MASKED_CANNOT_TRANSFORMATION => array(
        'id' => IS_MASKED_CANNOT_TRANSFORMATION,
        'link'=>'',
        'status'=>403,
        'code'=>'IS_MASKED_CANNOT_TRANSFORMATION',
        'title'=>'脱敏状态不能转换,请重新输入',
        'detail'=>'脱敏状态不能转换,请重新输入',
        'source'=>array(
            'pointer'=>'isMaskedCannotTransformation'
        ),
        'meta'=>array()
    ),
    MASK_RULE_CANNOT_TRANSFORMATION => array(
        'id' => MASK_RULE_CANNOT_TRANSFORMATION,
        'link'=>'',
        'status'=>403,
        'code'=>'MASK_RULE_CANNOT_TRANSFORMATION',
        'title'=>'脱敏范围不能转换,请重新输入',
        'detail'=>'脱敏范围不能转换,请重新输入',
        'source'=>array(
            'pointer'=>'maskRuleCannotTransformation'
        ),
        'meta'=>array()
    ),
    ZRR_NOT_INCLUDE_IDENTIFY => array(
        'id' => ZRR_NOT_INCLUDE_IDENTIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'ZRR_NOT_INCLUDE_IDENTIFY',
        'title'=>'补全依据需要包含身份证号信息项,请重新输入',
        'detail'=>'补全依据需要包含身份证号信息项,请重新输入',
        'source'=>array(
            'pointer'=>'zrrNotIncludeIdentify'
        ),
        'meta'=>array()
    ),
    COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST => array(
        'id' => COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST',
        'title'=>'补全规则目标资源目录模板待补全信息项不存在,请核对后重新输入',
        'detail'=>'补全规则目标资源目录模板待补全信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'completionRulesTransformationItemNotExist'
        ),
        'meta'=>array()
    ),
    COMPLETION_RULES_COMPLETION_TEMPLATE_NOT_EXIST => array(
        'id' => COMPLETION_RULES_COMPLETION_TEMPLATE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPLETION_RULES_COMPLETION_TEMPLATE_NOT_EXIST',
        'title'=>'补全规则补全信息项来源不存在,请核对后重新输入',
        'detail'=>'补全规则补全信息项来源不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'transformationRulesSourceNotExist'
        ),
        'meta'=>array()
    ),
    COMPLETION_RULES_COMPLETION_TEMPLATE_ITEM_NOT_EXIST => array(
        'id' => COMPLETION_RULES_COMPLETION_TEMPLATE_ITEM_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPLETION_RULES_COMPLETION_TEMPLATE_ITEM_NOT_EXIST',
        'title'=>'补全规则补全信息项不存在,请核对后重新输入',
        'detail'=>'补全规则补全信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'completionRulesCompletionTemplateItemNotExist'
        ),
        'meta'=>array()
    ),
    COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST => array(
        'id' => COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST',
        'title'=>'比对规则目标资源目录模板待比对信息项不存在,请核对后重新输入',
        'detail'=>'比对规则目标资源目录模板待比对信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesTransformationItemNotExist'
        ),
        'meta'=>array()
    ),
    COMPARISON_RULES_COMPARISON_TEMPLATE_NOT_EXIST => array(
        'id' => COMPARISON_RULES_COMPARISON_TEMPLATE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPARISON_RULES_COMPARISON_TEMPLATE_NOT_EXIST',
        'title'=>'比对规则比对信息项来源不存在,请核对后重新输入',
        'detail'=>'比对规则比对信息项来源不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesComparisonTemplateNotExist'
        ),
        'meta'=>array()
    ),
    COMPARISON_RULES_BASE_CANNOT_NAME => array(
        'id' => COMPARISON_RULES_BASE_CANNOT_NAME,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPARISON_RULES_BASE_CANNOT_NAME',
        'title'=>'比对项为企业名称,比对依据不能选择企业名称信息项,请重新输入',
        'detail'=>'比对项为企业名称,比对依据不能选择企业名称信息项,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesBaseCannotName'
        ),
        'meta'=>array()
    ),
    COMPARISON_RULES_BASE_CANNOT_IDENTIFY => array(
        'id' => COMPARISON_RULES_BASE_CANNOT_IDENTIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPARISON_RULES_BASE_CANNOT_IDENTIFY',
        'title'=>'比对项为统一社会信用代码/身份证号,那比对依据不能选择统一社会信用代码/身份证号信息项,请重新输入',
        'detail'=>'比对项为统一社会信用代码/身份证号,那比对依据不能选择统一社会信用代码/身份证号信息项,请重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesBaseCannotIdentify'
        ),
        'meta'=>array()
    ),
    COMPARISON_RULES_COMPARISON_TEMPLATE_ITEM_NOT_EXIST => array(
        'id' => COMPARISON_RULES_COMPARISON_TEMPLATE_ITEM_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'COMPARISON_RULES_COMPARISON_TEMPLATE_ITEM_NOT_EXIST',
        'title'=>'比对规则比对信息项不存在,请核对后重新输入',
        'detail'=>'比对规则比对信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'comparisonRulesComparisonTemplateItemNotExist'
        ),
        'meta'=>array()
    ),
    DE_DUPLICATION_RULES_ITEMS_NOT_EXIT => array(
        'id' => DE_DUPLICATION_RULES_ITEMS_NOT_EXIT,
        'link'=>'',
        'status'=>403,
        'code'=>'DE_DUPLICATION_RULES_ITEMS_NOT_EXIT',
        'title'=>'去重规则去重信息项不存在,请核对后重新输入',
        'detail'=>'去重规则去重信息项不存在,请核对后重新输入',
        'source'=>array(
            'pointer'=>'completionRulesCompletionTemplateNotExist'
        ),
        'meta'=>array()
    ),
    RULES_CATEGORY_FORMAT_ERROR => array(
        'id' => RULES_CATEGORY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RULES_CATEGORY_FORMAT_ERROR',
        'title'=>'规则目录类型不正确,请核对后重新输入',
        'detail'=>'规则目录类型不正确,请核对后重新输入',
        'source'=>array(
            'pointer'=>'category'
        ),
        'meta'=>array()
    ),
    TRANSFORMATION_CATEGORY_FORMAT_ERROR => array(
        'id' => TRANSFORMATION_CATEGORY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'TRANSFORMATION_CATEGORY_FORMAT_ERROR',
        'title'=>'目标目录类型不正确,请核对后重新输入',
        'detail'=>'目标目录类型不正确,请核对后重新输入',
        'source'=>array(
            'pointer'=>'transformationCategory'
        ),
        'meta'=>array()
    ),
    SOURCE_CATEGORY_FORMAT_ERROR => array(
        'id' => SOURCE_CATEGORY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SOURCE_CATEGORY_FORMAT_ERROR',
        'title'=>'来源目录类型不正确,请核对后重新输入',
        'detail'=>'来源目录类型不正确,请核对后重新输入',
        'source'=>array(
            'pointer'=>'sourceCategory'
        ),
        'meta'=>array()
    ),
    FILE_FORMAT_ERROR => array(
        'id'=>FILE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_FORMAT_ERROR',
        'title'=>'文件格式不正确,应为xls, xlsx格式,请重新上传',
        'detail'=>'文件格式不正确,应为xls, xlsx格式,请重新上传',
        'source'=>array(
            'pointer'=>'attachment'
        ),
        'meta'=>array()
    ),
    FILE_SIZE_FORMAT_ERROR => array(
        'id'=>FILE_SIZE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_SIZE_FORMAT_ERROR',
        'title'=>'文件大小不正确,应为1～10MB,请重新上传',
        'detail'=>'文件大小不正确,应为1～10MB,请重新上传',
        'source'=>array(
            'pointer'=>'attachmentSize'
        ),
        'meta'=>array()
    ),
    FILE_IS_EMPTY => array(
        'id'=>FILE_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_IS_EMPTY',
        'title'=>'文件为空,请重新上传有效的数据文件',
        'detail'=>'文件为空,请重新上传有效的数据文件',
        'source'=>array(
            'pointer'=>'attachment'
        ),
        'meta'=>array()
    ),
    FILE_IS_UNIQUE => array(
        'id'=>FILE_IS_UNIQUE,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_IS_UNIQUE',
        'title'=>'文件已存在,请重新上传',
        'detail'=>'文件已存在,请重新上传',
        'source'=>array(
            'pointer'=>'attachment'
        ),
        'meta'=>array()
    ),
    FILE_IDENTIFY_FORMAT_ERROR => array(
        'id'=>FILE_IDENTIFY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_IDENTIFY_FORMAT_ERROR',
        'title'=>'文件名称的资源目录标识不正确,请重新上传',
        'detail'=>'文件名称的资源目录标识不正确,请重新上传',
        'source'=>array(
            'pointer'=>'attachmentIdentify'
        ),
        'meta'=>array()
    ),
    FILE_CATEGORY_FORMAT_ERROR => array(
        'id'=>FILE_CATEGORY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_CATEGORY_FORMAT_ERROR',
        'title'=>'文件名称的资源目录类型不正确,请重新上传',
        'detail'=>'文件名称的资源目录类型不正确,请重新上传',
        'source'=>array(
            'pointer'=>'attachmentCategory'
        ),
        'meta'=>array()
    ),
    CREW_IS_EMPTY => array(
        'id'=>CREW_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_IS_EMPTY',
        'title'=>'员工id不能为空',
        'detail'=>'员工id不能为空',
        'source'=>array(
            'pointer'=>'crewId'
        ),
        'meta'=>array()
    ),
    WEBSITE_CUSTOMIZE_NOT_UNIQUER => array(
        'id'=>WEBSITE_CUSTOMIZE_NOT_UNIQUER,
        'link'=>'',
        'status'=>403,
        'code'=>'WEBSITE_CUSTOMIZE_NOT_UNIQUER',
        'title'=>'网站定制数据不存在',
        'detail'=>'网站定制数据不存在',
        'source'=>array(
            'pointer'=>'websiteCustomizeId'
        ),
        'meta'=>array()
    ),
    CONTENT_FORMAT_ERROR => array(
        'id'=>CONTENT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CONTENT_FORMAT_ERROR',
        'title'=>'内容格式不正确,请重新输入',
        'detail'=>'内容格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'content'
        ),
        'meta'=>array()
    ),
    CONTENT_KEYS_FORMAT_ERROR => array(
        'id'=>CONTENT_KEYS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CONTENT_KEYS_FORMAT_ERROR',
        'title'=>'内容数组格式不正确',
        'detail'=>'内容数组格式不正确',
        'source'=>array(
            'pointer'=>'contentKeys'
        ),
        'meta'=>array()
    ),
    CONTENT_REQUIRED_KEYS_FORMAT_ERROR => array(
        'id'=>CONTENT_REQUIRED_KEYS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CONTENT_REQUIRED_KEYS_FORMAT_ERROR',
        'title'=>'内容必填项格式不正确',
        'detail'=>'内容必填项格式不正确',
        'source'=>array(
            'pointer'=>'contentRequiredKeys'
        ),
        'meta'=>array()
    ),
    MEMORIAL_STATUS_FORMAT_ERROR => array(
        'id'=>MEMORIAL_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'MEMORIAL_STATUS_FORMAT_ERROR',
        'title'=>'内容背景置灰状态格式不正确',
        'detail'=>'内容背景置灰状态格式不正确',
        'source'=>array(
            'pointer'=>'memorialStatus'
        ),
        'meta'=>array()
    ),
    THEME_FORMAT_ERROR => array(
        'id'=>THEME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'THEME_FORMAT_ERROR',
        'title'=>'页面主题标识格式不正确',
        'detail'=>'页面主题标识格式不正确',
        'source'=>array(
            'pointer'=>'theme'
        ),
        'meta'=>array()
    ),
    THEME_COLOR_FORMAT_ERROR => array(
        'id'=>THEME_COLOR_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'THEME_COLOR_FORMAT_ERROR',
        'title'=>'页面主题标识-主题色格式不正确',
        'detail'=>'页面主题标识-主题色格式不正确',
        'source'=>array(
            'pointer'=>'themeColor'
        ),
        'meta'=>array()
    ),
    THEME_IMAGE_FORMAT_ERROR => array(
        'id'=>THEME_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'THEME_IMAGE_FORMAT_ERROR',
        'title'=>'页面主题标识-背景图格式不正确',
        'detail'=>'页面主题标识-背景图格式不正确',
        'source'=>array(
            'pointer'=>'themeImage'
        ),
        'meta'=>array()
    ),
    HEADER_BAR_LEFT_FORMAT_ERROR => array(
        'id'=>HEADER_BAR_LEFT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_BAR_LEFT_FORMAT_ERROR',
        'title'=>'头部左侧位置标识页面主题标识格式不正确',
        'detail'=>'头部左侧位置标识页面主题标识格式不正确',
        'source'=>array(
            'pointer'=>'headerBarLeft'
        ),
        'meta'=>array()
    ),
    HEADER_BAR_LEFT_NAME_FORMAT_ERROR => array(
        'id'=>HEADER_BAR_LEFT_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_BAR_LEFT_NAME_FORMAT_ERROR',
        'title'=>'头部左侧位置标识页面主题标识名称格式不正确',
        'detail'=>'头部左侧位置标识页面主题标识名称格式不正确',
        'source'=>array(
            'pointer'=>'headerBarLeftName'
        ),
        'meta'=>array()
    ),
    HEADER_BAR_LEFT_STATUS_FORMAT_ERROR => array(
        'id'=>HEADER_BAR_LEFT_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_BAR_LEFT_STATUS_FORMAT_ERROR',
        'title'=>'头部左侧位置标识页面主题标识状态格式不正确',
        'detail'=>'头部左侧位置标识页面主题标识状态格式不正确',
        'source'=>array(
            'pointer'=>'headerBarLeftStatus'
        ),
        'meta'=>array()
    ),
    HEADER_BAR_LEFT_TYPE_FORMAT_ERROR => array(
        'id'=>HEADER_BAR_LEFT_TYPE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_BAR_LEFT_TYPE_FORMAT_ERROR',
        'title'=>'头部左侧位置标识页面主题标识类型格式不正确',
        'detail'=>'头部左侧位置标识页面主题标识类型格式不正确',
        'source'=>array(
            'pointer'=>'headerBarLeftType'
        ),
        'meta'=>array()
    ),
    HEADER_BAR_LEFT_URL_FORMAT_ERROR => array(
        'id'=>HEADER_BAR_LEFT_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_BAR_LEFT_URL_FORMAT_ERROR',
        'title'=>'头部左侧位置标识页面主题标识链接格式不正确',
        'detail'=>'头部左侧位置标识页面主题标识链接格式不正确',
        'source'=>array(
            'pointer'=>'headerBarLeftUrl'
        ),
        'meta'=>array()
    ),
    HEADER_BAR_RIGHT_FORMAT_ERROR => array(
        'id'=>HEADER_BAR_RIGHT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_BAR_RIGHT_FORMAT_ERROR',
        'title'=>'头部右侧位置标识页面主题标识格式不正确',
        'detail'=>'头部右侧位置标识页面主题标识格式不正确',
        'source'=>array(
            'pointer'=>'headerBarRight'
        ),
        'meta'=>array()
    ),
    HEADER_BAR_RIGHT_NAME_FORMAT_ERROR => array(
        'id'=>HEADER_BAR_RIGHT_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_BAR_RIGHT_NAME_FORMAT_ERROR',
        'title'=>'头部右侧位置标识页面主题标识名称格式不正确',
        'detail'=>'头部右侧位置标识页面主题标识名称格式不正确',
        'source'=>array(
            'pointer'=>'headerBarRightName'
        ),
        'meta'=>array()
    ),
    HEADER_BAR_RIGHT_STATUS_FORMAT_ERROR => array(
        'id'=>HEADER_BAR_RIGHT_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_BAR_RIGHT_STATUS_FORMAT_ERROR',
        'title'=>'头部右侧位置标识页面主题标识状态格式不正确',
        'detail'=>'头部右侧位置标识页面主题标识状态格式不正确',
        'source'=>array(
            'pointer'=>'headerBarRightStatus'
        ),
        'meta'=>array()
    ),
    HEADER_BAR_RIGHT_TYPE_FORMAT_ERROR => array(
        'id'=>HEADER_BAR_RIGHT_TYPE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_BAR_RIGHT_TYPE_FORMAT_ERROR',
        'title'=>'头部右侧位置标识页面主题标识类型格式不正确',
        'detail'=>'头部右侧位置标识页面主题标识类型格式不正确',
        'source'=>array(
            'pointer'=>'headerBarRightType'
        ),
        'meta'=>array()
    ),
    HEADER_BAR_RIGHT_URL_FORMAT_ERROR => array(
        'id'=>HEADER_BAR_RIGHT_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_BAR_RIGHT_URL_FORMAT_ERROR',
        'title'=>'头部右侧位置标识页面主题标识链接格式不正确',
        'detail'=>'头部右侧位置标识页面主题标识链接格式不正确',
        'source'=>array(
            'pointer'=>'headerBarRightUrl'
        ),
        'meta'=>array()
    ),
    HEADER_BG_FORMAT_ERROR => array(
        'id'=>HEADER_BG_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_BG_FORMAT_ERROR',
        'title'=>'网站header背景格式不正确',
        'detail'=>'网站header背景格式不正确',
        'source'=>array(
            'pointer'=>'headerBg'
        ),
        'meta'=>array()
    ),
    LOGO_FORMAT_ERROR => array(
        'id'=>LOGO_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'LOGO_FORMAT_ERROR',
        'title'=>'网站logo格式不正确',
        'detail'=>'网站logo格式不正确',
        'source'=>array(
            'pointer'=>'logo'
        ),
        'meta'=>array()
    ),
    HEADER_SEARCH_FORMAT_ERROR => array(
        'id'=>HEADER_SEARCH_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_SEARCH_FORMAT_ERROR',
        'title'=>'头部搜索栏位置标识格式不正确',
        'detail'=>'头部搜索栏位置标识格式不正确',
        'source'=>array(
            'pointer'=>'headerSearch'
        ),
        'meta'=>array()
    ),
    HEADER_SEARCH_STATUS_FORMAT_ERROR => array(
        'id'=>HEADER_SEARCH_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'HEADER_SEARCH_STATUS_FORMAT_ERROR',
        'title'=>'头部搜索栏位置标识状态格式不正确',
        'detail'=>'头部搜索栏位置标识状态格式不正确',
        'source'=>array(
            'pointer'=>'headerSearchStatus'
        ),
        'meta'=>array()
    ),
    NAV_FORMAT_ERROR => array(
        'id'=>NAV_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NAV_FORMAT_ERROR',
        'title'=>'头部导航位置标识格式不正确',
        'detail'=>'头部导航位置标识格式不正确',
        'source'=>array(
            'pointer'=>'nav'
        ),
        'meta'=>array()
    ),
    NAV_COUNT_FORMAT_ERROR => array(
        'id'=>NAV_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NAV_COUNT_FORMAT_ERROR',
        'title'=>'头部导航位置标识数量超过限制',
        'detail'=>'头部导航位置标识数量超过限制',
        'source'=>array(
            'pointer'=>'navCount'
        ),
        'meta'=>array()
    ),
    NAV_NAME_FORMAT_ERROR => array(
        'id'=>NAV_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NAV_NAME_FORMAT_ERROR',
        'title'=>'头部导航位置标识名称格式不正确',
        'detail'=>'头部导航位置标识名称格式不正确',
        'source'=>array(
            'pointer'=>'navName'
        ),
        'meta'=>array()
    ),
    NAV_STATUS_FORMAT_ERROR => array(
        'id'=>NAV_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NAV_STATUS_FORMAT_ERROR',
        'title'=>'头部导航位置标识状态格式不正确',
        'detail'=>'头部导航位置标识状态格式不正确',
        'source'=>array(
            'pointer'=>'navStatus'
        ),
        'meta'=>array()
    ),
    NAV_URL_FORMAT_ERROR => array(
        'id'=>NAV_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NAV_URL_FORMAT_ERROR',
        'title'=>'头部导航位置标识链接格式不正确',
        'detail'=>'头部导航位置标识链接格式不正确',
        'source'=>array(
            'pointer'=>'navUrl'
        ),
        'meta'=>array()
    ),
    NAV_IMAGE_FORMAT_ERROR => array(
        'id'=>NAV_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'NAV_IMAGE_FORMAT_ERROR',
        'title'=>'头部导航位置标识图片格式不正确',
        'detail'=>'头部导航位置标识图片格式不正确',
        'source'=>array(
            'pointer'=>'navImage'
        ),
        'meta'=>array()
    ),
    CENTER_DIALOG_FORMAT_ERROR => array(
        'id'=>CENTER_DIALOG_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CENTER_DIALOG_FORMAT_ERROR',
        'title'=>'中间弹框位置标识格式不正确',
        'detail'=>'中间弹框位置标识格式不正确',
        'source'=>array(
            'pointer'=>'centerDialog'
        ),
        'meta'=>array()
    ),
    CENTER_DIALOG_STATUS_FORMAT_ERROR => array(
        'id'=>CENTER_DIALOG_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CENTER_DIALOG_STATUS_FORMAT_ERROR',
        'title'=>'中间弹框位置标识状态格式不正确',
        'detail'=>'中间弹框位置标识状态格式不正确',
        'source'=>array(
            'pointer'=>'centerDialogStatus'
        ),
        'meta'=>array()
    ),
    CENTER_DIALOG_URL_FORMAT_ERROR => array(
        'id'=>CENTER_DIALOG_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CENTER_DIALOG_URL_FORMAT_ERROR',
        'title'=>'中间弹框位置标识链接格式不正确',
        'detail'=>'中间弹框位置标识链接格式不正确',
        'source'=>array(
            'pointer'=>'centerDialogUrl'
        ),
        'meta'=>array()
    ),
    CENTER_DIALOG_IMAGE_FORMAT_ERROR => array(
        'id'=>CENTER_DIALOG_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CENTER_DIALOG_IMAGE_FORMAT_ERROR',
        'title'=>'中间弹框位置标识图片格式不正确',
        'detail'=>'中间弹框位置标识图片格式不正确',
        'source'=>array(
            'pointer'=>'centerDialogImage'
        ),
        'meta'=>array()
    ),
    ANIMATE_WINDOW_FORMAT_ERROR => array(
        'id'=>ANIMATE_WINDOW_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ANIMATE_WINDOW_FORMAT_ERROR',
        'title'=>'飘窗位置标识格式不正确',
        'detail'=>'飘窗位置标识格式不正确',
        'source'=>array(
            'pointer'=>'animateWindow'
        ),
        'meta'=>array()
    ),
    ANIMATE_WINDOW_COUNT_FORMAT_ERROR => array(
        'id'=>ANIMATE_WINDOW_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ANIMATE_WINDOW_COUNT_FORMAT_ERROR',
        'title'=>'飘窗位置标识数量超出限制',
        'detail'=>'飘窗位置标识数量超出限制',
        'source'=>array(
            'pointer'=>'animateWindowCount'
        ),
        'meta'=>array()
    ),
    ANIMATE_WINDOW_STATUS_FORMAT_ERROR => array(
        'id'=>ANIMATE_WINDOW_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ANIMATE_WINDOW_STATUS_FORMAT_ERROR',
        'title'=>'飘窗位置标识状态格式不正确',
        'detail'=>'飘窗位置标识状态格式不正确',
        'source'=>array(
            'pointer'=>'animateWindowStatus'
        ),
        'meta'=>array()
    ),
    ANIMATE_WINDOW_IMAGE_FORMAT_ERROR => array(
        'id'=>ANIMATE_WINDOW_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ANIMATE_WINDOW_IMAGE_FORMAT_ERROR',
        'title'=>'飘窗位置标识图片格式不正确',
        'detail'=>'飘窗位置标识图片格式不正确',
        'source'=>array(
            'pointer'=>'animateWindowImage'
        ),
        'meta'=>array()
    ),
    ANIMATE_WINDOW_URL_FORMAT_ERROR => array(
        'id'=>ANIMATE_WINDOW_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ANIMATE_WINDOW_URL_FORMAT_ERROR',
        'title'=>'飘窗位置标识链接格式不正确',
        'detail'=>'飘窗位置标识链接格式不正确',
        'source'=>array(
            'pointer'=>'animateWindow
            Url'
        ),
        'meta'=>array()
    ),
    LEFT_FLOAT_CARD_FORMAT_ERROR => array(
        'id'=>LEFT_FLOAT_CARD_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'LEFT_FLOAT_CARD_FORMAT_ERROR',
        'title'=>'左侧浮动卡片位置标识格式不正确',
        'detail'=>'左侧浮动卡片位置标识格式不正确',
        'source'=>array(
            'pointer'=>'leftFloatCard'
        ),
        'meta'=>array()
    ),
    LEFT_FLOAT_CARD_COUNT_FORMAT_ERROR => array(
        'id'=>LEFT_FLOAT_CARD_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'LEFT_FLOAT_CARD_COUNT_FORMAT_ERROR',
        'title'=>'左侧浮动卡片位置标识数量超出限制',
        'detail'=>'左侧浮动卡片位置标识数量超出限制',
        'source'=>array(
            'pointer'=>'leftFloatCardCount'
        ),
        'meta'=>array()
    ),
    LEFT_FLOAT_CARD_STATUS_FORMAT_ERROR => array(
        'id'=>LEFT_FLOAT_CARD_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'LEFT_FLOAT_CARD_STATUS_FORMAT_ERROR',
        'title'=>'左侧浮动卡片位置标识状态格式不正确',
        'detail'=>'左侧浮动卡片位置标识状态格式不正确',
        'source'=>array(
            'pointer'=>'leftFloatCardStatus'
        ),
        'meta'=>array()
    ),
    LEFT_FLOAT_CARD_IMAGE_FORMAT_ERROR => array(
        'id'=>LEFT_FLOAT_CARD_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'LEFT_FLOAT_CARD_IMAGE_FORMAT_ERROR',
        'title'=>'左侧浮动卡片位置标识图片格式不正确',
        'detail'=>'左侧浮动卡片位置标识图片格式不正确',
        'source'=>array(
            'pointer'=>'leftFloatCardImage'
        ),
        'meta'=>array()
    ),
    LEFT_FLOAT_CARD_URL_FORMAT_ERROR => array(
        'id'=>LEFT_FLOAT_CARD_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'LEFT_FLOAT_CARD_URL_FORMAT_ERROR',
        'title'=>'左侧浮动卡片位置标识链接格式不正确',
        'detail'=>'左侧浮动卡片位置标识链接格式不正确',
        'source'=>array(
            'pointer'=>'leftFloatCardUrl'
        ),
        'meta'=>array()
    ),
    FRAME_WINDOW_FORMAT_ERROR => array(
        'id'=>FRAME_WINDOW_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FRAME_WINDOW_FORMAT_ERROR',
        'title'=>'外链窗口格式不正确',
        'detail'=>'外链窗口格式不正确',
        'source'=>array(
            'pointer'=>'frameWindow'
        ),
        'meta'=>array()
    ),
    FRAME_WINDOW_STATUS_FORMAT_ERROR => array(
        'id'=>FRAME_WINDOW_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FRAME_WINDOW_STATUS_FORMAT_ERROR',
        'title'=>'外链窗口状态格式不正确',
        'detail'=>'外链窗口状态格式不正确',
        'source'=>array(
            'pointer'=>'frameWindowStatus'
        ),
        'meta'=>array()
    ),
    FRAME_WINDOW_URL_FORMAT_ERROR => array(
        'id'=>FRAME_WINDOW_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FRAME_WINDOW_URL_FORMAT_ERROR',
        'title'=>'外链窗口链接格式不正确',
        'detail'=>'外链窗口链接格式不正确',
        'source'=>array(
            'pointer'=>'frameWindowUrl'
        ),
        'meta'=>array()
    ),
    RIGHT_TOOL_BAR_FORMAT_ERROR => array(
        'id'=>RIGHT_TOOL_BAR_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RIGHT_TOOL_BAR_FORMAT_ERROR',
        'title'=>'右侧工具栏格式不正确',
        'detail'=>'右侧工具栏格式不正确',
        'source'=>array(
            'pointer'=>'rightToolBar'
        ),
        'meta'=>array()
    ),
    RIGHT_TOOL_BAR_COUNT_FORMAT_ERROR => array(
        'id'=>RIGHT_TOOL_BAR_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RIGHT_TOOL_BAR_COUNT_FORMAT_ERROR',
        'title'=>'右侧工具栏数量超出限制',
        'detail'=>'右侧工具栏数量超出限制',
        'source'=>array(
            'pointer'=>'rightToolBarCount'
        ),
        'meta'=>array()
    ),
    RIGHT_TOOL_BAR_STATUS_FORMAT_ERROR => array(
        'id'=>RIGHT_TOOL_BAR_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RIGHT_TOOL_BAR_STATUS_FORMAT_ERROR',
        'title'=>'右侧工具栏状态格式不正确',
        'detail'=>'右侧工具栏状态格式不正确',
        'source'=>array(
            'pointer'=>'rightToolBarStatus'
        ),
        'meta'=>array()
    ),
    RIGHT_TOOL_BAR_NAME_FORMAT_ERROR => array(
        'id'=>RIGHT_TOOL_BAR_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RIGHT_TOOL_BAR_NAME_FORMAT_ERROR',
        'title'=>'右侧工具栏名称格式不正确',
        'detail'=>'右侧工具栏名称格式不正确',
        'source'=>array(
            'pointer'=>'rightToolBarName'
        ),
        'meta'=>array()
    ),
    RIGHT_TOOL_BAR_CATEGORY_FORMAT_ERROR => array(
        'id'=>RIGHT_TOOL_BAR_CATEGORY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RIGHT_TOOL_BAR_CATEGORY_FORMAT_ERROR',
        'title'=>'右侧工具栏分类格式不正确',
        'detail'=>'右侧工具栏分类格式不正确',
        'source'=>array(
            'pointer'=>'rightToolBarCategory'
        ),
        'meta'=>array()
    ),
    RIGHT_TOOL_BAR_IMAGES_FORMAT_ERROR => array(
        'id'=>RIGHT_TOOL_BAR_IMAGES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RIGHT_TOOL_BAR_IMAGES_FORMAT_ERROR',
        'title'=>'右侧工具栏图片格式不正确',
        'detail'=>'右侧工具栏图片格式不正确',
        'source'=>array(
            'pointer'=>'rightToolBarImages'
        ),
        'meta'=>array()
    ),
    RIGHT_TOOL_BAR_IMAGES_COUNT_FORMAT_ERROR => array(
        'id'=>RIGHT_TOOL_BAR_IMAGES_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RIGHT_TOOL_BAR_IMAGES_COUNT_FORMAT_ERROR',
        'title'=>'右侧工具栏图片数量超出限制',
        'detail'=>'右侧工具栏图片数量超出限制',
        'source'=>array(
            'pointer'=>'rightToolBarImagesCount'
        ),
        'meta'=>array()
    ),
    RIGHT_TOOL_BAR_IMAGES_TITLE_FORMAT_ERROR => array(
        'id'=>RIGHT_TOOL_BAR_IMAGES_TITLE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RIGHT_TOOL_BAR_IMAGES_TITLE_FORMAT_ERROR',
        'title'=>'右侧工具栏图片标题格式不正确',
        'detail'=>'右侧工具栏图片标题格式不正确',
        'source'=>array(
            'pointer'=>'rightToolBarImagesTitle'
        ),
        'meta'=>array()
    ),
    RIGHT_TOOL_BAR_IMAGES_IMAGE_FORMAT_ERROR => array(
        'id'=>RIGHT_TOOL_BAR_IMAGES_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RIGHT_TOOL_BAR_IMAGES_IMAGE_FORMAT_ERROR',
        'title'=>'右侧工具栏图片格式不正确',
        'detail'=>'右侧工具栏图片格式不正确',
        'source'=>array(
            'pointer'=>'rightToolBarImagesImage'
        ),
        'meta'=>array()
    ),
    RIGHT_TOOL_BAR_URL_FORMAT_ERROR => array(
        'id'=>RIGHT_TOOL_BAR_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RIGHT_TOOL_BAR_URL_FORMAT_ERROR',
        'title'=>'右侧工具栏链接格式不正确',
        'detail'=>'右侧工具栏链接格式不正确',
        'source'=>array(
            'pointer'=>'rightToolBarUrl'
        ),
        'meta'=>array()
    ),
    RIGHT_TOOL_BAR_DESCRIPTION_FORMAT_ERROR => array(
        'id'=>RIGHT_TOOL_BAR_DESCRIPTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RIGHT_TOOL_BAR_DESCRIPTION_FORMAT_ERROR',
        'title'=>'右侧工具栏描述格式不正确',
        'detail'=>'右侧工具栏描述格式不正确',
        'source'=>array(
            'pointer'=>'rightToolBarDescription'
        ),
        'meta'=>array()
    ),
    SPECIAL_COLUMN_FORMAT_ERROR => array(
        'id'=>SPECIAL_COLUMN_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SPECIAL_COLUMN_FORMAT_ERROR',
        'title'=>'专题专栏格式不正确',
        'detail'=>'专题专栏格式不正确',
        'source'=>array(
            'pointer'=>'specialColumn'
        ),
        'meta'=>array()
    ),
    SPECIAL_COLUMN_COUNT_FORMAT_ERROR => array(
        'id'=>SPECIAL_COLUMN_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SPECIAL_COLUMN_COUNT_FORMAT_ERROR',
        'title'=>'专题专栏数量超出限制',
        'detail'=>'专题专栏数量超出限制',
        'source'=>array(
            'pointer'=>'specialColumnCount'
        ),
        'meta'=>array()
    ),
    SPECIAL_COLUMN_STATUS_FORMAT_ERROR => array(
        'id'=>SPECIAL_COLUMN_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SPECIAL_COLUMN_STATUS_FORMAT_ERROR',
        'title'=>'专题专栏状态格式不正确',
        'detail'=>'专题专栏状态格式不正确',
        'source'=>array(
            'pointer'=>'specialColumnStatus'
        ),
        'meta'=>array()
    ),
    SPECIAL_COLUMN_IMAGE_FORMAT_ERROR => array(
        'id'=>SPECIAL_COLUMN_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SPECIAL_COLUMN_IMAGE_FORMAT_ERROR',
        'title'=>'专题专栏图片格式不正确',
        'detail'=>'专题专栏图片格式不正确',
        'source'=>array(
            'pointer'=>'specialColumnImage'
        ),
        'meta'=>array()
    ),
    SPECIAL_COLUMN_URL_FORMAT_ERROR => array(
        'id'=>SPECIAL_COLUMN_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SPECIAL_COLUMN_URL_FORMAT_ERROR',
        'title'=>'专题专栏链接格式不正确',
        'detail'=>'专题专栏链接格式不正确',
        'source'=>array(
            'pointer'=>'specialColumnUrl'
        ),
        'meta'=>array()
    ),
    RELATED_LINKS_FORMAT_ERROR => array(
        'id'=>RELATED_LINKS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RELATED_LINKS_FORMAT_ERROR',
        'title'=>'相关链接格式不正确',
        'detail'=>'相关链接格式不正确',
        'source'=>array(
            'pointer'=>'relatedLinks'
        ),
        'meta'=>array()
    ),
    RELATED_LINKS_COUNT_FORMAT_ERROR => array(
        'id'=>RELATED_LINKS_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RELATED_LINKS_COUNT_FORMAT_ERROR',
        'title'=>'相关链接数量超出限制',
        'detail'=>'相关链接数量超出限制',
        'source'=>array(
            'pointer'=>'relatedLinksCount'
        ),
        'meta'=>array()
    ),
    RELATED_LINKS_STATUS_FORMAT_ERROR => array(
        'id'=>RELATED_LINKS_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RELATED_LINKS_STATUS_FORMAT_ERROR',
        'title'=>'相关链接状态格式不正确',
        'detail'=>'相关链接状态格式不正确',
        'source'=>array(
            'pointer'=>'relatedLinksStatus'
        ),
        'meta'=>array()
    ),
    RELATED_LINKS_NAME_FORMAT_ERROR => array(
        'id'=>RELATED_LINKS_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RELATED_LINKS_NAME_FORMAT_ERROR',
        'title'=>'相关链接名称格式不正确',
        'detail'=>'相关链接名称格式不正确',
        'source'=>array(
            'pointer'=>'relatedLinksName'
        ),
        'meta'=>array()
    ),
    RELATED_LINKS_ITEMS_FORMAT_ERROR => array(
        'id'=>RELATED_LINKS_ITEMS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RELATED_LINKS_ITEMS_FORMAT_ERROR',
        'title'=>'相关链接items格式不正确',
        'detail'=>'相关链接items格式不正确',
        'source'=>array(
            'pointer'=>'relatedLinksItems'
        ),
        'meta'=>array()
    ),
    RELATED_LINKS_ITEMS_STATUS_FORMAT_ERROR => array(
        'id'=>RELATED_LINKS_ITEMS_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RELATED_LINKS_ITEMS_STATUS_FORMAT_ERROR',
        'title'=>'相关链接items状态格式不正确',
        'detail'=>'相关链接items状态格式不正确',
        'source'=>array(
            'pointer'=>'relatedLinksItemsStatus'
        ),
        'meta'=>array()
    ),
    RELATED_LINKS_ITEMS_NAME_FORMAT_ERROR => array(
        'id'=>RELATED_LINKS_ITEMS_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RELATED_LINKS_ITEMS_NAME_FORMAT_ERROR',
        'title'=>'相关链接items名称格式不正确',
        'detail'=>'相关链接items名称格式不正确',
        'source'=>array(
            'pointer'=>'relatedLinksItemsName'
        ),
        'meta'=>array()
    ),
    RELATED_LINKS_ITEMS_URL_FORMAT_ERROR => array(
        'id'=>RELATED_LINKS_ITEMS_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'RELATED_LINKS_ITEMS_URL_FORMAT_ERROR',
        'title'=>'相关链接items链接格式不正确',
        'detail'=>'相关链接items链接格式不正确',
        'source'=>array(
            'pointer'=>'relatedLinksItemsUrl'
        ),
        'meta'=>array()
    ),
    FOOTER_BANNER_FORMAT_ERROR => array(
        'id'=>FOOTER_BANNER_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_BANNER_FORMAT_ERROR',
        'title'=>'底部轮播格式不正确',
        'detail'=>'底部轮播格式不正确',
        'source'=>array(
            'pointer'=>'footerBanner'
        ),
        'meta'=>array()
    ),
    FOOTER_BANNER_COUNT_FORMAT_ERROR => array(
        'id'=>FOOTER_BANNER_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_BANNER_COUNT_FORMAT_ERROR',
        'title'=>'底部轮播数量超出限制',
        'detail'=>'底部轮播数量超出限制',
        'source'=>array(
            'pointer'=>'footerBannerCount'
        ),
        'meta'=>array()
    ),
    FOOTER_BANNER_STATUS_FORMAT_ERROR => array(
        'id'=>FOOTER_BANNER_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_BANNER_STATUS_FORMAT_ERROR',
        'title'=>'底部轮播状态格式不正确',
        'detail'=>'底部轮播状态格式不正确',
        'source'=>array(
            'pointer'=>'footerBannerStatus'
        ),
        'meta'=>array()
    ),
    FOOTER_BANNER_IMAGE_FORMAT_ERROR => array(
        'id'=>FOOTER_BANNER_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_BANNER_IMAGE_FORMAT_ERROR',
        'title'=>'底部轮播图片格式不正确',
        'detail'=>'底部轮播图片格式不正确',
        'source'=>array(
            'pointer'=>'footerBannerImage'
        ),
        'meta'=>array()
    ),
    FOOTER_BANNER_URL_FORMAT_ERROR => array(
        'id'=>FOOTER_BANNER_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_BANNER_URL_FORMAT_ERROR',
        'title'=>'底部轮播链接格式不正确',
        'detail'=>'底部轮播链接格式不正确',
        'source'=>array(
            'pointer'=>'footerBannerUrl'
        ),
        'meta'=>array()
    ),
    ORGANIZATION_GROUP_FORMAT_ERROR => array(
        'id'=>ORGANIZATION_GROUP_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ORGANIZATION_GROUP_FORMAT_ERROR',
        'title'=>'组织列表格式不正确',
        'detail'=>'组织列表格式不正确',
        'source'=>array(
            'pointer'=>'organizationGroup'
        ),
        'meta'=>array()
    ),
    ORGANIZATION_GROUP_COUNT_FORMAT_ERROR => array(
        'id'=>ORGANIZATION_GROUP_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ORGANIZATION_GROUP_COUNT_FORMAT_ERROR',
        'title'=>'组织列表数量超出限制',
        'detail'=>'组织列表数量超出限制',
        'source'=>array(
            'pointer'=>'organizationGroupCount'
        ),
        'meta'=>array()
    ),
    ORGANIZATION_GROUP_STATUS_FORMAT_ERROR => array(
        'id'=>ORGANIZATION_GROUP_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ORGANIZATION_GROUP_STATUS_FORMAT_ERROR',
        'title'=>'组织列表状态格式不正确',
        'detail'=>'组织列表状态格式不正确',
        'source'=>array(
            'pointer'=>'organizationGroupStatus'
        ),
        'meta'=>array()
    ),
    ORGANIZATION_GROUP_IMAGE_FORMAT_ERROR => array(
        'id'=>ORGANIZATION_GROUP_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ORGANIZATION_GROUP_IMAGE_FORMAT_ERROR',
        'title'=>'组织列表图片格式不正确',
        'detail'=>'组织列表图片格式不正确',
        'source'=>array(
            'pointer'=>'organizationGroupImage'
        ),
        'meta'=>array()
    ),
    ORGANIZATION_GROUP_URL_FORMAT_ERROR => array(
        'id'=>ORGANIZATION_GROUP_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ORGANIZATION_GROUP_URL_FORMAT_ERROR',
        'title'=>'组织列表链接格式不正确',
        'detail'=>'组织列表链接格式不正确',
        'source'=>array(
            'pointer'=>'organizationGroupUrl'
        ),
        'meta'=>array()
    ),
    SILHOUETTE_FORMAT_ERROR => array(
        'id'=>SILHOUETTE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SILHOUETTE_FORMAT_ERROR',
        'title'=>'剪影格式不正确',
        'detail'=>'剪影格式不正确',
        'source'=>array(
            'pointer'=>'silhouette'
        ),
        'meta'=>array()
    ),
    SILHOUETTE_STATUS_FORMAT_ERROR => array(
        'id'=>SILHOUETTE_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SILHOUETTE_STATUS_FORMAT_ERROR',
        'title'=>'剪影状态格式不正确',
        'detail'=>'剪影状态格式不正确',
        'source'=>array(
            'pointer'=>'silhouetteStatus'
        ),
        'meta'=>array()
    ),
    SILHOUETTE_IMAGE_FORMAT_ERROR => array(
        'id'=>SILHOUETTE_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SILHOUETTE_IMAGE_FORMAT_ERROR',
        'title'=>'剪影图片格式不正确',
        'detail'=>'剪影图片格式不正确',
        'source'=>array(
            'pointer'=>'silhouetteImage'
        ),
        'meta'=>array()
    ),
    SILHOUETTE_DESCRIPTION_FORMAT_ERROR => array(
        'id'=>SILHOUETTE_DESCRIPTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SILHOUETTE_DESCRIPTION_FORMAT_ERROR',
        'title'=>'剪影描述格式不正确',
        'detail'=>'剪影描述格式不正确',
        'source'=>array(
            'pointer'=>'silhouetteDescription'
        ),
        'meta'=>array()
    ),
    PARTY_AND_GOVERNMENT_ORGANS_FORMAT_ERROR => array(
        'id'=>PARTY_AND_GOVERNMENT_ORGANS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'PARTY_AND_GOVERNMENT_ORGANS_FORMAT_ERROR',
        'title'=>'党政机关格式不正确',
        'detail'=>'党政机关格式不正确',
        'source'=>array(
            'pointer'=>'partyAndGovernmentOrgans'
        ),
        'meta'=>array()
    ),
    PARTY_AND_GOVERNMENT_ORGANS_STATUS_FORMAT_ERROR => array(
        'id'=>PARTY_AND_GOVERNMENT_ORGANS_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'PARTY_AND_GOVERNMENT_ORGANS_STATUS_FORMAT_ERROR',
        'title'=>'党政机关状态格式不正确',
        'detail'=>'党政机关状态格式不正确',
        'source'=>array(
            'pointer'=>'partyAndGovernmentOrgansStatus'
        ),
        'meta'=>array()
    ),
    PARTY_AND_GOVERNMENT_ORGANS_IMAGE_FORMAT_ERROR => array(
        'id'=>PARTY_AND_GOVERNMENT_ORGANS_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'PARTY_AND_GOVERNMENT_ORGANS_IMAGE_FORMAT_ERROR',
        'title'=>'党政机关图片格式不正确',
        'detail'=>'党政机关图片格式不正确',
        'source'=>array(
            'pointer'=>'partyAndGovernmentOrgansImage'
        ),
        'meta'=>array()
    ),
    PARTY_AND_GOVERNMENT_ORGANS_URL_FORMAT_ERROR => array(
        'id'=>PARTY_AND_GOVERNMENT_ORGANS_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'PARTY_AND_GOVERNMENT_ORGANS_URL_FORMAT_ERROR',
        'title'=>'党政机关链接格式不正确',
        'detail'=>'党政机关链接格式不正确',
        'source'=>array(
            'pointer'=>'partyAndGovernmentOrgansUrl'
        ),
        'meta'=>array()
    ),
    GOVERNMENT_ERROR_CORRECTION_FORMAT_ERROR => array(
        'id'=>GOVERNMENT_ERROR_CORRECTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'GOVERNMENT_ERROR_CORRECTION_FORMAT_ERROR',
        'title'=>'政府纠错格式不正确',
        'detail'=>'政府纠错格式不正确',
        'source'=>array(
            'pointer'=>'governmentErrorCorrection'
        ),
        'meta'=>array()
    ),
    GOVERNMENT_ERROR_CORRECTION_STATUS_FORMAT_ERROR => array(
        'id'=>GOVERNMENT_ERROR_CORRECTION_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'GOVERNMENT_ERROR_CORRECTION_STATUS_FORMAT_ERROR',
        'title'=>'政府纠错状态格式不正确',
        'detail'=>'政府纠错状态格式不正确',
        'source'=>array(
            'pointer'=>'governmentErrorCorrectionStatus'
        ),
        'meta'=>array()
    ),
    GOVERNMENT_ERROR_CORRECTION_IMAGE_FORMAT_ERROR => array(
        'id'=>GOVERNMENT_ERROR_CORRECTION_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'GOVERNMENT_ERROR_CORRECTION_IMAGE_FORMAT_ERROR',
        'title'=>'政府纠错图片格式不正确',
        'detail'=>'政府纠错图片格式不正确',
        'source'=>array(
            'pointer'=>'governmentErrorCorrectionImage'
        ),
        'meta'=>array()
    ),
    GOVERNMENT_ERROR_CORRECTION_URL_FORMAT_ERROR => array(
        'id'=>GOVERNMENT_ERROR_CORRECTION_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'GOVERNMENT_ERROR_CORRECTION_URL_FORMAT_ERROR',
        'title'=>'政府纠错链接格式不正确',
        'detail'=>'政府纠错链接格式不正确',
        'source'=>array(
            'pointer'=>'governmentErrorCorrectionUrl'
        ),
        'meta'=>array()
    ),
    FOOTER_NAV_FORMAT_ERROR => array(
        'id'=>FOOTER_NAV_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_NAV_FORMAT_ERROR',
        'title'=>'页脚导航格式不正确',
        'detail'=>'页脚导航格式不正确',
        'source'=>array(
            'pointer'=>'footerNav'
        ),
        'meta'=>array()
    ),
    FOOTER_NAV_STATUS_FORMAT_ERROR => array(
        'id'=>FOOTER_NAV_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_NAV_STATUS_FORMAT_ERROR',
        'title'=>'页脚导航状态格式不正确',
        'detail'=>'页脚导航状态格式不正确',
        'source'=>array(
            'pointer'=>'footerNavStatus'
        ),
        'meta'=>array()
    ),
    FOOTER_NAV_NAME_FORMAT_ERROR => array(
        'id'=>FOOTER_NAV_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_NAV_NAME_FORMAT_ERROR',
        'title'=>'页脚导航名称格式不正确',
        'detail'=>'页脚导航名称格式不正确',
        'source'=>array(
            'pointer'=>'footerNavName'
        ),
        'meta'=>array()
    ),
    FOOTER_NAV_URL_FORMAT_ERROR => array(
        'id'=>FOOTER_NAV_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_NAV_URL_FORMAT_ERROR',
        'title'=>'页脚导航链接格式不正确',
        'detail'=>'页脚导航链接格式不正确',
        'source'=>array(
            'pointer'=>'footerNavUrl'
        ),
        'meta'=>array()
    ),
    FOOTER_TWO_FORMAT_ERROR => array(
        'id'=>FOOTER_TWO_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_TWO_FORMAT_ERROR',
        'title'=>'页脚第二行内容呈现格式不正确',
        'detail'=>'页脚第二行内容呈现格式不正确',
        'source'=>array(
            'pointer'=>'footerTwo'
        ),
        'meta'=>array()
    ),
    FOOTER_TWO_STATUS_FORMAT_ERROR => array(
        'id'=>FOOTER_TWO_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_TWO_STATUS_FORMAT_ERROR',
        'title'=>'页脚第二行内容呈现状态格式不正确',
        'detail'=>'页脚第二行内容呈现状态格式不正确',
        'source'=>array(
            'pointer'=>'footerTwoStatus'
        ),
        'meta'=>array()
    ),
    FOOTER_TWO_NAME_FORMAT_ERROR => array(
        'id'=>FOOTER_TWO_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_TWO_NAME_FORMAT_ERROR',
        'title'=>'页脚第二行内容呈现名称格式不正确',
        'detail'=>'页脚第二行内容呈现名称格式不正确',
        'source'=>array(
            'pointer'=>'footerTwoName'
        ),
        'meta'=>array()
    ),
    FOOTER_TWO_URL_FORMAT_ERROR => array(
        'id'=>FOOTER_TWO_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_TWO_URL_FORMAT_ERROR',
        'title'=>'页脚第二行内容呈现链接格式不正确',
        'detail'=>'页脚第二行内容呈现链接格式不正确',
        'source'=>array(
            'pointer'=>'footerTwoUrl'
        ),
        'meta'=>array()
    ),
    FOOTER_TWO_DESCRIPTION_FORMAT_ERROR => array(
        'id'=>FOOTER_TWO_DESCRIPTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_TWO_DESCRIPTION_FORMAT_ERROR',
        'title'=>'页脚第二行内容呈现描述格式不正确',
        'detail'=>'页脚第二行内容呈现描述格式不正确',
        'source'=>array(
            'pointer'=>'footerTwoDescription'
        ),
        'meta'=>array()
    ),
    FOOTER_THREE_FORMAT_ERROR => array(
        'id'=>FOOTER_THREE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_THREE_FORMAT_ERROR',
        'title'=>'页脚第三行内容呈现格式不正确',
        'detail'=>'页脚第三行内容呈现格式不正确',
        'source'=>array(
            'pointer'=>'footerThree'
        ),
        'meta'=>array()
    ),
    FOOTER_THREE_STATUS_FORMAT_ERROR => array(
        'id'=>FOOTER_THREE_STATUS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_THREE_STATUS_FORMAT_ERROR',
        'title'=>'页脚第三行内容呈现状态格式不正确',
        'detail'=>'页脚第三行内容呈现状态格式不正确',
        'source'=>array(
            'pointer'=>'footerThreeStatus'
        ),
        'meta'=>array()
    ),
    FOOTER_THREE_NAME_FORMAT_ERROR => array(
        'id'=>FOOTER_THREE_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_THREE_NAME_FORMAT_ERROR',
        'title'=>'页脚第三行内容呈现名称格式不正确',
        'detail'=>'页脚第三行内容呈现名称格式不正确',
        'source'=>array(
            'pointer'=>'footerThreeName'
        ),
        'meta'=>array()
    ),
    FOOTER_THREE_URL_FORMAT_ERROR => array(
        'id'=>FOOTER_THREE_URL_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_THREE_URL_FORMAT_ERROR',
        'title'=>'页脚第三行内容呈现链接格式不正确',
        'detail'=>'页脚第三行内容呈现链接格式不正确',
        'source'=>array(
            'pointer'=>'footerThreeUrl'
        ),
        'meta'=>array()
    ),
    FOOTER_THREE_DESCRIPTION_FORMAT_ERROR => array(
        'id'=>FOOTER_THREE_DESCRIPTION_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_THREE_DESCRIPTION_FORMAT_ERROR',
        'title'=>'页脚第三行内容呈现描述格式不正确',
        'detail'=>'页脚第三行内容呈现描述格式不正确',
        'source'=>array(
            'pointer'=>'footerThreeDescription'
        ),
        'meta'=>array()
    ),
    FOOTER_THREE_TYPE_FORMAT_ERROR => array(
        'id'=>FOOTER_THREE_TYPE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_THREE_TYPE_FORMAT_ERROR',
        'title'=>'页脚第三行内容呈现类型格式不正确',
        'detail'=>'页脚第三行内容呈现类型格式不正确',
        'source'=>array(
            'pointer'=>'footerThreeType'
        ),
        'meta'=>array()
    ),
    FOOTER_THREE_IMAGE_FORMAT_ERROR => array(
        'id'=>FOOTER_THREE_IMAGE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'FOOTER_THREE_IMAGE_FORMAT_ERROR',
        'title'=>'页脚第三行内容呈现图片格式不正确',
        'detail'=>'页脚第三行内容呈现图片格式不正确',
        'source'=>array(
            'pointer'=>'footerThreeImage'
        ),
        'meta'=>array()
    ),
    CATEGORY_FORMAT_ERROR => array(
        'id'=>CATEGORY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CATEGORY_FORMAT_ERROR',
        'title'=>'网站定制分类格式不正确',
        'detail'=>'网站定制分类格式不正确',
        'source'=>array(
            'pointer'=>'category'
        ),
        'meta'=>array()
    ),
    CATEGORY_NOT_EXIST => array(
        'id'=>CATEGORY_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'CATEGORY_NOT_EXIST',
        'title'=>'资源目录类型不存在,请重新输入',
        'detail'=>'资源目录类型不存在,请重新输入',
        'source'=>array(
            'pointer'=>'category'
        ),
        'meta'=>array()
    ),
    INFO_CATEGORY_NOT_EXIST => array(
        'id'=>INFO_CATEGORY_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'INFO_CATEGORY_NOT_EXIST',
        'title'=>'资源目录信息类别不存在,请重新输入',
        'detail'=>'资源目录信息类别不存在,请重新输入',
        'source'=>array(
            'pointer'=>'infoCategory'
        ),
        'meta'=>array()
    ),


    QZJ_TEMPLATE_EXIST => array(
        'id'=>QZJ_TEMPLATE_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'QZJ_TEMPLATE_EXIST',
        'title'=>'前置机资源目录已经存在,请重新输入',
        'detail'=>'前置机资源目录已经存在,请重新输入',
        'source'=>array(
            'pointer'=>'qzjTemplate'
        ),
        'meta'=>array()
    ),
    INFO_CLASSIFY_FORMAT_ERROR => array(
        'id'=>INFO_CLASSIFY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'INFO_CLASSIFY_FORMAT_ERROR',
        'title'=>'信息分类格式不正确,请重新输入',
        'detail'=>'信息分类格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'infoClassify'
        ),
        'meta'=>array()
    ),
    EXCHANGE_FREQUENCY_FORMAT_ERROR => array(
        'id'=>EXCHANGE_FREQUENCY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'EXCHANGE_FREQUENCY_FORMAT_ERROR',
        'title'=>'更新频率格式不正确,请重新输入',
        'detail'=>'更新频率格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'exchangeFrequency'
        ),
        'meta'=>array()
    ),
    SOURCE_UNIT_FORMAT_ERROR => array(
        'id'=>SOURCE_UNIT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SOURCE_UNIT_FORMAT_ERROR',
        'title'=>'来源委办局格式不正确,请重新输入',
        'detail'=>'来源委办局格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'sourceUnit'
        ),
        'meta'=>array()
    ),
    ITEM_LENGTH_FORMAT_ERROR => array(
        'id'=>ITEM_LENGTH_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ITEM_LENGTH_FORMAT_ERROR',
        'title'=>'信息项数据长度格式不正确,请重新输入',
        'detail'=>'信息项数据长度格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'length'
        ),
        'meta'=>array()
    ),
    ITEM_OPTIONS_FORMAT_ERROR => array(
        'id'=>ITEM_OPTIONS_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ITEM_OPTIONS_FORMAT_ERROR',
        'title'=>'信息项可选范围格式不正确,请重新输入',
        'detail'=>'信息项可选范围格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'options'
        ),
        'meta'=>array()
    ),
    APPLY_STATUE_CAN_NOT_MODIFY => array(
        'id'=>APPLY_STATUE_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'APPLY_STATUE_CAN_NOT_MODIFY',
        'title'=>'审核状态不能操作',
        'detail'=>'审核状态不能操作',
        'source'=>array(
            'pointer'=>'applyStatus'
        ),
        'meta'=>array()
    ),
    MEMBER_ID_FORMAT_ERROR => array(
        'id'=>MEMBER_ID_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'MEMBER_ID_FORMAT_ERROR',
        'title'=>'前台用户格式不正确',
        'detail'=>'前台用户格式不正确',
        'source'=>array(
            'pointer'=>'memberId'
        ),
        'meta'=>array()
    ),
    ACCEPT_USER_GROUP_ID_FORMAT_ERROR => array(
        'id'=>ACCEPT_USER_GROUP_ID_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'ACCEPT_USER_GROUP_ID_FORMAT_ERROR',
        'title'=>'受理委办局格式不正确,请重新输入',
        'detail'=>'受理委办局格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'acceptUserGroupId'
        ),
        'meta'=>array()
    ),
    IMAGES_COUNT_FORMAT_ERROR => array(
        'id'=>IMAGES_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'IMAGES_COUNT_FORMAT_ERROR',
        'title'=>'图片数量超出限制,最多应为9张,请重新上传',
        'detail'=>'图片数量超出限制,最多应为9张,请重新上传',
        'source'=>array(
            'pointer'=>'imagesCount'
        ),
        'meta'=>array()
    ),
    SUBJECT_FORMAT_ERROR => array(
        'id'=>SUBJECT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'SUBJECT_FORMAT_ERROR',
        'title'=>'被表扬主体格式不正确',
        'detail'=>'被表扬主体格式不正确',
        'source'=>array(
            'pointer'=>'subject'
        ),
        'meta'=>array()
    ),
    CONTACT_FORMAT_ERROR => array(
        'id'=>CONTACT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CONTACT_FORMAT_ERROR',
        'title'=>'联系方式格式不正确,应为11位数字,请重新输入',
        'detail'=>'联系方式格式不正确,应为11位数字,请重新输入',
        'source'=>array(
            'pointer'=>'contact'
        ),
        'meta'=>array()
    ),
    REPLY_CONTENT_FORMAT_ERROR => array(
        'id'=>REPLY_CONTENT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'REPLY_CONTENT_FORMAT_ERROR',
        'title'=>'回复内容格式不正确,应为1-2000位字符串,请重新输入',
        'detail'=>'回复内容格式不正确,应为1-2000位字符串,请重新输入',
        'source'=>array(
            'pointer'=>'replyContent'
        ),
        'meta'=>array()
    ),
    REPLY_IMAGES_FORMAT_ERROR => array(
        'id'=>REPLY_IMAGES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'REPLY_IMAGES_FORMAT_ERROR',
        'title'=>'回复图片格式不正确,应为jpg,jpeg,png格式,请重新上传',
        'detail'=>'回复图片格式不正确,应为jpg,jpeg,png格式,请重新上传',
        'source'=>array(
            'pointer'=>'replyImages'
        ),
        'meta'=>array()
    ),
    REPLY_IMAGES_COUNT_FORMAT_ERROR => array(
        'id'=>REPLY_IMAGES_COUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'REPLY_IMAGES_COUNT_FORMAT_ERROR',
        'title'=>'回复图片数量超出限制,最多应为9张,请重新上传',
        'detail'=>'回复图片数量超出限制,最多应为9张,请重新上传',
        'source'=>array(
            'pointer'=>'replyImagesCount'
        ),
        'meta'=>array()
    ),
    REPLY_ADMISSIBILITY_FORMAT_ERROR => array(
        'id'=>REPLY_ADMISSIBILITY_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'REPLY_ADMISSIBILITY_FORMAT_ERROR',
        'title'=>'受理情况格式不正确',
        'detail'=>'受理情况格式不正确',
        'source'=>array(
            'pointer'=>'admissibility'
        ),
        'meta'=>array()
    ),
    REPLY_CREW_FORMAT_ERROR => array(
        'id'=>REPLY_CREW_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'REPLY_CREW_FORMAT_ERROR',
        'title'=>'受理人格式不正确',
        'detail'=>'受理人格式不正确',
        'source'=>array(
            'pointer'=>'replyCrew'
        ),
        'meta'=>array()
    ),
    MEMBER_ID_IS_EMPTY => array(
        'id'=>MEMBER_ID_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'MEMBER_ID_IS_EMPTY',
        'title'=>'前台用户不能为空',
        'detail'=>'前台用户不能为空',
        'source'=>array(
            'pointer'=>'memberId'
        ),
        'meta'=>array()
    ),
    ACCEPT_USER_GROUP_ID_IS_EMPTY => array(
        'id'=>ACCEPT_USER_GROUP_ID_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'ACCEPT_USER_GROUP_ID_IS_EMPTY',
        'title'=>'受理委办局不能为空',
        'detail'=>'受理委办局不能为空',
        'source'=>array(
            'pointer'=>'acceptUserGroupId'
        ),
        'meta'=>array()
    ),
    CREW_ID_IS_EMPTY => array(
        'id'=>CREW_ID_IS_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'CREW_ID_IS_EMPTY',
        'title'=>'受理人不能为空',
        'detail'=>'受理人不能为空',
        'source'=>array(
            'pointer'=>'crewId'
        ),
        'meta'=>array()
    ),
    CERTIFICATES_FORMAT_ERROR => array(
        'id'=>CERTIFICATES_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'CERTIFICATES_FORMAT_ERROR',
        'title'=>'上传身份证/营业执照格式不正确,应为jpg,jpeg,png格式,请重新上传',
        'detail'=>'上传身份证/营业执照格式不正确,应为jpg,jpeg,png格式,请重新上传',
        'source'=>array(
            'pointer'=>'certificates'
        ),
        'meta'=>array()
    ),
    INTERACTION_TYPE_FORMAT_ERROR => array(
        'id'=>INTERACTION_TYPE_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'INTERACTION_TYPE_FORMAT_ERROR',
        'title'=>'反馈类型格式不正确,请重新输入',
        'detail'=>'反馈类型格式不正确,请重新输入',
        'source'=>array(
            'pointer'=>'type'
        ),
        'meta'=>array()
    ),
);
