# 资源目录管理错误提示规范（10201-10300）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-10201: 名称格式不正确
* ER-10202: 标识格式不正确
* ER-10203: 主体类别格式不正确
* ER-10204: 公开范围格式不正确
* ER-10205: 数据长度格式不正确
* ER-10206: 模板信息格式不正确
* ER-10207: 信息项名称格式不正确
* ER-10208: 是否必填格式不正确
* ER-10209: 是否脱敏格式不正确
* ER-10210: 脱敏规则格式不正确
* ER-10211: 备注格式不正确
* ER-10212: 数据类型格式不正确
* ER-10213: 目录标识已存在
* ER-10214: 统一社会信用代码格式不正确
* ER-10215: 信用主体名称格式不正确
* ER-10216 - ER-10300: 资源目录管理错误预留

---

### <a name="ER-10201">ER-10201</a>

**id**

`10201`

**code**

`TEMPLATE_NAME_FORMAT_ERROR`

**title**

目录名称格式不正确.

**detail**

目录名称格式不正确.

**links**

待补充

### <a name="ER-10202">ER-10202</a>

**id**

`10202`

**code**

`IDENTIFY_FORMAT_ERROR`

**title**

目录标识格式不正确.

**detail**

目录标识格式不正确.

**links**

待补充

### <a name="ER-10203">ER-10203</a>

**id**

`10203`

**code**

`SUBJECT_CATEGORY_FORMAT_ERROR`

**title**

主体类别格式不正确.

**detail**

主体类别格式不正确.

**links**

待补充

### <a name="ER-10204">ER-10204</a>

**id**

`10204`

**code**

`DIMENSION_FORMAT_ERROR`

**title**

公开范围格式不正确.

**detail**

公开范围格式不正确.

**links**

待补充

### <a name="ER-10205">ER-10205</a>

**id**

`10205`

**code**

`LENGTH_FORMAT_ERROR`

**title**

数据长度格式不正确.

**detail**

数据长度格式不正确.

**links**

待补充

### <a name="ER-10206">ER-10206</a>

**id**

`10206`

**code**

`ITEMS_FORMAT_ERROR`

**title**

模板信息格式不正确.

**detail**

模板信息格式不正确.

**links**

待补充

### <a name="ER-10207">ER-10207</a>

**id**

`10207`

**code**

`OPTIONS_FORMAT_ERROR`

**title**

信息项名称格式不正确.

**detail**

信息项名称格式不正确.

**links**

待补充

### <a name="ER-10208">ER-10208</a>

**id**

`10208`

**code**

`IS_NECESSARY_FORMAT_ERROR`

**title**

是否必填格式不正确.

**detail**

是否必填格式不正确.

**links**

待补充

### <a name="ER-10209">ER-10209</a>

**id**

`10209`

**code**

`IS_MASKED_FORMAT_ERROR`

**title**

是否脱敏格式不正确.

**detail**

是否脱敏格式不正确.

**links**

待补充

### <a name="ER-10210">ER-10210</a>

**id**

`10210`

**code**

`MASK_RULE_FORMAT_ERROR`

**title**

脱敏规则格式不正确.

**detail**

脱敏规则格式不正确.

**links**

待补充

### <a name="ER-10211">ER-10211</a>

**id**

`10211`

**code**

`REMARKS_FORMAT_ERROR`

**title**

备注格式不正确.

**detail**

备注格式不正确.

**links**

待补充

### <a name="ER-10212">ER-10212</a>

**id**

`10212`

**code**

`ITEM_TYPE_FORMAT_ERROR`

**title**

数据类型格式不正确.

**detail**

数据类型格式不正确.

**links**

待补充

### <a name="ER-10213">ER-10213</a>

**id**

`10213`

**code**

`IDENTIFY_FORMAT_EXIST`

**title**

目录标识已存在.

**detail**

目录标识已存在.

**links**

待补充

### <a name="ER-10214">ER-10214</a>

**id**

`10214`

**code**

`TYSHXYDM_FORMAT_ERROR`

**title**

统一社会信用代码格式不正确.

**detail**

统一社会信用代码格式不正确.

**links**

待补充

### <a name="ER-10215">ER-10215</a>

**id**

`10215`

**code**

`ZTMC_FORMAT_ERROR`

**title**

信用主体名称格式不正确.

**detail**

信用主体名称格式不正确.

**links**

待补充

### ER-10216 - ER-10300 错误预留