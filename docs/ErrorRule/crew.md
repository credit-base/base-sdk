# 员工管理错误提示规范（10101-10200）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-10101: 员工类型不存在
* ER-10102: 员工权限范围格式不正确
* ER-10103: 员工类型权限不足
* ER-10104 - ER-10200: 员工管理错误预留

---

### <a name="ER-10101">ER-10101</a>

**id**

`10101`

**code**

`CREW_CATEGORY_NOT_EXIST`

**title**

员工类型不存在.

**detail**

员工类型不存在.

**links**

待补充

### <a name="ER-10102">ER-10102</a>

**id**

`10102`

**code**

`CREW_PURVIEW_FORMAT_ERROR`

**title**

员工权限范围格式不正确.

**detail**

员工权限范围格式不正确.

**links**

待补充

### <a name="ER-10103">ER-10103</a>

**id**

`10103`

**code**

`CREW_PURVIEW_HIERARCHY_FORMAT_ERROR`

**title**

员工类型权限不足.

**detail**

员工类型权限不足.

**links**

待补充

### ER-10104 - ER-10200 错误预留