# 错误提示规范

* 1-99 系统错误规范
* 100-200 通用错误规范
* 201-300 OA通用错误规范
* 301-400 门户通用错误规范
* 401-500 共享通用错误规范
* 501-600 用户通用错误规范
* 601-10000 预留错误规范
* 10001-10100 科室错误规范
* 10101-10200 员工错误规范
* 10201-10300 资源目录错误规范
* 10301-10400 工单任务错误规范
* 10401-10500 新闻管理错误规范
* 10501-10600 前台用户管理错误规范
* 10601-10700 信用刊物管理错误规范

---

# 通用错误提示规范（100-1000）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* `ER-0000`: 未定义错误
* `ER-0001`: 服务器运行错误
* `ER-0002`: 路由不存在
* `ER-0003`: 路由不支持该方法
* `ER-0004`: 不支持的媒体协议
* `ER-0005`: 无法使用请求内容来响应
* `ER-0006`: 请求数据格式错误
* `ER-0007` - `ER-0009`: 系统错误预留
* `ER-0010`: 资源不存在
* `ER-0011`: 命令处理器不存在
* `ER-0012`: 翻译器不存在
* `ER-0013`: 观察者不存在
* `ER-0014`: subject不存在
* `ER-0015`: csrf验证失效
* `ER-0016`: 滑动验证失败
* `ER-0017`: 用户未登录
* `ER-0018`: 短信发送太频繁
* `ER-0019`: hash失效
* `ER-0020`: 用户登录失效
* `ER-0021`: 权限未定义
* `ER-0022`: 验证码不正确
* `ER-0022` - `ER-0099`: 系统错误预留
* `ER-0100`: 数据重复
* `ER-0101`: 数据格式不正确
* `ER-0102`: 数据不能为空
* `ER-0103`: 状态不能操作
* `ER-0104` - `ER-0120`: 通用错误预留
* `ER-0121`: 图片格式不正确 
* `ER-0122`: 附件格式不正确 
* `ER-0123`: 姓名格式不正确 
* `ER-0124`: 手机号格式不正确
* `ER-0125`: 身份证格式不正确
* `ER-0126`: 名称格式不正确
* `ER-0127`: 标题格式不正确 
* `ER-0128`: 描述格式不正确 
* `ER-0129`: 原因格式不正确
* `ER-0130` - `ER-0200`: 通用错误预留
* `ER-201`: jwt-token为空
* `ER-202`: jwt-token已过期
* `ER-203`: jwt-token验证失败
* `ER-0204` - `ER-0300`: 通用错误预留

---

### <a name="ER-0000">ER-0000</a>

**id**

`0`

**code**

`ERROR_NOT_DEFINED`

**title**

error not defined. 未定义错误.

**detail**

error not defined. 未定义错误.

**links**

待补充

### <a name="ER-0001">ER-0001</a>

**id**

`1`

**code**

`INTERNAL_SERVER_ERROR`

**title**

internal server error. 服务器运行错误.

**detail**

internal server error. 服务器运行错误.

**links**

待补充

### <a name="ER-0002">ER-0002</a>

**id**

`2`

**code**

`ROUTE_NOT_EXIST`

**title**

route not exist. 路由不存在.

**detail**

route not exist. 路由不存在.

**links**

待补充

### <a name="ER-0003">ER-0003</a>

**id**

`3`

**code**

`METHOD_NOT_ALLOWED`

**title**

method not allowd. 路由不支持方法.

**detail**

method not allowd. 路由不支持方法.

**links**

待补充

### <a name="ER-0004">ER-0004</a>

**id**

`4`

**code**

`UNSUPPORTED_MEDIA_TYPE`

**title**

unsupported media type. 不支持的媒体协议.

**detail**

unsupported media type. 不支持的媒体协议.

**links**

待补充

### <a name="ER-0005">ER-0005</a>

**id**

`5`

**code**

`NOT_ACCEPTABLE_MEDIA_TYPE`

**title**

not acceptable media type. 无法使用请求内容来响应.

**detail**

not acceptable media type. 无法使用请求内容来响应.

**links**

待补充

### <a name="ER-0006">ER-0006</a>

**id**

`6`

**code**

`INCORRECT_RAW_BODY`

**title**

incorrent request data format. 请求数据格式错误.

**detail**

incorrent request data format. 请求数据格式错误.

**links**

待补充

### ER-0007 - ER-0009

系统错误预留字段.

### <a name="ER-0010">ER-0010</a>

**id**

`10`

**code**

`RESOURCE_NOT_EXIST`

**title**

resource not exist. 资源不存在.

**detail**

server can not find resource. 服务器找不见资源.

**links**

待补充

### <a name="ER-0011">ER-0011</a>

**id**

`11`

**code**

`COMMAND_HANDLER_NOT_EXIST`

**title**

command handler not exist. 命令处理器不存在.

**detail**

command handler not exist. 命令处理器不存在.

**links**

待补充

### <a name="ER-0012">ER-0012</a>

**id**

`12`

**code**

`TRANSLATOR_NOT_EXIST`

**title**

Translator not exist. 翻译器不存在.

**detail**

Translator not exist. 翻译器不存在.

**links**

待补充

### <a name="ER-0013">ER-0013</a>

**id**

`13`

**code**

`OBSERVER_NOT_EXIST`

**title**

Observer not exist. 观察者不存在.

**detail**

Observer not exist. 观察者不存在.

**links**

待补充

### <a name="ER-0014">ER-0014</a>

**id**

`14`

**code**

`SUBJECT_NOT_EXIST`

**title**

Subject not exist. Subject不存在.

**detail**

Subject not exist. Subject不存在.

**links**

待补充

### <a name="ER-0015">ER-0015</a>

**id**

`15`

**code**

`CSRF_VERIFY_FAILURE`

**title**

当前页面信息失效，请刷新重试.

**detail**

当前页面信息失效，请刷新重试.

**links**

待补充

### <a name="ER-0016">ER-0016</a>

**id**

`16`

**code**

`AFS_VERIFY_FAILURE`

**title**

滑动验证失效，请刷新重试.

**detail**

滑动验证失效，请刷新重试.

**links**

待补充

### <a name="ER-0017">ER-0017</a>

**id**

`17`

**code**

`NEED_SIGNIN`

**title**

用户未登录.

**detail**

用户未登录.

**links**

待补充

### <a name="ER-0018">ER-0018</a>

**id**

`18`

**code**

`SMS_SEND_TOO_QUICK`

**title**

短信发送太频繁.

**detail**

短信发送太频繁.

**links**

待补充

### <a name="ER-0019">ER-0019</a>

**id**

`19`

**code**

`HASH_INVALID`

**title**

哈希无效.

**detail**

哈希无效.

**links**

待补充

### <a name="ER-0020">ER-0020</a>

**id**

`20`

**code**

`LOGIN_EXPIRED`

**title**

用户登录失效.

**detail**

用户登录失效.

**links**

待补充

### <a name="ER-0021">ER-0021</a>

**id**

`21`

**code**

`PURVIEW_UNDEFINED`

**title**

权限未定义.

**detail**

权限未定义.

**links**

待补充

### <a name="ER-0022">ER-0022</a>

**id**

`22`

**code**

`CAPTCHA_ERROR`

**title**

验证码不正确.

**detail**

验证码不正确.

**links**

待补充

### ER-0023 - ER-0099

系统错误预留

### <a name="ER-0100">ER-0100</a>

**id**

`100`

**code**

`PARAMETER_IS_UNIQUE`

**title**

数据重复.

**detail**

表述传输了一个已经存在的数据,但是该数据不能重复.

**links**

待补充

### <a name="ER-0101">ER-0101</a>

**id**

`101`

**code**

`PARAMETER_FORMAT_INCORRECT`

**title**

数据格式不正确.

**detail**

表述传输了一个格式不正确的字段.

**links**

待补充

### <a name="ER-0102">ER-0102</a>

**id**

`102`

**code**

`PARAMETER_IS_EMPTY`

**title**

参数为空.

**detail**

表述传输了一个空数据,但是该数据不能为空.

**links**

待补充

### <a name="ER-0103">ER-0103</a>

**id**

`103`

**code**

`STATUS_CAN_NOT_MODIFY`

**title**

状态不能操作

**detail**

表述该资源状态不能操作.

**links**

待补充

### ER-0104 - ER-0120 通用错误预留

### <a name="ER-0121">ER-0121</a>

**id**

`0121`

**code**

`IMAGE_FORMAT_ERROR`

**title**

图片格式不正确.

**detail**

图片格式不正确,图片格式为jpg,png,jpeg,gif.

**links**

待补充

### <a name="ER-0122">ER-0122</a>

**id**

`0122`

**code**

`ATTACHMENT_FORMAT_ERROR`

**title**

附件格式不正确.

**detail**

附件格式不正确,附件格式为zip, doc, docx，xls, xlsx.

**links**

待补充

### <a name="ER-0123">ER-0123</a>

**id**

`0123`

**code**

`REAL_NAME_FORMAT_ERROR`

**title**

姓名格式不正确.

**detail**

姓名格式不正确.

**links**

待补充

### <a name="ER-0124">ER-0124</a>

**id**

`0124`

**code**

`CELLPHONE_FORMAT_ERROR`

**title**

手机号格式错误.

**detail**

手机号格式错误.

**links**

待补充

### <a name="ER-0125">ER-0125</a>

**id**

`0125`

**code**

`CARDID_FORMAT_ERROR`

**title**

身份证格式错误.

**detail**

身份证格式错误.

**links**

待补充

### <a name="ER-0126">ER-0126</a>

**id**

`0126`

**code**

`NAME_FORMAT_ERROR`

**title**

名称格式不正确.

**detail**

名称格式不正确.

**links**

待补充

### <a name="ER-0127">ER-0127</a>

**id**

`0127`

**code**

`TITLE_FORMAT_ERROR`

**title**

标题格式不正确

**detail**

标题长度范围为5-80

**links**

待补充

### <a name="ER-0128">ER-0128</a>

**id**

`0128`

**code**

`DESCRIPTION_FORMAT_ERROR`

**title**

描述格式不正确.

**detail**

描述格式不正确.

**links**

待补充

### <a name="ER-0129">ER-0129</a>

**id**

`0129`

**code**

`REASON_FORMAT_ERROR`

**title**

原因格式不正确.

**detail**

原因格式不正确.

**links**

待补充

### `ER-0130` - `ER-0200`: 通用错误预留

### <a name="ER-201">ER-201</a>

**id**

`201`

**code**

`JWT_TOKEN_EMPTY`

**title**

jwt-token 为空，您没有访问权限.

**detail**

jwt-token 为空，您没有访问权限.

**links**

待补充

### <a name="ER-202">ER-202</a>

**id**

`202`

**code**

`JWT_TOKEN_OVERDUE`

**title**

jwt-token 已过期，请重新登录.

**detail**

jwt-token 已过期，请重新登录.

**links**

待补充

### <a name="ER-203">ER-203</a>

**id**

`203`

**code**

`JWT_TOKEN_ERROR`

**title**

jwt-token 验证失败,请重新登录.

**detail**

jwt-token 验证失败,请重新登录.

**links**

待补充

### `ER-204` - `ER-0300`: OA通用错误预留

