# 工单任务管理错误提示规范（10301-10400）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-10301: 标题格式不正确
* ER-10302: 目录类型格式不正确
* ER-10303: 终结时间格式不正确
* ER-10304: 反馈信息格式不正确
* ER-10305: 反馈信息用户格式不正确
* ER-10306: 反馈信息委办局格式不正确
* ER-10307: 反馈信息目录格式不正确
* ER-10308: 反馈信息是否已存在目录格式不正确
* ER-10309 - ER-10400: 工单任务管理错误预留

---

### <a name="ER-10301">ER-10301</a>

**id**

`10301`

**code**

`WORK_ORDER_TASK_TITLE_FORMAT_ERROR`

**title**

工单任务标题格式不正确.

**detail**

工单任务标题格式不正确.

**links**

待补充

### <a name="ER-10302">ER-10302</a>

**id**

`10302`

**code**

`TEMPLATE_TYPE_FORMAT_ERROR`

**title**

工单任务目录类型格式不正确.

**detail**

工单任务目录类型格式不正确.

**links**

待补充

### <a name="ER-10303">ER-10303</a>

**id**

`10303`

**code**

`END_TIME_FORMAT_ERROR`

**title**

终结时间格式不正确.

**detail**

终结时间格式不正确.

**links**

待补充

### <a name="ER-10304">ER-10304</a>

**id**

`10304`

**code**

`FEEDBACK_RECORDS_FORMAT_ERROR`

**title**

反馈信息格式不正确.

**detail**

反馈信息格式不正确.

**links**

待补充

### <a name="ER-10305">ER-10305</a>

**id**

`10305`

**code**

`CREW_FORMAT_ERROR`

**title**

反馈信息用户格式不正确.

**detail**

反馈信息用户格式不正确.

**links**

待补充

### <a name="ER-10306">ER-10306</a>

**id**

`10306`

**code**

`USER_GROUP_FORMAT_ERROR`

**title**

反馈信息委办局格式不正确.

**detail**

反馈信息委办局格式不正确.

**links**

待补充

### <a name="ER-10307">ER-10307</a>

**id**

`10307`

**code**

`TEMPLATE_ID_FORMAT_ERROR`

**title**

反馈信息目录格式不正确.

**detail**

反馈信息目录格式不正确.

**links**

待补充

### <a name="ER-10308">ER-10308</a>

**id**

`10308`

**code**

`IS_EXISTED_TEMPLATE_PARAMETER_FORMAT_ERROR`

**title**

反馈信息是否已存在目录格式不正确.

**detail**

反馈信息是否已存在目录格式不正确.

**links**

待补充

### ER-10309 - ER-10400 错误预留