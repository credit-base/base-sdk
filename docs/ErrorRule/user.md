# 用户通用错误提示规范（501-600）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

---

* ER-501: 密码格式不正确
* ER-502: 旧密码不正确
* ER-503: 密码不正确
* ER-504: 确认密码与密码不一致
* ER-505: 手机号已存在
* ER-506: 手机号不存在
* ER-507 - ER-600: 用户通用错误预留

---

### <a name="ER-501">ER-501</a>

**id**

`501`

**code**

`PASSWORD_FORMAT_ERROR`

**title**

密码格式不正确.

**detail**

密码格式不正确.

**links**

待补充

### <a name="ER-502">ER-502</a>

**id**

`502`

**code**

`OLD_PASSWORD_INCORRECT`

**title**

旧密码不正确.

**detail**

旧密码不正确.

**links**

待补充

### <a name="ER-503">ER-503</a>

**id**

`503`

**code**

`PASSWORD_INCORRECT`

**title**

密码不正确.

**detail**

密码不正确.

**links**

待补充

### <a name="ER-504">ER-504</a>

**id**

`504`

**code**

`PASSWORD_INCONSISTENCY`

**title**

密码不一致,请重新输入.

**detail**

密码不一致,请重新输入.

**links**

待补充

### <a name="ER-505">ER-505</a>

**id**

`505`

**code**

`CELLPHONE_EXIST`

**title**

手机号已存在

**detail**

手机号已存在

**links**

待补充

### <a name="ER-506">ER-506</a>

**id**

`506`

**code**

`CELLPHONE_NOT_EXIST`

**title**

手机号不存在

**detail**

手机号不存在

**links**

待补充

### ER-507 - ER-600 错误预留