# 信用刊物

### 英文名称

**中文名称** 描述信息

---

### title

**标题** 刊物标题的表述.

* string

### source

**来源** 来源的表述.

* string

### publishUserGroup

**发布单位** 发布单位的表述,此处指的是我们系统中的单位.

* UserGroup

### description

**简介** 简介的表述.

* string

### cover

**封面** 封面的表述.

* array

### attachment

**附件** 附件的表述.

* array

### authImages

**授权图片** 授权图片的表述.

* array

### year

**年份** 年份的表述.

* int

### [状态](common.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
