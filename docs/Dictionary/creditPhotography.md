# 信用随手拍

### 英文名称

**中文名称** 描述信息

---

### member 

**前台用户** 对于发布信息的用户信息的表述

* Member

### realName 

**前台用户真实姓名** 此字段只用于搜索

* string

### description

**描述** 对于描述的表述.

* string

### attachments

**图片/视频** 图片/视频的表述.

* array

### status

**门户状态** 门户状态的表述.

* int

	* STATUS_NORMAL | 正常 | Lw
	* STATUS_DELETED | 删除 | LC0

### applyCrew

**审核人** 审核人的表述

* Crew

### [审核状态](common.md)
### [驳回原因](common.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
