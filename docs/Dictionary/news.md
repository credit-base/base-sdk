# 新闻字典

### 英文名称

**中文名称** 描述信息

---

### title

**标题** 新闻标题的表述.

* string

### source

**来源** 新闻来源的表述.

* string

### content

**内容** 新闻内容的表述.

* string

### description

**描述** 新闻描述的表述.

* string

### parentCategory

**新闻父级分类** 新闻父级分类的表述.

* int

### category

**新闻分类** 新闻分类的表述.

* int

### newsType

**新闻类型** 新闻类型的表述.

* int

### dimension

**数据共享维度** 新闻数据共享维度的表述.

* int
	* SOCIOLOGY | 社会公开 | 1 
	* GOVERNMENT_AFFAIRS | 政务共享 | 2

### cover

**封面** 新闻封面的表述.

* array

### attachments

**附件** 新闻附件的表述.

* array

### publishUserGroup

**发布委办局** 新闻发布委办局的表述,此处指的是我们系统中的委办局.

* UserGroup

### crew

**发布人** 新闻发布人的表述

* Crew

### bannerStatus

**轮播状态** 新闻设为轮播的状态表述.

* int

	* BANNER_STATUS_DISABLED | 不轮播 | 0
	* BANNER_STATUS_ENABLED | 轮播 |  2

### bannerImage

**轮播图图片** 轮播图图片的表述.

* array

### homePageShowStatus

**首页展示状态** 新闻首页展示状态的表述.

* int

	* HOME_PAGE_SHOW_STATUS_DISABLED | 不展示 | 0
	* HOME_PAGE_SHOW_STATUS_ENABLED | 展示 |  2

### status

**状态** 新闻状态的表述.

* int

	* STATUS_ENABLED | 启用 | 0
	* STATUS_DISABLED | 禁用 | -2

### stick

**置顶状态** 新闻置顶状态的表述.

* int

	* STICK_ENABLED | 置顶 | 2
	* STICK_DISABLED | 未置顶 | 0
	
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)

## 新闻分类定义

* [新闻分类定义](./docs/About/newsCategoryDefine.md "新闻分类定义")

