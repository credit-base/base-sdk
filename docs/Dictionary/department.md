# 科室管理字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### userGroup

**所属委办局** 所属委办局的表述.

* UserGroup

### name

**科室名称** 科室名称的表述.

* string

### [状态](common.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)