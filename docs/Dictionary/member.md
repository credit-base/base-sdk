# 前台用户管理字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [人通用字典](./docs/Dictionary/user.md "人通用字典")

### userName

**用户名**

* string
* 2-20位字符,唯一
* 必填项

### cardId

**身份证号码**

* string
* 15位或18位、数字或数字+大写字母
* 必填项

### email

**邮箱**

* string
* 2-30位字符,必须包含"@"和".",唯一
* 必填项

### contactAddress

**联系地址**

* string
* 1-255位字符
* 必填项

### securityQuestion

**密保问题**

* int
	* SECURITY_QUESTION[QUESTION_ONE] = 1 //你父亲或母亲的姓名
	* SECURITY_QUESTION[QUESTION_TWO] = 2 //你喜欢看的电影
	* SECURITY_QUESTION[QUESTION_THREE] = 3 //你最好朋友的名字
	* SECURITY_QUESTION[QUESTION_FOUR] = 4 //你毕业于哪个高中
	* SECURITY_QUESTION[QUESTION_FIVE] = 5 //你最喜欢的颜色
* 必填项

### securityAnswer

**密保答案**

* string,1-30位字符
* 必填项

### gender

**性别**

* int
	* GENDER[NULL] = Lw //默认 未知
	* GENDER[MALE] = MA //男
	* GENDER[FEMALE] = MQ //女
* 选填项

### status

**状态** 状态的表述.

* int

	* STATUS_ENABLED Lw 启用
	* STATUS_DISABLED LC0 禁用

### [手机号](user.md)
### [姓名](user.md)
### [密码](user.md)
### [确认密码](user.md)
### [旧密码](user.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
