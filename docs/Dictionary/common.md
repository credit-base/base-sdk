# 通用词典

### 英文名称

**中文名称** 描述信息

---

## 动词

### signIn

**登录**

### signUp

**注册**

### signOut

**退出**

### updatePassword 

**修改密码**

### show

**查看**

### add

**新增**

### edit

**编辑**

### resubmit

**重新编辑**

### delete

**删除**

### truncate

**清空**

### audit

**审核**

### reject

**驳回**

### approve

**通过**

### move

**移动**

### top

**置顶**

### cancelTop

**取消置顶**

---

## 名词

### status

**状态**

### statusTime

**状态更新时间** 状态更新时间的表述.

### createTime

**创建时间** 创建时间的表述.

### updateTime

**更新时间** 更新时间的表述.

### category

**分类** 大的分类的表述,一般指一级分类.

### type

**分类** 小的分类表述,一般指一级以下的分类.

### captcha

**验证码** 验证码.包括短信验证码等对验证码关键词统一的表述.

*　string

### 审核信息

#### applyStatus 

**审核状态** 表述审核数据的审核状态.

* int
	* APPLY_STATUS['PENDING'] = 0 //待审核
	* APPLY_STATUS ['APPROVE'] = 2 //通过
	* APPLY_STATUS['REJECT'] =-2 //驳回

### rejectReason

**驳回原因** 审核驳回原因的表述.

* string

### operationType

**操作类型** 审核操作的表述.

* int
	* OPERATION_TYPE['ADD'] = 1 //添加
	* OPERATION_TYPE['EDIT'] = 2 //编辑
	* OPERATION_TYPE['ENABLED'] = 3 //启用
	* OPERATION_TYPE['DISABLED'] = 4 //禁用
	* OPERATION_TYPE['TOP'] = 5 //置顶
	* OPERATION_TYPE['CANCEL_TOP'] = 6 //取消置顶
	* OPERATION_TYPE['MOVE'] = 7 //移动

#### applyCrew 

**审核人** 表述审核数据的审核人.

#### applyUserGroup 

**审核单位** 表述审核数据的审核单位.
