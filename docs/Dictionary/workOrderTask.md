# 任务目录字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [目录信息字典](./docs/Dictionary/template.md "目录信息字典")

---

## <a name="任务信息">任务信息</a>

### title

**任务标题** 

* string
* 1-100
* 必填项

### templateType

**目录类别** 

* int
* 必填项

### template

**目录信息** 

* GbTemplate/BjTemplate

### endTime

**终结时间** 

* int
* 必填项

### description

**任务描述** 

* string
* 1-2000
* 必填项

### reason

**原因** 

* string
* 1-2000
* 必填项

### attachment

**附件** 

* pdf
* 10M
* 非必填

### assignObjects

**指派对象** 

* array

### crew

**用户信息** 

* Crew

### items

**模板信息** 

* array