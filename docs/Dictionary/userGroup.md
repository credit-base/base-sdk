# 委办局管理字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### name

**委办局名称** 委办局名称的表述.

* string

### shortName

**委办局简称** 委办局简称的表述.

* string

### pageScene

**请求分页类型** 请求分页类型的表述.

* Lw=>获取分页数据/MA=>获取全部数据
* string

### [状态](common.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)