# 新闻类控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 新闻类

* `TITLE`: 标题控件规范
* `SOURCE`: 来源控件规范
* `COVER`: 封面控件规范
* `ATTACHMENTS`: 附件控件规范
* `STATUS`: 状态控件规范.
* `STICK`: 置顶状态控件规范.
* `CONTENT`: 内容控件规范
* `PARENT_CATEGORY`: 新闻父级分类控件规范
* `CATEGORY`: 新闻分类控件规范
* `NEWS_TYPE`: 新闻类型控件规范
* `DIMENSION`: 数据纬度控件规范
* `BANNER_STATUS`: 轮播状态控件规范
* `BANNER_IMAGE`: 轮播图图片控件规范
* `HOME_PAGE_SHOW_STATUS`: 首页展示状态控件规范

### `TITLE`
* [标题控件规范(TITLE)](common.md)(必填)

### `SOURCE`
* [来源控件规范(SOURCE)](common.md)(必填)

### `COVER`
* [封面控件规范(IMAGE)](common.md)(选填)

### `ATTACHMENTS`
* [附件控件规范(ATTACHMENTS)](common.md)(选填)

### `STATUS`
* [状态控件规范(STATUS)](common.md)(必填)

### `STICK`
* [置顶状态控件规范(STICK)](common.md)(选填)

### `REJECT_REASON`
* [原因控件规范(REASON)](common.md)(必填)

### `CONTENT`

**内容** 内容控件规范

#### 规则

* string
* 必填

#### 错误提示

* `ER-102`: 内容参数为空
* `ER-101`: 数据格式不正确

### `NEWS_TYPE`

**新闻类型** 控件规范,用于表述新闻类型

#### 规则

* int positive
* 在新闻类型数组中
* 必填

#### 错误提示

* `ER-102`,新闻类型不能为空
* `ER-10401`,新闻栏目分类不存在

### `DIMENSION`

**数据纬度** 控件规范,用于表述数据纬度

#### 规则

* int positive
	SOCIOLOGY | 社会公开 | 1 默认
	GOVERNMENT_AFFAIRS | 政务共享 | 2
* 属于1,2
* 必填

#### 错误提示

* `ER-102`,数据纬度不能为空
* `ER-10401`,新闻公共类型不存在

### `BANNER_STATUS`

**轮播状态** 控件规范,用于表述轮播状态

#### 规则

* int positive
* 轮播状态在以下范围中
	* BANNER_STATUS_DISABLED | 不轮播 | Lw 默认
	* BANNER_STATUS_ENABLED | 轮播 |  MQ
* 选填

#### 错误提示

* `ER-10404`,新闻轮播状态不存在

### `BANNER_IMAGE`
* [轮播图图片控件规范(IMAGE)](common.md)(轮播状态 Lw 选填 MQ 必填)

### `HOME_PAGE_SHOW_STATUS`

**首页展示状态** 控件规范,用于表述首页展示状态

#### 规则

* int positive
* 首页展示状态在以下范围中
	* HOME_PAGE_SHOW_STATUS_DISABLED | 不展示 | Lw 默认
	* HOME_PAGE_SHOW_STATUS_ENABLED | 展示 |  MQ
* 选填

#### 错误提示

* `ER-10403`,新闻首页展示状态不存在




