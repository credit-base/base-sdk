# 控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 员工管理

* `REAL_NAME`: 姓名
* `CELLPHONE`: 手机号码
* `CARDID`: 身份证号
* `PASSWORD`: 密码控件规范
* `REPASSWORD`: 重复密码控件规范
* `USER_GROUP`: 所属委办局
* `DEPARTMENT`: 所属科室
* `CATEGORY`: 员工类型
* `PURVIEW`: 权限范围

### `REAL_NAME`
* [姓名控件规范(REAL_NAME)](common.md)

### `CELLPHONE`
* [手机号控件规范(CELLPHONE)](common.md)

### `CARDID`
* [身份证号控件规范(CARDID)](common.md)

### `PASSWORD`
* [密码控件规范(PASSWORD)](user.md)

### `REPASSWORD`
* [重复密码控件规范(REPASSWORD)](user.md)

### `USER_GROUP`

**所属委办局** 所属委办局控件规范

#### 规则

* int

#### 错误提示

* `ER-0101`: 所属委办局格式不正确

### `DEPARTMENT`

**所属科室** 所属科室控件规范

#### 规则

* int

#### 错误提示

* `ER-0101`: 所属科室格式不正确

### `CATEGORY`

**员工类型** 员工类型控件规范

#### 规则

* 不能为空
* 数字，可选范围 2，3，4

#### 错误提示

* `ER-0102`: 员工类型不能为空
* `ER-10101`: 员工类型格式不正确

### `PURVIEW`

**权限范围** 权限范围控件规范

#### 规则

* 一维数组,且值为int

#### 错误提示

* `ER-10102`: 权限范围格式不正确



