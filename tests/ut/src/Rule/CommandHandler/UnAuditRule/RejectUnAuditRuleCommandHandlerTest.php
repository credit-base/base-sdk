<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

class RejectUnAuditRuleCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockRejectUnAuditRuleCommandHandler::class)
            ->setMethods(['fetchUnAuditRule'])
            ->getMock();
    }

    public function testExtendsRevokeCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->stub
        );
    }

    public function testFetchIModifyStatusObject()
    {
        $commandHandler = $this->getMockBuilder(MockRejectUnAuditRuleCommandHandler::class)
            ->setMethods(['fetchUnAuditRule'])
            ->getMock();

        $id = 1;
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule($id);

        $commandHandler->expects($this->exactly(1))
                    ->method('fetchUnAuditRule')
                    ->with($id)
                    ->willReturn($unAuditRule);

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditRule);
    }
}
