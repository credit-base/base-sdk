<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Rule\Model\UnAuditRule;
use Base\Sdk\Rule\Repository\UnAuditRuleRepository;
use Base\Sdk\Rule\Command\UnAuditRule\EditUnAuditRuleCommand;

class EditUnAuditRuleCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(MockEditUnAuditRuleCommandHandler::class)
            ->setMethods(['fetchUnAuditRule'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Repository\UnAuditRuleRepository',
            $this->commandHandler->getPublicRepository()
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(MockEditUnAuditRuleCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->fakerData = [
            'rules'=>array(1,2)
        ];
        $id = 1;
        
        $command = new EditUnAuditRuleCommand(
            $this->fakerData['rules'],
            $id
        );

        $unAuditRule = $this->prophesize(UnAuditRule::class);

        $unAuditRule->setRules(Argument::exact($command->rules))
             ->shouldBeCalledTimes(1);
        
        $unAuditRule->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $unAuditRuleRepository = $this->prophesize(UnAuditRuleRepository::class);
        $unAuditRuleRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($unAuditRule->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($unAuditRuleRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
