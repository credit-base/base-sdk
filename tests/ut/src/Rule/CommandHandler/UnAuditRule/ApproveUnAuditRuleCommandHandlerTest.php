<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Base\Sdk\Rule\Repository\UnAuditRuleRepository;

class ApproveUnAuditRuleCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveUnAuditRuleCommandHandler::class)
            ->setMethods(['fetchUnAuditRule'])
            ->getMock();
    }

    public function testExtendsApproveCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }

    public function testFetchIApplyObject()
    {
        $commandHandler = $this->getMockBuilder(MockApproveUnAuditRuleCommandHandler::class)
            ->setMethods(['fetchUnAuditRule'])
            ->getMock();

        $id = 1;
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule($id);

        $commandHandler->expects($this->exactly(1))
            ->method('fetchUnAuditRule')
            ->with($id)
            ->willReturn($unAuditRule);

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditRule);
    }
}
