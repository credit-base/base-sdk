<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use PHPUnit\Framework\TestCase;

class RevokeUnAuditRuleCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRevokeUnAuditRuleCommandHandler();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RevokeCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $commandHandler = $this->getMockBuilder(MockRevokeUnAuditRuleCommandHandler::class)
            ->setMethods(['fetchUnAuditRule'])
            ->getMock();

        $id = 1;
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule($id);

        $commandHandler->expects($this->exactly(1))
                    ->method('fetchUnAuditRule')
                    ->with($id)
                    ->willReturn($unAuditRule);


        $result = $commandHandler->fetchIModifyStatusObject($id);

        $this->assertEquals($result, $unAuditRule);
    }
}
