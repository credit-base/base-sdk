<?php
namespace Base\Sdk\Rule\CommandHandler\Rule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Repository\RuleRepository;

class DeleteRuleCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDeleteRuleCommandHandler::class)
            ->setMethods(['fetchRule'])
            ->getMock();
    }

    public function testExtendsDeleteCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\DeleteCommandHandler',
            $this->stub
        );
    }

    public function testFetchIModifyStatusObject()
    {
        $commandHandler = $this->getMockBuilder(MockDeleteRuleCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $rule = \Base\Sdk\Rule\Utils\Rule\MockFactory::generateRule($id);

        $repository = $this->prophesize(RuleRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($rule);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIModifyStatusObject($id);

        $this->assertEquals($result, $rule);
    }
}
