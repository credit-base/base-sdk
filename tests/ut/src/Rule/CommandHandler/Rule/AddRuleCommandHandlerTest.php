<?php
namespace Base\Sdk\Rule\CommandHandler\Rule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Command\Rule\AddRuleCommand;

use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\Repository\GbTemplateRepository;

class AddRuleCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(MockAddRuleCommandHandler::class)
            ->setMethods([ 'fetchTemplate'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    public function testGetRule()
    {

        $this->assertInstanceOf(
            'Base\Sdk\Rule\Model\Rule',
            $this->commandHandler->getPublicRule()
        );
    }

    public function testGetRepository()
    {

        $this->assertInstanceOf(
            'Base\Sdk\Rule\Repository\RuleRepository',
            $this->commandHandler->getPublicRepository()
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(MockAddRuleCommandHandler::class)
            ->setMethods(['getRule','fetchTemplate'])
            ->getMock();

        $this->fakerData = [
            'rules'=>array(1,2),
            'transformationCategory'=>1,
            'sourceCategory'=>2,
            'sourceTemplate'=>1,
            'transformationTemplate'=>1
        ];
        
        $command = new AddRuleCommand(
            $this->fakerData['rules'],
            $this->fakerData['transformationTemplate'],
            $this->fakerData['sourceTemplate'],
            $this->fakerData['transformationCategory'],
            $this->fakerData['sourceCategory']
        );

        $rule = $this->prophesize(Rule::class);
        $sourceTemplate = $this->prophesize(WbjTemplate::class);
        $transformationTemplate = $this->prophesize(GbTemplate::class);

        $this->commandHandler->expects($this->exactly(2))
        ->method('fetchTemplate')
        ->will($this->returnValueMap([
            [
                $command->transformationTemplate,
                $command->transformationCategory,
                $transformationTemplate->reveal()
            ],
            [
                $command->sourceTemplate,
                $command->sourceCategory,
                $sourceTemplate->reveal()
            ]]));
       
        $rule->setRules(Argument::exact($command->rules))->shouldBeCalledTimes(1);
        $rule->setTransformationCategory(Argument::exact($command->transformationCategory))->shouldBeCalledTimes(1);
        $rule->setSourceCategory(Argument::exact($command->sourceCategory))->shouldBeCalledTimes(1);
        $rule->setSourceTemplate(Argument::exact($sourceTemplate))->shouldBeCalledTimes(1);
        $rule->setTransformationTemplate(Argument::exact($transformationTemplate))->shouldBeCalledTimes(1);

        $rule->add()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->any())
            ->method('getRule')
            ->willReturn($rule->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testGetTemplateRepository()
    {
        $this->commandHandler = $this->getMockBuilder(MockAddRuleCommandHandler::class)
            ->setMethods(['fetchOne'])
            ->getMock();

        $category = 2;

        $result = $this->commandHandler->getTemplateRepositoryByCategory($category);
        $this->assertInstanceOf('Base\Sdk\Template\Repository\GbTemplateRepository', $result);
    }

    public function testFetchTemplate()
    {
        $this->commandHandler = $this->getMockBuilder(MockAddRuleCommandHandler::class)
            ->setMethods(['getTemplateRepositoryByCategory'])
            ->getMock();
        
        $category = 2;
        $templateId = 1;

        $template = new GbTemplate($templateId);
        $templateRepository = $this->prophesize(GbTemplateRepository::class);
        $this->commandHandler->expects($this->exactly(1))
            ->method('getTemplateRepositoryByCategory')
            ->willReturn($templateRepository->reveal());

        $templateRepository->fetchOne(Argument::exact($templateId))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($template);

        $result = $this->commandHandler->fetchTemplate($templateId, $category);

        $this->assertEquals($result, $template);
    }
}
