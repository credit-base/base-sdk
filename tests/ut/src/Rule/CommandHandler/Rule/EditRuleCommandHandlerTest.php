<?php
namespace Base\Sdk\Rule\CommandHandler\Rule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Repository\RuleRepository;
use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\Rule\Command\Rule\EditRuleCommand;

class EditRuleCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditRuleCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->fakerData = [
            'rules'=>array(1,2)
        ];

        $id = 1;
        
        $command = new EditRuleCommand(
            $this->fakerData['rules'],
            $id
        );

        $rule = $this->prophesize(Rule::class);
        
        $ruleRepository = $this->prophesize(RuleRepository::class);
        $ruleRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($rule->reveal());
                                
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($ruleRepository->reveal());

        $rule->setRules(Argument::exact($command->rules))
             ->shouldBeCalledTimes(1);

        $rule->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
