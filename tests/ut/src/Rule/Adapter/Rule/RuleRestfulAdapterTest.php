<?php
namespace Base\Sdk\Rule\Adapter\Rule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Model\NullRule;
use Base\Sdk\Rule\Translator\RuleRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class RuleRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(RuleRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors',
                               'rulesMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends RuleRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getRuleMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIRuleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Adapter\Rule\IRuleAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('rules', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'RULE_LIST',
                RuleRestfulAdapter::SCENARIOS['RULE_LIST']
            ],
            [
                'RULE_FETCH_ONE',
                RuleRestfulAdapter::SCENARIOS['RULE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    public function testFetchOne()
    {
        $id = 1;

        $rule = \Base\Sdk\Rule\Utils\Rule\MockFactory::generateRule($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullRule::getInstance())
            ->willReturn($rule);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($rule, $result);
    }

    /**
     * 为RuleRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$Rule,$keys,$RuleArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareRuleTranslator(
        Rule $rule,
        array $keys,
        array $ruleArray
    ) {
        $translator = $this->prophesize(RuleRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($rule),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($ruleArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Rule $rule)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($rule);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $rule = \Base\Sdk\Rule\Utils\Rule\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator(
            $rule,
            array(
                'rules',
                'crew',
                'transformationTemplate',
                'sourceTemplate',
                'transformationCategory',
                'sourceCategory'
            ),
            $ruleArray
        );

        $this->adapter
            ->method('post')
            ->with('', $ruleArray);

        $this->success($rule);
        $result = $this->adapter->add($rule);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $rule = \Base\Sdk\Rule\Utils\Rule\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator(
            $rule,
            array(
                'rules',
                'crew',
                'transformationTemplate',
                'sourceTemplate',
                'transformationCategory',
                'sourceCategory'
            ),
            $ruleArray
        );

        $this->adapter
            ->method('post')
            ->with('', $ruleArray);
        
            $this->failure($rule);
            $result = $this->adapter->add($rule);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $rule = \Base\Sdk\Rule\Utils\Rule\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator(
            $rule,
            array(
                'rules',
                'crew',
            ),
            $ruleArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$rule->getId(), $ruleArray);

        $this->success($rule);
        $result = $this->adapter->edit($rule);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $rule = \Base\Sdk\Rule\Utils\Rule\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator(
            $rule,
            array(
                'rules',
                'crew',
            ),
            $ruleArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$rule->getId(), $ruleArray);
        
            $this->failure($rule);
            $result = $this->adapter->edit($rule);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行deletes（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $rule = \Base\Sdk\Rule\Utils\Rule\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator(
            $rule,
            array('crew'),
            $ruleArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$rule->getId().'/delete', $ruleArray);

        $this->success($rule);
        $result = $this->adapter->deletes($rule);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行deletes（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $rule = \Base\Sdk\Rule\Utils\Rule\MockFactory::generateRule(1);
        $ruleArray = array();

        $this->prepareRuleTranslator(
            $rule,
            array('crew'),
            $ruleArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$rule->getId().'/delete', $ruleArray);
        
        $this->failure($rule);
        $result = $this->adapter->deletes($rule);
        $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $mapError = [
            103=>[
                'rule'=>RULES_IS_UNIQUE
            ],
            100=>[
                'crewId'=>PARAMETER_IS_EMPTY,
                'userGroupId'=>DEPARTMENT_USER_GROUP_IS_EMPTY,
                'sourceTemplateId'=>SOURCE_TEMPLATE_IS_EMPTY,
                'transformationTemplateId'=>TRANSFORMATION_TEMPLATE_IS_EMPTY
            ],
            102=>[
                'status'=>STATUS_CAN_NOT_MODIFY,
                'applyStatus'=>STATUS_CAN_NOT_MODIFY
            ],
            101 => [
                'rules' => RULES_FORMAT_ERROR,
                'rulesName'=>RULES_NAME_NOT_UNIQUE,
                'deDuplicationRules'=>DE_DUPLICATION_RULES_FORMAT_ERROR,
                'deDuplicationRulesItems'=>DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR,
                'deDuplicationRulesResult' => DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR,
                'completionRulesCount'=>COMPLETION_RULES_COUNT_FORMAT_ERROR,
                'completionRulesId' => COMPLETION_RULES_ID_FORMAT_ERROR,
                'completionRulesBase'=>COMPLETION_RULES_BASE_FORMAT_ERROR,
                'completionRulesItem'=>COMPLETION_RULES_ITEM_FORMAT_ERROR,
                'completionRules'=>COMPLETION_RULES_FORMAT_ERROR,
                'comparisonRulesCount'=>COMPARISON_RULES_COUNT_FORMAT_ERROR,
                'transformationRulesTransformationNotExist'=>TRANSFORMATION_RULES_TRANSFORMATION_NOT_EXIST,
                'transformationRulesSourceNotExist'=>TRANSFORMATION_RULES_SOURCE_NOT_EXIST,
                'transformationItemTypeNotExist'=>TRANSFORMATION_ITEM_TYPE_NOT_EXIST,
                'typeCannotTransformation'=>TYPE_CANNOT_TRANSFORMATION,
                'lengthCannotTransformation'=>LENGTH_CANNOT_TRANSFORMATION,
                'dimensionCannotTransformation'=>DIMENSION_CANNOT_TRANSFORMATION,
                'isMaskedCannotTransformation'=>IS_MASKED_CANNOT_TRANSFORMATION,
                'comparisonRulesId' => COMPARISON_RULES_ID_FORMAT_ERROR,
                'comparisonRulesBase'=>COMPARISON_RULES_BASE_FORMAT_ERROR,
                'comparisonRulesItem'=>COMPARISON_RULES_ITEM_FORMAT_ERROR,
                'comparisonRules'=>COMPARISON_RULES_FORMAT_ERROR,
                'maskRuleCannotTransformation'=>MASK_RULE_CANNOT_TRANSFORMATION,
                'zrrNotIncludeIdentify'=>ZRR_NOT_INCLUDE_IDENTIFY,
                'deDuplicationRulesItemsNotExit'=>DE_DUPLICATION_RULES_ITEMS_NOT_EXIT,
                'transformationTemplateId' => TRANSFORMATION_TEMPLATE_FORMAT_ERROR,
                'sourceTemplateId' => SOURCE_TEMPLATE_FORMAT_ERROR,
                'transformationCategory' => TRANSFORMATION_CATEGORY_FORMAT_ERROR,
                'sourceCategory' => SOURCE_CATEGORY_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
                'completionRulesTransformationItemNotExist'=>COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
                'completionRulesCompletionTemplateNotExist'=>COMPLETION_RULES_COMPLETION_TEMPLATE_NOT_EXIST,
                'completionRulesCompletionTemplateItemNotExist'=>COMPLETION_RULES_COMPLETION_TEMPLATE_ITEM_NOT_EXIST,
                'comparisonRulesTransformationItemNotExist'=>COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
                'comparisonRulesComparisonTemplateNotExist'=>COMPARISON_RULES_COMPARISON_TEMPLATE_NOT_EXIST,
                'comparisonRulesBaseCannotName'=>COMPARISON_RULES_BASE_CANNOT_NAME,
                'comparisonRulesBaseCannotIdentify'=>COMPARISON_RULES_BASE_CANNOT_IDENTIFY,
                'comparisonRulesComparisonTemplateItemNotExist'=>COMPARISON_RULES_COMPARISON_TEMPLATE_ITEM_NOT_EXIST,
                'applyCrewId' => APPLY_CREW_FORMAT_ERROR,
                'crewId' => CREW_ID_FORMAT_ERROR,
                'requiredItemsMapping' => REQUIRED_ITEMS_MAPPING_IS_EXIT,
            ],
        ];

        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $this->adapter->expects($this->any())
        ->method('rulesMapErrors')
        ->willReturn($mapError);
        
        $result = $this->childAdapter->getRuleMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
