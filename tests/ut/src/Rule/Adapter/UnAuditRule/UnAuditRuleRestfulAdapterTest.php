<?php
namespace Base\Sdk\Rule\Adapter\UnAuditRule;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Rule\Model\UnAuditRule;
use Base\Sdk\Rule\Model\NullUnAuditRule;
use Base\Sdk\Rule\Translator\UnAuditRuleRestfulTranslator;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class UnAuditRuleRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UnAuditRuleRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors',
                               'rulesMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends UnAuditRuleRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getUnAuditRuleMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIUnAuditRuleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Adapter\UnAuditRule\IUnAuditRuleAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedRules', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDIT_RULE_LIST',
                UnAuditRuleRestfulAdapter::SCENARIOS['UN_AUDIT_RULE_LIST']
            ],
            [
                'UN_AUDIT_RULE_FETCH_ONE',
                UnAuditRuleRestfulAdapter::SCENARIOS['UN_AUDIT_RULE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    public function testFetchOne()
    {
        $id = 1;

        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullUnAuditRule::getInstance())
            ->willReturn($unAuditRule);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($unAuditRule, $result);
    }

    /**
     * 为UnAuditRuleRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$UnAuditRule,$keys,$UnAuditRuleArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareUnAuditRuleTranslator(
        UnAuditRule $unAuditRule,
        array $keys,
        array $unAuditRuleArray
    ) {
        $translator = $this->prophesize(UnAuditRuleRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($unAuditRule),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($unAuditRuleArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(UnAuditRule $unAuditRule)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($unAuditRule);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAdd()
    {
        $unAuditRule = new NullUnAuditRule(1);
        $result = $this->adapter->add($unAuditRule);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditRuleTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule(1);
        $unAuditRuleArray = array();

        $this->prepareUnAuditRuleTranslator(
            $unAuditRule,
            array(
                'rules',
                'crew'
            ),
            $unAuditRuleArray
        );
        
        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditRule->getId().'/'.'resubmit', $unAuditRuleArray);
            
        $this->success($unAuditRule);
        $result = $this->adapter->edit($unAuditRule);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditRuleTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule(1);
        $unAuditRuleArray = array();

        $this->prepareUnAuditRuleTranslator(
            $unAuditRule,
            array(
                'rules',
                'crew'
            ),
            $unAuditRuleArray
        );
        
        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditRule->getId().'/'.'resubmit', $unAuditRuleArray);
           
            $this->failure($unAuditRule);
            $result = $this->adapter->edit($unAuditRule);
            $this->assertFalse($result);
    }

/**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行Approve（）
     * 判断 result 是否为true
     */
    public function testApproveSuccess()
    {
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule(1);
        $unAuditRuleArray = array();

        $this->prepareUnAuditRuleTranslator(
            $unAuditRule,
            array('applyCrew'),
            $unAuditRuleArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditRule->getId().'/approve', $unAuditRuleArray);

        $this->success($unAuditRule);
        $result = $this->adapter->Approve($unAuditRule);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行Approve（）
     * 判断 result 是否为false
     */
    public function testApproveFailure()
    {
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule(1);
        $unAuditRuleArray = array();

        $this->prepareUnAuditRuleTranslator(
            $unAuditRule,
            array('applyCrew'),
            $unAuditRuleArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditRule->getId().'/approve', $unAuditRuleArray);
        
        $this->failure($unAuditRule);
        $result = $this->adapter->Approve($unAuditRule);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行Reject（）
     * 判断 result 是否为true
     */
    public function testRejectSuccess()
    {
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule(1);
        $unAuditRuleArray = array();

        $this->prepareUnAuditRuleTranslator(
            $unAuditRule,
            array('rejectReason',
            'applyCrew'),
            $unAuditRuleArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditRule->getId().'/reject', $unAuditRuleArray);

        $this->success($unAuditRule);
        $result = $this->adapter->reject($unAuditRule);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行Reject（）
     * 判断 result 是否为false
     */
    public function testRejectFailure()
    {
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule(1);
        $unAuditRuleArray = array();

        $this->prepareUnAuditRuleTranslator(
            $unAuditRule,
            array('rejectReason',
            'applyCrew'),
            $unAuditRuleArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditRule->getId().'/reject', $unAuditRuleArray);
        
        $this->failure($unAuditRule);
        $result = $this->adapter->reject($unAuditRule);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行revoke（）
     * 判断 result 是否为true
     */
    public function testRevokeSuccess()
    {
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule(1);
        $unAuditRuleArray = array();

        $this->prepareUnAuditRuleTranslator(
            $unAuditRule,
            array('crew'),
            $unAuditRuleArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditRule->getId().'/revoke', $unAuditRuleArray);

        $this->success($unAuditRule);
        $result = $this->adapter->revoke($unAuditRule);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditRuleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行revoke（）
     * 判断 result 是否为false
     */
    public function testRevokeFailure()
    {
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule(1);
        $unAuditRuleArray = array();

        $this->prepareUnAuditRuleTranslator(
            $unAuditRule,
            array('crew'),
            $unAuditRuleArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditRule->getId().'/revoke', $unAuditRuleArray);
        
        $this->failure($unAuditRule);
        $result = $this->adapter->revoke($unAuditRule);
        $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [
            101 => [
                'comparisonRulesBase'=>COMPARISON_RULES_BASE_FORMAT_ERROR,
                'comparisonRulesItem'=>COMPARISON_RULES_ITEM_FORMAT_ERROR,
                'comparisonRules'=>COMPARISON_RULES_FORMAT_ERROR,
                'deDuplicationRules'=>DE_DUPLICATION_RULES_FORMAT_ERROR,
                'deDuplicationRulesItems'=>DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR,
                'deDuplicationRulesResult' => DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR,
                'rules' => RULES_FORMAT_ERROR,
                'rulesName'=>RULES_NAME_NOT_UNIQUE,
                'completionRulesCount'=>COMPLETION_RULES_COUNT_FORMAT_ERROR,
                'completionRulesId' => COMPLETION_RULES_ID_FORMAT_ERROR,
                'completionRulesBase'=>COMPLETION_RULES_BASE_FORMAT_ERROR,
                'completionRulesItem'=>COMPLETION_RULES_ITEM_FORMAT_ERROR,
                'completionRules'=>COMPLETION_RULES_FORMAT_ERROR,
                'comparisonRulesCount'=>COMPARISON_RULES_COUNT_FORMAT_ERROR,
                'comparisonRulesId' => COMPARISON_RULES_ID_FORMAT_ERROR,
                'dimensionCannotTransformation'=>DIMENSION_CANNOT_TRANSFORMATION,
                'isMaskedCannotTransformation'=>IS_MASKED_CANNOT_TRANSFORMATION,
                'maskRuleCannotTransformation'=>MASK_RULE_CANNOT_TRANSFORMATION,
                'zrrNotIncludeIdentify'=>ZRR_NOT_INCLUDE_IDENTIFY,
                'transformationRulesTransformationNotExist'=>TRANSFORMATION_RULES_TRANSFORMATION_NOT_EXIST,
                'transformationRulesSourceNotExist'=>TRANSFORMATION_RULES_SOURCE_NOT_EXIST,
                'transformationItemTypeNotExist'=>TRANSFORMATION_ITEM_TYPE_NOT_EXIST,
                'typeCannotTransformation'=>TYPE_CANNOT_TRANSFORMATION,
                'lengthCannotTransformation'=>LENGTH_CANNOT_TRANSFORMATION,
                'comparisonRulesBaseCannotIdentify'=>COMPARISON_RULES_BASE_CANNOT_IDENTIFY,
                'comparisonRulesComparisonTemplateItemNotExist'=>COMPARISON_RULES_COMPARISON_TEMPLATE_ITEM_NOT_EXIST,
                'deDuplicationRulesItemsNotExit'=>DE_DUPLICATION_RULES_ITEMS_NOT_EXIT,
                'transformationTemplateId' => TRANSFORMATION_TEMPLATE_FORMAT_ERROR,
                'sourceTemplateId' => SOURCE_TEMPLATE_FORMAT_ERROR,
                'transformationCategory' => TRANSFORMATION_CATEGORY_FORMAT_ERROR,
                'sourceCategory' => SOURCE_CATEGORY_FORMAT_ERROR,
                'crewId' => CREW_ID_FORMAT_ERROR,
                'requiredItemsMapping' => REQUIRED_ITEMS_MAPPING_IS_EXIT,
                'completionRulesTransformationItemNotExist'=>COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
                'completionRulesCompletionTemplateNotExist'=>COMPLETION_RULES_COMPLETION_TEMPLATE_NOT_EXIST,
                'comparisonRulesComparisonTemplateNotExist'=>COMPARISON_RULES_COMPARISON_TEMPLATE_NOT_EXIST,
                'comparisonRulesBaseCannotName'=>COMPARISON_RULES_BASE_CANNOT_NAME,
                'completionRulesCompletionTemplateItemNotExist'=>COMPLETION_RULES_COMPLETION_TEMPLATE_ITEM_NOT_EXIST,
                'comparisonRulesTransformationItemNotExist'=>COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
                'rejectReason' => REASON_FORMAT_ERROR,
                'applyCrewId' => APPLY_CREW_FORMAT_ERROR,
                
            ],
            100=>[
                'crewId'=>PARAMETER_IS_EMPTY,
                'userGroupId'=>DEPARTMENT_USER_GROUP_IS_EMPTY,
                'sourceTemplateId'=>SOURCE_TEMPLATE_IS_EMPTY,
                'transformationTemplateId'=>TRANSFORMATION_TEMPLATE_IS_EMPTY
            ],
            102=>[
                'status'=>STATUS_CAN_NOT_MODIFY,
                'applyStatus'=>STATUS_CAN_NOT_MODIFY
            ],
            103=>[
                'rule'=>RULES_IS_UNIQUE
            ],
        ];
        $this->adapter->expects($this->any())
            ->method('rulesMapErrors')
            ->willReturn($mapError);

        $result = $this->childAdapter->getUnAuditRuleMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
