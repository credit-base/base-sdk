<?php
namespace Base\Sdk\Rule\Adapter;

use PHPUnit\Framework\TestCase;

class RulesMapErrorsTraitTest extends TestCase
{
    use RulesMapErrorsTrait;

    public function testRulesMapErrors()
    {
        $data = [
            100=>[
                'crewId'=>PARAMETER_IS_EMPTY,
                'userGroupId'=>DEPARTMENT_USER_GROUP_IS_EMPTY,
                'sourceTemplateId'=>SOURCE_TEMPLATE_IS_EMPTY,
                'transformationTemplateId'=>TRANSFORMATION_TEMPLATE_IS_EMPTY
            ],
            101 => [
                
                'comparisonRulesCount'=>COMPARISON_RULES_COUNT_FORMAT_ERROR,
                'comparisonRulesId' => COMPARISON_RULES_ID_FORMAT_ERROR,
                'comparisonRulesBase'=>COMPARISON_RULES_BASE_FORMAT_ERROR,
                'comparisonRulesItem'=>COMPARISON_RULES_ITEM_FORMAT_ERROR,
                'comparisonRules'=>COMPARISON_RULES_FORMAT_ERROR,
                'deDuplicationRules'=>DE_DUPLICATION_RULES_FORMAT_ERROR,
                'deDuplicationRulesItems'=>DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR,
                'rules' => RULES_FORMAT_ERROR,
                'rulesName'=>RULES_NAME_NOT_UNIQUE,
                'completionRulesCount'=>COMPLETION_RULES_COUNT_FORMAT_ERROR,
                'completionRulesId' => COMPLETION_RULES_ID_FORMAT_ERROR,
                'completionRulesBase'=>COMPLETION_RULES_BASE_FORMAT_ERROR,
                'completionRulesItem'=>COMPLETION_RULES_ITEM_FORMAT_ERROR,
                'completionRules'=>COMPLETION_RULES_FORMAT_ERROR,
                'deDuplicationRulesResult' => DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR,
                'transformationRulesTransformationNotExist'=>TRANSFORMATION_RULES_TRANSFORMATION_NOT_EXIST,
                'transformationRulesSourceNotExist'=>TRANSFORMATION_RULES_SOURCE_NOT_EXIST,
                'transformationItemTypeNotExist'=>TRANSFORMATION_ITEM_TYPE_NOT_EXIST,
                'typeCannotTransformation'=>TYPE_CANNOT_TRANSFORMATION,
                'completionRulesTransformationItemNotExist'=>COMPLETION_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
                'completionRulesCompletionTemplateNotExist'=>COMPLETION_RULES_COMPLETION_TEMPLATE_NOT_EXIST,
                'completionRulesCompletionTemplateItemNotExist'=>COMPLETION_RULES_COMPLETION_TEMPLATE_ITEM_NOT_EXIST,
                'deDuplicationRulesItemsNotExit'=>DE_DUPLICATION_RULES_ITEMS_NOT_EXIT,
                'transformationTemplateId' => TRANSFORMATION_TEMPLATE_FORMAT_ERROR,
                'sourceTemplateId' => SOURCE_TEMPLATE_FORMAT_ERROR,
                'transformationCategory' => TRANSFORMATION_CATEGORY_FORMAT_ERROR,
                'sourceCategory' => SOURCE_CATEGORY_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
                'applyCrewId' => APPLY_CREW_FORMAT_ERROR,
                'crewId' => CREW_ID_FORMAT_ERROR,
                'requiredItemsMapping' => REQUIRED_ITEMS_MAPPING_IS_EXIT,
                'comparisonRulesTransformationItemNotExist'=>COMPARISON_RULES_TRANSFORMATION_ITEM_NOT_EXIST,
                'comparisonRulesComparisonTemplateNotExist'=>COMPARISON_RULES_COMPARISON_TEMPLATE_NOT_EXIST,
                'comparisonRulesBaseCannotName'=>COMPARISON_RULES_BASE_CANNOT_NAME,
                'lengthCannotTransformation'=>LENGTH_CANNOT_TRANSFORMATION,
                'dimensionCannotTransformation'=>DIMENSION_CANNOT_TRANSFORMATION,
                'isMaskedCannotTransformation'=>IS_MASKED_CANNOT_TRANSFORMATION,
                'maskRuleCannotTransformation'=>MASK_RULE_CANNOT_TRANSFORMATION,
                'zrrNotIncludeIdentify'=>ZRR_NOT_INCLUDE_IDENTIFY,
                'comparisonRulesBaseCannotIdentify'=>COMPARISON_RULES_BASE_CANNOT_IDENTIFY,
                'comparisonRulesComparisonTemplateItemNotExist'=>COMPARISON_RULES_COMPARISON_TEMPLATE_ITEM_NOT_EXIST,
            ],
            102=>[
                'status'=>STATUS_CAN_NOT_MODIFY,
                'applyStatus'=>STATUS_CAN_NOT_MODIFY
            ],
            103=>[
                'rule'=>RULES_IS_UNIQUE
            ],
        ];

        $this->assertEquals($data, $this->rulesMapErrors());
    }
}
