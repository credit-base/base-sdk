<?php
namespace Base\Sdk\Rule\Command;

use PHPUnit\Framework\TestCase;

class OperationCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'rules'=>array(1,2),
            'id'=>$faker->randomNumber()
        );

        $this->command = new OperationCommand(
            $this->fakerData['rules'],
            $this->fakerData['id']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
