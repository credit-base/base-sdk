<?php
namespace Base\Sdk\Rule\Command\Rule;

use PHPUnit\Framework\TestCase;

class AddRuleCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'rules'=>array(1,2),
            'transformationTemplate'=>$faker->randomNumber(),
            'sourceTemplate'=>$faker->randomNumber(),
            'transformationCategory'=>1,
            'sourceCategory'=>2,

        );

        $this->command = new AddRuleCommand(
            $this->fakerData['rules'],
            $this->fakerData['transformationTemplate'],
            $this->fakerData['sourceTemplate'],
            $this->fakerData['transformationCategory'],
            $this->fakerData['sourceCategory']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
