<?php
namespace Base\Sdk\Rule\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Utils\Rule\RuleUtils;

class RuleTranslatorTest extends TestCase
{
    use RuleUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new RuleTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Rule\Model\NullRule', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $rule = \Base\Sdk\Rule\Utils\Rule\MockFactory::generateRule(1);

        $expression = $this->translator->objectToArray($rule);
    
        $this->compareArrayAndObjectRule($expression, $rule);
    }

    public function testObjectToArrayFail()
    {
        $rule = null;

        $expression = $this->translator->objectToArray($rule);
        $this->assertEquals(array(), $expression);
    }
}
