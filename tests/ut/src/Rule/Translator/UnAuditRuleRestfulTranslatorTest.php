<?php
namespace Base\Sdk\Rule\Translator;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\Rule\Utils\UnAuditRule\UnAuditRuleRestfulUtils;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\Translator\GbTemplateRestfulTranslator;
use Base\Sdk\Template\Translator\WbjTemplateRestfulTranslator;

class UnAuditRuleRestfulTranslatorTest extends TestCase
{
    use UnAuditRuleRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditRuleRestfulTranslator();
        $this->childTranslator = new class extends UnAuditRuleRestfulTranslator
        {
           
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule(1);

        $expression['data']['id'] = $unAuditRule->getId();
        $expression['data']['attributes']['rules'] = $unAuditRule->getRules();
        $expression['data']['attributes']['dataTotal'] = $unAuditRule->getDataTotal();
        $expression['data']['attributes']['version'] = $unAuditRule->getVersion();
        $expression['data']['attributes']['transformationCategory'] = $unAuditRule->getTransformationCategory();
        $expression['data']['attributes']['sourceCategory'] = $unAuditRule->getSourceCategory();
        $expression['data']['attributes']['applyStatus'] = $unAuditRule->getApplyStatus();
        $expression['data']['attributes']['rejectReason'] = $unAuditRule->getRejectReason();
        $expression['data']['attributes']['operationType'] = $unAuditRule->getOperationType();
        $expression['data']['attributes']['relationId'] = $unAuditRule->getRelationId();

        $expression['data']['attributes']['createTime'] = $unAuditRule->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $unAuditRule->getUpdateTime();
        $expression['data']['attributes']['status'] = $unAuditRule->getStatus();
        $expression['data']['attributes']['statusTime'] = $unAuditRule->getStatusTime();

        $unAuditRuleObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Rule\Model\UnAuditRule', $unAuditRuleObject);
        $this->compareArrayAndUnAuditRuleCommon($expression, $unAuditRuleObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $unAuditRule = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Rule\Model\NullUnAuditRule', $unAuditRule);
    }

    public function testObjectToArray()
    {
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule(1);

        $expression = $this->translator->objectToArray($unAuditRule);
     
        $this->compareArrayAndUnAuditRuleCommon($expression, $unAuditRule);
    }

    public function testObjectToArrayFail()
    {
        $unAuditRule = null;

        $expression = $this->translator->objectToArray($unAuditRule);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['applyCrew']['data']、
     * $relationships['applyUserGroup']['data']、$relationships['publishCrew']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用3次, 分别用于处理 applyCrew、applyUserGroup 、
     * publishCrew 且返回各自的数组 $applyCrewInfo 、 $applyUserGroupInfo、$publishCrewInfo
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用2次,
     * 入参是 $applyCrewInfo、$publishCrewInfo, 出参是 $applyCrew、publishCrew
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次,
     * 入参是 $applyUserGroupInfo、, 出参是 $applyUserGroup
     * 6. $UnAuditRule->setApplyCrew 调用一次, 入参 $applyCrew
     * 7. $UnAuditRule->setApplyUserGroup 调用一次, 入参 $userGroup
     * 8. $UnAuditRule->setPublishCrew 调用一次, 入参 $publishCrew
     */
    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1,
                'attributes'=>[
                    'transformationCategory'=> 10,
                    'sourceCategory'=> 2
                ]
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'userGroup'=>['data'=>'mockUserGroup'],
            'transformationTemplate'=>['data'=>'mockTransformationTemplate'],
            'sourceTemplate'=>['data'=>'mockSourceTemplate'],
            'publishCrew'=>['data'=>'mockPublishCrew'],
            'applyCrew'=>['data'=>'mockApplyCrew'],
            'applyUserGroup'=>['data'=>'mockApplyUserGroup'],
        ];

        $translator = $this->getMockBuilder(UnAuditRuleRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getCrewRestfulTranslator',
             'getUserGroupRestfulTranslator',
             'getTemplateRestfulTranslatorByCategory',
         ])
        ->getMock();

        $crewInfo = ['mockCrewInfo'];
        $applyCrewInfo = ['mockApplyCrewInfo'];
        $publishCrewInfo = ['mockPublishCrewInfo'];
        $applyUserGroupInfo = ['mockApplyUserGroupInfo'];
        $userGroupInfo = ['mockUserGroupInfo'];
        $sourceTemplateInfo = ['mockSourceTemplateInfo'];
        $transformationTemplateInfo = ['mockTransformationTemplateInfo'];

        $crew = new Crew();
        $publishCrew = new Crew();
        $applyCrew = new Crew();
        $applyUserGroup = new UserGroup();
        $userGroup = new UserGroup();
        $sourceTemplate = new WbjTemplate();
        $transformationTemplate = new GbTemplate();

        //预言
        $translator->expects($this->exactly(2))
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用3次, 依次入参 applyCrew、applyUserGroup 、 publishCrew
        //依次返回 $applyCrewInfo 、 $applyUserGroupInfo、$publishCrewInfo
        $translator->expects($this->exactly(7))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['userGroup']['data']],
                 [$relationships['transformationTemplate']['data']],
                 [$relationships['sourceTemplate']['data']],
                 [$relationships['publishCrew']['data']],
                 [$relationships['applyCrew']['data']],
                 [$relationships['applyUserGroup']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $userGroupInfo,
                $transformationTemplateInfo,
                $sourceTemplateInfo,
                $publishCrewInfo,
                $applyCrewInfo,
                $applyUserGroupInfo
            ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($userGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroup);

        $userGroupRestfulTranslator->arrayToObject(Argument::exact($applyUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyUserGroup);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);
        $crewRestfulTranslator->arrayToObject(Argument::exact($publishCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishCrew);

        $crewRestfulTranslator->arrayToObject(Argument::exact($applyCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyCrew);

        $transformationTemplateRestfulTranslator = $this->prophesize(GbTemplateRestfulTranslator::class);

        $transformationTemplateRestfulTranslator->arrayToObject(Argument::exact($transformationTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($transformationTemplate);
                                    
        $sourceTemplateRestfulTranslator = $this->prophesize(WbjTemplateRestfulTranslator::class);
        $sourceTemplateRestfulTranslator->arrayToObject(Argument::exact($sourceTemplateInfo))
                                                                ->shouldBeCalledTimes(1)
                                                                ->willReturn($sourceTemplate);
                            
        $translator->expects($this->exactly(2))
                                    ->method('getTemplateRestfulTranslatorByCategory')
                                    ->will($this->returnValueMap([
                                        [
                                            10,$transformationTemplateRestfulTranslator->reveal()
                                        ],
                                        [
                                            2,$sourceTemplateRestfulTranslator->reveal()
                                        ]]));
        //绑定
        $translator->expects($this->exactly(2))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(3))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
       
        //揭示
        $unAuditRule = $translator->arrayToObject($expression);
         
        $this->assertInstanceof('Base\Sdk\Rule\Model\UnAuditRule', $unAuditRule);
        $this->assertEquals($userGroup, $unAuditRule->getUserGroup());
        $this->assertEquals($transformationTemplate, $unAuditRule->getTransformationTemplate());
        $this->assertEquals($sourceTemplate, $unAuditRule->getSourceTemplate());
        $this->assertEquals($applyCrew, $unAuditRule->getApplyCrew());
        $this->assertEquals($publishCrew, $unAuditRule->getPublishCrew());
        $this->assertEquals($applyUserGroup, $unAuditRule->getApplyUserGroup());
    }
}
