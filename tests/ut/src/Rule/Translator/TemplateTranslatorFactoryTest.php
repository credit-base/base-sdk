<?php
namespace Base\Sdk\Rule\Translator;

use PHPUnit\Framework\TestCase;

class TemplateTranslatorFactoryTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new TemplateTranslatorFactory();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testNullTranslator()
    {
        $translator = $this->translator->getTemplateTranslator('');
        
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Translator\NullTemplateTranslator',
            $translator
        );
    }

    public function testGetTemplateTranslator()
    {
        foreach (TemplateTranslatorFactory::TRANSLATOR_MAPS as $key => $translator) {
            $this->assertInstanceOf(
                $translator,
                $this->translator->getTemplateTranslator($key)
            );
        }
    }

    public function testNullRestfulTranslator()
    {
        $translator = $this->translator->getTemplateRestfulTranslator('');
        
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Translator\NullTemplateTranslator',
            $translator
        );
    }

    public function testGetRestfulTemplateTranslator()
    {
        foreach (TemplateTranslatorFactory::RESTFUL_TRANSLATOR_MAPS as $key => $translator) {
            $this->assertInstanceOf(
                $translator,
                $this->translator->getTemplateRestfulTranslator($key)
            );
        }
    }
}
