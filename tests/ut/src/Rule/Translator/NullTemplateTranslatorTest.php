<?php
namespace Base\Sdk\Rule\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Model\NullWbjTemplate;

class NullTemplateTranslatorTest extends TestCase
{
    private $nullTemplateTranslator;

    public function setUp()
    {
        $this->nullTemplateTranslator = new NullTemplateTranslator();
    }

    public function tearDown()
    {
        unset($this->nullTemplateTranslator);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullTemplateTranslator
        );
    }

    public function testArrayToObject()
    {

        $result = $this->nullTemplateTranslator->arrayToObject(array());
        $this->assertEquals(new NullWbjTemplate(), $result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testObjectToArray()
    {
        $result = $this->nullTemplateTranslator->objectToArray(new NullWbjTemplate());
        $this->assertEquals(array(), $result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
