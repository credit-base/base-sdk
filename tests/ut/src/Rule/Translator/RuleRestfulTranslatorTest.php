<?php
namespace Base\Sdk\Rule\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\Rule\Utils\Rule\RuleRestfulUtils;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\Translator\GbTemplateRestfulTranslator;
use Base\Sdk\Template\Translator\WbjTemplateRestfulTranslator;

class RuleRestfulTranslatorTest extends TestCase
{
    use RuleRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new RuleRestfulTranslator();

        Core::$container->set('crew', new Crew());
        $this->childTranslator = new class extends RuleRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetDepartmentRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $rule = \Base\Sdk\Rule\Utils\Rule\MockFactory::generateRule(1);

        $expression['data']['id'] = $rule->getId();
        $expression['data']['attributes']['rules'] = $rule->getRules();
        $expression['data']['attributes']['dataTotal'] = $rule->getDataTotal();
        $expression['data']['attributes']['version'] = $rule->getVersion();
        $expression['data']['attributes']['transformationCategory'] = $rule->getTransformationCategory();
        $expression['data']['attributes']['sourceCategory'] = $rule->getSourceCategory();

        $expression['data']['attributes']['createTime'] = $rule->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $rule->getUpdateTime();
        $expression['data']['attributes']['status'] = $rule->getStatus();
        $expression['data']['attributes']['statusTime'] = $rule->getStatusTime();

        $ruleObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Rule\Model\Rule', $ruleObject);
        $this->compareArrayAndObjectCommon($expression, $ruleObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $rule = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Rule\Model\NullRule', $rule);
    }

    public function testObjectToArray()
    {
        $rule = \Base\Sdk\Rule\Utils\Rule\MockFactory::generateRule(1);

        $expression = $this->translator->objectToArray($rule);

        $this->compareArrayAndObjectCommon($expression, $rule);
    }

    public function testObjectToArrayFail()
    {
        $rule = null;

        $expression = $this->translator->objectToArray($rule);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['crew']['data'] 、
     * $relationships['userGroup']['data']、$relationships['sourceTemplate']['data']、
     * $relationships['transformationTemplate']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用4次, 分别用于处理 userGroup 、crew、sourceTemplate、transformationTemplate
     * 且返回各自的数组 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用一次, 入参是 $crewInfo, 出参是 $crew
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $userGroupInfo, 出参是 $userGroup
     * 6. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次, 入参是 $sourceTemplateInfo, 出参是 $sourceTemplate
     * 7. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次,
     * 入参是 $transformationTemplateInfo, 出参是 $transformationTemplate
     * 8. $rule->setCrew 调用一次, 入参 $crew
     * 9. $rule->setUserGroup 调用一次, 入参 $userGroup
     * 10. $rule->setSourceTemplate 调用一次, 入参 $sourceTemplate
     * 11. $rule->setTransformationTemplate 调用一次, 入参 $transformationTemplate
     */
    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1,
                'attributes'=>[
                    'transformationCategory'=> 10,
                    'sourceCategory'=> 2
                ]
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'userGroup'=>['data'=>'mockUserGroup'],
            'transformationTemplate'=>['data'=>'mockTransformationTemplate'],
            'sourceTemplate'=>['data'=>'mockSourceTemplate'],
        ];

        $translator = $this->getMockBuilder(RuleRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getCrewRestfulTranslator',
             'getUserGroupRestfulTranslator',
             'getTemplateRestfulTranslatorByCategory',
         ])
        ->getMock();

        $crewInfo = ['mockCrewInfo'];
        $userGroupInfo = ['mockUserGroupInfo'];
        $sourceTemplateInfo = ['mockSourceTemplateInfo'];
        $transformationTemplateInfo = ['mockTransformationTemplateInfo'];

        $crew = new Crew();
        $userGroup = new UserGroup();
        $sourceTemplate = new WbjTemplate();
        $transformationTemplate = new GbTemplate();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用4次, 依次入参 crew, userGroup,sourceTemplate, transformationTemplate
        //依次返回 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
        $translator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['userGroup']['data']],
                 [$relationships['transformationTemplate']['data']],
                 [$relationships['sourceTemplate']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $userGroupInfo,
                $transformationTemplateInfo,
                $sourceTemplateInfo
            ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($userGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroup);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);
   
        $transformationRestfulTranslator = $this->prophesize(GbTemplateRestfulTranslator::class);

        $transformationRestfulTranslator->arrayToObject(Argument::exact($transformationTemplateInfo))
        ->shouldBeCalledTimes(1)
        ->willReturn($transformationTemplate);
        
        $sourceTemplateRestfulTranslator = $this->prophesize(WbjTemplateRestfulTranslator::class);
        $sourceTemplateRestfulTranslator->arrayToObject(Argument::exact($sourceTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceTemplate);

        $translator->expects($this->exactly(2))
        ->method('getTemplateRestfulTranslatorByCategory')
        ->will($this->returnValueMap([
            [
                10,$transformationRestfulTranslator->reveal()
            ],
            [
                2,$sourceTemplateRestfulTranslator->reveal()
            ]]));
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());

        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        //揭示
        $rule = $translator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\Rule\Model\Rule', $rule);
        $this->assertEquals($crew, $rule->getCrew());
        $this->assertEquals($userGroup, $rule->getUserGroup());
        $this->assertEquals($transformationTemplate, $rule->getTransformationTemplate());
        $this->assertEquals($sourceTemplate, $rule->getSourceTemplate());
    }
}
