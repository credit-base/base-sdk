<?php
namespace Base\Sdk\Rule\Translator;

use PHPUnit\Framework\TestCase;

class TemplateTranslatorTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockTemplateTranslatorTrait::class)
            ->setMethods(['getTemplateRestfulTranslator','getTemplateTranslator'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetTemplateRestfulTranslatorByCategory()
    {
        $type = 1;

        $result = $this->stub->publicGetTemplateRestfulTranslatorByCategory($type);
    
        $this->assertInstanceOf('Base\Sdk\Template\Translator\BjTemplateRestfulTranslator', $result);
    }

    public function testGetTemplateTranslatorByCategory()
    {
        $type = 1;

        $result = $this->stub->publicGetTemplateTranslatorByCategory($type);
       
        $this->assertInstanceOf('Base\Sdk\Template\Translator\BjTemplateTranslator', $result);
    }
}
