<?php
namespace Base\Sdk\Rule\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Utils\UnAuditRule\UnAuditRuleUtils;

class UnAuditRuleTranslatorTest extends TestCase
{
    use UnAuditRuleUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditRuleTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Rule\Model\NullUnAuditRule', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $unAuditRule = \Base\Sdk\Rule\Utils\UnAuditRule\MockFactory::generateUnAuditRule(1);

        $expression = $this->translator->objectToArray($unAuditRule);
    
        $this->compareArrayAndObjectUnAuditRule($expression, $unAuditRule);
    }

    public function testObjectToArrayFail()
    {
        $unAuditRule = null;

        $expression = $this->translator->objectToArray($unAuditRule);
        $this->assertEquals(array(), $expression);
    }
}
