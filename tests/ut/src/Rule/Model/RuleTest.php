<?php
namespace Base\Sdk\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;
use Base\Sdk\Template\Model\Template;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class RuleTest extends TestCase
{
    private $rule;

    public function setUp()
    {
        $this->rule = new MockRule();
    }

    public function tearDown()
    {
        unset($this->rule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf('Base\Sdk\UserGroup\Model\UserGroup', $this->rule->getUserGroup());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->rule->getCrew());
        $this->assertInstanceOf('Base\Sdk\Template\Model\Template', $this->rule->getTransformationTemplate());
        $this->assertInstanceOf('Base\Sdk\Template\Model\Template', $this->rule->getSourceTemplate());
        $this->assertEquals(array(), $this->rule->getRules());
        $this->assertEquals(0, $this->rule->getTransformationCategory());
        $this->assertEquals(0, $this->rule->getSourceCategory());
        $this->assertEquals(0, $this->rule->getVersion());
        $this->assertEquals(0, $this->rule->getDataTotal());
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Repository\RuleRepository',
            $this->rule->getRepository()
        );
    }

    //UserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->rule->setUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->rule->getUserGroup());
    }

    /**
     * 设置 Rule setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->rule->setUserGroup('userGroup');
    }
    //UserGroup 测试 --------------------------------------------------------   end

     //transformationTemplate 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setTransformationTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTransformationTemplateCorrectType()
    {
        $expected = new Template();

        $this->rule->setTransformationTemplate($expected);
        $this->assertEquals($expected, $this->rule->getTransformationTemplate());
    }

    /**
     * 设置 Rule setTransformationTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTransformationTemplateWrongType()
    {
        $this->rule->setTransformationTemplate('string');
    }
    //transformationTemplate 测试 --------------------------------------------------------   end

     //sourceTemplate 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setSourceTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetSourceTemplateCorrectType()
    {
        $expected = new Template();

        $this->rule->setSourceTemplate($expected);
        $this->assertEquals($expected, $this->rule->getSourceTemplate());
    }

    /**
     * 设置 Rule setSourceTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceTemplateWrongType()
    {
        $this->rule->setSourceTemplate('string');
    }
    //sourceTemplate 测试 --------------------------------------------------------   end

    //version 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setVersion() 正确的传参类型,期望传值正确
     */
    public function testSetVersionCorrectType()
    {
        $this->rule->setVersion(1);
        $this->assertEquals(1, $this->rule->getVersion());
    }

    /**
     * 设置 Rule setVersion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVersionWrongType()
    {
        $this->rule->setVersion(['string']);
    }
    //version 测试 --------------------------------------------------------   end

    //transformationCategory 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Rule setTransformationCategory() 是否符合预定范围
     *
     * @dataProvider transformationCategoryProvider
     */
    public function testSetTransformationCategory($actual, $expected)
    {
        $this->rule->setTransformationCategory($actual);
        $this->assertEquals($expected, $this->rule->getTransformationCategory());
    }

    /**
     * 循环测试 Rule setTransformationCategory() 数据构建器
     */
    public function transformationCategoryProvider()
    {
        return array(
            array(IRule::CATEGORY['BJ_RULE'], IRule::CATEGORY['BJ_RULE']),
            array(IRule::CATEGORY['GB_RULE'], IRule::CATEGORY['GB_RULE']),
            array(IRule::CATEGORY['BASE_RULE'], IRule::CATEGORY['BASE_RULE']),
            array(IRule::CATEGORY['WBJ_RULE'], IRule::CATEGORY['WBJ_RULE']),
        );
    }

    /**
     * 设置 Rule setTransformationCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTransformationCategoryWrongType()
    {
        $this->rule->setTransformationCategory(['string']);
    }
    //transformationCategory 测试 ------------------------------------------------------   end
    
    //sourceCategory 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Rule setSourceCategory() 是否符合预定范围
     *
     * @dataProvider sourceCategoryProvider
     */
    public function testSetSourceCategory($actual, $expected)
    {
        $this->rule->setSourceCategory($actual);
        $this->assertEquals($expected, $this->rule->getSourceCategory());
    }

    /**
     * 循环测试 Rule setSourceCategory() 数据构建器
     */
    public function sourceCategoryProvider()
    {
        return array(
            array(IRule::CATEGORY['BJ_RULE'], IRule::CATEGORY['BJ_RULE']),
            array(IRule::CATEGORY['GB_RULE'], IRule::CATEGORY['GB_RULE']),
            array(IRule::CATEGORY['BASE_RULE'], IRule::CATEGORY['BASE_RULE']),
            array(IRule::CATEGORY['WBJ_RULE'], IRule::CATEGORY['WBJ_RULE']),
        );
    }

    /**
     * 设置 Rule setSourceCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceCategoryWrongType()
    {
        $this->rule->setSourceCategory(['string']);
    }
    //sourceCategory 测试 ------------------------------------------------------   end

    //Crew 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $expected = new Crew();

        $this->rule->setCrew($expected);
        $this->assertEquals($expected, $this->rule->getCrew());
    }

    /**
     * 设置 Rule setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->rule->setCrew('Crew');
    }
    //Crew 测试 --------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Rule setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->rule->setStatus($actual);
        $this->assertEquals($expected, $this->rule->getStatus());
    }

    /**
     * 循环测试 Rule setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Rule::STATUS['NORMAL'], Rule::STATUS['NORMAL']),
            array(Rule::STATUS['DELETED'], Rule::STATUS['DELETED'])
        );
    }

    /**
     * 设置 Rule setStatusProvider() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->rule->setStatus(['string']);
    }
    //status 测试 ------------------------------------------------------   end

     //rules 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setRules() 正确的传参类型,期望传值正确
     */
    public function testSetRulesCorrectType()
    {
        $expected = array(1,2);

        $this->rule->setRules($expected);
        $this->assertEquals($expected, $this->rule->getRules());
    }

    /**
     * 设置 Rule setRules() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRulesWrongType()
    {
        $this->rule->setRules('string');
    }
    //rules 测试 --------------------------------------------------------   end

    //dataTotal 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setDataTotal() 正确的传参类型,期望传值正确
     */
    public function testSetDataTotalCorrectType()
    {
        $expected = 100;

        $this->rule->setDataTotal($expected);
        $this->assertEquals($expected, $this->rule->getDataTotal());
    }

    /**
     * 设置 Rule setDataTotal() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataTotalWrongType()
    {
        $this->rule->setDataTotal('string');
    }
    //dataTotal 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Repository\RuleRepository',
            $this->rule->getRepository()
        );
    }

    public function testGetIModifyStatusAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Repository\RuleRepository',
            $this->rule->getIModifyStatusAbleAdapter()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Repository\RuleRepository',
            $this->rule->getIOperateAbleAdapter()
        );
    }

    public function testDeletesSuccess()
    {
        $this->stub = $this->getMockBuilder(MockRule::class)
            ->setMethods(['getIModifyStatusAbleAdapter','isNormal'])->getMock();

        $modifyStatusAdapter = $this->prophesize(IModifyStatusAbleAdapter::class);
        $modifyStatusAdapter->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getIModifyStatusAbleAdapter')
            ->willReturn($modifyStatusAdapter->reveal());
        $this->stub->expects($this->exactly(1))
            ->method('isNormal')
            ->willReturn(true);

        $result = $this->stub->deletes();
        $this->assertTrue($result);
    }

    public function testDeletesFail()
    {
        $this->stub = $this->getMockBuilder(MockRule::class)
            ->setMethods(['isNormal'])->getMock();

        $this->stub->expects($this->exactly(1))
            ->method('isNormal')
            ->willReturn(false);

        $result = $this->stub->deletes();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }
}
