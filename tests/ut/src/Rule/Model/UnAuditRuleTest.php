<?php
namespace Base\Sdk\Rule\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Crew\Model\Crew;

use Base\Sdk\UserGroup\Model\UserGroup;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class UnAuditRuleTest extends TestCase
{
    private $unAuditRule;

    public function setUp()
    {
        $this->unAuditRule = new MockUnAuditRule();
    }

    public function tearDown()
    {
        unset($this->unAuditRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->unAuditRule->getApplyCrew());
        $this->assertInstanceOf('Base\Sdk\UserGroup\Model\UserGroup', $this->unAuditRule->getApplyUserGroup());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->unAuditRule->getPublishCrew());
        $this->assertEquals('', $this->unAuditRule->getRejectReason());
        $this->assertEquals(IApplyAble::APPLY_STATUS['PENDING'], $this->unAuditRule->getApplyStatus());
        $this->assertEquals(0, $this->unAuditRule->getRelationId());
        $this->assertEquals(0, $this->unAuditRule->getVersion());
        $this->assertEquals(0, $this->unAuditRule->getDataTotal());
        $this->assertEquals(0, $this->unAuditRule->getOperationType());
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Repository\UnAuditRuleRepository',
            $this->unAuditRule->getUnAuditRuleRepository()
        );
    }

    //applyUserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setApplyUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetApplyUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->unAuditRule->setApplyUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->unAuditRule->getApplyUserGroup());
    }

    /**
     * 设置 Rule setApplyUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyUserGroupWrongType()
    {
        $this->unAuditRule->setApplyUserGroup('userGroup');
    }
    //applyUserGroup 测试 --------------------------------------------------------   end

     //operationType 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setOperationType() 正确的传参类型,期望传值正确
     */
    public function testSetOperationTypeCorrectType()
    {
        $expected = 1;

        $this->unAuditRule->setOperationType($expected);
        $this->assertEquals($expected, $this->unAuditRule->getOperationType());
    }

    /**
     * 设置 Rule setOperationType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOperationTypeWrongType()
    {
        $this->unAuditRule->setOperationType('string');
    }
    //operationType 测试 --------------------------------------------------------   end

     //rejectReason 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $expected = '驳回原因';

        $this->unAuditRule->setRejectReason($expected);
        $this->assertEquals($expected, $this->unAuditRule->getRejectReason());
    }

    /**
     * 设置 Rule setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->unAuditRule->setRejectReason(['string']);
    }
    //rejectReason 测试 --------------------------------------------------------   end

    //relationId 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setRelationId() 正确的传参类型,期望传值正确
     */
    public function testSetRelationIdCorrectType()
    {
        $this->unAuditRule->setRelationId(1);
        $this->assertEquals(1, $this->unAuditRule->getRelationId());
    }

    /**
     * 设置 Rule setRelationId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRelationIdWrongType()
    {
        $this->unAuditRule->setRelationId(['string']);
    }
    //relationId 测试 --------------------------------------------------------   end

    //applyStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Rule setApplyStatus() 是否符合预定范围
     *
     * @dataProvider applyStatusProvider
     */
    public function testSetApplyStatus($actual, $expected)
    {
        $this->unAuditRule->setApplyStatus($actual);
        $this->assertEquals($expected, $this->unAuditRule->getApplyStatus());
    }

    /**
     * 循环测试 Rule setApplyStatus() 数据构建器
     */
    public function applyStatusProvider()
    {
        return array(
            array(IApplyAble::APPLY_STATUS['NOT_SUBMITTED'], IApplyAble::APPLY_STATUS['NOT_SUBMITTED']),
            array(IApplyAble::APPLY_STATUS['PENDING'], IApplyAble::APPLY_STATUS['PENDING']),
            array(IApplyAble::APPLY_STATUS['APPROVE'], IApplyAble::APPLY_STATUS['APPROVE']),
            array(IApplyAble::APPLY_STATUS['REJECT'], IApplyAble::APPLY_STATUS['REJECT']),
            array(IApplyAble::APPLY_STATUS['REVOKED'], IApplyAble::APPLY_STATUS['REVOKED']),
        );
    }

    /**
     * 设置 Rule setApplyStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyStatusWrongType()
    {
        $this->unAuditRule->setApplyStatus(['string']);
    }
    //applyStatus 测试 ------------------------------------------------------   end

    //applyCrew 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setApplyCrew() 正确的传参类型,期望传值正确
     */
    public function testSetApplyCrewCorrectType()
    {
        $expected = new Crew();

        $this->unAuditRule->setApplyCrew($expected);
        $this->assertEquals($expected, $this->unAuditRule->getApplyCrew());
    }

    /**
     * 设置 Rule setApplyCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyCrewWrongType()
    {
        $this->unAuditRule->setApplyCrew('Crew');
    }
    //applyCrew 测试 --------------------------------------------------------   end

    //publishCrew 测试 -------------------------------------------------------- start
    /**
     * 设置 Rule setPublishCrew() 正确的传参类型,期望传值正确
     */
    public function testSetPublishCrewCorrectType()
    {
        $expected = new Crew();

        $this->unAuditRule->setPublishCrew($expected);
        $this->assertEquals($expected, $this->unAuditRule->getPublishCrew());
    }

    /**
     * 设置 Rule setPublishCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPublishCrewWrongType()
    {
        $this->unAuditRule->setPublishCrew('Crew');
    }
    //publishCrew 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Repository\UnAuditRuleRepository',
            $this->unAuditRule->getUnAuditRuleRepository()
        );
    }

    public function testGetIModifyStatusAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Repository\UnAuditRuleRepository',
            $this->unAuditRule->getIModifyStatusAbleAdapter()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Repository\UnAuditRuleRepository',
            $this->unAuditRule->getIOperateAbleAdapter()
        );
    }

    public function testGetIApplyAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Repository\UnAuditRuleRepository',
            $this->unAuditRule->getIApplyAbleAdapter()
        );
    }
}
