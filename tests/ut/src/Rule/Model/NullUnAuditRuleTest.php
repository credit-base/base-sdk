<?php
namespace Base\Sdk\Rule\Model;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class NullUnAuditRuleTest extends TestCase
{
    private $nullUnAuditRule;

    public function setUp()
    {
        $this->nullUnAuditRule = NullUnAuditRule::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullUnAuditRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsUnAuditRule()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Rule\Model\UnAuditRule',
            $this->nullUnAuditRule
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullUnAuditRule
        );
    }

    public function testResourceNotExist()
    {
        $nullUnAuditRule = new MockNullUnAuditRule();

        $result = $nullUnAuditRule->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
