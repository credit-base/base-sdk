<?php
namespace Base\Sdk\Rule\WidgetRules;

use Base\Sdk\Rule\Model\IRule;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class RuleWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new MockRuleWidgetRules();
    }

    public function tearDown()
    {
        unset($this->widgetRule);
    }

    //templateCategory -- start
    /**
     * @dataProvider invalidTemplateCategoryProvider
     */
    public function testTemplateCategoryInvalid($actual, $expected)
    {
        $result = $this->widgetRule->templateCategory($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(RULES_CATEGORY_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidTemplateCategoryProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $templateCategory = [-1,-2,-3];

        return array(
            array($faker->randomElement($templateCategory), false),
            array($faker->randomElement(IRule::CATEGORY), true),
            array(['string'], false),
        );
    }
    //templateCategory -- end

    //rule
    public function testRulesSuccess()
    {
        $rules = ['ruleName'=>['rules']];

        $this->widgetRule = $this->getMockBuilder(MockRuleWidgetRules::class)
            ->setMethods([
                'ruleName',
                'ruleFormat',
                'validateRule'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                ->method('ruleName')
                ->willReturn(true);

        $this->widgetRule->expects($this->once())
                ->method('ruleFormat')
                ->willReturn(true);

        $this->widgetRule->expects($this->once())
                ->method('validateRule')
                ->with('ruleName', ['rules'])
                ->willReturn(true);

        $result = $this->widgetRule->rules($rules);
        $this->assertTrue($result);
    }

    public function testRulesFailNotArray()
    {
        $rules = 'rules';
        $result = $this->widgetRule->rules($rules);
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'rules'), Core::getLastError()->getSource());
    }

    public function testRulesFail()
    {
        $rules = ['ruleName'=>['rules']];

        $this->widgetRule = $this->getMockBuilder(MockRuleWidgetRules::class)
            ->setMethods([
                'ruleName',
                'ruleFormat',
                'validateRule'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                ->method('ruleName')
                ->willReturn(false);

        $this->widgetRule->expects($this->exactly(0))
                ->method('ruleFormat');

        $this->widgetRule->expects($this->exactly(0))
                ->method('validateRule')
                ->with('ruleName', ['rules']);

        $result = $this->widgetRule->rules($rules);
        $this->assertFalse($result);
    }

    //validateRule
    public function testValidateTransformationRule()
    {
        $rule = ['rule'];

        $result = $this->widgetRule->validateTransformationRule($rule);
        $this->assertTrue($result);
    }

    public function testValidateRuleCompletionRules()
    {
        $name = 'completionRule';
        $rule = ['rule'];

        $this->widgetRule = $this->getMockBuilder(MockRuleWidgetRules::class)
            ->setMethods([
                'validateCompletionRules'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                ->method('validateCompletionRules')
                ->with($rule)
                ->willReturn(true);

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertTrue($result);
    }

    public function testValidateRuleComparisonRules()
    {
        $name = 'comparisonRule';
        $rule = ['rule'];

        $this->widgetRule = $this->getMockBuilder(MockRuleWidgetRules::class)
            ->setMethods([
                'validateComparisonRules'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                ->method('validateComparisonRules')
                ->with($rule)
                ->willReturn(true);

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertTrue($result);
    }

    public function testValidateDeDuplicationRules()
    {
        $name = 'deDuplicationRule';
        $rule = ['rule'];

        $this->widgetRule = $this->getMockBuilder(MockRuleWidgetRules::class)
            ->setMethods([
                'validateDeDuplicationRules'
            ])
            ->getMock();

        $this->widgetRule->expects($this->once())
                ->method('validateDeDuplicationRules')
                ->with($rule)
                ->willReturn(true);

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertTrue($result);
    }
    public function testValidateRuleFalse()
    {
        $name = 'name';
        $rule = ['rule'];

        $result = $this->widgetRule->validateRule($name, $rule);
        $this->assertFalse($result);
    }

    //ruleName
    public function testRuleNameSuccess()
    {
        $name = 'transformationRule';
        $result = $this->widgetRule->ruleName($name);
        $this->assertTrue($result);
    }

    public function testRuleNameFail()
    {
        $name = 'name';
        $result = $this->widgetRule->ruleName($name);
        $this->assertFalse($result);
        $this->assertEquals(RULES_NAME_NOT_UNIQUE, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'rulesName'), Core::getLastError()->getSource());
    }

    //ruleFormat
    public function testRuleFormatSuccess()
    {
        $rule = ['rule'];
        $result = $this->widgetRule->ruleFormat($rule);
        $this->assertTrue($result);
    }

    public function testRuleFormatFail()
    {
        $rule = 'rule';
        $result = $this->widgetRule->ruleFormat($rule);
        $this->assertFalse($result);
        $this->assertEquals(RULES_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'rules'), Core::getLastError()->getSource());
    }

    //validateCompletionAndComparisonSuccess
    private function validateCompletionAndComparisonSuccess($function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];
        $result = $this->widgetRule->$function($rules);
        $this->assertTrue($result);
    }

    public function testValidateCompletionRulesSuccess()
    {
        $this->validateCompletionAndComparisonSuccess('validateCompletionRules');
    }

    public function testValidateComparisonRulesSuccess()
    {
        $this->validateCompletionAndComparisonSuccess('validateComparisonRules');
    }

    private function prepareCompletionAndComparisonRulesFail($rules, $pointer, $function)
    {
        $result = $this->widgetRule->$function($rules);
        $this->assertFalse($result);
        $this->assertEquals(array('pointer' => $pointer), Core::getLastError()->getSource());
    }

    //validateCompletionAndComparisonRulesFailNotArray
    private function validateCompletionAndComparisonRulesFailNotArray($pointer, $function)
    {
        $rules = [
        'ZTMC' => 'rule',
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailNotArray('completionRules', 'validateCompletionRules');
    }

    public function testValidateComparisonRulesFailNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailNotArray('comparisonRules', 'validateComparisonRules');
    }

    //validateCompletionAndComparisonRulesFailMaxRules
    private function validateCompletionAndComparisonRulesFailMaxRules($pointer, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                array('id'=>3, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            ),
            'TEST' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailMaxRules()
    {
        $this->validateCompletionAndComparisonRulesFailMaxRules('completionRulesCount', 'validateCompletionRules');
    }

    public function testValidateComparisonRulesFailMaxRules()
    {
        $this->validateCompletionAndComparisonRulesFailMaxRules('comparisonRulesCount', 'validateComparisonRules');
    }

    //validateCompletionAndComparisonRulesFailSetKey
    private function validateCompletionAndComparisonRulesFailSetKey($pointer, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailNotSetKey()
    {
        $this->validateCompletionAndComparisonRulesFailSetKey('completionRules', 'validateCompletionRules');
    }

    public function testValidateComparisonRulesFailNotSetKey()
    {
        $this->validateCompletionAndComparisonRulesFailSetKey('comparisonRules', 'validateComparisonRules');
    }

    //validateCompletionAndComparisonRulesFailIdNotNumber
    private function validateCompletionAndComparisonRulesFailIdNotNumber($pointer, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>'id', 'base'=> array(1,2), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailIdNotNumber()
    {
        $this->validateCompletionAndComparisonRulesFailIdNotNumber('completionRulesId', 'validateCompletionRules');
    }

    public function testValidateComparisonRulesFailIdNotNumber()
    {
        $this->validateCompletionAndComparisonRulesFailIdNotNumber('comparisonRulesId', 'validateComparisonRules');
    }

    //validateCompletionAndComparisonRulesFailIdBaseNotArray
    private function validateCompletionAndComparisonRulesFailIdBaseNotArray($pointer, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> 'base', 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailBaseNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailIdBaseNotArray('completionRulesBase', 'validateCompletionRules');
    }

    public function testValidateComparisonRulesFailBaseNotArray()
    {
        $this->validateCompletionAndComparisonRulesFailIdBaseNotArray('comparisonRulesBase', 'validateComparisonRules');
    }

    //validateCompletionAndComparisonRulesFailIdItemNotString
    private function validateCompletionAndComparisonRulesFailIdItemNotString($pointer, $function)
    {
        $rules = [
        'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>['ZTMC']),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailItemNotString()
    {
        $this->validateCompletionAndComparisonRulesFailIdItemNotString(
            'completionRulesItem',
            'validateCompletionRules'
        );
    }

    public function testValidateComparisonRulesFailItemNotString()
    {
        $this->validateCompletionAndComparisonRulesFailIdItemNotString(
            'comparisonRulesItem',
            'validateComparisonRules'
        );
    }

    //validateCompletionAndComparisonRulesFailBaseOutOfRange
    private function validateCompletionAndComparisonRulesFailBaseOutOfRange($pointer, $function)
    {
        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,3), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];

        $this->prepareCompletionAndComparisonRulesFail($rules, $pointer, $function);
    }

    public function testValidateCompletionRulesFailBaseOutOfRange()
    {
        $this->validateCompletionAndComparisonRulesFailBaseOutOfRange('completionRulesBase', 'validateCompletionRules');
    }

    public function testValidateComparisonRulesFailBaseOutOfRange()
    {
        $this->validateCompletionAndComparisonRulesFailBaseOutOfRange('comparisonRulesBase', 'validateComparisonRules');
    }

    //validateDeDuplicationRules
    public function testValidateDeDuplicationRulesSuccess()
    {
        $rules = [
            'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC", "TYSHXYDM"))
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertTrue($result);
    }

    public function testValidateDeDuplicationRulesFailNotSetResult()
    {
        $rules = [
            'deDuplicationRule' => array("items"=>array("ZTMC", "TYSHXYDM"))
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertFalse($result);
        $this->assertEquals(DE_DUPLICATION_RULES_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'deDuplicationRules'), Core::getLastError()->getSource());
    }

    public function testValidateDeDuplicationRulesFailItemsNotArray()
    {
        $rules = [
            'deDuplicationRule' => array("result"=>1, "items"=>"items")
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertFalse($result);
        $this->assertEquals(DE_DUPLICATION_RULES_ITEMS_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'deDuplicationRulesItems'), Core::getLastError()->getSource());
    }

    public function testValidateDeDuplicationRulesFailResultOtOfRange()
    {
        $rules = [
            'deDuplicationRule' => array("result"=>3, "items"=>array("ZTMC", "TYSHXYDM"))
        ];

        $result = $this->widgetRule->validateDeDuplicationRules($rules['deDuplicationRule']);
        $this->assertFalse($result);
        $this->assertEquals(DE_DUPLICATION_RULES_RESULT_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'deDuplicationRulesResult'), Core::getLastError()->getSource());
    }
}
