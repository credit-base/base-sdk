<?php
namespace Base\Sdk\Rule\Utils\UnAuditRule;

use Base\Sdk\Rule\Model\UnAuditRule;
use Base\Sdk\Rule\Model\IRule;

class MockFactory
{
    public static function generateUnAuditRule(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditRule {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditRule = new UnAuditRule($id);
        $unAuditRule->setId($id);

        self::generateTransformationTemplate($unAuditRule, $faker, $value);
        self::generateSourceTemplate($unAuditRule, $faker, $value);
        self::generateRules($unAuditRule, $faker, $value);
        self::generateVersion($unAuditRule, $faker, $value);
        self::generateDataTotal($unAuditRule, $faker, $value);
        self::generateCrew($unAuditRule, $faker, $value);
        self::generateUserGroup($unAuditRule, $faker, $value);
        self::generateRejectReason($unAuditRule, $faker, $value);
        self::generateOperationType($unAuditRule, $faker, $value);
        self::generateApplyCrew($unAuditRule, $faker, $value);
        self::generateApplyUserGroup($unAuditRule, $faker, $value);
        self::generateRelationId($unAuditRule, $faker, $value);
        self::generatePublishCrew($unAuditRule, $faker, $value);
        self::generateApplyStatus($unAuditRule, $faker, $value);
        self::generateTransformationCategory($unAuditRule, $faker, $value);
        self::generateSourceCategory($unAuditRule, $faker, $value);

        return $unAuditRule;
    }

    protected static function generateCrew($unAuditRule, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        unset($faker);
        $unAuditRule->setCrew($crew);
    }

    protected static function generateUserGroup($unAuditRule, $faker, $value) : void
    {
        $userGroup = isset($value['userGroup']) ?
        $value['userGroup'] :
        \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($faker->randomDigit());
        
        $unAuditRule->setUserGroup($userGroup);
    }

    private static function generateTransformationTemplate($unAuditRule, $faker, $value) : void
    {
        $transformationTemplate = isset($value['transformationTemplate']) ?
        $value['transformationTemplate'] :
        \Base\Sdk\Template\Utils\GbTemplate\MockGbTemplateFactory::generateGbTemplate($faker->randomDigit());
        
        $unAuditRule->setTransformationTemplate($transformationTemplate);
    }

    private static function generateSourceTemplate($unAuditRule, $faker, $value) : void
    {
        $sourceTemplate = isset($value['sourceTemplate']) ?
        $value['sourceTemplate'] :
        \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate($faker->randomDigit());
        
        $unAuditRule->setSourceTemplate($sourceTemplate);
    }

    private static function generateSourceCategory($rule, $faker, $value) : void
    {
        unset($faker);
        $sourceCategory = isset($value['sourceCategory']) ?
        $value['sourceCategory'] :
        IRule::CATEGORY['WBJ_RULE'];
        $rule->setSourceCategory($sourceCategory);
    }

    private static function generateRules($unAuditRule, $faker, $value) : void
    {
        unset($faker);
        $rules = isset($value['rules']) ?
        $value['rules'] :
        array(1,2);
        
        $unAuditRule->setRules($rules);
    }

    private static function generateVersion($unAuditRule, $faker, $value) : void
    {
        $version = isset($value['version']) ?
        $value['version'] :
        $faker->randomDigit;
        
        $unAuditRule->setVersion($version);
    }

    private static function generateDataTotal($unAuditRule, $faker, $value) : void
    {
        $dataTotal = isset($value['dataTotal']) ?
        $value['dataTotal'] :
        $faker->randomDigit();
        
        $unAuditRule->setDataTotal($dataTotal);
    }

    protected static function generateApplyCrew($unAuditRule, $faker, $value) : void
    {
        $applyCrew = isset($value['applyCrew']) ?
        $value['applyCrew'] :
        \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        unset($faker);
        $unAuditRule->setApplyCrew($applyCrew);
    }

    protected static function generatePublishCrew($unAuditRule, $faker, $value) : void
    {
        $publishCrew = isset($value['publishCrew']) ?
        $value['publishCrew'] :
        \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        unset($faker);
        $unAuditRule->setPublishCrew($publishCrew);
    }

    protected static function generateApplyUserGroup($unAuditRule, $faker, $value) : void
    {
        $applyUserGroup = isset($value['applyUserGroup']) ?
        $value['applyUserGroup'] :
        \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($faker->randomDigit());
        
        $unAuditRule->setApplyUserGroup($applyUserGroup);
    }

    protected static function generateOperationType($unAuditRule, $faker, $value) : void
    {
        $operationType = isset($value['operationType']) ?
            $value['operationType'] :
            $faker->randomElement(
                UnAuditRule::OPERATION_TYPE
            );
        $unAuditRule->setOperationType($operationType);
    }

    protected static function generateApplyStatus($unAuditRule, $faker, $value) : void
    {
        $applyStatus = isset($value['applyStatus']) ?
            $value['applyStatus'] :
            $faker->randomElement(
                UnAuditRule::APPLY_STATUS
            );
        $unAuditRule->setApplyStatus($applyStatus);
    }

    private static function generateRejectReason($unAuditRule, $faker, $value) : void
    {
        $rejectReason = isset($value['rejectReason']) ?
        $value['rejectReason'] :
        $faker->word;
        
        $unAuditRule->setRejectReason($rejectReason);
    }

    private static function generateRelationId($unAuditRule, $faker, $value) : void
    {
        $relationId = isset($value['relationId']) ?
        $value['relationId'] :
        $faker->randomDigit();
        
        $unAuditRule->setRelationId($relationId);
    }

    private static function generateTransformationCategory($rule, $faker, $value) : void
    {
        unset($faker);
        $transformationCategory = isset($value['transformationCategory'])
            ? $value['transformationCategory']
            : IRule::CATEGORY['GB_RULE'];
        
        $rule->setTransformationCategory($transformationCategory);
    }
}
