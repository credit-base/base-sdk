<?php
namespace Base\Sdk\Rule\Utils\UnAuditRule;

trait UnAuditRuleRestfulUtils
{
    private function compareArrayAndUnAuditRuleCommon(
        array $expectedArray,
        $unAuditRule
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $unAuditRule->getId());
        }
        
        if (isset($expectedArray['data']['attributes']['applyStatus'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['applyStatus'],
                $unAuditRule->getApplyStatus()
            );
        }

        $this->assertEquals(
            $expectedArray['data']['attributes']['rejectReason'],
            $unAuditRule->getRejectReason()
        );

        if (isset($expectedArray['data']['attributes']['operationType'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['operationType'],
                $unAuditRule->getOperationType()
            );
        }

        $this->assertEquals($expectedArray['data']['attributes']['rules'], $unAuditRule->getRules());
        $this->assertEquals(
            $expectedArray['data']['attributes']['transformationCategory'],
            $unAuditRule->getTransformationCategory()
        );

        if (isset($expectedArray['data']['attributes']['relationId'])) {
            $this->assertEquals($expectedArray['data']['attributes']['relationId'], $unAuditRule->getRelationId());
        }

        if (isset($expectedArray['data']['attributes']['dataTotal'])) {
            $this->assertEquals($expectedArray['data']['attributes']['dataTotal'], $unAuditRule->getDataTotal());
        }

        if (isset($expectedArray['data']['attributes']['version'])) {
            $this->assertEquals($expectedArray['data']['attributes']['version'], $unAuditRule->getVersion());
        }
    }
}
