<?php
namespace Base\Sdk\Rule\Utils\UnAuditRule;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Crew\Translator\CrewTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;
use Base\Sdk\Rule\Translator\RuleTranslator;

use Base\Sdk\Template\Translator\GbTemplateTranslator;
use Base\Sdk\Template\Translator\WbjTemplateTranslator;

trait UnAuditRuleUtils
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getWbjTemplateTranslator() : WbjTemplateTranslator
    {
        return new WbjTemplateTranslator();
    }

    protected function getGbTemplateTranslator() : GbTemplateTranslator
    {
        return new GbTemplateTranslator();
    }

    private function compareArrayAndObjectUnAuditRule(
        array $expectedArray,
        $unAuditRule
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditRule);
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $unAuditRule
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($unAuditRule->getId()));
        $this->assertEquals($expectedArray['rules'], $unAuditRule->getRules());
        $this->assertEquals($expectedArray['dataTotal'], $unAuditRule->getDataTotal());
        $this->assertEquals(
            $expectedArray['version'],
            $unAuditRule->getVersion()
        );
        $this->assertEquals(
            $expectedArray['transformationCategory']['id'],
            marmot_encode($unAuditRule->getTransformationCategory())
        );
        $this->assertEquals(
            $expectedArray['transformationCategory']['name'],
            RuleTranslator::CATEGORY_CN[$unAuditRule->getTransformationCategory()]
        );
        $this->assertEquals(
            $expectedArray['sourceCategory']['id'],
            marmot_encode($unAuditRule->getSourceCategory())
        );
        $this->assertEquals(
            $expectedArray['sourceCategory']['name'],
            RuleTranslator::CATEGORY_CN[$unAuditRule->getSourceCategory()]
        );
        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($unAuditRule->getCrew())
        );
        $this->assertEquals(
            $expectedArray['userGroup'],
            $this->getUserGroupTranslator()->objectToArray($unAuditRule->getUserGroup())
        );
        $this->assertEquals(
            $expectedArray['sourceTemplate'],
            $this->getWbjTemplateTranslator()->objectToArray($unAuditRule->getSourceTemplate())
        );
        $this->assertEquals(
            $expectedArray['transformationTemplate'],
            $this->getGbTemplateTranslator()->objectToArray($unAuditRule->getTransformationTemplate())
        );
        $this->assertEquals(
            $expectedArray['operationType']['id'],
            marmot_encode($unAuditRule->getOperationType())
        );
        $this->assertEquals(
            $expectedArray['operationType']['name'],
            IApplyCategory::OPERATION_TYPE_CN[$unAuditRule->getOperationType()]
        );
        $this->assertEquals(
            $expectedArray['rejectReason'],
            $unAuditRule->getRejectReason()
        );
        $this->assertEquals(
            $expectedArray['relationId'],
            $unAuditRule->getRelationId()
        );
        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($unAuditRule->getCrew())
        );
        $this->assertEquals(
            $expectedArray['applyCrew'],
            $this->getCrewTranslator()->objectToArray($unAuditRule->getApplyCrew())
        );
        $this->assertEquals(
            $expectedArray['applyUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($unAuditRule->getApplyUserGroup())
        );
        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($unAuditRule->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            IApplyCategory::STATUS_CN[$unAuditRule->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            IApplyCategory::STATUS_TAG_TYPE[$unAuditRule->getStatus()]
        );

        $this->assertEquals(
            $expectedArray['applyStatus']['id'],
            marmot_encode($unAuditRule->getApplyStatus())
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['name'],
            IApplyCategory::APPLY_STATUS_CN[$unAuditRule->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['type'],
            IApplyCategory::APPLY_STATUS_TAG_TYPE[$unAuditRule->getApplyStatus()]
        );

        $this->assertEquals(
            $expectedArray['updateTime'],
            $unAuditRule->getUpdateTime()
        );
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $unAuditRule->getUpdateTime())
        );
                    
        $this->assertEquals(
            $expectedArray['statusTime'],
            $unAuditRule->getStatusTime()
        );
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $unAuditRule->getStatusTime())
        );
                    
        $this->assertEquals(
            $expectedArray['createTime'],
            $unAuditRule->getCreateTime()
        );
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $unAuditRule->getCreateTime())
        );
    }
}
