<?php
namespace Base\Sdk\Rule\Utils\Rule;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Crew\Translator\CrewTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;
use Base\Sdk\Rule\Translator\RuleTranslator;

use Base\Sdk\Template\Translator\GbTemplateTranslator;
use Base\Sdk\Template\Translator\WbjTemplateTranslator;

trait RuleUtils
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getWbjTemplateTranslator() : WbjTemplateTranslator
    {
        return new WbjTemplateTranslator();
    }

    protected function getGbTemplateTranslator() : GbTemplateTranslator
    {
        return new GbTemplateTranslator();
    }

    private function compareArrayAndObjectRule(
        array $expectedArray,
        $rule
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $rule);
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $rule
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($rule->getId()));
        $this->assertEquals($expectedArray['rules'], $rule->getRules());
        $this->assertEquals($expectedArray['dataTotal'], $rule->getDataTotal());
        $this->assertEquals(
            $expectedArray['version'],
            $rule->getVersion()
        );
        $this->assertEquals(
            $expectedArray['transformationCategory']['id'],
            marmot_encode($rule->getTransformationCategory())
        );
        $this->assertEquals(
            $expectedArray['transformationCategory']['name'],
            RuleTranslator::CATEGORY_CN[$rule->getTransformationCategory()]
        );
        $this->assertEquals(
            $expectedArray['sourceCategory']['id'],
            marmot_encode($rule->getSourceCategory())
        );
        $this->assertEquals(
            $expectedArray['sourceCategory']['name'],
            RuleTranslator::CATEGORY_CN[$rule->getSourceCategory()]
        );
        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($rule->getCrew())
        );
        $this->assertEquals(
            $expectedArray['userGroup'],
            $this->getUserGroupTranslator()->objectToArray($rule->getUserGroup())
        );
        $this->assertEquals(
            $expectedArray['sourceTemplate'],
            $this->getWbjTemplateTranslator()->objectToArray($rule->getSourceTemplate())
        );
        $this->assertEquals(
            $expectedArray['transformationTemplate'],
            $this->getGbTemplateTranslator()->objectToArray($rule->getTransformationTemplate())
        );

        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($rule->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            IApplyCategory::STATUS_CN[$rule->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            IApplyCategory::STATUS_TAG_TYPE[$rule->getStatus()]
        );

        $this->assertEquals(
            $expectedArray['updateTime'],
            $rule->getUpdateTime()
        );
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $rule->getUpdateTime())
        );
                    
        $this->assertEquals(
            $expectedArray['statusTime'],
            $rule->getStatusTime()
        );
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $rule->getStatusTime())
        );
                    
        $this->assertEquals(
            $expectedArray['createTime'],
            $rule->getCreateTime()
        );
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $rule->getCreateTime())
        );
    }
}
