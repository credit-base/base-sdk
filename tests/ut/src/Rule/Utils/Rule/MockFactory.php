<?php
namespace Base\Sdk\Rule\Utils\Rule;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Model\IRule;

class MockFactory
{
    public static function generateRule(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Rule {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $rule = new Rule($id);
        $rule->setId($id);

        self::generateTransformationTemplate($rule, $faker, $value);
        self::generateSourceTemplate($rule, $faker, $value);
        self::generateTransformationCategory($rule, $faker, $value);
        self::generateSourceCategory($rule, $faker, $value);
        self::generateRules($rule, $faker, $value);
        self::generateVersion($rule, $faker, $value);
        self::generateDataTotal($rule, $faker, $value);
        self::generateCrew($rule, $faker, $value);
        self::generateUserGroup($rule, $faker, $value);

        return $rule;
    }

    protected static function generateCrew($rule, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        unset($faker);
        $rule->setCrew($crew);
    }

    protected static function generateUserGroup($rule, $faker, $value) : void
    {
        $userGroup = isset($value['userGroup']) ?
        $value['userGroup'] :
        \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($faker->randomDigit());
        
        $rule->setUserGroup($userGroup);
    }

    private static function generateTransformationTemplate($rule, $faker, $value) : void
    {
        $transformationTemplate = isset($value['transformationTemplate']) ?
        $value['transformationTemplate'] :
        \Base\Sdk\Template\Utils\GbTemplate\MockGbTemplateFactory::generateGbTemplate($faker->randomDigit());
        
        $rule->setTransformationTemplate($transformationTemplate);
    }

    private static function generateSourceTemplate($rule, $faker, $value) : void
    {
        $sourceTemplate = isset($value['sourceTemplate']) ?
        $value['sourceTemplate'] :
        \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate($faker->randomDigit());
        
        $rule->setSourceTemplate($sourceTemplate);
    }

    private static function generateTransformationCategory($rule, $faker, $value) : void
    {
        unset($faker);
        $transformationCategory = isset($value['transformationCategory'])
            ? $value['transformationCategory']
            :IRule::CATEGORY['GB_RULE'];
        
        $rule->setTransformationCategory($transformationCategory);
    }

    private static function generateRules($rule, $faker, $value) : void
    {
        unset($faker);
        $rules = isset($value['rules']) ?
        $value['rules'] :
        array(1,2);
        
        $rule->setRules($rules);
    }

    private static function generateVersion($rule, $faker, $value) : void
    {
        $version = isset($value['version']) ?
        $value['version'] :
        $faker->randomDigit;
        
        $rule->setVersion($version);
    }

    private static function generateDataTotal($rule, $faker, $value) : void
    {
        $dataTotal = isset($value['dataTotal']) ?
        $value['dataTotal'] :
        $faker->randomDigit();
        
        $rule->setDataTotal($dataTotal);
    }

    private static function generateSourceCategory($rule, $faker, $value) : void
    {
        unset($faker);
        $sourceCategory = isset($value['sourceCategory']) ?
        $value['sourceCategory'] :
        IRule::CATEGORY['WBJ_RULE'];
        $rule->setSourceCategory($sourceCategory);
    }
}
