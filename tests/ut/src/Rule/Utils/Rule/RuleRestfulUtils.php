<?php
namespace Base\Sdk\Rule\Utils\Rule;

trait RuleRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $object->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['rules'], $object->getRules());
        $this->assertEquals(
            $expectedArray['data']['attributes']['transformationCategory'],
            $object->getTransformationCategory()
        );

        if (isset($expectedArray['data']['attributes']['dataTotal'])) {
            $this->assertEquals($expectedArray['data']['attributes']['dataTotal'], $object->getDataTotal());
        }

        if (isset($expectedArray['data']['attributes']['version'])) {
            $this->assertEquals($expectedArray['data']['attributes']['version'], $object->getVersion());
        }
    }
}
