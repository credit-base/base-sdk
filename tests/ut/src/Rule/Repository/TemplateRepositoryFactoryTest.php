<?php
namespace Base\Sdk\Rule\Repository;

use PHPUnit\Framework\TestCase;

class TemplateRepositoryFactoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = new TemplateRepositoryFactory();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testNullRepository()
    {
        $repository = $this->repository->getTemplateRepository('');
        
        $this->assertFalse($repository);
    }

    public function testGetTemplateRepository()
    {
        foreach (TemplateRepositoryFactory::REPOSITORY_MAPS as $key => $repository) {
            $this->assertInstanceOf(
                $repository,
                $this->repository->getTemplateRepository($key)
            );
        }
    }
}
