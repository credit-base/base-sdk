<?php
namespace Base\Sdk\User\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\User\Utils\UserRestfulUtils;
use Base\Sdk\User\Model\User;

class UserRestfulTranslatorTest extends TestCase
{
    use UserRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder('Base\Sdk\User\Translator\UserRestfulTranslator')
                                 ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $user = \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);

        $expression['data']['id'] = $user->getId();
        $expression['data']['attributes']['cellphone'] = $user->getCellphone();
        $expression['data']['attributes']['realName'] = $user->getRealName();
        $expression['data']['attributes']['userName'] = $user->getUserName();
        $expression['data']['attributes']['password'] = $user->getPassword();
        $expression['data']['attributes']['oldPassword'] = $user->getOldPassword();
        $expression['data']['attributes']['cardId'] = $user->getCardId();
        $expression['data']['attributes']['avatar'] = $user->getAvatar();
        $expression['data']['attributes']['gender'] = $user->getGender();
        $expression['data']['attributes']['createTime'] = $user->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $user->getUpdateTime();
        $expression['data']['attributes']['status'] = $user->getStatus();
        $expression['data']['attributes']['statusTime'] = $user->getStatusTime();

        $userObject = $this->translator->arrayToObject($expression, $user);
        $this->assertInstanceof('Base\Sdk\User\Model\User', $userObject);
        $this->compareArrayAndObject($expression, $userObject);
    }

    public function testObjectToArray()
    {
        $user = \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);

        $expression = $this->translator->objectToArray($user);

        $this->compareArrayAndObject($expression, $user);
    }

    public function testObjectToArrayFail()
    {
        $user = null;

        $expression = $this->translator->objectToArray($user);
        $this->assertEquals(array(), $expression);
    }
}
