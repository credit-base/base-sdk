<?php

namespace Base\Sdk\Statistical\Repository\Statistical;

use Base\Sdk\Statistical\Adapter\Statistical\IAdapterFactory;
use Base\Sdk\Statistical\Model\Statistical;
use PHPUnit\Framework\TestCase;
use Base\Sdk\Statistical\Adapter\Statistical\StaticsEnterpriseRelationInformationCountAdapter;
use Prophecy\Argument;

class StatisticalRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = new MockStatisticalRepository();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testSetAdapter()
    {
        $adapter = IAdapterFactory::MAPS['enterpriseRelationInformationCount'];
        $adapter = new $adapter;
        $this->repository->setAdapter($adapter);
        $this->assertInstanceOf(
            'Base\Sdk\Statistical\Adapter\Statistical\StaticsEnterpriseRelationInformationCountAdapter',
            $this->repository->getAdapter()
        );
    }

    public function testAnalyse()
    {
        $this->repository = $this->getMockBuilder(MockStatisticalRepository::class)->setMethods([
            'getAdapter'
        ])->getMock();

        $filter = [];

        $adapter = $this->prophesize(StaticsEnterpriseRelationInformationCountAdapter::class);
        $adapter->analyse(Argument::exact($filter))->shouldBeCalledTimes(1)
            ->willReturn(new Statistical());

        $this->repository->expects($this->exactly(1))
            ->method('getAdapter')->willReturn($adapter->reveal());

        $this->assertInstanceOf(
            'Base\Sdk\Statistical\Model\Statistical',
            $this->repository->analyse($filter)
        );
    }

    public function testAnalyseAsync()
    {
        $this->repository = $this->getMockBuilder(MockStatisticalRepository::class)->setMethods([
            'getAdapter'
        ])->getMock();

        $filter = [
            'id' => 1
        ];

        $adapter = $this->prophesize(StaticsEnterpriseRelationInformationCountAdapter::class);
        $adapter->analyseAsync(Argument::exact($filter))->shouldBeCalledTimes(1)
            ->willReturn(new Statistical());

        $this->repository->expects($this->exactly(1))
            ->method('getAdapter')->willReturn($adapter->reveal());

        $this->assertInstanceOf(
            'Base\Sdk\Statistical\Model\Statistical',
            $this->repository->analyseAsync($filter)
        );
    }
}
