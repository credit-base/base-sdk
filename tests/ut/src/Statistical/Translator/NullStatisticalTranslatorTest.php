<?php
namespace Base\Sdk\Statistical\Translator;

use Base\Sdk\Statistical\Model\Statistical;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullStatisticalTranslatorTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new NullStatisticalTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjects()
    {
        $this->assertEquals(array(), $this->translator->arrayToObjects(array()));
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testArrayToObject()
    {
        $this->translator->arrayToObject(array());
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testObjectToArray()
    {
        $this->translator->objectToArray(new Statistical());
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
