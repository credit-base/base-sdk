<?php
namespace Base\Sdk\Statistical\Translator;

use Base\Sdk\Statistical\Model\Statistical;
use PHPUnit\Framework\TestCase;

class StatisticalRestfulTranslatorTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new MockStatisticalRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $this->translator = $this->getMockBuilder(MockStatisticalRestfulTranslator::class)
            ->setMethods(['translateToObject'])->getMock();
        $expression = array();

        $this->translator->expects($this->exactly(1))
            ->method('translateToObject')
            ->willReturn(new Statistical());

        $this->assertInstanceOf(
            'Base\Sdk\Statistical\Model\Statistical',
            $this->translator->arrayToObject($expression)
        );
    }

    public function testTranslateToObjectEmpty()
    {
        $expression = array();
        $this->assertInstanceOf(
            'Base\Sdk\Statistical\Model\NullStatistical',
            $this->translator->translateToObject($expression)
        );
    }

    public function testTranslateToObject()
    {
        $this->translator = new MockStatisticalRestfulTranslator();
        $expression = array(
            'data' => [
                'id' => 1,
                'attributes' => [
                    'result' => []
                ]
            ]
        );

        $result = $this->translator->translateToObject($expression);

        $this->assertEquals(
            $expression['data']['attributes']['result'],
            $result->getResult()
        );
        $this->assertEquals(
            $expression['data']['id'],
            $result->getId()
        );
        $this->assertInstanceOf(
            'Base\Sdk\Statistical\Model\Statistical',
            $result
        );
    }

    public function testObjectToArray()
    {
        $this->assertEquals(
            array(),
            $this->translator->objectToArray(new Statistical())
        );
    }
}
