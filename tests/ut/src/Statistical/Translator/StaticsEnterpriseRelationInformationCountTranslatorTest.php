<?php
namespace Base\Sdk\Statistical\Translator;

use Base\Sdk\Statistical\Model\Statistical;
use PHPUnit\Framework\TestCase;

class StaticsEnterpriseRelationInformationCountTranslatorTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new StaticsEnterpriseRelationInformationCountTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjects()
    {
        $this->assertEquals(
            array(),
            $this->translator->arrayToObjects(array())
        );
    }

    public function testArrayToObject()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Statistical\Model\NullStatistical',
            $this->translator->arrayToObject(array())
        );
    }

    public function testObjectToArrayWithError()
    {
        $class = new class() extends TestCase{
        };
        $this->assertEquals(
            array(),
            $this->translator->objectToArray($class)
        );
    }

    public function testObjectToArray()
    {
        $expression = [
            'id' => 1,
            'name' => 'name'
        ];

        $statistical = new Statistical();
        $statistical->setResult($expression);

        $this->assertEquals(
            $expression,
            $this->translator->objectToArray($statistical)
        );
    }
}
