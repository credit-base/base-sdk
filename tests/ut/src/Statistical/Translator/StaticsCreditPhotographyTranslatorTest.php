<?php
namespace Base\Sdk\Statistical\Translator;

use Base\Sdk\Statistical\Model\Statistical;
use PHPUnit\Framework\TestCase;

class StaticsCreditPhotographyTranslatorTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new StaticsCreditPhotographyTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjects()
    {
        $this->assertEquals(
            array(),
            $this->translator->arrayToObjects(array('id'=>1))
        );
    }

    public function testArrayToObject()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Statistical\Model\NullStatistical',
            $this->translator->arrayToObject(array('name'=>'张三'))
        );
    }

    public function testObjectToArrayWithErrorClass()
    {
        $class = new class($id = 0) extends TestCase{

        };
        unset($id);

        $this->assertEquals(
            array(),
            $this->translator->objectToArray($class)
        );
    }

    public function testObjectToArray()
    {
        $expression = [
            'pendingTotal'=> 1,
            'approveTotal'=>2,
            'rejectTotal'=>3
        ];

        $statistical = new Statistical();
        $statistical->setResult($expression);

        $this->assertEquals(
            $expression,
            $this->translator->objectToArray($statistical)
        );
    }
}
