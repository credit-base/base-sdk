<?php
namespace Base\Sdk\Statistical\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ITranslatorFactoryTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new ITranslatorFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->translator);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullTranslator()
    {
        $translator = $this->translator->getTranslator('');
            $this->assertInstanceOf(
                'Base\Sdk\Statistical\Translator\NullStatisticalTranslator',
                $translator
            );
    }

    public function testGetTranslator()
    {
        foreach (ITranslatorFactory::MAPS as $key => $translator) {
            $this->assertInstanceOf(
                $translator,
                $this->translator->getTranslator($key)
            );
        }
    }
}
