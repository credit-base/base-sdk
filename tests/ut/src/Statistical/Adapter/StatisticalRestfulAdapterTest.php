<?php


namespace Base\Sdk\Statistical\Adapter\Statistical;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class StatisticalRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockStatisticalRestfulAdapter::class)->setMethods([])->getMock();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->adapter);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testGetTranslator()
    {
        $this->adapter = new MockStatisticalRestfulAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->adapter = new MockStatisticalRestfulAdapter();
        $this->assertEquals(
            'statisticals',
            $this->adapter->getResource()
        );
    }

    public function testScenario()
    {
        $this->adapter = new MockStatisticalRestfulAdapter();
        $scenario = 'error';
        $this->adapter->scenario($scenario);
        $this->assertEquals(
            array(),
            $this->adapter->getScenario()
        );
    }

    public function testFetchOne()
    {
        $id = 1;
        $this->adapter = $this->getMockBuilder(MockStatisticalRestfulAdapter::class)->setMethods([
            'fetchOneAction'
        ])->getMock();

        $this->adapter->expects($this->exactly(1))->method('fetchOneAction')->willReturn(false);
        $this->assertFalse(
            $this->adapter->fetchOne($id)
        );
    }
}
