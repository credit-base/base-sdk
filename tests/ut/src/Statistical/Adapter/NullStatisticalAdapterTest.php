<?php


namespace Base\Sdk\Statistical\Adapter\Statistical;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullStatisticalAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new NullStatisticalAdapter();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->adapter);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testAnalyse()
    {
        $filter = array();
        $this->assertInstanceOf(
            'Base\Sdk\Statistical\Model\NullStatistical',
            $this->adapter->analyse($filter)
        );
    }
}
