<?php
namespace Base\Sdk\Statistical\Adapter\Statistical;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class IAdapterFactoryTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new IAdapterFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->adapter);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullAdapter()
    {
        $adapter = $this->adapter->getAdapter('');
            $this->assertInstanceOf(
                'Base\Sdk\Statistical\Adapter\Statistical\NullStatisticalAdapter',
                $adapter
            );
    }

    public function testGetAdapter()
    {
        foreach (IAdapterFactory::MAPS as $key => $adapter) {
            $this->assertInstanceOf(
                $adapter,
                $this->adapter->getAdapter($key)
            );
        }
    }
}
