<?php


namespace Base\Sdk\Statistical\Adapter\Statistical;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class StaticsEnterpriseRelationInformationCountAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockStaticsEnterpriseRelationInformationCountAdapter::class)
        ->setMethods([])->getMock();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->adapter);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testAnalyse()
    {
        $this->adapter = $this->getMockBuilder(MockStaticsEnterpriseRelationInformationCountAdapter::class)
            ->setMethods(['get','getResource','isSuccess'])->getMock();

        $this->adapter->expects($this->exactly(1))->method('getResource')->willReturn('statisticals');
        $this->adapter->expects($this->exactly(1))->method('get');
        $this->adapter->expects($this->exactly(1))->method('isSuccess')->willReturn(false);

        $this->assertInstanceOf(
            'Base\Sdk\Statistical\Model\NullStatistical',
            $this->adapter->analyse()
        );
    }

    public function testAnalyseAsync()
    {
        $this->adapter = $this->getMockBuilder(MockStaticsEnterpriseRelationInformationCountAdapter::class)
            ->setMethods(['getAsync','getResource'])->getMock();

        $this->adapter->expects($this->exactly(1))->method('getResource')->willReturn('statisticals');
        $this->adapter->expects($this->exactly(1))->method('getAsync')->willReturn(array());

        $this->assertIsArray($this->adapter->analyseAsync());
    }
}
