<?php


namespace Base\Sdk\Statistical\Model;

use PHPUnit\Framework\TestCase;

class StatisticalTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new Statistical();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testSetId()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    public function testSetResult()
    {
        $this->stub->setResult(array());
        $this->assertEquals(array(), $this->stub->getResult());
    }
}
