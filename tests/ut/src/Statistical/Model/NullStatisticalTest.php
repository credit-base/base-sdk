<?php
namespace Base\Sdk\Statistical\Model;

use PHPUnit\Framework\TestCase;

class NullStatisticalTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullStatistical::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

//    public function testExtendStatistical()
//    {
//        $this->stub = NullStatistical::getInstance();
//        $this->assertInstanceOf('Base\Sdk\Statistical\Statistical', $this->stub);
//    }

    public function testImplements()
    {
        $this->stub = NullStatistical::getInstance();
        $this->assertInstanceOf('Marmot\Interfaces\INull', $this->stub);
    }
}
