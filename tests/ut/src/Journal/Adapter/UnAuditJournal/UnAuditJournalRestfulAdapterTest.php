<?php
namespace Base\Sdk\Journal\Adapter\UnAuditJournal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Model\NullUnAuditJournal;
use Base\Sdk\Journal\Translator\UnAuditJournalRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class UnAuditJournalRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UnAuditJournalRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends UnAuditJournalRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getUnAuditJournalMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIJournalAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Journal\Adapter\UnAuditJournal\IUnAuditJournalAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedJournals', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDIT_JOURNAL_LIST',
                UnAuditJournalRestfulAdapter::SCENARIOS['UN_AUDIT_JOURNAL_LIST']
            ],
            [
                'UN_AUDIT_JOURNAL_FETCH_ONE',
                UnAuditJournalRestfulAdapter::SCENARIOS['UN_AUDIT_JOURNAL_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    public function testFetchOne()
    {
        $id = 1;

        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateJournal($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullUnAuditJournal::getInstance())
            ->willReturn($journal);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($journal, $result);
    }

    /**
     * 为UnAuditJournalRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$unAuditJournall,$keys,$JournalArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareJournalTranslator(
        UnAuditJournal $journal,
        array $keys,
        array $journalArray
    ) {
        $translator = $this->prophesize(UnAuditJournalRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($journal),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($journalArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(UnAuditJournal $journal)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($journal);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    public function testAdd()
    {
        $unAuditJournal = new NullUnAuditJournal();
        $result = $this->adapter->add($unAuditJournal);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareJournalTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal(1);
        $journalArray = array();

        $this->prepareJournalTranslator(
            $journal,
            array(
                'title',
                'source',
                'cover',
                'attachment',
                'authImages',
                'year',
                'description',
                'status',
                'crew'
            ),
            $journalArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$journal->getId().'/'.'resubmit', $journalArray);

        $this->success($journal);
        $result = $this->adapter->edit($journal);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareJournalTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal(1);
        $journalArray = array();

        $this->prepareJournalTranslator(
            $journal,
            array(
                'title',
                'source',
                'cover',
                'attachment',
                'authImages',
                'year',
                'description',
                'status',
                'crew'
            ),
            $journalArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$journal->getId().'/'.'resubmit', $journalArray);
        
            $this->failure($journal);
            $result = $this->adapter->edit($journal);
            $this->assertFalse($result);
    }

/**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareJournalTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行approve（）
     * 判断 result 是否为true
     */
    public function testApproveSuccess()
    {
        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal(1);
        $journalArray = array();

        $this->prepareJournalTranslator(
            $journal,
            array(
                'applyCrew',
            ),
            $journalArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$journal->getId().'/approve', $journalArray);

        $this->success($journal);
        $result = $this->adapter->approve($journal);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareJournalTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行approve（）
     * 判断 result 是否为false
     */
    public function testApproveFailure()
    {
        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal(1);
        $journalArray = array();

        $this->prepareJournalTranslator(
            $journal,
            array(
                'applyCrew',
            ),
            $journalArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$journal->getId().'/approve', $journalArray);
        
        $this->failure($journal);
        $result = $this->adapter->approve($journal);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareJournalTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行reject（）
     * 判断 result 是否为true
     */
    public function testRejectSuccess()
    {
        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal(1);
        $journalArray = array();

        $this->prepareJournalTranslator(
            $journal,
            array(
                'rejectReason',
                'applyCrew'
            ),
            $journalArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$journal->getId().'/reject', $journalArray);

        $this->success($journal);
        $result = $this->adapter->reject($journal);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareJournalTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行reject（）
     * 判断 result 是否为false
     */
    public function testRejectFailure()
    {
        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal(1);
        $journalArray = array();

        $this->prepareJournalTranslator(
            $journal,
            array(
                'rejectReason',
                'applyCrew'
            ),
            $journalArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$journal->getId().'/reject', $journalArray);
        
        $this->failure($journal);
        $result = $this->adapter->reject($journal);
        $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [
            101 => [
                'title' => TITLE_FORMAT_ERROR,
                'source' => SOURCE_FORMAT_ERROR,
                'year' => YEAR_FORMAT_ERROR,
                'authImages' => IMAGE_FORMAT_ERROR,
                'cover' => IMAGE_FORMAT_ERROR,
                'attachment' => ATTACHMENT_FORMAT_ERROR,
                'description' => DESCRIPTION_FORMAT_ERROR,
                'status' => STATUS_FORMAT_ERROR,
                'crewId' => PARAMETER_FORMAT_ERROR,
                'applyCrewId' => PARAMETER_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
            ]
        ];
        
        $result = $this->childAdapter->getUnAuditJournalMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
