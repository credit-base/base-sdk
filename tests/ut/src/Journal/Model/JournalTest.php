<?php
namespace Base\Sdk\Journal\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Journal\Repository\JournalRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class JournalTest extends TestCase
{
    private $journal;

    public function setUp()
    {
        $this->journal = new MockJournal();
    }

    public function tearDown()
    {
        unset($this->journal);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->journal->getId());
        $this->assertEquals('', $this->journal->getTitle());
        $this->assertEquals('', $this->journal->getDescription());
        $this->assertEquals('', $this->journal->getSource());
        $this->assertEquals(array(), $this->journal->getAuthImages());
        $this->assertEquals(array(), $this->journal->getAttachment());
        $this->assertEquals(array(), $this->journal->getCover());
        $this->assertEquals(0, $this->journal->getYear());
        $this->assertEquals(Journal::STATUS['ENABLED'], $this->journal->getStatus());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->journal->getCrew());
        $this->assertInstanceOf('Base\Sdk\UserGroup\Model\UserGroup', $this->journal->getUserGroup());
        $this->assertInstanceOf(
            'Base\Sdk\Journal\Repository\JournalRepository',
            $this->journal->getRepository()
        );
    }

    //year 测试 -------------------------------------------------------- start
    /**
     * 设置 Journal setYear() 正确的传参类型,期望传值正确
     */
    public function testSetYearCorrectType()
    {
        $expected = 2021;

        $this->journal->setYear($expected);
        $this->assertEquals($expected, $this->journal->getYear());
    }

    /**
     * 设置 Journal setYear() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetYearWrongType()
    {
        $this->journal->setYear('year');
    }
    //year 测试 --------------------------------------------------------   end

    //title 测试 -------------------------------------------------------- start
    /**
     * 设置 Journal setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->journal->setTitle('title');
        $this->assertEquals('title', $this->journal->getTitle());
    }

    /**
     * 设置 Journal setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->journal->setTitle(['title']);
    }
    //title 测试 --------------------------------------------------------   end

    //description 测试 -------------------------------------------------------- start
    /**
     * 设置 Journal setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->journal->setDescription('description');
        $this->assertEquals('description', $this->journal->getDescription());
    }

    /**
     * 设置 Journal setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->journal->setDescription(['description']);
    }
    //description 测试 --------------------------------------------------------   end
    
    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Journal setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->journal->setStatus($actual);
        $this->assertEquals($expected, $this->journal->getStatus());
    }

    /**
     * 循环测试 Journal setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Journal::STATUS['ENABLED'], Journal::STATUS['ENABLED']),
            array(Journal::STATUS['DISABLED'], Journal::STATUS['DISABLED']),
        );
    }

    /**
     * 设置 Journal setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->journal->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

    //Crew 测试 -------------------------------------------------------- start
    /**
     * 设置 Journal setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->journal->setCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->journal->getCrew());
    }

    /**
     * 设置 Journal setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->journal->setCrew('Crew');
    }
    //Crew 测试 --------------------------------------------------------   end

    //UserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 Journal setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->journal->setUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->journal->getUserGroup());
    }

    /**
     * 设置 Journal setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->journal->setUserGroup('UserGroup');
    }
    //UserGroup 测试 --------------------------------------------------------   end

    //authImages 测试 -------------------------------------------------------- start
    /**
     * 设置 Journal setAuthImages() 正确的传参类型,期望传值正确
     */
    public function testSetAuthImagesCorrectType()
    {
        $authImages = array(
            array('name' => 'name', 'identify' => 'identify.jpg'),
            array('name' => 'name', 'identify' => 'identify.jpeg'),
            array('name' => 'name', 'identify' => 'identify.png')
        );
        $this->journal->setAuthImages($authImages);
        $this->assertEquals($authImages, $this->journal->getAuthImages());
    }

    /**
     * 设置 Journal setAuthImages() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAuthImagesWrongType()
    {
        $this->journal->setAuthImages('string');
    }
    //authImages 测试 --------------------------------------------------------   end

    //attachment 测试 -------------------------------------------------------- start
    /**
     * 设置 Journal setAttachment() 正确的传参类型,期望传值正确
     */
    public function testSetAttachmentCorrectType()
    {
        $attachment = array('name' => 'name', 'identify' => 'identify.pdf');
        $this->journal->setAttachment($attachment);
        $this->assertEquals($attachment, $this->journal->getAttachment());
    }

    /**
     * 设置 Journal setAttachment() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAttachmentWrongType()
    {
        $this->journal->setAttachment('string');
    }
    //attachment 测试 --------------------------------------------------------   end

    //cover 测试 -------------------------------------------------------- start
    /**
     * 设置 Journal setCover() 正确的传参类型,期望传值正确
     */
    public function testSetCoverCorrectType()
    {
        $cover = array('name' => 'name', 'identify' => 'identify.jpg');
        $this->journal->setCover($cover);
        $this->assertEquals($cover, $this->journal->getCover());
    }

    /**
     * 设置 Journal setCover() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCoverWrongType()
    {
        $this->journal->setCover('string');
    }
    //cover 测试 --------------------------------------------------------   end

    //source 测试 -------------------------------------------------------- start
    /**
     * 设置 Journal setSource() 正确的传参类型,期望传值正确
     */
    public function testSetSourceCorrectType()
    {
        $this->journal->setSource('source');
        $this->assertEquals('source', $this->journal->getSource());
    }

    /**
     * 设置 Journal setSource() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceWrongType()
    {
        $this->journal->setSource(['source']);
    }
    //source 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Journal\Repository\JournalRepository',
            $this->journal->getRepository()
        );
    }
    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 JournalRepository 调用 add 并且传参 $Journal 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $journal = $this->getMockBuilder(MockJournal::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 JournalRepository
        $repository = $this->prophesize(JournalRepository::class);
        $repository->add(Argument::exact($journal))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $journal->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $journal->add();
        $this->assertTrue($result);
    }

    public function testGetIEnableAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\Adapter\IEnableAbleAdapter',
            $this->journal->getIEnableAbleAdapter()
        );
    }
}
