<?php
namespace Base\Sdk\Journal\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class UnAuditJournalTest extends TestCase
{
    private $unAuditJournal;

    public function setUp()
    {
        $this->unAuditJournal = new MockUnAuditJournal();
    }

    public function tearDown()
    {
        unset($this->unAuditJournal);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->unAuditJournal->getId());
        $this->assertEquals(UnAuditJournal::OPERATION_TYPE['NULL'], $this->unAuditJournal->getOperationType());
        $this->assertEquals(UnAuditJournal::APPLY_STATUS['PENDING'], $this->unAuditJournal->getApplyStatus());
        $this->assertEquals('', $this->unAuditJournal->getRejectReason());
        $this->assertEquals(0, $this->unAuditJournal->getApplyInfoType());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->unAuditJournal->getRelation());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->unAuditJournal->getApplyCrew());
        $this->assertInstanceOf('Base\Sdk\UserGroup\Model\UserGroup', $this->unAuditJournal->getPublishUserGroup());
        $this->assertInstanceOf('Base\Sdk\UserGroup\Model\UserGroup', $this->unAuditJournal->getApplyUserGroup());
        $this->assertInstanceOf(
            'Base\Sdk\Journal\Repository\unAuditJournalRepository',
            $this->unAuditJournal->getUnAuditJournalRepository()
        );
    }

    //rejectReason 测试 -------------------------------------------------------- start
    /**
     * 设置 Journal setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $expected = 'string';

        $this->unAuditJournal->setRejectReason($expected);
        $this->assertEquals($expected, $this->unAuditJournal->getRejectReason());
    }

    /**
     * 设置 unAuditJournal setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->unAuditJournal->setRejectReason(['string']);
    }
    //rejectReason 测试 --------------------------------------------------------   end
    
    //operationType 测试 ------------------------------------------------------ start
    /**
     * 循环测试 unAuditJournal setOperationType() 是否符合预定范围
     *
     * @dataProvider operationTypeProvider
     */
    public function testSetOperationType($actual, $expected)
    {
        $this->unAuditJournal->setOperationType($actual);
        $this->assertEquals($expected, $this->unAuditJournal->getOperationType());
    }

    /**
     * 循环测试 Journal setOperationType() 数据构建器
     */
    public function operationTypeProvider()
    {
        return array(
            array(UnAuditJournal::OPERATION_TYPE['NULL'], UnAuditJournal::OPERATION_TYPE['NULL']),
            array(UnAuditJournal::OPERATION_TYPE['ADD'], UnAuditJournal::OPERATION_TYPE['ADD']),
            array(UnAuditJournal::OPERATION_TYPE['EDIT'], UnAuditJournal::OPERATION_TYPE['EDIT']),
            array(UnAuditJournal::OPERATION_TYPE['ENABLED'], UnAuditJournal::OPERATION_TYPE['ENABLED']),
            array(UnAuditJournal::OPERATION_TYPE['DISABLED'], UnAuditJournal::OPERATION_TYPE['DISABLED']),
        );
    }

    /**
     * 设置 Journal setOperationType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOperationTypeWrongType()
    {
        $this->unAuditJournal->setOperationType('string');
    }
    //operationType 测试 ------------------------------------------------------   end

    //applyStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 unAuditJournal setApplyStatus() 是否符合预定范围
     *
     * @dataProvider applyStatusProvider
     */
    public function testSetApplyStatus($actual, $expected)
    {
        $this->unAuditJournal->setApplyStatus($actual);
        $this->assertEquals($expected, $this->unAuditJournal->getApplyStatus());
    }

    /**
     * 循环测试 Journal setApplyStatus() 数据构建器
     */
    public function applyStatusProvider()
    {
        return array(
            array(UnAuditJournal::APPLY_STATUS['NOT_SUBMITTED'], UnAuditJournal::APPLY_STATUS['NOT_SUBMITTED']),
            array(UnAuditJournal::APPLY_STATUS['PENDING'], UnAuditJournal::APPLY_STATUS['PENDING']),
            array(UnAuditJournal::APPLY_STATUS['APPROVE'], UnAuditJournal::APPLY_STATUS['APPROVE']),
            array(UnAuditJournal::APPLY_STATUS['REJECT'], UnAuditJournal::APPLY_STATUS['REJECT']),
        );
    }

    /**
     * 设置 Journal setApplyStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyStatusWrongType()
    {
        $this->unAuditJournal->setApplyStatus('string');
    }
    //applyStatus 测试 ------------------------------------------------------   end

    //relation 测试 -------------------------------------------------------- start
    /**
     * 设置 unAuditJournal setRelation() 正确的传参类型,期望传值正确
     */
    public function testSetRelationCorrectType()
    {
        $expectedCrew = new Crew();

        $this->unAuditJournal->setRelation($expectedCrew);
        $this->assertEquals($expectedCrew, $this->unAuditJournal->getRelation());
    }

    /**
     * 设置 unAuditJournal setRelation() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRelationWrongType()
    {
        $this->unAuditJournal->setRelation('Crew');
    }
    //relation 测试 --------------------------------------------------------   end

    //applyCrew 测试 -------------------------------------------------------- start
    /**
     * 设置 unAuditJournal setApplyCrew() 正确的传参类型,期望传值正确
     */
    public function testSetApplyCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->unAuditJournal->setApplyCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->unAuditJournal->getApplyCrew());
    }

    /**
     * 设置 unAuditJournal setApplyCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyCrewWrongType()
    {
        $this->unAuditJournal->setApplyCrew('Crew');
    }
    //applyCrew 测试 --------------------------------------------------------   end

    //publishUserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 unAuditJournal setPublishUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetPublishUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->unAuditJournal->setPublishUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->unAuditJournal->getPublishUserGroup());
    }

    /**
     * 设置 unAuditJournal setPublishUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPublishUserGroupWrongType()
    {
        $this->unAuditJournal->setPublishUserGroup('UserGroup');
    }
    //publishUserGroup 测试 --------------------------------------------------------   end

    //applyUserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 unAuditJournal setApplyUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetApplyUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->unAuditJournal->setApplyUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->unAuditJournal->getApplyUserGroup());
    }

    /**
     * 设置 unAuditJournal setApplyUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyUserGroupWrongType()
    {
        $this->unAuditJournal->setApplyUserGroup('UserGroup');
    }
    //applyUserGroup 测试 --------------------------------------------------------   end

     //applyInfoType 测试 -------------------------------------------------------- start
    /**
     * 设置 Journal setApplyInfoType() 正确的传参类型,期望传值正确
     */
    public function testSetApplyInfoTypeCorrectType()
    {
        $expected = 1;

        $this->unAuditJournal->setApplyInfoType($expected);
        $this->assertEquals($expected, $this->unAuditJournal->getApplyInfoType());
    }

    /**
     * 设置 unAuditJournal setApplyInfoType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyInfoTypeWrongType()
    {
        $this->unAuditJournal->setApplyInfoType(['string']);
    }
    //applyInfoType 测试 --------------------------------------------------------   end

    public function testGetIApplyAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\Adapter\IApplyAbleAdapter',
            $this->unAuditJournal->getIApplyAbleAdapter()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\Adapter\IOperateAbleAdapter',
            $this->unAuditJournal->getIOperateAbleAdapter()
        );
    }
}
