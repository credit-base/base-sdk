<?php
namespace Base\Sdk\Journal\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NullUnAuditJournalTest extends TestCase
{
    private $guest;

    public function setUp()
    {
        $this->guest = NullUnAuditJournal::getInstance();
    }

    public function tearDown()
    {
        unset($this->guest);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsJournal()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Journal\Model\UnAuditJournal',
            $this->guest
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->guest
        );
    }

    public function testResourceNotExist()
    {
        $guest = new MockNullUnAuditJournal();

        $result = $guest->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
