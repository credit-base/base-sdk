<?php
namespace Base\Sdk\Journal\Command\Journal;

use PHPUnit\Framework\TestCase;

class DisableJournalCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new DisableJournalCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
