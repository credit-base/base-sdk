<?php
namespace Base\Sdk\Journal\Command\Journal;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Command\CommonFackerDataTrait;

class EditJournalCommandTest extends TestCase
{
    use CommonFackerDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = $this->getOperationFackerData();

        $this->command = new EditJournalCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['description'],
            $this->fakerData['status'],
            $this->fakerData['year'],
            1,
            $this->fakerData['cover'],
            $this->fakerData['attachment'],
            $this->fakerData['authImages']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
