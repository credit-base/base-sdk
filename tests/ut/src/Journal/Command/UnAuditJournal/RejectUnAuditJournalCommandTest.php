<?php
namespace Base\Sdk\Journal\Command\UnAuditJournal;

use PHPUnit\Framework\TestCase;

class RejectUnAuditJournalCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $rejectReason = '驳回原因';
        $id = 1;
        $this->stub = new RejectUnAuditJournalCommand($rejectReason, $id);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
