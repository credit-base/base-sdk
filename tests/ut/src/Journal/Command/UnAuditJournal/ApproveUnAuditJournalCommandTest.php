<?php
namespace Base\Sdk\Journal\Command\UnAuditJournal;

use PHPUnit\Framework\TestCase;

class ApproveUnAuditJournalCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $id = 1;
        $this->stub = new ApproveUnAuditJournalCommand($id);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
