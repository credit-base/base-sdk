<?php
namespace Base\Sdk\Journal\Command;

use Base\Sdk\Journal\Model\Journal;

trait CommonFackerDataTrait
{
    public function getOperationFackerData()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $fakerData = array(
            'title' => $faker->name(),
            'source' => $faker->name(),
            'description' => $faker->name(),
            'status' => $faker->randomElement(Journal::STATUS),
            'year' => 2021,
            'cover' => array('name' => '封面名称', 'identify' => '封面地址.jpg'),
            'attachment' => array('name' => '附件名称', 'identify' => '附件地址.pdf'),
            'authImages' => array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg'),
                array('name' => 'name', 'identify' => 'identify.png')
            )
        );

        return $fakerData;
    }
}
