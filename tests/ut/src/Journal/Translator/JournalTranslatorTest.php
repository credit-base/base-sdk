<?php
namespace Base\Sdk\Journal\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Utils\JournalUtils;

class JournalTranslatorTest extends TestCase
{
    use JournalUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new JournalTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Journal\Model\NullJournal', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateJournal(1);

        $expression = $this->translator->objectToArray($journal);
        
        $this->compareArrayAndObject($expression, $journal);
    }

    public function testObjectToArrayFail()
    {
        $journal = null;

        $expression = $this->translator->objectToArray($journal);
        $this->assertEquals(array(), $expression);
    }
}
