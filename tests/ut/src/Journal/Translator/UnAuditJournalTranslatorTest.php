<?php
namespace Base\Sdk\Journal\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Utils\JournalUtils;

class UnAuditJournalTranslatorTest extends TestCase
{
    use JournalUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditJournalTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Journal\Model\NullUnAuditJournal', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $unAuditJournal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal(1);

        $expression = $this->translator->objectToArray($unAuditJournal);
        
        $this->compareArrayAndObject($expression, $unAuditJournal);
        $this->assertEquals($expression['rejectReason'], $unAuditJournal->getRejectReason());
    }

    public function testObjectToArrayFail()
    {
        $unAuditJournal = null;

        $expression = $this->translator->objectToArray($unAuditJournal);
        $this->assertEquals(array(), $expression);
    }
}
