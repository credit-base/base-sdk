<?php
namespace Base\Sdk\Journal\Translator;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\Journal\Utils\JournalRestfulUtils;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;
use Base\Sdk\Crew\Model\Crew;

class JournalRestfulTranslatorTest extends TestCase
{
    use JournalRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new JournalRestfulTranslator();
        $this->childTranslator = new class extends JournalRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateJournal(1);

        $expression['data']['id'] = $journal->getId();
        $expression['data']['attributes']['title'] = $journal->getTitle();
        $expression['data']['attributes']['source'] = $journal->getSource();

        $expression['data']['attributes']['attachment'] = $journal->getAttachment();
        $expression['data']['attributes']['status'] = $journal->getStatus();

        $expression['data']['attributes']['year'] = $journal->getYear();
        $expression['data']['attributes']['description'] = $journal->getDescription();

        $expression['data']['attributes']['authImages'] = $journal->getAuthImages();
        $expression['data']['attributes']['cover'] = $journal->getCover();

        $expression['data']['attributes']['createTime'] = $journal->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $journal->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $journal->getUpdateTime();

        $journalObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Journal\Model\Journal', $journalObject);
        $this->compareArrayAndObject($expression, $journalObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $journal = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Journal\Model\NullJournal', $journal);
    }

    public function testObjectToArray()
    {
        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateJournal(1);

        $expression = $this->translator->objectToArray($journal);

        $this->compareArrayAndObject($expression, $journal);
    }

    public function testObjectToArrayFail()
    {
        $journal = null;

        $expression = $this->translator->objectToArray($journal);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['crew']['data'] 和
     * $relationships['publishUserGroup']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用两次, 分别用于处理 crew 和 publishUserGroup 且返回各自的数组 $crew 和 $publishUserGroup
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用一次, 入参是 $crewInfo, 出参是 $crew
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $publishUserGroup
     Info, 出参是 $publishUserGroup
     * 6. $journal->setCrew 调用一次, 入参 $crew
     * 7. $journal->setUserGroup 调用一次, 入参 $userGroup
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup']
        ];

        $crewInfo = ['mockCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];

        $crew = new Crew();
        $publishUserGroup = new UserGroup();

        $translator = $this->getMockBuilder(JournalRestfulTranslator::class)
                           ->setMethods([
                                'relationship',
                                'changeArrayFormat',
                                'getCrewRestfulTranslator',
                                'getUserGroupRestfulTranslator',
                            ])
                           ->getMock();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用2次, 依次入参 crew, publishUserGroup
        //依次返回 $crewInfo 和 $userGroupInfo
        $translator->expects($this->exactly(2))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['publishUserGroup']['data']]
             )
            ->will($this->onConsecutiveCalls($crewInfo, $publishUserGroupInfo));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $userGrouptRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGrouptRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGrouptRestfulTranslator->reveal());

        //揭示
        $journal = $translator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\Journal\Model\Journal', $journal);
        $this->assertEquals($crew, $journal->getCrew());
        $this->assertEquals($publishUserGroup, $journal->getUserGroup());
    }
}
