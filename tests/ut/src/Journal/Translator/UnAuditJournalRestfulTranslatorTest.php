<?php
namespace Base\Sdk\Journal\Translator;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\Journal\Utils\JournalRestfulUtils;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;
use Base\Sdk\Crew\Model\Crew;

class UnAuditJournalRestfulTranslatorTest extends TestCase
{
    use JournalRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditJournalRestfulTranslator();
        $this->childTranslator = new class extends UnAuditJournalRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditJournal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal(1);

        $expression['data']['id'] = $unAuditJournal->getId();
        $expression['data']['attributes']['title'] = $unAuditJournal->getTitle();
        $expression['data']['attributes']['source'] = $unAuditJournal->getSource();

        $expression['data']['attributes']['attachment'] = $unAuditJournal->getAttachment();
        $expression['data']['attributes']['status'] = $unAuditJournal->getStatus();

        $expression['data']['attributes']['year'] = $unAuditJournal->getYear();
        $expression['data']['attributes']['description'] = $unAuditJournal->getDescription();

        $expression['data']['attributes']['authImages'] = $unAuditJournal->getAuthImages();
        $expression['data']['attributes']['cover'] = $unAuditJournal->getCover();
        $expression['data']['attributes']['rejectReason'] = $unAuditJournal->getRejectReason();
        $expression['data']['attributes']['applyStatus'] = $unAuditJournal->getApplyStatus();
        $expression['data']['attributes']['operationType'] = $unAuditJournal->getOperationType();
        $expression['data']['attributes']['applyInfoType'] = $unAuditJournal->getApplyInfoType();

        $expression['data']['attributes']['createTime'] = $unAuditJournal->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $unAuditJournal->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $unAuditJournal->getUpdateTime();

        $unAuditJournalObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Journal\Model\UnAuditJournal', $unAuditJournalObject);
        $this->compareArrayAndObject($expression, $unAuditJournalObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $journal = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Journal\Model\NullUnAuditJournal', $journal);
    }

    public function testObjectToArray()
    {
        $unAuditJournal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal(1);

        $expression = $this->translator->objectToArray($unAuditJournal);

        $this->compareArrayAndObject($expression, $unAuditJournal);
    }

    public function testObjectToArrayFail()
    {
        $object = null;

        $expression = $this->translator->objectToArray($object);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['crew']['data'] 、
     * $relationships['publishUserGroup']['data']、
     * $relationships['relation']['data']、
     * $relationships['applyCrew']['data']、
     * $relationships['applyUserGroup']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用5次, 分别用于处理 crew 、 publishUserGroup、relation、applyCrew、applyUserGroup
     *  且返回各自的数组 $crew 、 $publishUserGroup、$relation、$applyCrew、$applyUserGroup
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用3次, 入参是 $crewInfo, 出参是 $crew
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用3次, 入参是 $publishUserGroup
     Info, 出参是 $publishUserGroup
     * 6. $journal->setCrew 调用3次, 入参 $crew
     * 7. $journal->setUserGroup 调用2次, 入参 $userGroup
     */
/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup'],
            'applyUserGroup'=>['data'=>'mockApplyUserGroup'],
            'applyCrew'=>['data'=>'mockApplyCrew'],
            'relation'=>['data'=>'mockRelation'],
        ];
       
        $crewInfo = ['mockCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];
        $applyCrewInfo = ['mockApplyCrewInfo'];
        $applyUserGroupInfo = ['mockApplyUserGroupInfo'];
        $relationInfo = ['mockRelationInfo'];

        $crew = new Crew();
        $publishUserGroup = new UserGroup();
        $applyCrew = new Crew();
        $relation = new Crew();
        $applyUserGroup = new UserGroup();

        $translator = $this->getMockBuilder(UnAuditJournalRestfulTranslator::class)
                           ->setMethods([
                                'relationship',
                                'changeArrayFormat',
                                'getCrewRestfulTranslator',
                                'getUserGroupRestfulTranslator',
                            ])
                           ->getMock();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用5次, 依次入参 crew, publishUserGroup, applyUserGroup,applyCrew, relation
        //依次返回 $crewInfo , $publishUserGroupInfo,$applyUserGroupInfo, $applyCrewInfo , $relationInfo
        $translator->expects($this->exactly(5))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['publishUserGroup']['data']],
                 [$relationships['applyUserGroup']['data']],
                 [$relationships['applyCrew']['data']],
                 [$relationships['relation']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $publishUserGroupInfo,
                $applyUserGroupInfo,
                $applyCrewInfo,
                $relationInfo
            ));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);

        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
        ->shouldBeCalledTimes(1)
        ->willReturn($crew);

        $crewRestfulTranslator->arrayToObject(Argument::exact($applyCrewInfo))
        ->shouldBeCalledTimes(1)
        ->willReturn($applyCrew);

        $crewRestfulTranslator->arrayToObject(Argument::exact($relationInfo))
        ->shouldBeCalledTimes(1)
        ->willReturn($relation);

        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);

        $userGroupRestfulTranslator->arrayToObject(Argument::exact($applyUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyUserGroup);

        $translator->expects($this->exactly(3))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $translator->expects($this->exactly(2))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        
        //揭示
        $unAuditJournal = $translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Journal\Model\UnAuditJournal', $unAuditJournal);
        $this->assertEquals($crew, $unAuditJournal->getCrew());
        $this->assertEquals($applyCrew, $unAuditJournal->getApplyCrew());
        $this->assertEquals($relation, $unAuditJournal->getRelation());
        $this->assertEquals($publishUserGroup, $unAuditJournal->getUserGroup());
        $this->assertEquals($applyUserGroup, $unAuditJournal->getApplyUserGroup());
    }
}
