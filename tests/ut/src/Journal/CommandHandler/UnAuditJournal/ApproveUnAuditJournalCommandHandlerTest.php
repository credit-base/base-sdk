<?php
namespace Base\Sdk\Journal\CommandHandler\UnAuditJournal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Repository\UnAuditJournalRepository;
use Base\Sdk\Journal\Command\UnAuditJournal\ApproveUnAuditJournalCommand;

class ApproveUnAuditJournalCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveUnAuditJournalCommandHandler::class)
            ->setMethods(['fetchUnAuditJournal'])
            ->getMock();
    }

    public function testFetchIApplyObject()
    {
        $commandHandler = $this->getMockBuilder(MockApproveUnAuditJournalCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $unAuditJournal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal($id);

        $repository = $this->prophesize(UnAuditJournalRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditJournal);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditJournal);
    }

    public function testExtendsApproveCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }

    // public function testExecuteAction()
    // {
    //     $stub = $this->getMockBuilder(MockApproveUnAuditJournalCommandHandler::class)
    //             ->setMethods(['fetchIApplyObject', 'fetchCrew'])
    //             ->getMock();

    //     $id = 1;
    //     $unAuditJournal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal($id);
    //     $crew = $unAuditJournal->getCrew();

    //     $command = new ApproveUnAuditJournalCommand($crew->getId(), $id);

    //     $stub->expects($this->once())
    //          ->method('fetchCrew')
    //          ->with($crew->getId())
    //          ->willReturn($crew);

    //     $journal = $this->prophesize(UnAuditJournal::class);
    //     $journal->setApplyCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
    //     $journal->approve()->shouldBeCalledTimes(1)->willReturn(true);

    //     $stub->expects($this->exactly(1))->method('fetchIApplyObject')->willReturn($journal->reveal());

    //     $result = $stub->executeAction($command);

    //     $this->assertTrue($result);
    // }
}
