<?php
namespace Base\Sdk\Journal\CommandHandler\UnAuditJournal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Repository\UnAuditJournalRepository;
use Base\Sdk\Journal\Command\UnAuditJournal\RejectUnAuditJournalCommand;

class RejectUnAuditJournalCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRejectUnAuditJournalCommandHandler();
    }

    public function testExtendsRejectCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->stub
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Journal\Repository\UnAuditJournalRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchIEnableObject()
    {
        $commandHandler = $this->getMockBuilder(MockRejectUnAuditJournalCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $unAuditJournal = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal($id);

        $repository = $this->prophesize(UnAuditJournalRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditJournal);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditJournal);
    }

    // public function testExecuteAction()
    // {
    //     $stub = $this->getMockBuilder(MockRejectUnAuditJournalCommandHandler::class)
    //             ->setMethods(['fetchIApplyObject', 'fetchCrew'])
    //             ->getMock();

    //     $id = 1;
    //     $data = \Base\Sdk\Journal\Utils\MockFactory::generateUnAuditedJournal($id);
    //     $crew = $data->getCrew();
    //     $rejectReason = '驳回原因';

    //     $command = new RejectUnAuditJournalCommand($rejectReason, $crew->getId(), $id);

    //     $stub->expects($this->once())
    //          ->method('fetchCrew')
    //          ->with($crew->getId())
    //          ->willReturn($crew);

    //     $unAuditJournal = $this->prophesize(UnAuditJournal::class);
        
    //     $unAuditJournal->setApplyCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
    //     $unAuditJournal->setRejectReason(Argument::exact($rejectReason))->shouldBeCalledTimes(1);

    //     $unAuditJournal->reject()->shouldBeCalledTimes(1)->willReturn(true);

    //     $stub->expects($this->exactly(1))->method('fetchIApplyObject')->willReturn($unAuditJournal->reveal());

    //     $result = $stub->executeAction($command);

    //     $this->assertTrue($result);
    // }
}
