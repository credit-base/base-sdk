<?php
namespace Base\Sdk\Journal\CommandHandler\UnAuditJournal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Repository\UnAuditJournalRepository;
use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\Journal\Command\UnAuditJournal\EditUnAuditJournalCommand;

use Base\Sdk\Journal\Command\CommonFackerDataTrait;

class EditJournalCommandHandlerTest extends TestCase
{
    use CommonFackerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditUnAuditJournalCommandHandler::class)
            ->setMethods(['getRepository', 'getCrewRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    // public function testGetCrewRepository()
    // {
    //     $commandHandler = new MockEditUnAuditJournalCommandHandler();
    //     $this->assertInstanceOf(
    //         'Base\Sdk\Crew\Repository\CrewRepository',
    //         $commandHandler->getCrewRepository()
    //     );
    // }

    public function testGetRepository()
    {
        $commandHandler = new MockEditUnAuditJournalCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Journal\Repository\UnAuditJournalRepository',
            $commandHandler->getRepository()
        );
    }

    public function testExecute()
    {
        $fakerData = $this->getOperationFackerData();
        $id = 1;
        
        $command = new EditUnAuditJournalCommand(
            $fakerData['title'],
            $fakerData['source'],
            $fakerData['description'],
            $fakerData['status'],
            $fakerData['year'],
            $id,
            $fakerData['cover'],
            $fakerData['attachment'],
            $fakerData['authImages']
        );

        // $crew = \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        // $crewId = $fakerData['crew'];
        // $crewRepository = $this->prophesize(CrewRepository::class);
        // $crewRepository->fetchOne(Argument::exact($crewId))
        //     ->shouldBeCalledTimes(1)->willReturn($crew);
        // $this->commandHandler->expects($this->exactly(1))
        //             ->method('getCrewRepository')
        //             ->willReturn($crewRepository->reveal());

        $unAuditJournal = $this->prophesize(UnAuditJournal::class);
        $unAuditJournal->setCover(Argument::exact($command->cover))->shouldBeCalledTimes(1);
        $unAuditJournal->setYear(Argument::exact($command->year))->shouldBeCalledTimes(1);
        $unAuditJournal->setAuthImages(Argument::exact($command->authImages))->shouldBeCalledTimes(1);
        $unAuditJournal->setSource(Argument::exact($command->source))->shouldBeCalledTimes(1);
        $unAuditJournal->setStatus(Argument::exact($command->status))->shouldBeCalledTimes(1);
        $unAuditJournal->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $unAuditJournal->setAttachment(Argument::exact($command->attachment))->shouldBeCalledTimes(1);
        $unAuditJournal->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        // $unAuditJournal->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        
        $unAuditJournal->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $unAuditJournalRepository = $this->prophesize(UnAuditJournalRepository::class);
        $unAuditJournalRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($unAuditJournal->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($unAuditJournalRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
