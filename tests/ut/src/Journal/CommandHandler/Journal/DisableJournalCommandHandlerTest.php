<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Repository\JournalRepository;
use Base\Sdk\Journal\Command\Journal\DisableJournalCommand;

class DisableJournalCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockDisableJournalCommandHandler();
    }

    public function testFetchIEnableObject()
    {
        $commandHandler = $this->getMockBuilder(MockDisableJournalCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateJournal($id);

        $repository = $this->prophesize(JournalRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($journal);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIEnableObject($id);

        $this->assertEquals($result, $journal);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Journal\Repository\JournalRepository',
            $this->stub->getRepository()
        );
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    // public function testExecuteAction()
    // {
    //     $stub = $this->getMockBuilder(MockDisableJournalCommandHandler::class)
    //             ->setMethods(['fetchIEnableObject', 'fetchCrew'])
    //             ->getMock();

    //     $id = 1;
    //     $journalData = \Base\Sdk\Journal\Utils\MockFactory::generateJournal($id);
    //     $crew = $journalData->getCrew();

    //     $command = new DisableJournalCommand($id);

    //     $stub->expects($this->once())
    //          ->method('fetchCrew')
    //          ->with($crew->getId())
    //          ->willReturn($crew);

    //     $journal = $this->prophesize(Journal::class);
        
    //     $journal->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);

    //     $journal->disable()->shouldBeCalledTimes(1)->willReturn(true);

    //     $stub->expects($this->exactly(1))->method('fetchIEnableObject')->willReturn($journal->reveal());

    //     $result = $stub->executeAction($command);

    //     $this->assertTrue($result);
    // }
}
