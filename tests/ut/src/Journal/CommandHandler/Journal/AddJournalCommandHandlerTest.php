<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\Journal\Command\Journal\AddJournalCommand;
use Base\Sdk\Journal\Command\CommonFackerDataTrait;

class AddJournalCommandHandlerTest extends TestCase
{
    use CommonFackerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddJournalCommandHandler::class)
            ->setMethods(['getJournal', 'getCrewRepository', 'getJournalRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetJournal()
    {
        $commandHandler = new MockAddJournalCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Journal\Model\Journal',
            $commandHandler->getJournal()
        );
    }

    // public function testGetCrewRepository()
    // {
    //     $commandHandler = new MockAddJournalCommandHandler();
    //     $this->assertInstanceOf(
    //         'Base\Sdk\Crew\Repository\CrewRepository',
    //         $commandHandler->getCrewRepository()
    //     );
    // }

    public function initialExecute()
    {
        $fakerData = $this->getOperationFackerData();
        
        $command = new AddJournalCommand(
            $fakerData['title'],
            $fakerData['source'],
            $fakerData['description'],
            $fakerData['status'],
            $fakerData['year'],
            0,
            $fakerData['cover'],
            $fakerData['attachment'],
            $fakerData['authImages']
        );
        
        // $crew = \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        // $crewId = $fakerData['crew'];
        // $crewRepository = $this->prophesize(CrewRepository::class);
        // $crewRepository->fetchOne(Argument::exact($crewId))
        //     ->shouldBeCalledTimes(1)->willReturn($crew);
        // $this->commandHandler->expects($this->exactly(1))
        //             ->method('getCrewRepository')
        //             ->willReturn($crewRepository->reveal());

        $journal = $this->prophesize(Journal::class);
        
        // $journal->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $journal->setSource(Argument::exact($command->source))->shouldBeCalledTimes(1);
        $journal->setStatus(Argument::exact($command->status))->shouldBeCalledTimes(1);
        $journal->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $journal->setAuthImages(Argument::exact($command->authImages))->shouldBeCalledTimes(1);
        $journal->setCover(Argument::exact($command->cover))->shouldBeCalledTimes(1);
        $journal->setYear(Argument::exact($command->year))->shouldBeCalledTimes(1);
        $journal->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        $journal->setAttachment(Argument::exact($command->attachment))->shouldBeCalledTimes(1);
        $journal->add()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->any())
            ->method('getJournal')
            ->willReturn($journal->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }
}
