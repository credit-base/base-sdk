<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Repository\JournalRepository;
use Base\Sdk\Journal\Command\Journal\EnableJournalCommand;

class EnableJournalCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockEnableJournalCommandHandler::class)
            ->setMethods(['fetchJournal'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\EnableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $commandHandler = $this->getMockBuilder(MockEnableJournalCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $journal = \Base\Sdk\Journal\Utils\MockFactory::generateJournal($id);

        $repository = $this->prophesize(JournalRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($journal);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIEnableObject($id);

        $this->assertEquals($result, $journal);
    }

    // public function testExecuteAction()
    // {
    //     $stub = $this->getMockBuilder(MockEnableJournalCommandHandler::class)
    //             ->setMethods(['fetchIEnableObject', 'fetchCrew'])
    //             ->getMock();

    //     $id = 1;
    //     $journal = \Base\Sdk\Journal\Utils\MockFactory::generateJournal($id);
    //     $crew = $journal->getCrew();

    //     $command = new EnableJournalCommand($crew->getId(), $id);

    //     $stub->expects($this->once())
    //          ->method('fetchCrew')
    //          ->with($crew->getId())
    //          ->willReturn($crew);

    //     $journal = $this->prophesize(Journal::class);

    //     $journal->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);

    //     $journal->enable()->shouldBeCalledTimes(1)->willReturn(true);

    //     $stub->expects($this->exactly(1))->method('fetchIEnableObject')->willReturn($journal->reveal());

    //     $result = $stub->executeAction($command);

    //     $this->assertTrue($result);
    // }
}
