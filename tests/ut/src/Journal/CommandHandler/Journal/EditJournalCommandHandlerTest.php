<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Repository\JournalRepository;
use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\Journal\Command\Journal\EditJournalCommand;

use Base\Sdk\Journal\Command\CommonFackerDataTrait;

class EditJournalCommandHandlerTest extends TestCase
{
    use CommonFackerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditJournalCommandHandler::class)
            ->setMethods(['getRepository', 'getCrewRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    // public function testGetCrewRepository()
    // {
    //     $commandHandler = new MockEditJournalCommandHandler();
    //     $this->assertInstanceOf(
    //         'Base\Sdk\Crew\Repository\CrewRepository',
    //         $commandHandler->getCrewRepository()
    //     );
    // }

    public function testGetRepository()
    {
        $commandHandler = new MockEditJournalCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Journal\Repository\JournalRepository',
            $commandHandler->getRepository()
        );
    }

    public function testExecute()
    {
        $fakerData = $this->getOperationFackerData();
        $id = 1;
        
        $command = new EditJournalCommand(
            $fakerData['title'],
            $fakerData['source'],
            $fakerData['description'],
            $fakerData['status'],
            $fakerData['year'],
            $id,
            $fakerData['cover'],
            $fakerData['attachment'],
            $fakerData['authImages']
        );

        // $crew = \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        // $crewId = $fakerData['crew'];
        // $crewRepository = $this->prophesize(CrewRepository::class);
        // $crewRepository->fetchOne(Argument::exact($crewId))
        //     ->shouldBeCalledTimes(1)->willReturn($crew);
        // $this->commandHandler->expects($this->exactly(1))
        //             ->method('getCrewRepository')
        //             ->willReturn($crewRepository->reveal());

        $journal = $this->prophesize(Journal::class);
        $journal->setCover(Argument::exact($command->cover))->shouldBeCalledTimes(1);
        $journal->setYear(Argument::exact($command->year))->shouldBeCalledTimes(1);
        $journal->setAuthImages(Argument::exact($command->authImages))->shouldBeCalledTimes(1);
        $journal->setSource(Argument::exact($command->source))->shouldBeCalledTimes(1);
        $journal->setStatus(Argument::exact($command->status))->shouldBeCalledTimes(1);
        $journal->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $journal->setAttachment(Argument::exact($command->attachment))->shouldBeCalledTimes(1);
        $journal->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        // $journal->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        
        $journal->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $journalRepository = $this->prophesize(JournalRepository::class);
        $journalRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($journal->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($journalRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
