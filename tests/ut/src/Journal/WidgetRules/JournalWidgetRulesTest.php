<?php
namespace Base\Sdk\Journal\WidgetRules;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Sdk\Common\WidgetRules\WidgetRules;

class JournalWidgetRulesTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = JournalWidgetRules::getInstance();
        $this->childStub = new class extends JournalWidgetRules
        {
            public function getCommonWidgetRules() : WidgetRules
            {
                return parent::getCommonWidgetRules();
            }
        };

        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testGetCommonWidgetRules()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\WidgetRules\WidgetRules',
            $this->childStub->getCommonWidgetRules()
        );
    }

     /**
     * @dataProvider authImagesProvider
     */
    public function testAuthImages($actual, $expected)
    {
        $result = $this->stub->authImages($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(IMAGE_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function authImagesProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        
        return array(
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpeg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpeg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                ], false),
            array(
                [
                    ['name'=>$faker->word, 'identify'=>$faker->word],
                    ['name'=>$faker->word, 'identify'=>$faker->word]
                ], false),
            array($faker->word, false),
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.word'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.excel'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.xls'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.md'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.xsl'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.tpl'],
                ], false),
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpeg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpeg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpeg'],
                ], true),
            
        );
    }

    /**
     * @dataProvider attachmentProvider
     */
    public function testAttachment($actual, $expected)
    {
        $result = $this->stub->attachment($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(ATTACHMENT_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function attachmentProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        
        return array(
                array(
                    ['name'=>$faker->word,'identify'=>$faker->word.'.pdf'
                    ], true),
                array($faker->word, false),
                array(
                        ['name'=>$faker->word, 'identify'=>$faker->word]
                , false),
        );
    }

    //year -- start
     /**
     * @dataProvider invalidYearProvider
     */
    public function testYear($actual, $expected)
    {
        $result = $this->stub->year($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(YEAR_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidYearProvider()
    {
        return array(
            array(2021,true),
            array('s', false),
            array(1900, false),
            array(3000, false)
        );
    }
    //year -- end
}
