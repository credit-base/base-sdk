<?php
namespace Base\Sdk\Journal\Utils;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Model\UnAuditJournal;

use Base\Sdk\Common\Model\IApplyAble;

class MockFactory
{
    public static function generateJournal(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Journal {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $journal = new Journal($id);
        $journal->setId($id);

        //title
        self::generateTitle($journal, $faker, $value);
        //source
        self::generateSource($journal, $faker, $value);
        //cover
        self::generateCover($journal, $faker, $value);
        //description
        self::generateDescription($journal, $faker, $value);
        //attachment
        self::generateAttachment($journal, $faker, $value);
        //authImages
        self::generateAuthImages($journal, $faker, $value);
        //year
        self::generateYear($journal, $faker, $value);
        //crew
        self::generateCrew($journal, $faker, $value);
        //status
        self::generateStatus($journal, $faker, $value);

        $journal->setCreateTime($faker->unixTime());
        $journal->setUpdateTime($faker->unixTime());
        $journal->setStatusTime($faker->unixTime());

        return $journal;
    }

    private static function generateTitle($journal, $faker, $value) : void
    {
        $title = isset($value['title']) ?
            $value['title'] :
            $faker->word;
        
        $journal->setTitle($title);
    }

    private static function generateSource($journal, $faker, $value) : void
    {
        $source = isset($value['source']) ?
            $value['source'] :
            $faker->word;
        
        $journal->setSource($source);
    }

    private static function generateCover($journal, $faker, $value) : void
    {
        unset($faker);
        $cover = isset($value['cover']) ? $value['cover'] : array('name' => '封面名称', 'identify' => '封面地址.jpg');
        $journal->setCover($cover);
    }

    private static function generateDescription($journal, $faker, $value) : void
    {
        $description = isset($value['description']) ? $value['description'] : $faker->word;
        $journal->setDescription($description);
    }

    private static function generateAttachment($journal, $faker, $value) : void
    {
        unset($faker);
        $attachment = isset($value['attachment']) ?
            $value['attachment'] :
            array('name' => '附件名称', 'identify' => '附件地址.pdf');
        $journal->setAttachment($attachment);
    }

    private static function generateCrew($journal, $faker, $value) : void
    {
        unset($faker);
        $crew = isset($value['crew']) ?
            $value['crew'] :
            \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        $journal->setCrew($crew);
    }

    private static function generateAuthImages($journal, $faker, $value) : void
    {
        unset($faker);
        $authImages = isset($value['authImages']) ?
            $value['authImages'] :
            array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg'),
                array('name' => 'name', 'identify' => 'identify.png')
            );
        $journal->setAuthImages($authImages);
    }

    private static function generateYear($journal, $faker, $value) : void
    {
        unset($faker);
        $year = isset($value['year']) ? $value['year'] : 2020;
        $journal->setYear($year);
    }

    private static function generateStatus($journal, $faker, $value) : void
    {
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(Journal::STATUS);
        $journal->setStatus($status);
    }

    public static function generateUnAuditedJournal(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditJournal {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditJournal = new UnAuditJournal($id);

        $unAuditJournal->setId($id);

        //title
        self::generateTitle($unAuditJournal, $faker, $value);
        //source
        self::generateSource($unAuditJournal, $faker, $value);
        //cover
        self::generateCover($unAuditJournal, $faker, $value);
        //description
        self::generateDescription($unAuditJournal, $faker, $value);
        //attachment
        self::generateAttachment($unAuditJournal, $faker, $value);
        //authImages
        self::generateAuthImages($unAuditJournal, $faker, $value);
        //year
        self::generateYear($unAuditJournal, $faker, $value);
        //crew
        self::generateCrew($unAuditJournal, $faker, $value);
        //status
        self::generateStatus($unAuditJournal, $faker, $value);

        $unAuditJournal->setCreateTime($faker->unixTime());
        $unAuditJournal->setUpdateTime($faker->unixTime());
        $unAuditJournal->setStatusTime($faker->unixTime());

        $unAuditJournal->setRelation($unAuditJournal->getCrew());
        $unAuditJournal->setApplyUserGroup($unAuditJournal->getPublishUserGroup());
        $unAuditJournal->setApplyCrew($unAuditJournal->getCrew());
        $unAuditJournal->setRejectReason($unAuditJournal->getTitle());
        $unAuditJournal->setPublishUserGroup($unAuditJournal->getPublishUserGroup());

        //applyStatus
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomElement(
            IApplyAble::APPLY_STATUS
        );
        $unAuditJournal->setApplyStatus($applyStatus);

        //operationType
        $operationType = isset($value['operationType']) ? $value['operationType'] : $faker->randomElement(
            UnAuditJournal::OPERATION_TYPE
        );
        $unAuditJournal->setOperationType($operationType);

        return $unAuditJournal;
    }
}
