<?php
namespace Base\Sdk\Journal\Utils;

trait JournalRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $journal
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $journal->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['title'], $journal->getTitle());
        $this->assertEquals($expectedArray['data']['attributes']['source'], $journal->getSource());

        $this->assertEquals($expectedArray['data']['attributes']['attachment'], $journal->getAttachment());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $journal->getStatus());

        $this->assertEquals($expectedArray['data']['attributes']['year'], $journal->getYear());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $journal->getDescription());

        $this->assertEquals($expectedArray['data']['attributes']['authImages'], $journal->getAuthImages());
        $this->assertEquals($expectedArray['data']['attributes']['cover'], $journal->getCover());

        if (isset($expectedArray['data']['attributes']['applyStatus'])) {
            $this->assertEquals($expectedArray['data']['attributes']['applyStatus'], $journal->getApplyStatus());
        }

        if (isset($expectedArray['data']['attributes']['operationType'])) {
            $this->assertEquals($expectedArray['data']['attributes']['operationType'], $journal->getOperationType());
        }

        if (isset($expectedArray['data']['attributes']['applyInfoType'])) {
            $this->assertEquals($expectedArray['data']['attributes']['applyInfoType'], $journal->getApplyInfoType());
        }
        
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['crew']['data'])) {
                $this->assertEquals(
                    $relationships['crew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['crew']['data'][0]['id'],
                    $journal->getCrew()->getId()
                );
            }
        }
    }
}
