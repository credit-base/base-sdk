<?php
namespace Base\Sdk\Journal\Utils;

use Base\Sdk\Journal\Translator\JournalTranslator;
use Base\Sdk\Common\Model\IApplyCategory;

trait JournalUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $journal
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($journal->getId()));
        $this->assertEquals($expectedArray['title'], $journal->getTitle());
        $this->assertEquals($expectedArray['source'], $journal->getSource());
        $this->assertEquals($expectedArray['attachment'], $journal->getAttachment());
        $this->assertEquals($expectedArray['authImages'], $journal->getAuthImages());
        $this->assertEquals($expectedArray['description'], $journal->getDescription());
        $this->assertEquals($expectedArray['year'], $journal->getYear());
        $this->assertEquals($expectedArray['cover'], $journal->getCover());
        $this->assertEquals($expectedArray['status']['id'], marmot_encode($journal->getStatus()));
        $this->assertEquals($expectedArray['status']['name'], IApplyCategory::STATUS_CN[$journal->getStatus()]);
        $this->assertEquals($expectedArray['status']['type'], IApplyCategory::STATUS_TAG_TYPE[$journal->getStatus()]);

        $this->assertEquals($expectedArray['updateTime'], $journal->getUpdateTime());
        $this->assertEquals($expectedArray['updateTimeFormat'], date('Y年m月d日 H时i分', $journal->getUpdateTime()));

        $this->assertEquals($expectedArray['statusTime'], $journal->getStatusTime());
        $this->assertEquals($expectedArray['statusTimeFormat'], date('Y年m月d日 H时i分', $journal->getStatusTime()));

        $this->assertEquals($expectedArray['createTime'], $journal->getCreateTime());
        $this->assertEquals($expectedArray['createTimeFormat'], date('Y年m月d日 H时i分', $journal->getCreateTime()));

        // $this->assertEquals($expectedArray['crew'], $journal->getCrew());

        // $this->assertEquals($expectedArray['publishUserGroup'], $journal->getUserGroup());
    }
}
