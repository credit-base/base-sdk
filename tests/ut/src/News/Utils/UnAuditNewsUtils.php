<?php
namespace Base\Sdk\News\Utils;

use Marmot\Framework\Classes\Filter;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Crew\Translator\CrewTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;
use Base\Sdk\News\Translator\INews;

trait UnAuditNewsUtils
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    private function compareArrayAndObjectUnAuditNews(
        array $expectedArray,
        $unAuditNews
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditNews);
    }

    private function compareArrayAndObjectUnAuditedUnAuditNews(
        array $expectedArray,
        $unAuditNews
    ) {
        $this->assertEquals($expectedArray['relation'], $unAuditNews->getRelation());
        $this->assertEquals($expectedArray['operationType'], $unAuditNews->getOperationType());
        $this->assertEquals($expectedArray['applyInfoType'], $unAuditNews->getApplyInfoType());
        $this->assertEquals($expectedArray['rejectReason'], $unAuditNews->getRejectReason());
        $this->assertEquals($expectedArray['applyStatus'], $unAuditNews->getApplyStatus());
        $this->assertEquals($expectedArray['applyCrew'], $unAuditNews->getApplyCrew());
        $this->assertEquals($expectedArray['applyUserGroup'], $unAuditNews->getApplyUserGroup());
        $this->assertEquals($expectedArray['publishUserGroup'], $unAuditNews->getPublishUserGroup());
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $unAuditNews
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($unAuditNews->getId()));
        $this->assertEquals($expectedArray['title'], Filter::dhtmlspecialchars($unAuditNews->getTitle()));
        $this->assertEquals($expectedArray['source'], Filter::dhtmlspecialchars($unAuditNews->getSource()));
        $this->coverEquals($expectedArray, $unAuditNews);
        $this->attachmentsEquals($expectedArray, $unAuditNews);
        $this->assertEquals(
            $expectedArray['rejectReason'],
            Filter::dhtmlspecialchars($unAuditNews->getRejectReason())
        );
        $this->assertEquals(
            $expectedArray['content'],
            Filter::dhtmlspecialchars(str_replace("ℑ", "", $unAuditNews->getContent()))
        );
        $this->assertEquals(
            $expectedArray['newsType']['id'],
            marmot_encode($unAuditNews->getNewsType())
        );
        $this->assertEquals(
            $expectedArray['newsType']['name'],
            array_key_exists($unAuditNews->getNewsType(), NEWS_TYPE_CN) ?
                        NEWS_TYPE_CN[$unAuditNews->getNewsType()] :
                        ''
        );
        $this->assertEquals(
            $expectedArray['applyInfoType']['id'],
            marmot_encode($unAuditNews->getApplyInfoType())
        );
        $this->assertEquals(
            $expectedArray['applyInfoType']['name'],
            NEWS_TYPE_CN[$unAuditNews->getApplyInfoType()]
        );
        $this->assertEquals(
            $expectedArray['operationType']['id'],
            marmot_encode($unAuditNews->getOperationType())
        );
        $this->assertEquals(
            $expectedArray['operationType']['name'],
            IApplyCategory::OPERATION_TYPE_CN[$unAuditNews->getOperationType()]
        );
        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($unAuditNews->getCrew())
        );
        $this->assertEquals(
            $expectedArray['relation'],
            $this->getCrewTranslator()->objectToArray($unAuditNews->getRelation())
        );
        $this->assertEquals(
            $expectedArray['applyCrew'],
            $this->getCrewTranslator()->objectToArray($unAuditNews->getApplyCrew())
        );
        $this->assertEquals(
            $expectedArray['publishUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($unAuditNews->getPublishUserGroup())
        );
        $this->assertEquals(
            $expectedArray['applyUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($unAuditNews->getApplyUserGroup())
        );
        $this->bannerImageEquals($expectedArray, $unAuditNews);
        $this->assertEquals(
            $expectedArray['dimension']['id'],
            marmot_encode($unAuditNews->getDimension())
        );
        $this->assertEquals(
            $expectedArray['dimension']['name'],
            INews::DIMENSION_CN[$unAuditNews->getDimension()]
        );

        $this->assertEquals(
            $expectedArray['stick']['id'],
            marmot_encode($unAuditNews->getStick())
        );
        $this->assertEquals(
            $expectedArray['stick']['name'],
            IApplyCategory::STICK_CN[$unAuditNews->getStick()]
        );
        $this->assertEquals(
            $expectedArray['stick']['type'],
            IApplyCategory::STICK_TAG_TYPE[$unAuditNews->getStick()]
        );
        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($unAuditNews->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            IApplyCategory::STATUS_CN[$unAuditNews->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            IApplyCategory::STATUS_TAG_TYPE[$unAuditNews->getStatus()]
        );

        $this->assertEquals(
            $expectedArray['applyStatus']['id'],
            marmot_encode($unAuditNews->getApplyStatus())
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['name'],
            IApplyCategory::APPLY_STATUS_CN[$unAuditNews->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['type'],
            IApplyCategory::APPLY_STATUS_TAG_TYPE[$unAuditNews->getApplyStatus()]
        );

        $this->assertEquals(
            $expectedArray['bannerStatus']['id'],
            marmot_encode($unAuditNews->getBannerStatus())
        );
        $this->assertEquals(
            $expectedArray['bannerStatus']['name'],
            INews::BANNER_STATUS_CN[$unAuditNews->getBannerStatus()]
        );
        $this->assertEquals(
            $expectedArray['bannerStatus']['type'],
            INews::BANNER_STATUS_TAG_TYPE[$unAuditNews->getBannerStatus()]
        );
        $this->assertEquals(
            $expectedArray['homePageShowStatus']['id'],
            marmot_encode($unAuditNews->getHomePageShowStatus())
        );
        $this->assertEquals(
            $expectedArray['homePageShowStatus']['name'],
            INews::HOME_PAGE_SHOW_STATUS_CN[$unAuditNews->getHomePageShowStatus()]
        );
        $this->assertEquals(
            $expectedArray['homePageShowStatus']['type'],
            INews::HOME_PAGE_SHOW_STATUS_TAG_TYPE[$unAuditNews->getHomePageShowStatus()]
        );

        $this->assertEquals(
            $expectedArray['updateTime'],
            $unAuditNews->getUpdateTime()
        );
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $unAuditNews->getUpdateTime())
        );
                    
        $this->assertEquals(
            $expectedArray['statusTime'],
            $unAuditNews->getStatusTime()
        );
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $unAuditNews->getStatusTime())
        );
                    
        $this->assertEquals(
            $expectedArray['createTime'],
            $unAuditNews->getCreateTime()
        );
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $unAuditNews->getCreateTime())
        );
    }

    private function coverEquals($expectedArray, $unAuditNews)
    {
        $cover = array();

        if (is_string($expectedArray['cover'])) {
            $cover = json_decode($expectedArray['cover'], true);
        }
        if (is_array($expectedArray['cover'])) {
            $cover = $expectedArray['cover'];
        }

        $this->assertEquals($cover, $unAuditNews->getCover());
    }

    private function attachmentsEquals($expectedArray, $unAuditNews)
    {
        $attachments = array();

        if (is_string($expectedArray['attachments'])) {
            $attachments = json_decode($expectedArray['attachments'], true);
        }
        if (is_array($expectedArray['attachments'])) {
            $attachments = $expectedArray['attachments'];
        }

        $this->assertEquals($attachments, $unAuditNews->getAttachments());
    }

    private function bannerImageEquals($expectedArray, $unAuditNews)
    {
        $bannerImage = array();

        if (is_string($expectedArray['bannerImage'])) {
            $bannerImage = json_decode($expectedArray['bannerImage'], true);
        }
        if (is_array($expectedArray['bannerImage'])) {
            $bannerImage = $expectedArray['bannerImage'];
        }

        $this->assertEquals($bannerImage, $unAuditNews->getBannerImage());
    }
}
