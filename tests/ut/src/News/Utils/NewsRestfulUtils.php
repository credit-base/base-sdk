<?php
namespace Base\Sdk\News\Utils;

trait NewsRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $object->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['title'], $object->getTitle());
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $object->getDimension());

        if (isset($expectedArray['data']['attributes']['description'])) {
            $this->assertEquals($expectedArray['data']['attributes']['description'], $object->getDescription());
        }
        
        $this->assertEquals(
            $expectedArray['data']['attributes']['newsType'],
            $object->getNewsType()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['bannerImage'],
            $object->getBannerImage()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['content'],
            $object->getContent()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['stick'],
            $object->getStick()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['attachments'],
            $object->getAttachments()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['cover'],
            $object->getCover()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['source'],
            $object->getSource()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['bannerStatus'],
            $object->getBannerStatus()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['homePageShowStatus'],
            $object->getHomePageShowStatus()
        );
    }
}
