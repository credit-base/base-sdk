<?php
namespace Base\Sdk\News\Utils;

use Base\Sdk\News\Model\News;

use Base\Sdk\News\Model\UnAuditNews;

use Base\Sdk\Common\Model\ITopAble;
use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Model\IApplyCategory;

class MockFactory
{
    public static function generateCommon(
        News $news,
        int $seed = 0,
        array $value = array()
    ) {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);
        //title
        $title = isset($value['title']) ? $value['title'] : $faker->word();
        $news->setTitle($title);

        //source
        $source = isset($value['source']) ? $value['source'] : $faker->word();
        $news->setSource($source);

        //cover
        $cover = isset($value['cover']) ? $value['cover'] : array(
            'name'=>$faker->name(),
            'identify'=>$faker->word().'.jpg'
        );
        $news->setCover($cover);

        //attachments
        $attachments = isset($value['attachments']) ? $value['attachments'] : array(
            array('name'=>$faker->name(), 'identify'=>$faker->word().'.doc')
        );
        $news->setAttachments($attachments);

        //content
        $content = isset($value['content']) ? $value['content'] : $faker->text();
        $news->setContent($content);

        //newsType
        self::generateNewsType($news, $faker, $value);
        //crew
        self::generateCrew($news, $faker, $value);
        //userGroup
        self::generateUserGroup($news, $faker, $value);
        //bannerStatus
        self::generateBannerStatus($news, $faker, $value);
        //bannerImage
        self::generateBannerImage($news, $faker, $value);
        //dimension
        self::generateDimension($news, $faker, $value);
        //homePageShowStatus
        self::generateHomePageShowStatus($news, $faker, $value);

        self::generateStick($news, $faker, $value);
        
        self::generateStatus($news, $faker, $value);

        $news->setCreateTime($faker->unixTime());
        $news->setUpdateTime($faker->unixTime());
        $news->setStatusTime($faker->unixTime());

        return $news;
    }

    protected static function generateNewsType($news, $faker, $value) : void
    {
        $newsType = isset($value['newsType']) ?
            $value['newsType'] :
            $faker->randomElement(
                NEWS_TYPE
            );
        
        $news->setNewsType($newsType);
    }

    protected static function generateCrew($news, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        unset($faker);
        $news->setCrew($crew);
    }

    protected static function generateUserGroup($news, $faker, $value) : void
    {
        $userGroup = isset($value['userGroup']) ?
        $value['userGroup'] :
        \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($faker->randomDigit());
        
        $news->setUserGroup($userGroup);
    }

    protected static function generateBannerImage($news, $faker, $value) : void
    {
        //image
        $image = isset($value['image']) ? $value['image'] : array(
            'name'=>$faker->name(),
            'identify'=>$faker->word().'.jpg'
        );
        
        $news->setBannerImage($image);
    }

    protected static function generateBannerStatus($news, $faker, $value) : void
    {
        $bannerStatus = isset($value['bannerStatus']) ?
            $value['bannerStatus'] :
            $faker->randomElement(
                News::BANNER_STATUS
            );
        $news->setBannerStatus($bannerStatus);
    }
    
    protected static function generateDimension($news, $faker, $value) : void
    {
        $dimension = isset($value['dimension']) ?
            $value['dimension'] :
            $faker->randomElement(
                News::DIMENSION
            );
        
        $news->setDimension($dimension);
    }

    protected static function generateHomePageShowStatus($news, $faker, $value) : void
    {
        $homePageShowStatus = isset($value['homePageShowStatus']) ?
            $value['homePageShowStatus'] :
            $faker->randomElement(
                News::HOME_PAGE_SHOW_STATUS
            );
        
        $news->setHomePageShowStatus($homePageShowStatus);
    }

    protected static function generateStatus($news, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                IEnableAble::STATUS
            );
        
        $news->setStatus($status);
    }

    protected static function generateStick($news, $faker, $value) : void
    {
        $stick = isset($value['stick']) ?
            $value['stick'] :
            $faker->randomElement(
                ITopAble::STICK
            );
        
        $news->setStick($stick);
    }

    public static function generateNews(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : News {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $news = new News($id);
        $news->setId($id);

        $news = self::generateCommon($news, $seed, $value);

        return $news;
    }

    public static function generateUnAuditedNews(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditNews {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditNews = new UnAuditNews($id);
        
        $unAuditNews->setId($id);

        $unAuditNews = self::generateCommon($unAuditNews, $seed, $value);

        $unAuditNews->setTitle($unAuditNews->getTitle());
        $unAuditNews->setRelation($unAuditNews->getCrew());
        $unAuditNews->setApplyUserGroup($unAuditNews->getUserGroup());
        $unAuditNews->setApplyCrew($unAuditNews->getCrew());
        $unAuditNews->setApplyInfoType($unAuditNews->getNewsType());

         //rejectReason
        $rejectReason = isset($value['rejectReason']) ? $value['rejectReason'] : '内容不合理';
        $unAuditNews->setRejectReason($rejectReason);

        //applyStatus
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomElement(
            IApplyAble::APPLY_STATUS
        );
        $unAuditNews->setApplyStatus($applyStatus);

        //operationType
        $operationType = isset($value['operationType']) ? $value['operationType'] : $faker->randomElement(
            IApplyCategory::OPERATION_TYPE
        );
        $unAuditNews->setOperationType($operationType);
        

        return $unAuditNews;
    }
}
