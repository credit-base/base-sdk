<?php
namespace Base\Sdk\News\Utils;

use Marmot\Framework\Classes\Filter;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Crew\Translator\CrewTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;
use Base\Sdk\News\Translator\INews;

trait NewsUtils
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getUserGroupTranslator() : UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    private function compareArrayAndObjectNews(
        array $expectedArray,
        $news
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $news);
    }

    private function compareArrayAndObjectUnAuditedNews(
        array $expectedArray,
        $unAuditedNews
    ) {
        $this->assertEquals($expectedArray['relation'], $unAuditedNews->getRelation());
        $this->assertEquals($expectedArray['operationType'], $unAuditedNews->getOperationType());
        $this->assertEquals($expectedArray['applyInfoType'], $unAuditedNews->getApplyInfoType());
        $this->assertEquals($expectedArray['rejectReason'], $unAuditedNews->getRejectReason());
        $this->assertEquals($expectedArray['applyStatus'], $unAuditedNews->getApplyStatus());
        $this->assertEquals($expectedArray['applyCrew'], $unAuditedNews->getApplyCrew());
        $this->assertEquals($expectedArray['applyUserGroup'], $unAuditedNews->getApplyUserGroup());
        $this->assertEquals($expectedArray['publishUserGroup'], $unAuditedNews->getPublishUserGroup());
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $news
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($news->getId()));
        $this->assertEquals($expectedArray['title'], $news->getTitle());
        $this->assertEquals($expectedArray['source'], $news->getSource());
        $this->coverEquals($expectedArray, $news);
        $this->attachmentsEquals($expectedArray, $news);
        $this->assertEquals(
            $expectedArray['content'],
            Filter::dhtmlspecialchars(str_replace("ℑ", "", $news->getContent()))
        );
        $this->assertEquals(
            $expectedArray['description'],
            $news->getDescription()
        );
        $this->assertEquals(
            $expectedArray['newsType']['id'],
            marmot_encode($news->getNewsType())
        );
        $this->assertEquals(
            $expectedArray['newsType']['name'],
            NEWS_TYPE_CN[$news->getNewsType()]
        );
        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($news->getCrew())
        );
        $this->assertEquals(
            $expectedArray['publishUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($news->getUserGroup())
        );
        $this->bannerImageEquals($expectedArray, $news);
        $this->assertEquals(
            $expectedArray['dimension']['id'],
            marmot_encode($news->getDimension())
        );
        $this->assertEquals(
            $expectedArray['dimension']['name'],
            INews::DIMENSION_CN[$news->getDimension()]
        );

        $this->assertEquals(
            $expectedArray['stick']['id'],
            marmot_encode($news->getStick())
        );
        $this->assertEquals(
            $expectedArray['stick']['name'],
            IApplyCategory::STICK_CN[$news->getStick()]
        );
        $this->assertEquals(
            $expectedArray['stick']['type'],
            IApplyCategory::STICK_TAG_TYPE[$news->getStick()]
        );
        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($news->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            IApplyCategory::STATUS_CN[$news->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            IApplyCategory::STATUS_TAG_TYPE[$news->getStatus()]
        );

        $this->assertEquals(
            $expectedArray['bannerStatus']['id'],
            marmot_encode($news->getBannerStatus())
        );
        $this->assertEquals(
            $expectedArray['bannerStatus']['name'],
            INews::BANNER_STATUS_CN[$news->getBannerStatus()]
        );
        $this->assertEquals(
            $expectedArray['bannerStatus']['type'],
            INews::BANNER_STATUS_TAG_TYPE[$news->getBannerStatus()]
        );
        $this->assertEquals(
            $expectedArray['homePageShowStatus']['id'],
            marmot_encode($news->getHomePageShowStatus())
        );
        $this->assertEquals(
            $expectedArray['homePageShowStatus']['name'],
            INews::HOME_PAGE_SHOW_STATUS_CN[$news->getHomePageShowStatus()]
        );
        $this->assertEquals(
            $expectedArray['homePageShowStatus']['type'],
            INews::HOME_PAGE_SHOW_STATUS_TAG_TYPE[$news->getHomePageShowStatus()]
        );

        $this->assertEquals(
            $expectedArray['updateTime'],
            $news->getUpdateTime()
        );
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $news->getUpdateTime())
        );
                    
        $this->assertEquals(
            $expectedArray['statusTime'],
            $news->getStatusTime()
        );
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $news->getStatusTime())
        );
                    
        $this->assertEquals(
            $expectedArray['createTime'],
            $news->getCreateTime()
        );
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $news->getCreateTime())
        );
    }

    private function coverEquals($expectedArray, $news)
    {
        $cover = array();

        if (is_string($expectedArray['cover'])) {
            $cover = json_decode($expectedArray['cover'], true);
        }
        if (is_array($expectedArray['cover'])) {
            $cover = $expectedArray['cover'];
        }

        $this->assertEquals($cover, $news->getCover());
    }

    private function attachmentsEquals($expectedArray, $news)
    {
        $attachments = array();

        if (is_string($expectedArray['attachments'])) {
            $attachments = json_decode($expectedArray['attachments'], true);
        }
        if (is_array($expectedArray['attachments'])) {
            $attachments = $expectedArray['attachments'];
        }

        $this->assertEquals($attachments, $news->getAttachments());
    }

    private function bannerImageEquals($expectedArray, $news)
    {
        $bannerImage = array();

        if (is_string($expectedArray['bannerImage'])) {
            $bannerImage = json_decode($expectedArray['bannerImage'], true);
        }
        if (is_array($expectedArray['bannerImage'])) {
            $bannerImage = $expectedArray['bannerImage'];
        }

        $this->assertEquals($bannerImage, $news->getBannerImage());
    }
}
