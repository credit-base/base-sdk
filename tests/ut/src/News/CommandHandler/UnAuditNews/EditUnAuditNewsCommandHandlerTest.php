<?php
namespace Base\Sdk\News\CommandHandler\UnAuditNews;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Repository\UnAuditNewsRepository;
use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\News\Command\UnAuditNews\EditUnAuditNewsCommand;

use Base\Sdk\News\Command\NewsCommandDataTrait;

class EditUnAuditNewsCommandHandlerTest extends TestCase
{
    use NewsCommandDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditUnAuditNewsCommandHandler::class)
            ->setMethods(['getRepository', 'getCrewRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockEditUnAuditNewsCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\UnAuditNewsRepository',
            $commandHandler->getRepository()
        );
    }

    public function testExecute()
    {
        $this->fakerData = $this->getRequestCommonData();
        $id = 1;
        
        $command = new EditUnAuditNewsCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['content'],
            $this->fakerData['newsType'],
            $this->fakerData['dimension'],
            $this->fakerData['status'],
            $this->fakerData['stick'],
            $this->fakerData['bannerStatus'],
            $this->fakerData['homePageShowStatus'],
            $this->fakerData['cover'],
            $this->fakerData['attachments'],
            $this->fakerData['bannerImage'],
            $id
        );

        $unAuditNews = $this->prophesize(UnAuditNews::class);

        $unAuditNews->setCover(Argument::exact($command->cover))
             ->shouldBeCalledTimes(1);
        $unAuditNews->setStick(Argument::exact($command->stick))
             ->shouldBeCalledTimes(1);
        $unAuditNews->setTitle(Argument::exact($command->title))
             ->shouldBeCalledTimes(1);
        $unAuditNews->setSource(Argument::exact($command->source))
             ->shouldBeCalledTimes(1);
        $unAuditNews->setStatus(Argument::exact($command->status))
             ->shouldBeCalledTimes(1);
        $unAuditNews->setContent(Argument::exact($command->content))
             ->shouldBeCalledTimes(1);
        $unAuditNews->setNewsType(Argument::exact($command->newsType))
             ->shouldBeCalledTimes(1);
        $unAuditNews->setDimension(Argument::exact($command->dimension))
             ->shouldBeCalledTimes(1);
        $unAuditNews->setAttachments(Argument::exact($command->attachments))
             ->shouldBeCalledTimes(1);
        $unAuditNews->setBannerImage(Argument::exact($command->bannerImage))
             ->shouldBeCalledTimes(1);
        $unAuditNews->setBannerStatus(Argument::exact($command->bannerStatus))
             ->shouldBeCalledTimes(1);
        $unAuditNews->setHomePageShowStatus(Argument::exact($command->homePageShowStatus))
             ->shouldBeCalledTimes(1);
        $unAuditNews->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $unAuditNewsRepository = $this->prophesize(UnAuditNewsRepository::class);
        $unAuditNewsRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($unAuditNews->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($unAuditNewsRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
