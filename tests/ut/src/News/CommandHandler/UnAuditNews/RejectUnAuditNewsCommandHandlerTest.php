<?php
namespace Base\Sdk\News\CommandHandler\UnAuditNews;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Repository\UnAuditNewsRepository;

use Base\Sdk\News\Command\UnAuditNews\RejectUnAuditNewsCommand;

class RejectUnAuditNewsCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockRejectUnAuditNewsCommandHandler::class)
            ->setMethods(['fetchUnAuditNews'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $commandHandler = $this->getMockBuilder(MockRejectUnAuditNewsCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $unAuditNews = \Base\Sdk\News\Utils\MockFactory::generateUnAuditedNews($id);

        $repository = $this->prophesize(UnAuditNewsRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditNews);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditNews);
    }
}
