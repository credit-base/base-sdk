<?php
namespace Base\Sdk\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Repository\NewsRepository;
use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\News\Command\News\MoveNewsCommand;

use Base\Sdk\News\Command\NewsCommandDataTrait;

class MoveNewsCommandHandlerTest extends TestCase
{
    use NewsCommandDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(MoveNewsCommandHandler::class)
            ->setMethods(['getRepository', 'getCrewRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockMoveNewsCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\NewsRepository',
            $commandHandler->getRepository()
        );
    }

    public function testExecute()
    {
        $this->fakerData = $this->getRequestCommonData();
        $id = 1;
        
        $command = new MoveNewsCommand(
            $this->fakerData['newsType'],
            $id
        );

        $news = $this->prophesize(News::class);
    
        $news->setNewsType(Argument::exact($command->newType))
        ->shouldBeCalledTimes(1);
        $news->move()->shouldBeCalledTimes(1)->willReturn(true);

        $newsRepository = $this->prophesize(NewsRepository::class);
        $newsRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($news->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($newsRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
