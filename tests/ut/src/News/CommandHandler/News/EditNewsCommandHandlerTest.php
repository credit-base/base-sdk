<?php
namespace Base\Sdk\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Repository\NewsRepository;
use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\News\Command\News\EditNewsCommand;

use Base\Sdk\News\Command\NewsCommandDataTrait;

class EditNewsCommandHandlerTest extends TestCase
{
    use NewsCommandDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditNewsCommandHandler::class)
            ->setMethods(['getRepository', 'getCrewRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockEditNewsCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\NewsRepository',
            $commandHandler->getRepository()
        );
    }

    public function testExecute()
    {
        $this->fakerData = $this->getRequestCommonData();
        $id = 1;
        
        $command = new EditNewsCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['content'],
            $this->fakerData['newsType'],
            $this->fakerData['dimension'],
            $this->fakerData['status'],
            $this->fakerData['stick'],
            $this->fakerData['bannerStatus'],
            $this->fakerData['homePageShowStatus'],
            $this->fakerData['cover'],
            $this->fakerData['attachments'],
            $this->fakerData['bannerImage'],
            $id
        );

        $news = $this->prophesize(News::class);
        $news->setCover(Argument::exact($command->cover))
             ->shouldBeCalledTimes(1);
        $news->setStick(Argument::exact($command->stick))
             ->shouldBeCalledTimes(1);
        $news->setTitle(Argument::exact($command->title))
             ->shouldBeCalledTimes(1);
        $news->setSource(Argument::exact($command->source))
             ->shouldBeCalledTimes(1);
        $news->setStatus(Argument::exact($command->status))
             ->shouldBeCalledTimes(1);
        $news->setContent(Argument::exact($command->content))
             ->shouldBeCalledTimes(1);
        $news->setNewsType(Argument::exact($command->newsType))
             ->shouldBeCalledTimes(1);
        $news->setDimension(Argument::exact($command->dimension))
             ->shouldBeCalledTimes(1);
        $news->setAttachments(Argument::exact($command->attachments))
             ->shouldBeCalledTimes(1);
        $news->setBannerImage(Argument::exact($command->bannerImage))
             ->shouldBeCalledTimes(1);
        $news->setBannerStatus(Argument::exact($command->bannerStatus))
             ->shouldBeCalledTimes(1);
        $news->setHomePageShowStatus(Argument::exact($command->homePageShowStatus))
             ->shouldBeCalledTimes(1);
        $news->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $newsRepository = $this->prophesize(NewsRepository::class);
        $newsRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($news->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($newsRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
