<?php
namespace Base\Sdk\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Repository\NewsRepository;

use Base\Sdk\News\Command\News\EnableNewsCommand;

class EnableNewsCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockEnableNewsCommandHandler::class)
            ->setMethods(['fetchNews'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\EnableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $commandHandler = $this->getMockBuilder(MockEnableNewsCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews($id);

        $repository = $this->prophesize(NewsRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($news);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIEnableObject($id);

        $this->assertEquals($result, $news);
    }
}
