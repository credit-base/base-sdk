<?php
namespace Base\Sdk\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Repository\NewsRepository;

use Base\Sdk\News\Command\News\TopNewsCommand;

class TopNewsCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockTopNewsCommandHandler::class)
            ->setMethods(['fetchNews'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\TopCommandHandler',
            $this->stub
        );
    }

    public function testFetchITopObject()
    {
        $commandHandler = $this->getMockBuilder(MockTopNewsCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews($id);

        $repository = $this->prophesize(NewsRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($news);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchITopObject($id);

        $this->assertEquals($result, $news);
    }
}
