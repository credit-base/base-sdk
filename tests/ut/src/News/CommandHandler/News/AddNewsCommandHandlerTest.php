<?php
namespace Base\Sdk\News\CommandHandler\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\News\Model\News;
use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\News\Command\News\AddNewsCommand;

use Base\Sdk\News\Command\NewsCommandDataTrait;

class AddNewsCommandHandlerTest extends TestCase
{
    use NewsCommandDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddNewsCommandHandler::class)
            ->setMethods(['getNews', 'getCrewRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetNews()
    {
        $commandHandler = new MockAddNewsCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\News\Model\News',
            $commandHandler->getNews()
        );
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }

    public function initialExecute()
    {
        $this->fakerData = $this->getRequestCommonData();
        
        $command = new AddNewsCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['content'],
            $this->fakerData['newsType'],
            $this->fakerData['dimension'],
            $this->fakerData['status'],
            $this->fakerData['stick'],
            $this->fakerData['bannerStatus'],
            $this->fakerData['homePageShowStatus'],
            $this->fakerData['cover'],
            $this->fakerData['attachments'],
            $this->fakerData['bannerImage']
        );

        $news = $this->prophesize(News::class);
       
        $news->setContent(Argument::exact($command->content))->shouldBeCalledTimes(1);
        $news->setNewsType(Argument::exact($command->newsType))->shouldBeCalledTimes(1);
        $news->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $news->setCover(Argument::exact($command->cover))->shouldBeCalledTimes(1);
        $news->setStick(Argument::exact($command->stick))->shouldBeCalledTimes(1);
        $news->setBannerStatus(Argument::exact($command->bannerStatus))->shouldBeCalledTimes(1);
        $news->setHomePageShowStatus(Argument::exact($command->homePageShowStatus))->shouldBeCalledTimes(1);
        $news->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        $news->setSource(Argument::exact($command->source))->shouldBeCalledTimes(1);
        $news->setStatus(Argument::exact($command->status))->shouldBeCalledTimes(1);
        $news->setAttachments(Argument::exact($command->attachments))->shouldBeCalledTimes(1);
        $news->setBannerImage(Argument::exact($command->bannerImage))->shouldBeCalledTimes(1);
        $news->add()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->any())
            ->method('getNews')
            ->willReturn($news->reveal());

        return $command;
    }
}
