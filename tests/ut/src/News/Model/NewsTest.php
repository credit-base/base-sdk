<?php
namespace Base\Sdk\News\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\News\Repository\NewsRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class NewsTest extends TestCase
{
    private $news;

    public function setUp()
    {
        $this->news = new MockNews();
    }

    public function tearDown()
    {
        unset($this->news);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf('Base\Sdk\UserGroup\Model\UserGroup', $this->news->getUserGroup());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->news->getCrew());
        $this->assertEquals(array(), $this->news->getAttachments());
        $this->assertEquals(array(), $this->news->getCover());
        $this->assertEquals('', $this->news->getTitle());
        $this->assertEquals('', $this->news->getSource());
        $this->assertEquals('', $this->news->getDescription());
        $this->assertEquals('', $this->news->getContent());
        $this->assertEquals(0, $this->news->getNewsType());
        $this->assertEquals(News::STATUS['ENABLED'], $this->news->getStatus());
        $this->assertEquals(News::STICK['DISABLED'], $this->news->getStick());
        $this->assertEquals(News::STICK['DISABLED'], $this->news->getBannerStatus());
        $this->assertEquals(News::STICK['DISABLED'], $this->news->getHomePageShowStatus());
        $this->assertEquals(News::DIMENSION['NULL'], $this->news->getDimension());
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\NewsRepository',
            $this->news->getRepository()
        );
    }

    //UserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 News setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->news->setUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->news->getUserGroup());
    }

    /**
     * 设置 News setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->news->setUserGroup('userGroup');
    }
    //UserGroup 测试 --------------------------------------------------------   end

    //attachments 测试 -------------------------------------------------------- start
    /**
     * 设置 News setAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetAttachmentsCorrectType()
    {
        $this->news->setAttachments(['attachments']);
        $this->assertEquals(['attachments'], $this->news->getAttachments());
    }

    /**
     * 设置 news setAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAttachmentsWrongType()
    {
        $this->news->setAttachments('attachments');
    }
    //attachments 测试 --------------------------------------------------------   end

    //dimension 测试 ------------------------------------------------------ start
    /**
     * 循环测试 news setDimension() 是否符合预定范围
     *
     * @dataProvider dimensionProvider
     */
    public function testSetDimension($actual, $expected)
    {
        $this->news->setDimension($actual);
        $this->assertEquals($expected, $this->news->getDimension());
    }

    /**
     * 循环测试 news setDimension() 数据构建器
     */
    public function dimensionProvider()
    {
        return array(
            array(News::DIMENSION['NULL'], News::DIMENSION['NULL']),
            array(News::DIMENSION['SOCIOLOGY'], News::DIMENSION['SOCIOLOGY']),
            array(News::DIMENSION['GOVERNMENT_AFFAIRS'], News::DIMENSION['GOVERNMENT_AFFAIRS'])
        );
    }

    /**
     * 设置 news setDimension() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDimensionWrongType()
    {
        $this->news->setDimension('string');
    }
    //dimension 测试 ------------------------------------------------------   end

    //Crew 测试 -------------------------------------------------------- start
    /**
     * 设置 news setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->news->setCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->news->getCrew());
    }

    /**
     * 设置 news setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->news->setCrew('Crew');
    }
    //Crew 测试 --------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 news setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->news->setStatus($actual);
        $this->assertEquals($expected, $this->news->getStatus());
    }

    /**
     * 循环测试 news setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(News::STATUS['DISABLED'], News::STATUS['DISABLED']),
            array(News::STATUS['ENABLED'], News::STATUS['ENABLED'])
        );
    }

    /**
     * 设置 news setStatusProvider() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->news->setStatus(['string']);
    }
    //status 测试 ------------------------------------------------------   end

    //stick 测试 ------------------------------------------------------ start
    /**
     * 循环测试 news setStick() 是否符合预定范围
     *
     * @dataProvider stickProvider
     */
    public function testSetStick($actual, $expected)
    {
        $this->news->setStick($actual);
        $this->assertEquals($expected, $this->news->getStick());
    }

    /**
     * 循环测试 news setStick() 数据构建器
     */
    public function stickProvider()
    {
        return array(
            array(News::STICK['DISABLED'], News::STICK['DISABLED']),
            array(News::STICK['ENABLED'], News::STICK['ENABLED'])
        );
    }

    /**
     * 设置 news setStick() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStickWrongType()
    {
        $this->news->setStick(['string']);
    }
    //stick 测试 ------------------------------------------------------   end
    //bannerStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 news setBannerStatus() 是否符合预定范围
     *
     * @dataProvider bannerStatusProvider
     */
    public function testSetBannerStatus($actual, $expected)
    {
        $this->news->setBannerStatus($actual);
        $this->assertEquals($expected, $this->news->getBannerStatus());
    }

    /**
     * 循环测试 news setBannerStatus() 数据构建器
     */
    public function bannerStatusProvider()
    {
        return array(
            array(News::STICK['DISABLED'], News::STICK['DISABLED']),
            array(News::STICK['ENABLED'], News::STICK['ENABLED'])
        );
    }

    /**
     * 设置 news setBannerStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBannerStatusWrongType()
    {
        $this->news->setBannerStatus(['string']);
    }
    //bannerStatus 测试 ------------------------------------------------------   end

    //homePageShowStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 news setHomePageShowStatus() 是否符合预定范围
     *
     * @dataProvider homePageShowStatusProvider
     */
    public function testSetHomePageShowStatus($actual, $expected)
    {
        $this->news->setHomePageShowStatus($actual);
        $this->assertEquals($expected, $this->news->getHomePageShowStatus());
    }

    /**
     * 循环测试 news setHomePageShowStatus() 数据构建器
     */
    public function homePageShowStatusProvider()
    {
        return array(
            array(News::STICK['DISABLED'], News::STICK['DISABLED']),
            array(News::STICK['ENABLED'], News::STICK['ENABLED'])
        );
    }

    /**
     * 设置 news setHomePageShowStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHomePageShowStatusWrongType()
    {
        $this->news->setHomePageShowStatus(['string']);
    }
    //homePageShowStatus 测试 ------------------------------------------------------   end

    //title 测试 -------------------------------------------------------- start
    /**
     * 设置 News setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->news->setTitle('title');
        $this->assertEquals('title', $this->news->getTitle());
    }

    /**
     * 设置 news setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->news->setTitle(['title']);
    }
    //title 测试 --------------------------------------------------------   end

    //source 测试 -------------------------------------------------------- start
    /**
     * 设置 News setSource() 正确的传参类型,期望传值正确
     */
    public function testSetSourceCorrectType()
    {
        $this->news->setSource('source');
        $this->assertEquals('source', $this->news->getSource());
    }

    /**
     * 设置 news setSource() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceWrongType()
    {
        $this->news->setSource(['source']);
    }
    //source 测试 --------------------------------------------------------   end
    //description 测试 -------------------------------------------------------- start
    /**
     * 设置 News setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->news->setDescription('description');
        $this->assertEquals('description', $this->news->getDescription());
    }

    /**
     * 设置 news setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->news->setDescription(['description']);
    }
    //description 测试 --------------------------------------------------------   end

    //content 测试 -------------------------------------------------------- start
    /**
     * 设置 News setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->news->setContent('content');
        $this->assertEquals('content', $this->news->getContent());
    }

    /**
     * 设置 news setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->news->setContent(['content']);
    }
    //content 测试 --------------------------------------------------------   end

    //cover 测试 -------------------------------------------------------- start
    /**
     * 设置 News setCover() 正确的传参类型,期望传值正确
     */
    public function testSetCoverCorrectType()
    {
        $this->news->setCover(['cover']);
        $this->assertEquals(['cover'], $this->news->getCover());
    }

    /**
     * 设置 news setCover() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCoverWrongType()
    {
        $this->news->setCover('cover');
    }
    //cover 测试 --------------------------------------------------------   end

    //newsType 测试 -------------------------------------------------------- start
    /**
     * 设置 News setNewsType() 正确的传参类型,期望传值正确
     */
    public function testSetNewsTypeCorrectType()
    {
        $newsType = 1;
        $this->news->setNewsType($newsType);
        $this->assertEquals($newsType, $this->news->getNewsType());
    }

    /**
     * 设置 news setNewsType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNewsTypeWrongType()
    {
        $this->news->setNewsType('newsType');
    }
    //newsType 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\NewsRepository',
            $this->news->getRepository()
        );
    }

    public function testGetIEnableAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\NewsRepository',
            $this->news->getIEnableAbleAdapter()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\NewsRepository',
            $this->news->getIOperateAbleAdapter()
        );
    }

    public function testGetITopAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\NewsRepository',
            $this->news->getITopAbleAdapter()
        );
    }

    public function testMove()
    {
        $this->stub = $this->getMockBuilder(MockNews::class)
            ->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(NewsRepository::class);
        $repository->move(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->move();
        $this->assertTrue($result);
    }
}
