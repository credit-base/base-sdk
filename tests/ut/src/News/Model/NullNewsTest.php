<?php
namespace Base\Sdk\News\Model;

use Marmot\Core;
use Marmot\Framework\Classes\Server;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Firebase\JWT\JWT;

use Base\Sdk\News\Repository\NewsRepository;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NullNewsTest extends TestCase
{
    private $nullNews;

    public function setUp()
    {
        $this->nullNews = NullNews::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullNews);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsNews()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Model\News',
            $this->nullNews
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullNews
        );
    }

    public function testResourceNotExist()
    {
        $nullNews = new MockNullNews();

        $result = $nullNews->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testMove()
    {
        $nullNews = new MockNullNews();

        $result = $nullNews->move();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
