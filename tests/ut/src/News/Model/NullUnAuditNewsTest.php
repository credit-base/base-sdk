<?php
namespace Base\Sdk\News\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullUnAuditNewsTest extends TestCase
{
    private $nullUnAuditNews;

    public function setUp()
    {
        $this->nullUnAuditNews = NullUnAuditNews::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullUnAuditNews);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsUnAuditNews()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Model\UnAuditNews',
            $this->nullUnAuditNews
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullUnAuditNews
        );
    }

    public function testResourceNotExist()
    {
        $nullUnAuditNews = new MockNullUnAuditNews();

        $result = $nullUnAuditNews->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
