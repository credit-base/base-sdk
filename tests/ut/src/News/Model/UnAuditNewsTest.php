<?php
namespace Base\Sdk\News\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Common\Model\IApplyCategory;
use Base\Sdk\News\Adapter\UnAuditNews\IUnAuditNewsAdapter;
use Base\Sdk\News\Repository\UnAuditNewsRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class UnAuditNewsTest extends TestCase
{
    private $unAuditNews;

    public function setUp()
    {
        $this->unAuditNews = new MockUnAuditNews();
    }

    public function tearDown()
    {
        unset($this->unAuditNews);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf('Base\Sdk\UserGroup\Model\UserGroup', $this->unAuditNews->getPublishUserGroup());
        $this->assertInstanceOf('Base\Sdk\UserGroup\Model\UserGroup', $this->unAuditNews->getApplyUserGroup());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->unAuditNews->getApplyCrew());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->unAuditNews->getRelation());
        $this->assertEquals(0, $this->unAuditNews->getApplyInfoType());
        $this->assertEquals(IApplyCategory::OPERATION_TYPE['NULL'], $this->unAuditNews->getOperationType());
        $this->assertEquals(0, $this->unAuditNews->getApplyStatus());
        $this->assertEquals('', $this->unAuditNews->getRejectReason());
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\UnAuditNewsRepository',
            $this->unAuditNews->getUnAuditNewsRepository()
        );
    }

    //applyUserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 UnAuditNews setApplyUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetApplyUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->unAuditNews->setApplyUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->unAuditNews->getApplyUserGroup());
    }

    /**
     * 设置 UnAuditNews setApplyUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function tesSetApplyUserGroupWrongType()
    {
        $this->unAuditNews->setApplyUserGroup('applyUserGroup');
    }
    //applyUserGroup 测试 --------------------------------------------------------   end

    //publishUserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 UnAuditNews setPublishUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetPublishUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->unAuditNews->setPublishUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->unAuditNews->getPublishUserGroup());
    }

    /**
     * 设置 UnAuditNews setPublishUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function tesSetPublishUserGroupWrongType()
    {
        $this->unAuditNews->setPublishUserGroup('publishUserGroup');
    }
    //publishUserGroup 测试 --------------------------------------------------------   end

    //rejectReason 测试 -------------------------------------------------------- start
    /**
     * 设置 UnAuditNews setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $this->unAuditNews->setRejectReason('rejectReason');
        $this->assertEquals('rejectReason', $this->unAuditNews->getRejectReason());
    }

    /**
     * 设置 UnAuditNews setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->unAuditNews->setRejectReason(['rejectReason']);
    }
    //rejectReason 测试 --------------------------------------------------------   end

    //applyStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 UnAuditNews setApplyStatus() 是否符合预定范围
     *
     * @dataProvider applyStatusProvider
     */
    public function testSetApplyStatus($actual, $expected)
    {
        $this->unAuditNews->setApplyStatus($actual);
        $this->assertEquals($expected, $this->unAuditNews->getApplyStatus());
    }

    /**
     * 循环测试 UnAuditNews setApplyStatus() 数据构建器
     */
    public function applyStatusProvider()
    {
        return array(
            array(UnAuditNews::APPLY_STATUS['NOT_SUBMITTED'], UnAuditNews::APPLY_STATUS['NOT_SUBMITTED']),
            array(UnAuditNews::APPLY_STATUS['PENDING'], UnAuditNews::APPLY_STATUS['PENDING']),
            array(UnAuditNews::APPLY_STATUS['APPROVE'], UnAuditNews::APPLY_STATUS['APPROVE']),
            array(UnAuditNews::APPLY_STATUS['REJECT'], UnAuditNews::APPLY_STATUS['REJECT'])
        );
    }

    /**
     * 设置 UnAuditNews setApplyStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyStatusWrongType()
    {
        $this->unAuditNews->setApplyStatus(['string']);
    }
    //applyStatus 测试 ------------------------------------------------------   end

    //operationType 测试 ------------------------------------------------------ start
    /**
     * 循环测试 UnAuditNews setOperationType() 是否符合预定范围
     *
     * @dataProvider applyStatusProvider
     */
    public function testSetOperationType($actual, $expected)
    {
        $this->unAuditNews->setOperationType($actual);
        $this->assertEquals($expected, $this->unAuditNews->getOperationType());
    }

    /**
     * 循环测试 UnAuditNews setOperationType() 数据构建器
     */
    public function operationTypeProvider()
    {
        return array(
            array(IApplyCategory::OPERATION_TYPE['NULL'], IApplyCategory::OPERATION_TYPE['NULL']),
            array(IApplyCategory::OPERATION_TYPE['ADD'], IApplyCategory::OPERATION_TYPE['ADD']),
            array(IApplyCategory::OPERATION_TYPE['EDIT'], IApplyCategory::OPERATION_TYPE['EDIT']),
            array(IApplyCategory::OPERATION_TYPE['ENABLED'], IApplyCategory::OPERATION_TYPE['ENABLED']),
            array(IApplyCategory::OPERATION_TYPE['DISABLED'], IApplyCategory::OPERATION_TYPE['DISABLED']),
            array(IApplyCategory::OPERATION_TYPE['TOP'], IApplyCategory::OPERATION_TYPE['TOP']),
            array(IApplyCategory::OPERATION_TYPE['CANCEL_TOP'], IApplyCategory::OPERATION_TYPE['CANCEL_TOP']),
            array(IApplyCategory::OPERATION_TYPE['MOVE'], IApplyCategory::OPERATION_TYPE['MOVE']),
        );
    }

    /**
     * 设置 UnAuditNews setOperationType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOperationTypeWrongType()
    {
        $this->unAuditNews->setOperationType(['string']);
    }
    //operationType 测试 ------------------------------------------------------   end

    //applyInfoType 测试 -------------------------------------------------------- start
    /**
     * 设置 UnAuditNews setApplyInfoType() 正确的传参类型,期望传值正确
     */
    public function testSetApplyInfoTypeCorrectType()
    {
        $this->unAuditNews->setApplyInfoType(1);
        $this->assertEquals(1, $this->unAuditNews->getApplyInfoType());
    }

    /**
     * 设置 UnAuditNews setApplyInfoType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyInfoTypeWrongType()
    {
        $this->unAuditNews->setApplyInfoType(['string']);
    }
    //applyInfoType 测试 --------------------------------------------------------   end

    //relation 测试 -------------------------------------------------------- start
    /**
     * 设置 UnAuditNews setRelation() 正确的传参类型,期望传值正确
     */
    public function testSetRelationCorrectType()
    {
        $expectedCrew = new Crew();

        $this->unAuditNews->setRelation($expectedCrew);
        $this->assertEquals($expectedCrew, $this->unAuditNews->getRelation());
    }

    /**
     * 设置 UnAuditNews setRelation() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRelationWrongType()
    {
        $this->unAuditNews->setRelation('Crew');
    }
    //relation 测试 --------------------------------------------------------   end

    //applyCrew 测试 -------------------------------------------------------- start
    /**
     * 设置 UnAuditNews setApplyCrew() 正确的传参类型,期望传值正确
     */
    public function testSetApplyCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->unAuditNews->setApplyCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->unAuditNews->getApplyCrew());
    }

    /**
     * 设置 UnAuditNews setApplyCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyCrewWrongType()
    {
        $this->unAuditNews->setApplyCrew('Crew');
    }
    //applyCrew 测试 --------------------------------------------------------   end

    public function testGetUnAuditNewsRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\UnAuditNewsRepository',
            $this->unAuditNews->getUnAuditNewsRepository()
        );
    }

    public function testGetIApplyAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\UnAuditNewsRepository',
            $this->unAuditNews->getIApplyAbleAdapter()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Repository\UnAuditNewsRepository',
            $this->unAuditNews->getIOperateAbleAdapter()
        );
    }
}
