<?php
namespace Base\Sdk\News\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;

class UnAuditNewsRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditNewsRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIUnAuditNewsAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Adapter\UnAuditNews\IUnAuditNewsAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(UnAuditNewsRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
