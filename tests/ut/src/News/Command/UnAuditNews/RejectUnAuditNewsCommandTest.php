<?php
namespace Base\Sdk\News\Command\UnAuditNews;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Command\NewsCommandDataTrait;

class RejectUnAuditNewsCommandTest extends TestCase
{
    use NewsCommandDataTrait;

    private $stub;

    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = $this->getRequestCommonData();

        $this->stub = new RejectUnAuditNewsCommand(
            '驳回原因',
            $faker->randomNumber()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
