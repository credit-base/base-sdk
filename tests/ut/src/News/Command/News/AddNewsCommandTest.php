<?php
namespace Base\Sdk\News\Command\News;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Command\NewsCommandDataTrait;

class AddNewsCommandTest extends TestCase
{
    use NewsCommandDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = $this->getRequestCommonData();

        $this->command = new AddNewsCommand(
            $this->fakerData['title'],
            $this->fakerData['source'],
            $this->fakerData['content'],
            $this->fakerData['newsType'],
            $this->fakerData['dimension'],
            $this->fakerData['status'],
            $this->fakerData['stick'],
            $this->fakerData['bannerStatus'],
            $this->fakerData['homePageShowStatus'],
            $this->fakerData['cover'],
            $this->fakerData['attachments'],
            $this->fakerData['bannerImage']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
