<?php
namespace Base\Sdk\News\Command\News;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Command\NewsCommandDataTrait;

class TopNewsCommandTest extends TestCase
{
    use NewsCommandDataTrait;

    private $stub;

    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = $this->getRequestCommonData();

        $this->stub = new TopNewsCommand(
            $faker->randomNumber()
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
