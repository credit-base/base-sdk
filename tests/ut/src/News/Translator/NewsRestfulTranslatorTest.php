<?php
namespace Base\Sdk\News\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\News\Utils\NewsRestfulUtils;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

class NewsRestfulTranslatorTest extends TestCase
{
    use NewsRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new NewsRestfulTranslator();

        Core::$container->set('crew', new Crew());
        $this->childTranslator = new class extends NewsRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetDepartmentRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);

        $expression['data']['id'] = $news->getId();
        $expression['data']['attributes']['title'] = $news->getTitle();
        $expression['data']['attributes']['description'] = $news->getDescription();
        $expression['data']['attributes']['dimension'] = $news->getDimension();
        $expression['data']['attributes']['attachments'] = $news->getAttachments();
        $expression['data']['attributes']['newsType'] = $news->getNewsType();
        $expression['data']['attributes']['bannerImage'] = $news->getBannerImage();
        $expression['data']['attributes']['content'] = $news->getContent();
        $expression['data']['attributes']['stick'] = $news->getStick();
        $expression['data']['attributes']['cover'] = $news->getCover();
        $expression['data']['attributes']['source'] = $news->getSource();
        $expression['data']['attributes']['bannerStatus'] = $news->getBannerStatus();
        $expression['data']['attributes']['homePageShowStatus'] = $news->getHomePageShowStatus();

        $expression['data']['attributes']['createTime'] = $news->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $news->getUpdateTime();
        $expression['data']['attributes']['status'] = $news->getStatus();
        $expression['data']['attributes']['statusTime'] = $news->getStatusTime();

        $newsObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\News\Model\News', $newsObject);
        $this->compareArrayAndObjectCommon($expression, $newsObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $news = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\News\Model\NullNews', $news);
    }

    public function testObjectToArray()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);

        $expression = $this->translator->objectToArray($news);

        $this->compareArrayAndObjectCommon($expression, $news);
    }

    public function testObjectToArrayFail()
    {
        $news = null;

        $expression = $this->translator->objectToArray($news);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['department']['data'] 和
     * $relationships['userGroup']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用两次, 分别用于处理 usergroup 和 department 且返回各自的数组 $departmentInfo 和 $userGroupInfo
     * 4. $this->getDepartmentRestfulTranslator()->arrayToObject 被调用一次, 入参是 $departmentInfo, 出参是 $department
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $userGroup
     Info, 出参是 $userGroup
     * 6. $news->setDepartment 调用一次, 入参 $department
     * 7. $news->setUserGroup 调用一次, 入参 $userGroup
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup']
        ];

        $translator = $this->getMockBuilder(NewsRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getCrewRestfulTranslator',
             'getUserGroupRestfulTranslator',
         ])
        ->getMock();

        $crewInfo = ['mockCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];

        $crew = new Crew();
        $publishUserGroup = new UserGroup();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用2次, 依次入参 department, userGroup
        //依次返回 $departmentInfo 和 $userGroupInfo
        $translator->expects($this->exactly(2))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['publishUserGroup']['data']]
             )
            ->will($this->onConsecutiveCalls($crewInfo, $publishUserGroupInfo));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        //揭示
        $news = $translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\News\Model\News', $news);
        $this->assertEquals($crew, $news->getCrew());
        $this->assertEquals($publishUserGroup, $news->getUserGroup());
    }
}
