<?php
namespace Base\Sdk\News\Translator;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\News\Utils\UnAuditNewsRestfulUtils;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

class UnAuditNewsRestfulTranslatorTest extends TestCase
{
    use UnAuditNewsRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditNewsRestfulTranslator();
        $this->childTranslator = new class extends UnAuditNewsRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditNews = \Base\Sdk\News\Utils\MockFactory::generateUnAuditedNews(1);

        $expression['data']['id'] = $unAuditNews->getId();
        $expression['data']['attributes']['title'] = $unAuditNews->getTitle();
        $expression['data']['attributes']['description'] = $unAuditNews->getDescription();
        $expression['data']['attributes']['applyStatus'] = $unAuditNews->getApplyStatus();
        $expression['data']['attributes']['rejectReason'] = $unAuditNews->getRejectReason();
        $expression['data']['attributes']['operationType'] = $unAuditNews->getOperationType();
        $expression['data']['attributes']['applyInfoType'] = $unAuditNews->getApplyInfoType();
        $expression['data']['attributes']['dimension'] = $unAuditNews->getDimension();
        $expression['data']['attributes']['attachments'] = $unAuditNews->getAttachments();
        $expression['data']['attributes']['newsType'] = $unAuditNews->getNewsType();
        $expression['data']['attributes']['bannerImage'] = $unAuditNews->getBannerImage();
        $expression['data']['attributes']['content'] = $unAuditNews->getContent();
        $expression['data']['attributes']['stick'] = $unAuditNews->getStick();
        $expression['data']['attributes']['cover'] = $unAuditNews->getCover();
        $expression['data']['attributes']['source'] = $unAuditNews->getSource();
        $expression['data']['attributes']['bannerStatus'] = $unAuditNews->getBannerStatus();
        $expression['data']['attributes']['homePageShowStatus'] = $unAuditNews->getHomePageShowStatus();

        $expression['data']['attributes']['createTime'] = $unAuditNews->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $unAuditNews->getUpdateTime();
        $expression['data']['attributes']['status'] = $unAuditNews->getStatus();
        $expression['data']['attributes']['statusTime'] = $unAuditNews->getStatusTime();

        $unAuditNewsObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\News\Model\UnAuditNews', $unAuditNewsObject);
        $this->compareArrayAndObjectCommon($expression, $unAuditNewsObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $unAuditNews = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\News\Model\NullUnAuditNews', $unAuditNews);
    }

    public function testObjectToArray()
    {
        $unAuditNews = \Base\Sdk\News\Utils\MockFactory::generateUnAuditedNews(1);

        $expression = $this->translator->objectToArray($unAuditNews);
     
        $this->compareArrayAndObjectCommon($expression, $unAuditNews);
    }

    public function testObjectToArrayFail()
    {
        $unAuditNews = null;

        $expression = $this->translator->objectToArray($unAuditNews);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['department']['data'] 和
     * $relationships['userGroup']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用两次, 分别用于处理 usergroup 和 department 且返回各自的数组 $departmentInfo 和 $userGroupInfo
     * 4. $this->getDepartmentRestfulTranslator()->arrayToObject 被调用一次, 入参是 $departmentInfo, 出参是 $department
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $userGroup
     Info, 出参是 $userGroup
     * 6. $UnAuditNews->setDepartment 调用一次, 入参 $department
     * 7. $UnAuditNews->setUserGroup 调用一次, 入参 $userGroup
     */
    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'relation'=>['data'=>'mockRelation'],
            'applyCrew'=>['data'=>'mockApplyCrew'],
            'applyUserGroup'=>['data'=>'mockApplyUserGroup'],
            'publishUserGroup'=>['data'=>'mockPublishUserGroup'],
            'crew'=>['data'=>'mockCrew'],
        ];

        $translator = $this->getMockBuilder(UnAuditNewsRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getCrewRestfulTranslator',
             'getUserGroupRestfulTranslator',
         ])
        ->getMock();

        $crewInfo = ['mockCrewInfo'];
        $relationInfo = ['mockRelationInfo'];
        $applyCrewInfo = ['mockApplyCrewInfo'];
        $publishUserGroupInfo = ['mockPublishUserGroupInfo'];
        $applyUserGroupInfo = ['mockApplyUserGroupInfo'];

        $crew = new Crew();
        $applyCrew = new Crew();
        $relation = new Crew();
        $publishUserGroup = new UserGroup();
        $applyUserGroup = new UserGroup();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用5次, 依次入参 department, userGroup
        //依次返回 $departmentInfo 和 $userGroupInfo
        $translator->expects($this->exactly(5))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['relation']['data']],
                 [$relationships['applyCrew']['data']],
                 [$relationships['applyUserGroup']['data']],
                 [$relationships['publishUserGroup']['data']],
                 [$relationships['crew']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $relationInfo,
                $applyCrewInfo,
                $applyUserGroupInfo,
                $publishUserGroupInfo,
                $crewInfo
            ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($publishUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($publishUserGroup);

        $userGroupRestfulTranslator->arrayToObject(Argument::exact($applyUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyUserGroup);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        
        $crewRestfulTranslator->arrayToObject(Argument::exact($relationInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($relation);

        $crewRestfulTranslator->arrayToObject(Argument::exact($applyCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyCrew);
        
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);
        //绑定
        $translator->expects($this->exactly(2))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(3))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
       
        //揭示
        $unAuditNews = $translator->arrayToObject($expression);
         
        $this->assertInstanceof('Base\Sdk\News\Model\UnAuditNews', $unAuditNews);
        $this->assertEquals($crew, $unAuditNews->getCrew());
        $this->assertEquals($relation, $unAuditNews->getRelation());
        $this->assertEquals($applyCrew, $unAuditNews->getApplyCrew());
        $this->assertEquals($publishUserGroup, $unAuditNews->getPublishUserGroup());
        $this->assertEquals($applyUserGroup, $unAuditNews->getApplyUserGroup());
    }
}
