<?php
namespace Base\Sdk\News\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Utils\NewsUtils;

class NewsTranslatorTest extends TestCase
{
    use NewsUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new NewsTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\News\Model\NullNews', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);

        $expression = $this->translator->objectToArray($news);
    
        $this->compareArrayAndObjectNews($expression, $news);
    }

    public function testObjectToArrayFail()
    {
        $news = null;

        $expression = $this->translator->objectToArray($news);
        $this->assertEquals(array(), $expression);
    }
}
