<?php
namespace Base\Sdk\News\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Utils\UnAuditNewsUtils;

class UnAuditNewsTranslatorTest extends TestCase
{
    use UnAuditNewsUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditNewsTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\News\Model\NullUnAuditNews', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $unAuditNews = \Base\Sdk\News\Utils\MockFactory::generateUnAuditedNews(1);

        $expression = $this->translator->objectToArray($unAuditNews);
    
        $this->compareArrayAndObjectUnAuditNews($expression, $unAuditNews);
    }

    public function testObjectToArrayFail()
    {
        $unAuditNews = null;

        $expression = $this->translator->objectToArray($unAuditNews);
        $this->assertEquals(array(), $expression);
    }
}
