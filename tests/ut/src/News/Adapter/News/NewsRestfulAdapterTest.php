<?php
namespace Base\Sdk\News\Adapter\News;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Model\NullNews;
use Base\Sdk\News\Translator\NewsRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class NewsRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(NewsRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends NewsRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getNewsMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsINewsAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Adapter\News\INewsAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('news', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'NEWS_LIST',
                NewsRestfulAdapter::SCENARIOS['NEWS_LIST']
            ],
            [
                'NEWS_FETCH_ONE',
                NewsRestfulAdapter::SCENARIOS['NEWS_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    public function testFetchOne()
    {
        $id = 1;

        $news = \Base\Sdk\News\Utils\MockFactory::generateNews($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullNews::getInstance())
            ->willReturn($news);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($news, $result);
    }

    /**
     * 为NewsRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$News,$keys,$NewsArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareNewsTranslator(
        News $news,
        array $keys,
        array $newsArray
    ) {
        $translator = $this->prophesize(NewsRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($news),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($newsArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(News $news)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($news);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew'
            ),
            $newsArray
        );

        $this->adapter
            ->method('post')
            ->with('', $newsArray);

        $this->success($news);
        $result = $this->adapter->add($news);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew'
            ),
            $newsArray
        );

        $this->adapter
            ->method('post')
            ->with('', $newsArray);
        
            $this->failure($news);
            $result = $this->adapter->add($news);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew'
            ),
            $newsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId(), $newsArray);

        $this->success($news);
        $result = $this->adapter->edit($news);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew'
            ),
            $newsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId(), $newsArray);
        
            $this->failure($news);
            $result = $this->adapter->edit($news);
            $this->assertFalse($result);
    }

/**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行enable（）
     * 判断 result 是否为true
     */
    public function testEnableSuccess()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array('crew'),
            $newsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId().'/enable', $newsArray);

        $this->success($news);
        $result = $this->adapter->enable($news);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行enable（）
     * 判断 result 是否为false
     */
    public function testEnableFailure()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array('crew'),
            $newsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId().'/enable', $newsArray);
        
        $this->failure($news);
        $result = $this->adapter->enable($news);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行disable（）
     * 判断 result 是否为true
     */
    public function testDisableSuccess()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array('crew'),
            $newsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId().'/disable', $newsArray);

        $this->success($news);
        $result = $this->adapter->disable($news);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行disable（）
     * 判断 result 是否为false
     */
    public function testDisableFailure()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array('crew'),
            $newsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId().'/disable', $newsArray);
        
        $this->failure($news);
        $result = $this->adapter->disable($news);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行cancelTop（）
     * 判断 result 是否为true
     */
    public function testCancelTopSuccess()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array('crew'),
            $newsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId().'/cancelTop', $newsArray);

        $this->success($news);
        $result = $this->adapter->cancelTop($news);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行cancelTop（）
     * 判断 result 是否为false
     */
    public function testCancelTopFailure()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array('crew'),
            $newsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId().'/cancelTop', $newsArray);
        
        $this->failure($news);
        $result = $this->adapter->cancelTop($news);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行move（）
     * 判断 result 是否为true
     */
    public function testMoveSuccess()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
     
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array('crew'),
            $newsArray
        );
       
        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId().'/move'.'/'.$news->getNewsType(), $newsArray);

        $this->success($news);
        $result = $this->adapter->move($news);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行move（）
     * 判断 result 是否为false
     */
    public function testMoveFailure()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array('crew'),
            $newsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId().'/move'.'/'.$news->getNewsType(), $newsArray);
        
        $this->failure($news);
        $result = $this->adapter->move($news);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行top（）
     * 判断 result 是否为true
     */
    public function testTopSuccess()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array('crew'),
            $newsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId().'/top', $newsArray);

        $this->success($news);
        $result = $this->adapter->top($news);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行top（）
     * 判断 result 是否为false
     */
    public function testTopFailure()
    {
        $news = \Base\Sdk\News\Utils\MockFactory::generateNews(1);
        $newsArray = array();

        $this->prepareNewsTranslator(
            $news,
            array('crew'),
            $newsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$news->getId().'/top', $newsArray);
        
        $this->failure($news);
        $result = $this->adapter->top($news);
        $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [
            101 => [
                'title' => TITLE_FORMAT_ERROR,
                'source' => SOURCE_FORMAT_ERROR,
                'cover' => IMAGE_FORMAT_ERROR,
                'attachments' => ATTACHMENT_FORMAT_ERROR,
                'content' => PARAMETER_FORMAT_ERROR,
                'status' => STATUS_FORMAT_ERROR,
                'stick' => STATUS_FORMAT_ERROR,
                'newsType' => NEWS_TYPE_NOT_EXIST,
                'dimension' => DIMENSION_NOT_EXIST,
                'homePageShowStatus' => HOME_PAGE_SHOW_STATUS_NOT_EXIST,
                'bannerStatus' => BANNER_STATUS_NOT_EXIST,
                'bannerImage' => IMAGE_FORMAT_ERROR,
                'crewId' => PARAMETER_FORMAT_ERROR,
                'applyCrewId' => PARAMETER_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR
            ]
        ];
        
        $result = $this->childAdapter->getNewsMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
