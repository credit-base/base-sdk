<?php
namespace Base\Sdk\News\Adapter\UnAuditNews;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Model\NullUnAuditNews;
use Base\Sdk\News\Translator\UnAuditNewsRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class UnAuditNewsRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UnAuditNewsRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends UnAuditNewsRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getUnAuditNewsMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIUnAuditNewsAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\News\Adapter\UnAuditNews\IUnAuditNewsAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedNews', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDIT_NEWS_LIST',
                UnAuditNewsRestfulAdapter::SCENARIOS['UN_AUDIT_NEWS_LIST']
            ],
            [
                'UN_AUDIT_NEWS_FETCH_ONE',
                UnAuditNewsRestfulAdapter::SCENARIOS['UN_AUDIT_NEWS_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    public function testFetchOne()
    {
        $id = 1;

        $unAuditNews = \Base\Sdk\News\Utils\MockFactory::generateUnAuditedNews($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullUnAuditNews::getInstance())
            ->willReturn($unAuditNews);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($unAuditNews, $result);
    }

    /**
     * 为UnAuditNewsRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$UnAuditNews,$keys,$UnAuditNewsArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareUnAuditNewsTranslator(
        UnAuditNews $unAuditNews,
        array $keys,
        array $unAuditNewsArray
    ) {
        $translator = $this->prophesize(UnAuditNewsRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($unAuditNews),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($unAuditNewsArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(UnAuditNews $unAuditNews)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($unAuditNews);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAdd()
    {
        $unAuditNews = new NullUnAuditNews(1);
        $result = $this->adapter->add($unAuditNews);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditNewsTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $unAuditNews = \Base\Sdk\News\Utils\MockFactory::generateUnAuditedNews(1);
        $unAuditNewsArray = array();

        $this->prepareUnAuditNewsTranslator(
            $unAuditNews,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew'
            ),
            $unAuditNewsArray
        );
        
        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditNews->getId().'/'.'resubmit', $unAuditNewsArray);
            
        $this->success($unAuditNews);
        $result = $this->adapter->edit($unAuditNews);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditNewsTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $unAuditNews = \Base\Sdk\News\Utils\MockFactory::generateUnAuditedNews(1);
        $unAuditNewsArray = array();

        $this->prepareUnAuditNewsTranslator(
            $unAuditNews,
            array(
                'title',
                'source',
                'cover',
                'attachments',
                'content',
                'newsType',
                'dimension',
                'status',
                'stick',
                'bannerStatus',
                'bannerImage',
                'homePageShowStatus',
                'crew'
            ),
            $unAuditNewsArray
        );
        
        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditNews->getId().'/'.'resubmit', $unAuditNewsArray);
           
            $this->failure($unAuditNews);
            $result = $this->adapter->edit($unAuditNews);
            $this->assertFalse($result);
    }

/**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行Approve（）
     * 判断 result 是否为true
     */
    public function testApproveSuccess()
    {
        $unAuditNews = \Base\Sdk\News\Utils\MockFactory::generateUnAuditedNews(1);
        $unAuditNewsArray = array();

        $this->prepareUnAuditNewsTranslator(
            $unAuditNews,
            array('applyCrew'),
            $unAuditNewsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditNews->getId().'/approve', $unAuditNewsArray);

        $this->success($unAuditNews);
        $result = $this->adapter->Approve($unAuditNews);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行Approve（）
     * 判断 result 是否为false
     */
    public function testApproveFailure()
    {
        $unAuditNews = \Base\Sdk\News\Utils\MockFactory::generateUnAuditedNews(1);
        $unAuditNewsArray = array();

        $this->prepareUnAuditNewsTranslator(
            $unAuditNews,
            array('applyCrew'),
            $unAuditNewsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditNews->getId().'/approve', $unAuditNewsArray);
        
        $this->failure($unAuditNews);
        $result = $this->adapter->Approve($unAuditNews);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行Reject（）
     * 判断 result 是否为true
     */
    public function testRejectSuccess()
    {
        $unAuditNews = \Base\Sdk\News\Utils\MockFactory::generateUnAuditedNews(1);
        $unAuditNewsArray = array();

        $this->prepareUnAuditNewsTranslator(
            $unAuditNews,
            array('rejectReason',
            'applyCrew'),
            $unAuditNewsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditNews->getId().'/reject', $unAuditNewsArray);

        $this->success($unAuditNews);
        $result = $this->adapter->Reject($unAuditNews);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareUnAuditNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行Reject（）
     * 判断 result 是否为false
     */
    public function testRejectFailure()
    {
        $unAuditNews = \Base\Sdk\News\Utils\MockFactory::generateUnAuditedNews(1);
        $unAuditNewsArray = array();

        $this->prepareUnAuditNewsTranslator(
            $unAuditNews,
            array('rejectReason',
            'applyCrew'),
            $unAuditNewsArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditNews->getId().'/reject', $unAuditNewsArray);
        
        $this->failure($unAuditNews);
        $result = $this->adapter->Reject($unAuditNews);
        $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [
            101 => [
                'crewId' => PARAMETER_FORMAT_ERROR,
                'applyCrewId' => PARAMETER_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
                'title' => TITLE_FORMAT_ERROR,
                'source' => SOURCE_FORMAT_ERROR,
                'cover' => IMAGE_FORMAT_ERROR,
                'attachments' => ATTACHMENT_FORMAT_ERROR,
                'content' => PARAMETER_FORMAT_ERROR,
                'dimension' => DIMENSION_NOT_EXIST,
                'homePageShowStatus' => HOME_PAGE_SHOW_STATUS_NOT_EXIST,
                'bannerStatus' => BANNER_STATUS_NOT_EXIST,
                'bannerImage' => IMAGE_FORMAT_ERROR,
                'status' => STATUS_FORMAT_ERROR,
                'stick' => STATUS_FORMAT_ERROR,
                'newsType' => NEWS_TYPE_NOT_EXIST,
            ]
        ];
        
        $result = $this->childAdapter->getUnAuditNewsMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
