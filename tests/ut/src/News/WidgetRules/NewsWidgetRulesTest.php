<?php
namespace Base\Sdk\News\WidgetRules;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Sdk\News\Model\News;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class NewsWidgetRulesTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = NewsWidgetRules::getInstance();

        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //newsType -- start
    /**
     * @dataProvider invalidNewsTypeProvider
     */
    public function testNewsTypeInvalid($actual, $expected)
    {
        $result = $this->widgetRule->newsType($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(NEWS_TYPE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function invalidNewsTypeProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $errorNewsType = [1,2,3];

        return array(
            array($faker->randomElement($errorNewsType), false),
            array($faker->randomElement(NEWS_TYPE), true),
            array('string', false),
            array('', false),
            array(0, false),
        );
    }
    //newsType -- end

    //dimension -- start
    /**
     * @dataProvider invalidDimensionProvider
     */
    public function testDimensionInvalid($actual, $expected)
    {
        $result = $this->widgetRule->dimension($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DIMENSION_NOT_EXIST, Core::getLastError()->getId());
    }

    public function invalidDimensionProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed(0);

        $error = [-2,6];
        $dimension = [1,2];
        return array(
            array($faker->randomElement($error), false),
            array($faker->randomElement($dimension), true),
            array(0, false),
            array('', false)
        );
    }
    //dimension -- end

    //homePageShowStatus -- start
    /**
     * @dataProvider invalidHomePageShowStatusProvider
     */
    public function testHomePageShowStatusInvalid($actual, $expected)
    {
        $result = $this->widgetRule->homePageShowStatus($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(HOME_PAGE_SHOW_STATUS_NOT_EXIST, Core::getLastError()->getId());
    }

    public function invalidHomePageShowStatusProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed(0);

        $error = [-2,6];
        return array(
            array($faker->randomElement($error), false),
            array($faker->randomElement(News::HOME_PAGE_SHOW_STATUS), true)
        );
    }
    //homePageShowStatus -- end

    //bannerImage -- start
    public function testBannerStatusSuccess()
    {
        $bannerStatus = 0;
        $bannerImage = array();
        $result = $this->widgetRule->bannerImage($bannerStatus, $bannerImage);

        $this->assertTrue($result);
    }

    public function testBannerStatusFail()
    {
        $bannerStatus = -2;
        $bannerImage = array();
        $result = $this->widgetRule->bannerImage($bannerStatus, $bannerImage);

        $this->assertFalse($result);
        $this->assertEquals(BANNER_STATUS_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testBannerImageSuccess()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $bannerStatus = 2;
        $bannerImage =  array(
            'name'=>$faker->name(),
            'identify'=>$faker->word().'.jpg'
        );
        $result = $this->widgetRule->bannerImage($bannerStatus, $bannerImage);

        $this->assertTrue($result);
    }

    public function testBannerImageFail()
    {
        $bannerStatus = -2;
        $bannerImage = 'string';
        $result = $this->widgetRule->bannerImage($bannerStatus, $bannerImage);

        $this->assertFalse($result);
    }
    //bannerImage -- end
}
