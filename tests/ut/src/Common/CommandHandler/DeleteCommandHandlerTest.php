<?php
namespace Base\Sdk\Common\CommandHandler;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\Command\DeleteCommand;

use PHPUnit\Framework\TestCase;

class DeleteCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(DeleteCommandHandler::class)
            ->setMethods(['fetchIModifyStatusObject'])
            ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }

    public function testExecute()
    {
        $id =1;

        $command = new class($id) extends DeleteCommand
        {

        };

        $delete = $this->prophesize(IModifyStatusAble::class);
        $delete->deletes()->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('fetchIModifyStatusObject')
            ->with($id)
            ->willReturn($delete->reveal());

        $result = $this->stub->execute($command);
        $this->assertTrue($result);
    }
}
