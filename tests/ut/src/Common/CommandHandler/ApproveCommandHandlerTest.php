<?php
namespace Base\Sdk\Common\CommandHandler;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Command\ApproveCommand;

use PHPUnit\Framework\TestCase;

class ApproveCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(ApproveCommandHandler::class)
            ->setMethods(['fetchIApplyObject'])
            ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }

    public function testExecute()
    {
        $id =1;

        $command = new class($id) extends ApproveCommand
        {

        };

        $approve = $this->prophesize(IApplyAble::class);
        $approve->approve()->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('fetchIApplyObject')
            ->with($id)
            ->willReturn($approve->reveal());

        $result = $this->stub->execute($command);
        $this->assertTrue($result);
    }
}
