<?php
namespace Base\Sdk\Common\CommandHandler;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\Command\RevokeCommand;

use PHPUnit\Framework\TestCase;

class RevokeCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(RevokeCommandHandler::class)
            ->setMethods(['fetchIModifyStatusObject'])
            ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }

    public function testExecute()
    {
        $id = 1;

        $command = new class($id) extends RevokeCommand
        {
        };

        $revokeAble = $this->prophesize(IModifyStatusAble::class);
        $revokeAble->revoke()->shouldBeCalledTimes(1)->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('fetchIModifyStatusObject')
            ->with($id)
            ->willReturn($revokeAble->reveal());

        $result = $this->stub->execute($command);
        $this->assertTrue($result);
    }
}
