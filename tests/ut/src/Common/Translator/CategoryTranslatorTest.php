<?php
namespace Base\Sdk\Common\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\MockCategory;

class CategoryTranslatorTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder('Base\Sdk\Common\Translator\CategoryTranslator')
                                 ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjects()
    {

        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $category = new MockCategory(1, 'name');
        $expression = $this->translator->objectToArray($category);

        $this->assertEquals($expression['id'], marmot_encode($category->getId()));
        $this->assertEquals($expression['name'], $category->getName());
    }

    public function testObjectToArrayFail()
    {
        $category = null;

        $expression = $this->translator->objectToArray($category);
        $this->assertEquals(array(), $expression);
    }
}
