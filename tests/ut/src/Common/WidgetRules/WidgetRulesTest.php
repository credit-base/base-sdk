<?php
namespace Base\Sdk\Common\WidgetRules;

use Marmot\Core;

use Base\Sdk\Common\Utils\StringGenerate;
use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\Model\ITopAble;

use PHPUnit\Framework\TestCase;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class WidgetRulesTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = WidgetRules::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    /**
     * @dataProvider formatNumericProvider
     */
    public function testFormatNumeric($actual, $expected, $pointer)
    {
        $result = $this->stub->formatNumeric($actual, $pointer);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
            $this->assertEquals(array('pointer' => $pointer), Core::getLastError()->getSource());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function formatNumericProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomDigit, true, ''),
            array($faker->word, false, ''),
            array([], false, '')
        );
    }

    /**
     * @dataProvider formatStringProvider
     */
    public function testFormatString($actual, $expected, $pointer)
    {
        $result = $this->stub->formatString($actual, $pointer);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
            $this->assertEquals(array('pointer' => $pointer), Core::getLastError()->getSource());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function formatStringProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomDigit, false, ''),
            array($faker->word, true, ''),
            array([], false, '')
        );
    }

    /**
     * @dataProvider formatArrayProvider
     */
    public function testFormatArray($actual, $expected, $pointer)
    {
        $result = $this->stub->formatArray($actual, $pointer);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
            $this->assertEquals(array('pointer' => $pointer), Core::getLastError()->getSource());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function formatArrayProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        
        return array(
            array($faker->randomDigit, false, ''),
            array($faker->word, false, ''),
            array([], true, '')
        );
    }

    /**
     * @dataProvider imageProvider
     */
    public function testImage($actual, $expected)
    {
        $result = $this->stub->image($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(IMAGE_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function imageProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(['name'=>$faker->word, 'identify'=>'1.jpg'], true),
            array($faker->word, false),
            array(['name'=>$faker->word], false),
            array(['name'=>$faker->word, 'identify'=>$faker->word], false),
        );
    }

    /**
     * @dataProvider attachmentsProvider
     */
    public function testAttachments($actual, $expected)
    {
        $result = $this->stub->attachments($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(ATTACHMENT_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function attachmentsProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        
        return array(
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.zip'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.doc'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.docx'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.xlsx'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.xls'],
                ], true),
            array(
                    [
                        ['name'=>$faker->word,'identify'=>$faker->word.'.zip'],
                        ['name'=>$faker->word,'identify'=>$faker->word.'.doc'],
                        ['name'=>$faker->word,'identify'=>$faker->word.'.docx'],
                        ['name'=>$faker->word,'identify'=>$faker->word.'.xlsx'],
                        ['name'=>$faker->word,'identify'=>$faker->word.'.xls'],
                        ['name'=>$faker->word,'identify'=>$faker->word.'.pdf'],
                    ], false),
            array($faker->word, false),
            array(
                [
                    ['name'=>$faker->word, 'identify'=>$faker->word],
                    ['name'=>$faker->word, 'identify'=>$faker->word]
                ], false),
        );
    }

    //title -- start
    /**
     * @dataProvider invalidTitleProvider
     */
    public function testTitle($actual, $expected)
    {
        $result = $this->stub->title($actual);

        if (!$expected) {
            $this->assertFalse($result);
            
            $this->assertEquals(TITLE_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidTitleProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('titletitle', true),
            array('titl', false),
            array('', false),
            array($faker->randomDigit, false)
        );
    }
    //title -- end
    
    //source -- start
     /**
     * @dataProvider invalidSourceProvider
     */
    public function testSource($actual, $expected)
    {
        $result = $this->stub->source($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(SOURCE_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidSourceProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('sourcesource', true),
            array('s', false),
            array('', false),
            array($faker->randomDigit, false)
        );
    }
    //source -- end

    //reason -- start
    /**
     * @dataProvider invalidReasonProvider
     */
    public function testReasonInvalid($actual, $expected)
    {
        $result = $this->stub->reason($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(REASON_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidReasonProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(WidgetRules::REASON_MIN_LENGTH-1), false),
            array(StringGenerate::generate(WidgetRules::REASON_MAX_LENGTH+1), false),
            array(StringGenerate::generate(WidgetRules::REASON_MIN_LENGTH), true),
            array(StringGenerate::generate(WidgetRules::REASON_MIN_LENGTH+1), true),
            array(StringGenerate::generate(WidgetRules::REASON_MAX_LENGTH), true),
            array(StringGenerate::generate(WidgetRules::REASON_MAX_LENGTH-1), true)
        );
    }
    //reason -- end

    //stick -- start
    /**
     * @dataProvider invalidStickProvider
     */
    public function testStickInvalid($actual, $expected)
    {
        $result = $this->stub->stick($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(STATUS_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidStickProvider()
    {
        return array(
            array('', false),
            array(ITopAble::STICK['DISABLED'], true),
            array(ITopAble::STICK['ENABLED'], true),
            array('stick', false),
            array(999, false),
        );
    }
    //stick -- end

    //status -- start
    /**
     * @dataProvider invalidStatusProvider
     */
    public function testStatusInvalid($actual, $expected)
    {
        $result = $this->stub->status($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
        $this->assertEquals(STATUS_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidStatusProvider()
    {
        return array(
            array('', false),
            array(IEnableAble::STATUS['DISABLED'], true),
            array(IEnableAble::STATUS['ENABLED'], true),
            array('status', false),
            array(999, false),
        );
    }
    //status -- end

    //description -- start
    /**
     * @dataProvider invalidDescriptionProvider
     */
    public function testDescription($actual, $expected)
    {
        $result = $this->stub->description($actual);

        if (!$expected) {
            $this->assertFalse($result);
            
            $this->assertEquals(DESCRIPTION_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidDescriptionProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('description', true),
            array('descriptiondescriptiondescriptiondescriptiondescriptiondescriptiondesc
            riptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescr
            iptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescription', false),
            array('', false),
            array($faker->randomDigit, false)
        );
    }
    //description -- end

    /**
     * @dataProvider imagesProvider
     */
    public function testImages($actual, $expected)
    {
        $result = $this->stub->images($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(IMAGE_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function imagesProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        
        return array(
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpeg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpeg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpeg'],
                ], true),
            array(
                [
                    ['name'=>$faker->word, 'identify'=>$faker->word],
                    ['name'=>$faker->word, 'identify'=>$faker->word]
                ], false),
            array(
                    [
                        ['name'=>$faker->word,'identify'=>$faker->word.'.word'],
                        ['name'=>$faker->word,'identify'=>$faker->word.'.tpl'],
                        ['name'=>$faker->word,'identify'=>$faker->word.'.xls'],
                        ['name'=>$faker->word,'identify'=>$faker->word.'.xsl'],
                        ['name'=>$faker->word,'identify'=>$faker->word.'.docs'],
                        ['name'=>$faker->word,'identify'=>$faker->word.'.md'],
                       
                    ], false),
            array($faker->word, false),
            
        );
    }
}
