<?php
namespace Base\Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\MockModifyStatusAbleObject;

class ModifyStatusAbleMockAdapterTraitTest extends TestCase
{
    use ModifyStatusAbleMockAdapterTrait;

    public function testRevoke()
    {
        $object = new MockModifyStatusAbleObject();

        $result = $this->revoke($object);
        $this->assertTrue($result);
    }

    public function testClose()
    {
        $object = new MockModifyStatusAbleObject();

        $result = $this->close($object);
        $this->assertTrue($result);
    }

    public function testDeletes()
    {
        $object = new MockModifyStatusAbleObject();

        $result = $this->deletes($object);
        $this->assertTrue($result);
    }
}
