<?php
namespace Base\Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\ITopAble;
use Base\Sdk\Common\Model\MockTopAbleObject;

class TopAbleRestfulAdapterTraitTest extends TestCase
{
    public function testTop()
    {
        $object = $this->getMockBuilder(ITopAble::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockTopAbleRestfulAdapterObject::class)
                      ->setMethods(['topAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('topAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->top($object));
    }

    public function testTopActionPublic()
    {
        $this->initActionPublic(true, 'topActionPublic');
    }

    public function testTopActionPublicFail()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockTopAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockTopAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
             
        $this->assertFalse($trait->topActionPublic($object));
    }

    public function testCancelTop()
    {
        $object = $this->getMockBuilder(ITopAble::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockTopAbleRestfulAdapterObject::class)
                      ->setMethods(['cancelTopAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('cancelTopAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->cancelTop($object));
    }

    public function testCancelTopActionPublic()
    {
        $this->initActionPublic(true, 'cancelTopActionPublic');
    }

    public function testCancelTopActionPublicFail()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockTopAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockTopAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
             
        $this->assertFalse($trait->cancelTopActionPublic($object));
    }

    protected function initActionPublic(bool $result, string $method)
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockTopAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockTopAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn($result);

        $trait->expects($this->exactly(1))
             ->method('translateToObject')
             ->willReturn($result);
             
        $this->assertTrue($trait->$method($object));
    }
}
