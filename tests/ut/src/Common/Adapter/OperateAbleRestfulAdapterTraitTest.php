<?php
namespace Base\Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IOperateAble;

class OperateAbleRestfulAdapterTraitTest extends TestCase
{
    public function testAdd()
    {
        $object = $this->getMockBuilder(IOperateAble::class)
                                ->getMock();
        $trait = $this->getMockForTrait(OperateAbleRestfulAdapterTrait::class);

        $trait->expects($this->any())
             ->method('addAction')
             ->with($this->equalTo($object))
             ->willReturn(true);

        $this->assertTrue($trait->add($object));
    }

    public function testEdit()
    {
        $object = $this->getMockBuilder(IOperateAble::class)
                                ->getMock();
        $trait = $this->getMockForTrait(OperateAbleRestfulAdapterTrait::class);

        $trait->expects($this->any())
             ->method('editAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->edit($object));
    }
}
