<?php
namespace Base\Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IShelveAble;
use Base\Sdk\Common\Model\MockShelveAbleObject;

class ShelveAbleRestfulAdapterTraitTest extends TestCase
{
    public function testShelve()
    {
        $object = $this->getMockBuilder(IShelveAble::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockShelveAbleRestfulAdapterObject::class)
                      ->setMethods(['shelveAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('shelveAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->shelve($object));
    }

    public function testShelveActionPublic()
    {
        $this->initActionPublic(true, 'shelveActionPublic');
    }

    public function testShelveActionPublicFail()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockShelveAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockShelveAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
             
        $this->assertFalse($trait->shelveActionPublic($object));
    }

    public function testOffShelve()
    {
        $object = $this->getMockBuilder(IShelveAble::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockShelveAbleRestfulAdapterObject::class)
                      ->setMethods(['offShelveAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('offShelveAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->offShelve($object));
    }

    public function testOffShelveActionPublic()
    {
        $this->initActionPublic(true, 'offShelveActionPublic');
    }

    public function testOffShelveActionPublicFail()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockShelveAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockShelveAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
             
        $this->assertFalse($trait->OffShelveActionPublic($object));
    }

    protected function initActionPublic(bool $result, string $method)
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockShelveAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockShelveAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn($result);

        $trait->expects($this->exactly(1))
             ->method('translateToObject')
             ->willReturn($result);
             
        $this->assertTrue($trait->$method($object));
    }
}
