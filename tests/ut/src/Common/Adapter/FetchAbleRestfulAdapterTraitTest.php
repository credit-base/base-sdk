<?php
namespace Base\Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\INull;

class FetchAbleRestfulAdapterTraitTest extends TestCase
{
    public function testFetchOne()
    {
        $id = 1;
        $object = $this->getMockBuilder(INull::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['fetchOneAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('fetchOneAction')
             ->with($this->equalTo($id), $this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->fetchOne($id, $object));
    }

    public function testFetchOneActionPublic()
    {
        $id = 1;
        $resources = 'resources';
        $object = $this->getMockBuilder(INull::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'get', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(true);

        $trait->expects($this->exactly(1))
             ->method('translateToObject')
             ->willReturn(true);
             
        $this->assertTrue($trait->fetchOneActionPublic($id, $object));
    }

    public function testFetchOneActionPublicFail()
    {
        $id = 1;
        $resources = 'resources';
        $object = $this->getMockBuilder(INull::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'get', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
        
        $result = $trait->fetchOneActionPublic($id, $object);
        $this->assertEquals($object, $result);
    }

    public function testFetchList()
    {
        $ids = array(1,2,3);

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['fetchListAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('fetchListAction')
             ->with($this->equalTo($ids))
             ->willReturn(array());
             
        $this->assertArraySubset(array(), $trait->fetchList($ids));
    }

    public function testFetchListActionPublic()
    {
        $ids = array(1,2,3);
        $resources = 'resources';
        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'get', 'isSuccess', 'translateToObjects'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(true);

        $trait->expects($this->exactly(1))
             ->method('translateToObjects')
             ->willReturn(array());
             
        $result = $trait->fetchListActionPublic($ids);
        $this->assertEquals(array(), $result);
    }

    public function testFetchListActionPublicFail()
    {
        $ids = array(1,2,3);
        $resources = 'resources';
        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'get', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
        
        $result = $trait->fetchListActionPublic($ids);
        $this->assertEquals(array(0, array()), $result);
    }

    public function testSearch()
    {
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 20;

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['searchAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('searchAction')
             ->with($this->equalTo($filter), $this->equalTo($sort), $this->equalTo($number), $this->equalTo($size))
             ->willReturn(array());
             
        $this->assertArraySubset(array(), $trait->search($filter, $sort, $number, $size));
    }

    public function testSearchActionPublic()
    {
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 20;
        $resources = 'resources';

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'get', 'isSuccess', 'translateToObjects'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(true);

        $trait->expects($this->exactly(1))
             ->method('translateToObjects')
             ->willReturn(array());
        
        $result = $trait->searchActionPublic($filter, $sort, $number, $size);
        $this->assertEquals(array(), $result);
    }

    public function testSearchActionPublicFail()
    {
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 20;
        $resources = 'resources';

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'get', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
        
        $result = $trait->searchActionPublic($filter, $sort, $number, $size);
        $this->assertEquals(array(0, array()), $result);
    }
}
