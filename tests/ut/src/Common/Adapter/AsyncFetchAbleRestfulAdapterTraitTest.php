<?php
namespace Base\Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

class AsyncFetchAbleRestfulAdapterTraitTest extends TestCase
{
    public function testFetchOneAsync()
    {
        $id = 1;

        $trait = $this->getMockBuilder(MockAsyncFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['fetchOneAsyncAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('fetchOneAsyncAction')
             ->with($this->equalTo($id))
             ->willReturn(true);
             
        $this->assertTrue($trait->fetchOneAsync($id));
    }

    public function testFetchOneAsyncActionPublic()
    {
        $id = 1;
        $resources = 'resources';

        $trait = $this->getMockBuilder(MockAsyncFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'getAsync'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('getAsync');
        
        $trait->fetchOneAsyncActionPublic($id);
    }

    public function testFetchListAsync()
    {
        $ids = array(1,2,3);

        $trait = $this->getMockBuilder(MockAsyncFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['fetchListAsyncAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('fetchListAsyncAction')
             ->with($this->equalTo($ids))
             ->willReturn(true);
             
        $this->assertTrue($trait->fetchListAsync($ids));
    }

    public function testFetchListAsyncActionPublic()
    {
        $ids = array(1,2,3);
        $resources = 'resources';

        $trait = $this->getMockBuilder(MockAsyncFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'getAsync'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('getAsync');
        
        $trait->fetchListAsyncActionPublic($ids);
    }

    public function testSearchAsync()
    {
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 20;

        $trait = $this->getMockBuilder(MockAsyncFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['searchAsyncAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('searchAsyncAction')
             ->with($this->equalTo($filter), $this->equalTo($sort), $this->equalTo($number), $this->equalTo($size))
             ->willReturn(true);
             
        $this->assertTrue($trait->searchAsync($filter, $sort, $number, $size));
    }

    public function testSearchAsyncActionPublic()
    {
        $filter = array();
        $sort = array();
        $number = 0;
        $size = 20;
        $resources = 'resources';

        $trait = $this->getMockBuilder(MockAsyncFetchAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'getAsync'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('getAsync');
        
        $trait->searchAsyncActionPublic($filter, $sort, $number, $size);
    }
}
