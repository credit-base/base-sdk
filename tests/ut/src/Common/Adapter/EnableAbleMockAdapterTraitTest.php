<?php
namespace Base\Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\MockEnableAbleObject;

class EnableAbleMockAdapterTraitTest extends TestCase
{
    use EnableAbleMockAdapterTrait;

    public function testEnable()
    {
        $object = new MockEnableAbleObject();

        $result = $this->enable($object);
        $this->assertTrue($result);
    }

    public function testDisable()
    {
        $object = new MockEnableAbleObject();

        $result = $this->disable($object);
        $this->assertTrue($result);
    }
}
