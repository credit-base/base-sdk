<?php
namespace Base\Sdk\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\Model\MockModifyStatusAbleObject;

class ModifyStatusAbleRestfulAdapterTraitTest extends TestCase
{
    public function testRevoke()
    {
        $object = $this->getMockBuilder(IModifyStatusAble::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockModifyStatusAbleRestfulAdapterObject::class)
                      ->setMethods(['revokeAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('revokeAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->revoke($object));
    }

    public function testRevokeActionPublic()
    {
        $this->initActionPublic(true, 'revokeActionPublic');
    }

    public function testRevokeActionPublicFail()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockModifyStatusAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockModifyStatusAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
             
        $this->assertFalse($trait->revokeActionPublic($object));
    }

    public function testClose()
    {
        $object = $this->getMockBuilder(IModifyStatusAble::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockModifyStatusAbleRestfulAdapterObject::class)
                      ->setMethods(['closeAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('closeAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->close($object));
    }

    public function testCloseActionPublic()
    {
        $this->initActionPublic(true, 'closeActionPublic');
    }

    public function testCloseActionPublicFail()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockModifyStatusAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockModifyStatusAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
             
        $this->assertFalse($trait->closeActionPublic($object));
    }

    public function testDeletes()
    {
        $object = $this->getMockBuilder(IModifyStatusAble::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockModifyStatusAbleRestfulAdapterObject::class)
                      ->setMethods(['deletesAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('deletesAction')
             ->with($this->equalTo($object))
             ->willReturn(true);
             
        $this->assertTrue($trait->deletes($object));
    }

    public function testDeletesActionPublic()
    {
        $this->initActionPublic(true, 'deletesActionPublic');
    }

    public function testDeletesActionPublicFail()
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockModifyStatusAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockModifyStatusAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
             
        $this->assertFalse($trait->deletesActionPublic($object));
    }

    protected function initActionPublic(bool $result, string $method)
    {
        $resource = 'resource';
        $object = $this->getMockBuilder(MockModifyStatusAbleObject::class)
                       ->getMock();

        $trait = $this->getMockBuilder(MockModifyStatusAbleRestfulAdapterObject::class)
                      ->setMethods(['getResource', 'patch', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resource);

        $trait->expects($this->exactly(1))
             ->method('patch');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn($result);

        $trait->expects($this->exactly(1))
             ->method('translateToObject')
             ->willReturn($result);
             
        $this->assertTrue($trait->$method($object));
    }
}
