<?php
namespace Base\Sdk\Common\Persistence;

use PHPUnit\Framework\TestCase;

class UtilsSessionTest extends TestCase
{
    private $stub;

    /**
     * 初始化测试场景
     */
    public function setUp()
    {
        $this->stub = new UtilsSession();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    /**
     * 1. 测试是否继承 Session
     */
    public function testExtendsSession()
    {
        $this->assertInstanceOf('Marmot\Framework\Classes\Session', $this->stub);
    }
}
