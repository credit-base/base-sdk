<?php
namespace Base\Sdk\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

class ModifyStatusAbleRepositoryTraitTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MockModifyStatusAbleRepositoryObject::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testRevoke()
    {
        $modifyStatusAbleObject = $this->getMockBuilder(IModifyStatusAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IModifyStatusAbleAdapter::class);
        $adapter->revoke(Argument::exact($modifyStatusAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->revoke($modifyStatusAbleObject);
    }

    public function testClose()
    {
        $modifyStatusAbleObject = $this->getMockBuilder(IModifyStatusAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IModifyStatusAbleAdapter::class);
        $adapter->close(Argument::exact($modifyStatusAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->close($modifyStatusAbleObject);
    }

    public function testDeletes()
    {
        $modifyStatusAbleObject = $this->getMockBuilder(IModifyStatusAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IModifyStatusAbleAdapter::class);
        $adapter->deletes(Argument::exact($modifyStatusAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->deletes($modifyStatusAbleObject);
    }
}
