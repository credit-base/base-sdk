<?php
namespace Base\Sdk\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Adapter\IFetchAbleAdapter;

class FetchRepositoryTraitTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MockFetchRepositoryObject::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testFetchOne()
    {
        $id = 1;
        $adapter = $this->prophesize(IFetchAbleAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = array(1, 2, 3);
        $adapter = $this->prophesize(IFetchAbleAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testSearch()
    {
        $filter = array();
        $sort = array();
        $number = 1;
        $size = 2;

        $adapter = $this->prophesize(IFetchAbleAdapter::class);
        $adapter->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($number),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->search($filter, $sort, $number, $size);
    }
}
