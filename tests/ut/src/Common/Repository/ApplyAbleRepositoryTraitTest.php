<?php
namespace Base\Sdk\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;

class ApplyAbleRepositoryTraitTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MockApplyAbleRepositoryObject::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testApprove()
    {
        $applyAbleObject = $this->getMockBuilder(IApplyAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IApplyAbleAdapter::class);
        $adapter->approve(Argument::exact($applyAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->approve($applyAbleObject);
    }

    public function testReject()
    {
        $applyAbleObject = $this->getMockBuilder(IApplyAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IApplyAbleAdapter::class);
        $adapter->reject(Argument::exact($applyAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->reject($applyAbleObject);
    }
}
