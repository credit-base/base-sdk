<?php
namespace Base\Sdk\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Adapter\IResourcesFetchAbleAdapter;

class ResourcesFetchRepositoryTraitTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MockResourcesFetchRepositoryObject::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testFetchOne()
    {
        $id = 1;
        $resources = 'resources';
        $adapter = $this->prophesize(IResourcesFetchAbleAdapter::class);
        $adapter->fetchOne(Argument::exact($id), Argument::exact($resources))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id, $resources);
    }

    public function testSearch()
    {
        $resources = 'resources';
        $filter = array();
        $number = 1;
        $size = 2;

        $adapter = $this->prophesize(IResourcesFetchAbleAdapter::class);
        $adapter->search(
            Argument::exact($resources),
            Argument::exact($filter),
            Argument::exact($number),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->search($resources, $filter, $number, $size);
    }
}
