<?php
namespace Base\Sdk\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;

class EnableAbleRepositoryTraitTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MockEnableAbleRepositoryObject::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testEnable()
    {
        $enableAbleObject = $this->getMockBuilder(IEnableAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IEnableAbleAdapter::class);
        $adapter->enable(Argument::exact($enableAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->enable($enableAbleObject);
    }

    public function testDisable()
    {
        $enableAbleObject = $this->getMockBuilder(IEnableAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IEnableAbleAdapter::class);
        $adapter->disable(Argument::exact($enableAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->disable($enableAbleObject);
    }
}
