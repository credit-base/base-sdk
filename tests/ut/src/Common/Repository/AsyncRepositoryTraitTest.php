<?php
namespace Base\Sdk\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IAsyncAdapter;

class AsyncRepositoryTraitTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MockAsyncRepositoryObject::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testFetchOneAsync()
    {
        $id = 1;
        $adapter = $this->prophesize(IAsyncAdapter::class);
        $adapter->fetchOneAsync(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOneAsync($id);
    }

    public function testFetchListAsync()
    {
        $ids = array(1, 2, 3);
        $adapter = $this->prophesize(IAsyncAdapter::class);
        $adapter->fetchListAsync(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchListAsync($ids);
    }

    public function testSearchAsync()
    {
        $filter = array();
        $sort = array();
        $number = 1;
        $size = 2;

        $adapter = $this->prophesize(IAsyncAdapter::class);
        $adapter->searchAsync(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($number),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->searchAsync($filter, $sort, $number, $size);
    }
}
