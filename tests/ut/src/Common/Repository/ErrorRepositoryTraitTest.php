<?php
namespace Base\Sdk\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IErrorAdapter;

class ErrorRepositoryTraitTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MockErrorRepositoryObject::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testLastErrorId()
    {
        $adapter = $this->prophesize(IErrorAdapter::class);
        $adapter->lastErrorId()->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->lastErrorId();
    }

    public function testLastErrorInfo()
    {
        $adapter = $this->prophesize(IErrorAdapter::class);
        $adapter->lastErrorInfo()->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->lastErrorInfo();
    }
}
