<?php
namespace Base\Sdk\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

class OperateAbleRepositoryTraitTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MockOperateAbleRepositoryObject::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testAdd()
    {
        $operateAbleObject = $this->getMockBuilder(IOperateAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IOperateAbleAdapter::class);
        $adapter->add(Argument::exact($operateAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($operateAbleObject);
    }

    public function testEdit()
    {
        $operateAbleObject = $this->getMockBuilder(IOperateAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IOperateAbleAdapter::class);
        $adapter->edit(Argument::exact($operateAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($operateAbleObject);
    }
}
