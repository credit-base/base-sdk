<?php
namespace Base\Sdk\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IShelveAble;
use Base\Sdk\Common\Adapter\IShelveAbleAdapter;

class ShelveAbleRepositoryTraitTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(MockShelveAbleRepositoryObject::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testShelve()
    {
        $shelveAbleObject = $this->getMockBuilder(IShelveAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IShelveAbleAdapter::class);
        $adapter->shelve(Argument::exact($shelveAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->shelve($shelveAbleObject);
    }

    public function testOffShelve()
    {
        $shelveAbleObject = $this->getMockBuilder(IShelveAble::class)
                                ->getMock();

        $adapter = $this->prophesize(IShelveAbleAdapter::class);
        $adapter->offShelve(Argument::exact($shelveAbleObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->offShelve($shelveAbleObject);
    }
}
