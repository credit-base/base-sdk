<?php
namespace Base\Sdk\Common\Model;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Adapter\MockResubmitAbleAdapter;

class ResubmitAbleTraitTest extends TestCase
{
    public function testResubmit()
    {
        $trait = $this->getMockBuilder(MockResubmitAbleObject::class)
                      ->setMethods(['resubmitAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('resubmitAction')
             ->willReturn(true);
             
        $this->assertTrue($trait->resubmit());
    }

    public function testResubmitActionPublic()
    {
        $trait = $this->getMockBuilder(MockResubmitAbleObject::class)
                      ->setMethods(['getIResubmitAbleAdapter'])
                      ->getMock();

        $mockResubmitAbleAdapter = $this->prophesize(MockResubmitAbleAdapter::class);
        $mockResubmitAbleAdapter->resubmit($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $trait->expects($this->exactly(1))
            ->method('getIResubmitAbleAdapter')
            ->willReturn($mockResubmitAbleAdapter->reveal());
             
        $this->assertTrue($trait->resubmitActionPublic());
    }

    public function testResubmitActionPublicFail()
    {
        $trait = $this->getMockBuilder(MockResubmitAbleObject::class)
                      ->setMethods(['getIResubmitAbleAdapter'])
                      ->getMock();

        $mockResubmitAbleAdapter = $this->prophesize(MockResubmitAbleAdapter::class);
        $mockResubmitAbleAdapter->resubmit($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(false);
        $trait->expects($this->exactly(1))
            ->method('getIResubmitAbleAdapter')
            ->willReturn($mockResubmitAbleAdapter->reveal());
             
        $this->assertFalse($trait->resubmitActionPublic());
    }
}
