<?php
namespace Base\Sdk\Common\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;

class EnableAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockEnableAbleObject();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    /**
     * @dataProvider statusDataProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->trait->setStatus($actual);
        
        $result = $this->trait->getStatus();
        $this->assertEquals($expected, $result);
    }

    public function statusDataProvider()
    {
        return [
            [IEnableAble::STATUS['ENABLED'], IEnableAble::STATUS['ENABLED']],
            [IEnableAble::STATUS['DISABLED'], IEnableAble::STATUS['DISABLED']],
            [999, IEnableAble::STATUS['DISABLED']]
        ];
    }

    public function testEnableSuccess()
    {
        $trait = $this->getMockBuilder(MockEnableAbleObject::class)
                      ->setMethods(['getIEnableAbleAdapter'])
                      ->getMock();
                      
        $trait->setStatus(IEnableAble::STATUS['DISABLED']);

        $adapter = $this->prophesize(IEnableAbleAdapter::class);
        $adapter->enable($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $trait->expects($this->exactly(1))
            ->method('getIEnableAbleAdapter')
            ->willReturn($adapter->reveal());
             
        $this->assertTrue($trait->enable());
    }

    public function testEnableFail()
    {
        $this->trait->setStatus(IEnableAble::STATUS['ENABLED']);

        $result = $this->trait->enable();

        $this->assertFalse($result);
        $this->assertEquals(Core::getLastError()->getId(), STATUS_CAN_NOT_MODIFY);
    }

    public function testDisableSuccess()
    {
        $trait = $this->getMockBuilder(MockEnableAbleObject::class)
                      ->setMethods(['getIEnableAbleAdapter'])
                      ->getMock();

        $trait->setStatus(IEnableAble::STATUS['ENABLED']);
        
        $adapter = $this->prophesize(IEnableAbleAdapter::class);
        $adapter->disable($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $trait->expects($this->exactly(1))
            ->method('getIEnableAbleAdapter')
            ->willReturn($adapter->reveal());
             
        $this->assertTrue($trait->disable());
    }

    public function testDisableFail()
    {
        $this->trait->setStatus(IEnableAble::STATUS['DISABLED']);

        $result = $this->trait->disable();

        $this->assertFalse($result);
        $this->assertEquals(Core::getLastError()->getId(), STATUS_CAN_NOT_MODIFY);
    }
}
