<?php
namespace Base\Sdk\Common\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class NullEnableAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNullEnableAbleObject::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->trait);
    }

    public function testEnable()
    {
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->enable());
    }

    public function testDisable()
    {
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->disable());
    }

    public function testIsEnabled()
    {
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->isEnabled());
    }

    public function testIsDisable()
    {
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->isDisabled());
    }
}
