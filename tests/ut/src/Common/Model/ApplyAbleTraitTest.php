<?php
namespace Base\Sdk\Common\Model;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Adapter\MockApplyAbleAdapter;

class ApplyAbleTraitTest extends TestCase
{
    public function testApprove()
    {
        $trait = $this->getMockBuilder(MockApplyAbleObject::class)
                      ->setMethods(['approveAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('approveAction')
             ->willReturn(true);
             
        $this->assertTrue($trait->approve());
    }

    public function testApproveActionPublic()
    {
        $trait = $this->getMockBuilder(MockApplyAbleObject::class)
                      ->setMethods(['getIApplyAbleAdapter'])
                      ->getMock();
                      
        $mockApplyAbleAdapter = $this->prophesize(MockApplyAbleAdapter::class);
        $mockApplyAbleAdapter->approve($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $trait->expects($this->exactly(1))
            ->method('getIApplyAbleAdapter')
            ->willReturn($mockApplyAbleAdapter->reveal());
             
        $this->assertTrue($trait->approveActionPublic());
    }

    public function testApproveActionPublicFail()
    {
        $trait = $this->getMockBuilder(MockApplyAbleObject::class)
                      ->setMethods(['getIApplyAbleAdapter'])
                      ->getMock();

        $mockApplyAbleAdapter = $this->prophesize(MockApplyAbleAdapter::class);
        $mockApplyAbleAdapter->approve($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(false);
        $trait->expects($this->exactly(1))
            ->method('getIApplyAbleAdapter')
            ->willReturn($mockApplyAbleAdapter->reveal());
             
        $this->assertFalse($trait->approveActionPublic());
    }

    public function testReject()
    {
        $trait = $this->getMockBuilder(MockApplyAbleObject::class)
                      ->setMethods(['rejectAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('rejectAction')
             ->willReturn(true);
             
        $this->assertTrue($trait->reject());
    }

    public function testRejectActionPublic()
    {
        $trait = $this->getMockBuilder(MockApplyAbleObject::class)
                      ->setMethods(['getIApplyAbleAdapter'])
                      ->getMock();

        $mockApplyAbleAdapter = $this->prophesize(MockApplyAbleAdapter::class);
        $mockApplyAbleAdapter->reject($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $trait->expects($this->exactly(1))
            ->method('getIApplyAbleAdapter')
            ->willReturn($mockApplyAbleAdapter->reveal());
             
        $this->assertTrue($trait->rejectActionPublic());
    }

    public function testRejectActionPublicFail()
    {
        $trait = $this->getMockBuilder(MockApplyAbleObject::class)
                      ->setMethods(['getIApplyAbleAdapter'])
                      ->getMock();

        $mockApplyAbleAdapter = $this->prophesize(MockApplyAbleAdapter::class);
        $mockApplyAbleAdapter->reject($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(false);
        $trait->expects($this->exactly(1))
            ->method('getIApplyAbleAdapter')
            ->willReturn($mockApplyAbleAdapter->reveal());
             
        $this->assertFalse($trait->rejectActionPublic());
    }
}
