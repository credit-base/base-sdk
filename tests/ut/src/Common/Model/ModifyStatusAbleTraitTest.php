<?php
namespace Base\Sdk\Common\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

class ModifyStatusAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockModifyStatusAbleObject();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    /**
     * @dataProvider statusDataProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->trait->setStatus($actual);
        
        $result = $this->trait->getStatus();
        $this->assertEquals($expected, $result);
    }

    public function statusDataProvider()
    {
        return [
            [IModifyStatusAble::MODIFY_STATUS['NORMAL'], IModifyStatusAble::MODIFY_STATUS['NORMAL']],
            [IModifyStatusAble::MODIFY_STATUS['REVOKED'], IModifyStatusAble::MODIFY_STATUS['REVOKED']],
            [IModifyStatusAble::MODIFY_STATUS['CLOSED'], IModifyStatusAble::MODIFY_STATUS['CLOSED']],
            [IModifyStatusAble::MODIFY_STATUS['DELETED'], IModifyStatusAble::MODIFY_STATUS['DELETED']]
        ];
    }

    public function testRevokeSuccess()
    {
        $trait = $this->getMockBuilder(MockModifyStatusAbleObject::class)
                      ->setMethods(['getIModifyStatusAbleAdapter'])
                      ->getMock();
                      
        $trait->setStatus(IModifyStatusAble::MODIFY_STATUS['NORMAL']);
        $trait->setApplyStatus(IApplyAble::APPLY_STATUS['PENDING']);

        $adapter = $this->prophesize(IModifyStatusAbleAdapter::class);
        $adapter->revoke($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getIModifyStatusAbleAdapter')
            ->willReturn($adapter->reveal());
             
        $this->assertTrue($trait->revoke());
    }

    public function testRevokeFail()
    {
        $this->trait->setStatus(IModifyStatusAble::MODIFY_STATUS['REVOKED']);
        $this->trait->setApplyStatus(IApplyAble::APPLY_STATUS['PENDING']);

        $result = $this->trait->revoke();

        $this->assertFalse($result);
        $this->assertEquals(Core::getLastError()->getId(), STATUS_CAN_NOT_MODIFY);
    }

    public function testCloseSuccess()
    {
        $trait = $this->getMockBuilder(MockModifyStatusAbleObject::class)
                      ->setMethods(['getIModifyStatusAbleAdapter'])
                      ->getMock();

        $trait->setStatus(IModifyStatusAble::MODIFY_STATUS['NORMAL']);
        $trait->setApplyStatus(IApplyAble::APPLY_STATUS['APPROVE']);
        
        $adapter = $this->prophesize(IModifyStatusAbleAdapter::class);
        $adapter->close($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getIModifyStatusAbleAdapter')
            ->willReturn($adapter->reveal());
             
        $this->assertTrue($trait->close());
    }

    public function testCloseFail()
    {
        $this->trait->setStatus(IModifyStatusAble::MODIFY_STATUS['CLOSED']);
        $this->trait->setApplyStatus(IApplyAble::APPLY_STATUS['PENDING']);

        $result = $this->trait->close();

        $this->assertFalse($result);
        $this->assertEquals(Core::getLastError()->getId(), STATUS_CAN_NOT_MODIFY);
    }

    public function testDeletesSuccess()
    {
        $trait = $this->getMockBuilder(MockModifyStatusAbleObject::class)
                      ->setMethods(['getIModifyStatusAbleAdapter'])
                      ->getMock();

        $trait->setStatus(IModifyStatusAble::MODIFY_STATUS['NORMAL']);
        $trait->setApplyStatus(IApplyAble::APPLY_STATUS['REJECT']);
        
        $adapter = $this->prophesize(IModifyStatusAbleAdapter::class);
        $adapter->deletes($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
            
        $trait->expects($this->exactly(1))
            ->method('getIModifyStatusAbleAdapter')
            ->willReturn($adapter->reveal());
             
        $this->assertTrue($trait->deletes());
    }

    public function testDeletesFail()
    {
        $this->trait->setStatus(IModifyStatusAble::MODIFY_STATUS['DELETED']);
        $this->trait->setApplyStatus(IApplyAble::APPLY_STATUS['APPROVE']);

        $result = $this->trait->deletes();

        $this->assertFalse($result);
        $this->assertEquals(Core::getLastError()->getId(), STATUS_CAN_NOT_MODIFY);
    }
}
