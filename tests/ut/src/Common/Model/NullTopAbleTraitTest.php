<?php
namespace Base\Sdk\Common\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class NullTopAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNullTopAbleObject::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->trait);
    }

    public function testTop()
    {
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->top());
    }

    public function testCancelTop()
    {
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->cancelTop());
    }

    public function testIsStick()
    {
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->isStick());
    }
}
