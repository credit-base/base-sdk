<?php
namespace Base\Sdk\Common\Model;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Adapter\MockOperateAbleAdapter;

class OperateAbleTraitTest extends TestCase
{
    public function testAdd()
    {
        $trait = $this->getMockBuilder(MockOperateAbleObject::class)
                      ->setMethods(['addAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('addAction')
             ->willReturn(true);
             
        $this->assertTrue($trait->add());
    }

    public function testAddActionPublic()
    {
        $trait = $this->getMockBuilder(MockOperateAbleObject::class)
                      ->setMethods(['getIOperateAbleAdapter'])
                      ->getMock();

        $mockOperateAbleAdapter = $this->prophesize(MockOperateAbleAdapter::class);
        $mockOperateAbleAdapter->add($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $trait->expects($this->exactly(1))
            ->method('getIOperateAbleAdapter')
            ->willReturn($mockOperateAbleAdapter->reveal());
             
        $this->assertTrue($trait->addActionPublic());
    }

    public function testAddActionPublicFail()
    {
        $trait = $this->getMockBuilder(MockOperateAbleObject::class)
                      ->setMethods(['getIOperateAbleAdapter'])
                      ->getMock();

        $mockOperateAbleAdapter = $this->prophesize(MockOperateAbleAdapter::class);
        $mockOperateAbleAdapter->add($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(false);
        $trait->expects($this->exactly(1))
            ->method('getIOperateAbleAdapter')
            ->willReturn($mockOperateAbleAdapter->reveal());
             
        $this->assertFalse($trait->addActionPublic());
    }

    public function testEdit()
    {
        $trait = $this->getMockBuilder(MockOperateAbleObject::class)
                      ->setMethods(['editAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('editAction')
             ->willReturn(true);
             
        $this->assertTrue($trait->edit());
    }

    public function testEditActionPublic()
    {
        $trait = $this->getMockBuilder(MockOperateAbleObject::class)
                      ->setMethods(['getIOperateAbleAdapter'])
                      ->getMock();

        $mockOperateAbleAdapter = $this->prophesize(MockOperateAbleAdapter::class);
        $mockOperateAbleAdapter->edit($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $trait->expects($this->exactly(1))
            ->method('getIOperateAbleAdapter')
            ->willReturn($mockOperateAbleAdapter->reveal());
             
        $this->assertTrue($trait->editActionPublic());
    }

    public function testEditActionPublicFail()
    {
        $trait = $this->getMockBuilder(MockOperateAbleObject::class)
                      ->setMethods(['getIOperateAbleAdapter'])
                      ->getMock();

        $mockOperateAbleAdapter = $this->prophesize(MockOperateAbleAdapter::class);
        $mockOperateAbleAdapter->edit($trait)
            ->shouldBeCalledTimes(1)
            ->willReturn(false);
        $trait->expects($this->exactly(1))
            ->method('getIOperateAbleAdapter')
            ->willReturn($mockOperateAbleAdapter->reveal());
             
        $this->assertFalse($trait->editActionPublic());
    }
}
