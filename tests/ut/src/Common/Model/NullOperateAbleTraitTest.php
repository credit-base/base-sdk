<?php
namespace Base\Sdk\Common\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class NullOperateAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNullOperateAbleObject::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->trait);
    }

    public function testAdd()
    {
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->add());
    }

    public function testEdit()
    {
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->edit());
    }
}
