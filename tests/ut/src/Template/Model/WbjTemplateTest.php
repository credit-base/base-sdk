<?php
namespace Base\Sdk\Template\Model;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class WbjTemplateTest extends TestCase
{
    private $wbjTemplate;

    public function setUp()
    {
        $this->wbjTemplate = new WbjTemplate();
        $this->childWbjTemplate = new class extends WbjTemplate
        {
            public function getIOperateAbleAdapter() : IOperateAbleAdapter
            {
                return parent::getIOperateAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->wbjTemplate);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->wbjTemplate
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\Model\IOperateAble',
            $this->wbjTemplate
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\WbjTemplateRepository',
            $this->wbjTemplate->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 WbjTemplate setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->wbjTemplate->setId(1);
        $this->assertEquals(1, $this->wbjTemplate->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjTemplate setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->wbjTemplate->setStatus(0);
        $this->assertEquals(0, $this->wbjTemplate->getStatus());
    }

    //identify 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjTemplate setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->wbjTemplate->setIdentify('string');
        $this->assertEquals('string', $this->wbjTemplate->getIdentify());
    }

    /**
     * 设置 WbjTemplate setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->wbjTemplate->setIdentify(array(1,2,3));
    }
    //identify 测试 --------------------------------------------------------   end

    /**
     * 设置 WbjTemplate setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->wbjTemplate->setStatus(array(1,2,3));
    }
    //status 测试 --------------------------------------------------------   end

    //description 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjTemplate setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->wbjTemplate->setDescription('string');
        $this->assertEquals('string', $this->wbjTemplate->getDescription());
    }

    /**
     * 设置 WbjTemplate setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->wbjTemplate->setDescription(array(1,2,3));
    }
    //description 测试 --------------------------------------------------------   end

    //subjectCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjTemplate setSubjectCategory() 正确的传参类型,期望传值正确
     */
    public function testSetSubjectCategoryCorrectType()
    {
        $this->wbjTemplate->setSubjectCategory(array());
        $this->assertEquals(array(), $this->wbjTemplate->getSubjectCategory());
    }

    /**
     * 设置 WbjTemplate setSubjectCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubjectCategoryWrongType()
    {
        $this->wbjTemplate->setSubjectCategory('string');
    }
    //description 测试 --------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjTemplate setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->wbjTemplate->setName('string');
        $this->assertEquals('string', $this->wbjTemplate->getName());
    }

    /**
     * 设置 WbjTemplate setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->wbjTemplate->setName(array(1,2,3));
    }
    //name 测试 --------------------------------------------------------   end

    //items 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjTemplate setItems() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $this->wbjTemplate->setItems(array());
        $this->assertEquals(array(), $this->wbjTemplate->getItems());
    }

    /**
     * 设置 WbjTemplate setItems() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $this->wbjTemplate->setItems('string');
    }
    //items 测试 --------------------------------------------------------   end

    //infoClassify 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjTemplate setInfoClassify() 正确的传参类型,期望传值正确
     */
    public function testSetInfoClassifyCorrectType()
    {
        $this->wbjTemplate->setInfoClassify(0);
        $this->assertEquals(0, $this->wbjTemplate->getInfoClassify());
    }

    /**
     * 设置 WbjTemplate setInfoClassify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoClassifyWrongType()
    {
        $this->wbjTemplate->setInfoClassify(array(1,2,3));
    }
    //infoClassify 测试 --------------------------------------------------------   end

    //dimension 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjTemplate setDimension() 正确的传参类型,期望传值正确
     */
    public function testSetDimensionCorrectType()
    {
        $this->wbjTemplate->setDimension(0);
        $this->assertEquals(0, $this->wbjTemplate->getDimension());
    }

    /**
     * 设置 WbjTemplate setDimension() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDimensionWrongType()
    {
        $this->wbjTemplate->setDimension(array(1,2,3));
    }
    //dimension 测试 --------------------------------------------------------   end

    //exchangeFrequency 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjTemplate setExchangeFrequency() 正确的传参类型,期望传值正确
     */
    public function testSetExchangeFrequencyCorrectType()
    {
        $this->wbjTemplate->setExchangeFrequency(0);
        $this->assertEquals(0, $this->wbjTemplate->getExchangeFrequency());
    }

    /**
     * 设置 WbjTemplate setExchangeFrequency() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExchangeFrequencyWrongType()
    {
        $this->wbjTemplate->setExchangeFrequency(array(1,2,3));
    }
    //exchangeFrequency 测试 --------------------------------------------------------   end

    //infoCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjTemplate setInfoCategory() 正确的传参类型,期望传值正确
     */
    public function testSetInfoCategoryCorrectType()
    {
        $this->wbjTemplate->setInfoCategory(0);
        $this->assertEquals(0, $this->wbjTemplate->getInfoCategory());
    }

    /**
     * 设置 WbjTemplate setInfoCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoCategoryWrongType()
    {
        $this->wbjTemplate->setInfoCategory(array(1,2,3));
    }
    //infoCategory 测试 --------------------------------------------------------   end

    public function testGetIIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\WbjTemplateRepository',
            $this->childWbjTemplate->getIOperateAbleAdapter()
        );
    }
}
