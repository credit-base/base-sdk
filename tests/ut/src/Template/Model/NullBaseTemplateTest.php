<?php
namespace Base\Sdk\Template\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullBaseTemplateTest extends TestCase
{
    private $baseTemplate;

    private $childBaseTemplate;

    public function setUp()
    {
        $this->baseTemplate = NullBaseTemplate::getInstance();
        $this->childBaseTemplate = new class extends NullBaseTemplate
        {
            public function resourceNotExist() : bool
            {
                return parent::resourceNotExist();
            }
        };
    }

    public function tearDown()
    {
        unset($this->baseTemplate);
        unset($this->childBaseTemplate);
    }

    public function testExtendsUserGroup()
    {
        $this->assertInstanceof('Base\Sdk\Template\Model\BaseTemplate', $this->baseTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->baseTemplate);
    }

    public function testAdd()
    {
        $baseTemplate = $this->getMockBuilder(NullBaseTemplate::class)
                                ->setMethods(['resourceNotExist'])
                                ->getMock();
        $baseTemplate->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($baseTemplate->add());
    }

    public function testEdit()
    {
        $baseTemplate = $this->getMockBuilder(NullBaseTemplate::class)
                                ->setMethods(['resourceNotExist'])
                                ->getMock();
        $baseTemplate->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($baseTemplate->edit());
    }

    public function testResourceNotExist()
    {
        $result = $this->childBaseTemplate->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
