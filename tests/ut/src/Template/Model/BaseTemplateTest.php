<?php
namespace Base\Sdk\Template\Model;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class BaseTemplateTest extends TestCase
{
    private $baseTemplate;
    private $childBaseTemplate;

    public function setUp()
    {
        $this->baseTemplate = new BaseTemplate();
        $this->childBaseTemplate = new class extends BaseTemplate
        {
            public function getIOperateAbleAdapter() : IOperateAbleAdapter
            {
                return parent::getIOperateAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->baseTemplate);
        unset($this->childBaseTemplate);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->baseTemplate
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\Model\IOperateAble',
            $this->baseTemplate
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\BaseTemplateRepository',
            $this->baseTemplate->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->baseTemplate->setId(1);
        $this->assertEquals(1, $this->baseTemplate->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->baseTemplate->setStatus(0);
        $this->assertEquals(0, $this->baseTemplate->getStatus());
    }

    /**
     * 设置 BaseTemplate setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->baseTemplate->setStatus(array(1,2,3));
    }
    //status 测试 --------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->baseTemplate->setName('string');
        $this->assertEquals('string', $this->baseTemplate->getName());
    }

    /**
     * 设置 BaseTemplate setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->baseTemplate->setName(array(1,2,3));
    }
    //name 测试 --------------------------------------------------------   end

    //identify 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->baseTemplate->setIdentify('string');
        $this->assertEquals('string', $this->baseTemplate->getIdentify());
    }

    /**
     * 设置 BaseTemplate setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->baseTemplate->setIdentify(array(1,2,3));
    }
    //identify 测试 --------------------------------------------------------   end

    //subjectCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setSubjectCategory() 正确的传参类型,期望传值正确
     */
    public function testSetSubjectCategoryCorrectType()
    {
        $this->baseTemplate->setSubjectCategory(array());
        $this->assertEquals(array(), $this->baseTemplate->getSubjectCategory());
    }

    /**
     * 设置 BaseTemplate setSubjectCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubjectCategoryWrongType()
    {
        $this->baseTemplate->setSubjectCategory('string');
    }
    //description 测试 --------------------------------------------------------   end

    //items 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setItems() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $this->baseTemplate->setItems(array());
        $this->assertEquals(array(), $this->baseTemplate->getItems());
    }

    /**
     * 设置 BaseTemplate setItems() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $this->baseTemplate->setItems('string');
    }
    //items 测试 --------------------------------------------------------   end

    //dimension 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setDimension() 正确的传参类型,期望传值正确
     */
    public function testSetDimensionCorrectType()
    {
        $this->baseTemplate->setDimension(0);
        $this->assertEquals(0, $this->baseTemplate->getDimension());
    }

    //description 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->baseTemplate->setDescription('string');
        $this->assertEquals('string', $this->baseTemplate->getDescription());
    }

    /**
     * 设置 BaseTemplate setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->baseTemplate->setDescription(array(1,2,3));
    }
    //description 测试 --------------------------------------------------------   end

    /**
     * 设置 BaseTemplate setDimension() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDimensionWrongType()
    {
        $this->baseTemplate->setDimension(array(1,2,3));
    }
    //dimension 测试 --------------------------------------------------------   end

    //infoCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setInfoCategory() 正确的传参类型,期望传值正确
     */
    public function testSetInfoCategoryCorrectType()
    {
        $this->baseTemplate->setInfoCategory(0);
        $this->assertEquals(0, $this->baseTemplate->getInfoCategory());
    }

    /**
     * 设置 BaseTemplate setInfoCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoCategoryWrongType()
    {
        $this->baseTemplate->setInfoCategory(array(1,2,3));
    }
    //infoCategory 测试 --------------------------------------------------------   end
    
    //exchangeFrequency 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setExchangeFrequency() 正确的传参类型,期望传值正确
     */
    public function testSetExchangeFrequencyCorrectType()
    {
        $this->baseTemplate->setExchangeFrequency(0);
        $this->assertEquals(0, $this->baseTemplate->getExchangeFrequency());
    }

    /**
     * 设置 BaseTemplate setExchangeFrequency() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExchangeFrequencyWrongType()
    {
        $this->baseTemplate->setExchangeFrequency(array(1,2,3));
    }
    //exchangeFrequency 测试 --------------------------------------------------------   end

    //infoClassify 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setInfoClassify() 正确的传参类型,期望传值正确
     */
    public function testSetInfoClassifyCorrectType()
    {
        $this->baseTemplate->setInfoClassify(0);
        $this->assertEquals(0, $this->baseTemplate->getInfoClassify());
    }

    /**
     * 设置 BaseTemplate setInfoClassify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoClassifyWrongType()
    {
        $this->baseTemplate->setInfoClassify(array(1,2,3));
    }
    //infoClassify 测试 --------------------------------------------------------   end

    //ruleCount 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setRuleCount() 正确的传参类型,期望传值正确
     */
    public function testSetRuleCountCorrectType()
    {
        $this->baseTemplate->setRuleCount(0);
        $this->assertEquals(0, $this->baseTemplate->getRuleCount());
    }

    /**
     * 设置 BaseTemplate setRuleCount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRuleCountWrongType()
    {
        $this->baseTemplate->setRuleCount(array(1,2,3));
    }
    //ruleCount 测试 --------------------------------------------------------   end

    //dataTotal 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setDataTotal() 正确的传参类型,期望传值正确
     */
    public function testSetDataTotalCorrectType()
    {
        $this->baseTemplate->setDataTotal(0);
        $this->assertEquals(0, $this->baseTemplate->getDataTotal());
    }

    /**
     * 设置 BaseTemplate setDataTotal() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataTotalWrongType()
    {
        $this->baseTemplate->setDataTotal(array(1,2,3));
    }
    //dataTotal 测试 --------------------------------------------------------   end

    //category 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $this->baseTemplate->setCategory(1);
        $this->assertEquals(1, $this->baseTemplate->getCategory());
    }

    /**
     * 设置 BaseTemplate setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->baseTemplate->setCategory(['string']);
    }
    //category 测试 --------------------------------------------------------   end

    //userGroupCount 测试 -------------------------------------------------------- start
    /**
     * 设置 BaseTemplate setUserGroupCount() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCountCorrectType()
    {
        $this->baseTemplate->setUserGroupCount(10);
        $this->assertEquals(10, $this->baseTemplate->getUserGroupCount());
    }

    /**
     * 设置 BaseTemplate setUserGroupCount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupCountWrongType()
    {
        $this->baseTemplate->setUserGroupCount('string');
    }
    //userGroupCount 测试 --------------------------------------------------------   end

    public function testGetIIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\BaseTemplateRepository',
            $this->childBaseTemplate->getIOperateAbleAdapter()
        );
    }
}
