<?php
namespace Base\Sdk\Template\Model;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class BjTemplateTest extends TestCase
{
    private $bjTemplate;

    public function setUp()
    {
        $this->bjTemplate = new BjTemplate();
        $this->childBjTemplate = new class extends BjTemplate
        {
            public function getIOperateAbleAdapter() : IOperateAbleAdapter
            {
                return parent::getIOperateAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->bjTemplate);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->bjTemplate
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\Model\IOperateAble',
            $this->bjTemplate
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\BjTemplateRepository',
            $this->bjTemplate->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 BjTemplate setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->bjTemplate->setId(1);
        $this->assertEquals(1, $this->bjTemplate->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->bjTemplate->setStatus(0);
        $this->assertEquals(0, $this->bjTemplate->getStatus());
    }

    /**
     * 设置 BjTemplate setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->bjTemplate->setStatus(array(1,2,3));
    }
    //status 测试 --------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->bjTemplate->setName('string');
        $this->assertEquals('string', $this->bjTemplate->getName());
    }

    /**
     * 设置 BjTemplate setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->bjTemplate->setName(array(1,2,3));
    }
    //name 测试 --------------------------------------------------------   end

    //identify 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->bjTemplate->setIdentify('string');
        $this->assertEquals('string', $this->bjTemplate->getIdentify());
    }

    /**
     * 设置 BjTemplate setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->bjTemplate->setIdentify(array(1,2,3));
    }
    //identify 测试 --------------------------------------------------------   end

    //subjectCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setSubjectCategory() 正确的传参类型,期望传值正确
     */
    public function testSetSubjectCategoryCorrectType()
    {
        $this->bjTemplate->setSubjectCategory(array());
        $this->assertEquals(array(), $this->bjTemplate->getSubjectCategory());
    }

    /**
     * 设置 BjTemplate setSubjectCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubjectCategoryWrongType()
    {
        $this->bjTemplate->setSubjectCategory('string');
    }
    //description 测试 --------------------------------------------------------   end

    //items 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setItems() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $this->bjTemplate->setItems(array());
        $this->assertEquals(array(), $this->bjTemplate->getItems());
    }

    /**
     * 设置 BjTemplate setItems() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $this->bjTemplate->setItems('string');
    }
    //items 测试 --------------------------------------------------------   end

    //dimension 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setDimension() 正确的传参类型,期望传值正确
     */
    public function testSetDimensionCorrectType()
    {
        $this->bjTemplate->setDimension(0);
        $this->assertEquals(0, $this->bjTemplate->getDimension());
    }

    //description 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->bjTemplate->setDescription('string');
        $this->assertEquals('string', $this->bjTemplate->getDescription());
    }

    /**
     * 设置 BjTemplate setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->bjTemplate->setDescription(array(1,2,3));
    }
    //description 测试 --------------------------------------------------------   end

    /**
     * 设置 BjTemplate setDimension() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDimensionWrongType()
    {
        $this->bjTemplate->setDimension(array(1,2,3));
    }
    //dimension 测试 --------------------------------------------------------   end

    //infoCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setInfoCategory() 正确的传参类型,期望传值正确
     */
    public function testSetInfoCategoryCorrectType()
    {
        $this->bjTemplate->setInfoCategory(0);
        $this->assertEquals(0, $this->bjTemplate->getInfoCategory());
    }

    /**
     * 设置 BjTemplate setInfoCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoCategoryWrongType()
    {
        $this->bjTemplate->setInfoCategory(array(1,2,3));
    }
    //infoCategory 测试 --------------------------------------------------------   end
    
    //exchangeFrequency 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setExchangeFrequency() 正确的传参类型,期望传值正确
     */
    public function testSetExchangeFrequencyCorrectType()
    {
        $this->bjTemplate->setExchangeFrequency(0);
        $this->assertEquals(0, $this->bjTemplate->getExchangeFrequency());
    }

    /**
     * 设置 BjTemplate setExchangeFrequency() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExchangeFrequencyWrongType()
    {
        $this->bjTemplate->setExchangeFrequency(array(1,2,3));
    }
    //exchangeFrequency 测试 --------------------------------------------------------   end

    //infoClassify 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setInfoClassify() 正确的传参类型,期望传值正确
     */
    public function testSetInfoClassifyCorrectType()
    {
        $this->bjTemplate->setInfoClassify(0);
        $this->assertEquals(0, $this->bjTemplate->getInfoClassify());
    }

    /**
     * 设置 BjTemplate setInfoClassify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoClassifyWrongType()
    {
        $this->bjTemplate->setInfoClassify(array(1,2,3));
    }
    //infoClassify 测试 --------------------------------------------------------   end

    //ruleCount 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setRuleCount() 正确的传参类型,期望传值正确
     */
    public function testSetRuleCountCorrectType()
    {
        $this->bjTemplate->setRuleCount(0);
        $this->assertEquals(0, $this->bjTemplate->getRuleCount());
    }

    /**
     * 设置 BjTemplate setRuleCount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRuleCountWrongType()
    {
        $this->bjTemplate->setRuleCount(array(1,2,3));
    }
    //ruleCount 测试 --------------------------------------------------------   end

    //dataTotal 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setDataTotal() 正确的传参类型,期望传值正确
     */
    public function testSetDataTotalCorrectType()
    {
        $this->bjTemplate->setDataTotal(0);
        $this->assertEquals(0, $this->bjTemplate->getDataTotal());
    }

    /**
     * 设置 BjTemplate setDataTotal() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataTotalWrongType()
    {
        $this->bjTemplate->setDataTotal(array(1,2,3));
    }
    //dataTotal 测试 --------------------------------------------------------   end

    //category 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $this->bjTemplate->setCategory(1);
        $this->assertEquals(1, $this->bjTemplate->getCategory());
    }

    /**
     * 设置 BjTemplate setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->bjTemplate->setCategory(['string']);
    }
    //category 测试 --------------------------------------------------------   end

    //userGroupCount 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTemplate setUserGroupCount() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCountCorrectType()
    {
        $this->bjTemplate->setUserGroupCount(10);
        $this->assertEquals(10, $this->bjTemplate->getUserGroupCount());
    }

    /**
     * 设置 BjTemplate setUserGroupCount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupCountWrongType()
    {
        $this->bjTemplate->setUserGroupCount('string');
    }
    //userGroupCount 测试 --------------------------------------------------------   end

    public function testGetIIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\BjTemplateRepository',
            $this->childBjTemplate->getIOperateAbleAdapter()
        );
    }
}
