<?php
namespace Base\Sdk\Template\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullGbTemplateTest extends TestCase
{
    private $gbTemplate;

    private $childGbTemplate;

    public function setUp()
    {
        $this->gbTemplate = NullGbTemplate::getInstance();
        $this->childGbTemplate = new class extends NullGbTemplate
        {
            public function resourceNotExist() : bool
            {
                return parent::resourceNotExist();
            }
        };
    }

    public function tearDown()
    {
        unset($this->gbTemplate);
        unset($this->childGbTemplate);
    }

    public function testExtendsUserGroup()
    {
        $this->assertInstanceof('Base\Sdk\Template\Model\GbTemplate', $this->gbTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->gbTemplate);
    }

    public function testAdd()
    {
        $gbTemplate = $this->getMockBuilder(NullGbTemplate::class)
                                ->setMethods(['resourceNotExist'])
                                ->getMock();
        $gbTemplate->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($gbTemplate->add());
    }

    public function testEdit()
    {
        $gbTemplate = $this->getMockBuilder(NullGbTemplate::class)
                                ->setMethods(['resourceNotExist'])
                                ->getMock();
        $gbTemplate->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($gbTemplate->edit());
    }

    public function testResourceNotExist()
    {
        $result = $this->childGbTemplate->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
