<?php
namespace Base\Sdk\Template\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullBjTemplateTest extends TestCase
{
    private $gbTemplate;

    private $childBjTemplate;

    public function setUp()
    {
        $this->gbTemplate = NullBjTemplate::getInstance();
        $this->childBjTemplate = new class extends NullBjTemplate
        {
            public function resourceNotExist() : bool
            {
                return parent::resourceNotExist();
            }
        };
    }

    public function tearDown()
    {
        unset($this->gbTemplate);
        unset($this->childBjTemplate);
    }

    public function testExtendsUserGroup()
    {
        $this->assertInstanceof('Base\Sdk\Template\Model\BjTemplate', $this->gbTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->gbTemplate);
    }

    public function testAdd()
    {
        $gbTemplate = $this->getMockBuilder(NullBjTemplate::class)
                                ->setMethods(['resourceNotExist'])
                                ->getMock();
        $gbTemplate->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($gbTemplate->add());
    }

    public function testEdit()
    {
        $gbTemplate = $this->getMockBuilder(NullBjTemplate::class)
                                ->setMethods(['resourceNotExist'])
                                ->getMock();
        $gbTemplate->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($gbTemplate->edit());
    }

    public function testResourceNotExist()
    {
        $result = $this->childBjTemplate->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
