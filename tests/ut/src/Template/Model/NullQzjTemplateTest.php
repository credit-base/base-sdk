<?php
namespace Base\Sdk\Template\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullQzjTemplateTest extends TestCase
{
    private $qzjTemplate;

    private $childQzjTemplate;

    public function setUp()
    {
        $this->qzjTemplate = NullQzjTemplate::getInstance();
        $this->childQzjTemplate = new class extends NullQzjTemplate
        {
            public function resourceNotExist() : bool
            {
                return parent::resourceNotExist();
            }
        };
    }

    public function tearDown()
    {
        unset($this->qzjTemplate);
        unset($this->childQzjTemplate);
    }

    public function testExtendsUserGroup()
    {
        $this->assertInstanceof('Base\Sdk\Template\Model\QzjTemplate', $this->qzjTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->qzjTemplate);
    }

    public function testResourceNotExist()
    {
        $result = $this->childQzjTemplate->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
