<?php
namespace Base\Sdk\Template\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Base\Sdk\UserGroup\Model\UserGroup;

class QzjTemplateTest extends TestCase
{
    private $qzjTemplate;

    public function setUp()
    {
        $this->qzjTemplate = new MockQzjTemplate();
    }

    public function tearDown()
    {
        unset($this->qzjTemplate);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf('Base\Sdk\UserGroup\Model\UserGroup', $this->qzjTemplate->getSourceUnit());
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\QzjTemplateRepository',
            $this->qzjTemplate->getRepository()
        );
    }

    //sourceUnit 测试 -------------------------------------------------------- start
    /**
     * 设置 QzjTemplate setSourceUnit() 正确的传参类型,期望传值正确
     */
    public function testSetSourceUnitCorrectType()
    {
        $expected = new UserGroup();

        $this->qzjTemplate->setSourceUnit($expected);
        $this->assertEquals($expected, $this->qzjTemplate->getSourceUnit());
    }

    /**
     * 设置 QzjTemplate setSourceUnit() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSourceUnitWrongType()
    {
        $this->qzjTemplate->setSourceUnit('userGroup');
    }
    //sourceUnit 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\QzjTemplateRepository',
            $this->qzjTemplate->getRepository()
        );
    }

    public function testGetIIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\QzjTemplateRepository',
            $this->qzjTemplate->getIOperateAbleAdapter()
        );
    }
}
