<?php
namespace Base\Sdk\Template\Model;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class GbTemplateTest extends TestCase
{
    private $gbTemplate;

    public function setUp()
    {
        $this->gbTemplate = new GbTemplate();
        $this->childGbTemplate = new class extends GbTemplate
        {
            public function getIOperateAbleAdapter() : IOperateAbleAdapter
            {
                return parent::getIOperateAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->gbTemplate);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->gbTemplate
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\Model\IOperateAble',
            $this->gbTemplate
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\GbTemplateRepository',
            $this->gbTemplate->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 GbTemplate setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->gbTemplate->setId(1);
        $this->assertEquals(1, $this->gbTemplate->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 GbTemplate setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->gbTemplate->setName('string');
        $this->assertEquals('string', $this->gbTemplate->getName());
    }

    /**
     * 设置 GbTemplate setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->gbTemplate->setName(array(1,2,3));
    }
    //name 测试 --------------------------------------------------------   end

    //identify 测试 -------------------------------------------------------- start
    /**
     * 设置 GbTemplate setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->gbTemplate->setIdentify('string');
        $this->assertEquals('string', $this->gbTemplate->getIdentify());
    }

    /**
     * 设置 GbTemplate setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->gbTemplate->setIdentify(array(1,2,3));
    }
    //identify 测试 --------------------------------------------------------   end

    //description 测试 -------------------------------------------------------- start
    /**
     * 设置 GbTemplate setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->gbTemplate->setDescription('string');
        $this->assertEquals('string', $this->gbTemplate->getDescription());
    }

    /**
     * 设置 GbTemplate setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->gbTemplate->setDescription(array(1,2,3));
    }
    //description 测试 --------------------------------------------------------   end

    //subjectCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 GbTemplate setSubjectCategory() 正确的传参类型,期望传值正确
     */
    public function testSetSubjectCategoryCorrectType()
    {
        $this->gbTemplate->setSubjectCategory(array());
        $this->assertEquals(array(), $this->gbTemplate->getSubjectCategory());
    }

    /**
     * 设置 GbTemplate setSubjectCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubjectCategoryWrongType()
    {
        $this->gbTemplate->setSubjectCategory('string');
    }
    //description 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 GbTemplate setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->gbTemplate->setStatus(0);
        $this->assertEquals(0, $this->gbTemplate->getStatus());
    }

    /**
     * 设置 GbTemplate setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->gbTemplate->setStatus(array(1,2,3));
    }
    //status 测试 --------------------------------------------------------   end
    
    //items 测试 -------------------------------------------------------- start
    /**
     * 设置 GbTemplate setItems() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $this->gbTemplate->setItems(array());
        $this->assertEquals(array(), $this->gbTemplate->getItems());
    }

    /**
     * 设置 GbTemplate setItems() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $this->gbTemplate->setItems('string');
    }
    //items 测试 --------------------------------------------------------   end

    //dimension 测试 -------------------------------------------------------- start
    /**
     * 设置 GbTemplate setDimension() 正确的传参类型,期望传值正确
     */
    public function testSetDimensionCorrectType()
    {
        $this->gbTemplate->setDimension(0);
        $this->assertEquals(0, $this->gbTemplate->getDimension());
    }

    /**
     * 设置 GbTemplate setDimension() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDimensionWrongType()
    {
        $this->gbTemplate->setDimension(array(1,2,3));
    }
    //dimension 测试 --------------------------------------------------------   end

    //exchangeFrequency 测试 -------------------------------------------------------- start
    /**
     * 设置 GbTemplate setExchangeFrequency() 正确的传参类型,期望传值正确
     */
    public function testSetExchangeFrequencyCorrectType()
    {
        $this->gbTemplate->setExchangeFrequency(0);
        $this->assertEquals(0, $this->gbTemplate->getExchangeFrequency());
    }

    /**
     * 设置 GbTemplate setExchangeFrequency() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExchangeFrequencyWrongType()
    {
        $this->gbTemplate->setExchangeFrequency(array(1,2,3));
    }
    //exchangeFrequency 测试 --------------------------------------------------------   end

    //infoClassify 测试 -------------------------------------------------------- start
    /**
     * 设置 GbTemplate setInfoClassify() 正确的传参类型,期望传值正确
     */
    public function testSetInfoClassifyCorrectType()
    {
        $this->gbTemplate->setInfoClassify(0);
        $this->assertEquals(0, $this->gbTemplate->getInfoClassify());
    }

    /**
     * 设置 GbTemplate setInfoClassify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoClassifyWrongType()
    {
        $this->gbTemplate->setInfoClassify(array(1,2,3));
    }
    //infoClassify 测试 --------------------------------------------------------   end

    //infoCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 GbTemplate setInfoCategory() 正确的传参类型,期望传值正确
     */
    public function testSetInfoCategoryCorrectType()
    {
        $this->gbTemplate->setInfoCategory(0);
        $this->assertEquals(0, $this->gbTemplate->getInfoCategory());
    }

    /**
     * 设置 GbTemplate setInfoCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoCategoryWrongType()
    {
        $this->gbTemplate->setInfoCategory(array(1,2,3));
    }
    //infoCategory 测试 --------------------------------------------------------   end

    public function testGetIIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\GbTemplateRepository',
            $this->childGbTemplate->getIOperateAbleAdapter()
        );
    }
}
