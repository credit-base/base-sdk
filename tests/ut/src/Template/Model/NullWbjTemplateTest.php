<?php
namespace Base\Sdk\Template\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullWbjTemplateTest extends TestCase
{
    private $gbTemplate;

    private $childWbjTemplate;

    public function setUp()
    {
        $this->gbTemplate = NullWbjTemplate::getInstance();
        $this->childWbjTemplate = new class extends NullWbjTemplate
        {
            public function resourceNotExist() : bool
            {
                return parent::resourceNotExist();
            }
        };
    }

    public function tearDown()
    {
        unset($this->gbTemplate);
        unset($this->childWbjTemplate);
    }

    public function testExtendsUserGroup()
    {
        $this->assertInstanceof('Base\Sdk\Template\Model\WbjTemplate', $this->gbTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->gbTemplate);
    }

    public function testAdd()
    {
        $gbTemplate = $this->getMockBuilder(NullWbjTemplate::class)
                                ->setMethods(['resourceNotExist'])
                                ->getMock();
        $gbTemplate->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($gbTemplate->add());
    }

    public function testEdit()
    {
        $gbTemplate = $this->getMockBuilder(NullWbjTemplate::class)
                                ->setMethods(['resourceNotExist'])
                                ->getMock();
        $gbTemplate->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($gbTemplate->edit());
    }

    public function testResourceNotExist()
    {
        $result = $this->childWbjTemplate->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
