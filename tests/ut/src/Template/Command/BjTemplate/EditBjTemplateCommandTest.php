<?php
namespace Base\Sdk\Template\Command\BjTemplate;

use PHPUnit\Framework\TestCase;

class EditBjTemplateCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'id' => $faker->randomNumber(),
            'name' => $faker->name(),
            'identify' => $faker->name(),
            'description' => $faker->name(),
            'subjectCategory' => array(),
            'items' => array(),
            'sourceUnit' => $faker->randomNumber(),
            'gbTemplate' => $faker->randomNumber(),
            'dimension' => $faker->randomNumber(),
            'exchangeFrequency' => $faker->randomNumber(),
            'infoClassify' => $faker->randomNumber(),
            'infoCategory' => $faker->randomNumber()
        );

        $this->command = new EditBjTemplateCommand(
            $this->fakerData['name'],
            $this->fakerData['identify'],
            $this->fakerData['description'],
            $this->fakerData['subjectCategory'],
            $this->fakerData['items'],
            $this->fakerData['sourceUnit'],
            $this->fakerData['gbTemplate'],
            $this->fakerData['dimension'],
            $this->fakerData['exchangeFrequency'],
            $this->fakerData['infoClassify'],
            $this->fakerData['infoCategory'],
            $this->fakerData['id']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
