<?php
namespace Base\Sdk\Template\Command\BaseTemplate;

use PHPUnit\Framework\TestCase;

class EditBaseTemplateCommandTest extends TestCase
{
    private $faker = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->faker = array(
            'id' => $faker->randomNumber(),
            'name' => $faker->name(),
            'identify' => $faker->name(),
            'description' => $faker->name(),
            'subjectCategory' => array(),
            'items' => array(),
            'sourceUnit' => $faker->randomNumber(),
            'gbTemplate' => $faker->randomNumber(),
            'dimension' => $faker->randomNumber(),
            'exchangeFrequency' => $faker->randomNumber(),
            'infoClassify' => $faker->randomNumber(),
            'infoCategory' => $faker->randomNumber()
        );

        $this->command = new EditBaseTemplateCommand(
            $this->faker['name'],
            $this->faker['identify'],
            $this->faker['description'],
            $this->faker['subjectCategory'],
            $this->faker['items'],
            $this->faker['dimension'],
            $this->faker['exchangeFrequency'],
            $this->faker['infoClassify'],
            $this->faker['infoCategory'],
            $this->faker['id']
        );
    }

    public function tearDown()
    {
        unset($this->faker);
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
