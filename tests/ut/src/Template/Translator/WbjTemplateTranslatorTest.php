<?php
namespace Base\Sdk\Template\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Utils\BjTemplate\BjTemplateUtils;

 /**
   * @SuppressWarnings(PHPMD)
   */
class WbjTemplateTranslatorTest extends TestCase
{
    use BjTemplateUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new MockWbjTemplateTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Template\Model\NullWbjTemplate', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testGetWbjTemplateTypeCn()
    {
        $data = \Base\Sdk\Template\Utils\TemplateArrayMockFactory::generateTemplateTypeCn();
        $result = $this->translator->getTypeCn(1);
        
        $this->assertEquals($data, $result);
    }

    public function testGetWbjTemplateDimensionCn()
    {
        $data = \Base\Sdk\Template\Utils\TemplateArrayMockFactory::generateTemplateDimensionCn();
        $result = $this->translator->getDimensionCn(1);
        
        $this->assertEquals($data, $result);
    }

    public function testGetWbjTemplateItemCn()
    {
        $data = \Base\Sdk\Template\Utils\TemplateArrayMockFactory::generateTemplateItemCn();
        $expression = array(array(
            "name" => '主体名称',    //信息项名称
            "identify" => 'ZTMC',    //数据标识
            "type" => 1,    //数据类型
            "length" => '200',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
            "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则
            "remarks" => '信用主体名称',    //备注
        ));
        $result = $this->translator->getItemCn($expression);
        
        $this->assertEquals($data, $result);
    }

    public function testGetWbjTemplateIsMaskedCn()
    {
        $data = \Base\Sdk\Template\Utils\TemplateArrayMockFactory::generateTemplateIsMaskedCn();
        $result = $this->translator->getIsMaskedCn(1);
        
        $this->assertEquals($data, $result);
    }

    public function testGetWbjTemplateIsNecessaryCn()
    {
        $data = \Base\Sdk\Template\Utils\TemplateArrayMockFactory::generateTemplateIsNecessaryCn();
        $result = $this->translator->getIsNecessaryCn(1);
        
        $this->assertEquals($data, $result);
    }

    public function testGetWbjTemplateSubjectCategoryCn()
    {
        $data = \Base\Sdk\Template\Utils\TemplateArrayMockFactory::generateTemplateSubjectCategoryCn();
        $subjectCategory = array(1);
        $result = $this->translator->getSubjectCategoryCn($subjectCategory);
        
        $this->assertEquals($data, $result);
    }

    public function testObjectToArray()
    {
        $gbTemplate = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate(1);

        $expression = $this->translator->objectToArray($gbTemplate);

        $this->compareArrayAndObject($expression, $gbTemplate);
    }

    public function testObjectToArrayFail()
    {
        $gbTemplate = null;

        $expression = $this->translator->objectToArray($gbTemplate);
        $this->assertEquals(array(), $expression);
    }
}
