<?php
namespace Base\Sdk\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Utils\WbjTemplate\WbjTemplateRestfulUtils;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\UserGroup\Model\UserGroup;

class QzjTemplateRestfulTranslatorTest extends TestCase
{
    use WbjTemplateRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new QzjTemplateRestfulTranslator();
        $this->childTranslator = new class extends QzjTemplateRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $qzjTemplate = \Base\Sdk\Template\Utils\QzjTemplate\MockQzjTemplateFactory::generateQzjTemplate(1);

        $expression['data']['id'] = $qzjTemplate->getId();
        $expression['data']['attributes']['name'] = $qzjTemplate->getName();
        $expression['data']['attributes']['createTime'] = $qzjTemplate->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $qzjTemplate->getUpdateTime();
        $expression['data']['attributes']['identify'] = $qzjTemplate->getIdentify();
        $expression['data']['attributes']['description'] = $qzjTemplate->getDescription();
        $expression['data']['attributes']['subjectCategory'] = $qzjTemplate->getSubjectCategory();
        $expression['data']['attributes']['items'] = $qzjTemplate->getItems();
        $expression['data']['attributes']['dimension'] = $qzjTemplate->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $qzjTemplate->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $qzjTemplate->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $qzjTemplate->getInfoCategory();
        $expression['data']['attributes']['ruleCount'] = $qzjTemplate->getRuleCount();
        $expression['data']['attributes']['dataTotal'] = $qzjTemplate->getDataTotal();
        $expression['data']['attributes']['category'] = $qzjTemplate->getCategory();
        $expression['data']['attributes']['userGroupCount'] = $qzjTemplate->getUserGroupCount();

        $qzjTemplateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Template\Model\QzjTemplate', $qzjTemplateObject);
        $this->compareArrayAndObject($expression, $qzjTemplateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $qzjTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Template\Model\NullQzjTemplate', $qzjTemplate);
    }

    public function testObjectToArray()
    {
        $qzjTemplate = \Base\Sdk\Template\Utils\QzjTemplate\MockQzjTemplateFactory::generateQzjTemplate(1);

        $expression = $this->translator->objectToArray($qzjTemplate);

        $this->compareArrayAndObject($expression, $qzjTemplate);
    }

    public function testObjectToArrayFail()
    {
        $qzjTemplate = null;

        $expression = $this->translator->objectToArray($qzjTemplate);
        $this->assertEquals(array(), $expression);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['sourceUnit']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用1次, 用于处理 sourceUnit 且返回数组 $sourceUnitInfo
     * 4. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $sourceUnitInfo, 出参是 $sourceUnit
     * 5. $bjTemplate->setSourceUnit 调用一次, 入参 $sourceUnit
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'sourceUnit'=>['data'=>'mockSourceUnit']
        ];

        $qzjTemplateTranslator = $this->getMockBuilder(QzjTemplateRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getUserGroupRestfulTranslator',
         ])
        ->getMock();

        $sourceUnitInfo = ['mockSourceUnit'];

        $sourceUnit = new UserGroup();

        //预言
        $qzjTemplateTranslator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用1次, 入参sourceUnit
        //返回 $sourceUnitInfo
        $qzjTemplateTranslator->expects($this->exactly(1))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['sourceUnit']['data']]
             )
            ->will($this->onConsecutiveCalls($sourceUnitInfo));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($sourceUnitInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceUnit);

        //绑定
        $qzjTemplateTranslator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());

        //揭示
        $qzjTemplate = $qzjTemplateTranslator->arrayToObject($expression);
        $this->assertEquals($sourceUnit, $qzjTemplate->getSourceUnit());
    }
}
