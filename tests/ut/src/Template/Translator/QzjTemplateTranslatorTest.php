<?php
namespace Base\Sdk\Template\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Utils\BjTemplate\BjTemplateUtils;

class QzjTemplateTranslatorTest extends TestCase
{
    use BjTemplateUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new QzjTemplateTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Template\Model\NullQzjTemplate', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $qzjTemplate = \Base\Sdk\Template\Utils\QzjTemplate\MockQzjTemplateFactory::generateQzjTemplate(1);

        $expression = $this->translator->objectToArray($qzjTemplate);

        $this->compareArrayAndObject($expression, $qzjTemplate);
    }

    public function testObjectToArrayFail()
    {
        $gbTemplate = null;

        $expression = $this->translator->objectToArray($gbTemplate);
        $this->assertEquals(array(), $expression);
    }
}
