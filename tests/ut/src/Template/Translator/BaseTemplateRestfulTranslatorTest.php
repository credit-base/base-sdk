<?php
namespace Base\Sdk\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Utils\BaseTemplate\BaseTemplateRestfulUtils;

use Base\Sdk\Template\Translator\GbTemplateRestfulTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\UserGroup\Model\UserGroup;

class BaseTemplateRestfulTranslatorTest extends TestCase
{
    use BaseTemplateRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BaseTemplateRestfulTranslator();
        $this->childTranslator = new class extends BaseTemplateRestfulTranslator
        {
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $baseTemplate = \Base\Sdk\Template\Utils\BaseTemplate\MockBaseTemplateFactory::generateBaseTemplate(1);

        $expression['data']['id'] = $baseTemplate->getId();
        $expression['data']['attributes']['name'] = $baseTemplate->getName();
        $expression['data']['attributes']['createTime'] = $baseTemplate->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $baseTemplate->getUpdateTime();
        $expression['data']['attributes']['identify'] = $baseTemplate->getIdentify();
        $expression['data']['attributes']['description'] = $baseTemplate->getDescription();
        $expression['data']['attributes']['subjectCategory'] = $baseTemplate->getSubjectCategory();
        $expression['data']['attributes']['items'] = $baseTemplate->getItems();
        $expression['data']['attributes']['dimension'] = $baseTemplate->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $baseTemplate->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $baseTemplate->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $baseTemplate->getInfoCategory();
        $expression['data']['attributes']['ruleCount'] = $baseTemplate->getRuleCount();
        $expression['data']['attributes']['dataTotal'] = $baseTemplate->getDataTotal();
        $expression['data']['attributes']['category'] = $baseTemplate->getCategory();
        $expression['data']['attributes']['userGroupCount'] = $baseTemplate->getUserGroupCount();

        $baseTemplateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Template\Model\BaseTemplate', $baseTemplateObject);
        $this->compareArrayAndObject($expression, $baseTemplateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $baseTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Template\Model\NullBaseTemplate', $baseTemplate);
    }

    public function testObjectToArray()
    {
        $baseTemplate = \Base\Sdk\Template\Utils\BaseTemplate\MockBaseTemplateFactory::generateBaseTemplate(1);

        $expression = $this->translator->objectToArray($baseTemplate);

        $this->compareArrayAndObject($expression, $baseTemplate);
    }

    public function testObjectToArrayFail()
    {
        $baseTemplate = null;

        $expression = $this->translator->objectToArray($baseTemplate);
        $this->assertEquals(array(), $expression);
    }
}
