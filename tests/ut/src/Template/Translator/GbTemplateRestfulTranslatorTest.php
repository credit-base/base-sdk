<?php
namespace Base\Sdk\Template\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Utils\GbTemplate\GbTemplateRestfulUtils;

class GbTemplateRestfulTranslatorTest extends TestCase
{
    use GbTemplateRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new GbTemplateRestfulTranslator();
        $this->childTranslator = new class extends GbTemplateRestfulTranslator
        {
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $gbTemplate = \Base\Sdk\Template\Utils\GbTemplate\MockGbTemplateFactory::generateGbTemplate(1);

        $expression['data']['id'] = $gbTemplate->getId();
        $expression['data']['attributes']['name'] = $gbTemplate->getName();
        $expression['data']['attributes']['updateTime'] = $gbTemplate->getUpdateTime();
        $expression['data']['attributes']['createTime'] = $gbTemplate->getCreateTime();
        $expression['data']['attributes']['identify'] = $gbTemplate->getIdentify();
        $expression['data']['attributes']['subjectCategory'] = $gbTemplate->getSubjectCategory();
        $expression['data']['attributes']['description'] = $gbTemplate->getDescription();
        $expression['data']['attributes']['items'] = $gbTemplate->getItems();
        $expression['data']['attributes']['exchangeFrequency'] = $gbTemplate->getExchangeFrequency();
        $expression['data']['attributes']['dimension'] = $gbTemplate->getDimension();
        $expression['data']['attributes']['infoClassify'] = $gbTemplate->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $gbTemplate->getInfoCategory();
        $expression['data']['attributes']['ruleCount'] = $gbTemplate->getRuleCount();
        $expression['data']['attributes']['dataTotal'] = $gbTemplate->getDataTotal();
        $expression['data']['attributes']['category'] = $gbTemplate->getCategory();
        $expression['data']['attributes']['userGroupCount'] = $gbTemplate->getUserGroupCount();
        
        $gbTemplateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Template\Model\GbTemplate', $gbTemplateObject);
        $this->compareArrayAndObject($expression, $gbTemplateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $gbTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Template\Model\NullGbTemplate', $gbTemplate);
    }

    public function testObjectToArray()
    {
        $gbTemplate = \Base\Sdk\Template\Utils\GbTemplate\MockGbTemplateFactory::generateGbTemplate(1);

        $expression = $this->translator->objectToArray($gbTemplate);

        $this->compareArrayAndObject($expression, $gbTemplate);
    }

    public function testObjectToArrayFail()
    {
        $gbTemplate = null;

        $expression = $this->translator->objectToArray($gbTemplate);
        $this->assertEquals(array(), $expression);
    }
}
