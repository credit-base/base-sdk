<?php
namespace Base\Sdk\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Utils\WbjTemplate\WbjTemplateRestfulUtils;

use Base\Sdk\Template\Translator\GbTemplateRestfulTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\UserGroup\Model\UserGroup;

class WbjTemplateRestfulTranslatorTest extends TestCase
{
    use WbjTemplateRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WbjTemplateRestfulTranslator();
        $this->childTranslator = new class extends WbjTemplateRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $wbjTemplate = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate(1);

        $expression['data']['id'] = $wbjTemplate->getId();
        $expression['data']['attributes']['name'] = $wbjTemplate->getName();
        $expression['data']['attributes']['createTime'] = $wbjTemplate->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $wbjTemplate->getUpdateTime();
        $expression['data']['attributes']['identify'] = $wbjTemplate->getIdentify();
        $expression['data']['attributes']['description'] = $wbjTemplate->getDescription();
        $expression['data']['attributes']['subjectCategory'] = $wbjTemplate->getSubjectCategory();
        $expression['data']['attributes']['items'] = $wbjTemplate->getItems();
        $expression['data']['attributes']['dimension'] = $wbjTemplate->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $wbjTemplate->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $wbjTemplate->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $wbjTemplate->getInfoCategory();
        $expression['data']['attributes']['ruleCount'] = $wbjTemplate->getRuleCount();
        $expression['data']['attributes']['dataTotal'] = $wbjTemplate->getDataTotal();
        $expression['data']['attributes']['category'] = $wbjTemplate->getCategory();
        $expression['data']['attributes']['userGroupCount'] = $wbjTemplate->getUserGroupCount();

        $wbjTemplateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Template\Model\WbjTemplate', $wbjTemplateObject);
        $this->compareArrayAndObject($expression, $wbjTemplateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $wbjTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Template\Model\NullWbjTemplate', $wbjTemplate);
    }

    public function testObjectToArray()
    {
        $wbjTemplate = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate(1);

        $expression = $this->translator->objectToArray($wbjTemplate);

        $this->compareArrayAndObject($expression, $wbjTemplate);
    }

    public function testObjectToArrayFail()
    {
        $wbjTemplate = null;

        $expression = $this->translator->objectToArray($wbjTemplate);
        $this->assertEquals(array(), $expression);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['sourceUnit']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用1次, 用于处理 sourceUnit 且返回数组 $sourceUnitInfo
     * 4. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $sourceUnitInfo, 出参是 $sourceUnit
     * 5. $bjTemplate->setSourceUnit 调用一次, 入参 $sourceUnit
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'sourceUnit'=>['data'=>'mockSourceUnit']
        ];

        $translator = $this->getMockBuilder(WbjTemplateRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getUserGroupRestfulTranslator',
         ])
        ->getMock();

        $sourceUnitInfo = ['mockSourceUnit'];

        $sourceUnit = new UserGroup();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用1次, 入参sourceUnit
        //返回 $sourceUnitInfo
        $translator->expects($this->exactly(1))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['sourceUnit']['data']]
             )
            ->will($this->onConsecutiveCalls($sourceUnitInfo));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($sourceUnitInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceUnit);

        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());

        //揭示
        $wbjTemplate = $translator->arrayToObject($expression);
        $this->assertEquals($sourceUnit, $wbjTemplate->getSourceUnit());
    }
}
