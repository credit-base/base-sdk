<?php
namespace Base\Sdk\Template\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Utils\BjTemplate\BjTemplateRestfulUtils;

use Base\Sdk\Template\Translator\GbTemplateRestfulTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\UserGroup\Model\UserGroup;

class BjTemplateRestfulTranslatorTest extends TestCase
{
    use BjTemplateRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BjTemplateRestfulTranslator();
        $this->childTranslator = new class extends BjTemplateRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }

            public function getGbTemplateRestfulTranslator() : GbTemplateRestfulTranslator
            {
                return parent::getGbTemplateRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $bjTemplate = \Base\Sdk\Template\Utils\BjTemplate\MockBjTemplateFactory::generateBjTemplate(1);

        $expression['data']['id'] = $bjTemplate->getId();
        $expression['data']['attributes']['name'] = $bjTemplate->getName();
        $expression['data']['attributes']['createTime'] = $bjTemplate->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $bjTemplate->getUpdateTime();
        $expression['data']['attributes']['identify'] = $bjTemplate->getIdentify();
        $expression['data']['attributes']['description'] = $bjTemplate->getDescription();
        $expression['data']['attributes']['subjectCategory'] = $bjTemplate->getSubjectCategory();
        $expression['data']['attributes']['items'] = $bjTemplate->getItems();
        $expression['data']['attributes']['dimension'] = $bjTemplate->getDimension();
        $expression['data']['attributes']['exchangeFrequency'] = $bjTemplate->getExchangeFrequency();
        $expression['data']['attributes']['infoClassify'] = $bjTemplate->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $bjTemplate->getInfoCategory();
        $expression['data']['attributes']['ruleCount'] = $bjTemplate->getRuleCount();
        $expression['data']['attributes']['dataTotal'] = $bjTemplate->getDataTotal();
        $expression['data']['attributes']['category'] = $bjTemplate->getCategory();
        $expression['data']['attributes']['userGroupCount'] = $bjTemplate->getUserGroupCount();

        $bjTemplateObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Template\Model\BjTemplate', $bjTemplateObject);
        $this->compareArrayAndObject($expression, $bjTemplateObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $bjTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Template\Model\NullBjTemplate', $bjTemplate);
    }

    public function testObjectToArray()
    {
        $bjTemplate = \Base\Sdk\Template\Utils\BjTemplate\MockBjTemplateFactory::generateBjTemplate(1);

        $expression = $this->translator->objectToArray($bjTemplate);

        $this->compareArrayAndObject($expression, $bjTemplate);
    }

    public function testObjectToArrayFail()
    {
        $bjTemplate = null;

        $expression = $this->translator->objectToArray($bjTemplate);
        $this->assertEquals(array(), $expression);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetGbTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Translator\GbTemplateRestfulTranslator',
            $this->childTranslator->getGbTemplateRestfulTranslator()
        );
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['gbTemplate']['data'] 和
     * $relationships['sourceUnit']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用两次, 分别用于处理 sourceUnit 和 gbTemplate 且返回各自的数组 $gbTemplateInfo 和 $sourceUnitInfo
     * 4. $this->getGbTemplateRestfulTranslator()->arrayToObject 被调用一次, 入参是 $gbTemplateInfo, 出参是 $gbTemplate
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $sourceUnitInfo, 出参是 $sourceUnit
     * 6. $bjTemplate->setGbTemplate 调用一次, 入参 $gbTemplate
     * 7. $bjTemplate->setSourceUnit 调用一次, 入参 $sourceUnit
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'gbTemplate'=>['data'=>'mockGbTemplate'],
            'sourceUnit'=>['data'=>'mockSourceUnit']
        ];

        $translator = $this->getMockBuilder(BjTemplateRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getGbTemplateRestfulTranslator',
             'getUserGroupRestfulTranslator',
         ])
        ->getMock();

        $gbTemplateInfo = ['mockGbTemplate'];
        $sourceUnitInfo = ['mockSourceUnit'];

        $gbTemplate = new GbTemplate();
        $sourceUnit = new UserGroup();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用2次, 依次入参 gbTemplate, sourceUnit
        //依次返回 $gbTemplateInfo 和 $sourceUnitInfo
        $translator->expects($this->exactly(2))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['gbTemplate']['data']],
                 [$relationships['sourceUnit']['data']]
             )
            ->will($this->onConsecutiveCalls($gbTemplateInfo, $sourceUnitInfo));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($sourceUnitInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceUnit);

        $gbTemplateRestfulTranslator = $this->prophesize(GbTemplateRestfulTranslator::class);
        $gbTemplateRestfulTranslator->arrayToObject(Argument::exact($gbTemplateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($gbTemplate);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getGbTemplateRestfulTranslator')
            ->willReturn($gbTemplateRestfulTranslator->reveal());

        //揭示
        $bjTemplate = $translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Template\Model\BjTemplate', $bjTemplate);
        $this->assertEquals($gbTemplate, $bjTemplate->getGbTemplate());
        $this->assertEquals($sourceUnit, $bjTemplate->getSourceUnit());
    }
}
