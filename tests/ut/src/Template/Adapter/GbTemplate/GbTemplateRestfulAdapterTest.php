<?php
namespace Base\Sdk\Template\Adapter\GbTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;
use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\Model\NullGbTemplate;
use Base\Sdk\Template\Translator\GbTemplateRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class GbTemplateRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(GbTemplateRestfulAdapter::class)
                           ->setMethods([
                                'getTranslator',
                                'translateToObject',
                                'objectToArray',
                                'getResource',
                                'fetchOneAction',
                                'post',
                                'get',
                                'isSuccess',
                                'patch'
                           ])
                           ->getMock();
                           
        $this->childAdapter = new class extends GbTemplateRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIGbTemplateAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Adapter\GbTemplate\IGbTemplateAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('gbTemplates', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'GBTEMPLATE_LIST',
                GbTemplateRestfulAdapter::SCENARIOS['GBTEMPLATE_LIST']
            ],
            [
                'GBTEMPLATE_FETCH_ONE',
                GbTemplateRestfulAdapter::SCENARIOS['GBTEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $gbTemplate = \Base\Sdk\Template\Utils\GbTemplate\MockGbTemplateFactory::generateGbTemplate(1);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullGbTemplate())
            ->willReturn($gbTemplate);
        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($gbTemplate, $result);
    }

    /**
     * 为GbTemplateRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$gbTemplate,$keys,$gbTemplateArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareGbTemplateTranslator(
        GbTemplate $gbTemplate,
        array $keys,
        array $gbTemplateArray
    ) {
        $translator = $this->prophesize(GbTemplateRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($gbTemplate),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($gbTemplateArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(GbTemplate $gbTemplate)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($gbTemplate);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGbTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $gbTemplate = \Base\Sdk\Template\Utils\GbTemplate\MockGbTemplateFactory::generateGbTemplate(1);
        $gbTemplateArray = array();

        $this->prepareGbTemplateTranslator(
            $gbTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items'
            ),
            $gbTemplateArray
        );

        $this->adapter
            ->method('post')
            ->with('', $gbTemplateArray);

        $this->success($gbTemplate);
        $result = $this->adapter->add($gbTemplate);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGbTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $gbTemplate = \Base\Sdk\Template\Utils\GbTemplate\MockGbTemplateFactory::generateGbTemplate(1);
        $gbTemplateArray = array();

        $this->prepareGbTemplateTranslator(
            $gbTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items'
            ),
            $gbTemplateArray
        );

        $this->adapter
            ->method('post')
            ->with('', $gbTemplateArray);
        
            $this->failure($gbTemplate);
            $result = $this->adapter->add($gbTemplate);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $gbTemplate = \Base\Sdk\Template\Utils\GbTemplate\MockGbTemplateFactory::generateGbTemplate(1);
        $gbTemplateArray = array();

        $this->prepareGbTemplateTranslator(
            $gbTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items'
            ),
            $gbTemplateArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$gbTemplate->getId(), $gbTemplateArray);

        $this->success($gbTemplate);
        $result = $this->adapter->edit($gbTemplate);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $gbTemplate = \Base\Sdk\Template\Utils\GbTemplate\MockGbTemplateFactory::generateGbTemplate(1);
        $gbTemplateArray = array();

        $this->prepareGbTemplateTranslator(
            $gbTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items'
            ),
            $gbTemplateArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$gbTemplate->getId(), $gbTemplateArray);
        
            $this->failure($gbTemplate);
            $result = $this->adapter->edit($gbTemplate);
            $this->assertFalse($result);
    }
}
