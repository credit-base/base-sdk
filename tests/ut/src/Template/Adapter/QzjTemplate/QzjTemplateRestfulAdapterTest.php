<?php
namespace Base\Sdk\Template\Adapter\QzjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;
use Base\Sdk\Template\Model\QzjTemplate;
use Base\Sdk\Template\Model\NullQzjTemplate;
use Base\Sdk\Template\Translator\QzjTemplateRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class QzjTemplateRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(QzjTemplateRestfulAdapter::class)
                           ->setMethods([
                                'getTranslator',
                                'translateToObject',
                                'objectToArray',
                                'getResource',
                                'fetchOneAction',
                                'post',
                                'get',
                                'isSuccess',
                                'patch',
                                'commonMapErrors',
                           ])
                           ->getMock();
                           
        $this->childAdapter = new class extends QzjTemplateRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getQzjTemplateMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIQzjTemplateAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Adapter\QzjTemplate\IQzjTemplateAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('qzjTemplates', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'QZJ_TEMPLATE_LIST',
                QzjTemplateRestfulAdapter::SCENARIOS['QZJ_TEMPLATE_LIST']
            ],
            [
                'QZJ_TEMPLATE_FETCH_ONE',
                QzjTemplateRestfulAdapter::SCENARIOS['QZJ_TEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $qzjTemplate = \Base\Sdk\Template\Utils\QzjTemplate\MockQzjTemplateFactory::generateQzjTemplate(1);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullQzjTemplate())
            ->willReturn($qzjTemplate);
        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($qzjTemplate, $result);
    }

    /**
     * 为QzjTemplateRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$QzjTemplate,$keys,$QzjTemplateArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareQzjTemplateTranslator(
        QzjTemplate $qzjTemplate,
        array $keys,
        array $qzjTemplateArray
    ) {
        $translator = $this->prophesize(QzjTemplateRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($qzjTemplate),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($qzjTemplateArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(QzjTemplate $QzjTemplate)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($QzjTemplate);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareQzjTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $qzjTemplate = \Base\Sdk\Template\Utils\QzjTemplate\MockQzjTemplateFactory::generateQzjTemplate(1);
        $qzjTemplateArray = array();

        $this->prepareQzjTemplateTranslator(
            $qzjTemplate,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'category',
                'description',
                'items',
                'sourceUnit'
            ),
            $qzjTemplateArray
        );

        $this->adapter
            ->method('post')
            ->with('', $qzjTemplateArray);

        $this->success($qzjTemplate);
        $result = $this->adapter->add($qzjTemplate);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareQzjTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $qzjTemplate = \Base\Sdk\Template\Utils\QzjTemplate\MockQzjTemplateFactory::generateQzjTemplate(1);
        $qzjTemplateArray = array();

        $this->prepareQzjTemplateTranslator(
            $qzjTemplate,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'category',
                'description',
                'items',
                'sourceUnit'
            ),
            $qzjTemplateArray
        );

        $this->adapter
            ->method('post')
            ->with('', $qzjTemplateArray);
        
            $this->failure($qzjTemplate);
            $result = $this->adapter->add($qzjTemplate);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $qzjTemplate = \Base\Sdk\Template\Utils\QzjTemplate\MockQzjTemplateFactory::generateQzjTemplate(1);
        $qzjTemplateArray = array();

        $this->prepareQzjTemplateTranslator(
            $qzjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'category',
                'description',
                'items',
                'sourceUnit'
            ),
            $qzjTemplateArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$qzjTemplate->getId(), $qzjTemplateArray);

        $this->success($qzjTemplate);
        $result = $this->adapter->edit($qzjTemplate);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $qzjTemplate = \Base\Sdk\Template\Utils\QzjTemplate\MockQzjTemplateFactory::generateQzjTemplate(1);
        $qzjTemplateArray = array();

        $this->prepareQzjTemplateTranslator(
            $qzjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'category',
                'description',
                'items',
                'sourceUnit'
            ),
            $qzjTemplateArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$qzjTemplate->getId(), $qzjTemplateArray);
        
            $this->failure($qzjTemplate);
            $result = $this->adapter->edit($qzjTemplate);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [
            101=>[
                'qzjTemplate' => QZJ_TEMPLATE_EXIST,
                'name' => TEMPLATE_NAME_FORMAT_ERROR,
                'identify'=>IDENTIFY_FORMAT_ERROR,
                'subjectCategory'=>SUBJECT_CATEGORY_FORMAT_ERROR,
                'dimension' => DIMENSION_FORMAT_ERROR,
                'infoClassify'=>INFO_CLASSIFY_FORMAT_ERROR,
                'infoCategory'=>INFO_CATEGORY_NOT_EXIST,
                'description'=>DESCRIPTION_FORMAT_ERROR,
                'exchangeFrequency'=>EXCHANGE_FREQUENCY_FORMAT_ERROR,
                'sourceUnit'=>SOURCE_UNIT_FORMAT_ERROR,
                'items'=>ITEMS_FORMAT_ERROR,
                'ZTMC' => ZTMC_FORMAT_ERROR,
                'ZJHM' => CARDID_FORMAT_ERROR,
                'TYSHXYDM' => TYSHXYDM_FORMAT_ERROR,
                'type' => ITEM_TYPE_FORMAT_ERROR,
                'length' => ITEM_LENGTH_FORMAT_ERROR,
                'options' => ITEM_OPTIONS_FORMAT_ERROR,
                'isNecessary' => IS_NECESSARY_FORMAT_ERROR,
                'isMasked' => IS_MASKED_FORMAT_ERROR,
                'maskRule' => MASK_RULE_FORMAT_ERROR,
                'remarks' => REMARKS_FORMAT_ERROR
            ],
        ];
        
        $result = $this->childAdapter->getQzjTemplateMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
