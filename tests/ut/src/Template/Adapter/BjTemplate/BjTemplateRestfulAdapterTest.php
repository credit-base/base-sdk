<?php
namespace Base\Sdk\Template\Adapter\BjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\Template\Model\NullBjTemplate;
use Base\Sdk\Template\Translator\BjTemplateRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class BjTemplateRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(BjTemplateRestfulAdapter::class)
                            ->setMethods([
                                'getTranslator',
                                'translateToObject',
                                'objectToArray',
                                'getResource',
                                'fetchOneAction',
                                'post',
                                'get',
                                'isSuccess',
                                'patch'
                            ])
                            ->getMock();
                           
        $this->childAdapter = new class extends BjTemplateRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIBjTemplateAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Adapter\BjTemplate\IBjTemplateAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('bjTemplates', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'BJTEMPLATE_LIST',
                BjTemplateRestfulAdapter::SCENARIOS['BJTEMPLATE_LIST']
            ],
            [
                'BJTEMPLATE_FETCH_ONE',
                BjTemplateRestfulAdapter::SCENARIOS['BJTEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $bjTemplate = \Base\Sdk\Template\Utils\BjTemplate\MockBjTemplateFactory::generateBjTemplate(1);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullBjTemplate())
            ->willReturn($bjTemplate);
        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($bjTemplate, $result);
    }

    /**
     * 为BjTemplateRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$bjTemplate,$keys,$bjTemplateArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareBjTemplateTranslator(
        BjTemplate $bjTemplate,
        array $keys,
        array $bjTemplateArray
    ) {
        $translator = $this->prophesize(BjTemplateRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($bjTemplate),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($bjTemplateArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(BjTemplate $bjTemplate)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($bjTemplate);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBjTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $bjTemplate = \Base\Sdk\Template\Utils\BjTemplate\MockBjTemplateFactory::generateBjTemplate(1);
        $bjTemplateArray = array();

        $this->prepareBjTemplateTranslator(
            $bjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'gbTemplate'
            ),
            $bjTemplateArray
        );

        $this->adapter
            ->method('post')
            ->with('', $bjTemplateArray);

        $this->success($bjTemplate);
        $result = $this->adapter->add($bjTemplate);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBjTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $bjTemplate = \Base\Sdk\Template\Utils\BjTemplate\MockBjTemplateFactory::generateBjTemplate(1);
        $bjTemplateArray = array();

        $this->prepareBjTemplateTranslator(
            $bjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'gbTemplate'
            ),
            $bjTemplateArray
        );

        $this->adapter
            ->method('post')
            ->with('', $bjTemplateArray);
        
            $this->failure($bjTemplate);
            $result = $this->adapter->add($bjTemplate);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $bjTemplate = \Base\Sdk\Template\Utils\BjTemplate\MockBjTemplateFactory::generateBjTemplate(1);
        $bjTemplateArray = array();

        $this->prepareBjTemplateTranslator(
            $bjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'gbTemplate'
            ),
            $bjTemplateArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$bjTemplate->getId(), $bjTemplateArray);

        $this->success($bjTemplate);
        $result = $this->adapter->edit($bjTemplate);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $bjTemplate = \Base\Sdk\Template\Utils\BjTemplate\MockBjTemplateFactory::generateBjTemplate(1);
        $bjTemplateArray = array();

        $this->prepareBjTemplateTranslator(
            $bjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'gbTemplate'
            ),
            $bjTemplateArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$bjTemplate->getId(), $bjTemplateArray);
        
            $this->failure($bjTemplate);
            $result = $this->adapter->edit($bjTemplate);
            $this->assertFalse($result);
    }
}
