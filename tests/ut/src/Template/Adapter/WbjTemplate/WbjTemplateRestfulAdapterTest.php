<?php
namespace Base\Sdk\Template\Adapter\WbjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;
use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\Model\NullWbjTemplate;
use Base\Sdk\Template\Translator\WbjTemplateRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class WbjTemplateRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(WbjTemplateRestfulAdapter::class)
                           ->setMethods([
                                'getTranslator',
                                'translateToObject',
                                'objectToArray',
                                'getResource',
                                'fetchOneAction',
                                'post',
                                'get',
                                'isSuccess',
                                'patch'
                           ])
                           ->getMock();
                           
        $this->childAdapter = new class extends WbjTemplateRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIWbjTemplateAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('templates', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'WBJTEMPLATE_LIST',
                WbjTemplateRestfulAdapter::SCENARIOS['WBJTEMPLATE_LIST']
            ],
            [
                'WBJTEMPLATE_FETCH_ONE',
                WbjTemplateRestfulAdapter::SCENARIOS['WBJTEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $wbjTemplate = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate(1);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullWbjTemplate())
            ->willReturn($wbjTemplate);
        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($wbjTemplate, $result);
    }

    /**
     * 为WbjTemplateRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$wbjTemplate,$keys,$wbjTemplateArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareWbjTemplateTranslator(
        WbjTemplate $wbjTemplate,
        array $keys,
        array $wbjTemplateArray
    ) {
        $translator = $this->prophesize(WbjTemplateRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($wbjTemplate),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($wbjTemplateArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(WbjTemplate $wbjTemplate)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($wbjTemplate);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWbjTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $wbjTemplate = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate(1);
        $wbjTemplateArray = array();

        $this->prepareWbjTemplateTranslator(
            $wbjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit'
            ),
            $wbjTemplateArray
        );

        $this->adapter
            ->method('post')
            ->with('', $wbjTemplateArray);

        $this->success($wbjTemplate);
        $result = $this->adapter->add($wbjTemplate);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWbjTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $wbjTemplate = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate(1);
        $wbjTemplateArray = array();

        $this->prepareWbjTemplateTranslator(
            $wbjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit'
            ),
            $wbjTemplateArray
        );

        $this->adapter
            ->method('post')
            ->with('', $wbjTemplateArray);
        
            $this->failure($wbjTemplate);
            $result = $this->adapter->add($wbjTemplate);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $wbjTemplate = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate(1);
        $wbjTemplateArray = array();

        $this->prepareWbjTemplateTranslator(
            $wbjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit'
            ),
            $wbjTemplateArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$wbjTemplate->getId(), $wbjTemplateArray);

        $this->success($wbjTemplate);
        $result = $this->adapter->edit($wbjTemplate);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $wbjTemplate = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate(1);
        $wbjTemplateArray = array();

        $this->prepareWbjTemplateTranslator(
            $wbjTemplate,
            array(
                'id',
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit'
            ),
            $wbjTemplateArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$wbjTemplate->getId(), $wbjTemplateArray);
        
            $this->failure($wbjTemplate);
            $result = $this->adapter->edit($wbjTemplate);
            $this->assertFalse($result);
    }
}
