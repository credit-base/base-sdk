<?php
namespace Base\Sdk\Template\Utils\GbTemplate;

trait GbTemplateRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $gbTemplate
    ) {
        $this->assertEquals($expectedArray['data']['id'], $gbTemplate->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $gbTemplate->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $gbTemplate->getIdentify());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $gbTemplate->getDescription());
        $this->assertEquals($expectedArray['data']['attributes']['subjectCategory'], $gbTemplate->getSubjectCategory());
        $this->assertEquals($expectedArray['data']['attributes']['items'], $gbTemplate->getItems());
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $gbTemplate->getDimension());
        $this->assertEquals($expectedArray['data']['attributes']['exchangeFrequency'], $gbTemplate->getExchangeFrequency()); //phpcs:ignore
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $gbTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $gbTemplate->getInfoCategory());
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $gbTemplate->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $gbTemplate->getUpdateTime());
        }
    }
}
