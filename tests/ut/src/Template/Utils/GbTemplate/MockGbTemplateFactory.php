<?php
namespace Base\Sdk\Template\Utils\GbTemplate;

use Base\Sdk\Template\Model\GbTemplate;

class MockGbTemplateFactory
{
    public static function generateGbTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : GbTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $gbTemplate = new GbTemplate($id);
        $gbTemplate->setId($id);

        //name
        self::generateName($gbTemplate, $faker, $value);
        //identify
        self::generateIdentify($gbTemplate, $faker, $value);
        //description
        self::generateDescription($gbTemplate, $faker, $value);
        //subjectCategory
        self::generateSubjectCategory($gbTemplate, $faker, $value);
        //items
        self::generateTemplate($gbTemplate, $faker, $value);
        //dimension
        self::generateDimension($gbTemplate, $faker, $value);
        //exchangeFrequency
        self::generateExchangeFrequency($gbTemplate, $faker, $value);
        //infoClassify
        self::generateInfoClassify($gbTemplate, $faker, $value);
        //infoCategory
        self::generateInfoCategory($gbTemplate, $faker, $value);
        //ruleCount
        self::generateRuleCount($gbTemplate, $faker, $value);
        //dataTotal
        self::generateDataTotal($gbTemplate, $faker, $value);
        //category
        self::generateCategory($gbTemplate, $faker, $value);
        //userGroupCount
        self::generateUserGroupCount($gbTemplate, $faker, $value);

        $gbTemplate->setStatus(0);
        $gbTemplate->setCreateTime($faker->unixTime());
        $gbTemplate->setUpdateTime($faker->unixTime());
        $gbTemplate->setStatusTime($faker->unixTime());

        return $gbTemplate;
    }

    private static function generateName($gbTemplate, $faker, $value) : void
    {
        $name = isset($value['name']) ?
            $value['name'] :
            $faker->title;
        
        $gbTemplate->setName($name);
    }

    private static function generateIdentify($gbTemplate, $faker, $value) : void
    {
        $identify = isset($value['identify']) ?
            $value['identify'] :
            $faker->title;
        
        $gbTemplate->setIdentify($identify);
    }

    private static function generateDescription($gbTemplate, $faker, $value) : void
    {
        $description = isset($value['description']) ?
            $value['description'] :
            $faker->title;
        
        $gbTemplate->setDescription($description);
    }

    private static function generateRuleCount($gbTemplate, $faker, $value) : void
    {
        $ruleCount = isset($value['ruleCount']) ?
            $value['ruleCount'] :
            $faker->randomNumber;
        
        $gbTemplate->setRuleCount($ruleCount);
    }

    private static function generateDataTotal($gbTemplate, $faker, $value) : void
    {
        $dataTotal = isset($value['dataTotal']) ?
            $value['dataTotal'] :
            $faker->randomNumber;
        
        $gbTemplate->setDataTotal($dataTotal);
    }

    private static function generateCategory($gbTemplate, $faker, $value) : void
    {
        $category = isset($value['category']) ?
            $value['category'] :
            $faker->randomNumber;
        
        $gbTemplate->setCategory($category);
    }

    private static function generateUserGroupCount($gbTemplate, $faker, $value) : void
    {
        $userGroupCount = isset($value['userGroupCount']) ?
            $value['userGroupCount'] :
            $faker->randomNumber;
        
        $gbTemplate->setUserGroupCount($userGroupCount);
    }

    private static function generateSubjectCategory($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $subjectCategory = isset($value['subjectCategory']) ?
            $value['subjectCategory'] :
            array(1);
        
        $bjTemplate->setSubjectCategory($subjectCategory);
    }

    private static function generateTemplate($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $items = isset($value['items']) ?
            $value['items'] :
            array(
                array(
                    "name" => '主体名称',
                    "identify" => 'ZTMC',
                    "type" => 1,
                    "length" => '200',
                    "options" => array(),
                    "dimension" => 1,
                    "isNecessary" => 1,
                    "isMasked" => 1,
                    "maskRule" => array(),
                    "remarks" => '信用主体名称',
                )
            );
        
        $bjTemplate->setItems($items);
    }

    private static function generateDimension($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $dimension = isset($value['dimension']) ?
            $value['dimension'] :
            1;
        
        $bjTemplate->setDimension($dimension);
    }

    private static function generateExchangeFrequency($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $exchangeFrequency = isset($value['exchangeFrequency']) ?
            $value['exchangeFrequency'] :
            1;
        
        $bjTemplate->setExchangeFrequency($exchangeFrequency);
    }

    private static function generateInfoClassify($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $infoClassify = isset($value['infoClassify']) ?
            $value['infoClassify'] :
            1;
        
        $bjTemplate->setInfoClassify($infoClassify);
    }

    private static function generateInfoCategory($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $infoCategory = isset($value['infoCategory']) ?
            $value['infoCategory'] :
            1;
        
        $bjTemplate->setInfoCategory($infoCategory);
    }
}
