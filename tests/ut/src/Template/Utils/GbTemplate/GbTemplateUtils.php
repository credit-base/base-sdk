<?php
namespace Base\Sdk\Template\Utils\GbTemplate;

trait GbTemplateUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $gbTemplate
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($gbTemplate->getId()));
        $this->assertEquals($expectedArray['name'], $gbTemplate->getName());
        $this->assertEquals($expectedArray['identify'], $gbTemplate->getIdentify());
        $this->assertEquals($expectedArray['description'], $gbTemplate->getDescription());
        // $this->assertEquals($expectedArray['subjectCategory'], $gbTemplate->getSubjectCategory());
        // $this->assertEquals($expectedArray['items'], $gbTemplate->getItems());
        // $this->assertEquals($expectedArray['dimension']['id'], marmot_encode($gbTemplate->getDimension()));
        // $this->assertEquals($expectedArray['dimension']['name'], '');
        // $this->assertEquals(
        //     $expectedArray['exchangeFrequency']['id'],
        //     marmot_encode($gbTemplate->getExchangeFrequency())
        // );
        // $this->assertEquals($expectedArray['exchangeFrequency']['name'], '');
        // $this->assertEquals($expectedArray['infoClassify']['id'], marmot_encode($gbTemplate->getInfoClassify()));
        // $this->assertEquals($expectedArray['infoClassify']['name'], '');
        // $this->assertEquals($expectedArray['infoCategory']['id'], marmot_encode($gbTemplate->getInfoCategory()));
        // $this->assertEquals($expectedArray['infoCategory']['name'], '');
        $this->assertEquals($expectedArray['createTime'], $gbTemplate->getCreateTime());
        $this->assertEquals($expectedArray['updateTime'], $gbTemplate->getUpdateTime());
    }
}
