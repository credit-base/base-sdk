<?php
namespace Base\Sdk\Template\Utils;

use Base\Sdk\Template\Model\Template;

use Base\Sdk\UserGroup\Model\UserGroup;

class MockTemplateFactory
{
    public static function generateCommonTemplate(
        Template $template,
        int $seed = 0,
        array $value = array()
    ) {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        //name
        self::generateName($template, $faker, $value);
        //identify
        self::generateIdentify($template, $faker, $value);
        //description
        self::generateDescription($template, $faker, $value);
        //subjectCategory
        self::generateSubjectCategory($template, $faker, $value);
        //items
        self::generateItems($template, $faker, $value);
        //dimension
        self::generateDimension($template, $faker, $value);
        //exchangeFrequency
        self::generateExchangeFrequency($template, $faker, $value);
        //infoClassify
        self::generateInfoClassify($template, $faker, $value);
        //infoCategory
        self::generateInfoCategory($template, $faker, $value);
        //sourceUnit
        self::generateSourceUnit($template, $faker, $value);
         //ruleCount
         self::generateRuleCount($template, $faker, $value);
         //dataTotal
         self::generateDataTotal($template, $faker, $value);
         //category
         self::generateCategory($template, $faker, $value);
         //userGroupCount
         self::generateUserGroupCount($template, $faker, $value);
    
        $template->setStatus(0);
        $template->setCreateTime($faker->unixTime());
        $template->setUpdateTime($faker->unixTime());
        $template->setStatusTime($faker->unixTime());

        return $template;
    }

    private static function generateItems($template, $faker, $value) : void
    {
        unset($faker);
        $items = isset($value['items']) ?
            $value['items'] :
            array(
                array(
                    "name" => '主体名称',
                    "identify" => 'ZTMC',
                    "type" => 1,
                    "length" => '200',
                    "options" => array(),
                    "dimension" => 1,
                    "isNecessary" => 1,
                    "isMasked" => 1,
                    "maskRule" => array(),
                    "remarks" => '信用主体名称',
                )
            );
        
        $template->setItems($items);
    }

    private static function generateInfoCategory($template, $faker, $value) : void
    {
        unset($faker);
        $infoCategory = isset($value['infoCategory']) ?
            $value['infoCategory'] :
            1;
        
        $template->setInfoCategory($infoCategory);
    }

    private static function generateIdentify($template, $faker, $value) : void
    {
        $identify = isset($value['identify']) ?
            $value['identify'] :
            $faker->title;
        
        $template->setIdentify($identify);
    }

    private static function generateExchangeFrequency($template, $faker, $value) : void
    {
        unset($faker);
        $exchangeFrequency = isset($value['exchangeFrequency']) ?
            $value['exchangeFrequency'] :
            1;
        
        $template->setExchangeFrequency($exchangeFrequency);
    }

    private static function generateName($template, $faker, $value) : void
    {
        $name = isset($value['name']) ?
            $value['name'] :
            $faker->title;
        
        $template->setName($name);
    }

    private static function generateDimension($template, $faker, $value) : void
    {
        unset($faker);
        $dimension = isset($value['dimension']) ?
            $value['dimension'] :
            1;
        
        $template->setDimension($dimension);
    }

    private static function generateDescription($template, $faker, $value) : void
    {
        $description = isset($value['description']) ?
            $value['description'] :
            $faker->title;
        
        $template->setDescription($description);
    }

    private static function generateSourceUnit($template, $faker, $value) : void
    {
        unset($faker);

        $sourceUnit = isset($value['sourceUnit']) ?
            new UserGroup($value['sourceUnit']) :
            new UserGroup();
        $template->setSourceUnit($sourceUnit);
    }

    private static function generateSubjectCategory($template, $faker, $value) : void
    {
        unset($faker);
        $subjectCategory = isset($value['subjectCategory']) ?
            $value['subjectCategory'] :
            array(1);
        
        $template->setSubjectCategory($subjectCategory);
    }

    private static function generateRuleCount($template, $faker, $value) : void
    {
        $ruleCount = isset($value['ruleCount']) ?
            $value['ruleCount'] :
            $faker->randomNumber;
        
        $template->setRuleCount($ruleCount);
    }

    private static function generateDataTotal($template, $faker, $value) : void
    {
        $dataTotal = isset($value['dataTotal']) ?
            $value['dataTotal'] :
            $faker->randomNumber;
        
        $template->setDataTotal($dataTotal);
    }

    private static function generateCategory($template, $faker, $value) : void
    {
        unset($faker);
        $category = isset($value['category']) ?
            $value['category'] :
            21;
        
        $template->setCategory($category);
    }

    private static function generateUserGroupCount($template, $faker, $value) : void
    {
        $userGroupCount = isset($value['userGroupCount']) ?
            $value['userGroupCount'] :
            $faker->randomNumber;
        
        $template->setUserGroupCount($userGroupCount);
    }

    private static function generateInfoClassify($template, $faker, $value) : void
    {
        unset($faker);
        $infoClassify = isset($value['infoClassify']) ?
            $value['infoClassify'] :
            1;
        
        $template->setInfoClassify($infoClassify);
    }
}
