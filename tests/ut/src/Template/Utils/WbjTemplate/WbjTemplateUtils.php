<?php
namespace Base\Sdk\Template\Utils\WbjTemplate;

trait WbjTemplateUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $bjTemplate
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($bjTemplate->getId()));
        $this->assertEquals($expectedArray['name'], $bjTemplate->getName());
        $this->assertEquals($expectedArray['description'], $bjTemplate->getDescription());
        $this->assertEquals($expectedArray['identify'], $bjTemplate->getIdentify());
        // $this->assertEquals($expectedArray['subjectCategory'], $bjTemplate->getSubjectCategory());
        // $this->assertEquals($expectedArray['items'], $bjTemplate->getItems());
        // $this->assertEquals($expectedArray['dimension']['id'], marmot_encode($bjTemplate->getDimension()));
        // $this->assertEquals($expectedArray['dimension']['name'], '');
        // $this->assertEquals(
        //     $expectedArray['exchangeFrequency']['id'],
        //     marmot_encode($bjTemplate->getExchangeFrequency())
        // );
        // $this->assertEquals($expectedArray['exchangeFrequency']['name'], '');
        // $this->assertEquals($expectedArray['infoClassify']['id'], marmot_encode($bjTemplate->getInfoClassify()));
        // $this->assertEquals($expectedArray['infoClassify']['name'], '');
        // $this->assertEquals($expectedArray['infoCategory']['id'], marmot_encode($bjTemplate->getInfoCategory()));
        // $this->assertEquals($expectedArray['infoCategory']['name'], '');
        // $this->assertEquals(
        //     $expectedArray['sourceUnit']['id'],
        //     marmot_encode($bjTemplate->getSourceUnit()->getId())
        // );
        // $this->assertEquals($expectedArray['sourceUnit']['name'], $bjTemplate->getSourceUnit()->getName());
        $this->assertEquals($expectedArray['updateTime'], $bjTemplate->getUpdateTime());
        $this->assertEquals($expectedArray['createTime'], $bjTemplate->getCreateTime());
    }
}
