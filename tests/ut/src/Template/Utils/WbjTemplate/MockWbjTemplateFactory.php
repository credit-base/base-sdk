<?php
namespace Base\Sdk\Template\Utils\WbjTemplate;

use Base\Sdk\Template\Model\WbjTemplate;

use Base\Sdk\UserGroup\Model\UserGroup;

class MockWbjTemplateFactory
{
    public static function generateWbjTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WbjTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $wbjTemplate = new WbjTemplate($id);
        $wbjTemplate->setId($id);

        $wbjTemplate =
        \Base\Sdk\Template\Utils\MockTemplateFactory::generateCommonTemplate($wbjTemplate, $seed, $value);

        return $wbjTemplate;
    }
}
