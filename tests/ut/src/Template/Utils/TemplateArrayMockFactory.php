<?php
namespace Base\Sdk\Template\Utils;

class TemplateArrayMockFactory
{
    public static function generateTemplateTypeCn()
    {
        $data = [
            'id'=> 'MA',
            'name'=> '字符型',
        ];

        return $data;
    }

    public static function generateTemplateDimensionCn()
    {
        $data = [
            'id'=> 'MA',
            'name'=> '社会公开',
        ];

        return $data;
    }

    public static function generateTemplateItemCn()
    {
        $data = array(
            array(
            "name" => '主体名称',    //信息项名称
            "identify" => 'ZTMC',    //数据标识
            "type" => array(
                'id'=> 'MA',
                'name'=> '字符型',
            ),    //数据类型
            "length" => '200',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => array('id'=> 'MA',
            'name'=> '社会公开',),    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => array(
                'id'=> 'MA',
                'name'=> '是',
            ),    //是否必填，否 0 | 默认 是 1
            "isMasked" => array(
                'id'=> 'MA',
                'name'=> '是',
            ),    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则
            "remarks" => '信用主体名称',    //备注
        )
        );

        return $data;
    }

    public static function generateTemplateIsMaskedCn()
    {
        $data = [
            'id'=> 'MA',
            'name'=> '是',
        ];

        return $data;
    }

    public static function generateTemplateIsNecessaryCn()
    {
        $data = [
            'id'=> 'MA',
            'name'=> '是',
        ];

        return $data;
    }

    public static function generateTemplateSubjectCategoryCn()
    {
        $data = array(
            array(
                'id'=> 'MA',
                'name'=> '法人及非法人组织',
            )
        );

        return $data;
    }
}
