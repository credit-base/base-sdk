<?php
namespace Base\Sdk\Template\Utils\BjTemplate;

trait BjTemplateRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $bjTemplate
    ) {
        $this->assertEquals($expectedArray['data']['id'], $bjTemplate->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $bjTemplate->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $bjTemplate->getIdentify());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $bjTemplate->getDescription());
        $this->assertEquals($expectedArray['data']['attributes']['subjectCategory'], $bjTemplate->getSubjectCategory());
        $this->assertEquals($expectedArray['data']['attributes']['items'], $bjTemplate->getItems());
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $bjTemplate->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['exchangeFrequency'],
            $bjTemplate->getExchangeFrequency()
        );
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $bjTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $bjTemplate->getInfoCategory());

        if (isset($expectedArray['data']['attributes']['ruleCount'])) {
            $this->assertEquals($expectedArray['data']['attributes']['ruleCount'], $bjTemplate->getRuleCount());
        }
        if (isset($expectedArray['data']['attributes']['dataTotal'])) {
            $this->assertEquals($expectedArray['data']['attributes']['dataTotal'], $bjTemplate->getDataTotal());
        }
        if (isset($expectedArray['data']['attributes']['category'])) {
            $this->assertEquals($expectedArray['data']['attributes']['category'], $bjTemplate->getCategory());
        }
        if (isset($expectedArray['data']['attributes']['userGroupCount'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['userGroupCount'],
                $bjTemplate->getUserGroupCount()
            );
        }
       
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $bjTemplate->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $bjTemplate->getUpdateTime());
        }
    }
}
