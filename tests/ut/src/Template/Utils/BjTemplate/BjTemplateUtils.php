<?php
namespace Base\Sdk\Template\Utils\BjTemplate;

use Base\Sdk\Template\Model\BjTemplate;

use Base\Sdk\ResourceCatalog\Model\ISearchData;

use Base\Sdk\Template\Translator\QzjTemplateTranslator;

trait BjTemplateUtils
{
   /**
    * @SuppressWarnings(PHPMD)
    */
    private function compareArrayAndObject(
        array $expectedArray,
        $bjTemplate
    ) {
        $this->assertEquals($expectedArray['name'], $bjTemplate->getName());
        $this->assertEquals($expectedArray['identify'], $bjTemplate->getIdentify());
        $this->assertEquals(
            $expectedArray['dimension']['id'],
            marmot_encode($bjTemplate->getDimension())
        );
        $this->assertEquals(
            $expectedArray['dimension']['name'],
            BjTemplate::DIMENSION_CN[$bjTemplate->getDimension()]
        );
        $this->assertEquals(
            $expectedArray['dimension']['label'],
            ISearchData::DIMENSION_LABEL[$bjTemplate->getDimension()]
        );
        $this->assertEquals(
            $expectedArray['dimension']['type'],
            ISearchData::DIMENSION_TYPE[$bjTemplate->getDimension()]
        );
        $this->assertEquals($expectedArray['description'], $bjTemplate->getDescription());
        $this->assertEquals(
            $expectedArray['subjectCategory'],
            \Base\Sdk\Template\Utils\TemplateArrayMockFactory::generateTemplateSubjectCategoryCn()
        );
        $this->assertEquals(
            $expectedArray['items'],
            \Base\Sdk\Template\Utils\TemplateArrayMockFactory::generateTemplateItemCn()
        );
        
        $this->assertEquals(
            $expectedArray['exchangeFrequency']['id'],
            marmot_encode($bjTemplate->getExchangeFrequency())
        );
        $this->assertEquals(
            $expectedArray['exchangeFrequency']['name'],
            BjTemplate::EXCHANGE_FREQUENCY_CN[$bjTemplate->getExchangeFrequency()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['id'],
            marmot_encode($bjTemplate->getInfoClassify())
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['name'],
            BjTemplate::INFO_CLASSIFY_CN[$bjTemplate->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['label'],
            ISearchData::INFO_CLASSIFY_LABEL[$bjTemplate->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['type'],
            ISearchData::INFO_CLASSIFY_TYPE[$bjTemplate->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['id'],
            marmot_encode($bjTemplate->getInfoCategory())
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['name'],
            BjTemplate::INFO_CATEGORY_CN[$bjTemplate->getInfoCategory()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['label'],
            ISearchData::INFO_CATEGORY_LABEL[$bjTemplate->getInfoCategory()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['type'],
            ISearchData::INFO_CATEGORY_TYPE[$bjTemplate->getInfoCategory()]
        );
        if (isset($expectedArray['sourceUnit'])) {
            $this->assertEquals(
                $expectedArray['sourceUnit']['id'],
                marmot_encode($bjTemplate->getSourceUnit()->getId())
            );
            $this->assertEquals(
                $expectedArray['sourceUnit']['name'],
                $bjTemplate->getSourceUnit()->getName()
            );
        }
        
        if (isset($expectedArray['gbTemplate'])) {
            $this->assertEquals(
                $expectedArray['gbTemplate']['id'],
                marmot_encode($bjTemplate->getGbTemplate()->getId())
            );
            $this->assertEquals(
                $expectedArray['gbTemplate']['name'],
                $bjTemplate->getGbTemplate()->getName()
            );
        }

        if (isset($expectedArray['category'])) {
            $this->assertEquals(
                $expectedArray['category']['id'],
                marmot_encode($bjTemplate->getCategory())
            );
            $this->assertEquals(
                $expectedArray['category']['name'],
                QzjTemplateTranslator::CATEGORY_CN[$bjTemplate->getCategory()]
            );
        }
        
        $this->assertEquals(
            $expectedArray['ruleCount'],
            $bjTemplate->getRuleCount()
        );
        $this->assertEquals(
            $expectedArray['dataTotal'],
            $bjTemplate->getDataTotal()
        );
        if (isset($expectedArray['userGroupCount'])) {
            $this->assertEquals(
                $expectedArray['userGroupCount'],
                $bjTemplate->getUserGroupCount()
            );
        }
        
        $this->assertEquals($expectedArray['createTime'], $bjTemplate->getCreateTime());
        $this->assertEquals($expectedArray['updateTime'], $bjTemplate->getUpdateTime());
    }
}
