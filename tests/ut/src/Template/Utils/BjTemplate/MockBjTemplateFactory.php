<?php
namespace Base\Sdk\Template\Utils\BjTemplate;

use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\Template\Model\GbTemplate;

use Base\Sdk\UserGroup\Model\UserGroup;

class MockBjTemplateFactory
{
    public static function generateBjTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BjTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $bjTemplate = new BjTemplate($id);
        $bjTemplate->setId($id);

        //name
        self::generateName($bjTemplate, $faker, $value);
        //identify
        self::generateIdentify($bjTemplate, $faker, $value);
        //description
        self::generateDescription($bjTemplate, $faker, $value);
        //subjectCategory
        self::generateSubjectCategory($bjTemplate, $faker, $value);
        //items
        self::generateTemplate($bjTemplate, $faker, $value);
        //dimension
        self::generateDimension($bjTemplate, $faker, $value);
        //exchangeFrequency
        self::generateExchangeFrequency($bjTemplate, $faker, $value);
        //infoClassify
        self::generateInfoClassify($bjTemplate, $faker, $value);
        //infoCategory
        self::generateInfoCategory($bjTemplate, $faker, $value);
        //sourceUnit
        self::generateSourceUnit($bjTemplate, $faker, $value);
        //gbTemplate
        self::generateGbTemplate($bjTemplate, $faker, $value);
        //ruleCount
        self::generateRuleCount($bjTemplate, $faker, $value);
        //dataTotal
        self::generateDataTotal($bjTemplate, $faker, $value);
        //category
        self::generateCategory($bjTemplate, $faker, $value);
        //userGroupCount
        self::generateUserGroupCount($bjTemplate, $faker, $value);
    
        $bjTemplate->setStatus(0);
        $bjTemplate->setCreateTime($faker->unixTime());
        $bjTemplate->setUpdateTime($faker->unixTime());
        $bjTemplate->setStatusTime($faker->unixTime());

        return $bjTemplate;
    }

    private static function generateTemplate($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $items = isset($value['items']) ?
            $value['items'] :
            array(
                array(
                    "name" => '主体名称',
                    "identify" => 'ZTMC',
                    "type" => 1,
                    "length" => '200',
                    "options" => array(),
                    "dimension" => 1,
                    "isNecessary" => 1,
                    "isMasked" => 1,
                    "maskRule" => array(),
                    "remarks" => '信用主体名称',
                )
            );
        
        $bjTemplate->setItems($items);
    }

    private static function generateName($bjTemplate, $faker, $value) : void
    {
        $name = isset($value['name']) ?
            $value['name'] :
            $faker->title;
        
        $bjTemplate->setName($name);
    }

    private static function generateIdentify($bjTemplate, $faker, $value) : void
    {
        $identify = isset($value['identify']) ?
            $value['identify'] :
            $faker->title;
        
        $bjTemplate->setIdentify($identify);
    }

    private static function generateSubjectCategory($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $subjectCategory = isset($value['subjectCategory']) ?
            $value['subjectCategory'] :
            array(1);
        
        $bjTemplate->setSubjectCategory($subjectCategory);
    }

    private static function generateDescription($bjTemplate, $faker, $value) : void
    {
        $description = isset($value['description']) ?
            $value['description'] :
            $faker->title;
        
        $bjTemplate->setDescription($description);
    }

    private static function generateDimension($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $dimension = isset($value['dimension']) ?
            $value['dimension'] :
            1;
        
        $bjTemplate->setDimension($dimension);
    }

    private static function generateExchangeFrequency($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $exchangeFrequency = isset($value['exchangeFrequency']) ?
            $value['exchangeFrequency'] :
            1;
        
        $bjTemplate->setExchangeFrequency($exchangeFrequency);
    }

    private static function generateSourceUnit($bjTemplate, $faker, $value) : void
    {
        unset($faker);

        $sourceUnit = isset($value['sourceUnit']) ?
            new UserGroup($value['sourceUnit']) :
            new UserGroup();
        $bjTemplate->setSourceUnit($sourceUnit);
    }

    private static function generateInfoClassify($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $infoClassify = isset($value['infoClassify']) ?
            $value['infoClassify'] :
            1;
        
        $bjTemplate->setInfoClassify($infoClassify);
    }

    private static function generateInfoCategory($bjTemplate, $faker, $value) : void
    {
        unset($faker);
        $infoCategory = isset($value['infoCategory']) ?
            $value['infoCategory'] :
            1;
        
        $bjTemplate->setInfoCategory($infoCategory);
    }

    private static function generateGbTemplate($bjTemplate, $faker, $value) : void
    {
        unset($faker);

        $gbTemplate = isset($value['gbTemplate']) ?
            new GbTemplate($value['gbTemplate']) :
            new GbTemplate();
        $bjTemplate->setGbTemplate($gbTemplate);
    }

    private static function generateRuleCount($bjTemplate, $faker, $value) : void
    {
        $ruleCount = isset($value['ruleCount']) ?
            $value['ruleCount'] :
            $faker->randomNumber;
        
        $bjTemplate->setRuleCount($ruleCount);
    }

    private static function generateDataTotal($bjTemplate, $faker, $value) : void
    {
        $dataTotal = isset($value['dataTotal']) ?
            $value['dataTotal'] :
            $faker->randomNumber;
        
        $bjTemplate->setDataTotal($dataTotal);
    }

    private static function generateCategory($bjTemplate, $faker, $value) : void
    {
        $category = isset($value['category']) ?
            $value['category'] :
            $faker->randomNumber;
        
        $bjTemplate->setCategory($category);
    }

    private static function generateUserGroupCount($bjTemplate, $faker, $value) : void
    {
        $userGroupCount = isset($value['userGroupCount']) ?
            $value['userGroupCount'] :
            $faker->randomNumber;
        
        $bjTemplate->setUserGroupCount($userGroupCount);
    }
}
