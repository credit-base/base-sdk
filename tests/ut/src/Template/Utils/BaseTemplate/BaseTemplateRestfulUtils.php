<?php
namespace Base\Sdk\Template\Utils\BaseTemplate;

trait BaseTemplateRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $baseTemplate
    ) {
        $this->assertEquals($expectedArray['data']['id'], $baseTemplate->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $baseTemplate->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $baseTemplate->getIdentify());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $baseTemplate->getDescription());
        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $baseTemplate->getSubjectCategory()
        );
        $this->assertEquals($expectedArray['data']['attributes']['items'], $baseTemplate->getItems());
        $this->assertEquals($expectedArray['data']['attributes']['dimension'], $baseTemplate->getDimension());
        $this->assertEquals(
            $expectedArray['data']['attributes']['exchangeFrequency'],
            $baseTemplate->getExchangeFrequency()
        );
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $baseTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['data']['attributes']['infoCategory'], $baseTemplate->getInfoCategory());

        if (isset($expectedArray['data']['attributes']['ruleCount'])) {
            $this->assertEquals($expectedArray['data']['attributes']['ruleCount'], $baseTemplate->getRuleCount());
        }
        if (isset($expectedArray['data']['attributes']['dataTotal'])) {
            $this->assertEquals($expectedArray['data']['attributes']['dataTotal'], $baseTemplate->getDataTotal());
        }
        if (isset($expectedArray['data']['attributes']['category'])) {
            $this->assertEquals($expectedArray['data']['attributes']['category'], $baseTemplate->getCategory());
        }
        if (isset($expectedArray['data']['attributes']['userGroupCount'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['userGroupCount'],
                $baseTemplate->getUserGroupCount()
            );
        }
       
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $baseTemplate->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $baseTemplate->getUpdateTime());
        }
    }
}
