<?php
namespace Base\Sdk\Template\Utils\BaseTemplate;

use Base\Sdk\Template\Model\BaseTemplate;
use Base\Sdk\Template\Model\GbTemplate;

use Base\Sdk\UserGroup\Model\UserGroup;

class MockBaseTemplateFactory
{
    public static function generateBaseTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BaseTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $baseTemplate = new BaseTemplate($id);
        $baseTemplate->setId($id);

        //name
        self::generateName($baseTemplate, $faker, $value);
        //identify
        self::generateIdentify($baseTemplate, $faker, $value);
        //description
        self::generateDescription($baseTemplate, $faker, $value);
        //subjectCategory
        self::generateSubjectCategory($baseTemplate, $faker, $value);
        //items
        self::generateTemplate($baseTemplate, $faker, $value);
        //dimension
        self::generateDimension($baseTemplate, $faker, $value);
        //exchangeFrequency
        self::generateExchangeFrequency($baseTemplate, $faker, $value);
        //infoClassify
        self::generateInfoClassify($baseTemplate, $faker, $value);
        //infoCategory
        self::generateInfoCategory($baseTemplate, $faker, $value);
        //ruleCount
        self::generateRuleCount($baseTemplate, $faker, $value);
        //dataTotal
        self::generateDataTotal($baseTemplate, $faker, $value);
        //category
        self::generateCategory($baseTemplate, $faker, $value);
        //userGroupCount
        self::generateUserGroupCount($baseTemplate, $faker, $value);
    
        $baseTemplate->setStatus(0);
        $baseTemplate->setCreateTime($faker->unixTime());
        $baseTemplate->setUpdateTime($faker->unixTime());
        $baseTemplate->setStatusTime($faker->unixTime());

        return $baseTemplate;
    }

    private static function generateTemplate($baseTemplate, $faker, $value) : void
    {
        unset($faker);
        $items = isset($value['items']) ?
            $value['items'] :
            array(
                array(
                    "name" => '主体名称',
                    "identify" => 'ZTMC',
                    "type" => 1,
                    "length" => '200',
                    "options" => array(),
                    "dimension" => 1,
                    "isNecessary" => 1,
                    "isMasked" => 1,
                    "maskRule" => array(),
                    "remarks" => '信用主体名称',
                )
            );
        
        $baseTemplate->setItems($items);
    }

    private static function generateName($baseTemplate, $faker, $value) : void
    {
        $name = isset($value['name']) ?
            $value['name'] :
            $faker->title;
        
        $baseTemplate->setName($name);
    }

    private static function generateIdentify($baseTemplate, $faker, $value) : void
    {
        $identify = isset($value['identify']) ?
            $value['identify'] :
            $faker->title;
        
        $baseTemplate->setIdentify($identify);
    }

    private static function generateSubjectCategory($baseTemplate, $faker, $value) : void
    {
        unset($faker);
        $subjectCategory = isset($value['subjectCategory']) ?
            $value['subjectCategory'] :
            array(1);
        
        $baseTemplate->setSubjectCategory($subjectCategory);
    }

    private static function generateDescription($baseTemplate, $faker, $value) : void
    {
        $description = isset($value['description']) ?
            $value['description'] :
            $faker->title;
        
        $baseTemplate->setDescription($description);
    }

    private static function generateDimension($baseTemplate, $faker, $value) : void
    {
        unset($faker);
        $dimension = isset($value['dimension']) ?
            $value['dimension'] :
            1;
        
        $baseTemplate->setDimension($dimension);
    }

    private static function generateExchangeFrequency($baseTemplate, $faker, $value) : void
    {
        unset($faker);
        $exchangeFrequency = isset($value['exchangeFrequency']) ?
            $value['exchangeFrequency'] :
            1;
        
        $baseTemplate->setExchangeFrequency($exchangeFrequency);
    }


    private static function generateInfoClassify($baseTemplate, $faker, $value) : void
    {
        unset($faker);
        $infoClassify = isset($value['infoClassify']) ?
            $value['infoClassify'] :
            1;
        
        $baseTemplate->setInfoClassify($infoClassify);
    }

    private static function generateInfoCategory($baseTemplate, $faker, $value) : void
    {
        unset($faker);
        $infoCategory = isset($value['infoCategory']) ?
            $value['infoCategory'] :
            1;
        
        $baseTemplate->setInfoCategory($infoCategory);
    }

    private static function generateRuleCount($baseTemplate, $faker, $value) : void
    {
        $ruleCount = isset($value['ruleCount']) ?
            $value['ruleCount'] :
            $faker->randomNumber;
        
        $baseTemplate->setRuleCount($ruleCount);
    }

    private static function generateDataTotal($baseTemplate, $faker, $value) : void
    {
        $dataTotal = isset($value['dataTotal']) ?
            $value['dataTotal'] :
            $faker->randomNumber;
        
        $baseTemplate->setDataTotal($dataTotal);
    }

    private static function generateCategory($baseTemplate, $faker, $value) : void
    {
        $category = isset($value['category']) ?
            $value['category'] :
            $faker->randomNumber;
        
        $baseTemplate->setCategory($category);
    }

    private static function generateUserGroupCount($baseTemplate, $faker, $value) : void
    {
        $userGroupCount = isset($value['userGroupCount']) ?
            $value['userGroupCount'] :
            $faker->randomNumber;
        
        $baseTemplate->setUserGroupCount($userGroupCount);
    }
}
