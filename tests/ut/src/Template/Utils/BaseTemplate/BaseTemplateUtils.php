<?php
namespace Base\Sdk\Template\Utils\BaseTemplate;

use Base\Sdk\Template\Model\BaseTemplate;

use Base\Sdk\ResourceCatalog\Model\ISearchData;

trait BaseTemplateUtils
{
   /**
    * @SuppressWarnings(PHPMD)
    */
    private function compareArrayAndObject(
        array $expectedArray,
        $baseTemplate
    ) {
        $this->assertEquals($expectedArray['name'], $baseTemplate->getName());
        $this->assertEquals($expectedArray['identify'], $baseTemplate->getIdentify());
        $this->assertEquals(
            $expectedArray['dimension']['id'],
            marmot_encode($baseTemplate->getDimension())
        );
        $this->assertEquals(
            $expectedArray['dimension']['name'],
            BaseTemplate::DIMENSION_CN[$baseTemplate->getDimension()]
        );
        $this->assertEquals(
            $expectedArray['dimension']['label'],
            ISearchData::DIMENSION_LABEL[$baseTemplate->getDimension()]
        );
        $this->assertEquals(
            $expectedArray['dimension']['type'],
            ISearchData::DIMENSION_TYPE[$baseTemplate->getDimension()]
        );
        $this->assertEquals($expectedArray['description'], $baseTemplate->getDescription());
        $this->assertEquals(
            $expectedArray['subjectCategory'],
            \Base\Sdk\Template\Utils\TemplateArrayMockFactory::generateTemplateSubjectCategoryCn()
        );
        $this->assertEquals(
            $expectedArray['items'],
            \Base\Sdk\Template\Utils\TemplateArrayMockFactory::generateTemplateItemCn()
        );
        
        $this->assertEquals(
            $expectedArray['exchangeFrequency']['id'],
            marmot_encode($baseTemplate->getExchangeFrequency())
        );
        $this->assertEquals(
            $expectedArray['exchangeFrequency']['name'],
            BaseTemplate::EXCHANGE_FREQUENCY_CN[$baseTemplate->getExchangeFrequency()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['id'],
            marmot_encode($baseTemplate->getInfoClassify())
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['name'],
            BaseTemplate::INFO_CLASSIFY_CN[$baseTemplate->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['label'],
            ISearchData::INFO_CLASSIFY_LABEL[$baseTemplate->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['type'],
            ISearchData::INFO_CLASSIFY_TYPE[$baseTemplate->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['id'],
            marmot_encode($baseTemplate->getInfoCategory())
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['name'],
            BaseTemplate::INFO_CATEGORY_CN[$baseTemplate->getInfoCategory()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['label'],
            ISearchData::INFO_CATEGORY_LABEL[$baseTemplate->getInfoCategory()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['type'],
            ISearchData::INFO_CATEGORY_TYPE[$baseTemplate->getInfoCategory()]
        );

        $this->assertEquals(
            $expectedArray['ruleCount'],
            $baseTemplate->getRuleCount()
        );
        $this->assertEquals(
            $expectedArray['dataTotal'],
            $baseTemplate->getDataTotal()
        );
        if (isset($expectedArray['userGroupCount'])) {
            $this->assertEquals(
                $expectedArray['userGroupCount'],
                $baseTemplate->getUserGroupCount()
            );
        }
        $this->assertEquals(
            $expectedArray['category'],
            marmot_encode($baseTemplate->getCategory())
        );
        $this->assertEquals($expectedArray['createTime'], $baseTemplate->getCreateTime());
        $this->assertEquals($expectedArray['updateTime'], $baseTemplate->getUpdateTime());
    }
}
