<?php
namespace Base\Sdk\Template\Utils\QzjTemplate;

use Base\Sdk\Template\Model\QzjTemplate;

class MockQzjTemplateFactory
{
    public static function generateQzjTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : QzjTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $qzjTemplate = new QzjTemplate($id);
        $qzjTemplate->setId($id);

        $qzjTemplate =
        \Base\Sdk\Template\Utils\MockTemplateFactory::generateCommonTemplate($qzjTemplate, $seed, $value);

        return $qzjTemplate;
    }
}
