<?php
namespace Base\Sdk\Template\CommandHandler\WbjTemplate;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

class WbjTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        $this->commandHandler = new WbjTemplateCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Basecode\Classes\NullCommandHandler', $commandHandler);
    }
}
