<?php
namespace Base\Sdk\Template\CommandHandler\GbTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\Sdk\Template\Command\GbTemplate\AddGbTemplateCommand;
use Base\Sdk\Template\Model\GbTemplate;

class AddGbTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddGbTemplateCommandHandler::class)
            ->setMethods(['getGbTemplate'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetGbTemplate()
    {
        $commandHandler = new MockAddGbTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Template\Model\GbTemplate',
            $commandHandler->getGbTemplate()
        );
    }

    public function testExecute()
    {
        $command = new AddGbTemplateCommand(
            $this->faker->name(),
            $this->faker->name(),
            $this->faker->name(),
            array(),
            array(),
            1,
            1,
            1,
            1
        );

        $gbTemplate = $this->prophesize(GbTemplate::class);
        $gbTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $gbTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $gbTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $gbTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $gbTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $gbTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $gbTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $gbTemplate->setItems(Argument::exact($command->items))->shouldBeCalledTimes(1);
        $gbTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $gbTemplate->add()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->any())
            ->method('getGbTemplate')
            ->willReturn($gbTemplate->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
