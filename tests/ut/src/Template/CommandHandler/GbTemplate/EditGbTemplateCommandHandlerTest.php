<?php
namespace Base\Sdk\Template\CommandHandler\GbTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\Repository\GbTemplateRepository;
use Base\Sdk\Template\Command\GbTemplate\EditGbTemplateCommand;

class EditGbTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditGbTemplateCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockEditGbTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\GbTemplateRepository',
            $commandHandler->getRepository()
        );
    }

    public function testExecute()
    {
        $command = new EditGbTemplateCommand(
            $this->faker->name(),
            $this->faker->name(),
            $this->faker->name(),
            array(),
            array(),
            2,
            2,
            2,
            2,
            2
        );

        $gbTemplate = $this->prophesize(GbTemplate::class);
        $gbTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $gbTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $gbTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $gbTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $gbTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $gbTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $gbTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $gbTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $gbTemplate->setItems(Argument::exact($command->items))->shouldBeCalledTimes(1);
        $gbTemplate->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $gbTemplateRepository = $this->prophesize(GbTemplateRepository::class);
        $gbTemplateRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($gbTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($gbTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = new EditGbTemplateCommand(
            $this->faker->name(),
            $this->faker->name(),
            $this->faker->name(),
            array(),
            array(),
            $this->faker->randomNumber(),
            $this->faker->randomNumber(),
            $this->faker->randomNumber(),
            $this->faker->randomNumber(),
            $this->faker->randomNumber()
        );

        $gbTemplate = $this->prophesize(GbTemplate::class);
        $gbTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $gbTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $gbTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $gbTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $gbTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $gbTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $gbTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $gbTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $gbTemplate->setItems(Argument::exact($command->items))->shouldBeCalledTimes(1);
        $gbTemplate->edit()->shouldBeCalledTimes(1)->willReturn(false);

        $gbTemplateRepository = $this->prophesize(GbTemplateRepository::class);
        $gbTemplateRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($gbTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($gbTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }
}
