<?php
namespace Base\Sdk\Template\CommandHandler\QzjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\Sdk\Template\Model\QzjTemplate;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Template\Command\QzjTemplate\AddQzjTemplateCommand;

class AddQzjTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddQzjTemplateCommandHandler::class)
            ->setMethods(['getQzjTemplate','getUserGroupRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetQzjTemplate()
    {
        $commandHandler = new MockAddQzjTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Template\Model\QzjTemplate',
            $commandHandler->getQzjTemplate()
        );
    }

    public function testGetUserGroupRepository()
    {
        $commandHandler = new MockAddQzjTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Repository\UserGroupRepository',
            $commandHandler->getUserGroupRepository()
        );
    }

    public function initialExecute()
    {
        $name = 'name';
        $identify = 'identify';
        $description = 'description';
        $subjectCategory = [];
        $items = [];
        $sourceUnit = 1;
        $dimension = 1;
        $exchangeFrequency = 1;
        $infoClassify = 1;
        $infoCategory = 1;
        $category = 20;
        
        $command = new AddQzjTemplateCommand(
            $name,
            $identify,
            $description,
            $subjectCategory,
            $items,
            $sourceUnit,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $category
        );
       
        $userGroup = new UserGroup();
        $userGroupId = $sourceUnit;
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))
            ->shouldBeCalledTimes(1)->willReturn($userGroup);
        $this->commandHandler->expects($this->exactly(1))
            ->method('getUserGroupRepository')
            ->willReturn($userGroupRepository->reveal());

        $qzjTemplate = $this->prophesize(QzjTemplate::class);
        $qzjTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $qzjTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $qzjTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $qzjTemplate->setItems(Argument::exact($command->items))->shouldBeCalledTimes(1);
        $qzjTemplate->setSourceUnit(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        $qzjTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $qzjTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $qzjTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $qzjTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $qzjTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $qzjTemplate->setCategory(Argument::exact($command->category))->shouldBeCalledTimes(1);
        $qzjTemplate->add()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->any())
            ->method('getQzjTemplate')
            ->willReturn($qzjTemplate->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }
}
