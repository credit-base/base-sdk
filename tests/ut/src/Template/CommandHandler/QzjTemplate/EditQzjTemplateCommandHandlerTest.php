<?php
namespace Base\Sdk\Template\CommandHandler\QzjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Template\Model\QzjTemplate;
use Base\Sdk\Template\Repository\QzjTemplateRepository;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Template\Command\QzjTemplate\EditQzjTemplateCommand;

class EditQzjTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditQzjTemplateCommandHandler::class)
            ->setMethods(['getRepository','getUserGroupRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockEditQzjTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\QzjTemplateRepository',
            $commandHandler->getRepository()
        );
    }

    public function testGetUserGroupRepository()
    {
        $commandHandler = new MockEditQzjTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Repository\UserGroupRepository',
            $commandHandler->getUserGroupRepository()
        );
    }

    public function testExecute()
    {
        $name = 'name';
        $identify = 'identify';
        $description = 'description';
        $subjectCategory = [];
        $items = [];
        $sourceUnit = 1;
        $dimension = 1;
        $exchangeFrequency = 1;
        $infoClassify = 1;
        $infoCategory = 1;
        $id = 1;
        $category = 21;

        $command = new EditQzjTemplateCommand(
            $name,
            $identify,
            $description,
            $subjectCategory,
            $items,
            $sourceUnit,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $category,
            $id
        );

        $userGroup = \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
       
        $userGroupId = 1;
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))
            ->shouldBeCalledTimes(1)->willReturn($userGroup);
        $this->commandHandler->expects($this->exactly(1))
            ->method('getUserGroupRepository')
            ->willReturn($userGroupRepository->reveal());

        $qzjTemplate = $this->prophesize(QzjTemplate::class);
        
        $qzjTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $qzjTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $qzjTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $qzjTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $qzjTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $qzjTemplate->setCategory(Argument::exact($command->category))->shouldBeCalledTimes(1);
        $qzjTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $qzjTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $qzjTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $qzjTemplate->setItems(Argument::exact($command->items))->shouldBeCalledTimes(1);
        $qzjTemplate->setSourceUnit(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        $qzjTemplate->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $qzjTemplateRepository = $this->prophesize(QzjTemplateRepository::class);
        $qzjTemplateRepository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($qzjTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($qzjTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
