<?php
namespace Base\Sdk\Template\CommandHandler\BaseTemplate;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

class BaseTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;
    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        $this->commandHandler = new BaseTemplateCommandHandlerFactory();
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->commandHandler);
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Basecode\Classes\NullCommandHandler', $commandHandler);
    }
}
