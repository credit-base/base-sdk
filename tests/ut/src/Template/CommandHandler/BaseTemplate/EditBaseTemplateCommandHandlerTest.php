<?php
namespace Base\Sdk\Template\CommandHandler\BaseTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Template\Repository\GbTemplateRepository;
use Base\Sdk\Template\Model\BaseTemplate;
use Base\Sdk\Template\Repository\BaseTemplateRepository;
use Base\Sdk\Template\Command\BaseTemplate\EditBaseTemplateCommand;

class EditBaseTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditBaseTemplateCommandHandler::class)
            ->setMethods(['getRepository','getUserGroupRepository','getGbTemplateRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockEditBaseTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\BaseTemplateRepository',
            $commandHandler->getRepository()
        );
    }

    public function testExecute()
    {
        $name = 'name';
        $identify = 'identify';
        $description = 'description';
        $subjectCategory = [];
        $items = [];
        $dimension = 1;
        $exchangeFrequency = 1;
        $infoClassify = 1;
        $infoCategory = 1;
        $id = 1;

        $command = new EditBaseTemplateCommand(
            $name,
            $identify,
            $description,
            $subjectCategory,
            $items,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $id
        );

        $baseTemplate = $this->prophesize(BaseTemplate::class);
        $baseTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $baseTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $baseTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $baseTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $baseTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $baseTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $baseTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $baseTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $baseTemplate->setItems(Argument::exact($command->items))->shouldBeCalledTimes(1);
        $baseTemplate->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $baseTemplateRepository = $this->prophesize(BaseTemplateRepository::class);
        $baseTemplateRepository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($baseTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($baseTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
