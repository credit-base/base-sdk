<?php
namespace Base\Sdk\Template\CommandHandler\BjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Template\Repository\GbTemplateRepository;
use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\Template\Repository\BjTemplateRepository;
use Base\Sdk\Template\Command\BjTemplate\EditBjTemplateCommand;

class EditBjTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditBjTemplateCommandHandler::class)
            ->setMethods(['getRepository','getUserGroupRepository','getGbTemplateRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockEditBjTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\BjTemplateRepository',
            $commandHandler->getRepository()
        );
    }

    public function testGetUserGroupRepository()
    {
        $commandHandler = new MockEditBjTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Repository\UserGroupRepository',
            $commandHandler->getUserGroupRepository()
        );
    }

    public function testGbTemplateRepository()
    {
        $commandHandler = new MockEditBjTemplateCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\GbTemplateRepository',
            $commandHandler->getGbTemplateRepository()
        );
    }

    public function testExecute()
    {
        $name = 'name';
        $identify = 'identify';
        $description = 'description';
        $subjectCategory = [];
        $items = [];
        $sourceUnit = 1;
        $dimension = 1;
        $exchangeFrequency = 1;
        $infoClassify = 1;
        $infoCategory = 1;
        $gbTemplateId = 1;
        $id = 1;

        $command = new EditBjTemplateCommand(
            $name,
            $identify,
            $description,
            $subjectCategory,
            $items,
            $sourceUnit,
            $gbTemplateId,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $id
        );

        $userGroup = \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
       
        $userGroupId = 1;
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))
            ->shouldBeCalledTimes(1)->willReturn($userGroup);
        $this->commandHandler->expects($this->exactly(1))
            ->method('getUserGroupRepository')
            ->willReturn($userGroupRepository->reveal());

        $gbTemplate = new GbTemplate();
        $gbTemplateId = $gbTemplateId;
        $gbTemplateRepository = $this->prophesize(GbTemplateRepository::class);
        $gbTemplateRepository->fetchOne(Argument::exact($gbTemplateId))
            ->shouldBeCalledTimes(1)->willReturn($gbTemplate);
        $this->commandHandler->expects($this->exactly(1))
                    ->method('getGbTemplateRepository')
                    ->willReturn($gbTemplateRepository->reveal());

        $wbjTemplate = $this->prophesize(BjTemplate::class);
        $wbjTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $wbjTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $wbjTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $wbjTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $wbjTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $wbjTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $wbjTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $wbjTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $wbjTemplate->setItems(Argument::exact($command->items))->shouldBeCalledTimes(1);
        $wbjTemplate->setSourceUnit(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        $wbjTemplate->setGbTemplate(Argument::exact($gbTemplate))->shouldBeCalledTimes(1);
        $wbjTemplate->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $wbjTemplateRepository = $this->prophesize(BjTemplateRepository::class);
        $wbjTemplateRepository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($wbjTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($wbjTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
