<?php
namespace Base\Sdk\Template\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Adapter\QzjTemplate\QzjTemplateRestfulAdapter;

class QzjTemplateRepositoryTest extends TestCase
{
    private $repository;
    
    private $childRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(QzjTemplateRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->childRepository = new class extends QzjTemplateRepository
        {
            public function getAdapter()
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->childRepository);
    }

    public function testImplementsIQzjTemplateAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Adapter\QzjTemplate\IQzjTemplateAdapter',
            $this->repository
        );
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Adapter\QzjTemplate\QzjTemplateRestfulAdapter',
            $this->childRepository->getAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(QzjTemplateRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
