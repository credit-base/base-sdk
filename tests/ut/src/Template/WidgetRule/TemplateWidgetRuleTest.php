<?php
namespace Base\Sdk\Template\WidgetRule;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Sdk\Common\Utils\StringGenerate;

use Base\Sdk\Template\Model\Template;

/**
 * @SuppressWarnings(PHPMD)
 */
class TemplateWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = TemplateWidgetRule::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //name -- start
    /**
     * @dataProvider invalidNameProvider
     */
    public function testNameInvalid($actual, $expected)
    {
        $result = $this->widgetRule->name($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TEMPLATE_NAME_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidNameProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(TemplateWidgetRule::NAME_MIN_LENGTH-1), false),
            array(StringGenerate::generate(TemplateWidgetRule::NAME_MAX_LENGTH+1), false),
            array(StringGenerate::generate(TemplateWidgetRule::NAME_MIN_LENGTH), true),
            array(StringGenerate::generate(TemplateWidgetRule::NAME_MIN_LENGTH+1), true),
            array(StringGenerate::generate(TemplateWidgetRule::NAME_MAX_LENGTH), true),
            array(StringGenerate::generate(TemplateWidgetRule::NAME_MAX_LENGTH-1), true)
        );
    }
    //name -- end

    //identify -- start
    /**
     * @dataProvider invalidIdentifyProvider
     */
    public function testIdentifyInvalid($actual, $expected)
    {
        $result = $this->widgetRule->identify($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(IDENTIFY_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidIdentifyProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array('', false),
            array($faker->regexify('[A-Z][A-Z_]{101,200}[A-Z]'), false),
            array($faker->regexify('[_][A-Z_]{1,100}'), false),
            array($faker->regexify('[A-Z_]{-100,0}'), false),
            array($faker->regexify('[A-Z][A-Z_]{0,98}[A-Z]$'), true),
        );
    }
    //identify -- end

    //subjectCategory -- start
    /**
     * @dataProvider invalidSubjectCategoryProvider
     */
    public function testSubjectCategoryInvalid($actual, $expected)
    {
        $result = $this->widgetRule->subjectCategory($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(SUBJECT_CATEGORY_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidSubjectCategoryProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array(array(), false),
            array($faker->word, false),
            array($faker->randomElement(Template::SUBJECT_CATEGORY, 1), false),
            array($faker->randomElements(array(7,8,9), 1), false),
            array($faker->randomElements(Template::SUBJECT_CATEGORY, 1), true),
            array($faker->randomElements(Template::SUBJECT_CATEGORY, 2), true),
            array($faker->randomElements(Template::SUBJECT_CATEGORY, 3), true)
        );
    }
    //subjectCategory -- end

    //dimension -- start
    /**
     * @dataProvider invalidDimensionProvider
     */
    public function testDimensionInvalid($actual, $expected)
    {
        $result = $this->widgetRule->dimension($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DIMENSION_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidDimensionProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array('', false),
            array($faker->word, false),
            array($faker->regexify('[4-9]{1}'), false),
            array($faker->randomElements(Template::DIMENSION, 1), false),
            array($faker->randomElement(Template::DIMENSION, 1), true),
        );
    }
    //dimension -- end

    //formatNumeric -- start
    /**
     * @dataProvider invalidFormatNumericProvider
     */
    public function testFormatNumericInvalid($actual, $expected)
    {
        $result = $this->widgetRule->formatNumeric($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidFormatNumericProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array($faker->word, false),
            array(array($faker->md5), false),
            array($faker->randomDigit, true)
        );
    }
    //formatNumeric -- end

    //description -- start
    /**
     * @dataProvider invalidDescriptionProvider
     */
    public function testDescriptionInvalid($actual, $expected)
    {
        $result = $this->widgetRule->description($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DESCRIPTION_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidDescriptionProvider()
    {
        return array(
            array(StringGenerate::generate(TemplateWidgetRule::DESCRIPTION_MAX_LENGTH+1), false),
            array(StringGenerate::generate(TemplateWidgetRule::DESCRIPTION_MIN_LENGTH), true),
            array(StringGenerate::generate(TemplateWidgetRule::DESCRIPTION_MIN_LENGTH+1), true),
            array(StringGenerate::generate(TemplateWidgetRule::DESCRIPTION_MAX_LENGTH), true),
            array(StringGenerate::generate(TemplateWidgetRule::DESCRIPTION_MAX_LENGTH-1), true),
            array('', true)
        );
    }
    //description -- end

    //items -- start
    /**
     * @dataProvider invalidItemsProvider
     */
    public function testItemsInvalid($subjectCategory, $actual, $expected)
    {
        $result = $this->widgetRule->items($subjectCategory, $actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        // $this->assertEquals(ITEMS_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidItemsProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            // array(
            //     array(),
            //     array(
            //         array(
            //             "name" => '主体名称',    //信息项名称
            //             "identify" => 'ZTMC',    //数据标识
            //             "type" => 1,    //数据类型
            //             "length" => '200',    //数据长度
            //             "options" => array(),    //可选范围
            //             "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            //             "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
            //             "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
            //             "maskRule" => array(),    //脱敏规则
            //             "remarks" => '信用主体名称',    //备注
            //         )
            //     ),
            //     false
            // ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '统一社会信用代码',    //备注
                    ),
                    array(
                        "name" => '认定日期',    //信息项名称
                        "identify" => 'GKFW',    //数据标识
                        "type" => 5,    //数据类型
                        "length" => '20',    //数据长度
                        "options" => array(
                            "社会公开",
                            "政务共享",
                            "授权查询"
                        ),    //可选范围
                        "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则，即左边保留1位字符，右边保留2位字符
                        "remarks" => '',    //备注
                    )
                ),
                false
            ),
            array(Template::SUBJECT_CATEGORY, array(), false),
            array(Template::SUBJECT_CATEGORY, $faker->word, false),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => '',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 0,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 6,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 6,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 2,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(1, 2),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 2,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => StringGenerate::generate(TemplateWidgetRule::REMARKS_MAX_LENGTH+1),    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '身份证号',    //信息项名称
                        "identify" => 'ZJHM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'NAME',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '认定依据',    //信息项名称
                        "identify" => 'RDYJ',    //数据标识
                        "type" => 3,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(
                            '认定依据'
                        ),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '认定依据',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '认定依据',    //信息项名称
                        "identify" => 'RDYJ',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(1, 2),    //脱敏规则
                        "remarks" => '认定依据',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY['FRJFFRZZ'],
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '认定机关',    //信息项名称
                        "identify" => 'RDJG',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '',    //备注
                    ),
                    array(
                        "name" => '认定机关统一社会信用代码',    //信息项名称
                        "identify" => 'RDJG',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '',    //备注
                    ),
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '',    //备注
                    ),
                    array(
                        "name" => '公开范围',    //信息项名称
                        "identify" => 'GKFW',    //数据标识
                        "type" => 5,    //数据类型
                        "length" => '20',    //数据长度
                        "options" => array(
                            "社会公开",
                            "政务共享",
                            "授权查询"
                        ),    //可选范围
                        "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则，即左边保留1位字符，右边保留2位字符
                        "remarks" => '',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '',    //备注
                    ),
                    array(
                        "name" => '公开范围',    //信息项名称
                        "identify" => 'GKFW',    //数据标识
                        "type" => 5,    //数据类型
                        "length" => '20',    //数据长度
                        "options" => array(
                            "社会公开",
                            "政务共享"
                        ),    //可选范围
                        "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则，即左边保留1位字符，右边保留2位字符
                        "remarks" => '支持单选',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '信用主体代码',    //备注
                    )
                ),
                false
            ),
            array(
                Template::SUBJECT_CATEGORY,
                array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '信用主体代码',    //备注
                    ),
                    array(
                        "name" => '身份证号',    //信息项名称
                        "identify" => 'ZJHM',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '18',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '信用主体代码',    //备注
                    ),
                    array(
                        "name" => '认定日期',    //信息项名称
                        "identify" => 'RDRQ',    //数据标识
                        "type" => 2,    //数据类型
                        "length" => '',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                        "remarks" => '认定日期',    //备注
                    )
                ),
                true
            )
        );
    }
    //items -- end

    //category -- start
    /**
     * @dataProvider invalidCategoryProvider
     */
    public function testCategoryInvalid($actual, $expected)
    {
        $result = $this->widgetRule->category($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CATEGORY_NOT_EXIST, Core::getLastError()->getId());
    }

    public function invalidCategoryProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $errorCategory = [1,2,3];
        
        return array(
            array($faker->randomElement($errorCategory), false),
            array($faker->randomElement(Template::CATEGORY), true)
        );
    }
    //category -- end

    //infoCategory -- start
    /**
     * @dataProvider invalidInfoCategoryProvider
     */
    public function testInfoCategoryInvalid($actual, $expected)
    {
        $result = $this->widgetRule->infoCategory($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(INFO_CATEGORY_NOT_EXIST, Core::getLastError()->getId());
    }

    public function invalidInfoCategoryProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $errorInfoCategory = [-1,-2,-3,10,11];
        
        return array(
            array($faker->randomElement($errorInfoCategory), false),
            array($faker->randomElement(Template::INFO_CATEGORY), true)
        );
    }
    //infoCategory -- end
}
