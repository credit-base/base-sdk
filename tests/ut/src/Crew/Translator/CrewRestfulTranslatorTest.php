<?php
namespace Base\Sdk\Crew\Translator;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\Crew\Utils\CrewRestfulUtils;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Model\Guest;
use Base\Sdk\UserGroup\Model\Department;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\UserGroup\Translator\DepartmentRestfulTranslator;

class CrewRestfulTranslatorTest extends TestCase
{
    use CrewRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new CrewRestfulTranslator();
        $this->childTranslator = new class extends CrewRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
            public function getDepartmentRestfulTranslator() : DepartmentRestfulTranslator
            {
                return parent::getDepartmentRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testExtendsUserRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\User\Translator\UserRestfulTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetDepartmentRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\DepartmentRestfulTranslator',
            $this->childTranslator->getDepartmentRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $crew = \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);

        $expression['data']['id'] = $crew->getId();
        $expression['data']['attributes']['purview'] = $crew->getPurview();
        $expression['data']['attributes']['category'] = $crew->getCategory();

        $crewObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Crew\Model\Crew', $crewObject);
        $this->compareArrayAndObject($expression, $crewObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $crew = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Crew\Model\Guest', $crew);
    }

    public function testObjectToArray()
    {
        $crew = \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);

        $expression = $this->translator->objectToArray($crew);

        $this->compareArrayAndObject($expression, $crew);
    }

    public function testObjectToArrayFail()
    {
        $crew = null;

        $expression = $this->translator->objectToArray($crew);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['department']['data'] 和
     * $relationships['userGroup']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用两次, 分别用于处理 usergroup 和 department 且返回各自的数组 $departmentInfo 和 $userGroupInfo
     * 4. $this->getDepartmentRestfulTranslator()->arrayToObject 被调用一次, 入参是 $departmentInfo, 出参是 $department
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $userGroup
     Info, 出参是 $userGroup
     * 6. $crew->setDepartment 调用一次, 入参 $department
     * 7. $crew->setUserGroup 调用一次, 入参 $userGroup
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'department'=>['data'=>'mockDepartment'],
            'userGroup'=>['data'=>'mockUserGroup']
        ];

        $departmentInfo = ['mockDepartmentInfo'];
        $userGroupInfo = ['mockUserGroupInfo'];

        $department = new Department();
        $userGroup = new UserGroup();

        $translator = $this->getMockBuilder(CrewRestfulTranslator::class)
                           ->setMethods([
                                'relationship',
                                'changeArrayFormat',
                                'getDepartmentRestfulTranslator',
                                'getUserGroupRestfulTranslator',
                            ])
                           ->getMock();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用2次, 依次入参 department, userGroup
        //依次返回 $departmentInfo 和 $userGroupInfo
        $translator->expects($this->exactly(2))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['department']['data']],
                 [$relationships['userGroup']['data']]
             )
            ->will($this->onConsecutiveCalls($departmentInfo, $userGroupInfo));
  
        $departmentRestfulTranslator = $this->prophesize(DepartmentRestfulTranslator::class);
        $departmentRestfulTranslator->arrayToObject(Argument::exact($departmentInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($department);

        $userGrouptRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGrouptRestfulTranslator->arrayToObject(Argument::exact($userGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroup);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getDepartmentRestfulTranslator')
            ->willReturn($departmentRestfulTranslator->reveal());

        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGrouptRestfulTranslator->reveal());

        //揭示
        $crew = $translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Crew\Model\Crew', $crew);
        $this->assertEquals($department, $crew->getDepartment());
        $this->assertEquals($userGroup, $crew->getUserGroup());
    }
}
