<?php
namespace Base\Sdk\Crew\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Crew\Utils\CrewUtils;
use Base\Sdk\Crew\Model\Guest;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;

class CrewTranslatorTest extends TestCase
{
    use CrewUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new CrewTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testExtendsUserTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\User\Translator\UserTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Crew\Model\Guest', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $crew = \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);

        $expression = $this->translator->objectToArray($crew);

        $this->compareArrayAndObject($expression, $crew);
    }

    public function testObjectToArrayFail()
    {
        $crew = null;

        $expression = $this->translator->objectToArray($crew);
        $this->assertEquals(array(), $expression);
    }
}
