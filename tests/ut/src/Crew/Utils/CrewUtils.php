<?php
namespace Base\Sdk\Crew\Utils;

use Base\Sdk\Crew\Translator\CrewTranslator;

trait CrewUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $crew
    ) {
        $this->assertEquals($expectedArray['userGroup']['id'], marmot_encode($crew->getUserGroup()->getId()));
        $this->assertEquals($expectedArray['userGroup']['name'], $crew->getUserGroup()->getName());

        $this->assertEquals($expectedArray['category']['id'], marmot_encode($crew->getCategory()));
        $this->assertEquals($expectedArray['category']['name'], CrewTranslator::CATEGORY[$crew->getCategory()]);

        $this->assertEquals($expectedArray['department']['id'], marmot_encode($crew->getDepartment()->getId()));
        $this->assertEquals($expectedArray['department']['name'], $crew->getDepartment()->getName());

        $this->assertEquals($expectedArray['purview'], $crew->getPurview());
    }
}
