<?php
namespace Base\Sdk\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class SignOutCrewCommandTest extends TestCase
{
    private $command;
    
    public function setUp()
    {
        $this->command = new SignOutCrewCommand();
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
