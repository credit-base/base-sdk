<?php
namespace Base\Sdk\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class EditCrewCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'realName' => $faker->name(),
            'cardId' => $faker->name(),
            'userGroupId' => $faker->randomNumber(),
            'category' => $faker->randomNumber(),
            'departmentId' => $faker->randomNumber(),
            'purview' => array($faker->randomNumber()),
            'id' => $faker->randomNumber()
        );

        $this->command = new EditCrewCommand(
            $this->fakerData['realName'],
            $this->fakerData['cardId'],
            $this->fakerData['id'],
            $this->fakerData['userGroupId'],
            $this->fakerData['category'],
            $this->fakerData['departmentId'],
            $this->fakerData['purview']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
