<?php
namespace Base\Sdk\Crew\CommandHandler\Crew;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Crew\Repository\CrewRepository;

class DisableCrewCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockDisableCrewCommandHandler();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Repository\CrewRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchIEnableObject()
    {
        $commandHandler = $this->getMockBuilder(MockDisableCrewCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $crew = \Base\Sdk\Crew\Utils\MockFactory::generateCrew($id);

        $repository = $this->prophesize(CrewRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($crew);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIEnableObject($id);

        $this->assertEquals($result, $crew);
    }
}
