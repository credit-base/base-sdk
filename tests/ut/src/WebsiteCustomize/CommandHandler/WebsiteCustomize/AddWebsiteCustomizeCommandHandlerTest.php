<?php
namespace Base\Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand;
use Base\Sdk\WebsiteCustomize\Command\CommonFackerDataTrait;

class AddWebsiteCustomizeCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddWebsiteCustomizeCommandHandler::class)
            ->setMethods(['getWebsiteCustomize', 'getWebsiteCustomizeRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetWebsiteCustomize()
    {
        $commandHandler = new MockAddWebsiteCustomizeCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize',
            $commandHandler->getWebsiteCustomize()
        );
    }

    public function initialExecute()
    {
        $fakerData = array(
            'category'=>1,
            'content'=> array(1,2),
            'status' => 0
        );
        
        $command = new AddWebsiteCustomizeCommand(
            $fakerData['category'],
            $fakerData['status'],
            $fakerData['content']
        );

        $websiteCustomize = $this->prophesize(WebsiteCustomize::class);

        $websiteCustomize->setCategory(Argument::exact($command->category))->shouldBeCalledTimes(1);
        $websiteCustomize->setStatus(Argument::exact($command->status))->shouldBeCalledTimes(1);
        $websiteCustomize->setContent(Argument::exact($command->content))->shouldBeCalledTimes(1);
        $websiteCustomize->add()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->any())
            ->method('getWebsiteCustomize')
            ->willReturn($websiteCustomize->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }
}
