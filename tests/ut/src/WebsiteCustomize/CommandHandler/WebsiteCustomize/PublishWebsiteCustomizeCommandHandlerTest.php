<?php
namespace Base\Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Base\Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Base\Sdk\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand;

class PublishWebsiteCustomizeCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(PublishWebsiteCustomizeCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockPublishWebsiteCustomizeCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository',
            $commandHandler->getRepository()
        );
    }

    public function testExecute()
    {
        $id = 1;
        
        $command = new PublishWebsiteCustomizeCommand($id);

        $websiteCustomize = $this->prophesize(WebsiteCustomize::class);
        
        $websiteCustomize->publish()->shouldBeCalledTimes(1)->willReturn(true);

        $websiteCustomizeRepository = $this->prophesize(WebsiteCustomizeRepository::class);
        $websiteCustomizeRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($websiteCustomize->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($websiteCustomizeRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
