<?php
namespace Base\Sdk\WebsiteCustomize\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize\WebsiteCustomizeRestfulAdapter;

class WebsiteCustomizeRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(WebsiteCustomizeRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIWebsiteCustomizeAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize\WebsiteCustomizeRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize\WebsiteCustomizeRestfulAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(WebsiteCustomizeRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testPublish()
    {
        $websiteCustomize = \Base\Sdk\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize(1);

        $adapter = $this->prophesize(WebsiteCustomizeRestfulAdapter::class);
        $adapter->publish(Argument::exact($websiteCustomize))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->publish($websiteCustomize);
    }

    public function testCustomize()
    {
        $category = 'homePage';

        $adapter = $this->prophesize(WebsiteCustomizeRestfulAdapter::class);
        $adapter->customize(Argument::exact($category))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->customize($category);
    }
}
