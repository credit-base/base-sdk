<?php
namespace Base\Sdk\WebsiteCustomize\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

/**
 * @SuppressWarnings(PHPMD)
 */
class WebsiteCustomizeTest extends TestCase
{
    private $websiteCustomize;

    public function setUp()
    {
        $this->websiteCustomize = new MockWebsiteCustomize();
    }

    public function tearDown()
    {
        unset($this->websiteCustomize);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->websiteCustomize->getId());
        $this->assertEquals('', $this->websiteCustomize->getVersion());
        $this->assertEquals(0, $this->websiteCustomize->getCategory());
        $this->assertEquals(array(), $this->websiteCustomize->getContent());
        $this->assertEquals(WebsiteCustomize::STATUS['UNPUBLISHED'], $this->websiteCustomize->getStatus());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->websiteCustomize->getCrew());
        $this->assertInstanceOf(
            'Base\Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository',
            $this->websiteCustomize->getRepository()
        );
    }

    //version 测试 -------------------------------------------------------- start
    /**
     * 设置 WebsiteCustomize setVersion() 正确的传参类型,期望传值正确
     */
    public function testSetVersionCorrectType()
    {
        $this->websiteCustomize->setVersion('version');
        $this->assertEquals('version', $this->websiteCustomize->getVersion());
    }

    /**
     * 设置 WebsiteCustomize setVersion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVersionWrongType()
    {
        $this->websiteCustomize->setVersion(['version']);
    }
    //version 测试 --------------------------------------------------------   end

    //content 测试 -------------------------------------------------------- start
    /**
     * 设置 WebsiteCustomize setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->websiteCustomize->setContent(['content']);
        $this->assertEquals(['content'], $this->websiteCustomize->getContent());
    }

    /**
     * 设置 WebsiteCustomize setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->websiteCustomize->setContent('string');
    }
    //content 测试 --------------------------------------------------------   end
    
    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 WebsiteCustomize setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->websiteCustomize->setStatus($actual);
        $this->assertEquals($expected, $this->websiteCustomize->getStatus());
    }

    /**
     * 循环测试 WebsiteCustomize setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(WebsiteCustomize::STATUS['UNPUBLISHED'], WebsiteCustomize::STATUS['UNPUBLISHED']),
            array(WebsiteCustomize::STATUS['PUBLISHED'], WebsiteCustomize::STATUS['PUBLISHED']),
        );
    }

    /**
     * 设置 WebsiteCustomize setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->websiteCustomize->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

     //category 测试 ------------------------------------------------------ start
    /**
     * 循环测试 WebsiteCustomize setCategory() 是否符合预定范围
     *
     * @dataProvider categoryProvider
     */
    public function testSetCategory($actual, $expected)
    {
        $this->websiteCustomize->setCategory($actual);
        $this->assertEquals($expected, $this->websiteCustomize->getCategory());
    }

    /**
     * 循环测试 WebsiteCustomize setCategory() 数据构建器
     */
    public function categoryProvider()
    {
        return array(
            array(WebsiteCustomize::CATEGORY['HOME_PAGE'], WebsiteCustomize::CATEGORY['HOME_PAGE']),
        );
    }

    /**
     * 设置 WebsiteCustomize setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->websiteCustomize->setCategory('string');
    }
    //category 测试 ------------------------------------------------------   end

    //Crew 测试 -------------------------------------------------------- start
    /**
     * 设置 WebsiteCustomize setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->websiteCustomize->setCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->websiteCustomize->getCrew());
    }

    /**
     * 设置 WebsiteCustomize setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->websiteCustomize->setCrew('Crew');
    }
    //Crew 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository',
            $this->websiteCustomize->getRepository()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\Adapter\IOperateAbleAdapter',
            $this->websiteCustomize->getIOperateAbleAdapter()
        );
    }

    public function testPublish()
    {
        $this->stub = $this->getMockBuilder(MockWebsiteCustomize::class)
            ->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(WebsiteCustomizeRepository::class);
        $repository->publish(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->publish();
        $this->assertTrue($result);
    }
}
