<?php
namespace Base\Sdk\WebsiteCustomize\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullWebsiteCustomizeTest extends TestCase
{
    private $guest;

    public function setUp()
    {
        $this->guest = NullWebsiteCustomize::getInstance();
    }

    public function tearDown()
    {
        unset($this->guest);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsWebsiteCustomize()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize',
            $this->guest
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->guest
        );
    }

    public function testResourceNotExist()
    {
        $guest = new MockNullWebsiteCustomize();

        $result = $guest->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testPublish()
    {
        $nullWebsiteCustomize = new MockNullWebsiteCustomize();

        $result = $nullWebsiteCustomize->publish();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
