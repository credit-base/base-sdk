<?php
namespace Base\Sdk\WebsiteCustomize\Translator;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\WebsiteCustomize\Utils\WebsiteCustomizeRestfulUtils;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;
use Base\Sdk\Crew\Model\Crew;

class WebsiteCustomizeRestfulTranslatorTest extends TestCase
{
    use WebsiteCustomizeRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WebsiteCustomizeRestfulTranslator();
        $this->childTranslator = new class extends WebsiteCustomizeRestfulTranslator
        {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $websiteCustomize = \Base\Sdk\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize(1);

        $expression['data']['id'] = $websiteCustomize->getId();
        $expression['data']['attributes']['category'] = $websiteCustomize->getCategory();
        $expression['data']['attributes']['version'] = $websiteCustomize->getVersion();

        $expression['data']['attributes']['content'] = $websiteCustomize->getContent();
        $expression['data']['attributes']['status'] = $websiteCustomize->getStatus();

        $expression['data']['attributes']['createTime'] = $websiteCustomize->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $websiteCustomize->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $websiteCustomize->getUpdateTime();

        $websiteCustomizeObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize', $websiteCustomizeObject);
        $this->compareArrayAndObject($expression, $websiteCustomizeObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $websiteCustomize = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\WebsiteCustomize\Model\NullWebsiteCustomize', $websiteCustomize);
    }

    public function testObjectToArray()
    {
        $websiteCustomize = \Base\Sdk\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize(1);

        $expression = $this->translator->objectToArray($websiteCustomize);
        
        $this->compareArrayAndObject($expression, $websiteCustomize);
    }

    public function testObjectToArrayFail()
    {
        $websiteCustomize = null;

        $expression = $this->translator->objectToArray($websiteCustomize);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['crew']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用1次, 用于处理 crew 且返回数组 $crew
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用一次, 入参是 $crewInfo, 出参是 $crew
     * 5. $WebsiteCustomize->setCrew 调用一次, 入参 $crew
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
        ];

        $crewInfo = ['mockCrewInfo'];

        $crew = new Crew();

        $translator = $this->getMockBuilder(WebsiteCustomizeRestfulTranslator::class)
                           ->setMethods([
                                'relationship',
                                'changeArrayFormat',
                                'getCrewRestfulTranslator',
                            ])
                           ->getMock();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用1次, 入参 crew
        //返回 $crewInfo
        $translator->expects($this->exactly(1))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']]
             )
            ->will($this->onConsecutiveCalls($crewInfo));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        //揭示
        $websiteCustomize = $translator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize', $websiteCustomize);
        $this->assertEquals($crew, $websiteCustomize->getCrew());
    }
}
