<?php
namespace Base\Sdk\WebsiteCustomize\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\WebsiteCustomize\Utils\WebsiteCustomizeUtils;

class WebsiteCustomizeTranslatorTest extends TestCase
{
    use WebsiteCustomizeUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WebsiteCustomizeTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\WebsiteCustomize\Model\NullWebsiteCustomize', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $websiteCustomize = \Base\Sdk\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize(1);

        $expression = $this->translator->objectToArray($websiteCustomize);
        
        $this->compareArrayAndObject($expression, $websiteCustomize);
    }

    public function testObjectToArrayFail()
    {
        $websiteCustomize = null;

        $expression = $this->translator->objectToArray($websiteCustomize);
        $this->assertEquals(array(), $expression);
    }
}
