<?php
namespace Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Base\Sdk\WebsiteCustomize\Model\NullWebsiteCustomize;
use Base\Sdk\WebsiteCustomize\Translator\WebsiteCustomizeRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD)
 */
class WebsiteCustomizeRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(WebsiteCustomizeRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors',
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends WebsiteCustomizeRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getWebsiteCustomizeMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIWebsiteCustomizeAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize\IWebsiteCustomizeAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('websiteCustomizes', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'WEBSITE_CUSTOMIZE_LIST',
                WebsiteCustomizeRestfulAdapter::SCENARIOS['WEBSITE_CUSTOMIZE_LIST']
            ],
            [
                'WEBSITE_CUSTOMIZE_FETCH_ONE',
                WebsiteCustomizeRestfulAdapter::SCENARIOS['WEBSITE_CUSTOMIZE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    public function testFetchOne()
    {
        $id = 1;

        $websiteCustomize = \Base\Sdk\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullWebsiteCustomize::getInstance())
            ->willReturn($websiteCustomize);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($websiteCustomize, $result);
    }

    /**
     * 为WebsiteCustomizeRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$WebsiteCustomize,$keys,$WebsiteCustomizeArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareWebsiteCustomizeTranslator(
        WebsiteCustomize $websiteCustomize,
        array $keys,
        array $websiteCustomizeArray
    ) {
        $translator = $this->prophesize(WebsiteCustomizeRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($websiteCustomize),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($websiteCustomizeArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(WebsiteCustomize $websiteCustomize)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($websiteCustomize);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWebsiteCustomizeTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $websiteCustomize = \Base\Sdk\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize(1);
        $websiteCustomizeArray = array();

        $this->prepareWebsiteCustomizeTranslator(
            $websiteCustomize,
            array(
                'category',
                'status',
                'content',
                'crew'
            ),
            $websiteCustomizeArray
        );

        $this->adapter
            ->method('post')
            ->with('', $websiteCustomizeArray);

        $this->success($websiteCustomize);
        $result = $this->adapter->add($websiteCustomize);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWebsiteCustomizeTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $websiteCustomize = \Base\Sdk\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize(1);
        $websiteCustomizeArray = array();

        $this->prepareWebsiteCustomizeTranslator(
            $websiteCustomize,
            array(
                'category',
                'status',
                'content',
                'crew'
            ),
            $websiteCustomizeArray
        );

        $this->adapter
            ->method('post')
            ->with('', $websiteCustomizeArray);
        
            $this->failure($websiteCustomize);
            $result = $this->adapter->add($websiteCustomize);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWebsiteCustomizeTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行publish（）
     * 判断 result 是否为true
     */
    public function testPublishSuccess()
    {
        $websiteCustomize = \Base\Sdk\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize(1);

        $this->adapter
            ->method('patch')
            ->with('/'.$websiteCustomize->getId().'/'.'publish');

        $this->success($websiteCustomize);
        $result = $this->adapter->publish($websiteCustomize);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWebsiteCustomizeTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行publish（）
     * 判断 result 是否为false
     */
    public function testPublishFailure()
    {
        $websiteCustomize = \Base\Sdk\WebsiteCustomize\Utils\MockFactory::generateWebsiteCustomize(1);

        $this->adapter
            ->method('patch')
            ->with('/'.$websiteCustomize->getId().'/'.'publish');
        
            $this->failure($websiteCustomize);
            $result = $this->adapter->publish($websiteCustomize);
            $this->assertFalse($result);
    }

    public function testCustomizeSuccess()
    {
        $category = 'homePage';
        $resources = 'websiteCustomizes';

        $trait = $this->getMockBuilder(WebsiteCustomizeRestfulAdapter::class)
                      ->setMethods(['getResource', 'get', 'isSuccess', 'translateToObject'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(true);

        $trait->expects($this->exactly(1))
             ->method('translateToObject')
             ->willReturn(true);
             
        $this->assertTrue($trait->customize($category));
    }

    public function testCustomizeFail()
    {
        $category = 'homePage';
        $resources = 'websiteCustomizes';
        $object = new NullWebsiteCustomize();

        $trait = $this->getMockBuilder(WebsiteCustomizeRestfulAdapter::class)
                      ->setMethods(['getResource', 'get', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
        
        $result = $trait->customize($category);
        $this->assertEquals($object, $result);
    }

    public function testEditAction()
    {
        $websiteCustomize = new NullWebsiteCustomize();

        $result = $this->adapter->edit($websiteCustomize);
        $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $mapError = [
            102=>[
                'status'=>STATUS_CAN_NOT_MODIFY
            ],
            101 => [
                'memorialStatus'=>MEMORIAL_STATUS_FORMAT_ERROR,
                'theme' => THEME_FORMAT_ERROR,
                'theme-color' => THEME_COLOR_FORMAT_ERROR,
                'theme-image' => THEME_IMAGE_FORMAT_ERROR,
                'headerBarLeft' => HEADER_BAR_LEFT_FORMAT_ERROR,
                'headerBarLeft-name' => HEADER_BAR_LEFT_NAME_FORMAT_ERROR,
                'content'=>CONTENT_FORMAT_ERROR,
                'content-keys' => CONTENT_KEYS_FORMAT_ERROR,
                'content-requiredKeys' => CONTENT_REQUIRED_KEYS_FORMAT_ERROR,
                'headerBarLeft-status' => HEADER_BAR_LEFT_STATUS_FORMAT_ERROR,
                'headerBarLeft-type' => HEADER_BAR_LEFT_TYPE_FORMAT_ERROR,
                'headerBarLeft-url' => HEADER_BAR_LEFT_URL_FORMAT_ERROR,
                'headerBarRight' => HEADER_BAR_RIGHT_FORMAT_ERROR,
                'nav' => NAV_FORMAT_ERROR,
                'nav-count' => NAV_COUNT_FORMAT_ERROR,
                'nav-name' => NAV_NAME_FORMAT_ERROR,
                'nav-status' => NAV_STATUS_FORMAT_ERROR,
                'nav-url' => NAV_URL_FORMAT_ERROR,
                'nav-image' => NAV_IMAGE_FORMAT_ERROR,
                'headerBarRight-name' => HEADER_BAR_RIGHT_NAME_FORMAT_ERROR,
                'headerBarRight-status' => HEADER_BAR_RIGHT_STATUS_FORMAT_ERROR,
                'headerBarRight-type' => HEADER_BAR_RIGHT_TYPE_FORMAT_ERROR,
                'headerBarRight-url' => HEADER_BAR_RIGHT_URL_FORMAT_ERROR,
                'headerBg' => HEADER_BG_FORMAT_ERROR,
                'logo' => LOGO_FORMAT_ERROR,
                'headerSearch' => HEADER_SEARCH_FORMAT_ERROR,
                'headerSearch-status' => HEADER_SEARCH_STATUS_FORMAT_ERROR,
                'centerDialog' => CENTER_DIALOG_FORMAT_ERROR,
                'centerDialog-status' => CENTER_DIALOG_STATUS_FORMAT_ERROR,
                'centerDialog-url' => CENTER_DIALOG_URL_FORMAT_ERROR,
                'centerDialog-image' => CENTER_DIALOG_IMAGE_FORMAT_ERROR,
                'animateWindow' => ANIMATE_WINDOW_FORMAT_ERROR,
                'animateWindow-count' => ANIMATE_WINDOW_COUNT_FORMAT_ERROR,
                'animateWindow-status' => ANIMATE_WINDOW_STATUS_FORMAT_ERROR,
                'frameWindow' => FRAME_WINDOW_FORMAT_ERROR,
                'frameWindow-status' => FRAME_WINDOW_STATUS_FORMAT_ERROR,
                'frameWindow-url' => FRAME_WINDOW_URL_FORMAT_ERROR,
                'rightToolBar' => RIGHT_TOOL_BAR_FORMAT_ERROR,
                'rightToolBar-count' => RIGHT_TOOL_BAR_COUNT_FORMAT_ERROR,
                'rightToolBar-status' => RIGHT_TOOL_BAR_STATUS_FORMAT_ERROR,
                'rightToolBar-name' => RIGHT_TOOL_BAR_NAME_FORMAT_ERROR,
                'animateWindow-image' => ANIMATE_WINDOW_IMAGE_FORMAT_ERROR,
                'animateWindow-url' => ANIMATE_WINDOW_URL_FORMAT_ERROR,
                'leftFloatCard' => LEFT_FLOAT_CARD_FORMAT_ERROR,
                'leftFloatCard-count' => LEFT_FLOAT_CARD_COUNT_FORMAT_ERROR,
                'leftFloatCard-status' => LEFT_FLOAT_CARD_STATUS_FORMAT_ERROR,
                'leftFloatCard-image' => LEFT_FLOAT_CARD_IMAGE_FORMAT_ERROR,
                'leftFloatCard-url' => LEFT_FLOAT_CARD_URL_FORMAT_ERROR,
                'rightToolBar-category' => RIGHT_TOOL_BAR_CATEGORY_FORMAT_ERROR,
                'rightToolBar-images' => RIGHT_TOOL_BAR_IMAGES_FORMAT_ERROR,
                'rightToolBar-images-count' => RIGHT_TOOL_BAR_IMAGES_COUNT_FORMAT_ERROR,
                'rightToolBar-images-title' => RIGHT_TOOL_BAR_IMAGES_TITLE_FORMAT_ERROR,
                'rightToolBar-images-image' => RIGHT_TOOL_BAR_IMAGES_IMAGE_FORMAT_ERROR,
                'rightToolBar-url' => RIGHT_TOOL_BAR_URL_FORMAT_ERROR,
                'relatedLinks' => RELATED_LINKS_FORMAT_ERROR,
                'relatedLinks-count' => RELATED_LINKS_COUNT_FORMAT_ERROR,
                'relatedLinks-status' => RELATED_LINKS_STATUS_FORMAT_ERROR,
                'relatedLinks-name' => RELATED_LINKS_NAME_FORMAT_ERROR,
                'relatedLinks-items' => RELATED_LINKS_ITEMS_FORMAT_ERROR,
                'relatedLinks-items-status' => RELATED_LINKS_ITEMS_STATUS_FORMAT_ERROR,
                'relatedLinks-items-name' => RELATED_LINKS_ITEMS_NAME_FORMAT_ERROR,
                'relatedLinks-items-url' => RELATED_LINKS_ITEMS_URL_FORMAT_ERROR,
                'rightToolBar-description' => RIGHT_TOOL_BAR_DESCRIPTION_FORMAT_ERROR,
                'specialColumn' => SPECIAL_COLUMN_FORMAT_ERROR,
                'specialColumn-count' => SPECIAL_COLUMN_COUNT_FORMAT_ERROR,
                'specialColumn-status' => SPECIAL_COLUMN_STATUS_FORMAT_ERROR,
                'specialColumn-image' => SPECIAL_COLUMN_IMAGE_FORMAT_ERROR,
                'specialColumn-url' => SPECIAL_COLUMN_URL_FORMAT_ERROR,
                'footerBanner' => FOOTER_BANNER_FORMAT_ERROR,
                'footerBanner-count' => FOOTER_BANNER_COUNT_FORMAT_ERROR,
                'footerBanner-status' => FOOTER_BANNER_STATUS_FORMAT_ERROR,
                'footerBanner-image' => FOOTER_BANNER_IMAGE_FORMAT_ERROR,
                'footerBanner-url' => FOOTER_BANNER_URL_FORMAT_ERROR,
                'organizationGroup' => ORGANIZATION_GROUP_FORMAT_ERROR,
                'organizationGroup-count' => ORGANIZATION_GROUP_COUNT_FORMAT_ERROR,
                'organizationGroup-status' => ORGANIZATION_GROUP_STATUS_FORMAT_ERROR,
                'organizationGroup-image' => ORGANIZATION_GROUP_IMAGE_FORMAT_ERROR,
                'organizationGroup-url' => ORGANIZATION_GROUP_URL_FORMAT_ERROR,
                'silhouette' => SILHOUETTE_FORMAT_ERROR,
                'silhouette-status' => SILHOUETTE_STATUS_FORMAT_ERROR,
                'silhouette-image' => SILHOUETTE_IMAGE_FORMAT_ERROR,
                'silhouette-description' => SILHOUETTE_DESCRIPTION_FORMAT_ERROR,
                'footerThree' => FOOTER_THREE_FORMAT_ERROR,
                'footerThree-status' => FOOTER_THREE_STATUS_FORMAT_ERROR,
                'footerThree-name' => FOOTER_THREE_NAME_FORMAT_ERROR,
                'footerThree-url' => FOOTER_THREE_URL_FORMAT_ERROR,
                'footerThree-description' => FOOTER_THREE_DESCRIPTION_FORMAT_ERROR,
                'footerThree-type' => FOOTER_THREE_TYPE_FORMAT_ERROR,
                'footerThree-image' => FOOTER_THREE_IMAGE_FORMAT_ERROR,
                'partyAndGovernmentOrgans' => PARTY_AND_GOVERNMENT_ORGANS_FORMAT_ERROR,
                'partyAndGovernmentOrgans-status' => PARTY_AND_GOVERNMENT_ORGANS_STATUS_FORMAT_ERROR,
                'partyAndGovernmentOrgans-image' => PARTY_AND_GOVERNMENT_ORGANS_IMAGE_FORMAT_ERROR,
                'partyAndGovernmentOrgans-url' => PARTY_AND_GOVERNMENT_ORGANS_URL_FORMAT_ERROR,
                'governmentErrorCorrection' => GOVERNMENT_ERROR_CORRECTION_FORMAT_ERROR,
                'governmentErrorCorrection-status' => GOVERNMENT_ERROR_CORRECTION_STATUS_FORMAT_ERROR,
                'governmentErrorCorrection-image' => GOVERNMENT_ERROR_CORRECTION_IMAGE_FORMAT_ERROR,
                'governmentErrorCorrection-url' => GOVERNMENT_ERROR_CORRECTION_URL_FORMAT_ERROR,
                'footerNav' => FOOTER_NAV_FORMAT_ERROR,
                'footerNav-status' => FOOTER_NAV_STATUS_FORMAT_ERROR,
                'footerNav-name' => FOOTER_NAV_NAME_FORMAT_ERROR,
                'footerNav-url' => FOOTER_NAV_URL_FORMAT_ERROR,
                'footerTwo' => FOOTER_TWO_FORMAT_ERROR,
                'footerTwo-status' => FOOTER_TWO_STATUS_FORMAT_ERROR,
                'footerTwo-name' => FOOTER_TWO_NAME_FORMAT_ERROR,
                'footerTwo-url' => FOOTER_TWO_URL_FORMAT_ERROR,
                'footerTwo-description' => FOOTER_TWO_DESCRIPTION_FORMAT_ERROR,
                'crewId'=>CREW_ID_FORMAT_ERROR,
                'category'=>CATEGORY_FORMAT_ERROR,
                'status'=>STATUS_FORMAT_ERROR,
            ],
            100 => [
                'crewId' => 'CREW_IS_EMPTY',
                'websiteCustomizeId' => 'WEBSITE_CUSTOMIZE_NOT_UNIQUER'
            ],
        ];

        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);
        
        $result = $this->childAdapter->getWebsiteCustomizeMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
