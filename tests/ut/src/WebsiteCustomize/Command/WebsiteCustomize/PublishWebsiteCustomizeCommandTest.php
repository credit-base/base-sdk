<?php
namespace Base\Sdk\WebsiteCustomize\Command\WebsiteCustomize;

use PHPUnit\Framework\TestCase;

class PublishWebsiteCustomizeCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new PublishWebsiteCustomizeCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
