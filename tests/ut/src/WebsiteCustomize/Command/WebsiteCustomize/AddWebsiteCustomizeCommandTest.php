<?php
namespace Base\Sdk\WebsiteCustomize\Command\WebsiteCustomize;

use PHPUnit\Framework\TestCase;

class AddWebsiteCustomizeCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = array(
            'category'=>1,
            'content'=> array(1,2),
            'status' => 0
        );

        $this->command = new AddWebsiteCustomizeCommand(
            $this->fakerData['category'],
            $this->fakerData['status'],
            $this->fakerData['content']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
