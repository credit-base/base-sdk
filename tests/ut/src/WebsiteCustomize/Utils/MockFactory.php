<?php
namespace Base\Sdk\WebsiteCustomize\Utils;

use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;

use Base\Sdk\Common\Model\IApplyAble;

class MockFactory
{
    public static function generateWebsiteCustomize(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WebsiteCustomize {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $websiteCustomize = new WebsiteCustomize($id);
        $websiteCustomize->setId($id);

        //version
        self::generateVersion($websiteCustomize, $faker, $value);
        //category
        self::generateCategory($websiteCustomize, $faker, $value);
        //content
        self::generateContent($websiteCustomize, $faker, $value);
        //crew
        self::generateCrew($websiteCustomize, $faker, $value);
        //status
        self::generateStatus($websiteCustomize, $faker, $value);

        $websiteCustomize->setCreateTime($faker->unixTime());
        $websiteCustomize->setUpdateTime($faker->unixTime());
        $websiteCustomize->setStatusTime($faker->unixTime());

        return $websiteCustomize;
    }

    private static function generateVersion($websiteCustomize, $faker, $value) : void
    {
        unset($faker);
        $version = isset($value['version']) ?
            $value['version'] :
            '202108090078';
        
        $websiteCustomize->setVersion($version);
    }

    private static function generateCategory($websiteCustomize, $faker, $value) : void
    {
        $category = isset($value['category']) ?
            $value['category'] :
            $faker->randomElement(WebsiteCustomize::CATEGORY);
        
        $websiteCustomize->setCategory($category);
    }

    private static function generateContent($websiteCustomize, $faker, $value) : void
    {
        unset($faker);
        $content = isset($value['content']) ? $value['content'] : array(1,2);
        $websiteCustomize->setContent($content);
    }

    private static function generateCrew($websiteCustomize, $faker, $value) : void
    {
        unset($faker);
        $crew = isset($value['crew']) ?
            $value['crew'] :
            \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        $websiteCustomize->setCrew($crew);
    }

    private static function generateStatus($websiteCustomize, $faker, $value) : void
    {
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(WebsiteCustomize::STATUS);
        $websiteCustomize->setStatus($status);
    }
}
