<?php
namespace Base\Sdk\WebsiteCustomize\Utils;

use Base\Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator;

use Base\Sdk\Crew\Translator\CrewTranslator;

trait WebsiteCustomizeUtils
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    private function compareArrayAndObject(
        array $expectedArray,
        $websiteCustomize
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($websiteCustomize->getId()));
        $this->assertEquals($expectedArray['category'], marmot_encode($websiteCustomize->getCategory()));
        $this->assertEquals($expectedArray['content'], $websiteCustomize->getContent());
        $this->assertEquals($expectedArray['version'], $websiteCustomize->getVersion());
        $this->assertEquals($expectedArray['status']['id'], marmot_encode($websiteCustomize->getStatus()));
        $this->assertEquals(
            $expectedArray['status']['name'],
            WebsiteCustomizeTranslator::STATUS_CN[$websiteCustomize->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            WebsiteCustomizeTranslator::STATUS_TAG_TYPE[$websiteCustomize->getStatus()]
        );

        $this->assertEquals($expectedArray['updateTime'], $websiteCustomize->getUpdateTime());
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $websiteCustomize->getUpdateTime())
        );

        $this->assertEquals($expectedArray['statusTime'], $websiteCustomize->getStatusTime());
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $websiteCustomize->getStatusTime())
        );

        $this->assertEquals($expectedArray['createTime'], $websiteCustomize->getCreateTime());
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $websiteCustomize->getCreateTime())
        );

        $this->assertEquals(
            $expectedArray['crew'],
            $this->getCrewTranslator()->objectToArray($websiteCustomize->getCrew())
        );
    }
}
