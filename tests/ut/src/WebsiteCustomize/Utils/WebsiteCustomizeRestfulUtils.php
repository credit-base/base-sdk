<?php
namespace Base\Sdk\WebsiteCustomize\Utils;

trait WebsiteCustomizeRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $websiteCustomize
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $websiteCustomize->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['category'], $websiteCustomize->getCategory());
        if (isset($expectedArray['data']['attributes']['version'])) {
            $this->assertEquals($expectedArray['data']['attributes']['version'], $websiteCustomize->getVersion());
        }

        $this->assertEquals($expectedArray['data']['attributes']['content'], $websiteCustomize->getContent());
        $this->assertEquals($expectedArray['data']['attributes']['status'], $websiteCustomize->getStatus());
        
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['crew']['data'])) {
                $this->assertEquals(
                    $relationships['crew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['crew']['data'][0]['id'],
                    $websiteCustomize->getCrew()->getId()
                );
            }
        }
    }
}
