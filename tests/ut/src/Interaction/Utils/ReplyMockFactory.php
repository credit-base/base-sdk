<?php
namespace Base\Sdk\Interaction\Utils;

use Base\Sdk\Interaction\Model\Reply;

class ReplyMockFactory
{
    public static function generateReply(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Reply {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $reply = new Reply($id);
        $reply->setId($id);

        //content
        self::generateContent($reply, $faker, $value);
        //admissibility
        self::generateAdmissibility($reply, $faker, $value);
        //crew
        self::generateCrew($reply, $faker, $value);
        //images
        self::generateImages($reply, $faker, $value);
        //status
        self::generateStatus($reply, $faker, $value);

        $reply->setCreateTime($faker->unixTime());
        $reply->setUpdateTime($faker->unixTime());
        $reply->setStatusTime($faker->unixTime());

        return $reply;
    }

    private static function generateContent($reply, $faker, $value) : void
    {
        $content = isset($value['content']) ?
            $value['content'] :
            $faker->word;
        
        $reply->setContent($content);
    }

    private static function generateAdmissibility($reply, $faker, $value) : void
    {
        $admissibility = isset($value['admissibility']) ?
            $value['admissibility'] :
            $faker->randomElement(Reply::ADMISSIBILITY);
        
        $reply->setAdmissibility($admissibility);
    }

    private static function generateCrew($reply, $faker, $value) : void
    {
        unset($faker);
        $crew = isset($value['crew']) ?
            $value['crew'] :
            \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        
        $reply->setCrew($crew);
    }

    private static function generateImages($reply, $faker, $value) : void
    {
        unset($faker);
        $images = isset($value['images']) ?
            $value['images'] :
            array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg'),
                array('name' => 'name', 'identify' => 'identify.png')
            );
        
        $reply->setImages($images);
    }

    private static function generateStatus($reply, $faker, $value) : void
    {
        unset($faker);
        $status = isset($value['status']) ?
            $value['status'] :
            0;
        
        $reply->setStatus($status);
    }
}
