<?php
namespace Base\Sdk\Interaction\Utils;

use Base\Sdk\Interaction\Translator\Reply\ReplyRestfulTranslator;

trait InteractionRestfulUtils
{
    protected function getReplyRestfulTranslator(): ReplyRestfulTranslator
    {
        return new ReplyRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObject(
        array $expectedArray,
        $interactionObject
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $interactionObject->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['title'], $interactionObject->getTitle());
        $this->assertEquals($expectedArray['data']['attributes']['content'], $interactionObject->getContent());

        $this->assertEquals($expectedArray['data']['attributes']['images'], $interactionObject->getImages());

        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['status'],
                $interactionObject->getStatus()
            );
        }

        if (isset($expectedArray['data']['attributes']['acceptStatus'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['acceptStatus'],
                $interactionObject->getAcceptStatus()
            );
        }

        if (isset($expectedArray['data']['attributes']['certificates'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['certificates'],
                $interactionObject->getCertificates()
            );
        }
        

        $this->assertEquals($expectedArray['data']['attributes']['name'], $interactionObject->getName());
        $this->assertEquals($expectedArray['data']['attributes']['identify'], $interactionObject->getIdentify());

        if (isset($expectedArray['data']['attributes']['subject'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['subject'],
                $interactionObject->getSubject()
            );
        }

        $this->assertEquals($expectedArray['data']['attributes']['contact'], $interactionObject->getContact());

        if (isset($expectedArray['data']['attributes']['applyStatus'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['applyStatus'],
                $interactionObject->getApplyStatus()
            );
        }

        if (isset($expectedArray['data']['attributes']['rejectReason'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['rejectReason'],
                $interactionObject->getRejectReason()
            );
        }
        
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['member']['data'])) {
                $this->assertEquals(
                    $relationships['member']['data'][0]['type'],
                    'members'
                );
                $this->assertEquals(
                    $relationships['member']['data'][0]['id'],
                    $interactionObject->getMember()->getId()
                );
            }

            if (isset($relationships['acceptUserGroup']['data'])) {
                $this->assertEquals(
                    $relationships['acceptUserGroup']['data'][0]['type'],
                    'userGroups'
                );
                $this->assertEquals(
                    $relationships['acceptUserGroup']['data'][0]['id'],
                    $interactionObject->getAcceptUserGroup()->getId()
                );
            }

            if (isset($relationships['applyCrew']['data'])) {
                $this->assertEquals(
                    $relationships['applyCrew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['applyCrew']['data'][0]['id'],
                    $interactionObject->getApplyCrew()->getId()
                );
            }

            if (isset($relationships['reply']['data'])) {
                $this->assertEquals(
                    $relationships['reply']['data'],
                    array_values($this->getReplyRestfulTranslator()->objectToArray($interactionObject->getReply()))
                );
            }
        }
    }
}
