<?php
namespace Base\Sdk\Interaction\Utils;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Crew\Translator\CrewTranslator;

use Base\Sdk\Interaction\Translator\Reply\ReplyTranslator;

trait ReplyUtils
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }
    
    private function compareArrayAndObject(
        array $expectedArray,
        $reply
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($reply->getId()));
        $this->assertEquals($expectedArray['content'], $reply->getContent());
        $this->assertEquals($expectedArray['images'], $reply->getImages());
        $this->assertEquals($expectedArray['admissibility']['id'], marmot_encode($reply->getAdmissibility()));
        $this->assertEquals(
            $expectedArray['admissibility']['name'],
            ReplyTranslator::ADMISSIBILITY_CN[$reply->getAdmissibility()]
        );
        $this->assertEquals(
            $expectedArray['admissibility']['type'],
            ReplyTranslator::ADMISSIBILITY_TYPE[$reply->getAdmissibility()]
        );
        $this->assertEquals($expectedArray['status']['id'], marmot_encode($reply->getStatus()));
        $this->assertEquals(
            $expectedArray['status']['name'],
            IApplyCategory::STATUS_CN[$reply->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            IApplyCategory::STATUS_TAG_TYPE[$reply->getStatus()]
        );

        $this->assertEquals($expectedArray['updateTime'], $reply->getUpdateTime());
        $this->assertEquals($expectedArray['updateTimeFormat'], date('Y年m月d日 H时i分', $reply->getUpdateTime()));

        $this->assertEquals($expectedArray['statusTime'], $reply->getStatusTime());
        $this->assertEquals($expectedArray['statusTimeFormat'], date('Y年m月d日 H时i分', $reply->getStatusTime()));

        $this->assertEquals($expectedArray['createTime'], $reply->getCreateTime());
        $this->assertEquals($expectedArray['createTimeFormat'], date('Y年m月d日 H时i分', $reply->getCreateTime()));

        $this->assertEquals($expectedArray['crew'], $this->getCrewTranslator()->objectToArray($reply->getCrew()));
    }
}
