<?php
namespace Base\Sdk\Interaction\Utils;

use Base\Sdk\Interaction\Model\Interaction;
use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\Interaction\Model\Complaint;
use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Model\UnAuditInteraction;
use Base\Sdk\Interaction\Model\UnAuditAppeal;
use Base\Sdk\Interaction\Model\UnAuditComplaint;
use Base\Sdk\Interaction\Model\UnAuditFeedback;
use Base\Sdk\Interaction\Model\UnAuditPraise;
use Base\Sdk\Interaction\Model\UnAuditQa;
use Base\Sdk\Interaction\Translator\Interaction\UnAuditInteractionTranslator;

use Base\Sdk\Common\Model\IApplyAble;

/**
 * @SuppressWarnings(PHPMD)
 */
class MockFactory
{
    public static function generateCommon(
        Interaction $interaction,
        int $seed = 0,
        array $value = array()
    ) : Interaction {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        //title
        self::generateTitle($interaction, $faker, $value);
        //content
        self::generateContent($interaction, $faker, $value);
        //name
        self::generateName($interaction, $faker, $value);
        //identify
        self::generateIdentify($interaction, $faker, $value);
        //subject
        self::generateSubject($interaction, $faker, $value);
        //contact
        self::generateContact($interaction, $faker, $value);
        //type
        self::generateType($interaction, $faker, $value);
        //images
        self::generateImages($interaction, $faker, $value);
        //acceptStatus
        self::generateAcceptStatus($interaction, $faker, $value);
        //acceptUserGroup
        self::generateAcceptUserGroup($interaction, $faker, $value);
        //reply
        self::generateReply($interaction, $faker, $value);
        //member
        self::generateMember($interaction, $faker, $value);
        //status
        self::generateStatus($interaction, $faker, $value);

        $interaction->setCreateTime($faker->unixTime());
        $interaction->setUpdateTime($faker->unixTime());
        $interaction->setStatusTime($faker->unixTime());

        return $interaction;
    }

    private static function generateTitle($interaction, $faker, $value) : void
    {
        $title = isset($value['title']) ?
            $value['title'] :
            $faker->word;
        
        $interaction->setTitle($title);
    }

    private static function generateContent($interaction, $faker, $value) : void
    {
        $content = isset($value['content']) ?
            $value['content'] :
            $faker->word;
        
        $interaction->setContent($content);
    }

    private static function generateName($interaction, $faker, $value) : void
    {
        $name = isset($value['name']) ?
            $value['name'] :
            $faker->word;
        
        $interaction->setName($name);
    }

    private static function generateIdentify($interaction, $faker, $value) : void
    {
        $identify = isset($value['identify']) ?
            $value['identify'] :
            $faker->word;
        
        $interaction->setIdentify($identify);
    }

    private static function generateSubject($interaction, $faker, $value) : void
    {
        $subject = isset($value['subject']) ?
            $value['subject'] :
            $faker->word;
        
        $interaction->setSubject($subject);
    }

    private static function generateContact($interaction, $faker, $value) : void
    {
        unset($faker);
        $contact = isset($value['contact']) ? $value['contact'] : '16789206538';
        $interaction->setContact($contact);
    }

    private static function generateType($interaction, $faker, $value) : void
    {
        $type = isset($value['type']) ? $value['type'] : $faker->randomElement(Interaction::TYPE);
        $interaction->setType($type);
    }

    private static function generateMember($interaction, $faker, $value) : void
    {
        unset($faker);
        $member = isset($value['member']) ?
            $value['member'] :
            \Base\Sdk\Member\Utils\MockFactory::generateMember(1);
        $interaction->setMember($member);
    }

    private static function generateAcceptUserGroup($interaction, $faker, $value) : void
    {
        unset($faker);
        $acceptUserGroup = isset($value['acceptUserGroup']) ?
            $value['acceptUserGroup'] :
            \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $interaction->setAcceptUserGroup($acceptUserGroup);
    }

    private static function generateReply($interaction, $faker, $value) : void
    {
        unset($faker);
        $reply = isset($value['reply']) ?
            $value['reply'] :
            \Base\Sdk\Interaction\Utils\ReplyMockFactory::generateReply(1);
        $interaction->setReply($reply);
    }

    private static function generateImages($interaction, $faker, $value) : void
    {
        unset($faker);
        $images = isset($value['images']) ?
            $value['images'] :
            array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg'),
                array('name' => 'name', 'identify' => 'identify.png')
            );
        $interaction->setImages($images);
    }

    private static function generateAcceptStatus($interaction, $faker, $value) : void
    {
        $acceptStatus = isset($value['acceptStatus']) ?
            $value['acceptStatus'] :
            $faker->randomElement(Interaction::ACCEPT_STATUS);
        $interaction->setAcceptStatus($acceptStatus);
    }

    private static function generateStatus($interaction, $faker, $value) : void
    {
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(Interaction::STATUS);
        $interaction->setStatus($status);
    }

    public static function generateComplaint(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Complaint {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $complaint = new Complaint($id);
        $complaint->setId($id);

        $complaint = self::generateCommon($complaint, $seed, $value);

        return $complaint;
    }

    public static function generateFeedback(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Feedback {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $feedback = new Feedback($id);
        $feedback->setId($id);

        $feedback = self::generateCommon($feedback, $seed, $value);

        return $feedback;
    }

    public static function generatePraise(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Praise {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $praise = new Praise($id);
        $praise->setId($id);

        $praise = self::generateCommon($praise, $seed, $value);

        return $praise;
    }

    public static function generateQa(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Qa {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $qaObject = new Qa($id);
        $qaObject->setId($id);

        $qaObject = self::generateCommon($qaObject, $seed, $value);

        return $qaObject;
    }

    public static function generateAppeal(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Appeal {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $appeal = new Appeal($id);
        $appeal->setId($id);

        $appeal = self::generateCommon($appeal, $seed, $value);

        $certificates = isset($value['certificates']) ?
            $value['certificates'] :
            array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg')
            );
        $appeal->setCertificates($certificates);

        return $appeal;
    }

    public function getUnAuditInteractionTranslator():UnAuditInteractionTranslator
    {
        return new UnAuditInteractionTranslator();
    }

    public static function generateUnAuditedInteraction(
        UnAuditInteraction $unAuditInteraction,
        int $seed = 0,
        array $value = array()
    ) : UnAuditInteraction {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditInteraction = self::generateCommon($unAuditInteraction, $seed, $value);

        //applyStatus
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : 2;
        $unAuditInteraction->setApplyStatus($applyStatus);

        //rejectReason
        $rejectReason = isset($value['rejectReason']) ? $value['rejectReason'] : $faker->word;
        $unAuditInteraction->setRejectReason($rejectReason);

        //applyCrew
        $applyCrew = isset($value['applyCrew']) ? $value['applyCrew'] :
        \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        $unAuditInteraction->setApplyCrew($applyCrew);

        //applyUserGroup
        $applyUserGroup = isset($value['applyUserGroup']) ? $value['applyUserGroup']:
        \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $unAuditInteraction->setApplyUserGroup($applyUserGroup);

        //acceptStatus
        $unAuditInteraction->setAcceptStatus(2);

        return $unAuditInteraction;
    }

    public static function generateUnAuditComplaint(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditComplaint {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditComplaint = new UnAuditComplaint($id);
        $unAuditComplaint->setId($id);

        $unAuditComplaint = self::generateUnAuditedInteraction($unAuditComplaint, $seed, $value);

        return $unAuditComplaint;
    }

    public static function generateUnAuditFeedback(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditFeedback {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditFeedback = new UnAuditFeedback($id);
        $unAuditFeedback->setId($id);

        $unAuditFeedback = self::generateUnAuditedInteraction($unAuditFeedback, $seed, $value);

        return $unAuditFeedback;
    }

    public static function generateUnAuditPraise(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditPraise {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditPraise = new UnAuditPraise($id);
        $unAuditPraise->setId($id);

        $unAuditPraise = self::generateUnAuditedInteraction($unAuditPraise, $seed, $value);

        return $unAuditPraise;
    }

    public static function generateUnAuditQa(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditQa {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditQa = new UnAuditQa($id);
        $unAuditQa->setId($id);

        $unAuditQa = self::generateUnAuditedInteraction($unAuditQa, $seed, $value);

        return $unAuditQa;
    }

    public static function generateUnAuditAppeal(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditAppeal {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditAppeal = new UnAuditAppeal($id);
        $unAuditAppeal->setId($id);

        $unAuditAppeal = self::generateUnAuditedInteraction($unAuditAppeal, $seed, $value);

        return $unAuditAppeal;
    }
}
