<?php
namespace Base\Sdk\Interaction\Utils;

use Base\Sdk\Common\Utils\Mask;

use Base\Sdk\Interaction\Translator\Interaction\InteractionTranslator;
use Base\Sdk\Member\Translator\MemberTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;

use Base\Sdk\Interaction\Translator\Reply\ReplyTranslator;
use Base\Sdk\Common\Model\IApplyCategory;
use Base\Sdk\Crew\Translator\CrewTranslator;

trait InteractionUtils
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getMemberTranslator():MemberTranslator
    {
        return new MemberTranslator();
    }

    protected function getUserGroupTranslator():UserGroupTranslator
    {
        return new UserGroupTranslator();
    }

    protected function getReplyTranslator():ReplyTranslator
    {
        return new ReplyTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObject(
        array $expectedArray,
        $interactionObject
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($interactionObject->getId()));
        $this->assertEquals($expectedArray['title'], $interactionObject->getTitle());
        $this->assertEquals($expectedArray['content'], $interactionObject->getContent());
        $this->assertEquals(
            $expectedArray['name'],
            $interactionObject->getName()
        );
        $this->assertEquals($expectedArray['identify'], $interactionObject->getIdentify());
        $this->assertEquals($expectedArray['identifyMask'], Mask::mask($interactionObject->getIdentify(), 4, 10));
        
        if (isset($expectedArray['data']['attributes']['subject'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['subject'],
                $interactionObject->getSubject()
            );
        }
        $this->assertEquals($expectedArray['type']['id'], marmot_encode($interactionObject->getType()));
        $this->assertEquals(
            $expectedArray['type']['name'],
            InteractionTranslator::TYPE_CN[$interactionObject->getType()]
        );
        $this->assertEquals($expectedArray['images'], $interactionObject->getImages());
        $this->assertEquals($expectedArray['contact'], $interactionObject->getContact());

        $this->assertEquals(
            $expectedArray['acceptStatus']['id'],
            marmot_encode($interactionObject->getAcceptStatus())
        );
        $this->assertEquals(
            $expectedArray['acceptStatus']['name'],
            InteractionTranslator::ACCEPT_STATUS_CN[$interactionObject->getAcceptStatus()]
        );
        $this->assertEquals(
            $expectedArray['acceptStatus']['type'],
            InteractionTranslator::ACCEPT_STATUS_TYPE[$interactionObject->getAcceptStatus()]
        );

        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($interactionObject->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            InteractionTranslator::STATUS_CN[$interactionObject->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            InteractionTranslator::STATUS_TYPE[$interactionObject->getStatus()]
        );

        $this->assertEquals($expectedArray['admissibility'], $expectedArray['reply']['admissibility']);

        $this->assertEquals($expectedArray['updateTime'], $interactionObject->getUpdateTime());
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $interactionObject->getUpdateTime())
        );

        $this->assertEquals($expectedArray['statusTime'], $interactionObject->getStatusTime());

        $this->assertEquals($expectedArray['createTime'], $interactionObject->getCreateTime());

        $this->assertEquals(
            $expectedArray['member'],
            $this->getMemberTranslator()->objectToArray($interactionObject->getMember())
        );

        $this->assertEquals(
            $expectedArray['acceptUserGroup'],
            $this->getUserGroupTranslator()->objectToArray($interactionObject->getAcceptUserGroup())
        );

        $this->assertEquals(
            $expectedArray['reply'],
            $this->getReplyTranslator()->objectToArray($interactionObject->getReply())
        );

        if (isset($expectedArray['applyStatus'])) {
            $this->assertEquals(
                $expectedArray['applyStatus']['id'],
                marmot_encode($interactionObject->getApplyStatus())
            );
            $this->assertEquals(
                $expectedArray['applyStatus']['name'],
                IApplyCategory::APPLY_STATUS_CN[$interactionObject->getApplyStatus()]
            );
            $this->assertEquals(
                $expectedArray['applyStatus']['type'],
                IApplyCategory::APPLY_STATUS_TAG_TYPE[$interactionObject->getApplyStatus()]
            );
        }

        if (isset($expectedArray['applyCrew'])) {
            $this->assertEquals(
                $expectedArray['applyCrew'],
                $this->getCrewTranslator()->objectToArray($interactionObject->getApplyCrew())
            );
        }

        if (isset($expectedArray['applyUserGroup'])) {
            $this->assertEquals(
                $expectedArray['applyUserGroup'],
                $this->getUserGroupTranslator()->objectToArray($interactionObject->getApplyUserGroup())
            );
        }

        if (isset($expectedArray['data']['attributes']['certificates'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['certificates'],
                $interactionObject->getCertificates()
            );
        }
    }
}
