<?php
namespace Base\Sdk\Interaction\Utils;

trait ReplyRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $reply
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $reply->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['content'], $reply->getContent());
        $this->assertEquals($expectedArray['data']['attributes']['images'], $reply->getImages());

        $this->assertEquals($expectedArray['data']['attributes']['admissibility'], $reply->getAdmissibility());
        
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['crew']['data'])) {
                $this->assertEquals(
                    $relationships['crew']['data'][0]['type'],
                    'crews'
                );
                $this->assertEquals(
                    $relationships['crew']['data'][0]['id'],
                    $reply->getCrew()->getId()
                );
            }
        }
    }
}
