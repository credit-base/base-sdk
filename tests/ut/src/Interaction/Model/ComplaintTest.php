<?php
namespace Base\Sdk\Interaction\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class ComplaintTest extends TestCase
{
    private $complaint;

    public function setUp()
    {
        $this->complaint = new Complaint();
    }

    public function tearDown()
    {
        unset($this->complaint);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\ComplaintRepository',
            $this->complaint->getRepository()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\ComplaintRepository',
            $this->complaint->getRepository()
        );
    }
}
