<?php
namespace Base\Sdk\Interaction\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class UnAuditComplaintTest extends TestCase
{
    private $unAuditComplaint;

    public function setUp()
    {
        $this->unAuditComplaint = new UnAuditComplaint();
    }

    public function tearDown()
    {
        unset($this->unAuditComplaint);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->unAuditComplaint->getId());
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditComplaintRepository',
            $this->unAuditComplaint->getRepository()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditComplaintRepository',
            $this->unAuditComplaint->getRepository()
        );
    }
}
