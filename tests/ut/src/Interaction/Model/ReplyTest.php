<?php
namespace Base\Sdk\Interaction\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Sdk\Crew\Model\Crew;

/**
 * @SuppressWarnings(PHPMD)
 */
class ReplyTest extends TestCase
{
    private $reply;

    public function setUp()
    {
        $this->reply = new Reply();
    }

    public function tearDown()
    {
        unset($this->reply);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->reply->getId());
        $this->assertEquals(0, $this->reply->getStatus());
        $this->assertEquals(Reply::ADMISSIBILITY['ADMISSIBLE'], $this->reply->getAdmissibility());
        $this->assertEquals('', $this->reply->getContent());
        $this->assertEquals(array(), $this->reply->getImages());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->reply->getCrew());
    }

    //content 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $expected = 'string';

        $this->reply->setContent($expected);
        $this->assertEquals($expected, $this->reply->getContent());
    }

    /**
     * 设置 reply setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->reply->setContent(['string']);
    }
    //content 测试 --------------------------------------------------------   end

    //admissibility 测试 ------------------------------------------------------ start
    /**
     * 循环测试 reply setAdmissibility() 是否符合预定范围
     *
     * @dataProvider admissibilityProvider
     */
    public function testSetAdmissibility($actual, $expected)
    {
        $this->reply->setAdmissibility($actual);
        $this->assertEquals($expected, $this->reply->getAdmissibility());
    }

    /**
     * 循环测试 Praise setAdmissibility() 数据构建器
     */
    public function admissibilityProvider()
    {
        return array(
            array(Reply::ADMISSIBILITY['ADMISSIBLE'], Reply::ADMISSIBILITY['ADMISSIBLE']),
            array(Reply::ADMISSIBILITY['INADMISSIBLE'], Reply::ADMISSIBILITY['INADMISSIBLE']),
        );
    }

    /**
     * 设置 Praise setAdmissibility() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAdmissibilityWrongType()
    {
        $this->reply->setAdmissibility('string');
    }
    //admissibility 测试 ------------------------------------------------------   end

    //crew 测试 -------------------------------------------------------- start
    /**
     * 设置 reply setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->reply->setCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->reply->getCrew());
    }

    /**
     * 设置 reply setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->reply->setCrew('Crew');
    }
    //crew 测试 --------------------------------------------------------   end

    //images 测试 -------------------------------------------------------- start
    /**
     * 设置 reply setImages() 正确的传参类型,期望传值正确
     */
    public function testSetImagesCorrectType()
    {
        $expected = array();

        $this->reply->setImages($expected);
        $this->assertEquals($expected, $this->reply->getImages());
    }

    /**
     * 设置 reply setImages() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetImagesWrongType()
    {
        $this->reply->setImages('string');
    }
    //images 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 reply setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $expected = 0;

        $this->reply->setStatus($expected);
        $this->assertEquals($expected, $this->reply->getStatus());
    }

    /**
     * 设置 reply setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->reply->setStatus(['string']);
    }
    //status 测试 --------------------------------------------------------   end
}
