<?php
namespace Base\Sdk\Interaction\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class QaTest extends TestCase
{
    private $qaObject;

    public function setUp()
    {
        $this->qaObject = new Qa();
    }

    public function tearDown()
    {
        unset($this->qaObject);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\QaRepository',
            $this->qaObject->getRepository()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\QaRepository',
            $this->qaObject->getRepository()
        );
    }
}
