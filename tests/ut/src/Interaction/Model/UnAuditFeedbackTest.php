<?php
namespace Base\Sdk\Interaction\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class UnAuditFeedbackTest extends TestCase
{
    private $unAuditFeedback;

    public function setUp()
    {
        $this->unAuditFeedback = new UnAuditFeedback();
    }

    public function tearDown()
    {
        unset($this->unAuditFeedback);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->unAuditFeedback->getId());
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditFeedbackRepository',
            $this->unAuditFeedback->getRepository()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditFeedbackRepository',
            $this->unAuditFeedback->getRepository()
        );
    }
}
