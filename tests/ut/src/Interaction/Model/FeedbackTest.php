<?php
namespace Base\Sdk\Interaction\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class FeedbackTest extends TestCase
{
    private $feedback;

    public function setUp()
    {
        $this->feedback = new Feedback();
    }

    public function tearDown()
    {
        unset($this->feedback);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\FeedbackRepository',
            $this->feedback->getRepository()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\FeedbackRepository',
            $this->feedback->getRepository()
        );
    }
}
