<?php
namespace Base\Sdk\Interaction\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullPraiseTest extends TestCase
{
    private $guest;

    public function setUp()
    {
        $this->guest = NullPraise::getInstance();
    }

    public function tearDown()
    {
        unset($this->guest);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsPraise()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Model\Praise',
            $this->guest
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->guest
        );
    }

    public function testResourceNotExist()
    {
        $guest = new MockNullPraise();

        $result = $guest->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAccept()
    {
        $nullPraise = new MockNullPraise();

        $result = $nullPraise->accept();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testPublish()
    {
        $nullPraise = new MockNullPraise();

        $result = $nullPraise->publish();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testUnPublish()
    {
        $nullPraise = new MockNullPraise();

        $result = $nullPraise->unPublish();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
