<?php
namespace Base\Sdk\Interaction\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullUnAuditQaTest extends TestCase
{
    private $guest;

    public function setUp()
    {
        $this->guest = NullUnAuditQa::getInstance();
    }

    public function tearDown()
    {
        unset($this->guest);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsQa()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Model\UnAuditQa',
            $this->guest
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->guest
        );
    }

    public function testResourceNotExist()
    {
        $guest = new MockNullUnAuditQa();

        $result = $guest->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
