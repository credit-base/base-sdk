<?php
namespace Base\Sdk\Interaction\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullUnAuditAppealTest extends TestCase
{
    private $guest;

    public function setUp()
    {
        $this->guest = NullUnAuditAppeal::getInstance();
    }

    public function tearDown()
    {
        unset($this->guest);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsAppeal()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Model\UnAuditAppeal',
            $this->guest
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->guest
        );
    }

    public function testResourceNotExist()
    {
        $guest = new MockNullUnAuditAppeal();

        $result = $guest->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
