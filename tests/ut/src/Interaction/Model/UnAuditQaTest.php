<?php
namespace Base\Sdk\Interaction\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class UnAuditQaTest extends TestCase
{
    private $unAuditQa;

    public function setUp()
    {
        $this->unAuditQa = new UnAuditQa();
    }

    public function tearDown()
    {
        unset($this->unAuditQa);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->unAuditQa->getId());
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditQaRepository',
            $this->unAuditQa->getRepository()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditQaRepository',
            $this->unAuditQa->getRepository()
        );
    }
}
