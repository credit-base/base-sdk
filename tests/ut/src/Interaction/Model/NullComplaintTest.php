<?php
namespace Base\Sdk\Interaction\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullComplaintTest extends TestCase
{
    private $guest;

    public function setUp()
    {
        $this->guest = NullComplaint::getInstance();
    }

    public function tearDown()
    {
        unset($this->guest);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsComplaint()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Model\Complaint',
            $this->guest
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->guest
        );
    }

    public function testResourceNotExist()
    {
        $guest = new MockNullComplaint();

        $result = $guest->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAccept()
    {
        $nullComplaint = new MockNullComplaint();

        $result = $nullComplaint->accept();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
