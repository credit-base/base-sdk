<?php
namespace Base\Sdk\Interaction\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullAppealTest extends TestCase
{
    private $guest;

    public function setUp()
    {
        $this->guest = NullAppeal::getInstance();
    }

    public function tearDown()
    {
        unset($this->guest);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsAppeal()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Model\Appeal',
            $this->guest
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->guest
        );
    }

    public function testResourceNotExist()
    {
        $guest = new MockNullAppeal();

        $result = $guest->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAccept()
    {
        $nullAppeal = new MockNullAppeal();

        $result = $nullAppeal->accept();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
