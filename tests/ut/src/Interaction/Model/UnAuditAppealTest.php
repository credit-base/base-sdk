<?php
namespace Base\Sdk\Interaction\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class UnAuditAppealTest extends TestCase
{
    private $unAuditAppeal;

    public function setUp()
    {
        $this->unAuditAppeal = new UnAuditAppeal();
    }

    public function tearDown()
    {
        unset($this->unAuditAppeal);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(array(), $this->unAuditAppeal->getCertificates());
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditAppealRepository',
            $this->unAuditAppeal->getRepository()
        );
    }

    //certificates 测试 -------------------------------------------------------- start
    /**
     * 设置 Appeal setCertificates() 正确的传参类型,期望传值正确
     */
    public function testSetCertificatesCorrectType()
    {
        $certificates = array(
            array('name' => 'name', 'identify' => 'identify.jpg'),
            array('name' => 'name', 'identify' => 'identify.jpeg')
        );
        $this->unAuditAppeal->setCertificates($certificates);
        $this->assertEquals($certificates, $this->unAuditAppeal->getCertificates());
    }

    /**
     * 设置 Appeal setCertificates() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCertificatesWrongType()
    {
        $this->unAuditAppeal->setCertificates('string');
    }
    //certificates 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditAppealRepository',
            $this->unAuditAppeal->getRepository()
        );
    }
}
