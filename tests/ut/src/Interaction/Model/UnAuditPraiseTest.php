<?php
namespace Base\Sdk\Interaction\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;

/**
 * @SuppressWarnings(PHPMD)
 */
class UnAuditPraiseTest extends TestCase
{
    private $unAuditPraise;

    public function setUp()
    {
        $this->unAuditPraise = new MockUnAuditPraise();
    }

    public function tearDown()
    {
        unset($this->unAuditPraise);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->unAuditPraise->getId());
        $this->assertEquals(UnAuditPraise::APPLY_STATUS['PENDING'], $this->unAuditPraise->getApplyStatus());
        $this->assertEquals('', $this->unAuditPraise->getRejectReason());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->unAuditPraise->getApplyCrew());
        $this->assertInstanceOf('Base\Sdk\UserGroup\Model\UserGroup', $this->unAuditPraise->getApplyUserGroup());
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\unAuditPraiseRepository',
            $this->unAuditPraise->getRepository()
        );
    }

    //rejectReason 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $expected = 'string';

        $this->unAuditPraise->setRejectReason($expected);
        $this->assertEquals($expected, $this->unAuditPraise->getRejectReason());
    }

    /**
     * 设置 unAuditPraise setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->unAuditPraise->setRejectReason(['string']);
    }
    //rejectReason 测试 --------------------------------------------------------   end

    //applyStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 unAuditPraise setApplyStatus() 是否符合预定范围
     *
     * @dataProvider applyStatusProvider
     */
    public function testSetApplyStatus($actual, $expected)
    {
        $this->unAuditPraise->setApplyStatus($actual);
        $this->assertEquals($expected, $this->unAuditPraise->getApplyStatus());
    }

    /**
     * 循环测试 Praise setApplyStatus() 数据构建器
     */
    public function applyStatusProvider()
    {
        return array(
            array(UnAuditPraise::APPLY_STATUS['NOT_SUBMITTED'], UnAuditPraise::APPLY_STATUS['NOT_SUBMITTED']),
            array(UnAuditPraise::APPLY_STATUS['PENDING'], UnAuditPraise::APPLY_STATUS['PENDING']),
            array(UnAuditPraise::APPLY_STATUS['APPROVE'], UnAuditPraise::APPLY_STATUS['APPROVE']),
            array(UnAuditPraise::APPLY_STATUS['REJECT'], UnAuditPraise::APPLY_STATUS['REJECT']),
        );
    }

    /**
     * 设置 Praise setApplyStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyStatusWrongType()
    {
        $this->unAuditPraise->setApplyStatus('string');
    }
    //applyStatus 测试 ------------------------------------------------------   end

    //applyCrew 测试 -------------------------------------------------------- start
    /**
     * 设置 unAuditPraise setApplyCrew() 正确的传参类型,期望传值正确
     */
    public function testSetApplyCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->unAuditPraise->setApplyCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->unAuditPraise->getApplyCrew());
    }

    /**
     * 设置 unAuditPraise setApplyCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyCrewWrongType()
    {
        $this->unAuditPraise->setApplyCrew('Crew');
    }
    //applyCrew 测试 --------------------------------------------------------   end

    //applyUserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 unAuditPraise setApplyUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetApplyUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->unAuditPraise->setApplyUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->unAuditPraise->getApplyUserGroup());
    }

    /**
     * 设置 unAuditPraise setApplyUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyUserGroupWrongType()
    {
        $this->unAuditPraise->setApplyUserGroup('UserGroup');
    }
    //applyUserGroup 测试 --------------------------------------------------------   end

    public function testGetIApplyAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditPraiseRepository',
            $this->unAuditPraise->getIApplyAbleAdapter()
        );
    }

    public function testGetIResubmitAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditPraiseRepository',
            $this->unAuditPraise->getIResubmitAbleAdapter()
        );
    }
}
