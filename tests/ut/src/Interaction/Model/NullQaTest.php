<?php
namespace Base\Sdk\Interaction\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullQaTest extends TestCase
{
    private $guest;

    public function setUp()
    {
        $this->guest = NullQa::getInstance();
    }

    public function tearDown()
    {
        unset($this->guest);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsQa()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Model\Qa',
            $this->guest
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->guest
        );
    }

    public function testResourceNotExist()
    {
        $guest = new MockNullQa();

        $result = $guest->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testAccept()
    {
        $nullQa = new MockNullQa();

        $result = $nullQa->accept();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testPublish()
    {
        $nullQa = new MockNullQa();

        $result = $nullQa->publish();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testUnPublish()
    {
        $nullQa = new MockNullQa();

        $result = $nullQa->unPublish();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
