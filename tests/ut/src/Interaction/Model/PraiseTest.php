<?php
namespace Base\Sdk\Interaction\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Interaction\Repository\PraiseRepository;

/**
 * @SuppressWarnings(PHPMD)
 */
class PraiseTest extends TestCase
{
    private $praise;

    public function setUp()
    {
        $this->praise = new MockPraise();
    }

    public function tearDown()
    {
        unset($this->praise);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->praise->getId());
        $this->assertEquals('', $this->praise->getTitle());
        $this->assertEquals('', $this->praise->getContent());
        $this->assertEquals('', $this->praise->getName());
        $this->assertEquals('', $this->praise->getIdentify());
        $this->assertEquals('', $this->praise->getSubject());
        $this->assertEquals('', $this->praise->getContact());
        $this->assertEquals(array(), $this->praise->getImages());
        $this->assertEquals(Praise::STATUS['NORMAL'], $this->praise->getStatus());
        $this->assertEquals(Praise::ACCEPT_STATUS['PENDING'], $this->praise->getAcceptStatus());
        $this->assertInstanceOf('Base\Sdk\Member\Model\Member', $this->praise->getMember());
        $this->assertInstanceOf('Base\Sdk\Interaction\Model\Reply', $this->praise->getReply());
        $this->assertInstanceOf('Base\Sdk\UserGroup\Model\UserGroup', $this->praise->getAcceptUserGroup());
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\PraiseRepository',
            $this->praise->getRepository()
        );
    }

    //contact 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setContact() 正确的传参类型,期望传值正确
     */
    public function testSetContactCorrectType()
    {
        $expected = '18790643792';

        $this->praise->setContact($expected);
        $this->assertEquals($expected, $this->praise->getContact());
    }

    /**
     * 设置 Praise setContact() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContactWrongType()
    {
        $this->praise->setContact(['contact']);
    }
    //contact 测试 --------------------------------------------------------   end

    //title 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->praise->setTitle('title');
        $this->assertEquals('title', $this->praise->getTitle());
    }

    /**
     * 设置 Praise setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->praise->setTitle(['title']);
    }
    //title 测试 --------------------------------------------------------   end

    //content 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->praise->setContent('content');
        $this->assertEquals('content', $this->praise->getContent());
    }

    /**
     * 设置 Praise setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->praise->setContent(['content']);
    }
    //content 测试 --------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->praise->setName('name');
        $this->assertEquals('name', $this->praise->getName());
    }

    /**
     * 设置 Praise setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->praise->setName(['name']);
    }
    //name 测试 --------------------------------------------------------   end

    //identify 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->praise->setIdentify('identify');
        $this->assertEquals('identify', $this->praise->getIdentify());
    }

    /**
     * 设置 Praise setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->praise->setIdentify(['identify']);
    }
    //identify 测试 --------------------------------------------------------   end

    //subject 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setSubject() 正确的传参类型,期望传值正确
     */
    public function testSetSubjectCorrectType()
    {
        $this->praise->setSubject('subject');
        $this->assertEquals('subject', $this->praise->getSubject());
    }

    /**
     * 设置 Praise setSubject() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubjectWrongType()
    {
        $this->praise->setSubject(['subject']);
    }
    //subject 测试 --------------------------------------------------------   end
    
    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Praise setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->praise->setStatus($actual);
        $this->assertEquals($expected, $this->praise->getStatus());
    }

    /**
     * 循环测试 Praise setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Praise::STATUS['NORMAL'], Praise::STATUS['NORMAL']),
            array(Praise::STATUS['REVOKED'], Praise::STATUS['REVOKED']),
            array(Praise::STATUS['PUBLISH'], Praise::STATUS['PUBLISH']),
        );
    }

    /**
     * 设置 Praise setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->praise->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

    //type 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Praise setType() 是否符合预定范围
     *
     * @dataProvider typeProvider
     */
    public function testSetType($actual, $expected)
    {
        $this->praise->setType($actual);
        $this->assertEquals($expected, $this->praise->getType());
    }

    /**
     * 循环测试 Praise setType() 数据构建器
     */
    public function typeProvider()
    {
        return array(
            array(Praise::TYPE['PERSONAL'], Praise::TYPE['PERSONAL']),
            array(Praise::TYPE['ENTERPRISE'], Praise::TYPE['ENTERPRISE']),
            array(Praise::TYPE['GOVERNMENT'], Praise::TYPE['GOVERNMENT']),
        );
    }

    /**
     * 设置 Praise setType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTypeWrongType()
    {
        $this->praise->setType('string');
    }
    //type 测试 ------------------------------------------------------   end

    //acceptStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Praise setAcceptStatus() 是否符合预定范围
     *
     * @dataProvider acceptStatusProvider
     */
    public function testSetAcceptStatus($actual, $expected)
    {
        $this->praise->setAcceptStatus($actual);
        $this->assertEquals($expected, $this->praise->getAcceptStatus());
    }

    /**
     * 循环测试 Praise setAcceptStatus() 数据构建器
     */
    public function acceptStatusProvider()
    {
        return array(
            array(Praise::ACCEPT_STATUS['PENDING'], Praise::ACCEPT_STATUS['PENDING']),
            array(Praise::ACCEPT_STATUS['ACCEPTING'], Praise::ACCEPT_STATUS['ACCEPTING']),
            array(Praise::ACCEPT_STATUS['PENDING'], Praise::ACCEPT_STATUS['PENDING']),
        );
    }

    /**
     * 设置 Praise setAcceptStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptStatusWrongType()
    {
        $this->praise->setAcceptStatus('string');
    }
    //acceptStatus 测试 ------------------------------------------------------   end

    //reply 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setReply() 正确的传参类型,期望传值正确
     */
    public function testSetReplyCorrectType()
    {
        $expectedReply = new Reply();

        $this->praise->setReply($expectedReply);
        $this->assertEquals($expectedReply, $this->praise->getReply());
    }

    /**
     * 设置 Praise setReply() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReplyWrongType()
    {
        $this->praise->setReply('reply');
    }
    //reply 测试 --------------------------------------------------------   end

    //member 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $expectedMember = new Member();

        $this->praise->setMember($expectedMember);
        $this->assertEquals($expectedMember, $this->praise->getMember());
    }

    /**
     * 设置 Praise setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberWrongType()
    {
        $this->praise->setMember('member');
    }
    //member 测试 --------------------------------------------------------   end

    //acceptUserGroup 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setAcceptUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetAcceptUserGroupCorrectType()
    {
        $expectedUserGroup = new UserGroup();

        $this->praise->setAcceptUserGroup($expectedUserGroup);
        $this->assertEquals($expectedUserGroup, $this->praise->getAcceptUserGroup());
    }

    /**
     * 设置 Praise setAcceptUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptUserGroupWrongType()
    {
        $this->praise->setAcceptUserGroup('UserGroup');
    }
    //acceptUserGroup 测试 --------------------------------------------------------   end

    //images 测试 -------------------------------------------------------- start
    /**
     * 设置 Praise setImages() 正确的传参类型,期望传值正确
     */
    public function testSetImagesCorrectType()
    {
        $images = array(
            array('name' => 'name', 'identify' => 'identify.jpg'),
            array('name' => 'name', 'identify' => 'identify.jpeg'),
            array('name' => 'name', 'identify' => 'identify.png')
        );
        $this->praise->setImages($images);
        $this->assertEquals($images, $this->praise->getImages());
    }

    /**
     * 设置 Praise setImages() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetImagesWrongType()
    {
        $this->praise->setImages('string');
    }
    //images 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\PraiseRepository',
            $this->praise->getRepository()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\PraiseRepository',
            $this->praise->getIOperateAbleAdapter()
        );
    }

    public function testGetIModifyStatusAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\PraiseRepository',
            $this->praise->getIModifyStatusAbleAdapter()
        );
    }

    public function testAcceptSuccess()
    {
        $this->stub = $this->getMockBuilder(MockPraise::class)
            ->setMethods(['getRepository','isNormal'])->getMock();

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->accept(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->setStatus(Praise::STATUS['NORMAL']);
        $this->stub->expects($this->exactly(1))
                ->method('isNormal')
                ->willReturn(true);

        $result = $this->stub->accept();
        $this->assertTrue($result);
    }

    public function testAcceptFail()
    {
        $this->stub = $this->getMockBuilder(MockPraise::class)
            ->setMethods(['getRepository','isNormal'])->getMock();

        $this->stub->setStatus(Praise::STATUS['REVOKED']);
        $this->stub->expects($this->exactly(1))
            ->method('isNormal')
            ->willReturn(false);

        $result = $this->stub->accept();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testPublishSuccess()
    {
        $this->stub = $this->getMockBuilder(MockPraise::class)
            ->setMethods(['getRepository','isAccept'])->getMock();

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->publish(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->setAcceptStatus(Praise::ACCEPT_STATUS['COMPLETE']);
        $this->stub->expects($this->exactly(1))
                ->method('isAccept')
                ->willReturn(true);

        $result = $this->stub->publish();
        $this->assertTrue($result);
    }

    public function testPublishFail()
    {
        $this->stub = $this->getMockBuilder(MockPraise::class)
            ->setMethods(['getRepository','isAccept'])->getMock();

        $this->stub->setAcceptStatus(Praise::ACCEPT_STATUS['PENDING']);
        $this->stub->expects($this->exactly(1))
            ->method('isAccept')
            ->willReturn(false);

        $result = $this->stub->publish();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testUnPublishSuccess()
    {
        $this->stub = $this->getMockBuilder(MockPraise::class)
            ->setMethods(['getRepository','isAccept','isPublish'])
            ->getMock();

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->unPublish(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->setStatus(Praise::STATUS['PUBLISH']);
        $this->stub->setAcceptStatus(Praise::ACCEPT_STATUS['COMPLETE']);
        $this->stub->expects($this->exactly(1))
                ->method('isAccept')
                ->willReturn(true);
        $this->stub->expects($this->any())
                ->method('isPublish')
                ->willReturn(true);

        $result = $this->stub->unPublish();
        $this->assertTrue($result);
    }

    public function testUnPublishFail()
    {
        $this->stub = $this->getMockBuilder(MockPraise::class)
            ->setMethods(['getRepository','isAccept','isPublish'])
            ->getMock();

        $this->stub->setStatus(Praise::STATUS['REVOKED']);
        $this->stub->setAcceptStatus(Praise::ACCEPT_STATUS['ACCEPTING']);
        $this->stub->expects($this->exactly(1))
            ->method('isAccept')
            ->willReturn(false);
        $this->stub->expects($this->exactly(1))
            ->method('isPublish')
            ->willReturn(false);

        $result = $this->stub->unPublish();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testRevokeSuccess()
    {
        $this->stub = $this->getMockBuilder(MockPraise::class)
            ->setMethods(['getRepository','isPending','isNormal'])
            ->getMock();

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->revoke(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->setStatus(Praise::STATUS['NORMAL']);
        $this->stub->setAcceptStatus(Praise::ACCEPT_STATUS['PENDING']);
        $this->stub->expects($this->exactly(1))
                ->method('isPending')
                ->willReturn(true);
        $this->stub->expects($this->exactly(1))
                ->method('isNormal')
                ->willReturn(true);

        $result = $this->stub->revoke();
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        $this->stub = $this->getMockBuilder(MockPraise::class)
            ->setMethods(['getRepository','isPending','isNormal'])
            ->getMock();

        $this->stub->setStatus(Praise::STATUS['REVOKED']);
        $this->stub->setAcceptStatus(Praise::ACCEPT_STATUS['COMPLETE']);
        $this->stub->expects($this->exactly(1))
            ->method('isPending')
            ->willReturn(false);
        $this->stub->expects($this->any())
            ->method('isNormal')
            ->willReturn(false);

        $result = $this->stub->revoke();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testIsPublishTrue()
    {
        $this->praise->setStatus(Praise::STATUS['PUBLISH']);

        $result = $this->praise->isPublish();
        $this->assertTrue($result);
    }

    public function testIsPublishFalse()
    {
        $this->praise->setStatus(Praise::STATUS['NORMAL']);

        $result = $this->praise->isPublish();
        $this->assertFalse($result);
    }

    public function testIsAcceptTrue()
    {
        $this->praise->setAcceptStatus(Praise::ACCEPT_STATUS['COMPLETE']);

        $result = $this->praise->isAccept();
        $this->assertTrue($result);
    }

    public function testIsAcceptFalse()
    {
        $this->praise->setAcceptStatus(Praise::ACCEPT_STATUS['PENDING']);

        $result = $this->praise->isAccept();
        $this->assertFalse($result);
    }

    public function testIsPendingTrue()
    {
        $this->praise->setAcceptStatus(Praise::ACCEPT_STATUS['PENDING']);

        $result = $this->praise->isPending();
        $this->assertTrue($result);
    }

    public function testIsPendingFalse()
    {
        $this->praise->setAcceptStatus(Praise::ACCEPT_STATUS['COMPLETE']);

        $result = $this->praise->isPending();
        $this->assertFalse($result);
    }
}
