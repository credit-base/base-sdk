<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditAppeal;

use PHPUnit\Framework\TestCase;
use Base\Sdk\Interaction\Model\NullUnAuditAppeal;

class UnAuditAppealRestfulAdapterTest extends TestCase
{
    private $adapter;

    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UnAuditAppealRestfulAdapter::class)
                           ->setMethods([
                               'fetchOneAction',
                            ])
                           ->getMock();
        $this->childAdapter = new class extends UnAuditAppealRestfulAdapter
        {
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedAppeals', $this->childAdapter->getResource());
    }

     /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDIT_APPEAL_LIST',
                UnAuditAppealRestfulAdapter::SCENARIOS['UN_AUDIT_APPEAL_LIST']
            ],
            [
                'UN_AUDIT_APPEAL_FETCH_ONE',
                UnAuditAppealRestfulAdapter::SCENARIOS['UN_AUDIT_APPEAL_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $unAuditAppeal = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditAppeal($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullUnAuditAppeal::getInstance())
            ->willReturn($unAuditAppeal);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($unAuditAppeal, $result);
    }
}
