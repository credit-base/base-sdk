<?php
namespace Base\Sdk\Interaction\Adapter\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\Interaction\Model\NullFeedback;
use Base\Sdk\Interaction\Translator\Feedback\FeedbackRestfulTranslator;

class FeedbackRestfulAdapterTest extends TestCase
{
    private $adapter;

    private $childAdapter;
    
    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(FeedbackRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'fetchOneAction',
                               'getResource',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                            ])
                           ->getMock();
        $this->childAdapter = new class extends FeedbackRestfulAdapter
        {
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testGetResource()
    {
        $this->assertEquals('feedbacks', $this->childAdapter->getResource());
    }

     /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'FEEDBACK_LIST',
                FeedbackRestfulAdapter::SCENARIOS['FEEDBACK_LIST']
            ],
            [
                'FEEDBACK_FETCH_ONE',
                FeedbackRestfulAdapter::SCENARIOS['FEEDBACK_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $feedback = \Base\Sdk\Interaction\Utils\MockFactory::generateFeedback($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullFeedback::getInstance())
            ->willReturn($feedback);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($feedback, $result);
    }

    /**
     * 为FeedbackRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$Feedback,$keys,$FeedbackArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareFeedbackTranslator(
        Feedback $feedback,
        array $keys,
        array $feedbackArray
    ) {
        $translator = $this->prophesize(FeedbackRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($feedback),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($feedbackArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Feedback $feedback)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($feedback);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFeedbackTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $feedback = \Base\Sdk\Interaction\Utils\MockFactory::generateFeedback(1);
        $feedbackArray = array();

        $this->prepareFeedbackTranslator(
            $feedback,
            array(
                 'title',
                 'content',
                 'member',
                 'acceptUserGroup'
            ),
            $feedbackArray
        );

        $this->adapter
            ->method('post')
            ->with('', $feedbackArray);

        $this->success($feedback);
        $result = $this->adapter->add($feedback);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFeedbackTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $feedback = \Base\Sdk\Interaction\Utils\MockFactory::generateFeedback(1);
        $feedbackArray = array();

        $this->prepareFeedbackTranslator(
            $feedback,
            array(
                 'title',
                 'content',
                 'member',
                 'acceptUserGroup'
            ),
            $feedbackArray
        );

        $this->adapter
            ->method('post')
            ->with('', $feedbackArray);
        
            $this->failure($feedback);
            $result = $this->adapter->add($feedback);
            $this->assertFalse($result);
    }
}
