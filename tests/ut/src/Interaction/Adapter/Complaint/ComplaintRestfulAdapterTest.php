<?php
namespace Base\Sdk\Interaction\Adapter\Complaint;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\NullComplaint;

class ComplaintRestfulAdapterTest extends TestCase
{
    private $adapter;

    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(ComplaintRestfulAdapter::class)
                           ->setMethods([
                               'fetchOneAction',
                            ])
                           ->getMock();
        $this->childAdapter = new class extends ComplaintRestfulAdapter
        {
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('complaints', $this->childAdapter->getResource());
    }

     /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'COMPLAINT_LIST',
                ComplaintRestfulAdapter::SCENARIOS['COMPLAINT_LIST']
            ],
            [
                'COMPLAINT_FETCH_ONE',
                ComplaintRestfulAdapter::SCENARIOS['COMPLAINT_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testImplementsIComplaintAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Adapter\Interaction\IInteractionAdapter',
            $this->adapter
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $complaint = \Base\Sdk\Interaction\Utils\MockFactory::generateComplaint($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullComplaint::getInstance())
            ->willReturn($complaint);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($complaint, $result);
    }
}
