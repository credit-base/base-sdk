<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditComplaint;

use PHPUnit\Framework\TestCase;
use Base\Sdk\Interaction\Model\NullUnAuditComplaint;

class UnAuditComplaintRestfulAdapterTest extends TestCase
{
    private $adapter;

    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UnAuditComplaintRestfulAdapter::class)
                           ->setMethods([
                               'fetchOneAction',
                            ])
                           ->getMock();
        $this->childAdapter = new class extends UnAuditComplaintRestfulAdapter
        {
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedComplaints', $this->childAdapter->getResource());
    }

     /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDIT_COMPLAINT_LIST',
                UnAuditComplaintRestfulAdapter::SCENARIOS['UN_AUDIT_COMPLAINT_LIST']
            ],
            [
                'UN_AUDIT_COMPLAINT_FETCH_ONE',
                UnAuditComplaintRestfulAdapter::SCENARIOS['UN_AUDIT_COMPLAINT_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $unAuditComplaint = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditComplaint($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullUnAuditComplaint::getInstance())
            ->willReturn($unAuditComplaint);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($unAuditComplaint, $result);
    }
}
