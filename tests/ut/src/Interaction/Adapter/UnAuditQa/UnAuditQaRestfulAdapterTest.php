<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditQa;

use PHPUnit\Framework\TestCase;
use Base\Sdk\Interaction\Model\NullUnAuditQa;

class UnAuditQaRestfulAdapterTest extends TestCase
{
    private $adapter;

    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UnAuditQaRestfulAdapter::class)
                           ->setMethods([
                               'fetchOneAction',
                            ])
                           ->getMock();
        $this->childAdapter = new class extends UnAuditQaRestfulAdapter
        {
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedQas', $this->childAdapter->getResource());
    }

     /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDIT_QA_LIST',
                UnAuditQaRestfulAdapter::SCENARIOS['UN_AUDIT_QA_LIST']
            ],
            [
                'UN_AUDIT_QA_FETCH_ONE',
                UnAuditQaRestfulAdapter::SCENARIOS['UN_AUDIT_QA_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $unAuditQa = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditQa($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullUnAuditQa::getInstance())
            ->willReturn($unAuditQa);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($unAuditQa, $result);
    }
}
