<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditPraise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Interaction\Model\UnAuditPraise;
use Base\Sdk\Interaction\Model\NullUnAuditPraise;
use Base\Sdk\Interaction\Translator\Praise\UnAuditPraiseRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD)
 */
class UnAuditPraiseRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UnAuditPraiseRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends UnAuditPraiseRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getUnAuditMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIPraiseAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Adapter\UnAuditInteraction\IUnAuditInteractionAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedPraises', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDIT_PRAISE_LIST',
                UnAuditPraiseRestfulAdapter::SCENARIOS['UN_AUDIT_PRAISE_LIST']
            ],
            [
                'UN_AUDIT_PRAISE_FETCH_ONE',
                UnAuditPraiseRestfulAdapter::SCENARIOS['UN_AUDIT_PRAISE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    public function testFetchOne()
    {
        $id = 1;

        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullUnAuditPraise::getInstance())
            ->willReturn($unAuditPraise);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($unAuditPraise, $result);
    }

    /**
     * 为UnAuditPraiseRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$unAuditPraise,$keys,$unAuditPraiseArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function preparePraiseTranslator(
        UnAuditPraise $unAuditPraise,
        array $keys,
        array $unAuditPraiseArray
    ) {
        $translator = $this->prophesize(UnAuditPraiseRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($unAuditPraise),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($unAuditPraiseArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(UnAuditPraise $unAuditPraise)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($unAuditPraise);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行resubmit（）
     * 判断 result 是否为true
     */
    public function testResubmitSuccess()
    {
        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise(1);
        $unAuditPraiseArray = array();

        $this->preparePraiseTranslator(
            $unAuditPraise,
            array(
                'reply'
            ),
            $unAuditPraiseArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditPraise->getId().'/'.'resubmit', $unAuditPraiseArray);

        $this->success($unAuditPraise);
        $result = $this->adapter->resubmit($unAuditPraise);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行resubmit（）
     * 判断 result 是否为false
     */
    public function testResubmitFailure()
    {
        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise(1);
        $unAuditPraiseArray = array();

        $this->preparePraiseTranslator(
            $unAuditPraise,
            array(
                'reply'
            ),
            $unAuditPraiseArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditPraise->getId().'/'.'resubmit', $unAuditPraiseArray);
        
            $this->failure($unAuditPraise);
            $result = $this->adapter->resubmit($unAuditPraise);
            $this->assertFalse($result);
    }

/**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行approve（）
     * 判断 result 是否为true
     */
    public function testApproveSuccess()
    {
        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise(1);
        $unAuditPraiseArray = array();

        $this->preparePraiseTranslator(
            $unAuditPraise,
            array(
                'applyCrew',
            ),
            $unAuditPraiseArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditPraise->getId().'/approve', $unAuditPraiseArray);

        $this->success($unAuditPraise);
        $result = $this->adapter->approve($unAuditPraise);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行approve（）
     * 判断 result 是否为false
     */
    public function testApproveFailure()
    {
        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise(1);
        $unAuditPraiseArray = array();

        $this->preparePraiseTranslator(
            $unAuditPraise,
            array(
                'applyCrew',
            ),
            $unAuditPraiseArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditPraise->getId().'/approve', $unAuditPraiseArray);
        
        $this->failure($unAuditPraise);
        $result = $this->adapter->approve($unAuditPraise);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行reject（）
     * 判断 result 是否为true
     */
    public function testRejectSuccess()
    {
        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise(1);
        $unAuditPraiseArray = array();

        $this->preparePraiseTranslator(
            $unAuditPraise,
            array(
                'rejectReason',
                'applyCrew'
            ),
            $unAuditPraiseArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditPraise->getId().'/reject', $unAuditPraiseArray);

        $this->success($unAuditPraise);
        $result = $this->adapter->reject($unAuditPraise);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行reject（）
     * 判断 result 是否为false
     */
    public function testRejectFailure()
    {
        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise(1);
        $unAuditPraiseArray = array();

        $this->preparePraiseTranslator(
            $unAuditPraise,
            array(
                'rejectReason',
                'applyCrew'
            ),
            $unAuditPraiseArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$unAuditPraise->getId().'/reject', $unAuditPraiseArray);
        
        $this->failure($unAuditPraise);
        $result = $this->adapter->reject($unAuditPraise);
        $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [
            101 => [
                'replyContent'=>REPLY_CONTENT_FORMAT_ERROR,
                'replyImages'=>REPLY_IMAGES_FORMAT_ERROR,
                'replyImagesCount'=>REPLY_IMAGES_COUNT_FORMAT_ERROR,
                'replyAdmissibility'=>REPLY_ADMISSIBILITY_FORMAT_ERROR,
                'replyCrew'=>REPLY_CREW_FORMAT_ERROR,
                'applyCrewId' => PARAMETER_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
            ]
        ];
        
        $result = $this->childAdapter->getUnAuditMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
