<?php
namespace Base\Sdk\Interaction\Adapter\UnAuditFeedback;

use PHPUnit\Framework\TestCase;
use Base\Sdk\Interaction\Model\NullUnAuditFeedback;

class UnAuditFeedbackRestfulAdapterTest extends TestCase
{
    private $adapter;

    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UnAuditFeedbackRestfulAdapter::class)
                           ->setMethods([
                               'fetchOneAction',
                            ])
                           ->getMock();
        $this->childAdapter = new class extends UnAuditFeedbackRestfulAdapter
        {
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedFeedbacks', $this->childAdapter->getResource());
    }

     /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UN_AUDIT_FEEDBACK_LIST',
                UnAuditFeedbackRestfulAdapter::SCENARIOS['UN_AUDIT_FEEDBACK_LIST']
            ],
            [
                'UN_AUDIT_FEEDBACK_FETCH_ONE',
                UnAuditFeedbackRestfulAdapter::SCENARIOS['UN_AUDIT_FEEDBACK_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $unAuditFeedback = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditFeedback($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullUnAuditFeedback::getInstance())
            ->willReturn($unAuditFeedback);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($unAuditFeedback, $result);
    }
}
