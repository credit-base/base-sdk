<?php
namespace Base\Sdk\Interaction\Adapter\Qa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Model\NullQa;
use Base\Sdk\Interaction\Translator\Qa\QaRestfulTranslator;

class QaRestfulAdapterTest extends TestCase
{
    private $adapter;

    private $childAdapter;
    
    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(QaRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'fetchOneAction',
                               'getResource',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                            ])
                           ->getMock();
        $this->childAdapter = new class extends QaRestfulAdapter
        {
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testGetResource()
    {
        $this->assertEquals('qas', $this->childAdapter->getResource());
    }

     /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'QA_LIST',
                QaRestfulAdapter::SCENARIOS['QA_LIST']
            ],
            [
                'QA_FETCH_ONE',
                QaRestfulAdapter::SCENARIOS['QA_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $qaObject = \Base\Sdk\Interaction\Utils\MockFactory::generateQa($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullQa::getInstance())
            ->willReturn($qaObject);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($qaObject, $result);
    }

    /**
     * 为QaRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$Qa,$keys,$QaArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareQaTranslator(
        Qa $qaObject,
        array $keys,
        array $qaObjectArray
    ) {
        $translator = $this->prophesize(QaRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($qaObject),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($qaObjectArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Qa $qaObject)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($qaObject);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareQaTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $qaObject = \Base\Sdk\Interaction\Utils\MockFactory::generateQa(1);
        $qaObjectArray = array();

        $this->prepareQaTranslator(
            $qaObject,
            array(
                 'title',
                 'content',
                 'member',
                 'acceptUserGroup'
            ),
            $qaObjectArray
        );

        $this->adapter
            ->method('post')
            ->with('', $qaObjectArray);

        $this->success($qaObject);
        $result = $this->adapter->add($qaObject);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareQaTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $qaObject = \Base\Sdk\Interaction\Utils\MockFactory::generateQa(1);
        $qaObjectArray = array();

        $this->prepareQaTranslator(
            $qaObject,
            array(
                 'title',
                 'content',
                 'member',
                 'acceptUserGroup'
            ),
            $qaObjectArray
        );

        $this->adapter
            ->method('post')
            ->with('', $qaObjectArray);
        
            $this->failure($qaObject);
            $result = $this->adapter->add($qaObject);
            $this->assertFalse($result);
    }
}
