<?php
namespace Base\Sdk\Interaction\Adapter\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\Interaction\Model\NullAppeal;
use Base\Sdk\Interaction\Translator\Appeal\AppealRestfulTranslator;

class AppealRestfulAdapterTest extends TestCase
{
    private $adapter;

    private $childAdapter;
    
    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(AppealRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                            ])
                           ->getMock();
        $this->childAdapter = new class extends AppealRestfulAdapter
        {
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testGetResource()
    {
        $this->assertEquals('appeals', $this->childAdapter->getResource());
    }

     /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'APPEAL_LIST',
                AppealRestfulAdapter::SCENARIOS['APPEAL_LIST']
            ],
            [
                'APPEAL_FETCH_ONE',
                AppealRestfulAdapter::SCENARIOS['APPEAL_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $appeal = \Base\Sdk\Interaction\Utils\MockFactory::generateAppeal($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullAppeal::getInstance())
            ->willReturn($appeal);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($appeal, $result);
    }

    /**
     * 为AppealRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$Appeal,$keys,$AppealArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareAppealTranslator(
        Appeal $appeal,
        array $keys,
        array $appealArray
    ) {
        $translator = $this->prophesize(AppealRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($appeal),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($appealArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Appeal $appeal)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($appeal);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAppealTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $appeal = \Base\Sdk\Interaction\Utils\MockFactory::generateAppeal(1);
        $appealArray = array();

        $this->prepareAppealTranslator(
            $appeal,
            array(
                'title',
                'content',
                'name',
                'identify',
                'certificates',
                'type',
                'contact',
                'images',
                'member',
                'acceptUserGroup'
            ),
            $appealArray
        );

        $this->adapter
            ->method('post')
            ->with('', $appealArray);

        $this->success($appeal);
        $result = $this->adapter->add($appeal);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAppealTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $appeal = \Base\Sdk\Interaction\Utils\MockFactory::generateAppeal(1);
        $appealArray = array();

        $this->prepareAppealTranslator(
            $appeal,
            array(
                'title',
                'content',
                'name',
                'identify',
                'certificates',
                'type',
                'contact',
                'images',
                'member',
                'acceptUserGroup'
            ),
            $appealArray
        );

        $this->adapter
            ->method('post')
            ->with('', $appealArray);
        
            $this->failure($appeal);
            $result = $this->adapter->add($appeal);
            $this->assertFalse($result);
    }
}
