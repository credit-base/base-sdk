<?php
namespace Base\Sdk\Interaction\Adapter\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Model\NullPraise;
use Base\Sdk\Interaction\Translator\Praise\PraiseRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD)
 */
class PraiseRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(PraiseRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends PraiseRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIPraiseAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Adapter\Interaction\IInteractionAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('praises', $this->childAdapter->getResource());
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [
            101 => [
                'title' => TITLE_FORMAT_ERROR,
                'content' => CONTENT_FORMAT_ERROR,
                'name'=>NAME_FORMAT_ERROR,
                'identify'=>IDENTIFY_FORMAT_ERROR,
                'subject'=>SUBJECT_FORMAT_ERROR,
                'type'=>TYPE_FORMAT_ERROR,
                'images' => IMAGE_FORMAT_ERROR,
                'imagesCount' => IMAGES_COUNT_FORMAT_ERROR,
                'replyContent'=>REPLY_CONTENT_FORMAT_ERROR,
                'replyImages'=>REPLY_IMAGES_FORMAT_ERROR,
                'replyImagesCount'=>REPLY_IMAGES_COUNT_FORMAT_ERROR,
                'replyAdmissibility'=>REPLY_ADMISSIBILITY_FORMAT_ERROR,
                'replyCrew'=>REPLY_CREW_FORMAT_ERROR,
                'memberId' => MEMBER_ID_FORMAT_ERROR,
                'acceptUserGroupId' => ACCEPT_USER_GROUP_ID_FORMAT_ERROR,
                'contact'=>CONTACT_FORMAT_ERROR,
                'certificates'=>CERTIFICATES_FORMAT_ERROR
            ],
            100=>[
                'memberId'=>MEMBER_ID_IS_EMPTY,
                'acceptUserGroupId'=>ACCEPT_USER_GROUP_ID_IS_EMPTY,
                'crewId' => CREW_ID_IS_EMPTY
            ],
            102=>[
                'applyStatus'=>APPLY_STATUE_CAN_NOT_MODIFY,
                'status'=>STATUS_CAN_NOT_MODIFY
            ]
        ];
        
        $result = $this->childAdapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'PRAISE_LIST',
                PraiseRestfulAdapter::SCENARIOS['PRAISE_LIST']
            ],
            [
                'PRAISE_FETCH_ONE',
                PraiseRestfulAdapter::SCENARIOS['PRAISE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullPraise::getInstance())
            ->willReturn($praise);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($praise, $result);
    }

    /**
     * 为PraiseRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$Praise,$keys,$PraiseArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function preparePraiseTranslator(
        Praise $praise,
        array $keys,
        array $praiseArray
    ) {
        $translator = $this->prophesize(PraiseRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($praise),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($praiseArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Praise $praise)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($praise);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise(1);
        $praiseArray = array();

        $this->preparePraiseTranslator(
            $praise,
            array(
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'contact',
                'images',
                'member',
                'acceptUserGroup'
            ),
            $praiseArray
        );

        $this->adapter
            ->method('post')
            ->with('', $praiseArray);

        $this->success($praise);
        $result = $this->adapter->add($praise);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise(1);
        $praiseArray = array();

        $this->preparePraiseTranslator(
            $praise,
            array(
                'title',
                'content',
                'name',
                'identify',
                'subject',
                'type',
                'contact',
                'images',
                'member',
                'acceptUserGroup'
            ),
            $praiseArray
        );

        $this->adapter
            ->method('post')
            ->with('', $praiseArray);
        
            $this->failure($praise);
            $result = $this->adapter->add($praise);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行accept（）
     * 判断 result 是否为true
     */
    public function testAcceptSuccess()
    {
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise(1);
        $praiseArray = array();

        $this->preparePraiseTranslator(
            $praise,
            array(
                'reply',
            ),
            $praiseArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$praise->getId().'/accept', $praiseArray);

        $this->success($praise);
        $result = $this->adapter->accept($praise);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行accept（）
     * 判断 result 是否为false
     */
    public function testAcceptFailure()
    {
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise(1);
        $praiseArray = array();

        $this->preparePraiseTranslator(
            $praise,
            array(
                'reply'
            ),
            $praiseArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$praise->getId().'/accept', $praiseArray);
        
            $this->failure($praise);
            $result = $this->adapter->accept($praise);
            $this->assertFalse($result);
    }

/**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行publish（）
     * 判断 result 是否为true
     */
    public function testPublishSuccess()
    {
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise(1);
        $praiseArray = array();

        $this->adapter
            ->method('patch')
            ->with('/'.$praise->getId().'/publish', $praiseArray);

        $this->success($praise);
        $result = $this->adapter->publish($praise);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行publish（）
     * 判断 result 是否为false
     */
    public function testPublishFailure()
    {
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise(1);
        $praiseArray = array();

        $this->adapter
            ->method('patch')
            ->with('/'.$praise->getId().'/publish', $praiseArray);
        
        $this->failure($praise);
        $result = $this->adapter->publish($praise);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行unPublish（）
     * 判断 result 是否为true
     */
    public function testUnPublishSuccess()
    {
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise(1);
        $praiseArray = array();

        $this->adapter
            ->method('patch')
            ->with('/'.$praise->getId().'/unPublish', $praiseArray);

        $this->success($praise);
        $result = $this->adapter->unPublish($praise);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePraiseTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行unPublish（）
     * 判断 result 是否为false
     */
    public function testUnPublishFailure()
    {
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise(1);
        $praiseArray = array();

        $this->adapter
            ->method('patch')
            ->with('/'.$praise->getId().'/unPublish', $praiseArray);
        
        $this->failure($praise);
        $result = $this->adapter->unPublish($praise);
        $this->assertFalse($result);
    }

    public function testEdit()
    {
        $praise = new NullPraise();
        $result = $this->adapter->edit($praise);
        $this->assertFalse($result);
    }
}
