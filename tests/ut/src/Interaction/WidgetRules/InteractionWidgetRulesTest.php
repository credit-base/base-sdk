<?php
namespace Base\Sdk\Interaction\WidgetRules;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Base\Sdk\Common\WidgetRules\WidgetRules;

use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Model\Interaction;

/**
 * @SuppressWarnings(PHPMD)
 */
class InteractionWidgetRulesTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = InteractionWidgetRules::getInstance();
        $this->childStub = new class extends InteractionWidgetRules
        {
            public function getCommonWidgetRules() : WidgetRules
            {
                return parent::getCommonWidgetRules();
            }
        };

        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testGetCommonWidgetRules()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\WidgetRules\WidgetRules',
            $this->childStub->getCommonWidgetRules()
        );
    }

    /**
     * @dataProvider imagesProvider
     */
    public function testImages($actual, $expected)
    {
        $result = $this->stub->images($actual);

        if (!$expected) {
            $this->assertFalse($result);
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function imagesProvider()
    {
        $fakerData = \Faker\Factory::create('zh_CN');
        
        return array(
            array(
                [
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.png'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.jpg'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.jpeg'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.jpeg'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.jpeg'],
                ], true),
            array(
                [
                    ['name'=>$fakerData->word, 'identify'=>$fakerData->word],
                    ['name'=>$fakerData->word, 'identify'=>$fakerData->word]
                ], false),
            array(
                [
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.word'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.excel'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.xls'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.md'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.xsl'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.tpl'],
                ], false),
            array($fakerData->word, false),
            array(
                [
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.jpg'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.jpeg'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.png'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.jpeg'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.jpg'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.png'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.png'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.png'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.png'],
                    ['name'=>$fakerData->word,'identify'=>$fakerData->word.'.png'],
                ], false),
        );
    }

    //content -- start
     /**
     * @dataProvider invalidContentProvider
     */
    public function testContent($actual, $expected)
    {
        $result = $this->stub->content($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(REPLY_CONTENT_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidContentProvider()
    {
        return array(
            array('content',true),
            array(123, false),
            array('', false),
        );
    }
    //content -- end

    //admissibility -- start
    /**
     * @dataProvider invalidAdmissibilityProvider
     */
    public function testAdmissibility($actual, $expected)
    {
        $result = $this->stub->admissibility($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(REPLY_ADMISSIBILITY_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidAdmissibilityProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $errorData = [-1,-2,3];

        return array(
            array($faker->randomElement($errorData), false),
            array($faker->randomElement(Reply::ADMISSIBILITY), true),
            array('string', false),
            array('', false),
            array(0, false),
        );
    }
    //admissibility -- end

    //title -- start
    /**
     * @dataProvider invalidTitleProvider
     */
    public function testTitle($actual, $expected)
    {
        $result = $this->stub->title($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(TITLE_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidTitleProvider()
    {
        return array(
            array('title',true),
            array(123, false),
            array('', false),
        );
    }
    //title -- end

    //subject -- start
    /**
     * @dataProvider invalidSubjectProvider
     */
    public function testSubject($actual, $expected)
    {
        $result = $this->stub->subject($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(SUBJECT_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidSubjectProvider()
    {
        return array(
            array('subject',true),
            array(123, false),
            array('s', false),
        );
    }
    //subject -- end

    //identify -- start
    /**
     * @dataProvider invalidIdentifyProvider
     */
    public function testIdentifyInvalid($actual, $expected)
    {
        $result = $this->stub->identify($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(IDENTIFY_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidIdentifyProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array($faker->bothify(''), false),
            array($faker->bothify('##??'), false),//四位混编
            array($faker->bothify('???###???####????##'), false),//十九位
            array($faker->bothify('###############'), true),//十五位
            array($faker->bothify('#################X'), true),//十八位
        );
    }
    //identify -- end

    //type -- start
    /**
     * @dataProvider invalidTypeProvider
     */
    public function testType($actual, $expected)
    {
        $result = $this->stub->type($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(TYPE_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidTypeProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $errorData = [-1,-2,-3,6];

        return array(
            array($faker->randomElement($errorData), false),
            array($faker->randomElement(Interaction::TYPE), true),
            array('string', false),
            array('', false),
            array(0, false),
        );
    }
    //type -- end


    //contact -- start
    /**
     * @dataProvider invalidContactProvider
     */
    public function testContact($actual, $expected)
    {
        $result = $this->stub->contact($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(CONTACT_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidContactProvider()
    {
        return array(
            array('contact',true),
            array(123, false),
            array('', false),
        );
    }
    //contact -- end

    /**
     * @dataProvider certificatesProvider
     */
    public function testCertificates($actual, $expected)
    {
        $result = $this->stub->certificates($actual);

        if (!$expected) {
            $this->assertFalse($result);
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function certificatesProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        
        return array(
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpeg'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                ], false),
            array($faker->word, false),
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.xsl'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.tpl'],
                ], false),
            array(
                [
                    ['name'=>$faker->word,'identify'=>$faker->word.'.png'],
                    ['name'=>$faker->word,'identify'=>$faker->word.'.jpg'],
                ], true),
            
        );
    }
}
