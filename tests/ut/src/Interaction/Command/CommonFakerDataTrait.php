<?php
namespace Base\Sdk\Interaction\Command;

use Base\Sdk\Interaction\Model\Interaction;
use Base\Sdk\Interaction\Model\Reply;

trait CommonFakerDataTrait
{
    public function getAddCommonData()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $fakerData = array(
            'title' => $faker->word,
            'content' => $faker->word,
            'name' => $faker->word,
            'identify' => $faker->word,
            'contact' => 16892875054,
            'subject' => $faker->word,
            'images' => array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg'),
                array('name' => 'name', 'identify' => 'identify.png')
            ),
            'type'=>$faker->randomElement(Interaction::TYPE),
            'acceptUserGroupId' => $faker->randomNumber(1),
            'certificates'=>array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg')
            ),
        );

        return $fakerData;
    }

    public function getReplyCommonData()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $replyData = array(
            'content' => $faker->word,
            'images' => array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg'),
                array('name' => 'name', 'identify' => 'identify.png')
            ),
            'admissibility'=>$faker->randomElement(Reply::ADMISSIBILITY),
            'crew' => $faker->randomNumber(1),
        );

        return $replyData;
    }
}
