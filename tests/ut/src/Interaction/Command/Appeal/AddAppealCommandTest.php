<?php
namespace Base\Sdk\Interaction\Command\Appeal;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AddAppealCommandTest extends TestCase
{
    use CommonFakerDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = $this->getAddCommonData();

        $this->command = new AddAppealCommand(
            $this->fakerData['title'],
            $this->fakerData['content'],
            $this->fakerData['name'],
            $this->fakerData['identify'],
            $this->fakerData['contact'],
            $this->fakerData['certificates'],
            $this->fakerData['images'],
            $this->fakerData['type'],
            $this->fakerData['acceptUserGroupId']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
