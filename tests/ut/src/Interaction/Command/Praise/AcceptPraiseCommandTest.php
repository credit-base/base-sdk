<?php
namespace Base\Sdk\Interaction\Command\Praise;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AcceptPraiseCommandTest extends TestCase
{
    use CommonFakerDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = $this->getReplyCommonData();

        $this->command = new AcceptPraiseCommand(
            $this->fakerData,
            $faker->randomNumber(1)
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
