<?php
namespace Base\Sdk\Interaction\Command\Praise;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AddPraiseCommandTest extends TestCase
{
    use CommonFakerDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = $this->getAddCommonData();

        $this->command = new AddPraiseCommand(
            $this->fakerData['title'],
            $this->fakerData['content'],
            $this->fakerData['name'],
            $this->fakerData['identify'],
            $this->fakerData['contact'],
            $this->fakerData['subject'],
            $this->fakerData['images'],
            $this->fakerData['type'],
            $this->fakerData['acceptUserGroupId']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
