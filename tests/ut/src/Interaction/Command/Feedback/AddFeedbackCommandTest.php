<?php
namespace Base\Sdk\Interaction\Command\Feedback;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AddFeedbackCommandTest extends TestCase
{
    use CommonFakerDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = $this->getAddCommonData();

        $this->command = new AddFeedbackCommand(
            $this->fakerData['title'],
            $this->fakerData['content'],
            $this->fakerData['acceptUserGroupId']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
