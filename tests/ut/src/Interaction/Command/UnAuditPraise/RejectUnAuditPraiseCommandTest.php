<?php
namespace Base\Sdk\Interaction\Command\UnAuditPraise;

use PHPUnit\Framework\TestCase;

class RejectUnAuditPraiseCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $rejectReason = '驳回原因';
        $id = 1;
        $this->stub = new RejectUnAuditPraiseCommand($rejectReason, $id);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
