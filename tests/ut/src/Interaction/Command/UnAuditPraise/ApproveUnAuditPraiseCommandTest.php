<?php
namespace Base\Sdk\Interaction\Command\UnAuditPraise;

use PHPUnit\Framework\TestCase;

class ApproveUnAuditPraiseCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $id = 1;
        $this->stub = new ApproveUnAuditPraiseCommand($id);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
