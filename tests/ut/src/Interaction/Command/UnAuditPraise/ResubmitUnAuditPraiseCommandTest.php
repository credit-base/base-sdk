<?php
namespace Base\Sdk\Interaction\Command\UnAuditPraise;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class ResubmitUnAuditPraiseCommandTest extends TestCase
{
    use CommonFakerDataTrait;

    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = $this->getReplyCommonData();

        $this->command = new ResubmitUnAuditPraiseCommand(
            $this->fakerData,
            $faker->randomNumber(1)
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
