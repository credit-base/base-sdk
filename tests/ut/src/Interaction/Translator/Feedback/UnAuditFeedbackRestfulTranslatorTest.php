<?php
namespace Base\Sdk\Interaction\Translator\Feedback;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\Interaction\Utils\InteractionRestfulUtils;
use Base\Sdk\Crew\Model\Crew;

class UnAuditFeedbackRestfulTranslatorTest extends TestCase
{
    use InteractionRestfulUtils;

    private $translator;

    public function setUp()
    {
        Core::$container->set('crew', new Crew());

        $this->translator = new UnAuditFeedbackRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditFeedback = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditFeedback(1);

        $expression['data']['id'] = $unAuditFeedback->getId();
        $expression['data']['attributes']['title'] = $unAuditFeedback->getTitle();
        $expression['data']['attributes']['content'] = $unAuditFeedback->getContent();

        $expression['data']['attributes']['images'] = $unAuditFeedback->getImages();
        $expression['data']['attributes']['status'] = $unAuditFeedback->getStatus();

        $expression['data']['attributes']['identify'] = $unAuditFeedback->getIdentify();
    
        $expression['data']['attributes']['name'] = $unAuditFeedback->getName();
        $expression['data']['attributes']['contact'] = $unAuditFeedback->getContact();

        $expression['data']['attributes']['acceptStatus'] = $unAuditFeedback->getAcceptStatus();

        $expression['data']['attributes']['createTime'] = $unAuditFeedback->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $unAuditFeedback->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $unAuditFeedback->getUpdateTime();

        $expression['data']['attributes']['rejectReason'] = $unAuditFeedback->getRejectReason();
        $expression['data']['attributes']['applyStatus'] = $unAuditFeedback->getApplyStatus();

        $unAuditFeedbackObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\UnAuditFeedback', $unAuditFeedbackObject);
        $this->compareArrayAndObject($expression, $unAuditFeedbackObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $unAuditFeedback = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullUnAuditFeedback', $unAuditFeedback);
    }

    public function testObjectToArray()
    {
        $unAuditFeedback = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditFeedback(1);

        $expression = $this->translator->objectToArray($unAuditFeedback);

        $this->compareArrayAndObject($expression, $unAuditFeedback);
    }

    public function testObjectToArrayFail()
    {
        $object = null;

        $expression = $this->translator->objectToArray($object);
        $this->assertEquals(array(), $expression);
    }
}
