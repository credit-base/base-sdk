<?php
namespace Base\Sdk\Interaction\Translator\Feedback;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionUtils;

class FeedbackTranslatorTest extends TestCase
{
    use InteractionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new FeedbackTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullFeedback', $objects);
    }

    public function testObjectToArray()
    {
        $feedback = \Base\Sdk\Interaction\Utils\MockFactory::generateFeedback(1);

        $expression = $this->translator->objectToArray($feedback);
        
        $this->compareArrayAndObject($expression, $feedback);
    }

    public function testObjectToArrayFail()
    {
        $feedback = null;

        $expression = $this->translator->objectToArray($feedback);
        $this->assertEquals(array(), $expression);
    }
}
