<?php
namespace Base\Sdk\Interaction\Translator\Feedback;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionRestfulUtils;

class FeedbackRestfulTranslatorTest extends TestCase
{
    use InteractionRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new FeedbackRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $feedback = \Base\Sdk\Interaction\Utils\MockFactory::generateFeedback(1);

        $expression['data']['id'] = $feedback->getId();
        $expression['data']['attributes']['title'] = $feedback->getTitle();
        $expression['data']['attributes']['content'] = $feedback->getContent();

        $expression['data']['attributes']['images'] = $feedback->getImages();
        $expression['data']['attributes']['status'] = $feedback->getStatus();

        $expression['data']['attributes']['identify'] = $feedback->getIdentify();

        $expression['data']['attributes']['name'] = $feedback->getName();
        $expression['data']['attributes']['contact'] = $feedback->getContact();

        $expression['data']['attributes']['acceptStatus'] = $feedback->getAcceptStatus();

        $expression['data']['attributes']['createTime'] = $feedback->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $feedback->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $feedback->getUpdateTime();

        $feedbackObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\Feedback', $feedbackObject);
        $this->compareArrayAndObject($expression, $feedbackObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $feedback = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullFeedback', $feedback);
    }

    public function testObjectToArray()
    {
        $feedback = \Base\Sdk\Interaction\Utils\MockFactory::generateFeedback(1);
       
        $expression = $this->translator->objectToArray($feedback);
        
        $this->compareArrayAndObject($expression, $feedback);
    }

    public function testObjectToArrayFail()
    {
        $feedback = null;

        $expression = $this->translator->objectToArray($feedback);
        $this->assertEquals(array(), $expression);
    }
}
