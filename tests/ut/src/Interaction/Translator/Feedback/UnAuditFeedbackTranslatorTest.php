<?php
namespace Base\Sdk\Interaction\Translator\Feedback;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionUtils;

class UnAuditFeedbackTranslatorTest extends TestCase
{
    use InteractionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditFeedbackTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullUnAuditFeedback', $objects);
    }

    public function testObjectToArray()
    {
        $unAuditFeedback = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditFeedback(1);

        $expression = $this->translator->objectToArray($unAuditFeedback);
        
        $this->compareArrayAndObject($expression, $unAuditFeedback);
        $this->assertEquals($expression['rejectReason'], $unAuditFeedback->getRejectReason());
    }

    public function testObjectToArrayFail()
    {
        $unAuditFeedback = null;

        $expression = $this->translator->objectToArray($unAuditFeedback);
        $this->assertEquals(array(), $expression);
    }
}
