<?php
namespace Base\Sdk\Interaction\Translator\Appeal;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionUtils;

class UnAuditAppealTranslatorTest extends TestCase
{
    use InteractionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditAppealTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullUnAuditAppeal', $objects);
    }

    public function testObjectToArray()
    {
        $unAuditAppeal = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditAppeal(1);

        $expression = $this->translator->objectToArray($unAuditAppeal);

        $this->compareArrayAndObject($expression, $unAuditAppeal);
        $this->assertEquals($expression['rejectReason'], $unAuditAppeal->getRejectReason());
    }

    public function testObjectToArrayFail()
    {
        $unAuditAppeal = null;

        $expression = $this->translator->objectToArray($unAuditAppeal);
        $this->assertEquals(array(), $expression);
    }
}
