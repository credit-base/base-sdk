<?php
namespace Base\Sdk\Interaction\Translator\Appeal;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionRestfulUtils;

class AppealRestfulTranslatorTest extends TestCase
{
    use InteractionRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new AppealRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $appeal = \Base\Sdk\Interaction\Utils\MockFactory::generateAppeal(1);

        $expression['data']['id'] = $appeal->getId();
        $expression['data']['attributes']['title'] = $appeal->getTitle();
        $expression['data']['attributes']['content'] = $appeal->getContent();

        $expression['data']['attributes']['images'] = $appeal->getImages();
        $expression['data']['attributes']['status'] = $appeal->getStatus();

        $expression['data']['attributes']['identify'] = $appeal->getIdentify();
        $expression['data']['attributes']['appealType'] = $appeal->getType();
        $expression['data']['attributes']['name'] = $appeal->getName();
        $expression['data']['attributes']['contact'] = $appeal->getContact();

        $expression['data']['attributes']['certificates'] = $appeal->getCertificates();
        $expression['data']['attributes']['acceptStatus'] = $appeal->getAcceptStatus();

        $expression['data']['attributes']['createTime'] = $appeal->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $appeal->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $appeal->getUpdateTime();

        $appealObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\Appeal', $appealObject);
        $this->compareArrayAndObject($expression, $appealObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $appeal = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullAppeal', $appeal);
    }

    public function testObjectToArray()
    {
        $appeal = \Base\Sdk\Interaction\Utils\MockFactory::generateAppeal(1);
       
        $expression = $this->translator->objectToArray($appeal);
        
        $this->compareArrayAndObject($expression, $appeal);
    }

    public function testObjectToArrayFail()
    {
        $appeal = null;

        $expression = $this->translator->objectToArray($appeal);
        $this->assertEquals(array(), $expression);
    }
}
