<?php
namespace Base\Sdk\Interaction\Translator\Appeal;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionRestfulUtils;
use Base\Sdk\Crew\Model\Crew;

class UnAuditAppealRestfulTranslatorTest extends TestCase
{
    use InteractionRestfulUtils;

    private $translator;

    public function setUp()
    {
        Core::$container->set('crew', new Crew());

        $this->translator = new UnAuditAppealRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditAppeal = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditAppeal(1);

        $expression['data']['id'] = $unAuditAppeal->getId();
        $expression['data']['attributes']['title'] = $unAuditAppeal->getTitle();
        $expression['data']['attributes']['content'] = $unAuditAppeal->getContent();

        $expression['data']['attributes']['images'] = $unAuditAppeal->getImages();
        $expression['data']['attributes']['status'] = $unAuditAppeal->getStatus();

        $expression['data']['attributes']['identify'] = $unAuditAppeal->getIdentify();
        $expression['data']['attributes']['appealType'] = $unAuditAppeal->getType();
        $expression['data']['attributes']['name'] = $unAuditAppeal->getName();
        $expression['data']['attributes']['contact'] = $unAuditAppeal->getContact();

        $expression['data']['attributes']['certificates'] = $unAuditAppeal->getCertificates();
        $expression['data']['attributes']['acceptStatus'] = $unAuditAppeal->getAcceptStatus();

        $expression['data']['attributes']['createTime'] = $unAuditAppeal->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $unAuditAppeal->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $unAuditAppeal->getUpdateTime();

        $expression['data']['attributes']['rejectReason'] = $unAuditAppeal->getRejectReason();
        $expression['data']['attributes']['applyStatus'] = $unAuditAppeal->getApplyStatus();

        $unAuditAppealObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\UnAuditAppeal', $unAuditAppealObject);
        $this->compareArrayAndObject($expression, $unAuditAppealObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $unAuditAppeal = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullUnAuditAppeal', $unAuditAppeal);
    }

    public function testObjectToArray()
    {
        $unAuditAppeal = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditAppeal(1);

        $expression = $this->translator->objectToArray($unAuditAppeal);

        $this->compareArrayAndObject($expression, $unAuditAppeal);
    }

    public function testObjectToArrayFail()
    {
        $object = null;

        $expression = $this->translator->objectToArray($object);
        $this->assertEquals(array(), $expression);
    }
}
