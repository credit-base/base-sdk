<?php
namespace Base\Sdk\Interaction\Translator\Appeal;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionUtils;

class AppealTranslatorTest extends TestCase
{
    use InteractionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new AppealTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullAppeal', $objects);
    }

    public function testObjectToArray()
    {
        $appeal = \Base\Sdk\Interaction\Utils\MockFactory::generateAppeal(1);

        $expression = $this->translator->objectToArray($appeal);
        
        $this->compareArrayAndObject($expression, $appeal);
    }

    public function testObjectToArrayFail()
    {
        $appeal = null;

        $expression = $this->translator->objectToArray($appeal);
        $this->assertEquals(array(), $expression);
    }
}
