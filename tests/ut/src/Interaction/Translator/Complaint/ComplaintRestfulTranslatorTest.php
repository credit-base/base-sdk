<?php
namespace Base\Sdk\Interaction\Translator\Complaint;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionRestfulUtils;

class ComplaintRestfulTranslatorTest extends TestCase
{
    use InteractionRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ComplaintRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $complaint = \Base\Sdk\Interaction\Utils\MockFactory::generateComplaint(1);

        $expression['data']['id'] = $complaint->getId();
        $expression['data']['attributes']['title'] = $complaint->getTitle();
        $expression['data']['attributes']['content'] = $complaint->getContent();

        $expression['data']['attributes']['images'] = $complaint->getImages();
        $expression['data']['attributes']['status'] = $complaint->getStatus();

        $expression['data']['attributes']['identify'] = $complaint->getIdentify();
        $expression['data']['attributes']['complaintType'] = $complaint->getType();
        $expression['data']['attributes']['name'] = $complaint->getName();
        $expression['data']['attributes']['contact'] = $complaint->getContact();

        $expression['data']['attributes']['acceptStatus'] = $complaint->getAcceptStatus();

        $expression['data']['attributes']['createTime'] = $complaint->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $complaint->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $complaint->getUpdateTime();

        $complaintObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\Complaint', $complaintObject);
        $this->compareArrayAndObject($expression, $complaintObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $complaint = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullComplaint', $complaint);
    }

    public function testObjectToArray()
    {
        $complaint = \Base\Sdk\Interaction\Utils\MockFactory::generateComplaint(1);
       
        $expression = $this->translator->objectToArray($complaint);
        
        $this->compareArrayAndObject($expression, $complaint);
    }

    public function testObjectToArrayFail()
    {
        $complaint = null;

        $expression = $this->translator->objectToArray($complaint);
        $this->assertEquals(array(), $expression);
    }
}
