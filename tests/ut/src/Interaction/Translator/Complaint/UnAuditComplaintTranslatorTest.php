<?php
namespace Base\Sdk\Interaction\Translator\Complaint;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionUtils;

class UnAuditComplaintTranslatorTest extends TestCase
{
    use InteractionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditComplaintTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullUnAuditComplaint', $objects);
    }

    public function testObjectToArray()
    {
        $unAuditComplaint = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditComplaint(1);

        $expression = $this->translator->objectToArray($unAuditComplaint);
        
        $this->compareArrayAndObject($expression, $unAuditComplaint);
        $this->assertEquals($expression['rejectReason'], $unAuditComplaint->getRejectReason());
    }

    public function testObjectToArrayFail()
    {
        $unAuditComplaint = null;

        $expression = $this->translator->objectToArray($unAuditComplaint);
        $this->assertEquals(array(), $expression);
    }
}
