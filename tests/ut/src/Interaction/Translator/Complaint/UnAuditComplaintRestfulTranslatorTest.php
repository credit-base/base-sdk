<?php
namespace Base\Sdk\Interaction\Translator\Complaint;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionRestfulUtils;
use Base\Sdk\Crew\Model\Crew;

class UnAuditComplaintRestfulTranslatorTest extends TestCase
{
    use InteractionRestfulUtils;

    private $translator;

    public function setUp()
    {
        Core::$container->set('crew', new Crew());

        $this->translator = new UnAuditComplaintRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditComplaint = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditComplaint(1);

        $expression['data']['id'] = $unAuditComplaint->getId();
        $expression['data']['attributes']['title'] = $unAuditComplaint->getTitle();
        $expression['data']['attributes']['content'] = $unAuditComplaint->getContent();

        $expression['data']['attributes']['images'] = $unAuditComplaint->getImages();
        $expression['data']['attributes']['status'] = $unAuditComplaint->getStatus();

        $expression['data']['attributes']['identify'] = $unAuditComplaint->getIdentify();
        $expression['data']['attributes']['complaintType'] = $unAuditComplaint->getType();
        $expression['data']['attributes']['name'] = $unAuditComplaint->getName();
        $expression['data']['attributes']['contact'] = $unAuditComplaint->getContact();

        $expression['data']['attributes']['acceptStatus'] = $unAuditComplaint->getAcceptStatus();

        $expression['data']['attributes']['createTime'] = $unAuditComplaint->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $unAuditComplaint->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $unAuditComplaint->getUpdateTime();

        $expression['data']['attributes']['rejectReason'] = $unAuditComplaint->getRejectReason();
        $expression['data']['attributes']['applyStatus'] = $unAuditComplaint->getApplyStatus();

        $unAuditComplaintObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\UnAuditComplaint', $unAuditComplaintObject);
        $this->compareArrayAndObject($expression, $unAuditComplaintObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $unAuditComplaint = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullUnAuditComplaint', $unAuditComplaint);
    }

    public function testObjectToArray()
    {
        $unAuditComplaint = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditComplaint(1);

        $expression = $this->translator->objectToArray($unAuditComplaint);

        $this->compareArrayAndObject($expression, $unAuditComplaint);
    }

    public function testObjectToArrayFail()
    {
        $object = null;

        $expression = $this->translator->objectToArray($object);
        $this->assertEquals(array(), $expression);
    }
}
