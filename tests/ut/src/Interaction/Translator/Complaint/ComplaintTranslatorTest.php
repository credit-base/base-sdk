<?php
namespace Base\Sdk\Interaction\Translator\Complaint;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionUtils;

class ComplaintTranslatorTest extends TestCase
{
    use InteractionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ComplaintTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullComplaint', $objects);
    }

    public function testObjectToArray()
    {
        $complaint = \Base\Sdk\Interaction\Utils\MockFactory::generateComplaint(1);

        $expression = $this->translator->objectToArray($complaint);
        
        $this->compareArrayAndObject($expression, $complaint);
    }

    public function testObjectToArrayFail()
    {
        $complaint = null;

        $expression = $this->translator->objectToArray($complaint);
        $this->assertEquals(array(), $expression);
    }
}
