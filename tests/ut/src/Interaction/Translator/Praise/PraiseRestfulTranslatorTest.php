<?php
namespace Base\Sdk\Interaction\Translator\Praise;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\Interaction\Utils\InteractionRestfulUtils;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Translator\MemberRestfulTranslator;

use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Translator\Reply\ReplyRestfulTranslator;

class PraiseRestfulTranslatorTest extends TestCase
{
    use InteractionRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new PraiseRestfulTranslator();
        $this->childTranslator = new class extends PraiseRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }

            public function getMemberRestfulTranslator(): MemberRestfulTranslator
            {
                return parent::getMemberRestfulTranslator();
            }
        
            public function getReplyRestfulTranslator(): ReplyRestfulTranslator
            {
                return parent::getReplyRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetMemberRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Member\Translator\MemberRestfulTranslator',
            $this->childTranslator->getMemberRestfulTranslator()
        );
    }

    public function testGetReplyRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Translator\Reply\ReplyRestfulTranslator',
            $this->childTranslator->getReplyRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise(1);

        $expression['data']['id'] = $praise->getId();
        $expression['data']['attributes']['title'] = $praise->getTitle();
        $expression['data']['attributes']['content'] = $praise->getContent();

        $expression['data']['attributes']['images'] = $praise->getImages();
        $expression['data']['attributes']['status'] = $praise->getStatus();

        $expression['data']['attributes']['identify'] = $praise->getIdentify();
        $expression['data']['attributes']['praiseType'] = $praise->getType();
        $expression['data']['attributes']['name'] = $praise->getName();
        $expression['data']['attributes']['contact'] = $praise->getContact();

        $expression['data']['attributes']['subject'] = $praise->getSubject();
        $expression['data']['attributes']['acceptStatus'] = $praise->getAcceptStatus();

        $expression['data']['attributes']['createTime'] = $praise->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $praise->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $praise->getUpdateTime();

        $praiseObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\Praise', $praiseObject);
        $this->compareArrayAndObject($expression, $praiseObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $praise = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullPraise', $praise);
    }

    public function testObjectToArray()
    {
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise(1);

        $expression = $this->translator->objectToArray($praise);

        $this->compareArrayAndObject($expression, $praise);
    }

    public function testObjectToArrayFail()
    {
        $praise = null;

        $expression = $this->translator->objectToArray($praise);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['acceptUserGroup']['data'] 、
     * $relationships['member']['data']、$relationships['reply']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用3次, 分别用于处理 acceptUserGroup 、 member、reply
     * 且返回各自的数组 $acceptUserGroup 、 $member、$reply
     * 4. $this->getMemberRestfulTranslator()->arrayToObject 被调用一次, 入参是 $memberInfo, 出参是 $member
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $acceptUserGroupInfo, 出参是 $acceptUserGroup
     * 6. $this->getReplyRestfulTranslator()->arrayToObject 被调用一次, 入参是 $replyInfo, 出参是 $reply
     * 7. $praise->setMember 调用一次, 入参 $member
     * 8. $praise->setUserGroup 调用一次, 入参 $userGroup
     * 9. $praise->setReply 调用一次, 入参 $reply
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'acceptUserGroup'=>['data'=>'mockAcceptUserGroup'],
            'member'=>['data'=>'mockMember'],
            'reply'=>['data'=>'mockReply'],
        ];

        $memberInfo = ['mockMemberInfo'];
        $acceptUserGroupInfo = ['mockAcceptUserGroupInfo'];
        $replyInfo = ['mockReplyInfo'];

        $member = new Member();
        $acceptUserGroup = new UserGroup();
        $reply = new Reply();

        $translator = $this->getMockBuilder(PraiseRestfulTranslator::class)
                           ->setMethods([
                                'relationship',
                                'changeArrayFormat',
                                'getMemberRestfulTranslator',
                                'getUserGroupRestfulTranslator',
                                'getReplyRestfulTranslator',
                            ])
                           ->getMock();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用3次, 依次入参 member, acceptUserGroup,reply
        //依次返回 $memberInfo , $acceptUserGroupInfo, $replyInfo
        $translator->expects($this->exactly(3))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['acceptUserGroup']['data']],
                 [$relationships['member']['data']],
                 [$relationships['reply']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $acceptUserGroupInfo,
                $memberInfo,
                $replyInfo
            ));
  
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($memberInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($member);

        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($acceptUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($acceptUserGroup);
        $replyRestfulTranslator = $this->prophesize(ReplyRestfulTranslator::class);
        $replyRestfulTranslator->arrayToObject(Argument::exact($replyInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($reply);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getReplyRestfulTranslator')
            ->willReturn($replyRestfulTranslator->reveal());

        //揭示
        $praise = $translator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\Interaction\Model\Praise', $praise);
        $this->assertEquals($member, $praise->getMember());
        $this->assertEquals($acceptUserGroup, $praise->getAcceptUserGroup());
        $this->assertEquals($reply, $praise->getReply());
    }

    public function testArrayToObjectWithNotIssetIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>['reply'=>['data'=>["type"=> "replies",
                "id"=> "10"]]],
                'id' => 1
            ],
        ];
        $relationships = [
            'reply'=>['data'=>["type"=> "replies",
            "id"=> "10"]],
        ];

        $replyInfo = ['mockReplyInfo'];
        $reply = new Reply();

        $translator = $this->getMockBuilder(PraiseRestfulTranslator::class)
                           ->setMethods([
                                'relationship',
                                'changeArrayFormat',
                                'getReplyRestfulTranslator',
                            ])
                           ->getMock();


        $translator->expects($this->exactly(1))
            ->method('changeArrayFormat')
             ->with(
                 $relationships['reply']['data']
             )
            ->willReturn($replyInfo);
  
        $replyRestfulTranslator = $this->prophesize(ReplyRestfulTranslator::class);
        $replyRestfulTranslator->arrayToObject(Argument::exact($replyInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($reply);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getReplyRestfulTranslator')
            ->willReturn($replyRestfulTranslator->reveal());

        //揭示
        $praise = $translator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\Interaction\Model\Praise', $praise);
        $this->assertEquals($reply, $praise->getReply());
    }
}
