<?php
namespace Base\Sdk\Interaction\Translator\Praise;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionUtils;

use Base\Sdk\Member\Translator\MemberTranslator;
use Base\Sdk\UserGroup\Translator\UserGroupTranslator;

use Base\Sdk\Interaction\Translator\Reply\ReplyTranslator;

class PraiseTranslatorTest extends TestCase
{
    use InteractionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new PraiseTranslator();
        $this->childTranslator = new class extends PraiseTranslator
        {
            public function getUserGroupTranslator() : UserGroupTranslator
            {
                return parent::getUserGroupTranslator();
            }

            public function getMemberTranslator(): MemberTranslator
            {
                return new MemberTranslator();
            }
        
            public function getReplyTranslator(): ReplyTranslator
            {
                return new ReplyTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testGetUserGroupTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupTranslator',
            $this->childTranslator->getUserGroupTranslator()
        );
    }

    public function testGetMemberTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Member\Translator\MemberTranslator',
            $this->childTranslator->getMemberTranslator()
        );
    }

    public function testGetReplyTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Translator\Reply\ReplyTranslator',
            $this->childTranslator->getReplyTranslator()
        );
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullPraise', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise(1);

        $expression = $this->translator->objectToArray($praise);
        
        $this->compareArrayAndObject($expression, $praise);
    }

    public function testObjectToArrayFail()
    {
        $praise = null;

        $expression = $this->translator->objectToArray($praise);
        $this->assertEquals(array(), $expression);
    }
}
