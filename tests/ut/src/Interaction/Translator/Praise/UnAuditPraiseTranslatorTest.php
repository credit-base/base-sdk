<?php
namespace Base\Sdk\Interaction\Translator\Praise;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionUtils;

use Base\Sdk\Crew\Translator\CrewTranslator;

class UnAuditPraiseTranslatorTest extends TestCase
{
    use InteractionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditPraiseTranslator();
        $this->childTranslator = new class extends UnAuditPraiseTranslator
        {
            public function getCrewTranslator() : CrewTranslator
            {
                return parent::getCrewTranslator();
            }

            public function getAcceptStatusFormat(int $applyStatus)
            {
                return parent::getAcceptStatusFormat($applyStatus);
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testGetCrewTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewTranslator',
            $this->childTranslator->getCrewTranslator()
        );
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullUnAuditPraise', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise(1);

        $expression = $this->translator->objectToArray($unAuditPraise);
        
        $this->compareArrayAndObject($expression, $unAuditPraise);
        $this->assertEquals($expression['rejectReason'], $unAuditPraise->getRejectReason());
    }

    public function testObjectToArrayFail()
    {
        $unAuditPraise = null;

        $expression = $this->translator->objectToArray($unAuditPraise);
        $this->assertEquals(array(), $expression);
    }

    public function testGetAcceptStatusFormat()
    {
        $applyStatus = -2;
        $acceptStatus = 1;
        $result = $this->childTranslator->getAcceptStatusFormat($applyStatus);

        $this->assertEquals($acceptStatus, $result);
    }
}
