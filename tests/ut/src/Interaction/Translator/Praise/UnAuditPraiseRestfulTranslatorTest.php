<?php
namespace Base\Sdk\Interaction\Translator\Praise;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\Interaction\Utils\InteractionRestfulUtils;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Translator\MemberRestfulTranslator;

use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Translator\Reply\ReplyRestfulTranslator;

class UnAuditPraiseRestfulTranslatorTest extends TestCase
{
    use InteractionRestfulUtils;

    private $translator;

    public function setUp()
    {
        Core::$container->set('crew', new Crew());

        $this->translator = new UnAuditPraiseRestfulTranslator();
        $this->childTranslator = new class extends UnAuditPraiseRestfulTranslator
        {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise(1);

        $expression['data']['id'] = $unAuditPraise->getId();
        $expression['data']['attributes']['title'] = $unAuditPraise->getTitle();
        $expression['data']['attributes']['content'] = $unAuditPraise->getContent();

        $expression['data']['attributes']['images'] = $unAuditPraise->getImages();
        $expression['data']['attributes']['status'] = $unAuditPraise->getStatus();

        $expression['data']['attributes']['identify'] = $unAuditPraise->getIdentify();
        $expression['data']['attributes']['praiseType'] = $unAuditPraise->getType();
        $expression['data']['attributes']['name'] = $unAuditPraise->getName();
        $expression['data']['attributes']['contact'] = $unAuditPraise->getContact();

        $expression['data']['attributes']['subject'] = $unAuditPraise->getSubject();
        $expression['data']['attributes']['acceptStatus'] = $unAuditPraise->getAcceptStatus();

        $expression['data']['attributes']['createTime'] = $unAuditPraise->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $unAuditPraise->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $unAuditPraise->getUpdateTime();

        $expression['data']['attributes']['rejectReason'] = $unAuditPraise->getRejectReason();
        $expression['data']['attributes']['applyStatus'] = $unAuditPraise->getApplyStatus();

        $unAuditPraiseObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\UnAuditPraise', $unAuditPraiseObject);
        $this->compareArrayAndObject($expression, $unAuditPraiseObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $unAuditPraise = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullUnAuditPraise', $unAuditPraise);
    }

    public function testObjectToArray()
    {
        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise(1);

        $expression = $this->translator->objectToArray($unAuditPraise);

        $this->compareArrayAndObject($expression, $unAuditPraise);
    }

    public function testObjectToArrayFail()
    {
        $object = null;

        $expression = $this->translator->objectToArray($object);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['acceptUserGroup']['data'] 、
     * $relationships['member']['data']、$relationships['reply']['data']、
     * $relationships['applyCrew']['data']、$relationships['applyUserGroup']['data']、
     * $relationships['relation']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用6次, 分别用于处理 acceptUserGroup 、member、reply、
     * applyCrew、applyUserGroup、relation 且返回各自的数组 $acceptUserGroup 、 $member、$reply、$applyCrew、$applyUserGroup、$relation
     * 4. $this->getMemberRestfulTranslator()->arrayToObject 被调用一次, 入参是 $memberInfo, 出参是 $member
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $acceptUserGroupInfo, 出参是 $acceptUserGroup
     * 6. $this->getReplyRestfulTranslator()->arrayToObject 被调用一次, 入参是 $replyInfo, 出参是 $reply
     * 7. $this->getCrewRestfulTranslator()->arrayToObject 被调用一次, 入参是 $applyCrewInfo, 出参是 $applyCrew
     * 8. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $applyUserGroupInfo, 出参是 $applyUserGroup
     * 9. $this->getMemberRestfulTranslator()->arrayToObject 被调用一次, 入参是 $relationInfo, 出参是 $relation
     * 10. $unAuditPraise->setMember 调用一次, 入参 $member
     * 11. $unAuditPraise->setUserGroup 调用一次, 入参 $userGroup
     * 12. $unAuditPraise->setReply 调用一次, 入参 $reply
     * 13. $unAuditPraise->setApplyCrew 调用一次, 入参 $applyCrew
     * 14. $unAuditPraise->setApplyUserGroup 调用一次, 入参 $applyUserGroup
     * 15. $unAuditPraise->setMember 调用一次, 入参 $relation
     */
    /**
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'acceptUserGroup'=>['data'=>'mockAcceptUserGroup'],
            'member'=>['data'=>'mockMember'],
            'reply'=>['data'=>'mockReply'],
            'applyCrew'=>['data'=>'mockApplyCrew'],
            'applyUserGroup'=>['data'=>'mockApplyUserGroup'],
            'relation'=>['data'=>'mockRelation'],
        ];
       
        $memberInfo = ['mockMemberInfo'];
        $acceptUserGroupInfo = ['mockAcceptUserGroupInfo'];
        $replyInfo = ['mockReplyInfo'];
        $applyCrewInfo = ['mockApplyCrewInfo'];
        $applyUserGroupInfo = ['mockApplyUserGroupInfo'];
        $relationInfo = ['mockRelationInfo'];

        $member = new Member();
        $acceptUserGroup = new UserGroup();
        $reply = new Reply();
        $applyCrew = new Crew();
        $applyUserGroup = new UserGroup();
        $relation = new Member();

        $translator = $this->getMockBuilder(UnAuditPraiseRestfulTranslator::class)
                           ->setMethods([
                                'relationship',
                                'changeArrayFormat',
                                'getMemberRestfulTranslator',
                                'getUserGroupRestfulTranslator',
                                'getReplyRestfulTranslator',
                                'getCrewRestfulTranslator',
                            ])
                           ->getMock();

        //预言
        $translator->expects($this->exactly(2))
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用5次, 依次入参 member, acceptUserGroup,reply, applyUserGroup,applyCrew,relation
        //依次返回 $memberInfo , $acceptUserGroupInfo, $replyInfo,$applyUserGroupInfo, $applyCrewInfo,$relationInfo
        $translator->expects($this->exactly(6))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['acceptUserGroup']['data']],
                 [$relationships['member']['data']],
                 [$relationships['reply']['data']],
                 [$relationships['applyCrew']['data']],
                 [$relationships['applyUserGroup']['data']],
                 [$relationships['relation']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $acceptUserGroupInfo,
                $memberInfo,
                $replyInfo,
                $applyCrewInfo,
                $applyUserGroupInfo,
                $relationInfo
            ));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);

        $crewRestfulTranslator->arrayToObject(Argument::exact($applyCrewInfo))
        ->shouldBeCalledTimes(1)
        ->willReturn($applyCrew);

        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($acceptUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($acceptUserGroup);

        $userGroupRestfulTranslator->arrayToObject(Argument::exact($applyUserGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyUserGroup);

        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($memberInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($member);

        $replyRestfulTranslator = $this->prophesize(ReplyRestfulTranslator::class);
        $replyRestfulTranslator->arrayToObject(Argument::exact($replyInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($reply);

        $memberRestfulTranslator->arrayToObject(Argument::exact($relationInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($relation);

        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $translator->expects($this->exactly(2))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());
        $translator->expects($this->exactly(2))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());
        $translator->expects($this->exactly(1))
            ->method('getReplyRestfulTranslator')
            ->willReturn($replyRestfulTranslator->reveal());
        
        //揭示
        $unAuditPraise = $translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\UnAuditPraise', $unAuditPraise);
        $this->assertEquals($member, $unAuditPraise->getMember());
        $this->assertEquals($applyCrew, $unAuditPraise->getApplyCrew());
        $this->assertEquals($acceptUserGroup, $unAuditPraise->getAcceptUserGroup());
        $this->assertEquals($reply, $unAuditPraise->getReply());
        $this->assertEquals($applyUserGroup, $unAuditPraise->getApplyUserGroup());
        $this->assertEquals($relation, $unAuditPraise->getMember());
    }
}
