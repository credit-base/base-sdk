<?php
namespace Base\Sdk\Interaction\Translator\Qa;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionRestfulUtils;

class QaRestfulTranslatorTest extends TestCase
{
    use InteractionRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new QaRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $qaObject = \Base\Sdk\Interaction\Utils\MockFactory::generateQa(1);

        $expression['data']['id'] = $qaObject->getId();
        $expression['data']['attributes']['title'] = $qaObject->getTitle();
        $expression['data']['attributes']['content'] = $qaObject->getContent();

        $expression['data']['attributes']['images'] = $qaObject->getImages();
        $expression['data']['attributes']['status'] = $qaObject->getStatus();

        $expression['data']['attributes']['identify'] = $qaObject->getIdentify();
        $expression['data']['attributes']['name'] = $qaObject->getName();
        $expression['data']['attributes']['contact'] = $qaObject->getContact();

        $expression['data']['attributes']['acceptStatus'] = $qaObject->getAcceptStatus();

        $expression['data']['attributes']['createTime'] = $qaObject->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $qaObject->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $qaObject->getUpdateTime();

        $qaObjectObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\Qa', $qaObjectObject);
        $this->compareArrayAndObject($expression, $qaObjectObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $qaObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullQa', $qaObject);
    }

    public function testObjectToArray()
    {
        $qaObject = \Base\Sdk\Interaction\Utils\MockFactory::generateQa(1);
       
        $expression = $this->translator->objectToArray($qaObject);
        
        $this->compareArrayAndObject($expression, $qaObject);
    }

    public function testObjectToArrayFail()
    {
        $qaObject = null;

        $expression = $this->translator->objectToArray($qaObject);
        $this->assertEquals(array(), $expression);
    }
}
