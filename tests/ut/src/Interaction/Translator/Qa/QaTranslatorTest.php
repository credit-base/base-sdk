<?php
namespace Base\Sdk\Interaction\Translator\Qa;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionUtils;

class QaTranslatorTest extends TestCase
{
    use InteractionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new QaTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullQa', $objects);
    }

    public function testObjectToArray()
    {
        $qaObject = \Base\Sdk\Interaction\Utils\MockFactory::generateQa(1);

        $expression = $this->translator->objectToArray($qaObject);
        
        $this->compareArrayAndObject($expression, $qaObject);
    }

    public function testObjectToArrayFail()
    {
        $qaObject = null;

        $expression = $this->translator->objectToArray($qaObject);
        $this->assertEquals(array(), $expression);
    }
}
