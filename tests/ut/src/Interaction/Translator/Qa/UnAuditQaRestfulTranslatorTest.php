<?php
namespace Base\Sdk\Interaction\Translator\Qa;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionRestfulUtils;
use Base\Sdk\Crew\Model\Crew;

class UnAuditQaRestfulTranslatorTest extends TestCase
{
    use InteractionRestfulUtils;

    private $translator;

    public function setUp()
    {
        Core::$container->set('crew', new Crew());

        $this->translator = new UnAuditQaRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $unAuditQa = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditQa(1);

        $expression['data']['id'] = $unAuditQa->getId();
        $expression['data']['attributes']['title'] = $unAuditQa->getTitle();
        $expression['data']['attributes']['content'] = $unAuditQa->getContent();

        $expression['data']['attributes']['images'] = $unAuditQa->getImages();
        $expression['data']['attributes']['status'] = $unAuditQa->getStatus();

        $expression['data']['attributes']['identify'] = $unAuditQa->getIdentify();

        $expression['data']['attributes']['name'] = $unAuditQa->getName();
        $expression['data']['attributes']['contact'] = $unAuditQa->getContact();

        $expression['data']['attributes']['acceptStatus'] = $unAuditQa->getAcceptStatus();

        $expression['data']['attributes']['createTime'] = $unAuditQa->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $unAuditQa->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $unAuditQa->getUpdateTime();

        $expression['data']['attributes']['rejectReason'] = $unAuditQa->getRejectReason();
        $expression['data']['attributes']['applyStatus'] = $unAuditQa->getApplyStatus();

        $unAuditQaObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\UnAuditQa', $unAuditQaObject);
        $this->compareArrayAndObject($expression, $unAuditQaObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $unAuditQa = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullUnAuditQa', $unAuditQa);
    }

    public function testObjectToArray()
    {
        $unAuditQa = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditQa(1);

        $expression = $this->translator->objectToArray($unAuditQa);

        $this->compareArrayAndObject($expression, $unAuditQa);
    }

    public function testObjectToArrayFail()
    {
        $object = null;

        $expression = $this->translator->objectToArray($object);
        $this->assertEquals(array(), $expression);
    }
}
