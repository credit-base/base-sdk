<?php
namespace Base\Sdk\Interaction\Translator\Qa;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\InteractionUtils;

class UnAuditQaTranslatorTest extends TestCase
{
    use InteractionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditQaTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullUnAuditQa', $objects);
    }

    public function testObjectToArray()
    {
        $unAuditQa = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditQa(1);

        $expression = $this->translator->objectToArray($unAuditQa);
        
        $this->compareArrayAndObject($expression, $unAuditQa);
        $this->assertEquals($expression['rejectReason'], $unAuditQa->getRejectReason());
    }

    public function testObjectToArrayFail()
    {
        $unAuditQa = null;

        $expression = $this->translator->objectToArray($unAuditQa);
        $this->assertEquals(array(), $expression);
    }
}
