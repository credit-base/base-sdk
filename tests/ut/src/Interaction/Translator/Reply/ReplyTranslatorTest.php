<?php
namespace Base\Sdk\Interaction\Translator\Reply;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Utils\ReplyUtils;
use Base\Sdk\Crew\Translator\CrewTranslator;

class ReplyTranslatorTest extends TestCase
{
    use ReplyUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ReplyTranslator();
        $this->childTranslator = new class extends ReplyTranslator
        {
            public function getCrewTranslator() : CrewTranslator
            {
                return parent::getCrewTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testGetCrewTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewTranslator',
            $this->childTranslator->getCrewTranslator()
        );
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullReply', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $reply = \Base\Sdk\Interaction\Utils\ReplyMockFactory::generateReply(1);

        $expression = $this->translator->objectToArray($reply);
        
        $this->compareArrayAndObject($expression, $reply);
    }

    public function testObjectToArrayFail()
    {
        $reply = null;

        $expression = $this->translator->objectToArray($reply);
        $this->assertEquals(array(), $expression);
    }
}
