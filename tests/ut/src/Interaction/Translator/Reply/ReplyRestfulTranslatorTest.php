<?php
namespace Base\Sdk\Interaction\Translator\Reply;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\Interaction\Utils\ReplyRestfulUtils;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;
use Base\Sdk\Crew\Model\Crew;

class ReplyRestfulTranslatorTest extends TestCase
{
    use ReplyRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ReplyRestfulTranslator();
        $this->childTranslator = new class extends ReplyRestfulTranslator
        {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $reply = \Base\Sdk\Interaction\Utils\ReplyMockFactory::generateReply(1);

        $expression['data']['id'] = $reply->getId();
        $expression['data']['attributes']['content'] = $reply->getContent();
        $expression['data']['attributes']['images'] = $reply->getImages();

        $expression['data']['attributes']['admissibility'] = $reply->getAdmissibility();
        $expression['data']['attributes']['status'] = $reply->getStatus();

        $expression['data']['attributes']['createTime'] = $reply->getCreateTime();
        $expression['data']['attributes']['statusTime'] = $reply->getStatusTime();
        $expression['data']['attributes']['updateTime'] = $reply->getUpdateTime();

        $replyObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\Reply', $replyObject);
        $this->compareArrayAndObject($expression, $replyObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $reply = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\Interaction\Model\NullReply', $reply);
    }

    public function testObjectToArray()
    {
        $reply = \Base\Sdk\Interaction\Utils\ReplyMockFactory::generateReply(1);

        $expression = $this->translator->objectToArray($reply);

        $this->compareArrayAndObject($expression, $reply);
    }

    public function testObjectToArrayFail()
    {
        $reply = null;

        $expression = $this->translator->objectToArray($reply);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['crew']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用1次, 用于处理 crew 且返回数组 $crew
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用一次, 入参是 $crewInfo, 出参是 $crew
     * 5. $reply->setCrew 调用一次, 入参 $crew
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew']
        ];

        $crewInfo = ['mockCrewInfo'];
        $crew = new Crew();

        $replyTranslator = $this->getMockBuilder(ReplyRestfulTranslator::class)
                           ->setMethods([
                                'relationship',
                                'changeArrayFormat',
                                'getCrewRestfulTranslator',
                            ])
                           ->getMock();

        //预言
        $replyTranslator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用2次, 依次入参 crew, publishUserGroup
        //依次返回 $crewInfo 和 $userGroupInfo
        $replyTranslator->expects($this->exactly(1))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']]
             )
            ->will($this->onConsecutiveCalls($crewInfo));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        //绑定
        $replyTranslator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        //揭示
        $reply = $replyTranslator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\Interaction\Model\Reply', $reply);
        $this->assertEquals($crew, $reply->getCrew());
    }
}
