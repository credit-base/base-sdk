<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditAppeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Base\Sdk\Interaction\Repository\UnAuditAppealRepository;

class RejectUnAuditAppealCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRejectUnAuditAppealCommandHandler();
    }

    public function testExtendsRejectCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->stub
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditAppealRepository',
            $this->stub->getPublicRepository()
        );
    }

    public function testFetchIApplyObject()
    {
        $commandHandler = $this->getMockBuilder(MockRejectUnAuditAppealCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $unAuditAppeal = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditAppeal($id);

        $repository = $this->prophesize(UnAuditAppealRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditAppeal);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditAppeal);
    }
}
