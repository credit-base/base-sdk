<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditAppeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\UnAuditAppeal;
use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Repository\UnAuditAppealRepository;
use Base\Sdk\Interaction\Command\UnAuditAppeal\ResubmitUnAuditAppealCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class ResubmitUnAuditAppealCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitUnAuditAppealCommandHandler::class)
            ->setMethods(['getRepository',])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitUnAuditAppealCommandHandler::class)
        ->setMethods(['getRepository'])
        ->getMock();

        $fakerData = $this->getReplyCommonData();
        $id = 1;
        
        $command = new ResubmitUnAuditAppealCommand(
            $fakerData,
            $id
        );

        $unAuditAppeal = $this->prophesize(UnAuditAppeal::class);

        $reply = $this->prophesize(Reply::class);

        $unAuditAppeal->getReply()
        ->shouldBeCalledTimes(1)->willReturn($reply->reveal());

        $reply->setContent(Argument::exact($fakerData['content']))->shouldBeCalledTimes(1);
        $reply->setImages(Argument::exact($fakerData['images']))->shouldBeCalledTimes(1);
        $reply->setAdmissibility(Argument::exact($fakerData['admissibility']))->shouldBeCalledTimes(1);

        $unAuditAppeal->setReply(Argument::exact($reply->reveal()))->shouldBeCalledTimes(1);
        
        $unAuditAppeal->resubmit()->shouldBeCalledTimes(1)->willReturn(true);

        $unAuditAppealRepository = $this->prophesize(UnAuditAppealRepository::class);
        $unAuditAppealRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($unAuditAppeal->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($unAuditAppealRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
