<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditComplaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Repository\UnAuditComplaintRepository;

class ApproveUnAuditComplaintCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveUnAuditComplaintCommandHandler::class)
            ->setMethods(['fetchUnAuditComplaint'])
            ->getMock();
    }

    public function testFetchIApplyObject()
    {
        $commandHandler = $this->getMockBuilder(MockApproveUnAuditComplaintCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $unAuditComplaint = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditComplaint($id);

        $repository = $this->prophesize(UnAuditComplaintRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditComplaint);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditComplaint);
    }

    public function testExtendsApproveCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }
}
