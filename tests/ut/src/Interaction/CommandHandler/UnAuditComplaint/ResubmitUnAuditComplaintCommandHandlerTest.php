<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditComplaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\UnAuditComplaint;
use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Repository\UnAuditComplaintRepository;
use Base\Sdk\Interaction\Command\UnAuditComplaint\ResubmitUnAuditComplaintCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class ResubmitUnAuditComplaintCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitUnAuditComplaintCommandHandler::class)
            ->setMethods(['getRepository',])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitUnAuditComplaintCommandHandler::class)
        ->setMethods(['getRepository'])
        ->getMock();

        $fakerData = $this->getReplyCommonData();
        $id = 1;
        
        $command = new ResubmitUnAuditComplaintCommand(
            $fakerData,
            $id
        );

        $unAuditComplaint = $this->prophesize(UnAuditComplaint::class);

        $reply = $this->prophesize(Reply::class);

        $unAuditComplaint->getReply()
        ->shouldBeCalledTimes(1)->willReturn($reply->reveal());

        $reply->setContent(Argument::exact($fakerData['content']))->shouldBeCalledTimes(1);
        $reply->setImages(Argument::exact($fakerData['images']))->shouldBeCalledTimes(1);
        $reply->setAdmissibility(Argument::exact($fakerData['admissibility']))->shouldBeCalledTimes(1);

        $unAuditComplaint->setReply(Argument::exact($reply->reveal()))->shouldBeCalledTimes(1);
        
        $unAuditComplaint->resubmit()->shouldBeCalledTimes(1)->willReturn(true);

        $unAuditComplaintRepository = $this->prophesize(UnAuditComplaintRepository::class);
        $unAuditComplaintRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($unAuditComplaint->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($unAuditComplaintRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
