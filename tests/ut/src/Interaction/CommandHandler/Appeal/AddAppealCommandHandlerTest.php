<?php
namespace Base\Sdk\Interaction\CommandHandler\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Interaction\Command\Appeal\AddAppealCommand;
use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AddAppealCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddAppealCommandHandler::class)
            ->setMethods(['getAppeal', 'getUserGroupRepository', 'getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetAppeal()
    {
        $commandHandler = new MockAddAppealCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Model\Appeal',
            $commandHandler->getPublicAppeal()
        );
    }

    public function initialExecute()
    {
        $fakerData = $this->getAddCommonData();
        
        $command = new AddAppealCommand(
            $fakerData['title'],
            $fakerData['content'],
            $fakerData['name'],
            $fakerData['identify'],
            $fakerData['contact'],
            $fakerData['certificates'],
            $fakerData['images'],
            $fakerData['type'],
            $fakerData['acceptUserGroupId']
        );
        
        $acceptUserGroup = \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $acceptUserGroupId = $fakerData['acceptUserGroupId'];
        $acceptUserGroupRepository = $this->prophesize(UserGroupRepository::class);
        $acceptUserGroupRepository->fetchOne(Argument::exact($acceptUserGroupId))
            ->shouldBeCalledTimes(1)->willReturn($acceptUserGroup);
        $this->commandHandler->expects($this->exactly(1))
                    ->method('getUserGroupRepository')
                    ->willReturn($acceptUserGroupRepository->reveal());

        $appeal = $this->prophesize(Appeal::class);
        
        $this->commandHandler->expects($this->exactly(1))
            ->method('getAppeal')
            ->willReturn($appeal->reveal());

        $appeal->setAcceptUserGroup(Argument::exact($acceptUserGroup))->shouldBeCalledTimes(1);
        $appeal->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        $appeal->setContent(Argument::exact($command->content))->shouldBeCalledTimes(1);
        $appeal->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $appeal->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $appeal->setContact(Argument::exact($command->contact))->shouldBeCalledTimes(1);
        $appeal->setCertificates(Argument::exact($command->certificates))->shouldBeCalledTimes(1);
        $appeal->setImages(Argument::exact($command->images))->shouldBeCalledTimes(1);
        $appeal->setType(Argument::exact($command->type))->shouldBeCalledTimes(1);
        $appeal->add()->shouldBeCalledTimes(1)->willReturn(true);

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }
}
