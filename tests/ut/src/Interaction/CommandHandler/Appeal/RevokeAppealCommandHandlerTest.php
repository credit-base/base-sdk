<?php
namespace Base\Sdk\Interaction\CommandHandler\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Repository\AppealRepository;

class RevokeAppealCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRevokeAppealCommandHandler();
    }

    public function testFetchIModifyStatusObject()
    {
        $commandHandler = $this->getMockBuilder(MockRevokeAppealCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $appeal = \Base\Sdk\Interaction\Utils\MockFactory::generateAppeal($id);

        $repository = $this->prophesize(AppealRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($appeal);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIModifyStatusObject($id);

        $this->assertEquals($result, $appeal);
    }

    public function testExtendsRevokeCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RevokeCommandHandler',
            $this->stub
        );
    }
}
