<?php
namespace Base\Sdk\Interaction\CommandHandler\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Repository\AppealRepository;
use Base\Sdk\Interaction\Command\Appeal\AcceptAppealCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AcceptAppealCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptAppealCommandHandler::class)
            ->setMethods()
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    public function testGetRepository()
    {
        $commandHandler = new MockAddAppealCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\AppealRepository',
            $commandHandler->getPublicRepository()
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptAppealCommandHandler::class)
        ->setMethods(['getRepository'])
        ->getMock();

        $fakerData = $this->getReplyCommonData();
        $id = 1;
        
        $command = new AcceptAppealCommand(
            $fakerData,
            $id
        );

        $appeal = $this->prophesize(Appeal::class);

        $reply = $this->prophesize(Reply::class);

        $appeal->getReply()
        ->shouldBeCalledTimes(1)->willReturn($reply->reveal());

        $reply->setContent(Argument::exact($fakerData['content']))->shouldBeCalledTimes(1);
        $reply->setImages(Argument::exact($fakerData['images']))->shouldBeCalledTimes(1);
        $reply->setAdmissibility(Argument::exact($fakerData['admissibility']))->shouldBeCalledTimes(1);

        $appeal->setReply(Argument::exact($reply->reveal()))->shouldBeCalledTimes(1);
        
        $appeal->accept()->shouldBeCalledTimes(1)->willReturn(true);

        $appealRepository = $this->prophesize(AppealRepository::class);
        $appealRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($appeal->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($appealRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
