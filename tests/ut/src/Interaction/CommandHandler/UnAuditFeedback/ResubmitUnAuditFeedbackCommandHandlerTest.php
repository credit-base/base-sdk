<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditFeedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\UnAuditFeedback;
use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Repository\UnAuditFeedbackRepository;
use Base\Sdk\Interaction\Command\UnAuditFeedback\ResubmitUnAuditFeedbackCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class ResubmitUnAuditFeedbackCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitUnAuditFeedbackCommandHandler::class)
            ->setMethods(['getRepository',])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitUnAuditFeedbackCommandHandler::class)
        ->setMethods(['getRepository'])
        ->getMock();

        $fakerData = $this->getReplyCommonData();
        $id = 1;
        
        $command = new ResubmitUnAuditFeedbackCommand(
            $fakerData,
            $id
        );

        $unAuditFeedback = $this->prophesize(UnAuditFeedback::class);

        $reply = $this->prophesize(Reply::class);

        $unAuditFeedback->getReply()
        ->shouldBeCalledTimes(1)->willReturn($reply->reveal());

        $reply->setContent(Argument::exact($fakerData['content']))->shouldBeCalledTimes(1);
        $reply->setImages(Argument::exact($fakerData['images']))->shouldBeCalledTimes(1);
        $reply->setAdmissibility(Argument::exact($fakerData['admissibility']))->shouldBeCalledTimes(1);

        $unAuditFeedback->setReply(Argument::exact($reply->reveal()))->shouldBeCalledTimes(1);
        
        $unAuditFeedback->resubmit()->shouldBeCalledTimes(1)->willReturn(true);

        $unAuditFeedbackRepository = $this->prophesize(UnAuditFeedbackRepository::class);
        $unAuditFeedbackRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($unAuditFeedback->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($unAuditFeedbackRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
