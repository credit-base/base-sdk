<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditFeedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Base\Sdk\Interaction\Repository\UnAuditFeedbackRepository;

class RejectUnAuditFeedbackCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRejectUnAuditFeedbackCommandHandler();
    }

    public function testExtendsRejectCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->stub
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditFeedbackRepository',
            $this->stub->getPublicRepository()
        );
    }

    public function testFetchIApplyObject()
    {
        $commandHandler = $this->getMockBuilder(MockRejectUnAuditFeedbackCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $unAuditFeedback = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditFeedback($id);

        $repository = $this->prophesize(UnAuditFeedbackRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditFeedback);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditFeedback);
    }
}
