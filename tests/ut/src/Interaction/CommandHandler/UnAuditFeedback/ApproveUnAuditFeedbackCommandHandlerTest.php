<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditFeedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Repository\UnAuditFeedbackRepository;

class ApproveUnAuditFeedbackCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveUnAuditFeedbackCommandHandler::class)
            ->setMethods(['fetchUnAuditFeedback'])
            ->getMock();
    }

    public function testFetchIApplyObject()
    {
        $commandHandler = $this->getMockBuilder(MockApproveUnAuditFeedbackCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $unAuditFeedback = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditFeedback($id);

        $repository = $this->prophesize(UnAuditFeedbackRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditFeedback);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditFeedback);
    }

    public function testExtendsApproveCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }
}
