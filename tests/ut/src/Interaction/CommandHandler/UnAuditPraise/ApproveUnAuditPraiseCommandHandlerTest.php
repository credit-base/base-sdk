<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditPraise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Repository\UnAuditPraiseRepository;

class ApproveUnAuditPraiseCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveUnAuditPraiseCommandHandler::class)
            ->setMethods(['fetchUnAuditPraise'])
            ->getMock();
    }

    public function testFetchIApplyObject()
    {
        $commandHandler = $this->getMockBuilder(MockApproveUnAuditPraiseCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise($id);

        $repository = $this->prophesize(UnAuditPraiseRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditPraise);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditPraise);
    }

    public function testExtendsApproveCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }
}
