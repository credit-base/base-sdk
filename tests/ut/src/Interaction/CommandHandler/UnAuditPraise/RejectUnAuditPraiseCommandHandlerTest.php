<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditPraise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Base\Sdk\Interaction\Repository\UnAuditPraiseRepository;

class RejectUnAuditPraiseCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRejectUnAuditPraiseCommandHandler();
    }

    public function testExtendsRejectCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->stub
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditPraiseRepository',
            $this->stub->getPublicRepository()
        );
    }

    public function testFetchIApplyObject()
    {
        $commandHandler = $this->getMockBuilder(MockRejectUnAuditPraiseCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $unAuditPraise = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditPraise($id);

        $repository = $this->prophesize(UnAuditPraiseRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditPraise);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditPraise);
    }
}
