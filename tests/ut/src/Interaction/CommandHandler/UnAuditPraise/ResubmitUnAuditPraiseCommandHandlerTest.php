<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditPraise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\UnAuditPraise;
use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Repository\UnAuditPraiseRepository;
use Base\Sdk\Interaction\Command\UnAuditPraise\ResubmitUnAuditPraiseCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class ResubmitUnAuditPraiseCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitUnAuditPraiseCommandHandler::class)
            ->setMethods(['getRepository',])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitUnAuditPraiseCommandHandler::class)
        ->setMethods(['getRepository'])
        ->getMock();

        $fakerData = $this->getReplyCommonData();
        $id = 1;
        
        $command = new ResubmitUnAuditPraiseCommand(
            $fakerData,
            $id
        );

        $unAuditPraise = $this->prophesize(UnAuditPraise::class);

        $reply = $this->prophesize(Reply::class);

        $unAuditPraise->getReply()
        ->shouldBeCalledTimes(1)->willReturn($reply->reveal());

        $reply->setContent(Argument::exact($fakerData['content']))->shouldBeCalledTimes(1);
        $reply->setImages(Argument::exact($fakerData['images']))->shouldBeCalledTimes(1);
        $reply->setAdmissibility(Argument::exact($fakerData['admissibility']))->shouldBeCalledTimes(1);

        $unAuditPraise->setReply(Argument::exact($reply->reveal()))->shouldBeCalledTimes(1);
        
        $unAuditPraise->resubmit()->shouldBeCalledTimes(1)->willReturn(true);

        $unAuditPraiseRepository = $this->prophesize(UnAuditPraiseRepository::class);
        $unAuditPraiseRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($unAuditPraise->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($unAuditPraiseRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
