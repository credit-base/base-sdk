<?php
namespace Base\Sdk\Interaction\CommandHandler\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Complaint;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Interaction\Command\Complaint\AddComplaintCommand;
use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AddComplaintCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddComplaintCommandHandler::class)
            ->setMethods(['getComplaint', 'getUserGroupRepository', 'getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetComplaint()
    {
        $commandHandler = new MockAddComplaintCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Model\Complaint',
            $commandHandler->getPublicComplaint()
        );
    }

    public function initialExecute()
    {
        $fakerData = $this->getAddCommonData();
        
        $command = new AddComplaintCommand(
            $fakerData['title'],
            $fakerData['content'],
            $fakerData['name'],
            $fakerData['identify'],
            $fakerData['contact'],
            $fakerData['subject'],
            $fakerData['images'],
            $fakerData['type'],
            $fakerData['acceptUserGroupId']
        );
        
        $acceptUserGroup = \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $acceptUserGroupId = $fakerData['acceptUserGroupId'];
        $acceptUserGroupRepository = $this->prophesize(UserGroupRepository::class);
        $acceptUserGroupRepository->fetchOne(Argument::exact($acceptUserGroupId))
            ->shouldBeCalledTimes(1)->willReturn($acceptUserGroup);
        $this->commandHandler->expects($this->exactly(1))
                    ->method('getUserGroupRepository')
                    ->willReturn($acceptUserGroupRepository->reveal());

        $complaint = $this->prophesize(Complaint::class);
        
        $this->commandHandler->expects($this->exactly(1))
            ->method('getComplaint')
            ->willReturn($complaint->reveal());

        $complaint->setAcceptUserGroup(Argument::exact($acceptUserGroup))->shouldBeCalledTimes(1);
        $complaint->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        $complaint->setContent(Argument::exact($command->content))->shouldBeCalledTimes(1);
        $complaint->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $complaint->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $complaint->setContact(Argument::exact($command->contact))->shouldBeCalledTimes(1);
        $complaint->setSubject(Argument::exact($command->subject))->shouldBeCalledTimes(1);
        $complaint->setImages(Argument::exact($command->images))->shouldBeCalledTimes(1);
        $complaint->setType(Argument::exact($command->type))->shouldBeCalledTimes(1);
        $complaint->add()->shouldBeCalledTimes(1)->willReturn(true);

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }
}
