<?php
namespace Base\Sdk\Interaction\CommandHandler\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Complaint;
use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Repository\ComplaintRepository;
use Base\Sdk\Interaction\Command\Complaint\AcceptComplaintCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AcceptComplaintCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptComplaintCommandHandler::class)
            ->setMethods()
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    public function testGetRepository()
    {
        $commandHandler = new MockAddComplaintCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\ComplaintRepository',
            $commandHandler->getPublicRepository()
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptComplaintCommandHandler::class)
        ->setMethods(['getRepository'])
        ->getMock();

        $fakerData = $this->getReplyCommonData();
        $id = 1;
        
        $command = new AcceptComplaintCommand(
            $fakerData,
            $id
        );

        $complaint = $this->prophesize(Complaint::class);

        $reply = $this->prophesize(Reply::class);

        $complaint->getReply()
        ->shouldBeCalledTimes(1)->willReturn($reply->reveal());

        $reply->setContent(Argument::exact($fakerData['content']))->shouldBeCalledTimes(1);
        $reply->setImages(Argument::exact($fakerData['images']))->shouldBeCalledTimes(1);
        $reply->setAdmissibility(Argument::exact($fakerData['admissibility']))->shouldBeCalledTimes(1);

        $complaint->setReply(Argument::exact($reply->reveal()))->shouldBeCalledTimes(1);
        
        $complaint->accept()->shouldBeCalledTimes(1)->willReturn(true);

        $complaintRepository = $this->prophesize(ComplaintRepository::class);
        $complaintRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($complaint->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($complaintRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
