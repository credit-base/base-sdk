<?php
namespace Base\Sdk\Interaction\CommandHandler\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Repository\ComplaintRepository;

class RevokeComplaintCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRevokeComplaintCommandHandler();
    }

    public function testFetchIModifyStatusObject()
    {
        $commandHandler = $this->getMockBuilder(MockRevokeComplaintCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $complaint = \Base\Sdk\Interaction\Utils\MockFactory::generateComplaint($id);

        $repository = $this->prophesize(ComplaintRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($complaint);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIModifyStatusObject($id);

        $this->assertEquals($result, $complaint);
    }

    public function testExtendsRevokeCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RevokeCommandHandler',
            $this->stub
        );
    }
}
