<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Repository\QaRepository;
use Base\Sdk\Interaction\Command\Qa\PublishQaCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class PublishQaCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(PublishQaCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $id = 1;
        
        $command = new PublishQaCommand($id);

        $qaObject = $this->prophesize(Qa::class);
        
        $qaObject->publish()->shouldBeCalledTimes(1)->willReturn(true);

        $qaObjectRepository = $this->prophesize(QaRepository::class);
        $qaObjectRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($qaObject->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($qaObjectRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
