<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Interaction\Command\Qa\AddQaCommand;
use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AddQaCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddQaCommandHandler::class)
            ->setMethods(['getQa', 'getUserGroupRepository', 'getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetQa()
    {
        $commandHandler = new MockAddQaCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Model\Qa',
            $commandHandler->getPublicQa()
        );
    }

    public function initialExecute()
    {
        $fakerData = $this->getAddCommonData();
        
        $command = new AddQaCommand(
            $fakerData['title'],
            $fakerData['content'],
            $fakerData['acceptUserGroupId']
        );
        
        $acceptUserGroup = \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $acceptUserGroupId = $fakerData['acceptUserGroupId'];
        $acceptUserGroupRepository = $this->prophesize(UserGroupRepository::class);
        $acceptUserGroupRepository->fetchOne(Argument::exact($acceptUserGroupId))
            ->shouldBeCalledTimes(1)->willReturn($acceptUserGroup);
        $this->commandHandler->expects($this->exactly(1))
                    ->method('getUserGroupRepository')
                    ->willReturn($acceptUserGroupRepository->reveal());

        $qaObject = $this->prophesize(Qa::class);
        
        $this->commandHandler->expects($this->exactly(1))
            ->method('getQa')
            ->willReturn($qaObject->reveal());

        $qaObject->setAcceptUserGroup(Argument::exact($acceptUserGroup))->shouldBeCalledTimes(1);
        $qaObject->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        $qaObject->setContent(Argument::exact($command->content))->shouldBeCalledTimes(1);
       
        $qaObject->add()->shouldBeCalledTimes(1)->willReturn(true);

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }
}
