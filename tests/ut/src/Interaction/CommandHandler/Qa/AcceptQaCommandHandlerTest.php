<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Repository\QaRepository;
use Base\Sdk\Interaction\Command\Qa\AcceptQaCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AcceptQaCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptQaCommandHandler::class)
            ->setMethods()
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    public function testGetRepository()
    {
        $commandHandler = new MockAddQaCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\QaRepository',
            $commandHandler->getPublicRepository()
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptQaCommandHandler::class)
        ->setMethods(['getRepository'])
        ->getMock();

        $fakerData = $this->getReplyCommonData();
        $id = 1;
        
        $command = new AcceptQaCommand(
            $fakerData,
            $id
        );

        $qaObject = $this->prophesize(Qa::class);

        $reply = $this->prophesize(Reply::class);

        $qaObject->getReply()
        ->shouldBeCalledTimes(1)->willReturn($reply->reveal());

        $reply->setContent(Argument::exact($fakerData['content']))->shouldBeCalledTimes(1);
        $reply->setImages(Argument::exact($fakerData['images']))->shouldBeCalledTimes(1);
        $reply->setAdmissibility(Argument::exact($fakerData['admissibility']))->shouldBeCalledTimes(1);

        $qaObject->setReply(Argument::exact($reply->reveal()))->shouldBeCalledTimes(1);
        
        $qaObject->accept()->shouldBeCalledTimes(1)->willReturn(true);

        $qaObjectRepository = $this->prophesize(QaRepository::class);
        $qaObjectRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($qaObject->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($qaObjectRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
