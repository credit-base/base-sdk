<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Repository\QaRepository;

class RevokeQaCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRevokeQaCommandHandler();
    }

    public function testFetchIModifyStatusObject()
    {
        $commandHandler = $this->getMockBuilder(MockRevokeQaCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $qaObject = \Base\Sdk\Interaction\Utils\MockFactory::generateQa($id);

        $repository = $this->prophesize(QaRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($qaObject);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIModifyStatusObject($id);

        $this->assertEquals($result, $qaObject);
    }

    public function testExtendsRevokeCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RevokeCommandHandler',
            $this->stub
        );
    }
}
