<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditQa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Base\Sdk\Interaction\Repository\UnAuditQaRepository;

class RejectUnAuditQaCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRejectUnAuditQaCommandHandler();
    }

    public function testExtendsRejectCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->stub
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\UnAuditQaRepository',
            $this->stub->getPublicRepository()
        );
    }

    public function testFetchIApplyObject()
    {
        $commandHandler = $this->getMockBuilder(MockRejectUnAuditQaCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $unAuditQa = \Base\Sdk\Interaction\Utils\MockFactory::generateUnAuditQa($id);

        $repository = $this->prophesize(UnAuditQaRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($unAuditQa);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $unAuditQa);
    }
}
