<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditQa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\UnAuditQa;
use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Repository\UnAuditQaRepository;
use Base\Sdk\Interaction\Command\UnAuditQa\ResubmitUnAuditQaCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class ResubmitUnAuditQaCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitUnAuditQaCommandHandler::class)
            ->setMethods(['getRepository',])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(ResubmitUnAuditQaCommandHandler::class)
        ->setMethods(['getRepository'])
        ->getMock();

        $fakerData = $this->getReplyCommonData();
        $id = 1;
        
        $command = new ResubmitUnAuditQaCommand(
            $fakerData,
            $id
        );

        $unAuditQa = $this->prophesize(UnAuditQa::class);

        $reply = $this->prophesize(Reply::class);

        $unAuditQa->getReply()
        ->shouldBeCalledTimes(1)->willReturn($reply->reveal());

        $reply->setContent(Argument::exact($fakerData['content']))->shouldBeCalledTimes(1);
        $reply->setImages(Argument::exact($fakerData['images']))->shouldBeCalledTimes(1);
        $reply->setAdmissibility(Argument::exact($fakerData['admissibility']))->shouldBeCalledTimes(1);

        $unAuditQa->setReply(Argument::exact($reply->reveal()))->shouldBeCalledTimes(1);
        
        $unAuditQa->resubmit()->shouldBeCalledTimes(1)->willReturn(true);

        $unAuditQaRepository = $this->prophesize(UnAuditQaRepository::class);
        $unAuditQaRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($unAuditQa->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($unAuditQaRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
