<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Repository\FeedbackRepository;
use Base\Sdk\Interaction\Command\Feedback\AcceptFeedbackCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AcceptFeedbackCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptFeedbackCommandHandler::class)
            ->setMethods()
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    public function testGetRepository()
    {
        $commandHandler = new MockAddFeedbackCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\FeedbackRepository',
            $commandHandler->getPublicRepository()
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptFeedbackCommandHandler::class)
        ->setMethods(['getRepository'])
        ->getMock();

        $fakerData = $this->getReplyCommonData();
        $id = 1;
        
        $command = new AcceptFeedbackCommand(
            $fakerData,
            $id
        );

        $feedback = $this->prophesize(Feedback::class);

        $reply = $this->prophesize(Reply::class);

        $feedback->getReply()
        ->shouldBeCalledTimes(1)->willReturn($reply->reveal());

        $reply->setContent(Argument::exact($fakerData['content']))->shouldBeCalledTimes(1);
        $reply->setImages(Argument::exact($fakerData['images']))->shouldBeCalledTimes(1);
        $reply->setAdmissibility(Argument::exact($fakerData['admissibility']))->shouldBeCalledTimes(1);

        $feedback->setReply(Argument::exact($reply->reveal()))->shouldBeCalledTimes(1);
        
        $feedback->accept()->shouldBeCalledTimes(1)->willReturn(true);

        $feedbackRepository = $this->prophesize(FeedbackRepository::class);
        $feedbackRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($feedback->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($feedbackRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
