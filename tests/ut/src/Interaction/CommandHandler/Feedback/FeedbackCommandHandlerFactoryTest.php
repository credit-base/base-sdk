<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

class FeedbackCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        $this->commandHandler = new FeedbackCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Basecode\Classes\NullCommandHandler', $commandHandler);
    }
}
