<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Repository\FeedbackRepository;

class RevokeFeedbackCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRevokeFeedbackCommandHandler();
    }

    public function testFetchIModifyStatusObject()
    {
        $commandHandler = $this->getMockBuilder(MockRevokeFeedbackCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $feedback = \Base\Sdk\Interaction\Utils\MockFactory::generateFeedback($id);

        $repository = $this->prophesize(FeedbackRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($feedback);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIModifyStatusObject($id);

        $this->assertEquals($result, $feedback);
    }

    public function testExtendsRevokeCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RevokeCommandHandler',
            $this->stub
        );
    }
}
