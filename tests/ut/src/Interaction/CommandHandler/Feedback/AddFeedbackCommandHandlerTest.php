<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Interaction\Command\Feedback\AddFeedbackCommand;
use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AddFeedbackCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddFeedbackCommandHandler::class)
            ->setMethods(['getFeedback', 'getUserGroupRepository', 'getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetFeedback()
    {
        $commandHandler = new MockAddFeedbackCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Model\Feedback',
            $commandHandler->getPublicFeedback()
        );
    }

    public function initialExecute()
    {
        $fakerData = $this->getAddCommonData();
        
        $command = new AddFeedbackCommand(
            $fakerData['title'],
            $fakerData['content'],
            $fakerData['acceptUserGroupId']
        );
        
        $acceptUserGroup = \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $acceptUserGroupId = $fakerData['acceptUserGroupId'];
        $acceptUserGroupRepository = $this->prophesize(UserGroupRepository::class);
        $acceptUserGroupRepository->fetchOne(Argument::exact($acceptUserGroupId))
            ->shouldBeCalledTimes(1)->willReturn($acceptUserGroup);
        $this->commandHandler->expects($this->exactly(1))
                    ->method('getUserGroupRepository')
                    ->willReturn($acceptUserGroupRepository->reveal());

        $feedback = $this->prophesize(Feedback::class);
        
        $this->commandHandler->expects($this->exactly(1))
            ->method('getFeedback')
            ->willReturn($feedback->reveal());

        $feedback->setAcceptUserGroup(Argument::exact($acceptUserGroup))->shouldBeCalledTimes(1);
        $feedback->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        $feedback->setContent(Argument::exact($command->content))->shouldBeCalledTimes(1);

        $feedback->add()->shouldBeCalledTimes(1)->willReturn(true);

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }
}
