<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Model\Reply;
use Base\Sdk\Interaction\Repository\PraiseRepository;
use Base\Sdk\Interaction\Command\Praise\AcceptPraiseCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AcceptPraiseCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptPraiseCommandHandler::class)
            ->setMethods()
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    public function testGetRepository()
    {
        $commandHandler = new MockAddPraiseCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Repository\PraiseRepository',
            $commandHandler->getPublicRepository()
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $this->commandHandler = $this->getMockBuilder(AcceptPraiseCommandHandler::class)
        ->setMethods(['getRepository'])
        ->getMock();

        $fakerData = $this->getReplyCommonData();
        $id = 1;
        
        $command = new AcceptPraiseCommand(
            $fakerData,
            $id
        );

        $praise = $this->prophesize(Praise::class);

        $reply = $this->prophesize(Reply::class);

        $praise->getReply()
        ->shouldBeCalledTimes(1)->willReturn($reply->reveal());

        $reply->setContent(Argument::exact($fakerData['content']))->shouldBeCalledTimes(1);
        $reply->setImages(Argument::exact($fakerData['images']))->shouldBeCalledTimes(1);
        $reply->setAdmissibility(Argument::exact($fakerData['admissibility']))->shouldBeCalledTimes(1);

        $praise->setReply(Argument::exact($reply->reveal()))->shouldBeCalledTimes(1);
        
        $praise->accept()->shouldBeCalledTimes(1)->willReturn(true);

        $praiseRepository = $this->prophesize(PraiseRepository::class);
        $praiseRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($praise->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($praiseRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
