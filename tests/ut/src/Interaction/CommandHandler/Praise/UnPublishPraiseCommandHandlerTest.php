<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Repository\PraiseRepository;
use Base\Sdk\Interaction\Command\Praise\UnPublishPraiseCommand;

use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class UnPublishPraiseCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(UnPublishPraiseCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $id = 1;
        
        $command = new UnPublishPraiseCommand($id);

        $praise = $this->prophesize(Praise::class);
        
        $praise->unPublish()->shouldBeCalledTimes(1)->willReturn(true);

        $praiseRepository = $this->prophesize(PraiseRepository::class);
        $praiseRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($praise->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($praiseRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
