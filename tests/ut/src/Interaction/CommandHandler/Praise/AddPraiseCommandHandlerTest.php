<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Interaction\Command\Praise\AddPraiseCommand;
use Base\Sdk\Interaction\Command\CommonFakerDataTrait;

class AddPraiseCommandHandlerTest extends TestCase
{
    use CommonFakerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddPraiseCommandHandler::class)
            ->setMethods(['getPraise', 'getUserGroupRepository', 'getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetPraise()
    {
        $commandHandler = new MockAddPraiseCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Model\Praise',
            $commandHandler->getPublicPraise()
        );
    }

    public function testGetUserGroupRepository()
    {
        $commandHandler = new MockAddPraiseCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Repository\UserGroupRepository',
            $commandHandler->getPublicUserGroupRepository()
        );
    }

    public function initialExecute()
    {
        $fakerData = $this->getAddCommonData();
        
        $command = new AddPraiseCommand(
            $fakerData['title'],
            $fakerData['content'],
            $fakerData['name'],
            $fakerData['identify'],
            $fakerData['contact'],
            $fakerData['subject'],
            $fakerData['images'],
            $fakerData['type'],
            $fakerData['acceptUserGroupId']
        );
        
        $acceptUserGroup = \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
        $acceptUserGroupId = $fakerData['acceptUserGroupId'];
        $acceptUserGroupRepository = $this->prophesize(UserGroupRepository::class);
        $acceptUserGroupRepository->fetchOne(Argument::exact($acceptUserGroupId))
            ->shouldBeCalledTimes(1)->willReturn($acceptUserGroup);
        $this->commandHandler->expects($this->exactly(1))
                    ->method('getUserGroupRepository')
                    ->willReturn($acceptUserGroupRepository->reveal());

        $praise = $this->prophesize(Praise::class);
        
        $this->commandHandler->expects($this->exactly(1))
            ->method('getPraise')
            ->willReturn($praise->reveal());

        $praise->setAcceptUserGroup(Argument::exact($acceptUserGroup))->shouldBeCalledTimes(1);
        $praise->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        $praise->setContent(Argument::exact($command->content))->shouldBeCalledTimes(1);
        $praise->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $praise->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $praise->setContact(Argument::exact($command->contact))->shouldBeCalledTimes(1);
        $praise->setSubject(Argument::exact($command->subject))->shouldBeCalledTimes(1);
        $praise->setImages(Argument::exact($command->images))->shouldBeCalledTimes(1);
        $praise->setType(Argument::exact($command->type))->shouldBeCalledTimes(1);
        $praise->add()->shouldBeCalledTimes(1)->willReturn(true);

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);

        $this->assertTrue($result);
    }
}
