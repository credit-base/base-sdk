<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Repository\PraiseRepository;

class RevokePraiseCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRevokePraiseCommandHandler();
    }

    public function testFetchIModifyStatusObject()
    {
        $commandHandler = $this->getMockBuilder(MockRevokePraiseCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $praise = \Base\Sdk\Interaction\Utils\MockFactory::generatePraise($id);

        $repository = $this->prophesize(PraiseRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($praise);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIModifyStatusObject($id);

        $this->assertEquals($result, $praise);
    }

    public function testExtendsRevokeCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RevokeCommandHandler',
            $this->stub
        );
    }
}
