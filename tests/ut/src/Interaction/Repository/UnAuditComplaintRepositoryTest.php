<?php
namespace Base\Sdk\Interaction\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Adapter\UnAuditComplaint\UnAuditComplaintRestfulAdapter;

class UnAuditComplaintRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditComplaintRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIUnAuditInteractionAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Adapter\UnAuditInteraction\IUnAuditInteractionAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Adapter\UnAuditComplaint\UnAuditComplaintRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Adapter\UnAuditComplaint\UnAuditComplaintRestfulAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(UnAuditComplaintRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
