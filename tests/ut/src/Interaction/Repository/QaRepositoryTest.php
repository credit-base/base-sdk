<?php
namespace Base\Sdk\Interaction\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Adapter\Qa\QaRestfulAdapter;

class QaRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(QaRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testImplementsIQaAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Adapter\Interaction\IInteractionAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Adapter\Qa\QaRestfulAdapter',
            $this->repository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Interaction\Adapter\Qa\QaRestfulAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(QaRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testAccept()
    {
        $qaObject = \Base\Sdk\Interaction\Utils\MockFactory::generateQa(1);

        $adapter = $this->prophesize(QaRestfulAdapter::class);
        $adapter->accept(Argument::exact($qaObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->accept($qaObject);
    }

    public function testPublish()
    {
        $qaObject = \Base\Sdk\Interaction\Utils\MockFactory::generateQa(1);

        $adapter = $this->prophesize(QaRestfulAdapter::class);
        $adapter->publish(Argument::exact($qaObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->publish($qaObject);
    }

    public function testUnPublish()
    {
        $qaObject = \Base\Sdk\Interaction\Utils\MockFactory::generateQa(1);

        $adapter = $this->prophesize(QaRestfulAdapter::class);
        $adapter->unPublish(Argument::exact($qaObject))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->unPublish($qaObject);
    }
}
