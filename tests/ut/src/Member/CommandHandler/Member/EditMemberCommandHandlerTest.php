<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Command\Member\EditMemberCommand;

class EditMemberCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditMemberCommandHandler::class)
                                     ->setMethods(['fetchMember'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        $command = new EditMemberCommand(
            $this->faker->randomNumber(),
            $this->faker->randomNumber()
        );

        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(1);

        $member = $this->prophesize(Member::class);
        $member->setGender(Argument::exact($command->gender))->shouldBeCalledTimes(1);
        $member->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->once())
             ->method('fetchMember')
             ->willReturn($member->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}
