<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Model\SecurityQA;
use Base\Sdk\Member\Command\Member\ResetPasswordMemberCommand;

class ResetPasswordMemberCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ResetPasswordMemberCommandHandler::class)
                                     ->setMethods(['getMember'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $faker = \Faker\Factory::create('zh_CN');
        $userName = $faker->name();
        $securityAnswer = $faker->word();
        $password = md5($faker->password());

        $command = new ResetPasswordMemberCommand(
            $userName,
            $securityAnswer,
            $password
        );

        $member = $this->prophesize(Member::class);
        $securityQA = $this->prophesize(SecurityQA::class);
        $securityQA->setAnswer(
            Argument::exact($command->securityAnswer)
        )->shouldBeCalledTimes(1);

        $member->getSecurityQA()->shouldBeCalledTimes(1)->willReturn($securityQA->reveal());

        $member->setUserName(Argument::exact($command->userName))->shouldBeCalledTimes(1);
    
        $member->setPassword(Argument::exact($command->password))->shouldBeCalledTimes(1);
        $member->resetPassword()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->exactly(1))
            ->method('getMember')
            ->willReturn($member->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
