<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Command\Member\SignInMemberCommand;

class SignInMemberCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(SignInMemberCommandHandler::class)
                                     ->setMethods(['getMember'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    private function initialExecute($result)
    {
        $faker = \Faker\Factory::create('zh_CN');
        $userName = $faker->name();
        $password = md5($faker->password());

        $command = new SignInMemberCommand(
            $userName,
            $password
        );

        $member = $this->prophesize(Member::class);
        $member->setUserName(Argument::exact($command->userName))->shouldBeCalledTimes(1);
        $member->setPassword(Argument::exact($command->password))->shouldBeCalledTimes(1);
        $member->signIn()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->exactly(1))
            ->method('getMember')
            ->willReturn($member->reveal());

        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);

        $this->assertFalse($result);
    }
}
