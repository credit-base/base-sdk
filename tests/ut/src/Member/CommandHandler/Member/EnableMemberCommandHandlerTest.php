<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class EnableMemberCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockEnableMemberCommandHandler::class)
            ->setMethods(['fetchMember'])
            ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\EnableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember($id);

        $this->stub->expects($this->once())
             ->method('fetchMember')
             ->with($id)
             ->willReturn($member);

        $result = $this->stub->fetchIEnableObject($id);

        $this->assertEquals($result, $member);
    }
}
