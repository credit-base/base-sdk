<?php
namespace Base\Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class SignOutMemberCommandTest extends TestCase
{
    private $command;
    
    public function setUp()
    {
        $this->command = new SignOutMemberCommand();
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
