<?php
namespace Base\Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class AuthMemberCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'id' => $faker->randomNumber(),
            'identify' => $faker->name()
        );

        $this->command = new AuthMemberCommand(
            $this->fakerData['id'],
            $this->fakerData['identify']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
