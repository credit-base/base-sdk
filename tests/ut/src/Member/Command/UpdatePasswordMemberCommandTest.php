<?php
namespace Base\Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class UpdatePasswordMemberCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'password' => md5($faker->password()),
            'oldPassword' => md5($faker->password()),
            'id' => $faker->randomNumber(),
        );

        $this->command = new UpdatePasswordMemberCommand(
            $this->fakerData['password'],
            $this->fakerData['oldPassword'],
            $this->fakerData['id']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testPasswordParameter()
    {
        $this->assertEquals($this->fakerData['password'], $this->command->password);
    }

    public function testOldPasswordParameter()
    {
        $this->assertEquals($this->fakerData['oldPassword'], $this->command->oldPassword);
    }
}
