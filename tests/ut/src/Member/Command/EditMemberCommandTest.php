<?php
namespace Base\Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class EditMemberCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'gender' => $faker->randomNumber(),
            'id' => $faker->randomNumber()
        );

        $this->command = new EditMemberCommand(
            $this->fakerData['gender'],
            $this->fakerData['id']
        );
    }

    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testGenderParameter()
    {
        $this->assertEquals($this->fakerData['gender'], $this->command->gender);
    }
}
