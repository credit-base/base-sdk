<?php
namespace Base\Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class SignInMemberCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'userName' => $faker->name(),
            'password' => $faker->password(),
            'id' => $faker->randomNumber(),
        );

        $this->command = new SignInMemberCommand(
            $this->fakerData['userName'],
            $this->fakerData['password'],
            $this->fakerData['id']
        );
    }

    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testPasswordParameter()
    {
        $this->assertEquals($this->fakerData['password'], $this->command->password);
    }

    public function testUsernameParameter()
    {
        $this->assertEquals($this->fakerData['userName'], $this->command->userName);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }

    public function testDefaultIdParameter()
    {
        $this->command = new SignInMemberCommand(
            $this->fakerData['userName'],
            $this->fakerData['password']
        );
        $this->assertEquals(0, $this->command->id);
    }
}
