<?php
namespace Base\Sdk\Member\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class SecurityQATest extends TestCase
{
    private $securityQA;

    public function setUp()
    {
        $this->securityQA = new SecurityQA();
    }

    public function tearDown()
    {
        unset($this->securityQA);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->securityQA->getId());
        $this->assertEquals('', $this->securityQA->getAnswer());
    }

    //id 测试 ------------------------------------------------------ start
    /**
     * 循环测试 SecurityQA setId() 数据构建器
     */
    public function securityQuestionProvider()
    {
        return array(
            array(SecurityQA::SECURITY_QUESTION['NULL'],
                  SecurityQA::SECURITY_QUESTION['NULL']),
            array(SecurityQA::SECURITY_QUESTION['QUESTION_NAME'],
                  SecurityQA::SECURITY_QUESTION['QUESTION_NAME']),
            array(SecurityQA::SECURITY_QUESTION['QUESTION_MOVIE'],
                  SecurityQA::SECURITY_QUESTION['QUESTION_MOVIE']),
            array(SecurityQA::SECURITY_QUESTION['QUESTION_FRIEND'],
                  SecurityQA::SECURITY_QUESTION['QUESTION_FRIEND']),
            array(SecurityQA::SECURITY_QUESTION['QUESTION_HIGH_SCHOOL'],
                  SecurityQA::SECURITY_QUESTION['QUESTION_HIGH_SCHOOL']),
            array(SecurityQA::SECURITY_QUESTION['QUESTION_COLOR'],
                  SecurityQA::SECURITY_QUESTION['QUESTION_COLOR']),
        );
    }
 
    /**
     * 循环测试 securityQA setId() 是否符合预定范围
     *
     * @dataProvider securityQuestionProvider
     */
    public function testSetId($actual, $expected)
    {
        $this->securityQA->setId($actual);
        $this->assertEquals($expected, $this->securityQA->getId());
    }

    /**
     * 设置 SecurityQA setId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    // public function testSetIdWrongType()
    // {
    //     $this->securityQA->setId('string');
    // }
    //id 测试 ------------------------------------------------------   end

    //answer 测试 -------------------------------------------------------- start
    /**
     * 设置 securityQA setAnswer() 正确的传参类型,期望传值正确
     */
    public function testSetAnswerCorrectType()
    {
        $this->securityQA->setAnswer('string');
        $this->assertEquals('string', $this->securityQA->getAnswer());
    }

    /**
     * 设置 securityQA setAnswer() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAnswerWrongType()
    {
        $this->securityQA->setAnswer(['string']);
    }
    //answer 测试 --------------------------------------------------------   end
}
