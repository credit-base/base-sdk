<?php
namespace Base\Sdk\Member\Adapter\Member;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Model\NullMember;
use Base\Sdk\Member\Translator\MemberSessionTranslator;
use Base\Sdk\Member\Adapter\Member\Query\MemberSessionDataCacheQuery;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

class MemberSessionAdapterTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockMemberSessionAdapter::class)
            ->setMethods(
                [
                    'getTranslator',
                    'getSession',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetTranslator()
    {
        $stub = new MockMemberSessionAdapter();

        $this->assertInstanceOf(
            'Base\Sdk\Member\Translator\MemberSessionTranslator',
            $stub->getTranslator()
        );
    }

    public function testGetSession()
    {
        $stub = new MockMemberSessionAdapter();

        $this->assertInstanceOf(
            'Base\Sdk\Member\Adapter\Member\Query\MemberSessionDataCacheQuery',
            $stub->getSession()
        );
    }

    public function testGetTTL()
    {
        $stub = new MockMemberSessionAdapter();

        $result = $stub->getTTL();
        $this->assertEquals(MemberSessionAdapter::TTL, $result);
    }

    public function testGetTTLCache()
    {
        $stub = new MockMemberSessionAdapter();

        Core::$container->set('cache.session.ttl', MemberSessionAdapter::TTL);

        $result = $stub->getTTL();
        $this->assertEquals(MemberSessionAdapter::TTL, $result);
    }

    public function testGet()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(1);
        $infoArray = array($member);

        $info = $this->prophesize(MemberSessionDataCacheQuery::class);
        $info->get(Argument::exact($member->getId()))->shouldBeCalledTimes(1)->willReturn($infoArray);
        $this->stub->expects($this->exactly(1))
            ->method('getSession')
            ->willReturn($info->reveal());

        $translator = $this->prophesize(MemberSessionTranslator::class);
        $translator->arrayToObject(Argument::exact($infoArray))->shouldBeCalledTimes(1)->willReturn($member);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $result = $this->stub->get($member->getId());

        $this->assertEquals($member, $result);
    }

    private function prepareMemberSessionTranslator(
        Member $member,
        array $infoArray
    ) {
        $translator = $this->prophesize(MemberSessionTranslator::class);
        $translator->objectToArray(Argument::exact($member))->shouldBeCalledTimes(1)->willReturn($infoArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    public function testSaveTrue()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(1);
        $infoArray = array($member);

        $this->prepareMemberSessionTranslator(
            $member,
            $infoArray
        );

        $session = $this->prophesize(MemberSessionDataCacheQuery::class);
        $session->save(
            Argument::exact($member->getId()),
            Argument::exact($infoArray),
            Argument::exact(MemberSessionAdapter::TTL)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getSession')
            ->willReturn($session->reveal());

        $result = $this->stub->save($member);

        $this->assertTrue($result);
    }

    public function testSaveFalse()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(1);
        $infoArray = array($member);
        
        $this->prepareMemberSessionTranslator(
            $member,
            $infoArray
        );

        $session = $this->prophesize(MemberSessionDataCacheQuery::class);
        $session->save(
            Argument::exact($member->getId()),
            Argument::exact($infoArray),
            Argument::exact(MemberSessionAdapter::TTL)
        )->shouldBeCalledTimes(1)->willReturn(false);

        $this->stub->expects($this->exactly(1))
            ->method('getSession')
            ->willReturn($session->reveal());

        $result = $this->stub->save($member);

        $this->assertFalse($result);
    }

    public function testDelTrue()
    {
        $id = 1;
        
        $session = $this->prophesize(MemberSessionDataCacheQuery::class);
        $session->del(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getSession')
            ->willReturn($session->reveal());

        $result = $this->stub->del($id);

        $this->assertTrue($result);
    }
}
