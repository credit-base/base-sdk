<?php
namespace Base\Sdk\Member\Adapter\Member\Query;

use PHPUnit\Framework\TestCase;

class MemberSessionDataCacheQueryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MemberSessionDataCacheQuery();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Query\DataCacheQuery', $this->stub);
    }
}
