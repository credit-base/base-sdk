<?php
namespace Base\Sdk\Member\Adapter\Member;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Member\Model\Member;

/**
 * @SuppressWarnings(PHPMD)
 */
class MemberMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MemberMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testSignIn()
    {
        $this->assertTrue($this->adapter->signIn(new Member()));
    }

    public function testUpdatePassword()
    {
        $this->assertTrue($this->adapter->updatePassword(new Member()));
    }

    public function testResetPassword()
    {
        $this->assertTrue($this->adapter->resetPassword(new Member()));
    }

    public function testValidateSecurity()
    {
        $this->assertTrue($this->adapter->validateSecurity(new Member()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Member\Model\Member',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Sdk\Member\Model\Member',
                $each
            );
        }
    }

    public function testFetchOneAsync()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Member\Model\Member',
            $this->adapter->fetchOneAsync(1)
        );
    }

    public function testFetchListAsync()
    {
        $list = $this->adapter->fetchListAsync([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Sdk\Member\Model\Member',
                $each
            );
        }
    }

    public function testSearch()
    {
        list($list, $count) = $this->adapter->search(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Sdk\Member\Model\Member',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }

    public function testSearchAsync()
    {
        list($list, $count) = $this->adapter->searchAsync(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Base\Sdk\Member\Model\Member',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}
