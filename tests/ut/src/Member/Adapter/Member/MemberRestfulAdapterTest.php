<?php
namespace Base\Sdk\Member\Adapter\Member;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Model\NullMember;
use Base\Sdk\Member\Translator\MemberRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class MemberRestfulAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockMemberRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors'
                            ])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIMemberAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Member\Adapter\Member\IMemberAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $adapter = new MockMemberRestfulAdapter();

        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $adapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('members', $this->adapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->adapter->scenario($expect);
        $this->assertEquals($actual, $this->adapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'MEMBER_LIST',
                MemberRestfulAdapter::SCENARIOS['MEMBER_LIST']
            ],
            [
                'MEMBER_FETCH_ONE',
                MemberRestfulAdapter::SCENARIOS['MEMBER_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullMember::getInstance())
            ->willReturn($member);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($member, $result);
    }

    /**
     * 为MemberRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$Member,$keys,$MemberArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareMemberTranslator(
        Member $member,
        array $keys,
        array $memberArray
    ) {
        $translator = $this->prophesize(MemberRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($member),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($memberArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Member $member)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($member);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
     
    public function testAddSuccess()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(1);
        $memberArray = array($member);

        $this->prepareMemberTranslator(
            $member,
            array(
                'userName',
                'realName',
                'cardId',
                'cellphone',
                'email',
                'contactAddress',
                'securityAnswer',
                'password',
                'securityQuestion'
            ),
            $memberArray
        );

        $this->adapter
            ->method('post')
            ->with('members', $memberArray);

        $this->success($member);
        $result = $this->adapter->add($member);
        $this->assertTrue($result);
    }

    public function testAddFailure()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(1);
        $memberArray = array('member');

        $this->prepareMemberTranslator(
            $member,
            array(
                'userName',
                'realName',
                'cardId',
                'cellphone',
                'email',
                'contactAddress',
                'securityAnswer',
                'password',
                'securityQuestion'
            ),
            $memberArray
        );

        $this->adapter
            ->method('post')
            ->with('members', $memberArray);
        
            $this->failure($member);
            $result = $this->adapter->add($member);
            $this->assertFalse($result);
    }

    public function testEditSuccess()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(2);
        $memberArray = array($member);

        $this->prepareMemberTranslator(
            $member,
            array('gender'),
            $memberArray
        );

        $this->adapter
            ->method('patch')
            ->with('members/'.$member->getId(), $memberArray);

        $this->success($member);
        $result = $this->adapter->edit($member);
        $this->assertTrue($result);
    }

    public function testEditFailure()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(2);
        $memberArray = array('member');

        $this->prepareMemberTranslator(
            $member,
            array('gender'),
            $memberArray
        );

        $this->adapter
            ->method('patch')
            ->with('members/'.$member->getId(), $memberArray);
        
            $this->failure($member);
            $result = $this->adapter->edit($member);
            $this->assertFalse($result);
    }
     
    public function testSignInSuccess()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(3);
        $memberArray = array($member);

        $this->prepareMemberTranslator(
            $member,
            array('userName','password'),
            $memberArray
        );

        $this->adapter
            ->method('patch')
            ->with('members/signIn', $memberArray);

        $this->success($member);
        $result = $this->adapter->signIn($member);
        $this->assertTrue($result);
    }

    public function testSignInFailure()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(3);
        $memberArray = array('member');

        $this->prepareMemberTranslator(
            $member,
            array('userName','password'),
            $memberArray
        );

        $this->adapter
            ->method('patch')
            ->with('members/signIn', $memberArray);
        
            $this->failure($member);
            $result = $this->adapter->signIn($member);
            $this->assertFalse($result);
    }

    public function testResetPasswordSuccess()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(3);
        $memberArray = array($member);

        $this->prepareMemberTranslator(
            $member,
            array('userName', 'password', 'securityAnswer'),
            $memberArray
        );

        $this->adapter
            ->method('patch')
            ->with('members/resetPassword', $memberArray);

        $this->success($member);
        $result = $this->adapter->resetPassword($member);
        $this->assertTrue($result);
    }

    public function testResetPasswordFailure()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(3);
        $memberArray = array('member');

        $this->prepareMemberTranslator(
            $member,
            array('userName', 'password', 'securityAnswer'),
            $memberArray
        );

        $this->adapter
            ->method('patch')
            ->with('members/resetPassword', $memberArray);
        
            $this->failure($member);
            $result = $this->adapter->resetPassword($member);
            $this->assertFalse($result);
    }

    public function testUpdatePasswordSuccess()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(2);
        $memberArray = array($member);

        $this->prepareMemberTranslator(
            $member,
            array('password', 'oldPassword'),
            $memberArray
        );

        $this->adapter
            ->method('patch')
            ->with('members/'.$member->getId().'/updatePassword', $memberArray);

        $this->success($member);
        $result = $this->adapter->updatePassword($member);
        $this->assertTrue($result);
    }

    public function testUpdatePasswordFailure()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(2);
        $memberArray = array('member');

        $this->prepareMemberTranslator(
            $member,
            array('password', 'oldPassword'),
            $memberArray
        );

        $this->adapter
            ->method('patch')
            ->with('members/'.$member->getId().'/updatePassword', $memberArray);
        
            $this->failure($member);
            $result = $this->adapter->updatePassword($member);
            $this->assertFalse($result);
    }

    public function testValidateSecuritySuccess()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(2);
        $memberArray = array($member);

        $this->prepareMemberTranslator(
            $member,
            array('securityAnswer'),
            $memberArray
        );

        $this->adapter
            ->method('patch')
            ->with('members/'.$member->getId().'/validateSecurity', $memberArray);

        $this->success($member);
        $result = $this->adapter->validateSecurity($member);
        $this->assertTrue($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->exactly(1))
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [
            501 => REAL_NAME_FORMAT_ERROR,
            502 => CELLPHONE_FORMAT_ERROR,
            503 => PASSWORD_FORMAT_ERROR,
            504 => CARDID_FORMAT_ERROR,
            505 =>[
                'password' => PASSWORD_INCORRECT,
                'oldPassword' => OLD_PASSWORD_INCORRECT,
            ],
            10 => [
                'userName'=>NAME_NOT_EXIT,
                ''=>RESOURCE_NOT_EXIST
            ],
            101 => [
                'userName' => NAME_FORMAT_ERROR,
                'email' => SOURCE_FORMAT_ERROR,
                'contactAddress' => CONTACT_ADDRESS_FORMAT_ERROR,
                'securityQuestion' => SECURITY_QUESTION_FORMAT_ERROR,
                'securityAnswer' => SECURITY_ANSWER_FORMAT_ERROR,
                'gender' => GENDER_FORMAT_ERROR,
            ],
            102 =>[
                'status' => STATUS_CAN_NOT_MODIFY,
                'signInStatus'=>USER_STATUS_DISABLE,
            ],
            104 =>[
                'securityAnswer' => SECURITY_ANSWER_INCORRECT
            ],
            103 =>[
                'userName' => USER_NAME_EXIST,
                'cellphone' => CELLPHONE_EXIST,
                'email' => EMAIL_EXIST,
            ],
        ];
        
        $result = $this->adapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }

    public function testValidateSecurityFailure()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(2);
        $memberArray = array('member');

        $this->prepareMemberTranslator(
            $member,
            array('securityAnswer'),
            $memberArray
        );

        $this->adapter
            ->method('patch')
            ->with('members/'.$member->getId().'/validateSecurity', $memberArray);
        
            $this->failure($member);
            $result = $this->adapter->validateSecurity($member);
            $this->assertFalse($result);
    }
}
