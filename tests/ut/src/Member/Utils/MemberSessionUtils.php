<?php
namespace Base\Sdk\Member\Utils;

trait MemberSessionUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $member
    ) {
        $this->assertEquals($expectedArray['id'], $member->getId());
        $this->assertEquals($expectedArray['userName'], $member->getUserName());
        $this->assertEquals($expectedArray['cellphone'], $member->getCellphone());
        $this->assertEquals($expectedArray['realName'], $member->getRealName());
        $this->assertEquals($expectedArray['identify'], $member->getIdentify());
        $this->assertEquals($expectedArray['avatar'], $member->getAvatar());
    }
}
