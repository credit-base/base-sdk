<?php
namespace Base\Sdk\Member\Utils;

trait MemberRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $member
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $member->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['userName'], $member->getUserName());
        $this->assertEquals($expectedArray['data']['attributes']['realName'], $member->getRealName());
        $this->assertEquals($expectedArray['data']['attributes']['cellphone'], $member->getCellphone());
        $this->assertEquals($expectedArray['data']['attributes']['email'], $member->getEmail());
        $this->assertEquals($expectedArray['data']['attributes']['cardId'], $member->getCardId());
        $this->assertEquals(
            $expectedArray['data']['attributes']['contactAddress'],
            $member->getContactAddress()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['securityQuestion'],
            $member->getSecurityQa()->getId()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['securityAnswer'],
            $member->getSecurityQa()->getAnswer()
        );
    }
}
