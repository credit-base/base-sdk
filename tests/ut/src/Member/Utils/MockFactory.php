<?php
namespace Base\Sdk\Member\Utils;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Model\SecurityQA;

class MockFactory
{
    public static function generateMember(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Member {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $member = new Member($id);
        $member->setId($id);

        //userName
        self::generateUserName($member, $faker, $value);
        //realName
        self::generateRealName($member, $faker, $value);
        //cardId
        self::generateCardId($member, $faker, $value);
        //cellphone
        self::generateCellphone($member, $faker, $value);
        //email
        self::generateEmail($member, $faker, $value);
        //contactAddress
        self::generateContactAddress($member, $faker, $value);
        //securityQa
        self::generateSecurityQa($member, $faker, $value);
        //password
        self::generatePassword($member, $faker, $value);
        //gender
        self::generateGender($member, $faker, $value);

        //status
        self::generateStatus($member, $faker, $value);

        $member->setCreateTime($faker->unixTime());
        $member->setUpdateTime($faker->unixTime());
        $member->setStatusTime($faker->unixTime());

        return $member;
    }

    private static function generateCellphone($member, $faker, $value) : void
    {
        $cellphone = isset($value['cellphone']) ?
            $value['cellphone'] :
            $faker->phoneNumber();
        
        $member->setUserName($cellphone);
        $member->setCellphone($cellphone);
    }

    private static function generateRealName($member, $faker, $value) : void
    {
        $realName = isset($value['realName']) ?
            $value['realName'] :
            $faker->name();
        
        $member->setRealName($realName);
    }

    private static function generateCardId($member, $faker, $value) : void
    {
        $cardId = isset($value['cardId']) ? $value['cardId'] : $faker->creditCardNumber();
        $member->setCardId($cardId);
    }

    private static function generateUserName($member, $faker, $value) : void
    {
        unset($faker);
        $userName = isset($value['userName']) ? $value['userName'] : 'userName';
        $member->setUserName($userName);
    }

    private static function generateEmail($member, $faker, $value) : void
    {
        unset($faker);
        $email = isset($value['email']) ?
            $value['email'] :'997934301@qq.com';
        $member->setEmail($email);
    }

    private static function generateContactAddress($member, $faker, $value) : void
    {
        unset($faker);
        $contactAddress = isset($value['contactAddress']) ?
            $value['contactAddress'] :'陕西省西安市雁塔区';
        $member->setContactAddress($contactAddress);
    }

    private static function generateSecurityQa($member, $faker, $value) : void
    {
        unset($faker);
        $securityQa= isset($value['securityQa']) ?
            $value['securityQa'] : \Base\Sdk\Member\Utils\MockFactory::generateSecurityQaObject(1);
        $member->setSecurityQa($securityQa);
    }

    private static function generatePassword($member, $faker, $value) : void
    {
        $password = isset($value['password']) ?
            $value['password'] :
            md5($faker->randomNumber());
        $member->setPassword($password);
    }

    private static function generateGender($member, $faker, $value) : void
    {
        $gender = isset($value['gender']) ? $value['gender'] : $faker->randomElement(Member::GENDER);
        $member->setGender($gender);
    }

    private static function generateStatus($member, $faker, $value) : void
    {
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(Member::STATUS);
        $member->setStatus($status);
    }

    public static function generateSecurityQaObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : SecurityQA {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $securityQa = new SecurityQA($id);
        $securityQa->setId($id);

        //answer
        self::generateAnswer($securityQa, $faker, $value);
       

        return $securityQa;
    }

    private static function generateAnswer($securityQa, $faker, $value) : void
    {
        unset($faker);
        $answer = isset($value['answer']) ?
            $value['answer'] :'严明';
        $securityQa->setAnswer($answer);
    }
}
