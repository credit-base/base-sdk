<?php
namespace Base\Sdk\Member\Utils;

use Base\Sdk\Common\Model\IApplyCategory;
use Base\Sdk\Member\Translator\MemberTranslator;

use Base\Sdk\Common\Utils\Mask;

trait MemberUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $member
    ) {
        $this->assertEquals($expectedArray['userName'], $member->getUserName());
        $this->assertEquals($expectedArray['realName'], $member->getRealName());
        $this->assertEquals($expectedArray['cellphone'], $member->getCellphone());
        $this->assertEquals($expectedArray['cellphoneMask'], Mask::mask($member->getCellphone(), 4, 3));
        $this->assertEquals($expectedArray['email'], $member->getEmail());
        $this->assertEquals($expectedArray['cardId'], $member->getCardId());
        $cardId = strlen($member->getCardId()) == MemberTranslator::CARD_ID_MAX_LENGTH ?
        Mask::mask($member->getCardId(), 4, 10) :
        Mask::mask($member->getCardId(), 4, 7);
        $this->assertEquals($expectedArray['cardIdMask'], $cardId);
        $this->assertEquals(
            $expectedArray['securityQuestion']['id'],
            marmot_encode($member->getSecurityQa()->getId())
        );
        $securityQuestionName = array_key_exists(
            $member->getSecurityQa()->getId(),
            MemberTranslator::SECURITY_QUESTION_CN
        ) ? MemberTranslator::SECURITY_QUESTION_CN[$member->getSecurityQa()->getId()] : '';
        
        $this->assertEquals(
            $expectedArray['securityQuestion']['name'],
            $securityQuestionName
        );
        $this->assertEquals(
            $expectedArray['securityAnswer'],
            $member->getSecurityQa()->getAnswer()
        );
        $this->assertEquals($expectedArray['gender']['id'], marmot_encode($member->getGender()));
        $this->assertEquals($expectedArray['gender']['name'], MemberTranslator::GENDER_CN[$member->getGender()]);

        $this->assertEquals($expectedArray['status']['id'], marmot_encode($member->getStatus()));
        $this->assertEquals($expectedArray['status']['name'], IApplyCategory:: STATUS_CN[$member->getStatus()]);
        $this->assertEquals($expectedArray['status']['type'], IApplyCategory:: STATUS_TAG_TYPE[$member->getStatus()]);

        $this->assertEquals($expectedArray['updateTime'], $member->getUpdateTime());
        $this->assertEquals($expectedArray['updateTimeFormat'], date('Y年m月d日 H点i分', $member->getUpdateTime()));

        $this->assertEquals($expectedArray['statusTime'], $member->getStatusTime());
        $this->assertEquals($expectedArray['statusTimeFormat'], date('Y年m月d日 H点i分', $member->getStatusTime()));

        $this->assertEquals($expectedArray['createTime'], $member->getCreateTime());
        $this->assertEquals($expectedArray['createTimeFormat'], date('Y年m月d日 H点i分', $member->getCreateTime()));
    }
}
