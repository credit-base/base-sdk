<?php
namespace Base\Sdk\Member\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Member\Utils\MemberUtils;

class MemberTranslatorTest extends TestCase
{
    use MemberUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new MemberTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Member\Model\NullMember', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(1);
        $member->setCardId('412345678998762234');

        $expression = $this->translator->objectToArray($member);
        
        $this->compareArrayAndObject($expression, $member);
    }

    public function testObjectToArrayCardId()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(1);
        $member->setCardId('412345678998762');
        
        $expression = $this->translator->objectToArray($member);
        
        $this->compareArrayAndObject($expression, $member);
    }

    public function testObjectToArrayFail()
    {
        $member = null;

        $expression = $this->translator->objectToArray($member);
        $this->assertEquals(array(), $expression);
    }
}
