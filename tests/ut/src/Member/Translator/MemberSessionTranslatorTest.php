<?php
namespace Base\Sdk\Member\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Member\Utils\MemberSessionUtils;

class MemberSessionTranslatorTest extends TestCase
{
    use MemberSessionUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new MemberSessionTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(1);

        $expression['id'] = $member->getId();
        $expression['identify'] = $member->getIdentify();
        $expression['cellphone'] = $member->getCellphone();
        $expression['userName'] = $member->getUserName();
        $expression['realName'] = $member->getRealName();
        $expression['avatar'] = $member->getAvatar();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Member\Model\Member', $objects);
    }

    public function testObjectToArray()
    {
        $member = \Base\Sdk\Member\Utils\MockFactory::generateMember(1);

        $expression = $this->translator->objectToArray($member);

        $this->compareArrayAndObject($expression, $member);
    }

    public function testArrayToObjects()
    {
        $translator = $this->getMockBuilder(MemberSessionTranslator::class)
                     ->setMethods(['arrayToObject'])
                     ->getMock();

        $info = array('info');
        $expression = array(
            'id' => 1,
            'info' => $info
        );

        $translator->expects($this->once())
                   ->method('arrayToObject')
                   ->willReturn($info);

        $objects = $translator->arrayToObjects($expression);

        $this->assertEquals([1=>$info], $objects);
    }
}
