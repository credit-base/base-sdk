<?php
namespace Base\Sdk\WorkOrderTask\Command\ParentTask;

use PHPUnit\Framework\TestCase;

class AssignParentTaskCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'title' => $faker->name(),
            'description' => $faker->name(),
            'endTime' => $faker->name(),
            'attachment' => array(),
            'assignObjects' => array(),
            'templateType' => $faker->randomNumber(),
            'template' => $faker->randomNumber()
        );

        $this->command = new AssignParentTaskCommand(
            $this->fakerData['title'],
            $this->fakerData['description'],
            $this->fakerData['endTime'],
            $this->fakerData['attachment'],
            $this->fakerData['assignObjects'],
            $this->fakerData['templateType'],
            $this->fakerData['template']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
