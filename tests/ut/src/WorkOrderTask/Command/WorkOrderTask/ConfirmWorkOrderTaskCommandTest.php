<?php
namespace Base\Sdk\WorkOrderTask\Command\WorkOrderTask;

use PHPUnit\Framework\TestCase;

class ConfirmWorkOrderTaskCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'id' => $faker->randomNumber()
        );

        $this->command = new ConfirmWorkOrderTaskCommand(
            $this->fakerData['id']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
