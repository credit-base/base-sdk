<?php
namespace Base\Sdk\WorkOrderTask\Command;

use PHPUnit\Framework\TestCase;

class OperationCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'id' => $faker->randomNumber(),
            'reason' => $faker->word()
        );

        $this->command = new OperationCommand(
            $this->fakerData['id'],
            $this->fakerData['reason']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }
}
