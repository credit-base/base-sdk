<?php
namespace Base\Sdk\WorkOrderTask\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Template\Model\Template;
use Base\Sdk\WorkOrderTask\Model\ParentTask;

use Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class WorkOrderTaskTest extends TestCase
{
    private $workOrderTask;

    public function setUp()
    {
        $this->workOrderTask = new WorkOrderTask();
        $this->stub = $this->getMockBuilder(WorkOrderTask::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends WorkOrderTask{
            public function getRepository() : WorkOrderTaskRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->workOrderTask);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->workOrderTask
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->workOrderTask->setId(1);
        $this->assertEquals(1, $this->workOrderTask->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //parentTask 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setParentTask() 正确的传参类型,期望传值正确
     */
    public function testSetParentTaskCorrectType()
    {
        $expectedParentTask = new ParentTask();

        $this->workOrderTask->setParentTask($expectedParentTask);
        $this->assertEquals($expectedParentTask, $this->workOrderTask->getParentTask());
    }

    /**
     * 设置 WorkOrderTask setParentTask() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetParentTaskWrongType()
    {
        $this->workOrderTask->setParentTask('parentTask');
    }
    //parentTask 测试 --------------------------------------------------------   end

    //assignObject 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setAssignObject() 正确的传参类型,期望传值正确
     */
    public function testSetAssignObjectCorrectType()
    {
        $expectedAssignObject = new UserGroup();

        $this->workOrderTask->setAssignObject($expectedAssignObject);
        $this->assertEquals($expectedAssignObject, $this->workOrderTask->getAssignObject());
    }

    /**
     * 设置 WorkOrderTask setAssignObject() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAssignObjectWrongType()
    {
        $this->workOrderTask->setAssignObject('assignObject');
    }
    //assignObject 测试 --------------------------------------------------------   end

    //template 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $expectedTemplate = new Template();

        $this->workOrderTask->setTemplate($expectedTemplate);
        $this->assertEquals($expectedTemplate, $this->workOrderTask->getTemplate());
    }

    /**
     * 设置 WorkOrderTask setTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    // public function testSetTemplateWrongType()
    // {
    //     $this->workOrderTask->setTemplate('template');
    // }
    //template 测试 --------------------------------------------------------   end

    //feedbackRecords 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setFeedbackRecords() 正确的传参类型,期望传值正确
     */
    public function testSetFeedbackRecordsCorrectType()
    {
        $this->workOrderTask->setFeedbackRecords(array());
        $this->assertEquals(array(), $this->workOrderTask->getFeedbackRecords());
    }

    /**
     * 设置 WorkOrderTask setFeedbackRecords() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFeedbackRecordsWrongType()
    {
        $this->workOrderTask->setFeedbackRecords('string');
    }
    //feedbackRecords 测试 --------------------------------------------------------   end

    //isExistedTemplate 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setIsExistedTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetIsExistedTemplateCorrectType()
    {
        $this->workOrderTask->setIsExistedTemplate(0);
        $this->assertEquals(0, $this->workOrderTask->getIsExistedTemplate());
    }

    /**
     * 设置 WorkOrderTask setIsExistedTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIsExistedTemplateWrongType()
    {
        $this->workOrderTask->setIsExistedTemplate(array(1,2,3));
    }
    //isExistedTemplate 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository',
            $this->workOrderTask->getRepository()
        );
    }

    public function testEnd()
    {
        $this->stub = $this->getMockBuilder(WorkOrderTask::class)
            ->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(WorkOrderTaskRepository::class);
        $repository->end(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->end();
        $this->assertTrue($result);
    }

    public function testRevoke()
    {
        $this->stub = $this->getMockBuilder(WorkOrderTask::class)
            ->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(WorkOrderTaskRepository::class);
        $repository->revoke(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->revoke();
        $this->assertTrue($result);
    }

    public function testConfirm()
    {
        $this->stub = $this->getMockBuilder(WorkOrderTask::class)
            ->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(WorkOrderTaskRepository::class);
        $repository->confirm(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->confirm();
        $this->assertTrue($result);
    }

    public function testFeedback()
    {
        $this->stub = $this->getMockBuilder(WorkOrderTask::class)
            ->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(WorkOrderTaskRepository::class);
        $repository->feedback(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->feedback();
        $this->assertTrue($result);
    }
}
