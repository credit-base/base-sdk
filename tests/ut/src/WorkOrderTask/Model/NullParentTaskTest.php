<?php
namespace Base\Sdk\WorkOrderTask\Model;

use PHPUnit\Framework\TestCase;

class NullParentTaskTest extends TestCase
{
    private $parentTask;
    
    private $childParentTask;

    public function setUp()
    {
        $this->parentTask = NullParentTask::getInstance();
        $this->childParentTask = new class extends NullParentTask
        {
            public function resourceNotExist() : bool
            {
                return parent::resourceNotExist();
            }
        };
    }

    public function tearDown()
    {
        unset($this->parentTask);
        unset($this->childParentTask);
    }

    public function testExtendsParentTask()
    {
        $this->assertInstanceof('Base\Sdk\WorkOrderTask\Model\ParentTask', $this->parentTask);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->parentTask);
    }

    public function testAssign()
    {
        $parentTask = $this->getMockBuilder(MockNullParentTask::class)
            ->setMethods(['resourceNotExist'])
            ->getMock();
        $parentTask->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($parentTask->assign());
    }

    public function testRevoke()
    {
        $stub = new MockNullParentTask();
        $this->assertFalse($stub->revoke());
    }

    public function testResourceNotExist()
    {
        $stub = new MockNullParentTask();
        $this->assertFalse($stub->resourceNotExist());
    }
}
