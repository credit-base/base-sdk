<?php
namespace Base\Sdk\WorkOrderTask\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullWorkOrderTaskTest extends TestCase
{
    private $workOrderTask;

    private $childWorkOrderTask;

    public function setUp()
    {
        $this->workOrderTask = NullWorkOrderTask::getInstance();
        $this->childWorkOrderTask = new class extends NullWorkOrderTask
        {
            public function resourceNotExist() : bool
            {
                return parent::resourceNotExist();
            }
        };
    }

    public function tearDown()
    {
        unset($this->workOrderTask);
        unset($this->childWorkOrderTask);
    }

    public function testExtendsWorkOrderTask()
    {
        $this->assertInstanceof('Base\Sdk\WorkOrderTask\Model\WorkOrderTask', $this->workOrderTask);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->workOrderTask);
    }

    public function testConfirm()
    {
        $workOrderTask = $this->getMockBuilder(NullWorkOrderTask::class)
            ->setMethods(['resourceNotExist'])
            ->getMock();
        $workOrderTask->expects($this->once())
            ->method('resourceNotExist')
            ->willReturn(false);

        $this->assertFalse($workOrderTask->confirm());
    }

    public function testEnd()
    {
        $workOrderTask = $this->getMockBuilder(NullWorkOrderTask::class)
            ->setMethods(['resourceNotExist'])
            ->getMock();
        $workOrderTask->expects($this->once())
            ->method('resourceNotExist')
            ->willReturn(false);

        $this->assertFalse($workOrderTask->end());
    }

    public function testRevoke()
    {
        $workOrderTask = $this->getMockBuilder(NullWorkOrderTask::class)
            ->setMethods(['resourceNotExist'])
            ->getMock();
        $workOrderTask->expects($this->once())
            ->method('resourceNotExist')
            ->willReturn(false);

        $this->assertFalse($workOrderTask->revoke());
    }

    public function testResourceNotExist()
    {
        $result = $this->childWorkOrderTask->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testFeedback()
    {
        $this->assertFalse($this->childWorkOrderTask->feedback());
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
