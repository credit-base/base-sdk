<?php
namespace Base\Sdk\WorkOrderTask\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Model\Template;
use Base\Sdk\WorkOrderTask\Repository\ParentTaskRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class ParentTaskTest extends TestCase
{
    private $workOrderTask;

    public function setUp()
    {
        $this->workOrderTask = new ParentTask();
        $this->stub = $this->getMockBuilder(ParentTask::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends ParentTask{
            public function getRepository() : ParentTaskRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->workOrderTask);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->workOrderTask
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->workOrderTask->setId(1);
        $this->assertEquals(1, $this->workOrderTask->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //title 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->workOrderTask->setTitle('string');
        $this->assertEquals('string', $this->workOrderTask->getTitle());
    }

    /**
     * 设置 WorkOrderTask setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->workOrderTask->setTitle(array(1,2,3));
    }
    //title 测试 --------------------------------------------------------   end

    //template 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $expectedTemplate = new Template();

        $this->workOrderTask->setTemplate($expectedTemplate);
        $this->assertEquals($expectedTemplate, $this->workOrderTask->getTemplate());
    }

    /**
     * 设置 WorkOrderTask setTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    // public function testSetTemplateWrongType()
    // {
    //     $this->workOrderTask->setTemplate('template');
    // }
    //template 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->workOrderTask->setStatus(0);
        $this->assertEquals(0, $this->workOrderTask->getStatus());
    }

    /**
     * 设置 WorkOrderTask setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->workOrderTask->setStatus(array(1,2,3));
    }
    //status 测试 --------------------------------------------------------   end

    //ratio 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setRatio() 正确的传参类型,期望传值正确
     */
    public function testSetRatioCorrectType()
    {
        $this->workOrderTask->setRatio(0);
        $this->assertEquals(0, $this->workOrderTask->getRatio());
    }

    /**
     * 设置 WorkOrderTask setRatio() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRatioWrongType()
    {
        $this->workOrderTask->setRatio(array(1,2,3));
    }
    //ratio 测试 --------------------------------------------------------   end
    
    //reason 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setReason() 正确的传参类型,期望传值正确
     */
    public function testSetReasonCorrectType()
    {
        $this->workOrderTask->setReason('string');
        $this->assertEquals('string', $this->workOrderTask->getReason());
    }

    /**
     * 设置 WorkOrderTask setReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReasonWrongType()
    {
        $this->workOrderTask->setReason(array(1,2,3));
    }
    //reason 测试 --------------------------------------------------------   end

    //description 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->workOrderTask->setDescription('string');
        $this->assertEquals('string', $this->workOrderTask->getDescription());
    }

    /**
     * 设置 WorkOrderTask setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->workOrderTask->setDescription(array(1,2,3));
    }
    //description 测试 --------------------------------------------------------   end

    //finishCount 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setFinishCount() 正确的传参类型,期望传值正确
     */
    public function testSetFinishCountCorrectType()
    {
        $this->workOrderTask->setFinishCount(0);
        $this->assertEquals(0, $this->workOrderTask->getFinishCount());
    }

    /**
     * 设置 WorkOrderTask setFinishCount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFinishCountWrongType()
    {
        $this->workOrderTask->setFinishCount(array(1,2,3));
    }
    //finishCount 测试 --------------------------------------------------------   end
    
    //assignObjects 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setAssignObjects() 正确的传参类型,期望传值正确
     */
    public function testSetAssignObjectsCorrectType()
    {
        $this->workOrderTask->setAssignObjects(array());
        $this->assertEquals(array(), $this->workOrderTask->getAssignObjects());
    }

    /**
     * 设置 WorkOrderTask setAssignObjects() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAssignObjectsWrongType()
    {
        $this->workOrderTask->setAssignObjects('string');
    }
    //assignObjects 测试 --------------------------------------------------------   end

    //attachment 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setAttachment() 正确的传参类型,期望传值正确
     */
    public function testSetAttachmentCorrectType()
    {
        $this->workOrderTask->setAttachment(array());
        $this->assertEquals(array(), $this->workOrderTask->getAttachment());
    }

    /**
     * 设置 WorkOrderTask setAttachment() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAttachmentWrongType()
    {
        $this->workOrderTask->setAttachment('string');
    }
    //attachment 测试 --------------------------------------------------------   end

    //templateType 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setTemplateType() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateTypeCorrectType()
    {
        $this->workOrderTask->setTemplateType(0);
        $this->assertEquals(0, $this->workOrderTask->getTemplateType());
    }

    /**
     * 设置 WorkOrderTask setTemplateType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateTypeWrongType()
    {
        $this->workOrderTask->setTemplateType(array(1,2,3));
    }
    //templateType 测试 --------------------------------------------------------   end

    //endTime 测试 -------------------------------------------------------- start
    /**
     * 设置 WorkOrderTask setEndTime() 正确的传参类型,期望传值正确
     */
    public function testSetEndTimeCorrectType()
    {
        $this->workOrderTask->setEndTime('0000-00-00');
        $this->assertEquals('0000-00-00', $this->workOrderTask->getEndTime());
    }

    /**
     * 设置 WorkOrderTask setEndTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEndTimeWrongType()
    {
        $this->workOrderTask->setEndTime(array(1,2,3));
    }
    //endTime 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Repository\ParentTaskRepository',
            $this->workOrderTask->getRepository()
        );
    }

    public function testAssign()
    {
        $this->stub = $this->getMockBuilder(ParentTask::class)
            ->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(ParentTaskRepository::class);
        $repository->assign(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->assign();
        $this->assertTrue($result);
    }

    public function testRevoke()
    {
        $this->stub = $this->getMockBuilder(ParentTask::class)
            ->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(ParentTaskRepository::class);
        $repository->revoke(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->revoke();
        $this->assertTrue($result);
    }
}
