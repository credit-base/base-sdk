<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

class WorkOrderTaskCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;
    
    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        $this->commandHandler = new WorkOrderTaskCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Basecode\Classes\NullCommandHandler', $commandHandler);
    }
}
