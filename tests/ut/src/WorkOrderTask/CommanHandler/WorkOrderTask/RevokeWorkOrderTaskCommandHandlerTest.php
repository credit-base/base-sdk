<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;
use Base\Sdk\WorkOrderTask\Command\WorkOrderTask\RevokeWorkOrderTaskCommand;
use Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

class RevokeWorkOrderTaskCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(RevokeWorkOrderTaskCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockRevokeWorkOrderTaskCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository',
            $commandHandler->getRepository()
        );
    }

    public function testExecute()
    {
        $command = new RevokeWorkOrderTaskCommand(
            $this->faker->randomNumber(),
            $this->faker->word
        );

        $workOrderTask = $this->prophesize(WorkOrderTask::class);
        $workOrderTask->setReason(Argument::exact($command->reason))->shouldBeCalledTimes(1);
        $workOrderTask->revoke()->shouldBeCalledTimes(1)->willReturn(true);

        $workOrderTaskRepository = $this->prophesize(WorkOrderTaskRepository::class);
        $workOrderTaskRepository->fetchOne(Argument::exact($command->id))
            ->shouldBeCalledTimes(1)
            ->willReturn($workOrderTask->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($workOrderTaskRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = new RevokeWorkOrderTaskCommand(
            $this->faker->randomNumber(),
            $this->faker->name
        );

        $workOrderTask = $this->prophesize(WorkOrderTask::class);
        $workOrderTask->setReason(Argument::exact($command->reason))->shouldBeCalledTimes(1);
        $workOrderTask->revoke()->shouldBeCalledTimes(1)->willReturn(false);

        $workOrderTaskRepository = $this->prophesize(WorkOrderTaskRepository::class);
        $workOrderTaskRepository->fetchOne(Argument::exact($command->id))
            ->shouldBeCalledTimes(1)
            ->willReturn($workOrderTask->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($workOrderTaskRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }
}
