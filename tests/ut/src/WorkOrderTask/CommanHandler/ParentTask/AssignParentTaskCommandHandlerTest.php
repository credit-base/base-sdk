<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\ParentTask;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\Repository\GbTemplateRepository;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\WorkOrderTask\Command\ParentTask\AssignParentTaskCommand;
use Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

class AssignParentTaskCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AssignParentTaskCommandHandler::class)
            ->setMethods(['getParentTask'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetParentTask()
    {
        $commandHandler = new MockAssignParentTaskCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Model\ParentTask',
            $commandHandler->getParentTask()
        );
    }

    public function testGetUserGroupRepository()
    {
        $commandHandler = new MockAssignParentTaskCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Repository\UserGroupRepository',
            $commandHandler->getUserGroupRepository()
        );
    }

    public function testGetTemplateRepository()
    {
        $commandHandler = new MockAssignParentTaskCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\GbTemplateRepository',
            $commandHandler->getTemplateRepository(ParentTask::TEMPLATE_TYPE['GB'])
        );
    }

    public function testExecuteAction()
    {
        $commandHandler = $this->getMockBuilder(MockAssignParentTaskCommandHandler::class)
            ->setMethods([
                'getParentTask',
                'getTemplateRepository',
                'getUserGroupRepository',
            ])->getMock();
        $parentTask = $this->prophesize(ParentTask::class);
        $commandHandler->expects($this->exactly(1))
            ->method('getParentTask')
            ->willReturn($parentTask->reveal());

        $command = new AssignParentTaskCommand(
            $this->faker->name,
            $this->faker->word,
            $this->faker->word,
            array(),
            array(),
            ParentTask::TEMPLATE_TYPE['GB'],
            $this->faker->randomNumber()
        );
        $parentTask->setTitle(Argument::exact($command->title))->shouldBeCalledTimes(1);
        $parentTask->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $parentTask->setEndTime(Argument::exact($command->endTime))->shouldBeCalledTimes(1);
        $parentTask->setAttachment(Argument::exact($command->attachment))->shouldBeCalledTimes(1);
        $parentTask->setTemplateType(Argument::exact($command->templateType))->shouldBeCalledTimes(1);

        $templateRepository = $this->prophesize(GbTemplateRepository::class);
        $template = new GbTemplate();
        $templateRepository->fetchOne(Argument::exact($command->template))
            ->shouldBeCalledTimes(1)
            ->willReturn($template);
        $commandHandler->expects($this->exactly(1))
            ->method('getTemplateRepository')
            ->willReturn($templateRepository->reveal());
        $parentTask->setTemplate(Argument::exact($template))->shouldBeCalledTimes(1);

        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $assignObjects = [];
        $userGroupRepository->fetchList(Argument::exact($command->assignObjects))
            ->shouldBeCalledTimes(1)
            ->willReturn($assignObjects);
        $commandHandler->expects($this->exactly(1))
            ->method('getUserGroupRepository')
            ->willReturn($userGroupRepository->reveal());
        $parentTask->setAssignObjects(Argument::exact($assignObjects))->shouldBeCalledTimes(1);

        $parentTask->assign()->shouldBeCalledTimes(1)->willReturn(true);

        $this->assertTrue($commandHandler->executeAction($command));
    }
}
