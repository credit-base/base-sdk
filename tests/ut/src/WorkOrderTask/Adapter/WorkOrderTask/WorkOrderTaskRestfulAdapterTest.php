<?php
namespace Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask;

use Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;
use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;
use Base\Sdk\WorkOrderTask\Model\NullWorkOrderTask;
use Base\Sdk\WorkOrderTask\Translator\WorkOrderTaskRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class WorkOrderTaskRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(WorkOrderTaskRestfulAdapter::class)
            ->setMethods([
                'getTranslator',
                'translateToObject',
                'objectToArray',
                'getResource',
                'fetchOneAction',
                'post',
                'get',
                'isSuccess',
                'patch'
            ])
            ->getMock();
                           
        $this->childAdapter = new class extends WorkOrderTaskRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIWorkOrderTaskAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('workOrderTasks', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'WORK_ORDER_TASK_LIST',
                WorkOrderTaskRestfulAdapter::SCENARIOS['WORK_ORDER_TASK_LIST']
            ],
            [
                'WORK_ORDER_TASK_FETCH_ONE',
                WorkOrderTaskRestfulAdapter::SCENARIOS['WORK_ORDER_TASK_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullWorkOrderTask())
            ->willReturn($workOrderTask);
        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($workOrderTask, $result);
    }

    /**
     * 为WorkOrderTaskRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$workOrderTask,$keys,$workOrderTaskArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareWorkOrderTaskTranslator(
        WorkOrderTask $workOrderTask,
        array $keys,
        array $workOrderTaskArray
    ) {
        $translator = $this->prophesize(WorkOrderTaskRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($workOrderTask),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($workOrderTaskArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(WorkOrderTask $workOrderTask)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($workOrderTask);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWorkOrderTaskTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行end（）
     * 判断 result 是否为true
     */
    public function testEndSuccess()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $workOrderTaskArray = array();

        $this->prepareWorkOrderTaskTranslator(
            $workOrderTask,
            array('reason'),
            $workOrderTaskArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$workOrderTask->getId().'/end', $workOrderTaskArray);

        $this->success($workOrderTask);
        $result = $this->adapter->end($workOrderTask);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行end（）
     * 判断 result 是否为false
     */
    public function testEndFailure()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $workOrderTaskArray = array();

        $this->prepareWorkOrderTaskTranslator(
            $workOrderTask,
            array('reason'),
            $workOrderTaskArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$workOrderTask->getId().'/end', $workOrderTaskArray);
        
            $this->failure($workOrderTask);
            $result = $this->adapter->end($workOrderTask);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWorkOrderTaskTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行confirm（）
     * 判断 result 是否为true
     */
    public function testConfirmSuccess()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $workOrderTaskArray = array();

        $this->adapter
            ->method('patch')
            ->with('/'.$workOrderTask->getId().'/confirm', $workOrderTaskArray);

        $this->success($workOrderTask);
        $result = $this->adapter->confirm($workOrderTask);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行confirm（）
     * 判断 result 是否为false
     */
    public function testConfirmFailure()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $workOrderTaskArray = array();

        $this->adapter
            ->method('patch')
            ->with('/'.$workOrderTask->getId().'/confirm', $workOrderTaskArray);
        
            $this->failure($workOrderTask);
            $result = $this->adapter->confirm($workOrderTask);
            $this->assertFalse($result);
    }

     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWorkOrderTaskTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行revoke（）
     * 判断 result 是否为true
     */
    public function testRevokeSuccess()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $workOrderTaskArray = array();

        $this->prepareWorkOrderTaskTranslator(
            $workOrderTask,
            array('reason'),
            $workOrderTaskArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$workOrderTask->getId().'/revoke', $workOrderTaskArray);

        $this->success($workOrderTask);
        $result = $this->adapter->revoke($workOrderTask);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行revoke（）
     * 判断 result 是否为false
     */
    public function testRevokeFailure()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $workOrderTaskArray = array();

        $this->prepareWorkOrderTaskTranslator(
            $workOrderTask,
            array('reason'),
            $workOrderTaskArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$workOrderTask->getId().'/revoke', $workOrderTaskArray);
        
            $this->failure($workOrderTask);
            $result = $this->adapter->revoke($workOrderTask);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $stub = $this->getMockBuilder(MockWorkOrderTaskRestfulAdapter::class)
            ->setMethods(['commonMapErrors'])->getMock();

        $stub->expects($this->exactly(1))->method('commonMapErrors')->willReturn(array());

        $result = [
            101 => [
                'feedbackRecords' => FEEDBACK_RECORDS_FORMAT_ERROR,
                'crew' => CREW_FORMAT_ERROR,
                'userGroup' => USER_GROUP_FORMAT_ERROR,
                'reason' => REASON_FORMAT_ERROR,
                'templateId' => TEMPLATE_ID_FORMAT_ERROR,
                'items' => ITEMS_FORMAT_ERROR,
                'isExistedTemplate' => IS_EXISTED_TEMPLATE_PARAMETER_FORMAT_ERROR,
                'identify' => IDENTIFY_FORMAT_ERROR,
                'ZTMC' => ZTMC_FORMAT_ERROR,
                'TYSHXYDM'=>TYSHXYDM_FORMAT_ERROR,
                'type' => ITEM_TYPE_FORMAT_ERROR,
                'length' => LENGTH_FORMAT_ERROR,
                'isNecessary' => IS_NECESSARY_FORMAT_ERROR,
                'isMasked' => IS_MASKED_FORMAT_ERROR,
                'options' => OPTIONS_FORMAT_ERROR
            ]
        ];

        $this->assertEquals($result, $stub->getMapErrors());
    }


    public function testFeedback()
    {
        $workOrderTask = MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $this->initFeedback($workOrderTask, true);
        $this->assertTrue($this->adapter->feedback($workOrderTask));
    }
    public function testFeedbackFailed()
    {
        $workOrderTask = MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $this->initFeedback($workOrderTask, false);
        $this->assertFalse($this->adapter->feedback($workOrderTask));
    }

    protected function initFeedback($workOrderTask, $success)
    {
        $this->adapter = $this->getMockBuilder(MockWorkOrderTaskRestfulAdapter::class)
            ->setMethods([
                'getTranslator',
                'patch',
                'getResource',
                'isSuccess',
                'translateToObject',
            ])->getMock();


        $resource = 'workOrderTasks';

        $translator = $this->prophesize(WorkOrderTaskRestfulTranslator::class);
        $translator->objectToArray(
            $workOrderTask,
            array('feedbackRecords')
        )->shouldBeCalledTimes(1)
            ->willReturn(array());

        $this->adapter->expects($this->exactly(1))
            ->method('getResource')
            ->willReturn($resource);
        $this->adapter->expects($this->exactly(1))->method('patch');
        $this->adapter->expects($this->exactly(1))->method('isSuccess')->willReturn($success);
        $this->adapter->expects($this->exactly(1))->method('getTranslator')
            ->willReturn($translator->reveal());

        if ($success) {
            $this->adapter->expects($this->exactly(1))->method('translateToObject');
        }
    }
}
