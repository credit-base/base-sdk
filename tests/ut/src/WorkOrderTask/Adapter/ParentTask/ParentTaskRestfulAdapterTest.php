<?php
namespace Base\Sdk\WorkOrderTask\Adapter\ParentTask;

use phpDocumentor\Reflection\Types\Parent_;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;
use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\WorkOrderTask\Model\NullParentTask;
use Base\Sdk\WorkOrderTask\Translator\ParentTaskRestfulTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class ParentTaskTaskRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(ParentTaskRestfulAdapter::class)
            ->setMethods([
                'getTranslator',
                'translateToObject',
                'objectToArray',
                'getResource',
                'fetchOneAction',
                'post',
                'get',
                'isSuccess',
                'patch'
            ])
            ->getMock();
                           
        $this->childAdapter = new class extends ParentTaskRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }

            public function getMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIParentTaskAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('parentTasks', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'PARENT_TASK_LIST',
                ParentTaskRestfulAdapter::SCENARIOS['PARENT_TASK_LIST']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $parentTask = \Base\Sdk\WorkOrderTask\Utils\MockParentTaskFactory::generateParentTask(1);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullParentTask())
            ->willReturn($parentTask);
        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($parentTask, $result);
    }

    /**
     * 为ParentTaskRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$parentTask,$keys,$parentTaskArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareParentTaskTranslator(
        ParentTask $parentTask,
        array $keys,
        array $parentTaskArray
    ) {
        $translator = $this->prophesize(ParentTaskRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($parentTask),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($parentTaskArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(ParentTask $parentTask)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($parentTask);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareParentTaskTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行assign（）
     * 判断 result 是否为true
     */
    public function testAssignSuccess()
    {
        $parentTask = \Base\Sdk\WorkOrderTask\Utils\MockParentTaskFactory::generateParentTask(1);
        $parentTaskArray = array();

        $this->prepareParentTaskTranslator(
            $parentTask,
            array(
                'title',
                'description',
                'attachment',
                'assignObjects',
                'templateType',
                'template',
                'endTime'
            ),
            $parentTaskArray
        );

        $this->adapter
            ->method('post')
            ->with('', $parentTaskArray);

        $this->success($parentTask);
        $result = $this->adapter->assign($parentTask);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行assign（）
     * 判断 result 是否为false
     */
    public function testAssignFailure()
    {
        $parentTask = \Base\Sdk\WorkOrderTask\Utils\MockParentTaskFactory::generateParentTask(1);
        $parentTaskArray = array();

        $this->prepareParentTaskTranslator(
            $parentTask,
            array(
                'title',
                'description',
                'attachment',
                'assignObjects',
                'templateType',
                'template',
                'endTime'
            ),
            $parentTaskArray
        );

        $this->adapter
            ->method('post')
            ->with('', $parentTaskArray);
        
            $this->failure($parentTask);
            $result = $this->adapter->assign($parentTask);
            $this->assertFalse($result);
    }

     /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareParentTaskTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行revoke（）
     * 判断 result 是否为true
     */
    public function testRevokeSuccess()
    {
        $parentTask = \Base\Sdk\WorkOrderTask\Utils\MockParentTaskFactory::generateParentTask(1);
        $parentTaskArray = array();

        $this->prepareParentTaskTranslator(
            $parentTask,
            array('reason'),
            $parentTaskArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$parentTask->getId().'/revoke', $parentTaskArray);

        $this->success($parentTask);
        $result = $this->adapter->revoke($parentTask);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepartmentTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行revoke（）
     * 判断 result 是否为false
     */
    public function testRevokeFailure()
    {
        $parentTask = \Base\Sdk\WorkOrderTask\Utils\MockParentTaskFactory::generateParentTask(1);
        $parentTaskArray = array();

        $this->prepareParentTaskTranslator(
            $parentTask,
            array('reason'),
            $parentTaskArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$parentTask->getId().'/revoke', $parentTaskArray);
        
            $this->failure($parentTask);
            $result = $this->adapter->revoke($parentTask);
            $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $stub = $this->getMockBuilder(MockParentTaskRestfulAdapter::class)
            ->setMethods(['commonMapErrors'])->getMock();

        $stub->expects($this->exactly(1))->method('commonMapErrors')->willReturn(array());

        $result = [
            101 => [
                'title' => TITLE_FORMAT_ERROR,
                'templateType' => TEMPLATE_TYPE_FORMAT_ERROR,
                'description' => DESCRIPTION_FORMAT_ERROR,
                'endTime' => END_TIME_FORMAT_ERROR,
                'reason' => REASON_FORMAT_ERROR,
                'attachment' => ATTACHMENT_FORMAT_ERROR
            ]
        ];

        $this->assertEquals($result, $stub->getMapErrors());
    }
}
