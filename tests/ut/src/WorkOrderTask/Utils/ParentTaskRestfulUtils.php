<?php
namespace Base\Sdk\WorkOrderTask\Utils;

trait ParentTaskRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $parentTask
    ) {
        $this->assertEquals($expectedArray['data']['id'], $parentTask->getId());
        $this->assertEquals($expectedArray['data']['attributes']['title'], $parentTask->getTitle());
        $this->assertEquals($expectedArray['data']['attributes']['description'], $parentTask->getDescription());
        $this->assertEquals($expectedArray['data']['attributes']['attachment'], $parentTask->getAttachment());
        $this->assertEquals($expectedArray['data']['attributes']['templateType'], $parentTask->getTemplateType());
        $this->assertEquals($expectedArray['data']['attributes']['endTime'], $parentTask->getEndTime());
        $this->assertEquals($expectedArray['data']['attributes']['reason'], $parentTask->getReason());
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $parentTask->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $parentTask->getUpdateTime());
        }
        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals($expectedArray['data']['attributes']['status'], $parentTask->getStatus());
        }
    }
}
