<?php
namespace Base\Sdk\WorkOrderTask\Utils;

trait WorkOrderTaskUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $workOrderTask
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($workOrderTask->getId()));
        $this->assertEquals($expectedArray['reason'], $workOrderTask->getReason());
        $this->assertEquals($expectedArray['feedbackRecords'], $workOrderTask->getFeedbackRecords());
        $this->assertEquals($expectedArray['createTime'], $workOrderTask->getCreateTime());
        $this->assertEquals($expectedArray['updateTime'], $workOrderTask->getUpdateTime());
    }
}
