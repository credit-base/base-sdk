<?php
namespace Base\Sdk\WorkOrderTask\Utils;

trait ParentTaskUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $parentTask
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($parentTask->getId()));
        $this->assertEquals($expectedArray['title'], $parentTask->getTitle());
        $this->assertEquals($expectedArray['description'], $parentTask->getDescription());
        $this->assertEquals($expectedArray['attachment'], $parentTask->getAttachment());
        $this->assertEquals($expectedArray['templateType'], $parentTask->getTemplateType());
        $this->assertEquals($expectedArray['endTime'], $parentTask->getEndTime());
        $this->assertEquals($expectedArray['reason'], $parentTask->getReason());
        $this->assertEquals($expectedArray['status'], $parentTask->getStatus());
        $this->assertEquals($expectedArray['createTime'], $parentTask->getCreateTime());
        $this->assertEquals($expectedArray['updateTime'], $parentTask->getUpdateTime());
    }
}
