<?php
namespace Base\Sdk\WorkOrderTask\Utils;

trait WorkOrderTaskRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $workOrderTask
    ) {
        $this->assertEquals($expectedArray['data']['id'], $workOrderTask->getId());
        $this->assertEquals($expectedArray['data']['attributes']['reason'], $workOrderTask->getReason());
        if (isset($expectedArray['data']['attributes']['feedbackRecords'])) {
            $this->assertEquals(
                $expectedArray['data']['attributes']['feedbackRecords'],
                $workOrderTask->getFeedbackRecords()
            );
        }
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $workOrderTask->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $workOrderTask->getUpdateTime());
        }
        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals($expectedArray['data']['attributes']['status'], $workOrderTask->getStatus());
        }
    }
}
