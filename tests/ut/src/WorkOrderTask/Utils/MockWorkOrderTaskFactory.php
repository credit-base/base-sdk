<?php
namespace Base\Sdk\WorkOrderTask\Utils;

use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;
use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\Template\Model\Template;

class MockWorkOrderTaskFactory
{
    public static function generateWorkOrderTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WorkOrderTask {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $workOrderTask = new WorkOrderTask($id);
        $workOrderTask->setId($id);

        //reason
        self::generateReason($workOrderTask, $faker, $value);
        //feedbackRecords
        self::generateFeedbackRecords($workOrderTask, $faker, $value);
        // //isExistedTemplate
        // self::generateIsExistedTemplate($workOrderTask, $faker, $value);
        // //parentTask
        // self::generateParentTask($workOrderTask, $faker, $value);
        // //template
        // self::generateTemplate($workOrderTask, $faker, $value);
        // //assignObject
        // self::generateAssignObject($workOrderTask, $faker, $value);
        
        $workOrderTask->setStatus(0);
        $workOrderTask->setUpdateTime($faker->unixTime());
        $workOrderTask->setCreateTime($faker->unixTime());
        // $workOrderTask->setStatusTime($faker->unixTime());

        return $workOrderTask;
    }

    private static function generateReason($workOrderTask, $faker, $value) : void
    {
        $reason = isset($value['reason']) ?
            $value['reason'] :
            $faker->title;
        
        $workOrderTask->setReason($reason);
    }

    private static function generateFeedbackRecords($workOrderTask, $faker, $value) : void
    {
        unset($faker);
        $feedbackRecords = isset($value['feedbackRecords']) ?
            $value['feedbackRecords'] : array();
        
        $workOrderTask->setFeedbackRecords($feedbackRecords);
    }

    // private static function generateParentTask($workOrderTask, $faker, $value) : void
    // {
    //     unset($faker);

    //     $parentTask = isset($value['parentTask']) ?
    //         new ParentTask($value['parentTask']) :
    //         new ParentTask();
    //     $workOrderTask->setParentTask($parentTask);
    // }

    // private static function generateTemplate($workOrderTask, $faker, $value) : void
    // {
    //     unset($faker);

    //     $template = isset($value['template']) ?
    //         new Template($value['template']) :
    //         new Template();
    //     $workOrderTask->setTemplate($template);
    // }

    // private static function generateAssignObject($workOrderTask, $faker, $value) : void
    // {
    //     unset($faker);

    //     $assignObject = isset($value['assignObject']) ?
    //         new UserGroup($value['assignObject']) :
    //         new UserGroup();
    //     $workOrderTask->setAssignObject($assignObject);
    // }

    // private static function generateIsExistedTemplate($workOrderTask, $faker, $value) : void
    // {
    //     unset($faker);
    //     $isExistedTemplate = isset($value['isExistedTemplate']) ?
    //         $value['isExistedTemplate'] : 1;
        
    //     $workOrderTask->setIsExistedTemplate($isExistedTemplate);
    // }
}
