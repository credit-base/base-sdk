<?php
namespace Base\Sdk\WorkOrderTask\Utils;

use Base\Sdk\UserGroup\Utils\MockUserGroupFactory;
use Base\Sdk\WorkOrderTask\Model\ParentTask;

class MockParentTaskFactory
{
    public static function generateParentTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ParentTask {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $parentTask = new ParentTask($id);
        $parentTask->setId($id);

        //title
        self::generateTitle($parentTask, $faker, $value);
        //description
        self::generateDescription($parentTask, $faker, $value);
        //attachment
        self::generateAttachment($parentTask, $faker, $value);
        //templateType
        self::generateTemplateType($parentTask, $faker, $value);
        //endTime
        self::generateEndTime($parentTask, $faker, $value);

        $parentTask->setAssignObjects(
            [
                1,
                [
                    MockUserGroupFactory::generateUserGroup(1)
                ]
            ]
        );

        $parentTask->setStatus(0);
        $parentTask->setCreateTime($faker->unixTime());
        $parentTask->setUpdateTime($faker->unixTime());
        $parentTask->setStatusTime($faker->unixTime());

        return $parentTask;
    }

    private static function generateTitle($parentTask, $faker, $value) : void
    {
        $title = isset($value['title']) ?
            $value['title'] :
            $faker->title;
        
        $parentTask->setTitle($title);
    }

    private static function generateDescription($parentTask, $faker, $value) : void
    {
        $description = isset($value['description']) ?
            $value['description'] :
            $faker->title;
        
        $parentTask->setDescription($description);
    }

    private static function generateTemplateType($parentTask, $faker, $value) : void
    {

        $templateType = isset($value['templateType']) ?
            $value['templateType'] :
            ParentTask::TEMPLATE_TYPE['GB'];
        unset($faker);
        $parentTask->setTemplateType($templateType);
    }

    private static function generateEndTime($parentTask, $faker, $value) : void
    {
        $endTime = isset($value['endTime']) ?
            $value['endTime'] :
            $faker->randomNumber;
        
        $parentTask->setEndTime($endTime);
    }

    private static function generateAttachment($parentTask, $faker, $value) : void
    {
        unset($faker);
        $attachment = isset($value['attachment']) ?
            $value['attachment'] :
            array();
        
        $parentTask->setAttachment($attachment);
    }
}
