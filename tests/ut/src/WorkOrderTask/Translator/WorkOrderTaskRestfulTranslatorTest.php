<?php

namespace Base\Sdk\WorkOrderTask\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\WorkOrderTask\Utils\WorkOrderTaskRestfulUtils;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;
use Base\Sdk\WorkOrderTask\Translator\ParentTaskRestfulTranslator;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class WorkOrderTaskRestfulTranslatorTest extends TestCase
{
    use WorkOrderTaskRestfulUtils;

    private $translator;
    private $childTranslator;

    public function setUp()
    {
        $this->translator = new WorkOrderTaskRestfulTranslator();
        $this->childTranslator = new class extends WorkOrderTaskRestfulTranslator {
            public function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }

            public function getParentTaskRestfulTranslator(): ParentTaskRestfulTranslator
            {
                return parent::getParentTaskRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetTranslatorFactory()
    {
        $stub = new MockWorkOrderTaskRestfulTranslator();
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Translator\TranslatorFactory',
            $stub->getTranslatorFactory()
        );
    }

    public function testArrayToObject()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);

        $expression['data']['id'] = $workOrderTask->getId();
        $expression['data']['attributes']['reason'] = $workOrderTask->getReason();
        $expression['data']['attributes']['feedbackRecords'] = $workOrderTask->getFeedbackRecords();
        $expression['data']['attributes']['updateTime'] = $workOrderTask->getUpdateTime();
        $expression['data']['attributes']['status'] = $workOrderTask->getStatus();

        $workOrderTaskObject = $this->translator->arrayToObject(
            $expression
        );
        $this->assertInstanceof('Base\Sdk\WorkOrderTask\Model\WorkOrderTask', $workOrderTaskObject);
        $this->compareArrayAndObject($expression, $workOrderTaskObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $workOrderTask = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\WorkOrderTask\Model\NullWorkOrderTask', $workOrderTask);
    }

    public function testObjectToArray()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);

        $expression = $this->translator->objectToArray($workOrderTask);

        $this->compareArrayAndObject($expression, $workOrderTask);
    }

    public function testObjectToArrayFail()
    {
        $workOrderTask = null;

        $expression = $this->translator->objectToArray($workOrderTask);
        $this->assertEquals(array(), $expression);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetParentTaskRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Translator\ParentTaskRestfulTranslator',
            $this->childTranslator->getParentTaskRestfulTranslator()
        );
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function getExpression($included = 1) : array
    {
        $expression = [
            "data" => [
                "type" => "workOrderTasks",
                "id" => "1",
                "attributes" => [
                    "reason" => "",
                    "feedbackRecords" => [
                        [
                            "crew" => "43",
                            "items" => [
                                [
                                    "name" => "主体名称",
                                    "type" => "2",
                                    "length" => "100",
                                    "options" => [

                                    ],
                                    "remarks" => "",
                                    "identify" => "ZTMC",
                                    "isMasked" => "1",
                                    "maskRule" => [

                                    ],
                                    "dimension" => "1",
                                    "isNecessary" => "1"
                                ],
                                [
                                    "name" => "统一社会信用代码",
                                    "type" => "1",
                                    "length" => "13",
                                    "options" => [

                                    ],
                                    "remarks" => "",
                                    "identify" => "TYSHXYDM",
                                    "isMasked" => "1",
                                    "maskRule" => [
                                        "2",
                                        "4"
                                    ],
                                    "dimension" => "1",
                                    "isNecessary" => "0"
                                ],
                                [
                                    "name" => "类型",
                                    "type" => "1",
                                    "length" => "201",
                                    "options" => [

                                    ],
                                    "remarks" => "remarks",
                                    "identify" => "LX",
                                    "isMasked" => "0",
                                    "maskRule" => [

                                    ],
                                    "dimension" => "1",
                                    "isNecessary" => "1"
                                ]
                            ],
                            "reason" => "反馈反馈反馈反馈",
                            "crewName" => "平台操作人员",
                            "userGroup" => "8",
                            "templateId" => "",
                            "feedbackTime" => "1615365541",
                            "userGroupName" => "市场监督管理局",
                            "isExistedTemplate" => "0"
                        ],
                        [
                            "crew" => "1",
                            "items" => [
                                [
                                    "name" => "主体名称",
                                    "type" => "1",
                                    "length" => "100",
                                    "options" => [

                                    ],
                                    "remarks" => "",
                                    "identify" => "ZTMC",
                                    "isMasked" => "0",
                                    "maskRule" => [

                                    ],
                                    "dimension" => "1",
                                    "isNecessary" => "1"
                                ],
                                [
                                    "name" => "统一社会信用代码",
                                    "type" => "1",
                                    "length" => "18",
                                    "options" => [

                                    ],
                                    "remarks" => "",
                                    "identify" => "TYSHXYDM",
                                    "isMasked" => "1",
                                    "maskRule" => [
                                        "5",
                                        "4"
                                    ],
                                    "dimension" => "1",
                                    "isNecessary" => "1"
                                ],
                                [
                                    "name" => "类型",
                                    "type" => "1",
                                    "length" => "20",
                                    "options" => [

                                    ],
                                    "remarks" => "",
                                    "identify" => "LX",
                                    "isMasked" => "0",
                                    "maskRule" => [

                                    ],
                                    "dimension" => "1",
                                    "isNecessary" => "1"
                                ]
                            ],
                            "reason" => "测试反馈信息",
                            "crewName" => "张科",
                            "userGroup" => "1",
                            "templateId" => "",
                            "feedbackTime" => "1615440609",
                            "userGroupName" => "发展和改革委员会",
                            "isExistedTemplate" => "0"
                        ]
                    ],
                    "status" => 3,
                    "createTime" => 1615280264,
                    "updateTime" => 1615440609,
                    "statusTime" => 1615440609
                ],
                "relationships" => [
                    "template" => [
                        "data" => [
                            "type" => "gbTemplates",
                            "id" => "1"
                        ]
                    ],
                    "assignObject" => [
                        "data" => [
                            "type" => "userGroups",
                            "id" => "8"
                        ]
                    ],
                    "parentTask" => [
                        "data" => [
                            "type" => "parentTasks",
                            "id" => "1"
                        ]
                    ]
                ]
            ]
        ];

        if ($included == 1) {
            $expression['included'] = [
                [
                    "type" => "gbTemplates",
                    "id" => "1",
                    "attributes" => [
                        "name" => "登记信息",
                        "identify" => "DJXX",
                        "subjectCategory" => [
                            "1"
                        ],
                        "dimension" => 1,
                        "exchangeFrequency" => 1,
                        "infoClassify" => "5",
                        "infoCategory" => "4",
                        "description" => "登录信息目录描述",
                        "category" => 2,
                        "items" => [
                            [
                                "name" => "主体名称",
                                "type" => "1",
                                "length" => "200",
                                "options" => [

                                ],
                                "remarks" => "",
                                "identify" => "ZTMC",
                                "isMasked" => "0",
                                "maskRule" => [

                                ],
                                "dimension" => "1",
                                "isNecessary" => "1"
                            ],
                            [
                                "name" => "主体类别",
                                "type" => "1",
                                "length" => "2",
                                "options" => [

                                ],
                                "remarks" => "",
                                "identify" => "ZTLB",
                                "isMasked" => "0",
                                "maskRule" => [

                                ],
                                "dimension" => "1",
                                "isNecessary" => "1"
                            ],
                            [
                                "name" => "统一社会信用代码",
                                "type" => "1",
                                "length" => "18",
                                "options" => [

                                ],
                                "remarks" => "",
                                "identify" => "TYSHXYDM",
                                "isMasked" => "1",
                                "maskRule" => [
                                    "5",
                                    "4"
                                ],
                                "dimension" => "1",
                                "isNecessary" => "1"
                            ]
                        ],
                        "status" => 0,
                        "createTime" => 1615278291,
                        "updateTime" => 1615278291,
                        "statusTime" => 0
                    ]
                ],
                [
                    "type" => "userGroups",
                    "id" => "15",
                    "attributes" => [
                        "name" => "人力和资源社会保障局",
                        "shortName" => "",
                        "status" => 0,
                        "createTime" => 1630916764,
                        "updateTime" => 1630916764,
                        "statusTime" => 0
                    ]
                ],
                [
                    "type" => "userGroups",
                    "id" => "8",
                    "attributes" => [
                        "name" => "",
                        "shortName" => "",
                        "status" => 0,
                        "createTime" => 1630916764,
                        "updateTime" => 1630916764,
                        "statusTime" => 0
                    ]
                ],
                [
                    "type" => "userGroups",
                    "id" => "22",
                    "attributes" => [
                        "name" => "",
                        "shortName" => "",
                        "status" => 0,
                        "createTime" => 1630916764,
                        "updateTime" => 1630916764,
                        "statusTime" => 0
                    ]
                ],
                [
                    "type" => "parentTasks",
                    "id" => "1",
                    "attributes" => [
                        "templateType" => 1,
                        "title" => "任务1",
                        "description" => "描述1",
                        "endTime" => "2021-03-11",
                        "attachment" => [

                        ],
                        "ratio" => 33,
                        "reason" => "",
                        "status" => 0,
                        "createTime" => 1615280264,
                        "updateTime" => 1615281597,
                        "statusTime" => 0
                    ],
                    "relationships" => [
                        "template" => [
                            "data" => [
                                "type" => "gbTemplates",
                                "id" => "1"
                            ]
                        ],
                        "assignObjects" => [
                            "data" => [
                                [
                                    "type" => "userGroups",
                                    "id" => "8"
                                ],
                                [
                                    "type" => "userGroups",
                                    "id" => "22"
                                ],
                                [
                                    "type" => "userGroups",
                                    "id" => "15"
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        return $expression;
    }

    public function testArrayToObjectAll()
    {
        $expression = $this->getExpression();

        $workOrderTaskObject = $this->translator->arrayToObject(
            $expression
        );

        $this->assertInstanceof('Base\Sdk\WorkOrderTask\Model\WorkOrderTask', $workOrderTaskObject);
        $this->compareArrayAndObject($expression, $workOrderTaskObject);
    }

    public function testArrayToObjectNoIncluded()
    {
        $expression = $this->getExpression(0);

        $workOrderTaskObject = $this->translator->arrayToObject(
            $expression
        );

        $this->assertInstanceof('Base\Sdk\WorkOrderTask\Model\WorkOrderTask', $workOrderTaskObject);
        $this->compareArrayAndObject($expression, $workOrderTaskObject);
    }

    public function testTranslateToObjectFailed()
    {
        $this->translator = new MockWorkOrderTaskRestfulTranslator();
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Model\NullWorkOrderTask',
            $this->translator->translateToObject(array())
        );
    }
}
