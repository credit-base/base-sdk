<?php


namespace Base\Sdk\WorkOrderTask\Translator;

use Base\Sdk\Template\Translator\GbTemplateTranslator;
use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\WorkOrderTask\Utils\MockParentTaskFactory;
use PHPUnit\Framework\TestCase;

class ParentTaskTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockParentTaskTranslator();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testArrayToObject()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Model\NullParentTask',
            $this->stub->arrayToObject(array())
        );
    }

    public function testArrayToObjects()
    {
        $this->assertEquals(
            array(),
            $this->stub->arrayToObjects(array())
        );
    }

    public function testGetTranslatorFactory()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Translator\TranslatorFactory',
            $this->stub->getTranslatorFactory()
        );
    }

    public function testGetUserGroupTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupTranslator',
            $this->stub->getUserGroupTranslator()
        );
    }

    public function testObjectToArray()
    {
        $task = MockParentTaskFactory::generateParentTask(1);
        $task->setTemplateType(ParentTask::TEMPLATE_TYPE['GB']);
        $translator  = new GbTemplateTranslator();
        $expression = array(
            'id' => marmot_encode($task->getId()),
            'title' => $task->getTitle(),
            'description' => $task->getDescription(),
            'ratio' => $task->getRatio(),
            'attachment' => $task->getAttachment(),
            'templateType' => [
                'id' => $task->getTemplateType(),
                'name' => ParentTask::TEMPLATE_TYPE_CN[$task->getTemplateType()]
            ],
            'reason' => $task->getReason(),
            'endTime' => $task->getEndTime(),
            'status' => $task->getStatus(),
            'updateTime' => $task->getUpdateTime(),
            'createTime' => $task->getCreateTime(),
            'template' => $translator->objectToArray($task->getTemplate()),
        );

        $this->assertEquals(
            $expression,
            $this->stub->objectToArray($task)
        );
    }

    public function testObjectToArrayFailed()
    {
        $task = new class {
        };

        $this->assertEquals(
            array(),
            $this->stub->objectToArray($task)
        );
    }
}
