<?php
namespace Base\Sdk\WorkOrderTask\Translator;

use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\WorkOrderTask\Utils\MockParentTaskFactory;
use PHPUnit\Framework\TestCase;

use Base\Sdk\WorkOrderTask\Utils\WorkOrderTaskUtils;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class WorkOrderTaskTranslatorTest extends TestCase
{
    use WorkOrderTaskUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WorkOrderTaskTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\WorkOrderTask\Model\NullWorkOrderTask', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects(
            $expression
        );

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);

        $workOrderTask->setParentTask(
            MockParentTaskFactory::generateParentTask(1)
        );

        $expression = $this->translator->objectToArray(
            $workOrderTask
        );

        $this->compareArrayAndObject($expression, $workOrderTask);
    }
    public function testObjectToArrayWithFeedbackRecords()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $workOrderTask->setFeedbackRecords(
            [
                [
                    'items' => [
                        [
                            'isNecessary' => 1,
                            'isMasked' => 1,
                            'dimension' => 1,
                            'type' => 1
                        ],
                    ]
                ]
            ]
        );
        $expression = $this->translator->objectToArray(
            $workOrderTask,
            array('feedbackRecords')
        );

        $result =  [
            'feedbackRecords' => array(
                array(
                    'items' => array(
                        array(
                            'isNecessary' => ['id' => 'MA', 'name' => '是'],
                            'isMasked' => ['id' => 'MA', 'name' => '是'],
                            'dimension' => ['id' => 'MA', 'name' => '社会公开'],
                            'type' => ['id' => 'MA', 'name' => '字符型'],
                        )
                    )
                )
            )
        ];
        $this->assertEquals($result, $expression);
    }

    public function testObjectToArrayFail()
    {
        $workOrderTask = null;

        $expression = $this->translator->objectToArray($workOrderTask);
        $this->assertEquals(array(), $expression);
    }

    public function testGetTypeCn()
    {
        $array = BjTemplate::TYPE_CN;

        $stub = new MockWorkOrderTaskTranslator();
        foreach ($array as $type => $value) {
            $data = array(
                'id' => marmot_encode($type),
                'name' => $value
            );
            $this->assertEquals($data, $stub->getTypeCn($type));
        }
    }

    public function testGetDimensionCn()
    {
        $array = BjTemplate::DIMENSION_CN;

        $stub = new MockWorkOrderTaskTranslator();
        foreach ($array as $dimension => $value) {
            $data = array(
                'id' => marmot_encode($dimension),
                'name' => $value
            );
            $this->assertEquals($data, $stub->getDimensionCn($dimension));
        }
    }

    public function testGetIsMaskedCn()
    {
        $array = BjTemplate::IS_MASKED_CN;

        $stub = new MockWorkOrderTaskTranslator();
        foreach ($array as $isMask => $value) {
            $data = array(
                'id' => marmot_encode($isMask),
                'name' => $value
            );
            $this->assertEquals($data, $stub->getIsMaskedCn($isMask));
        }
    }

    public function testGetIsNecessaryCn()
    {
        $array = BjTemplate::IS_NECESSARY_CN;

        $stub = new MockWorkOrderTaskTranslator();
        foreach ($array as $isNecessary => $value) {
            $data = array(
                'id' => marmot_encode($isNecessary),
                'name' => $value
            );
            $this->assertEquals($data, $stub->getIsMaskedCn($isNecessary));
        }
    }

    public function testGetItemCn()
    {
        $stub = new MockWorkOrderTaskTranslator();

        $isNecessary = 1;
        $isMasked = 1;
        $dimension = 1;
        $type = 1;

        $items = [
            [
                'isNecessary' => $isNecessary,
                'isMasked' => $isMasked,
                'dimension' => $dimension,
                'type' => $type
            ],
        ];

        $result = [
            [
                'isNecessary' => array(
                    'id' => marmot_encode($isNecessary),
                    'name' => BjTemplate::IS_NECESSARY_CN[$isNecessary]
                ),

                'isMasked' => array(
                    'id' => marmot_encode($isMasked),
                    'name' => BjTemplate::IS_MASKED_CN[$isMasked]
                ),

                'dimension' => array(
                    'id' => marmot_encode($dimension),
                    'name' => BjTemplate::DIMENSION_CN[$dimension]
                ),

                'type' => array(
                    'id' => marmot_encode($type),
                    'name' => BjTemplate::TYPE_CN[$type]
                ),
            ]
        ];

        $this->assertEquals(
            $result,
            $stub->getItemCn($items)
        );
    }

    public function testFeedbackRecords()
    {
        $isNecessary = 1;
        $isMasked = 1;
        $dimension = 1;
        $type = 1;

        $feedbackRecords = [
            [
                'items' => [
                    [
                        'isNecessary' => $isNecessary,
                        'isMasked' => $isMasked,
                        'dimension' => $dimension,
                        'type' => $type
                    ],
                ]
            ]
        ];
        $feedbackRecordsResult = [
            array(
                'items' => [
                    [
                        'isNecessary' => array(
                            'id' => marmot_encode($isNecessary), 'name' => BjTemplate::IS_NECESSARY_CN[$isNecessary]
                        ),
                        'isMasked' => array(
                            'id' => marmot_encode($isMasked), 'name' => BjTemplate::IS_MASKED_CN[$isMasked]
                        ),
                        'dimension' => array(
                            'id' => marmot_encode($dimension), 'name' => BjTemplate::DIMENSION_CN[$dimension]
                        ),
                        'type' => array(
                            'id' => marmot_encode($type), 'name' => BjTemplate::TYPE_CN[$type]
                        ),
                    ],
                ]
            )
        ];

        $stub = new MockWorkOrderTaskTranslator();
        $this->assertEquals($feedbackRecordsResult, $stub->feedbackRecords($feedbackRecords));
    }
}
