<?php
namespace Base\Sdk\WorkOrderTask\Translator;

use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;
use PHPUnit\Framework\TestCase;

class NullRestfulTranslatorTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = MockNullRestfulTranslator::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testTranslatorNotExist()
    {
        $this->assertFalse($this->stub->translatorNotExist());
    }

    public function testArrayToObject()
    {
        $this->assertFalse($this->stub->arrayToObject([]));
    }

    public function testObjectToArray()
    {
        $this->assertEquals(
            array(),
            $this->stub->objectToArray(new WorkOrderTask())
        );
    }

    public function testArrayToObjects()
    {
        $this->assertEquals(
            array(),
            $this->stub->arrayToObjects(array())
        );
    }
}
