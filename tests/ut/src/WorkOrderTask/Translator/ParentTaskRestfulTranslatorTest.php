<?php


namespace Base\Sdk\WorkOrderTask\Translator;

use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\WorkOrderTask\Utils\ParentTaskRestfulUtils;
use PHPUnit\Framework\TestCase;

class ParentTaskRestfulTranslatorTest extends TestCase
{
    use ParentTaskRestfulUtils;

    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockParentTaskRestfulTranslator::class)->setMethods([])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testArrayToObject()
    {
        $expression = [];
        $this->stub = new MockParentTaskRestfulTranslator();
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Model\NullParentTask',
            $this->stub->arrayToObject($expression)
        );
    }

    public function testGetTranslatorFactory()
    {
        $this->stub = new MockParentTaskRestfulTranslator();
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Translator\TranslatorFactory',
            $this->stub->getTranslatorFactory()
        );
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->stub = new MockParentTaskRestfulTranslator();
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->stub->getUserGroupRestfulTranslator()
        );
    }

    public function testObjectToArrayFail()
    {
        $this->stub = new MockParentTaskRestfulTranslator();
        $parentTask = new class {
        };
        $this->assertEquals(array(), $this->stub->objectToArray($parentTask));
    }

    public function testTranslateToObjectFailed()
    {
        $this->stub = new MockParentTaskRestfulTranslator();
        $this->assertInstanceof(
            'Base\Sdk\WorkOrderTask\Model\NullParentTask',
            $this->stub->translateToObject(array())
        );
    }

    public function testObjectToArray()
    {
        $this->stub = new MockParentTaskRestfulTranslator();
        $parentTask = \Base\Sdk\WorkOrderTask\Utils\MockParentTaskFactory::generateParentTask(1);
        $expression = $this->stub->objectToArray($parentTask);

        $this->compareArrayAndObject($expression, $parentTask);
    }

    public function testTranslateToObject()
    {
        $this->stub = new MockParentTaskRestfulTranslator();

        $expression = $this->getExpression();
        $task = $this->getParentTask($expression);

        $this->assertInstanceof(
            'Base\Sdk\WorkOrderTask\Model\ParentTask',
            $this->stub->translateToObject($expression)
        );
        $this->compareArrayAndObject($expression, $task);
    }
    public function testTranslateToObjectWithNullInclude()
    {
        $this->stub = new MockParentTaskRestfulTranslator();

        $expression = $this->getExpression(0);
        $task = $this->getParentTask($expression);

        $this->assertInstanceof(
            'Base\Sdk\WorkOrderTask\Model\ParentTask',
            $this->stub->translateToObject($expression)
        );
        $this->compareArrayAndObject($expression, $task);
    }

    protected function getExpression($include = 1) : array
    {
        $expression = [
            "data" => [
                "type" => "parentTasks",
                "id" => "107",
                "attributes" => [
                    "templateType" => 1,
                    "title" => "测试任务",
                    "description" => "任务描述",
                    "endTime" => "2021-08-31",
                    "attachment" => [

                    ],
                    "ratio" => 66,
                    "reason" => "",
                    "status" => 0,
                    "createTime" => 1626330081,
                    "updateTime" => 1629534607,
                    "statusTime" => 0
                ],
                "relationships" => [
                    "template" => [
                        "data" => [
                            "type" => "gbTemplates",
                            "id" => "64"
                        ]
                    ],
                    "assignObjects" => [
                        "data" => [
                            [
                                "type" => "userGroups",
                                "id" => "15"
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if ($include) {
            $expression['included'] = [
                [
                    "type" => "gbTemplates",
                    "id" => "64",
                    "attributes" => [
                        "name" => "地方性黑名单信息",
                        "name" => "地方性黑名单信息",
                        "identify" => "",
                        "subjectCategory" => [],
                        "dimension" => 0,
                        "exchangeFrequency" => 0,
                        "infoClassify" => "0",
                        "infoCategory" => "0",
                        "description" => "",
                        "category" => 2,
                        "items" => [],
                        "status" => 0,
                        "createTime" => 1630906162,
                        "updateTime" => 1630906162,
                        "statusTime" => 0
                    ]
                ]
            ];
        }

        return $expression;
    }

    protected function getParentTask(array $expression) : ParentTask
    {
        $task = new ParentTask();
        $task->setId($expression['data']['id']);
        $task->setTitle($expression['data']['attributes']['title']);
        $task->setDescription($expression['data']['attributes']['description']);

        $task->setAttachment($expression['data']['attributes']['attachment']);
        $task->setTemplateType($expression['data']['attributes']['templateType']);
        $task->setEndTime($expression['data']['attributes']['endTime']);
        $task->setReason($expression['data']['attributes']['reason']);
        $task->setCreateTime($expression['data']['attributes']['createTime']);
        $task->setUpdateTime($expression['data']['attributes']['updateTime']);
        $task->setStatus($expression['data']['attributes']['status']);
        return $task;
    }
}
