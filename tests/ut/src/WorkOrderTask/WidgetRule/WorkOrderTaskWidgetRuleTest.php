<?php


namespace Base\Sdk\WorkOrderTask\WidgetRule;

use PHPUnit\Framework\TestCase;

class WorkOrderTaskWidgetRuleTest extends TestCase
{
    private $widgetRule;
    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        $this->widgetRule = WorkOrderTaskWidgetRule::getInstance();
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        unset($this->faker);
    }

    public function testTitle()
    {
        $this->assertFalse($this->widgetRule->title(''));
        $this->assertFalse($this->widgetRule->title(1));
        $this->assertTrue($this->widgetRule->title('title'));
        $string = '';
        for ($i=0; $i<=WorkOrderTaskWidgetRule::TITLE_MAX_LENGTH; $i++) {
            $string .= '题';
        }
        $this->assertFalse($this->widgetRule->title($string));
    }

    public function testFormatNumeric()
    {
        $this->assertFalse($this->widgetRule->formatNumeric($this->faker->name()));
        $this->assertTrue($this->widgetRule->formatNumeric($this->faker->randomNumber()));
    }

    public function testDescription()
    {
        $this->assertFalse($this->widgetRule->description(''));
        $this->assertFalse($this->widgetRule->description($this->faker->randomNumber()));
        $this->assertTrue($this->widgetRule->description(
            $this->faker->text(WorkOrderTaskWidgetRule::DESCRIPTION_MAX_LENGTH)
        ));
    }

    public function testReason()
    {
        $this->assertFalse($this->widgetRule->reason(''));
        $this->assertFalse($this->widgetRule->reason($this->faker->randomNumber()));
        $this->assertTrue($this->widgetRule->reason(
            $this->faker->text(WorkOrderTaskWidgetRule::REASON_MAX_LENGTH)
        ));
    }

    public function testAttachments()
    {
        $this->assertFalse($this->widgetRule->attachments($this->faker->randomNumber()));
        $this->assertFalse($this->widgetRule->attachments($this->faker->title()));

        $attachments = ['identify' => 'a.tar.gz'];
        $this->assertFalse($this->widgetRule->attachments($attachments));
        $attachments = ['identify' => 'a.pdf'];
        $this->assertTrue($this->widgetRule->attachments($attachments));
    }
}
