<?php
namespace Base\Sdk\WorkOrderTask\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskRestfulAdapter;

class WorkOrderTaskRepositoryTest extends TestCase
{
    private $repository;
    
    private $childRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(WorkOrderTaskRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();
                           
        $this->childRepository = new class extends WorkOrderTaskRepository
        {
            public function getAdapter()
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->childRepository);
    }

    public function testImplementsIWorkOrderTaskAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->repository
        );
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskRestfulAdapter',
            $this->childRepository->getAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(WorkOrderTaskRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
            ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testEnd()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $adapter = $this->prophesize(WorkOrderTaskRestfulAdapter::class);
        $adapter->end(Argument::exact($workOrderTask))->shouldBeCalledTimes(1)->willReturn(true);

        $this->repository->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->repository->end($workOrderTask);
        $this->assertTrue($result);
    }

    public function testRevoke()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $adapter = $this->prophesize(WorkOrderTaskRestfulAdapter::class);
        $adapter->revoke(Argument::exact($workOrderTask))->shouldBeCalledTimes(1)->willReturn(true);

        $this->repository->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->repository->revoke($workOrderTask);
        $this->assertTrue($result);
    }

    public function testConfirm()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $adapter = $this->prophesize(WorkOrderTaskRestfulAdapter::class);
        $adapter->confirm(Argument::exact($workOrderTask))->shouldBeCalledTimes(1)->willReturn(true);

        $this->repository->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->repository->confirm($workOrderTask);
        $this->assertTrue($result);
    }

    public function testFeedback()
    {
        $workOrderTask = \Base\Sdk\WorkOrderTask\Utils\MockWorkOrderTaskFactory::generateWorkOrderTask(1);
        $adapter = $this->prophesize(WorkOrderTaskRestfulAdapter::class);
        $adapter->feedback(Argument::exact($workOrderTask))->shouldBeCalledTimes(1)->willReturn(true);

        $this->repository->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->repository->feedback($workOrderTask);
        $this->assertTrue($result);
    }
}
