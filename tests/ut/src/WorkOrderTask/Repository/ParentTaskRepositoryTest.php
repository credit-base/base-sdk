<?php
namespace Base\Sdk\WorkOrderTask\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\WorkOrderTask\Adapter\ParentTask\ParentTaskRestfulAdapter;

class ParentTaskRepositoryTest extends TestCase
{
    private $repository;
    
    private $childRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(ParentTaskRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->childRepository = new class extends ParentTaskRepository
        {
            public function getAdapter()
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->childRepository);
    }

    public function testImplementsIParentTaskAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $this->repository
        );
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\WorkOrderTask\Adapter\ParentTask\ParentTaskRestfulAdapter',
            $this->childRepository->getAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(ParentTaskRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
            ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testAssign()
    {
        $parentTask = \Base\Sdk\WorkOrderTask\Utils\MockParentTaskFactory::generateParentTask(1);
        $adapter = $this->prophesize(ParentTaskRestfulAdapter::class);
        $adapter->assign(Argument::exact($parentTask))->shouldBeCalledTimes(1)->willReturn(true);

        $this->repository->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->repository->assign($parentTask);
        $this->assertTrue($result);
    }

    public function testRevoke()
    {
        $parentTask = \Base\Sdk\WorkOrderTask\Utils\MockParentTaskFactory::generateParentTask(1);
        $adapter = $this->prophesize(ParentTaskRestfulAdapter::class);
        $adapter->revoke(Argument::exact($parentTask))->shouldBeCalledTimes(1)->willReturn(true);

        $this->repository->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->repository->revoke($parentTask);
        $this->assertTrue($result);
    }
}
