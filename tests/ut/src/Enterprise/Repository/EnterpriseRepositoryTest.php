<?php
namespace Base\Sdk\Enterprise\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Enterprise\Adapter\Enterprise\EnterpriseRestfulAdapter;

class EnterpriseRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(EnterpriseRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
        
        $this->childRepository = new class extends EnterpriseRepository {
            public function getActualAdapter()
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter()
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->childRepository);
    }

    public function testImplementsIEnterpriseAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Enterprise\Adapter\Enterprise\IEnterpriseAdapter',
            $this->repository
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(EnterpriseRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Enterprise\Adapter\Enterprise\EnterpriseRestfulAdapter',
            $this->childRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Enterprise\Adapter\Enterprise\EnterpriseMockAdapter',
            $this->childRepository->getMockAdapter()
        );
    }
}
