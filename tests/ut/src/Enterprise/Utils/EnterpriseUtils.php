<?php
namespace Base\Sdk\Enterprise\Utils;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Enterprise\Translator\EnterpriseTranslator;

trait EnterpriseUtils
{
    /**
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObject(
        array $expectedArray,
        $enterprise
    ) {
        $this->assertEquals(
            $expectedArray['name'],
            $enterprise->getName()
        );
        $this->assertEquals(
            $expectedArray['unifiedSocialCreditCode'],
            $enterprise->getUnifiedSocialCreditCode()
        );
        $this->assertEquals(
            $expectedArray['establishmentDate'],
            $enterprise->getEstablishmentDate()
        );

        $this->assertEquals(
            $expectedArray['approvalDate'],
            $enterprise->getApprovalDate()
        );
        $this->assertEquals(
            $expectedArray['address'],
            $enterprise->getAddress()
        );
        $this->assertEquals(
            $expectedArray['registrationCapital'],
            $enterprise->getRegistrationCapital()
        );
        $this->assertEquals(
            $expectedArray['businessTermStart'],
            $enterprise->getBusinessTermStart()
        );
        $this->assertEquals(
            $expectedArray['businessTermTo'],
            $enterprise->getBusinessTermTo()
        );
        $this->assertEquals(
            $expectedArray['businessScope'],
            $enterprise->getBusinessScope()
        );
        $this->assertEquals(
            $expectedArray['registrationAuthority'],
            $enterprise->getRegistrationAuthority()
        );
        $this->assertEquals(
            $expectedArray['principal'],
            $enterprise->getPrincipal()
        );
        $this->assertEquals(
            $expectedArray['principalCardId'],
            $enterprise->getPrincipalCardId()
        );
        $this->assertEquals(
            $expectedArray['registrationStatus'],
            $enterprise->getRegistrationStatus()
        );
        $this->assertEquals(
            $expectedArray['enterpriseTypeCode'],
            $enterprise->getEnterpriseTypeCode()
        );
        $this->assertEquals(
            $expectedArray['enterpriseType'],
            $enterprise->getEnterpriseType()
        );
        $this->assertEquals(
            $expectedArray['industryCategory'],
            INDUSTRY_CATEGORY_CN[$enterprise->getIndustryCategory()]
        );
        $this->assertEquals(
            $expectedArray['industryCode'],
            $enterprise->getIndustryCode()
        );
        $this->assertEquals(
            $expectedArray['administrativeArea'],
            ADMINISTRATIVE_AREA_CN[$enterprise->getAdministrativeArea()]
        );

        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($enterprise->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            IApplyCategory:: STATUS_CN[$enterprise->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            IApplyCategory:: STATUS_TAG_TYPE[$enterprise->getStatus()]
        );

        $this->assertEquals(
            $expectedArray['updateTime'],
            $enterprise->getUpdateTime()
        );
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $enterprise->getUpdateTime())
        );

        $this->assertEquals(
            $expectedArray['statusTime'],
            $enterprise->getStatusTime()
        );
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $enterprise->getStatusTime())
        );

        $this->assertEquals(
            $expectedArray['createTime'],
            $enterprise->getCreateTime()
        );
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $enterprise->getCreateTime())
        );
    }
}
