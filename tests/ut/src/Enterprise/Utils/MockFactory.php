<?php
namespace Base\Sdk\Enterprise\Utils;

use Base\Sdk\Enterprise\Model\Enterprise;
use Base\Sdk\Common\Model\IEnableAble;

class MockFactory
{
    public static function generateEnterprise(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Enterprise {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $enterprise = new Enterprise($id);
        $enterprise->setId($id);

        //name
        self::generateName($enterprise, $faker, $value);
        //unifiedSocialCreditCode
        self::generateUnifiedSocialCreditCode($enterprise, $faker, $value);
        //establishmentDate
        self::generateEstablishmentDate($enterprise, $faker, $value);
        //approvalDate
        self::generateApprovalDate($enterprise, $faker, $value);
        //address
        self::generateAddress($enterprise, $faker, $value);
        //registrationCapital
        self::generateRegistrationCapital($enterprise, $faker, $value);
        //businessTermStart
        self::generateBusinessTermStart($enterprise, $faker, $value);
        //businessTermTo
        self::generateBusinessTermTo($enterprise, $faker, $value);
        //businessScope
        self::generateBusinessScope($enterprise, $faker, $value);
        //registrationAuthority
        self::generateRegistrationAuthority($enterprise, $faker, $value);
        //principal
        self::generatePrincipal($enterprise, $faker, $value);
        //principalCardId
        self::generatePrincipalCardId($enterprise, $faker, $value);
        //registrationStatus
        self::generateRegistrationStatus($enterprise, $faker, $value);
        //enterpriseTypeCode
        self::generateEnterpriseTypeCode($enterprise, $faker, $value);
        //enterpriseType
        self::generateEnterpriseType($enterprise, $faker, $value);
        //industryCategory
        self::generateIndustryCategory($enterprise, $faker, $value);
        //industryCode
        self::generateIndustryCode($enterprise, $faker, $value);
        //administrativeArea
        self::generateAdministrativeArea($enterprise, $faker, $value);

        //status
        self::generateStatus($enterprise, $faker, $value);

        $enterprise->setCreateTime($faker->unixTime());
        $enterprise->setUpdateTime($faker->unixTime());
        $enterprise->setStatusTime($faker->unixTime());

        return $enterprise;
    }

    private static function generateName($enterprise, $faker, $value) : void
    {
        $name = isset($value['name']) ?
            $value['name'] :
            $faker->word();
        
        $enterprise->setName($name);
    }

    private static function generateUnifiedSocialCreditCode($enterprise, $faker, $value) : void
    {
        $unifiedSocialCreditCode = isset($value['unifiedSocialCreditCode']) ?
            $value['unifiedSocialCreditCode'] :
            $faker->word();
        
        $enterprise->setUnifiedSocialCreditCode($unifiedSocialCreditCode);
    }

    private static function generateEstablishmentDate($enterprise, $faker, $value) : void
    {
        unset($faker);
        $establishmentDate = isset($value['establishmentDate']) ? $value['establishmentDate'] : 20210802;
        $enterprise->setEstablishmentDate($establishmentDate);
    }

    private static function generateApprovalDate($enterprise, $faker, $value) : void
    {
        unset($faker);
        $approvalDate = isset($value['approvalDate']) ? $value['approvalDate'] :  20210802;
        $enterprise->setApprovalDate($approvalDate);
    }

    private static function generateAddress($enterprise, $faker, $value) : void
    {
        $address = isset($value['address']) ?
            $value['address'] : $faker->word();
        $enterprise->setAddress($address);
    }

    private static function generateRegistrationCapital($enterprise, $faker, $value) : void
    {
        $registrationCapital = isset($value['registrationCapital']) ?
            $value['registrationCapital'] :$faker->word();
        $enterprise->setRegistrationCapital($registrationCapital);
    }

    private static function generateBusinessTermStart($enterprise, $faker, $value) : void
    {
        unset($faker);
        $businessTermStart = isset($value['businessTermStart']) ?
            $value['businessTermStart'] :20160316;
        $enterprise->setBusinessTermStart($businessTermStart);
    }

    private static function generateBusinessTermTo($enterprise, $faker, $value) : void
    {
        unset($faker);
        $businessTermTo = isset($value['businessTermTo']) ?
            $value['businessTermTo'] :20210316;
        $enterprise->setBusinessTermTo($businessTermTo);
    }

    private static function generateBusinessScope($enterprise, $faker, $value) : void
    {
        $businessScope = isset($value['businessScope']) ?
            $value['businessScope'] :$faker->word();
        $enterprise->setBusinessScope($businessScope);
    }

    private static function generateRegistrationAuthority($enterprise, $faker, $value) : void
    {
        $registrationAuthority = isset($value['registrationAuthority']) ?
            $value['registrationAuthority'] :$faker->word();
        $enterprise->setRegistrationAuthority($registrationAuthority);
    }

    private static function generatePrincipal($enterprise, $faker, $value) : void
    {
        $principal = isset($value['principal']) ?
            $value['principal'] :$faker->word();
        $enterprise->setPrincipal($principal);
    }

    private static function generatePrincipalCardId($enterprise, $faker, $value) : void
    {
        $principalCardId = isset($value['principalCardId']) ?
            $value['principalCardId'] :$faker->word();
        $enterprise->setPrincipalCardId($principalCardId);
    }

    private static function generateRegistrationStatus($enterprise, $faker, $value) : void
    {
        $registrationStatus = isset($value['registrationStatus']) ?
            $value['registrationStatus'] :$faker->word();
        $enterprise->setRegistrationStatus($registrationStatus);
    }

    private static function generateEnterpriseTypeCode($enterprise, $faker, $value) : void
    {
        $enterpriseTypeCode = isset($value['enterpriseTypeCode']) ?
            $value['enterpriseTypeCode'] :$faker->word();
        $enterprise->setEnterpriseTypeCode($enterpriseTypeCode);
    }

    private static function generateEnterpriseType($enterprise, $faker, $value) : void
    {
        $enterpriseType = isset($value['enterpriseType']) ?
            $value['enterpriseType'] :$faker->word();
        $enterprise->setEnterpriseType($enterpriseType);
    }

    private static function generateIndustryCategory($enterprise, $faker, $value) : void
    {
        unset($faker);
        $industryCategory = isset($value['industryCategory']) ?
            $value['industryCategory'] :'A';
        $enterprise->setIndustryCategory($industryCategory);
    }

    private static function generateIndustryCode($enterprise, $faker, $value) : void
    {
        $industryCode = isset($value['industryCode']) ?
            $value['industryCode'] :$faker->word();
        $enterprise->setIndustryCode($industryCode);
    }

    private static function generateAdministrativeArea($enterprise, $faker, $value) : void
    {
        unset($faker);
        $administrativeArea = isset($value['administrativeArea']) ?
            $value['administrativeArea'] :101;
        $enterprise->setAdministrativeArea($administrativeArea);
    }

    private static function generateStatus($enterprise, $faker, $value) : void
    {
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(IEnableAble::STATUS);
        $enterprise->setStatus($status);
    }
}
