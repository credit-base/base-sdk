<?php
namespace Base\Sdk\Enterprise\Utils;

trait EnterpriseRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $enterprise
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $enterprise->getId());
        }
        
        $this->assertEquals(
            $expectedArray['data']['attributes']['name'],
            $enterprise->getName()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['unifiedSocialCreditCode'],
            $enterprise->getUnifiedSocialCreditCode()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['establishmentDate'],
            $enterprise->getEstablishmentDate()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['approvalDate'],
            $enterprise->getApprovalDate()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['registrationCapital'],
            $enterprise->getRegistrationCapital()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['businessTermStart'],
            $enterprise->getBusinessTermStart()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['businessTermTo'],
            $enterprise->getBusinessTermTo()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['businessScope'],
            $enterprise->getBusinessScope()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['registrationAuthority'],
            $enterprise->getRegistrationAuthority()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['principal'],
            $enterprise->getPrincipal()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['principalCardId'],
            $enterprise->getPrincipalCardId()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['registrationStatus'],
            $enterprise->getRegistrationStatus()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['enterpriseTypeCode'],
            $enterprise->getEnterpriseTypeCode()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['enterpriseType'],
            $enterprise->getEnterpriseType()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['industryCategory'],
            $enterprise->getIndustryCategory()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['industryCode'],
            $enterprise->getIndustryCode()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['administrativeArea'],
            $enterprise->getAdministrativeArea()
        );
    }
}
