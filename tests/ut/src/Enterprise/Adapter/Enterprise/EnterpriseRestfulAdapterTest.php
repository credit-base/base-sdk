<?php
namespace Base\Sdk\Enterprise\Adapter\Enterprise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Enterprise\Model\Enterprise;
use Base\Sdk\Enterprise\Model\NullEnterprise;
use Base\Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

class EnterpriseRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(EnterpriseRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends EnterpriseRestfulAdapter {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getEnterpriseMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIEnterpriseAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Enterprise\Adapter\Enterprise\IEnterpriseAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('enterprises', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'ENTERPRISE_LIST',
                EnterpriseRestfulAdapter::SCENARIOS['ENTERPRISE_LIST']
            ],
            [
                'ENTERPRISE_FETCH_ONE',
                EnterpriseRestfulAdapter::SCENARIOS['ENTERPRISE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    public function testFetchOne()
    {
        $id = 1;

        $enterprise = \Base\Sdk\Enterprise\Utils\MockFactory::generateEnterprise($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullEnterprise::getInstance())
            ->willReturn($enterprise);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($enterprise, $result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [];
        $result = $this->childAdapter->getEnterpriseMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
