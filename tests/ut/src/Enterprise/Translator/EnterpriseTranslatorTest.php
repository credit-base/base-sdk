<?php
namespace Base\Sdk\Enterprise\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Enterprise\Utils\EnterpriseUtils;

class EnterpriseTranslatorTest extends TestCase
{
    use EnterpriseUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new EnterpriseTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\Enterprise\Model\NullEnterprise', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $enterprise = \Base\Sdk\Enterprise\Utils\MockFactory::generateEnterprise(1);
        
        $expression = $this->translator->objectToArray($enterprise);
        
        $this->compareArrayAndObject($expression, $enterprise);
    }

    public function testObjectToArrayFail()
    {
        $enterprise = null;

        $expression = $this->translator->objectToArray($enterprise);
        $this->assertEquals(array(), $expression);
    }
}
