<?php
namespace Base\Sdk\Enterprise\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Base\Sdk\Common\Model\IEnableAble;

/**
 * @SuppressWarnings(PHPMD)
 */
class EnterpriseTest extends TestCase
{
    private $enterprise;

    public function setUp()
    {
        $this->enterprise = new MockEnterprise();
    }

    public function tearDown()
    {
        unset($this->enterprise);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->enterprise->getId());
        $this->assertEquals('', $this->enterprise->getName());
        $this->assertEquals('', $this->enterprise->getUnifiedSocialCreditCode());
        $this->assertEquals(0, $this->enterprise->getEstablishmentDate());
        $this->assertEquals(0, $this->enterprise->getApprovalDate());
        $this->assertEquals('', $this->enterprise->getAddress());
        $this->assertEquals('', $this->enterprise->getRegistrationCapital());
        $this->assertEquals(0, $this->enterprise->getBusinessTermStart());
        $this->assertEquals(0, $this->enterprise->getBusinessTermTo());
        $this->assertEquals('', $this->enterprise->getBusinessScope());
        $this->assertEquals('', $this->enterprise->getRegistrationAuthority());
        $this->assertEquals('', $this->enterprise->getPrincipal());
        $this->assertEquals('', $this->enterprise->getPrincipalCardId());
        $this->assertEquals('', $this->enterprise->getRegistrationStatus());
        $this->assertEquals('', $this->enterprise->getEnterpriseTypeCode());
        $this->assertEquals('', $this->enterprise->getEnterpriseType());
        $this->assertEquals('', $this->enterprise->getIndustryCategory());
        $this->assertEquals('', $this->enterprise->getIndustryCode());
        $this->assertEquals(0, $this->enterprise->getAdministrativeArea());

        $this->assertEquals(0, $this->enterprise->getStatus());
        $this->assertInstanceOf(
            'Base\Sdk\Enterprise\Repository\EnterpriseRepository',
            $this->enterprise->getRepository()
        );
    }

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 Enterprise setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->enterprise->setName('name');
        $this->assertEquals('name', $this->enterprise->getName());
    }

    /**
     * 设置 Enterprise setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->enterprise->setName(['name']);
    }
    //name 测试 --------------------------------------------------------   end

    //unifiedSocialCreditCode 测试 -------------------------------------------------------- start
    /**
     * 设置 Enterprise setUnifiedSocialCreditCode() 正确的传参类型,期望传值正确
     */
    public function testsetUnifiedSocialCreditCodeCorrectType()
    {
        $unifiedSocialCreditCode = '91410900MA46184E7Q';
        $this->enterprise->setUnifiedSocialCreditCode($unifiedSocialCreditCode);
        $this->assertEquals($unifiedSocialCreditCode, $this->enterprise->getUnifiedSocialCreditCode());
    }

    /**
     * 设置 enterprise setUnifiedSocialCreditCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUnifiedSocialCreditCodeWrongType()
    {
        $this->enterprise->setUnifiedSocialCreditCode(['string']);
    }
    //unifiedSocialCreditCode 测试 --------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 enterprise setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->enterprise->setStatus($actual);
        $this->assertEquals($expected, $this->enterprise->getStatus());
    }

    /**
     * 循环测试 enterprise setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(IEnableAble::STATUS['ENABLED'], IEnableAble::STATUS['ENABLED']),
            array(IEnableAble::STATUS['DISABLED'], IEnableAble::STATUS['DISABLED']),
        );
    }

    /**
     * 设置 Enterprise setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->enterprise->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

    //establishmentDate 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setEstablishmentDate() 正确的传参类型,期望传值正确
     */
    public function testSetEstablishmentDateCorrectType()
    {
        $this->enterprise->setEstablishmentDate(20210802);
        $this->assertEquals(20210802, $this->enterprise->getEstablishmentDate());
    }

    /**
     * 设置 enterprise setEstablishmentDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEstablishmentDateWrongType()
    {
        $this->enterprise->setEstablishmentDate(['sting']);
    }
    //establishmentDate 测试 --------------------------------------------------------   end

    //approvalDate 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setApprovalDate() 正确的传参类型,期望传值正确
     */
    public function testSetApprovalDateCorrectType()
    {
        $this->enterprise->setApprovalDate(20210802);
        $this->assertEquals(20210802, $this->enterprise->getApprovalDate());
    }

    /**
     * 设置 enterprise setApprovalDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApprovalDateWrongType()
    {
        $this->enterprise->setApprovalDate(['sting']);
    }
    //approvalDate 测试 --------------------------------------------------------   end

    //address 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setAddress() 正确的传参类型,期望传值正确
     */
    public function testSetAddressCorrectType()
    {
        $address = 'address';

        $this->enterprise->setAddress($address);
        $this->assertEquals($address, $this->enterprise->getAddress());
    }

    /**
     * 设置 enterprise setAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAddressWrongType()
    {
        $this->enterprise->setAddress(['address']);
    }
    //address 测试 --------------------------------------------------------   end

    //registrationCapital 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setRegistrationCapital() 正确的传参类型,期望传值正确
     */
    public function testSetRegistrationCapitalCorrectType()
    {
        $expected = 'string';

        $this->enterprise->setRegistrationCapital($expected);
        $this->assertEquals($expected, $this->enterprise->getRegistrationCapital());
    }

    /**
     * 设置 enterprise setRegistrationCapital() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRegistrationCapitalWrongType()
    {
        $this->enterprise->setRegistrationCapital(['registrationCapital']);
    }
    //registrationCapital 测试 --------------------------------------------------------   end

    //businessTermStart 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setBusinessTermStart() 正确的传参类型,期望传值正确
     */
    public function testSetBusinessTermStartCorrectType()
    {
        $expected = 20160316;

        $this->enterprise->setBusinessTermStart($expected);
        $this->assertEquals($expected, $this->enterprise->getBusinessTermStart());
    }

    /**
     * 设置 enterprise setBusinessTermStart() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBusinessTermStartWrongType()
    {
        $this->enterprise->setBusinessTermStart(['businessTermStart']);
    }
    //businessTermStart 测试 --------------------------------------------------------   end

    //businessTermTo 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setBusinessTermTo() 正确的传参类型,期望传值正确
     */
    public function testSetBusinessTermToCorrectType()
    {
        $expected = 20210316;

        $this->enterprise->setBusinessTermTo($expected);
        $this->assertEquals($expected, $this->enterprise->getBusinessTermTo());
    }

    /**
     * 设置 enterprise setBusinessTermTo() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBusinessTermToWrongType()
    {
        $this->enterprise->setBusinessTermTo(['businessTermTo']);
    }
    //businessTermTo 测试 --------------------------------------------------------   end

    //businessScope 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setBusinessScope() 正确的传参类型,期望传值正确
     */
    public function testSetBusinessScopeCorrectType()
    {
        $expected = 'string';

        $this->enterprise->setBusinessScope($expected);
        $this->assertEquals($expected, $this->enterprise->getBusinessScope());
    }

    /**
     * 设置 enterprise setBusinessScope() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBusinessScopeWrongType()
    {
        $this->enterprise->setBusinessScope(['businessScope']);
    }
    //businessScope 测试 --------------------------------------------------------   end

    //registrationAuthority 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setRegistrationAuthority() 正确的传参类型,期望传值正确
     */
    public function testSetRegistrationAuthorityCorrectType()
    {
        $expected = 'string';

        $this->enterprise->setRegistrationAuthority($expected);
        $this->assertEquals($expected, $this->enterprise->getRegistrationAuthority());
    }

    /**
     * 设置 enterprise setRegistrationAuthority() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRegistrationAuthorityWrongType()
    {
        $this->enterprise->setRegistrationAuthority(['registrationAuthority']);
    }
    //registrationAuthority 测试 --------------------------------------------------------   end

    //principal 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setPrincipal() 正确的传参类型,期望传值正确
     */
    public function testSetPrincipalCorrectType()
    {
        $expected = 'string';

        $this->enterprise->setPrincipal($expected);
        $this->assertEquals($expected, $this->enterprise->getPrincipal());
    }

    /**
     * 设置 enterprise setPrincipal() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPrincipalWrongType()
    {
        $this->enterprise->setPrincipal(['principal']);
    }
    //principal 测试 --------------------------------------------------------   end

    //principalCardId 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setPrincipalCardId() 正确的传参类型,期望传值正确
     */
    public function testSetPrincipalCardIdCorrectType()
    {
        $expected = 'string';

        $this->enterprise->setPrincipalCardId($expected);
        $this->assertEquals($expected, $this->enterprise->getPrincipalCardId());
    }

    /**
     * 设置 enterprise setPrincipalCardId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPrincipalCardIdWrongType()
    {
        $this->enterprise->setPrincipalCardId(['principalCardId']);
    }
    //principalCardId 测试 --------------------------------------------------------   end

    //registrationStatus 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setRegistrationStatus() 正确的传参类型,期望传值正确
     */
    public function testSetRegistrationStatusCorrectType()
    {
        $expected = 'string';

        $this->enterprise->setRegistrationStatus($expected);
        $this->assertEquals($expected, $this->enterprise->getRegistrationStatus());
    }

    /**
     * 设置 enterprise setRegistrationStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRegistrationStatusWrongType()
    {
        $this->enterprise->setRegistrationStatus(['registrationStatus']);
    }
    //registrationStatus 测试 --------------------------------------------------------   end

    //enterpriseTypeCode 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setEnterpriseTypeCode() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseTypeCodeCorrectType()
    {
        $expected = 'string';

        $this->enterprise->setEnterpriseTypeCode($expected);
        $this->assertEquals($expected, $this->enterprise->getEnterpriseTypeCode());
    }

    /**
     * 设置 enterprise setEnterpriseTypeCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseTypeCodeWrongType()
    {
        $this->enterprise->setEnterpriseTypeCode(['enterpriseTypeCode']);
    }
    //enterpriseTypeCode 测试 --------------------------------------------------------   end

    //enterpriseType 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setEnterpriseType() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseTypeCorrectType()
    {
        $expected = 'string';

        $this->enterprise->setEnterpriseType($expected);
        $this->assertEquals($expected, $this->enterprise->getEnterpriseType());
    }

    /**
     * 设置 enterprise setEnterpriseType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseTypeWrongType()
    {
        $this->enterprise->setEnterpriseType(['enterpriseType']);
    }
    //enterpriseType 测试 --------------------------------------------------------   end

    //industryCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setIndustryCategory() 正确的传参类型,期望传值正确
     */
    public function testSetIndustryCategoryCorrectType()
    {
        $expected = 'string';

        $this->enterprise->setIndustryCategory($expected);
        $this->assertEquals($expected, $this->enterprise->getIndustryCategory());
    }

    /**
     * 设置 enterprise setIndustryCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIndustryCategoryWrongType()
    {
        $this->enterprise->setIndustryCategory(['industryCategory']);
    }
    //industryCategory 测试 --------------------------------------------------------   end

    //industryCode 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setIndustryCode() 正确的传参类型,期望传值正确
     */
    public function testSetIndustryCodeCorrectType()
    {
        $expected = 'string';

        $this->enterprise->setIndustryCode($expected);
        $this->assertEquals($expected, $this->enterprise->getIndustryCode());
    }

    /**
     * 设置 enterprise setIndustryCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIndustryCodeWrongType()
    {
        $this->enterprise->setIndustryCode(['industryCode']);
    }
    //industryCode 测试 --------------------------------------------------------   end

    //administrativeArea 测试 -------------------------------------------------------- start
    /**
     * 设置 enterprise setAdministrativeArea() 正确的传参类型,期望传值正确
     */
    public function testSetAdministrativeAreaCorrectType()
    {
        $expected = 0111;

        $this->enterprise->setAdministrativeArea($expected);
        $this->assertEquals($expected, $this->enterprise->getAdministrativeArea());
    }

    /**
     * 设置 enterprise setAdministrativeArea() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAdministrativeAreaWrongType()
    {
        $this->enterprise->setAdministrativeArea(['administrativeArea']);
    }
    //administrativeArea 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Enterprise\Repository\EnterpriseRepository',
            $this->enterprise->getRepository()
        );
    }
}
