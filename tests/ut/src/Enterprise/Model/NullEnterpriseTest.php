<?php
namespace Base\Sdk\Enterprise\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullEnterpriseTest extends TestCase
{
    private $nullEnterprise;

    public function setUp()
    {
        $this->nullEnterprise = NullEnterprise::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullEnterprise);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsEnterprise()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Enterprise\Model\Enterprise',
            $this->nullEnterprise
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullEnterprise
        );
    }

    public function testResourceNotExist()
    {
        $nullEnterprise = new MockNullEnterprise();

        $result = $nullEnterprise->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
