<?php
namespace Base\Sdk\CreditPhotography\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Member\Model\Member;
use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class CreditPhotographyTest extends TestCase
{
    private $creditPhotography;

    public function setUp()
    {
        $this->creditPhotography = new MockCreditPhotography();
    }

    public function tearDown()
    {
        unset($this->creditPhotography);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->creditPhotography->getId());
        $this->assertEquals('', $this->creditPhotography->getDescription());
        $this->assertEquals(array(), $this->creditPhotography->getAttachments());
        $this->assertEquals('', $this->creditPhotography->getRejectReason());
        $this->assertInstanceOf('Base\Sdk\Crew\Model\Crew', $this->creditPhotography->getApplyCrew());
        $this->assertInstanceOf('Base\Sdk\Member\Model\Member', $this->creditPhotography->getMember());

        $this->assertEquals(CreditPhotography::STATUS['NOMAL'], $this->creditPhotography->getStatus());
        $this->assertEquals(CreditPhotography::APPLY_STATUS['PENDING'], $this->creditPhotography->getApplyStatus());
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository',
            $this->creditPhotography->getRepository()
        );
    }

    //description 测试 -------------------------------------------------------- start
    /**
     * 设置 CreditPhotography setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->creditPhotography->setDescription('description');
        $this->assertEquals('description', $this->creditPhotography->getDescription());
    }

    /**
     * 设置 CreditPhotography setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->creditPhotography->setDescription(['description']);
    }
    //description 测试 --------------------------------------------------------   end

    //attachments 测试 -------------------------------------------------------- start
    /**
     * 设置 creditPhotography setAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetAttachmentsCorrectType()
    {
        $attachments = array(
            array('name' => 'name', 'identify' => 'identify.jpg'),
            array('name' => 'name', 'identify' => 'identify.jpeg'),
            array('name' => 'name', 'identify' => 'identify.mp4'));
        $this->creditPhotography->setAttachments($attachments);
        $this->assertEquals($attachments, $this->creditPhotography->getAttachments());
    }

    /**
     * 设置 creditPhotography setAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAttachmentsWrongType()
    {
        $this->creditPhotography->setAttachments('string');
    }
    //attachments 测试 --------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 creditPhotography setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->creditPhotography->setStatus($actual);
        $this->assertEquals($expected, $this->creditPhotography->getStatus());
    }

    /**
     * 循环测试 creditPhotography setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(CreditPhotography::STATUS['NOMAL'], CreditPhotography::STATUS['NOMAL']),
            array(CreditPhotography::STATUS['DELETE'], CreditPhotography::STATUS['DELETE']),
        );
    }


    /**
     * 设置 CreditPhotography setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->creditPhotography->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

    //rejectReason 测试 -------------------------------------------------------- start
    /**
     * 设置 creditPhotography setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $this->creditPhotography->setRejectReason('rejectReason');
        $this->assertEquals('rejectReason', $this->creditPhotography->getRejectReason());
    }

    /**
     * 设置 creditPhotography setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->creditPhotography->setRejectReason(['email']);
    }
    //rejectReason 测试 --------------------------------------------------------   end

    //applyStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 creditPhotography setApplyStatus() 是否符合预定范围
     *
     * @dataProvider applyStatusProvider
     */
    public function testSetApplyStatus($actual, $expected)
    {
        $this->creditPhotography->setApplyStatus($actual);
        $this->assertEquals($expected, $this->creditPhotography->getApplyStatus());
    }

    /**
     * 循环测试 Journal setApplyStatus() 数据构建器
     */
    public function applyStatusProvider()
    {
        return array(
            array(CreditPhotography::APPLY_STATUS['NOT_SUBMITTED'], CreditPhotography::APPLY_STATUS['NOT_SUBMITTED']),
            array(CreditPhotography::APPLY_STATUS['PENDING'], CreditPhotography::APPLY_STATUS['PENDING']),
            array(CreditPhotography::APPLY_STATUS['APPROVE'], CreditPhotography::APPLY_STATUS['APPROVE']),
            array(CreditPhotography::APPLY_STATUS['REJECT'], CreditPhotography::APPLY_STATUS['REJECT']),
        );
    }

    /**
     * 设置 Journal setApplyStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyStatusWrongType()
    {
        $this->creditPhotography->setApplyStatus('string');
    }
    //applyStatus 测试 ------------------------------------------------------   end

    //Crew 测试 -------------------------------------------------------- start
    /**
     * 设置 creditPhotography setApplyCrew() 正确的传参类型,期望传值正确
     */
    public function testSetApplyCrewCorrectType()
    {
        $expectedCrew = new Crew();

        $this->creditPhotography->setApplyCrew($expectedCrew);
        $this->assertEquals($expectedCrew, $this->creditPhotography->getApplyCrew());
    }

    /**
     * 设置 creditPhotography setApplyCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyCrewWrongType()
    {
        $this->creditPhotography->setApplyCrew('Crew');
    }
    //Crew 测试 --------------------------------------------------------   end

    //Member 测试 -------------------------------------------------------- start
    /**
     * 设置 creditPhotography setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $expected = new Member();

        $this->creditPhotography->setMember($expected);
        $this->assertEquals($expected, $this->creditPhotography->getMember());
    }

    /**
     * 设置 creditPhotography setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberWrongType()
    {
        $this->creditPhotography->setMember('Member');
    }
    //Member 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository',
            $this->creditPhotography->getRepository()
        );
    }

    public function testGetIApplyAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository',
            $this->creditPhotography->getIApplyAbleAdapter()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository',
            $this->creditPhotography->getIOperateAbleAdapter()
        );
    }

    public function testDelete()
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotography::class)
            ->setMethods(['getRepository'])->getMock();

        $repository = $this->prophesize(CreditPhotographyRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->delete();
        $this->assertTrue($result);
    }
}
