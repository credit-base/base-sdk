<?php
namespace Base\Sdk\CreditPhotography\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullCreditPhotographyTest extends TestCase
{
    private $nullCreditPhotography;

    public function setUp()
    {
        $this->nullCreditPhotography = NullCreditPhotography::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullCreditPhotography);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsCreditPhotography()
    {
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Model\CreditPhotography',
            $this->nullCreditPhotography
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullCreditPhotography
        );
    }

    public function testResourceNotExist()
    {
        $nullCreditPhotography = new MockNullCreditPhotography();

        $result = $nullCreditPhotography->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
