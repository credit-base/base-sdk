<?php
namespace Base\Sdk\CreditPhotography\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\CreditPhotography\Adapter\CreditPhotography\CreditPhotographyRestfulAdapter;

class CreditPhotographyRepositoryTest extends TestCase
{
    private $repository;
    
    public function setUp()
    {
        $this->repository = $this->getMockBuilder(CreditPhotographyRepository::class)
                                 ->setMethods(['getAdapter'])
                                 ->getMock();
        
        $this->childRepository = new class extends CreditPhotographyRepository {
            public function getActualAdapter()
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter()
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->childRepository);
    }

    public function testImplementsICreditPhotographyAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter',
            $this->repository
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(CreditPhotographyRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Adapter\CreditPhotography\CreditPhotographyRestfulAdapter',
            $this->childRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Adapter\CreditPhotography\CreditPhotographyRestfulAdapter',
            $this->childRepository->getMockAdapter()
        );
    }

    public function testDeletes()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);
        $adapter = $this->prophesize(CreditPhotographyRestfulAdapter::class);
        $adapter->deletes(Argument::exact($creditPhotography))->shouldBeCalledTimes(1)->willReturn(true);

        $this->repository->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->repository->deletes($creditPhotography);
        $this->assertTrue($result);
    }
}
