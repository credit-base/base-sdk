<?php
namespace Base\Sdk\CreditPhotography\Utils;

trait CreditPhotographyRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $creditPhotography
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $creditPhotography->getId());
        }
        
        $this->assertEquals(
            $expectedArray['data']['attributes']['description'],
            $creditPhotography->getDescription()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['attachments'],
            $creditPhotography->getAttachments()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['rejectReason'],
            $creditPhotography->getRejectReason()
        );

        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['member']['data'])) {
                $this->assertEquals(
                    $relationships['member']['data'][0]['type'],
                    'members'
                );
                $this->assertEquals(
                    $relationships['member']['data'][0]['id'],
                    $creditPhotography->getMember()->getId()
                );
            }
        }
    }
}
