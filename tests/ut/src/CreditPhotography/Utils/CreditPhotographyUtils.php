<?php
namespace Base\Sdk\CreditPhotography\Utils;

use Base\Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;
use Base\Sdk\Common\Model\IApplyCategory;
use Base\Sdk\Crew\Translator\CrewTranslator;
use Base\Sdk\Member\Translator\MemberTranslator;

trait CreditPhotographyUtils
{
    protected function getCrewTranslator() : CrewTranslator
    {
        return new CrewTranslator();
    }

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }

    private function compareArrayAndObject(
        array $expectedArray,
        $creditPhotography
    ) {
        $this->assertEquals(
            $expectedArray['description'],
            $creditPhotography->getDescription()
        );
        $this->assertEquals(
            $expectedArray['attachments'],
            $creditPhotography->getAttachments()
        );
        $this->assertEquals(
            $expectedArray['rejectReason'],
            $creditPhotography->getRejectReason()
        );

        $this->assertEquals(
            $expectedArray['applyStatus']['id'],
            marmot_encode($creditPhotography->getApplyStatus())
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['name'],
            IApplyCategory::APPLY_STATUS_CN[$creditPhotography->getApplyStatus()]
        );
        $this->assertEquals(
            $expectedArray['applyStatus']['type'],
            IApplyCategory:: APPLY_STATUS_TAG_TYPE[$creditPhotography->getApplyStatus()]
        );

        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($creditPhotography->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            CreditPhotographyTranslator:: STATUS_CN[$creditPhotography->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            CreditPhotographyTranslator:: STATUS_TAG_TYPE[$creditPhotography->getStatus()]
        );

        $this->assertEquals(
            $expectedArray['updateTime'],
            $creditPhotography->getUpdateTime()
        );
        $this->assertEquals(
            $expectedArray['updateTimeFormat'],
            date('Y年m月d日 H时i分', $creditPhotography->getUpdateTime())
        );

        $this->assertEquals(
            $expectedArray['statusTime'],
            $creditPhotography->getStatusTime()
        );
        $this->assertEquals(
            $expectedArray['statusTimeFormat'],
            date('Y年m月d日 H时i分', $creditPhotography->getStatusTime())
        );

        $this->assertEquals(
            $expectedArray['createTime'],
            $creditPhotography->getCreateTime()
        );
        $this->assertEquals(
            $expectedArray['createTimeFormat'],
            date('Y年m月d日 H时i分', $creditPhotography->getCreateTime())
        );

        $this->assertEquals(
            $expectedArray['applyCrew'],
            $this->getCrewTranslator()->objectToArray($creditPhotography->getApplyCrew())
        );

        $this->assertEquals(
            $expectedArray['member'],
            $this->getMemberTranslator()->objectToArray($creditPhotography->getMember())
        );
    }
}
