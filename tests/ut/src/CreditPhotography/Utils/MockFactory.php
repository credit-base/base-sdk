<?php
namespace Base\Sdk\CreditPhotography\Utils;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;

class MockFactory
{
    public static function generateCreditPhotography(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : CreditPhotography {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $creditPhotography = new CreditPhotography($id);
        $creditPhotography->setId($id);

        //description
        self::generateDescription($creditPhotography, $faker, $value);
        //attachments
        self::generateAttachments($creditPhotography, $faker, $value);
        //rejectReason
        self::generateRejectReason($creditPhotography, $faker, $value);
        //member
        self::generateMember($creditPhotography, $faker, $value);
        //applyCrew
        self::generateApplyCrew($creditPhotography, $faker, $value);
        //applyStatus
        self::generateApplyStatus($creditPhotography, $faker, $value);

        //status
        self::generateStatus($creditPhotography, $faker, $value);

        $creditPhotography->setCreateTime($faker->unixTime());
        $creditPhotography->setUpdateTime($faker->unixTime());
        $creditPhotography->setStatusTime($faker->unixTime());

        return $creditPhotography;
    }

    private static function generateDescription($creditPhotography, $faker, $value) : void
    {
        $description = isset($value['description']) ?
            $value['description'] :
            $faker->word();
        
        $creditPhotography->setDescription($description);
    }

    private static function generateAttachments($creditPhotography, $faker, $value) : void
    {
        unset($faker);
        $attachments = isset($value['attachments']) ?
            $value['attachments'] :
            array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg'),
                array('name' => 'name', 'identify' => 'identify.mp4')
            );
        
        $creditPhotography->setAttachments($attachments);
    }

    private static function generateRejectReason($creditPhotography, $faker, $value) : void
    {
        $rejectReason = isset($value['rejectReason']) ? $value['rejectReason'] : $faker->word();
        $creditPhotography->setRejectReason($rejectReason);
    }

    private static function generateMember($creditPhotography, $faker, $value) : void
    {
        unset($faker);
        $member = isset($value['member']) ? $value['member'] :  \Base\Sdk\Member\Utils\MockFactory::generateMember(1);
        $creditPhotography->setMember($member);
    }

    private static function generateApplyCrew($creditPhotography, $faker, $value) : void
    {
        unset($faker);
        $applyCrew = isset($value['applyCrew']) ?
            $value['applyCrew'] : \Base\Sdk\Crew\Utils\MockFactory::generateCrew(1);
        $creditPhotography->setApplyCrew($applyCrew);
    }

    private static function generateApplyStatus($creditPhotography, $faker, $value) : void
    {
        $applyStatus = isset($value['applyStatus']) ?
            $value['applyStatus'] :$faker->randomElement(CreditPhotography::APPLY_STATUS);
        $creditPhotography->setApplyStatus($applyStatus);
    }

    private static function generateStatus($creditPhotography, $faker, $value) : void
    {
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(CreditPhotography::STATUS);
        $creditPhotography->setStatus($status);
    }
}
