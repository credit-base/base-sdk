<?php
namespace Base\Sdk\CreditPhotography\Adapter\CreditPhotography;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Model\NullCreditPhotography;
use Base\Sdk\CreditPhotography\Translator\CreditPhotographyRestfulTranslator;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class CreditPhotographyRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(CreditPhotographyRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends CreditPhotographyRestfulAdapter {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsICreditPhotographyAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Adapter\CreditPhotography\ICreditPhotographyAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('creditPhotography', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
    /**
    * 循环测试 testScenario() 数据构建器
    */
    public function scenarioDataProvider()
    {
        return [
            [
                'CREDIT_PHOTOGRAPHY_LIST',
                CreditPhotographyRestfulAdapter::SCENARIOS['CREDIT_PHOTOGRAPHY_LIST']
            ],
            [
                'CREDIT_PHOTOGRAPHY_FETCH_ONE',
                CreditPhotographyRestfulAdapter::SCENARIOS['CREDIT_PHOTOGRAPHY_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    public function testFetchOne()
    {
        $id = 1;

        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullCreditPhotography::getInstance())
            ->willReturn($creditPhotography);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($creditPhotography, $result);
    }

    /**
     * 为CreditPhotographyRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$CreditPhotography,$keys,$CreditPhotographyArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareCreditPhotographyTranslator(
        CreditPhotography $creditPhotography,
        array $keys,
        array $creditPhotographyArray
    ) {
        $translator = $this->prophesize(CreditPhotographyRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($creditPhotography),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($creditPhotographyArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(CreditPhotography $creditPhotography)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($creditPhotography);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCreditPhotographyTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行approve（）
     * 判断 result 是否为true
     */
    public function testApproveSuccess()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);
        $creditPhotographyArray = array();

        $this->prepareCreditPhotographyTranslator(
            $creditPhotography,
            array(
                'applyCrew',
            ),
            $creditPhotographyArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$creditPhotography->getId().'/approve', $creditPhotographyArray);

        $this->success($creditPhotography);
        $result = $this->adapter->approve($creditPhotography);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCreditPhotographyTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行approve（）
     * 判断 result 是否为false
     */
    public function testApproveFailure()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);
        $creditPhotographyArray = array();

        $this->prepareCreditPhotographyTranslator(
            $creditPhotography,
            array(
                'applyCrew',
            ),
            $creditPhotographyArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$creditPhotography->getId().'/approve', $creditPhotographyArray);
        
        $this->failure($creditPhotography);
        $result = $this->adapter->approve($creditPhotography);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCreditPhotographyTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行reject（）
     * 判断 result 是否为true
     */
    public function testRejectSuccess()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);
        $creditPhotographyArray = array();

        $this->prepareCreditPhotographyTranslator(
            $creditPhotography,
            array(
                'rejectReason',
                'applyCrew'
            ),
            $creditPhotographyArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$creditPhotography->getId().'/reject', $creditPhotographyArray);

        $this->success($creditPhotography);
        $result = $this->adapter->reject($creditPhotography);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCreditPhotographyTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行reject（）
     * 判断 result 是否为false
     */
    public function testRejectFailure()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);
        $creditPhotographyArray = array();

        $this->prepareCreditPhotographyTranslator(
            $creditPhotography,
            array(
                'rejectReason',
                'applyCrew'
            ),
            $creditPhotographyArray
        );

        $this->adapter
            ->method('patch')
            ->with('/'.$creditPhotography->getId().'/reject', $creditPhotographyArray);
        
        $this->failure($creditPhotography);
        $result = $this->adapter->reject($creditPhotography);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCreditPhotographyTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行 success（）
     * 执行 add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);
        $creditPhotographyArray = array();

        $this->prepareCreditPhotographyTranslator(
            $creditPhotography,
            array(
                'description',
                'attachments',
                'member'
            ),
            $creditPhotographyArray
        );

        $this->adapter
            ->method('post')
            ->with('', $creditPhotographyArray);

        $this->success($creditPhotography);
        $result = $this->adapter->add($creditPhotography);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCreditPhotographyTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行 success（）
     * 执行 add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);
        $creditPhotographyArray = array();

        $this->prepareCreditPhotographyTranslator(
            $creditPhotography,
            array(
                'description',
                'attachments',
                'member'
            ),
            $creditPhotographyArray
        );

        $this->adapter
            ->method('post')
            ->with('', $creditPhotographyArray);
        
        $this->failure($creditPhotography);
        $result = $this->adapter->add($creditPhotography);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCreditPhotographyTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行delete（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);

        $this->adapter
            ->method('patch')
            ->with('/'.$creditPhotography->getId().'/delete');

        $this->success($creditPhotography);
        $result = $this->adapter->deletes($creditPhotography);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCreditPhotographyTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行delete（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);

        $this->adapter
            ->method('patch')
            ->with('/'.$creditPhotography->getId().'/delete');
        
        $this->failure($creditPhotography);
        $result = $this->adapter->deletes($creditPhotography);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $creditPhotography = new NullCreditPhotography();
        
        $result = $this->adapter->edit($creditPhotography);
        $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [
            101 => [
                'description' => DESCRIPTION_FORMAT_ERROR,
                'attachments' => ATTACHMENT_FORMAT_ERROR,
                'memberId' => PARAMETER_FORMAT_ERROR,
                'applyCrewId' => PARAMETER_FORMAT_ERROR,
                'rejectReason' => REASON_FORMAT_ERROR,
            ],
            102 =>[
                'status' => STATUS_CAN_NOT_MODIFY,
                'applyStatus' => STATUS_CAN_NOT_MODIFY,
            ]
        ];
        $result = $this->childAdapter->getMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
