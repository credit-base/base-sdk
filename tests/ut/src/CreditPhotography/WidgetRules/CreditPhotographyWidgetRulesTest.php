<?php
namespace Base\Sdk\CreditPhotography\WidgetRules;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class CreditPhotographyWidgetRulesTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = CreditPhotographyWidgetRules::getInstance();

        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //attachments -- start
    /**
     * @dataProvider invalidAttachmentsProvider
     */
    public function testAttachmentsInvalid($actual, $expected)
    {
        $result = $this->widgetRule->attachments($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
    }

    public function invalidAttachmentsProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(array(), false),
            array($faker->name, false),
            array(array(
                array('name'=>$faker->name, 'identify'=>$faker->name)
            ), false),
            array(array(
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'2.jpg'),
                array('name'=>$faker->name,'identify'=>'3.jpg'),
                array('name'=>$faker->name,'identify'=>'4.jpg'),
                array('name'=>$faker->name,'identify'=>'5.jpg'),
                array('name'=>$faker->name,'identify'=>'6.jpg'),
                array('name'=>$faker->name,'identify'=>'7.jpg'),
                array('name'=>$faker->name,'identify'=>'8.jpg'),
                array('name'=>$faker->name,'identify'=>'9.jpg'),
            ), false),
            array(array(
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
                array('name'=>$faker->name,'identify'=>'1.jpg'),
            ), true),
            array(array(
                array('name'=>$faker->name,'identify'=>'1.mp4'),
                array('name'=>$faker->name,'identify'=>'1.mp4'),
            ), false),
            array(array(
                array('name'=>$faker->name,'identify'=>'1.mp4'),
            ), true),
        );
    }
    //attachments -- end

    //description -- start
    /**
     * @dataProvider invalidDescriptionProvider
     */
    public function testDescription($actual, $expected)
    {
        $result = $this->widgetRule->description($actual);

        if (!$expected) {
            $this->assertFalse($result);
            
            $this->assertEquals(DESCRIPTION_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function invalidDescriptionProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('titletitle', true),
            array('', false),
            array($faker->randomDigit, false)
        );
    }
    //title -- end

    //isVideo -- start
    /**
     * @dataProvider invalidIsVideoProvider
     */
    public function testIsVideoInvalid($actual, $expected)
    {
        $result = $this->widgetRule->isVideo($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
    }

    public function invalidIsVideoProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('1.jpg', false),
            array('identify'=>'1.mp4', true),
        );
    }
    //isVideo -- end
}
