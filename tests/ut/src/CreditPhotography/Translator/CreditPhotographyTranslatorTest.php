<?php
namespace Base\Sdk\CreditPhotography\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\CreditPhotography\Utils\CreditPhotographyUtils;

class CreditPhotographyTranslatorTest extends TestCase
{
    use CreditPhotographyUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new CreditPhotographyTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\CreditPhotography\Model\NullCreditPhotography', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);
        
        $expression = $this->translator->objectToArray($creditPhotography);
        
        $this->compareArrayAndObject($expression, $creditPhotography);
    }

    public function testObjectToArrayFail()
    {
        $creditPhotography = null;

        $expression = $this->translator->objectToArray($creditPhotography);
        $this->assertEquals(array(), $expression);
    }
}
