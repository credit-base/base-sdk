<?php
namespace Base\Sdk\CreditPhotography\Translator;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\CreditPhotography\Utils\CreditPhotographyRestfulUtils;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;
use Base\Sdk\Member\Translator\MemberRestfulTranslator;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Crew\Model\Crew;

class CreditPhotographyRestfulTranslatorTest extends TestCase
{
    use CreditPhotographyRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new CreditPhotographyRestfulTranslator();
        $this->childTranslator = new class extends CreditPhotographyRestfulTranslator
        {
            public function getCrewRestfulTranslator(): CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }

            public function getMemberRestfulTranslator(): MemberRestfulTranslator
            {
                return parent::getMemberRestfulTranslator();
            }

        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);

        $expression['data']['id'] = $creditPhotography->getId();
        $expression['data']['attributes']['description'] = $creditPhotography->getDescription();
        $expression['data']['attributes']['applyStatus'] = $creditPhotography->getApplyStatus();
        $expression['data']['attributes']['attachments'] = $creditPhotography->getAttachments();
        $expression['data']['attributes']['rejectReason'] = $creditPhotography->getRejectReason();

        $expression['data']['attributes']['createTime'] = $creditPhotography->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $creditPhotography->getUpdateTime();
        $expression['data']['attributes']['status'] = $creditPhotography->getStatus();
        $expression['data']['attributes']['statusTime'] = $creditPhotography->getStatusTime();

        $creditPhotographyObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\CreditPhotography\Model\CreditPhotography', $creditPhotographyObject);
        $this->compareArrayAndObject($expression, $creditPhotographyObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $creditPhotography = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\CreditPhotography\Model\NullCreditPhotography', $creditPhotography);
    }

    public function testObjectToArray()
    {
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography(1);

        $expression = $this->translator->objectToArray($creditPhotography);

        $this->compareArrayAndObject($expression, $creditPhotography);
    }

    public function testObjectToArrayFail()
    {
        $creditPhotography = null;

        $expression = $this->translator->objectToArray($creditPhotography);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['member']['data'] 和
     * $relationships['applyCrew']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用两次, 分别用于处理 crew 和 applyCrew 且返回各自的数组 $applyCrew 和 $member
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用一次, 入参是 $applyCrewInfo, 出参是 $applyCrew
     * 5. $this->getMemberRestfulTranslator()->arrayToObject 被调用一次, 入参是 $memberInfo, 出参是 $member
     * 6. $journal->setApplyCrew 调用一次, 入参 $applyCrew
     * 7. $journal->setMember 调用一次, 入参 $member
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'applyCrew'=>['data'=>'mockApplyCrew'],
            'member'=>['data'=>'mockMember']
        ];

        $applyCrewInfo = ['mockApplyCrewInfo'];
        $memberInfo = ['mockMemberInfo'];

        $applyCrew = new Crew();
        $member = new Member();

        $translator = $this->getMockBuilder(CreditPhotographyRestfulTranslator::class)
                           ->setMethods([
                                'relationship',
                                'changeArrayFormat',
                                'getCrewRestfulTranslator',
                                'getMemberRestfulTranslator',
                            ])
                           ->getMock();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用2次, 依次入参 applyCrew, member
        //依次返回 $applyCrewInfo 和 $memberInfo
        $translator->expects($this->exactly(2))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['applyCrew']['data']],
                 [$relationships['member']['data']]
             )
            ->will($this->onConsecutiveCalls($applyCrewInfo, $memberInfo));
  
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($applyCrewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($applyCrew);

        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($memberInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($member);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $translator->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        //揭示
        $creditPhotography = $translator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\CreditPhotography\Model\CreditPhotography', $creditPhotography);
        $this->assertEquals($applyCrew, $creditPhotography->getApplyCrew());
        $this->assertEquals($member, $creditPhotography->getMember());
    }

    public function testGetMemberRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Member\Translator\MemberRestfulTranslator',
            $this->childTranslator->getMemberRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }
}
