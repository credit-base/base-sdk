<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository;

class ApproveCreditPhotographyCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockApproveCreditPhotographyCommandHandler::class)
            ->setMethods(['fetchCreditPhotography'])
            ->getMock();
    }

    public function testFetchIApplyObject()
    {
        $commandHandler = $this->getMockBuilder(MockApproveCreditPhotographyCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography($id);

        $repository = $this->prophesize(CreditPhotographyRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($creditPhotography);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $creditPhotography);
    }

    public function testExtendsApproveCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\ApproveCommandHandler',
            $this->stub
        );
    }
}
