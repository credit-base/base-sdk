<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\Sdk\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand;

class DeleteCreditPhotographyCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(DeleteCreditPhotographyCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockDeleteCreditPhotographyCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository',
            $commandHandler->getRepository()
        );
    }

    public function testExecute()
    {
        $command = new DeleteCreditPhotographyCommand(
            $this->faker->randomNumber()
        );

        $creditPhotography = $this->prophesize(CreditPhotography::class);
        $creditPhotography->delete()->shouldBeCalledTimes(1)->willReturn(true);

        $creditPhotographyRepository = $this->prophesize(CreditPhotographyRepository::class);
        $creditPhotographyRepository->fetchOne(Argument::exact($command->id))
            ->shouldBeCalledTimes(1)
            ->willReturn($creditPhotography->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($creditPhotographyRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = new DeleteCreditPhotographyCommand(
            $this->faker->randomNumber()
        );

        $creditPhotography = $this->prophesize(CreditPhotography::class);
        $creditPhotography->delete()->shouldBeCalledTimes(1)->willReturn(false);

        $creditPhotographyRepository = $this->prophesize(CreditPhotographyRepository::class);
        $creditPhotographyRepository->fetchOne(Argument::exact($command->id))
            ->shouldBeCalledTimes(1)
            ->willReturn($creditPhotography->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($creditPhotographyRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }
}
