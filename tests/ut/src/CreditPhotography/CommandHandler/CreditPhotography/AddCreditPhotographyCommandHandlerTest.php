<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand;

class AddCreditPhotographyCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddCreditPhotographyCommandHandler::class)
            ->setMethods(['getCreditPhotography'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetCreditPhotography()
    {
        $commandHandler = new MockAddCreditPhotographyCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Model\CreditPhotography',
            $commandHandler->getCreditPhotography()
        );
    }

    public function testExecute()
    {
        $command = new AddCreditPhotographyCommand(
            $this->faker->word,
            array()
        );

        $parentTask = $this->prophesize(CreditPhotography::class);
        $parentTask->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);
        $parentTask->setAttachments(Argument::exact($command->attachments))->shouldBeCalledTimes(1);
        $parentTask->add()->shouldBeCalledTimes(1)->willReturn(true);

        $this->commandHandler->expects($this->any())
            ->method('getCreditPhotography')
            ->willReturn($parentTask->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
