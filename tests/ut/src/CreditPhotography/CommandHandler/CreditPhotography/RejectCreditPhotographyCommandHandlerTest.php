<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\Sdk\CreditPhotography\Command\CreditPhotography\RejectCreditPhotographyCommand;

class RejectCreditPhotographyCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockRejectCreditPhotographyCommandHandler();
    }

    public function testExtendsRejectCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\RejectCommandHandler',
            $this->stub
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchIEnableObject()
    {
        $commandHandler = $this->getMockBuilder(MockRejectCreditPhotographyCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $creditPhotography = \Base\Sdk\CreditPhotography\Utils\MockFactory::generateCreditPhotography($id);

        $repository = $this->prophesize(CreditPhotographyRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($creditPhotography);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIApplyObject($id);

        $this->assertEquals($result, $creditPhotography);
    }
}
