<?php
namespace Base\Sdk\UserGroup\CommandHandler;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\UserGroup\Model\Department;
use Base\Sdk\UserGroup\Command\EditDepartmentCommand;
use Base\Sdk\UserGroup\Repository\DepartmentRepository;

class EditDepartmentCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditDepartmentCommandHandler::class)
                                     ->setMethods(['getRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockEditDepartmentCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Repository\DepartmentRepository',
            $commandHandler->getRepository()
        );
    }

    public function initialExecute($result)
    {
        $command = new EditDepartmentCommand(
            $this->faker->randomNumber(),
            $this->faker->name
        );

        $department = $this->prophesize(Department::class);
        $department->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $department->edit()->shouldBeCalledTimes(1)->willReturn($result);

        $departmentRepository = $this->prophesize(DepartmentRepository::class);
        $departmentRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($department->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($departmentRepository->reveal());

        return $command;
    }
    
    public function testExecute()
    {
        $command = $this->initialExecute(true);

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }
}
