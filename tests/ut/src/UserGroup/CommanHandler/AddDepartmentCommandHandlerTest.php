<?php
namespace Base\Sdk\UserGroup\CommandHandler;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Model\Department;
use Base\Sdk\UserGroup\Command\AddDepartmentCommand;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class AddDepartmentCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddDepartmentCommandHandler::class)
                                     ->setMethods(['getDepartment', 'getUserGroupRepository'])
                                     ->getMock();
                                     
        $this->childStub = new class extends AddDepartmentCommandHandler{
            public function getUserGroupRepository() : UserGroupRepository
            {
                return parent::getUserGroupRepository();
            }
            public function getDepartment() : Department
            {
                return parent::getDepartment();
            }
        };

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetDepartment()
    {
        $commandHandler = new MockAddDepartmentCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Model\Department',
            $commandHandler->getDepartment()
        );
    }

    public function testGetUserGroupRepository()
    {
        $commandHandler = new MockAddDepartmentCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Repository\UserGroupRepository',
            $commandHandler->getUserGroupRepository()
        );
    }

    public function initialExecute($result)
    {
        $this->commandHandler = $this->getMockBuilder(AddDepartmentCommandHandler::class)
                ->setMethods(['getDepartment', 'getUserGroupRepository'])
                ->getMock();

        $name = 'name';
        $userGroupId = 1;

        $command = new AddDepartmentCommand(
            $name,
            $userGroupId //userGroupId
        );

        $userGroup = \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);
       
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($userGroupId))
                             ->shouldBeCalledTimes(1)->willReturn($userGroup);
        $this->commandHandler->expects($this->exactly(1))
                   ->method('getUserGroupRepository')
                   ->willReturn($userGroupRepository->reveal());

        $department = $this->prophesize(Department::class);
        $department->setName($name)->shouldBeCalledTimes(1);
        $department->setUserGroup(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        $department->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->any())
            ->method('getDepartment')
            ->willReturn($department->reveal());
                    
        return $command;
    }

    public function testExecuteSuccess()
    {
        $command = $this->initialExecute(true);
        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initialExecute(false);
        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }
}
