<?php
namespace Base\Sdk\UserGroup\Command;

use PHPUnit\Framework\TestCase;

class AddDepartmentCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'name' => $faker->name(),
            'userGroupId' => $faker->randomNumber()
        );

        $this->command = new AddDepartmentCommand(
            $this->fakerData['name'],
            $this->fakerData['userGroupId']
        );
    }

    public function testImplementsICommand()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommand',
            $this->command
        );
    }

    public function testDepartmentNameParameter()
    {
        $this->assertEquals($this->fakerData['name'], $this->command->name);
    }

    public function testUserGroupIdParameter()
    {
        $this->assertEquals($this->fakerData['userGroupId'], $this->command->userGroupId);
    }
}
