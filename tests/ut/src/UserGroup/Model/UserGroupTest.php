<?php
namespace Base\Sdk\UserGroup\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class UserGroupTest extends TestCase
{
    private $userGroup;

    public function setUp()
    {
        $this->userGroup = new UserGroup();
    }

    public function tearDown()
    {
        unset($this->userGroup);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->userGroup
        );
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->userGroup->getId());

        $this->assertEmpty($this->userGroup->getName());

        $this->assertEmpty($this->userGroup->getShortName());

        $this->assertEquals(0, $this->userGroup->getUpdateTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 UserGroup setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->userGroup->setId(1);
        $this->assertEquals(1, $this->userGroup->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 UserGroup setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->userGroup->setName('string');
        $this->assertEquals('string', $this->userGroup->getName());
    }

    /**
     * 设置 UserGroup setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->userGroup->setName(array(1,2,3));
    }
    //name 测试 --------------------------------------------------------   end

    //shortName 测试 -------------------------------------------------------- start
    /**
     * 设置 UserGroup setShortName() 正确的传参类型,期望传值正确
     */
    public function testSetShortNameCorrectType()
    {
        $this->userGroup->setShortName('string');
        $this->assertEquals('string', $this->userGroup->getShortName());
    }

    /**
     * 设置 UserGroup setShortName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetShortNameWrongType()
    {
        $this->userGroup->setShortName(array(1,2,3));
    }
    //shortName 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 UserGroup setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->userGroup->setStatus(0);
        $this->assertEquals(0, $this->userGroup->getStatus());
    }

    /**
     * 设置 UserGroup setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->userGroup->setStatus(array(1,2,3));
    }
    //status 测试 --------------------------------------------------------   end
}
