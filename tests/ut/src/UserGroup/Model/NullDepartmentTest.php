<?php
namespace Base\Sdk\UserGroup\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullDepartmentTest extends TestCase
{
    private $department;
    private $childDepartment;

    public function setUp()
    {
        $this->department = NullDepartment::getInstance();
        $this->childDepartment = new class extends NullDepartment
        {
            public function resourceNotExist() : bool
            {
                return parent::resourceNotExist();
            }
        };
    }

    public function tearDown()
    {
        unset($this->department);
        unset($this->childDepartment);
    }

    public function testExtendsUserGroup()
    {
        $this->assertInstanceof('Base\Sdk\UserGroup\Model\Department', $this->department);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->department);
    }

    public function testAdd()
    {
        $department = $this->getMockBuilder(NullDepartment::class)
                                ->setMethods(['resourceNotExist'])
                                ->getMock();
        $department->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($department->add());
    }

    public function testEdit()
    {
        $department = $this->getMockBuilder(NullDepartment::class)
                                ->setMethods(['resourceNotExist'])
                                ->getMock();
        $department->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($department->edit());
    }

    public function testResourceNotExist()
    {
        $result = $this->childDepartment->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
