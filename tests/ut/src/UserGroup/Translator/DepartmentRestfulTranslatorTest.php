<?php
namespace Base\Sdk\UserGroup\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\UserGroup\Model\Department;
use Base\Sdk\UserGroup\Utils\DepartmentRestfulUtils;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

class DepartmentRestfulTranslatorTest extends TestCase
{
    use DepartmentRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new DepartmentRestfulTranslator();
        $this->childTranslator = new class extends DepartmentRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $this->translator = $this->getMockBuilder(DepartmentRestfulTranslator::class)
                ->setMethods([
                    'getUserGroupRestfulTranslator', 'relationship'
                ])
                ->getMock();

        $department = \Base\Sdk\UserGroup\Utils\MockDepartmentFactory::generateDepartment(1);

        $expression['data']['id'] = $department->getId();
        $expression['data']['attributes']['name'] = $department->getName();
        $expression['data']['attributes']['createTime'] = $department->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $department->getUpdateTime();
        $expression['data']['attributes']['status'] = $department->getStatus();
        $expression['data']['relationships']['userGroup']['data'] = [
            [
                "type" => "userGroups",
                "id" => $department->getUserGroup()->getId()
            ]
        ];

        $userGroupRestfulTranslator = new UserGroupRestfulTranslator();
        $expression['included'][] = $userGroupRestfulTranslator->objectToArray($department->getUserGroup());

        $this->translator->expects($this->exactly(1))
            ->method('relationship')
            ->willReturn($expression['data']['relationships']);

        $userGroup = new UserGroup($department->getUserGroup()->getId());
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(
            Argument::exact($expression['data']['relationships']['userGroup'])
        )->shouldBeCalledTimes(1)->willReturn($userGroup);

        $this->translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());

        $departmentObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\UserGroup\Model\Department', $departmentObject);
        $this->compareArrayAndObject($expression, $departmentObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $department = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\UserGroup\Model\NullDepartment', $department);
    }

    public function testObjectToArray()
    {
        $department = \Base\Sdk\UserGroup\Utils\MockDepartmentFactory::generateDepartment(1);

        $expression = $this->translator->objectToArray($department);

        $this->compareArrayAndObject($expression, $department);
    }

    public function testObjectToArrayFail()
    {
        $department = null;

        $expression = $this->translator->objectToArray($department);
        $this->assertEquals(array(), $expression);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }
}
