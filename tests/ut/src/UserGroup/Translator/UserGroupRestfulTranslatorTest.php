<?php
namespace Base\Sdk\UserGroup\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\UserGroup\Utils\UserGroupRestfulUtils;
use Base\Sdk\UserGroup\Model\UserGroup;

class UserGroupRestfulTranslatorTest extends TestCase
{
    use UserGroupRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UserGroupRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $userGroup = \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);

        $expression['data']['id'] = $userGroup->getId();
        $expression['data']['attributes']['name'] = $userGroup->getName();
        $expression['data']['attributes']['shortName'] = $userGroup->getShortName();
        $expression['data']['attributes']['createTime'] = $userGroup->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $userGroup->getUpdateTime();
        $expression['data']['attributes']['status'] = $userGroup->getStatus();

        $userGroupObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\UserGroup\Model\UserGroup', $userGroupObject);
        $this->compareArrayAndObject($expression, $userGroupObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $department = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\UserGroup\Model\NullUserGroup', $department);
    }

    public function testObjectToArray()
    {
        $userGroup = \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);

        $expression = $this->translator->objectToArray($userGroup);

        $this->compareArrayAndObject($expression, $userGroup);
    }

    public function testObjectToArrayFail()
    {
        $userGroup = null;

        $expression = $this->translator->objectToArray($userGroup);
        $this->assertEquals(array(), $expression);
    }
}
