<?php
namespace Base\Sdk\UserGroup\WidgetRules;

use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Base\Sdk\Common\Utils\StringGenerate;

class DepartmentWidgetRulesTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = DepartmentWidgetRules::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //departmentName -- start
    /**
     * @dataProvider invalidDepartmentNameProvider
     */
    public function testDepartmentNameInvalid($actual, $expected)
    {
        $result = $this->widgetRule->departmentName($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DEPARTMENT_NAME_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidDepartmentNameProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(DepartmentWidgetRules::NAME_MIN_LENGTH-1), false),
            array(StringGenerate::generate(DepartmentWidgetRules::NAME_MAX_LENGTH+1), false),
            array(StringGenerate::generate(DepartmentWidgetRules::NAME_MIN_LENGTH), true),
            array(StringGenerate::generate(DepartmentWidgetRules::NAME_MIN_LENGTH+1), true),
            array(StringGenerate::generate(DepartmentWidgetRules::NAME_MAX_LENGTH), true),
            array(StringGenerate::generate(DepartmentWidgetRules::NAME_MAX_LENGTH-1), true)
        );
    }
    //departmentName -- end
}
