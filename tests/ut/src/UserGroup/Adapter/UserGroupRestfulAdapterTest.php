<?php
namespace Base\Sdk\UserGroup\Adapter;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Model\NullUserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

class UserGroupRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UserGroupRestfulAdapter::class)
                           ->setMethods([
                                'getTranslator',
                                'getResource',
                                'fetchOneAction',
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends UserGroupRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIUserGroupAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Adapter\IUserGroupAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('userGroups', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'USERGROUP_LIST',
                UserGroupRestfulAdapter::SCENARIOS['USERGROUP_LIST']
            ],
            [
                'USERGROUP_FETCH_ONE',
                UserGroupRestfulAdapter::SCENARIOS['USERGROUP_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $userGroup = \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup(1);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullUserGroup())
            ->willReturn($userGroup);
        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($userGroup, $result);
    }
}
