<?php
namespace Base\Sdk\UserGroup\Utils;

trait UserGroupUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $userGroup
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($userGroup->getId()));
        $this->assertEquals($expectedArray['name'], $userGroup->getName());
        $this->assertEquals($expectedArray['shortName'], $userGroup->getShortName());
        $this->assertEquals($expectedArray['updateTime'], $userGroup->getUpdateTime());
        $this->assertEquals($expectedArray['updateTimeFormat'], date('Y年m月d日 H点i分', $userGroup->getUpdateTime()));
    }
}
