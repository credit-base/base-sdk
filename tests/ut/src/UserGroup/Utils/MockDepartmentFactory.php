<?php
namespace Base\Sdk\UserGroup\Utils;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Model\Department;

class MockDepartmentFactory
{
    public static function generateDepartment(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Department {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $department = new Department($id);
        $department->setId($id);

        //name
        self::generateName($department, $faker, $value);
        //userGroup
        self::generateUserGroup($department, $faker, $value);

        $department->setStatus(0);
        $department->setCreateTime($faker->unixTime());
        $department->setUpdateTime($faker->unixTime());
        $department->setStatusTime($faker->unixTime());

        return $department;
    }

    private static function generateName($department, $faker, $value) : void
    {
        $name = isset($value['name']) ?
            $value['name'] :
            $faker->title;
        
        $department->setName($name);
    }

    private static function generateUserGroup($department, $faker, $value) : void
    {
        $userGroup = isset($value['userGroup']) ?
            new UserGroup($value['userGroup']) :
            \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($faker->randomDigit());
        $department->setUserGroup($userGroup);
    }
}
