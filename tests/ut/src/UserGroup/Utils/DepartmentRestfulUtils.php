<?php
namespace Base\Sdk\UserGroup\Utils;

trait DepartmentRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $department
    ) {
        $this->assertEquals($expectedArray['data']['id'], $department->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $department->getName());
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $department->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $department->getUpdateTime());
        }
        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals($expectedArray['data']['attributes']['status'], $department->getStatus());
        }
    }
}
