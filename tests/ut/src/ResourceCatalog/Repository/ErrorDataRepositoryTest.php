<?php
namespace Base\Sdk\ResourceCatalog\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Adapter\ErrorData\ErrorDataRestfulAdapter;

class ErrorDataRepositoryTest extends TestCase
{
    private $repository;
    
    private $childRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(ErrorDataRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->childRepository = new class extends ErrorDataRepository
        {
            public function getActualAdapter()
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter()
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->childRepository);
    }

    public function testImplementsIErrorDataAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Adapter\ErrorData\IErrorDataAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Adapter\ErrorData\ErrorDataRestfulAdapter',
            $this->childRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Adapter\ErrorData\ErrorDataRestfulAdapter',
            $this->childRepository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(ErrorDataRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
