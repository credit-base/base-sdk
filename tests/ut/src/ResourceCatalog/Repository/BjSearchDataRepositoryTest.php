<?php
namespace Base\Sdk\ResourceCatalog\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Adapter\BjSearchData\BjSearchDataRestfulAdapter;

class BjSearchDataRepositoryTest extends TestCase
{
    private $repository;
    
    private $childRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(BjSearchDataRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->childRepository = new class extends BjSearchDataRepository
        {
            public function getActualAdapter()
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter()
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->childRepository);
    }

    public function testImplementsIBjSearchDataAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Adapter\BjSearchData\IBjSearchDataAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Adapter\BjSearchData\BjSearchDataRestfulAdapter',
            $this->childRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Adapter\BjSearchData\BjSearchDataRestfulAdapter',
            $this->childRepository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(BjSearchDataRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testConfirm()
    {
        $bjSearchData =
            \Base\Sdk\ResourceCatalog\Utils\BjSearchData\BjSearchDataMockFactory::generateBjSearchData(1);

        $adapter = $this->prophesize(BjSearchDataRestfulAdapter::class);
        $adapter->confirm(Argument::exact($bjSearchData))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->confirm($bjSearchData);
    }
}
