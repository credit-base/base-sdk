<?php
namespace Base\Sdk\ResourceCatalog\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Adapter\Task\TaskRestfulAdapter;

class TaskRepositoryTest extends TestCase
{
    private $repository;
    
    private $childRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(TaskRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->childRepository = new class extends TaskRepository
        {
            public function getActualAdapter()
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter()
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->repository);
        unset($this->childRepository);
    }

    public function testImplementsITaskAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Adapter\Task\ITaskAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Adapter\Task\TaskRestfulAdapter',
            $this->childRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Adapter\Task\TaskRestfulAdapter',
            $this->childRepository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(TaskRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }
}
