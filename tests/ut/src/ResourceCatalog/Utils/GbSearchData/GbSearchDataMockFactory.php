<?php
namespace Base\Sdk\ResourceCatalog\Utils\GbSearchData;

use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Utils\SearchDataMockFactoryTrait;

class GbSearchDataMockFactory
{
    use SearchDataMockFactoryTrait;
    
    public static function generateGbSearchData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : GbSearchData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $gbSearchData = new GbSearchData($id);
        $gbSearchData->setId($id);

        self::generateInfoClassify($gbSearchData, $faker, $value);
        self::generateInfoCategory($gbSearchData, $faker, $value);
        self::generateCrew($gbSearchData, $faker, $value);
        self::generateSourceUnit($gbSearchData, $faker, $value);
        self::generateSubjectCategory($gbSearchData, $faker, $value);
        self::generateDimension($gbSearchData, $faker, $value);
        self::generateName($gbSearchData, $faker, $value);
        self::generateIdentify($gbSearchData, $faker, $value);
        self::generateExpirationDate($gbSearchData, $faker, $value);
        self::generateStatus($gbSearchData, $faker, $value);
        self::generateTemplate($gbSearchData, $faker, $value);
        self::generateItemsData($gbSearchData, $faker, $value);
        
        $gbSearchData->setCreateTime($faker->unixTime());
        $gbSearchData->setUpdateTime($faker->unixTime());
        $gbSearchData->setStatusTime($faker->unixTime());

        return $gbSearchData;
    }

    private static function generateTemplate($gbSearchData, $faker, $value) : void
    {
        $template = isset($value['template']) ?
        $value['template'] :
        \Base\Sdk\Template\Utils\GbTemplate\MockGbTemplateFactory::generateGbTemplate($faker->randomDigit());
        
        $gbSearchData->setTemplate($template);
    }

    private static function generateItemsData($gbSearchData, $faker, $value) : void
    {
        $itemsData = isset($value['itemsData']) ?
        $value['itemsData'] :
        \Base\Sdk\ResourceCatalog\Utils\ItemsDataMockFactory::generateItemsData($faker->randomDigit());
        
        $gbSearchData->setItemsData($itemsData);
    }
    protected static function generateStatus($gbSearchData, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                GbSearchData::STATUS
            );
        
        $gbSearchData->setStatus($status);
    }
}
