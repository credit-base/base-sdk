<?php
namespace Base\Sdk\ResourceCatalog\Utils\GbSearchData;

trait GbSearchDataRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $gbSearchData
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $gbSearchData->getId());
        }
        $this->assertEquals(
            $expectedArray['data']['attributes']['dimension'],
            $gbSearchData->getDimension()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['name'],
            $gbSearchData->getName()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['infoClassify'],
            $gbSearchData->getInfoClassify()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['infoCategory'],
            $gbSearchData->getInfoCategory()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $gbSearchData->getSubjectCategory()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['identify'],
            $gbSearchData->getIdentify()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['expirationDate'],
            $gbSearchData->getExpirationDate()
        );
    }
}
