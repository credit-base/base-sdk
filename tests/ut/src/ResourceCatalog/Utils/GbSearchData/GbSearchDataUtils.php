<?php
namespace Base\Sdk\ResourceCatalog\Utils\GbSearchData;

use Base\Sdk\Template\Translator\GbTemplateTranslator;
use Base\Sdk\ResourceCatalog\Model\ISearchData;

use Base\Sdk\ResourceCatalog\Translator\ItemsDataTranslator;

trait GbSearchDataUtils
{
    protected function getGbTemplateTranslator():GbTemplateTranslator
    {
        return new GbTemplateTranslator();
    }

    protected function getItemsDataTranslator() : ItemsDataTranslator
    {
        return new ItemsDataTranslator();
    }
    
    private function compareArrayAndObjectGbSearchData(
        array $expectedArray,
        $object
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $object);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($object->getId()));
      
        $this->assertEquals(
            $expectedArray['infoCategory']['id'],
            marmot_encode($object->getInfoCategory())
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['name'],
            ISearchData::INFO_CATEGORY_CN[$object->getInfoCategory()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['label'],
            ISearchData::INFO_CATEGORY_LABEL[$object->getInfoCategory()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['type'],
            ISearchData::INFO_CATEGORY_TYPE[$object->getInfoCategory()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['id'],
            marmot_encode($object->getInfoClassify())
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['name'],
            ISearchData::INFO_CLASSIFY_CN[$object->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['label'],
            ISearchData::INFO_CLASSIFY_LABEL[$object->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['type'],
            ISearchData::INFO_CLASSIFY_TYPE[$object->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['subjectCategory']['id'],
            'MQ'
        );
        $this->assertEquals(
            $expectedArray['subjectCategory']['name'],
            '自然人'
        );
        
        $this->assertEquals(
            $expectedArray['name'],
            $object->getName()
        );
        $this->assertEquals(
            $expectedArray['identify'],
            $object->getIdentify()
        );
        $this->assertEquals(
            $expectedArray['dimension']['id'],
            marmot_encode($object->getDimension())
        );
        $this->assertEquals(
            $expectedArray['dimension']['name'],
            ISearchData::DIMENSION_CN[$object->getDimension()]
        );
        $this->assertEquals(
            $expectedArray['dimension']['label'],
            ISearchData::DIMENSION_LABEL[$object->getDimension()]
        );
        $this->assertEquals(
            $expectedArray['dimension']['type'],
            ISearchData::DIMENSION_TYPE[$object->getDimension()]
        );
        $this->assertEquals(
            $expectedArray['expirationDate'],
            $object->getExpirationDate()
        );
        $this->assertEquals(
            $expectedArray['expirationDateFormat'],
            date(
                'Y-m-d',
                $object->getExpirationDate()
            )
        );
        $this->assertEquals(
            $expectedArray['crew']['id'],
            marmot_encode($object->getCrew()->getId())
        );
        $this->assertEquals(
            $expectedArray['crew']['name'],
            $object->getCrew()->getRealName()
        );
        $this->assertEquals(
            $expectedArray['sourceUnit']['id'],
            marmot_encode($object->getSourceUnit()->getId())
        );
        $this->assertEquals(
            $expectedArray['sourceUnit']['name'],
            $object->getSourceUnit()->getName()
        );
        $this->assertEquals(
            $expectedArray['itemsData'],
            $this->getItemsDataTranslator()->objectToArray(
                $object->getItemsData()
            )
        );
        $this->assertEquals(
            $expectedArray['template'],
            $this->getGbTemplateTranslator()->objectToArray(
                $object->getTemplate()
            )
        );
        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($object->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            ISearchData::DATA_STATUS_CN[$object->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            ISearchData::DATA_STATUS_TYPE[$object->getStatus()]
        );
    }
}
