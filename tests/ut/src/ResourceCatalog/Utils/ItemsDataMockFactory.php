<?php
namespace Base\Sdk\ResourceCatalog\Utils;

use Base\Sdk\ResourceCatalog\Model\ItemsData;

class ItemsDataMockFactory
{
    public static function generateItemsData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ItemsData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $itemsData = new ItemsData($id);
        $itemsData->setId($id);

        self::generateData($itemsData, $faker, $value);

        return $itemsData;
    }

    private static function generateData($itemsData, $faker, $value) : void
    {
        $data = isset($value['data']) ?
            $value['data'] :
            $faker->words(3, false);
        
        $itemsData->setData($data);
    }
}
