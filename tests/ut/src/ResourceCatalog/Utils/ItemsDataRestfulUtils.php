<?php
namespace Base\Sdk\ResourceCatalog\Utils;

trait ItemsDataRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $object->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['data'], $object->getData());
    }

    private function compareObjectToArrayCommon(
        array $expectedArray,
        $itemsData
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($itemsData->getId()));
        $this->assertEquals($expectedArray['data'], $itemsData->getData());
    }
}
