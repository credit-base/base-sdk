<?php
namespace Base\Sdk\ResourceCatalog\Utils\Task;

use Marmot\Core;

trait TaskUtils
{
    private function compareArrayAndObjectTask(
        array $expectedArray,
        $object
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $object);
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($object->getId()));
        $this->assertEquals($expectedArray['pid'], $object->getPid());
        $this->assertEquals($expectedArray['total'], $object->getTotal());
        $this->assertEquals(
            $expectedArray['successNumber'],
            $object->getSuccessNumber()
        );
        $this->assertEquals(
            $expectedArray['failureNumber'],
            $object->getFailureNumber()
        );
        $this->assertEquals(
            $expectedArray['sourceCategory'],
            $object->getSourceCategory()
        );
        $this->assertEquals(
            $expectedArray['sourceTemplate'],
            $object->getSourceTemplate()
        );
        $this->assertEquals(
            $expectedArray['targetCategory'],
            $object->getTargetCategory()
        );
        $this->assertEquals(
            $expectedArray['targetTemplate'],
            $object->getTargetTemplate()
        );
        $this->assertEquals(
            $expectedArray['targetRule'],
            $object->getTargetRule()
        );
        $this->assertEquals(
            $expectedArray['fileName'],
            Core::$container->get('attachment.download.path').$object->getFileName()
        );
        $this->assertEquals(
            $expectedArray['crew']['id'],
            marmot_encode($object->getCrew()->getId())
        );
        $this->assertEquals(
            $expectedArray['crew']['name'],
            $object->getCrew()->getRealName()
        );
        $this->assertEquals(
            $expectedArray['userGroup']['id'],
            marmot_encode($object->getUserGroup()->getId())
        );
        $this->assertEquals(
            $expectedArray['userGroup']['name'],
            $object->getUserGroup()->getName()
        );
    }
}
