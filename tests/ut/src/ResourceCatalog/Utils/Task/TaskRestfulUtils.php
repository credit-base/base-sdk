<?php
namespace Base\Sdk\ResourceCatalog\Utils\Task;

trait TaskRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $object->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['pid'], $object->getPid());
        $this->assertEquals(
            $expectedArray['data']['attributes']['total'],
            $object->getTotal()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['successNumber'],
            $object->getSuccessNumber()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['failureNumber'],
            $object->getFailureNumber()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['sourceCategory'],
            $object->getSourceCategory()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['sourceTemplate'],
            $object->getSourceTemplate()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['targetCategory'],
            $object->getTargetCategory()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['targetTemplate'],
            $object->getTargetTemplate()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['targetRule'],
            $object->getTargetRule()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['fileName'],
            $object->getFileName()
        );
    }
}
