<?php
namespace Base\Sdk\ResourceCatalog\Utils\Task;

use Base\Sdk\ResourceCatalog\Model\Task\Task;

class MockFactory
{
    public static function generateTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Task {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $task = new Task($id);
        $task->setId($id);

        self::generatePid($task, $faker, $value);
        self::generateTotal($task, $faker, $value);
        self::generateSuccessNumber($task, $faker, $value);
        self::generateFailureNumber($task, $faker, $value);
        self::generateSourceCategory($task, $faker, $value);
        self::generateSourceTemplate($task, $faker, $value);
        self::generateTargetCategory($task, $faker, $value);
        self::generateTargetTemplate($task, $faker, $value);
        self::generateTargetRule($task, $faker, $value);
        self::generateCrew($task, $faker, $value);
        self::generateUserGroup($task, $faker, $value);
        self::generateFileName($task, $faker, $value);
        self::generateStatus($task, $faker, $value);

        $task->setCreateTime($faker->unixTime());
        $task->setUpdateTime($faker->unixTime());
        $task->setStatusTime($faker->unixTime());
        

        return $task;
    }

    private static function generatePid($task, $faker, $value) : void
    {
        $pid = isset($value['pid']) ?
        $value['pid'] :
        $faker->randomDigit();
        
        $task->setPid($pid);
    }

    private static function generateTotal($task, $faker, $value) : void
    {
        $total = isset($value['total']) ?
        $value['total'] :
        $faker->randomDigit();
        
        $task->setTotal($total);
    }

    protected static function generateUserGroup($task, $faker, $value) : void
    {
        $userGroup = isset($value['userGroup']) ?
        $value['userGroup'] :
        \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($faker->randomDigit());
        
        $task->setUserGroup($userGroup);
    }

    protected static function generateCrew($task, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Sdk\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $task->setCrew($crew);
    }

    private static function generateSuccessNumber($task, $faker, $value) : void
    {
        $successNumber = isset($value['successNumber']) ?
        $value['successNumber'] :
        $faker->randomDigit();
        
        $task->setSuccessNumber($successNumber);
    }

    private static function generateFailureNumber($task, $faker, $value) : void
    {
        $failureNumber = isset($value['failureNumber']) ?
        $value['failureNumber'] :
        $faker->randomDigit();
        
        $task->setFailureNumber($failureNumber);
    }

    private static function generateSourceCategory($task, $faker, $value) : void
    {
        $sourceCategory = isset($value['sourceCategory']) ?
        $value['sourceCategory'] :
        $faker->randomElement(
            TASK::CATEGORY
        );
        
        $task->setSourceCategory($sourceCategory);
    }

    private static function generateSourceTemplate($task, $faker, $value) : void
    {
        $sourceTemplate = isset($value['sourceTemplate']) ?
        $value['sourceTemplate'] :
        $faker->randomDigit();
        
        $task->setSourceTemplate($sourceTemplate);
    }

    private static function generateTargetCategory($task, $faker, $value) : void
    {
        $targetCategory = isset($value['targetCategory']) ?
        $value['targetCategory'] :
        $faker->randomElement(
            TASK::CATEGORY
        );
        
        $task->setTargetCategory($targetCategory);
    }

    private static function generateTargetTemplate($task, $faker, $value) : void
    {
        $targetTemplate = isset($value['targetTemplate']) ?
        $value['targetTemplate'] :
        $faker->randomDigit();
        
        $task->setTargetTemplate($targetTemplate);
    }

    private static function generateTargetRule($task, $faker, $value) : void
    {
        $targetRule = isset($value['targetRule']) ?
        $value['targetRule'] :
        $faker->randomDigit();
        
        $task->setTargetRule($targetRule);
    }

    private static function generateStatus($task, $faker, $value) : void
    {
        $status = isset($value['status']) ?
        $value['status'] :
        $faker->randomElement(
            TASK::STATUS
        );
        
        $task->setStatus($status);
    }

    private static function generateFileName($task, $faker, $value) : void
    {
        unset($faker);
        $fileName = isset($value['fileName']) ?
        $value['fileName'] :
        'FAILURE_1_10_HZXKXX_1_10_1_234551212.xlsx';
        
        $task->setFileName($fileName);
    }
}
