<?php
namespace Base\Sdk\ResourceCatalog\Utils\Attachment;

trait AttachmentRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $object->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['name'], $object->getName());
        $this->assertEquals(
            $expectedArray['data']['attributes']['hash'],
            $object->getHash()
        );
    }
}
