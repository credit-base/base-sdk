<?php
namespace Base\Sdk\ResourceCatalog\Utils\Attachment;

use Base\Sdk\ResourceCatalog\Model\Attachment;

class MockFactory
{
    public static function generateAttachment(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Attachment {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $attachment = new Attachment($id);
        $attachment->setId($id);

        self::generateName($attachment, $faker, $value);
        self::generateHash($attachment, $faker, $value);
        
        // $attachment->setCreateTime($faker->unixTime());

        return $attachment;
    }

    private static function generateName($attachment, $faker, $value) : void
    {
        $name = isset($value['name']) ?
        $value['name'] :
        $faker->word();
        
        $attachment->setName($name);
    }

    private static function generateHash($attachment, $faker, $value) : void
    {
        unset($faker);
        $hash = isset($value['hash']) ?
        $value['hash'] :
        '864660f58eff3ccc28c6d5d39ea7e131';
        
        $attachment->setHash($hash);
    }
}
