<?php
namespace Base\Sdk\ResourceCatalog\Utils\ErrorData;

trait ErrorDataRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $object->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['category'], $object->getCategory());
        $this->assertEquals(
            $expectedArray['data']['attributes']['itemsData'],
            $object->getItemsData()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['errorType'],
            $object->getErrorType()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['errorReason'],
            $object->getErrorReason()
        );
    }
}
