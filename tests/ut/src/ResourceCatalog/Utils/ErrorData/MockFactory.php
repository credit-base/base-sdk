<?php
namespace Base\Sdk\ResourceCatalog\Utils\ErrorData;

use Base\Sdk\ResourceCatalog\Model\ErrorData;

class MockFactory
{
    public static function generateErrorData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ErrorData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $errorData = new ErrorData($id);
        $errorData->setId($id);

        self::generateCategory($errorData, $faker, $value);
        self::generateTemplate($errorData, $faker, $value);
        self::generateItemsData($errorData, $faker, $value);
        self::generateErrorType($errorData, $faker, $value);
        self::generateErrorReason($errorData, $faker, $value);
        self::generateTask($errorData, $faker, $value);

        return $errorData;
    }

    private static function generateCategory($errorData, $faker, $value) : void
    {
        $category = isset($value['category']) ?
        $value['category'] :
        $faker->randomElement(
            ErrorData::CATEGORY
        );
        
        $errorData->setCategory($category);
    }

    private static function generateItemsData($errorData, $faker, $value) : void
    {
        unset($faker);
        $itemsData = isset($value['itemsData']) ?
        $value['itemsData'] :
        array(1,2);
        
        $errorData->setItemsData($itemsData);
    }

    private static function generateErrorType($errorData, $faker, $value) : void
    {
        unset($faker);
        $errorType = isset($value['errorType']) ?
        $value['errorType'] :
        1;
        
        $errorData->setErrorType($errorType);
    }

    private static function generateErrorReason($errorData, $faker, $value) : void
    {
        unset($faker);
        $errorReason = isset($value['errorReason']) ?
        $value['errorReason'] :
        array(1,2);
        
        $errorData->setErrorReason($errorReason);
    }

    private static function generateTemplate($errorData, $faker, $value) : void
    {
        $template = isset($value['template']) ?
        $value['template'] :
        \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate($faker->randomDigit());
        
        $errorData->setTemplate($template);
    }

    private static function generateTask($errorData, $faker, $value) : void
    {
        $task = isset($value['task']) ?
        $value['task'] :
        \Base\Sdk\ResourceCatalog\Utils\Task\MockFactory::generateTask($faker->randomDigit());
        
        $errorData->setTask($task);
    }
}
