<?php
namespace Base\Sdk\ResourceCatalog\Utils\ErrorData;

use Base\Sdk\ResourceCatalog\Translator\ErrorDataTranslator;
use Base\Sdk\ResourceCatalog\Translator\TaskTranslator;

use Base\Sdk\Rule\Translator\TemplateTranslatorTrait;

trait ErrorDataUtils
{
    use TemplateTranslatorTrait;

    protected function getTaskTranslator():TaskTranslator
    {
        return new TaskTranslator();
    }
    
    private function compareArrayAndObjectErrorData(
        array $expectedArray,
        $object
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $object);
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($object->getId()));
        $this->assertEquals($expectedArray['category'], $object->getCategory());
        $this->assertEquals($expectedArray['itemsData'], $object->getItemsData());
        $this->assertEquals(
            $expectedArray['errorType'],
            $object->getErrorType()
        );
        $this->assertEquals(
            $expectedArray['errorReason'],
            $object->getErrorReason()
        );
        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($object->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            ErrorDataTranslator::STATUS_CN[$object->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            ErrorDataTranslator::STATUS_TYPE[$object->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['taskData'],
            $this->getTaskTranslator()->objectToArray(
                $object->getTask()
            )
        );
        $this->assertEquals(
            $expectedArray['template'],
            $this->getTemplateTranslatorByCategory($object->getCategory())->objectToArray(
                $object->getTemplate()
            )
        );
    }
}
