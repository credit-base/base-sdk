<?php
namespace Base\Sdk\ResourceCatalog\Utils\BjSearchData;

use Base\Sdk\ResourceCatalog\Translator\ItemsDataTranslator;

use Base\Sdk\Template\Translator\BjTemplateTranslator;
use Base\Sdk\ResourceCatalog\Model\ISearchData;

trait BjSearchDataUtils
{
    protected function getBjTemplateTranslator():BjTemplateTranslator
    {
        return new BjTemplateTranslator();
    }

    protected function getItemsDataTranslator() : ItemsDataTranslator
    {
        return new ItemsDataTranslator();
    }
    
    private function compareArrayAndObjectBjSearchData(
        array $expectedArray,
        $bjSearchData
    ) {
        $this->compareArrayAndObjectCommon($expectedArray, $bjSearchData);
    }

    /**
     *
     * @SuppressWarnings(PHPMD)
     */
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $bjSearchData
    ) {
        $this->assertEquals($expectedArray['id'], marmot_encode($bjSearchData->getId()));
        $this->assertEquals(
            $expectedArray['infoClassify']['id'],
            marmot_encode($bjSearchData->getInfoClassify())
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['name'],
            ISearchData::INFO_CLASSIFY_CN[$bjSearchData->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['label'],
            ISearchData::INFO_CLASSIFY_LABEL[$bjSearchData->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoClassify']['type'],
            ISearchData::INFO_CLASSIFY_TYPE[$bjSearchData->getInfoClassify()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['id'],
            marmot_encode($bjSearchData->getInfoCategory())
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['name'],
            ISearchData::INFO_CATEGORY_CN[$bjSearchData->getInfoCategory()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['label'],
            ISearchData::INFO_CATEGORY_LABEL[$bjSearchData->getInfoCategory()]
        );
        $this->assertEquals(
            $expectedArray['infoCategory']['type'],
            ISearchData::INFO_CATEGORY_TYPE[$bjSearchData->getInfoCategory()]
        );
        $this->assertEquals(
            $expectedArray['subjectCategory']['id'],
            'MQ'
        );
        $this->assertEquals(
            $expectedArray['subjectCategory']['name'],
            '自然人'
        );
        $this->assertEquals(
            $expectedArray['dimension']['id'],
            marmot_encode($bjSearchData->getDimension())
        );
        $this->assertEquals(
            $expectedArray['dimension']['name'],
            ISearchData::DIMENSION_CN[$bjSearchData->getDimension()]
        );
        $this->assertEquals(
            $expectedArray['dimension']['label'],
            ISearchData::DIMENSION_LABEL[$bjSearchData->getDimension()]
        );
        $this->assertEquals(
            $expectedArray['dimension']['type'],
            ISearchData::DIMENSION_TYPE[$bjSearchData->getDimension()]
        );
        $this->assertEquals(
            $expectedArray['name'],
            $bjSearchData->getName()
        );
        $this->assertEquals(
            $expectedArray['identify'],
            $bjSearchData->getIdentify()
        );
        $this->assertEquals(
            $expectedArray['expirationDate'],
            $bjSearchData->getExpirationDate()
        );
        $this->assertEquals(
            $expectedArray['expirationDateFormat'],
            date(
                'Y-m-d',
                $bjSearchData->getExpirationDate()
            )
        );
        $this->assertEquals(
            $expectedArray['status']['id'],
            marmot_encode($bjSearchData->getStatus())
        );
        $this->assertEquals(
            $expectedArray['status']['name'],
            ISearchData::DATA_STATUS_CN[$bjSearchData->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['status']['type'],
            ISearchData::DATA_STATUS_TYPE[$bjSearchData->getStatus()]
        );
        $this->assertEquals(
            $expectedArray['crew']['id'],
            marmot_encode($bjSearchData->getCrew()->getId())
        );
        $this->assertEquals(
            $expectedArray['crew']['name'],
            $bjSearchData->getCrew()->getRealName()
        );
        $this->assertEquals(
            $expectedArray['sourceUnit']['id'],
            marmot_encode($bjSearchData->getSourceUnit()->getId())
        );
        $this->assertEquals(
            $expectedArray['sourceUnit']['name'],
            $bjSearchData->getSourceUnit()->getName()
        );
        $this->assertEquals(
            $expectedArray['itemsData'],
            $this->getItemsDataTranslator()->objectToArray(
                $bjSearchData->getItemsData()
            )
        );
        $this->assertEquals(
            $expectedArray['template'],
            $this->getBjTemplateTranslator()->objectToArray(
                $bjSearchData->getTemplate()
            )
        );
    }
}
