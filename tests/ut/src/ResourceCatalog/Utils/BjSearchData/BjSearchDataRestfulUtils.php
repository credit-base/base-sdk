<?php
namespace Base\Sdk\ResourceCatalog\Utils\BjSearchData;

trait BjSearchDataRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $object
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $object->getId());
        }
        
        $this->assertEquals($expectedArray['data']['attributes']['infoClassify'], $object->getInfoClassify());
        $this->assertEquals(
            $expectedArray['data']['attributes']['infoCategory'],
            $object->getInfoCategory()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $object->getSubjectCategory()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['dimension'],
            $object->getDimension()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['name'],
            $object->getName()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['identify'],
            $object->getIdentify()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['expirationDate'],
            $object->getExpirationDate()
        );
    }
}
