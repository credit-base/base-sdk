<?php
namespace Base\Sdk\ResourceCatalog\Utils\BjSearchData;

use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use Base\Sdk\ResourceCatalog\Utils\SearchDataMockFactoryTrait;

class BjSearchDataMockFactory
{
    use SearchDataMockFactoryTrait;
    
    public static function generateBjSearchData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BjSearchData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $bjSearchData = new BjSearchData($id);
        $bjSearchData->setId($id);

        self::generateInfoClassify($bjSearchData, $faker, $value);
        self::generateInfoCategory($bjSearchData, $faker, $value);
        self::generateCrew($bjSearchData, $faker, $value);
        self::generateSourceUnit($bjSearchData, $faker, $value);
        self::generateSubjectCategory($bjSearchData, $faker, $value);
        self::generateDimension($bjSearchData, $faker, $value);
        self::generateName($bjSearchData, $faker, $value);
        self::generateIdentify($bjSearchData, $faker, $value);
        self::generateExpirationDate($bjSearchData, $faker, $value);
        self::generateStatus($bjSearchData, $faker, $value);
        self::generateTemplate($bjSearchData, $faker, $value);
        self::generateItemsData($bjSearchData, $faker, $value);
        
        $bjSearchData->setCreateTime($faker->unixTime());
        $bjSearchData->setUpdateTime($faker->unixTime());
        $bjSearchData->setStatusTime($faker->unixTime());

        return $bjSearchData;
    }

    private static function generateTemplate($bjSearchData, $faker, $value) : void
    {
        $template = isset($value['template']) ?
        $value['template'] :
        \Base\Sdk\Template\Utils\BjTemplate\MockBjTemplateFactory::generateBjTemplate($faker->randomDigit());
        
        $bjSearchData->setTemplate($template);
    }

    private static function generateItemsData($bjSearchData, $faker, $value) : void
    {
        $itemsData = isset($value['itemsData']) ?
        $value['itemsData'] :
        \Base\Sdk\ResourceCatalog\Utils\ItemsDataMockFactory::generateItemsData($faker->randomDigit());
        
        $bjSearchData->setItemsData($itemsData);
    }

    protected static function generateStatus($bjSearchData, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                BjSearchData::STATUS
            );
        
        $bjSearchData->setStatus($status);
    }
}
