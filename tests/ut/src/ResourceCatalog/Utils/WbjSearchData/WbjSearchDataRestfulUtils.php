<?php
namespace Base\Sdk\ResourceCatalog\Utils\WbjSearchData;

trait WbjSearchDataRestfulUtils
{
    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $wbjSearchData
    ) {
        if (isset($expectedArray['data']['id'])) {
            $this->assertEquals($expectedArray['data']['id'], $wbjSearchData->getId());
        }
        
        $this->assertEquals(
            $expectedArray['data']['attributes']['subjectCategory'],
            $wbjSearchData->getSubjectCategory()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['dimension'],
            $wbjSearchData->getDimension()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['infoClassify'],
            $wbjSearchData->getInfoClassify()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['infoCategory'],
            $wbjSearchData->getInfoCategory()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['expirationDate'],
            $wbjSearchData->getExpirationDate()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['name'],
            $wbjSearchData->getName()
        );
        $this->assertEquals(
            $expectedArray['data']['attributes']['identify'],
            $wbjSearchData->getIdentify()
        );
    }
}
