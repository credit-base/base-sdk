<?php
namespace Base\Sdk\ResourceCatalog\Utils;

use Base\Sdk\ResourceCatalog\Model\SearchData;
use Base\Sdk\ResourceCatalog\Model\ISearchData;

trait SearchDataMockFactoryTrait
{
    protected static function generateInfoClassify(SearchData $searchData, $faker, $value) : void
    {
        $infoClassify = isset($value['infoClassify']) ?
            $value['infoClassify'] :
            $faker->randomElement(
                ISearchData::INFO_CLASSIFY
            );
        
        $searchData->setInfoClassify($infoClassify);
    }

    protected static function generateInfoCategory(SearchData $searchData, $faker, $value) : void
    {
        $infoCategory = isset($value['infoCategory']) ?
            $value['infoCategory'] :
            $faker->randomElement(
                ISearchData::INFO_CATEGORY
            );
        
        $searchData->setInfoCategory($infoCategory);
    }

    protected static function generateCrew(SearchData $searchData, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \Base\Sdk\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $searchData->setCrew($crew);
    }

    protected static function generateSourceUnit(SearchData $searchData, $faker, $value) : void
    {
        $sourceUnit = isset($value['sourceUnit']) ?
        $value['sourceUnit'] :
        \Base\Sdk\UserGroup\Utils\MockUserGroupFactory::generateUserGroup($faker->randomDigit());
        
        $searchData->setSourceUnit($sourceUnit);
    }

    protected static function generateSubjectCategory(SearchData $searchData, $faker, $value) : void
    {
        unset($faker);
        $subjectCategory = isset($value['subjectCategory']) ?
            $value['subjectCategory'] :
            ISearchData::SUBJECT_CATEGORY['ZRR'];
        
        $searchData->setSubjectCategory($subjectCategory);
    }

    protected static function generateDimension(SearchData $searchData, $faker, $value) : void
    {
        $dimension = isset($value['dimension']) ?
            $value['dimension'] :
            $faker->randomElement(
                ISearchData::DIMENSION
            );
        
        $searchData->setDimension($dimension);
    }

    protected static function generateName(SearchData $searchData, $faker, $value) : void
    {
        $name = isset($value['name']) ?
            $value['name'] :
            $faker->name;
        
        $searchData->setName($name);
    }

    protected static function generateIdentify(SearchData $searchData, $faker, $value) : void
    {
        $identify = isset($value['identify']) ?
            $value['identify'] :
            $faker->creditCardNumber();
        
        $searchData->setIdentify($identify);
    }

    protected static function generateExpirationDate(SearchData $searchData, $faker, $value) : void
    {
        $expirationDate = isset($value['expirationDate']) ?
            $value['expirationDate'] :
            $faker->unixTime();
        
        $searchData->setExpirationDate($expirationDate);
    }
}
