<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository;
use Base\Sdk\ResourceCatalog\Command\GbSearchData\ConfirmGbSearchDataCommand;

class ConfirmGbSearchDataCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(MockConfirmGbSearchDataCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockConfirmGbSearchDataCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository',
            $commandHandler->getPublicRepository()
        );
    }

    public function testExecute()
    {
        $id = 1;
        
        $command = new ConfirmGbSearchDataCommand(
            $id
        );

        $gbSearchData = $this->prophesize(GbSearchData::class);
    
        $gbSearchData->confirm()->shouldBeCalledTimes(1)->willReturn(true);

        $gbSearchDataRepository = $this->prophesize(GbSearchDataRepository::class);
        $gbSearchDataRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($gbSearchData->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($gbSearchDataRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
