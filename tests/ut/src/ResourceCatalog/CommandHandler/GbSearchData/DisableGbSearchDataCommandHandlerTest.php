<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository;

class DisableGbSearchDataCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockDisableGbSearchDataCommandHandler();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository',
            $this->stub->getPublicRepository()
        );
    }

    public function testFetchIEnableObject()
    {
        $commandHandler = $this->getMockBuilder(MockDisableGbSearchDataCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $gbSearchData = \Base\Sdk\ResourceCatalog\Utils\GbSearchData\GbSearchDataMockFactory::generateGbSearchData($id);

        $repository = $this->prophesize(GbSearchDataRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($gbSearchData);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIEnableObject($id);

        $this->assertEquals($result, $gbSearchData);
    }
}
