<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository;

class DeleteGbSearchDataCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDeleteGbSearchDataCommandHandler::class)
            ->setMethods(['fetchGbSearchData'])
            ->getMock();
    }

    public function testExtendsDeleteCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\DeleteCommandHandler',
            $this->stub
        );
    }

    public function testFetchIModifyStatusObject()
    {
        $commandHandler = $this->getMockBuilder(MockDeleteGbSearchDataCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $gbSearchData = \Base\Sdk\ResourceCatalog\Utils\GbSearchData\GbSearchDataMockFactory::generateGbSearchData($id);

        $repository = $this->prophesize(GbSearchDataRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($gbSearchData);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIModifyStatusObject($id);

        $this->assertEquals($result, $gbSearchData);
    }
}
