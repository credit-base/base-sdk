<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\WbjSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Repository\WbjSearchDataRepository;

class DisableWbjSearchDataCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockDisableWbjSearchDataCommandHandler();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\WbjSearchDataRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchIEnableObject()
    {
        $commandHandler = $this->getMockBuilder(MockDisableWbjSearchDataCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $wbjSearchData =
            \Base\Sdk\ResourceCatalog\Utils\WbjSearchData\WbjSearchDataMockFactory::generateWbjSearchData($id);

        $repository = $this->prophesize(WbjSearchDataRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($wbjSearchData);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIEnableObject($id);

        $this->assertEquals($result, $wbjSearchData);
    }
}
