<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

class DisableBjSearchDataCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockDisableBjSearchDataCommandHandler();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository',
            $this->stub->getPublicRepository()
        );
    }

    public function testFetchIEnableObject()
    {
        $commandHandler = $this->getMockBuilder(MockDisableBjSearchDataCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $bjSearchData = \Base\Sdk\ResourceCatalog\Utils\BjSearchData\BjSearchDataMockFactory::generateBjSearchData($id);

        $repository = $this->prophesize(BjSearchDataRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($bjSearchData);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIEnableObject($id);

        $this->assertEquals($result, $bjSearchData);
    }
}
