<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository;
use Base\Sdk\ResourceCatalog\Command\BjSearchData\ConfirmBjSearchDataCommand;

class ConfirmBjSearchDataCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(MockConfirmBjSearchDataCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetRepository()
    {
        $commandHandler = new MockConfirmBjSearchDataCommandHandler();
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository',
            $commandHandler->getPublicRepository()
        );
    }

    public function testExecute()
    {
        $id = 1;
        
        $command = new ConfirmBjSearchDataCommand(
            $id
        );

        $bjSearchData = $this->prophesize(BjSearchData::class);
    
        $bjSearchData->confirm()->shouldBeCalledTimes(1)->willReturn(true);

        $bjSearchDataRepository = $this->prophesize(BjSearchDataRepository::class);
        $bjSearchDataRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($bjSearchData->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getRepository')
            ->willReturn($bjSearchDataRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}
