<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

class DeleteBjSearchDataCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDeleteBjSearchDataCommandHandler::class)
            ->setMethods(['fetchBjSearchData'])
            ->getMock();
    }

    public function testExtendsDeleteCommandHandler()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Common\CommandHandler\DeleteCommandHandler',
            $this->stub
        );
    }

    public function testFetchIModifyStatusObject()
    {
        $commandHandler = $this->getMockBuilder(MockDeleteBjSearchDataCommandHandler::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = 1;
        $bjSearchData = \Base\Sdk\ResourceCatalog\Utils\BjSearchData\BjSearchDataMockFactory::generateBjSearchData($id);

        $repository = $this->prophesize(BjSearchDataRepository::class);

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($bjSearchData);

        $commandHandler->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $commandHandler->fetchIModifyStatusObject($id);

        $this->assertEquals($result, $bjSearchData);
    }
}
