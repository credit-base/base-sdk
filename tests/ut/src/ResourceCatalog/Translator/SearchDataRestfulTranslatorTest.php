<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Utils\GbSearchData\GbSearchDataRestfulUtils;
use Base\Sdk\ResourceCatalog\Model\GbSearchData;

class SearchDataRestfulTranslatorTest extends TestCase
{
    use GbSearchDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder('Base\Sdk\ResourceCatalog\Translator\SearchDataRestfulTranslator')
        ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $searchData = \Base\Sdk\ResourceCatalog\Utils\WbjSearchData\WbjSearchDataMockFactory::generateWbjSearchData(1);
    
        $expression['data']['id'] = $searchData->getId();
        $expression['data']['attributes']['infoClassify'] = $searchData->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $searchData->getInfoCategory();
        $expression['data']['attributes']['subjectCategory'] = $searchData->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $searchData->getDimension();
        $expression['data']['attributes']['name'] = $searchData->getName();
        $expression['data']['attributes']['identify'] = $searchData->getIdentify();
        $expression['data']['attributes']['expirationDate'] = $searchData->getExpirationDate();

        $expression['data']['attributes']['createTime'] = $searchData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $searchData->getUpdateTime();
        $expression['data']['attributes']['status'] = $searchData->getStatus();
        $expression['data']['attributes']['statusTime'] = $searchData->getStatusTime();
        
        $searchDataObject = $this->translator->arrayToObject($expression, $searchData);
        unset($searchDataObject);
        $this->compareArrayAndObjectCommon($expression, $searchData);
    }

    public function testObjectToArray()
    {
        $searchData = null;

        $expression = $this->translator->objectToArray($searchData);
        $this->assertEquals(array(), $expression);
    }
}
