<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\ResourceCatalog\Utils\BjSearchData\BjSearchDataRestfulUtils;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

use Base\Sdk\ResourceCatalog\Model\itemsData;
use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\Template\Translator\BjTemplateRestfulTranslator;
use Base\Sdk\ResourceCatalog\Translator\ItemsDataRestfulTranslator;

class BjSearchDataRestfulTranslatorTest extends TestCase
{
    use BjSearchDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BjSearchDataRestfulTranslator();

        Core::$container->set('crew', new Crew());
        $this->childTranslator = new class extends BjSearchDataRestfulTranslator
        {
            public function getItemsDataRestfulTranslator() : ItemsDataRestfulTranslator
            {
                return parent::getItemsDataRestfulTranslator();
            }

            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }

            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }

            public function getBjTemplateRestfulTranslator() : BjTemplateRestfulTranslator
            {
                return parent::getBjTemplateRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testGetItemsDataRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Translator\ItemsDataRestfulTranslator',
            $this->childTranslator->getItemsDataRestfulTranslator()
        );
    }

    public function testGetBjTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Translator\BjTemplateRestfulTranslator',
            $this->childTranslator->getBjTemplateRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $bjSearchData = \Base\Sdk\ResourceCatalog\Utils\BjSearchData\BjSearchDataMockFactory::generateBjSearchData(1);

        $expression['data']['id'] = $bjSearchData->getId();
        $expression['data']['attributes']['infoClassify'] = $bjSearchData->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $bjSearchData->getInfoCategory();
        $expression['data']['attributes']['subjectCategory'] = $bjSearchData->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $bjSearchData->getDimension();
        $expression['data']['attributes']['name'] = $bjSearchData->getName();
        $expression['data']['attributes']['identify'] = $bjSearchData->getIdentify();
        $expression['data']['attributes']['expirationDate'] = $bjSearchData->getExpirationDate();

        $expression['data']['attributes']['createTime'] = $bjSearchData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $bjSearchData->getUpdateTime();
        $expression['data']['attributes']['status'] = $bjSearchData->getStatus();
        $expression['data']['attributes']['statusTime'] = $bjSearchData->getStatusTime();

        $bjSearchDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\BjSearchData', $bjSearchDataObject);
        $this->compareArrayAndObjectCommon($expression, $bjSearchDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $bjSearchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\NullBjSearchData', $bjSearchData);
    }

    public function testObjectToArray()
    {
        $bjSearchData = null;

        $expression = $this->translator->objectToArray($bjSearchData);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['crew']['data'] 、
     * $relationships['userGroup']['data']、$relationships['sourceTemplate']['data']、
     * $relationships['transformationTemplate']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用4次, 分别用于处理 userGroup 、crew、sourceTemplate、transformationTemplate
     * 且返回各自的数组 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用一次, 入参是 $crewInfo, 出参是 $crew
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $userGroupInfo, 出参是 $userGroup
     * 6. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次, 入参是 $sourceTemplateInfo, 出参是 $sourceTemplate
     * 7. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次,
     * 入参是 $transformationTemplateInfo, 出参是 $transformationTemplate
     * 8. $BjSearchData->setCrew 调用一次, 入参 $crew
     * 9. $BjSearchData->setUserGroup 调用一次, 入参 $userGroup
     * 10. $BjSearchData->setSourceTemplate 调用一次, 入参 $sourceTemplate
     * 11. $BjSearchData->setTransformationTemplate 调用一次, 入参 $transformationTemplate
     */
    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1,
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'sourceUnit'=>['data'=>'mockSourceUnit'],
            'itemsData'=>['data'=>'mockItemsData'],
            'template'=>['data'=>'mockTemplate'],
        ];

        $translator = $this->getMockBuilder(BjSearchDataRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getCrewRestfulTranslator',
             'getUserGroupRestfulTranslator',
             'getItemsDataRestfulTranslator',
             'getBjTemplateRestfulTranslator',
         ])
        ->getMock();

        $crewInfo = ['mockCrewInfo'];
        $sourceUnitInfo = ['mockSourceUnitInfo'];
        $templateInfo = ['mockTemplateInfo'];
        $itemsDataInfo = ['mockItemsDataInfo'];

        $crew = new Crew();
        $sourceUnit = new UserGroup();
        $template = new BjTemplate();
        $itemsData = new ItemsData();

        //预言
        $translator->expects($this->exactly(2))
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用4次, 依次入参 crew, userGroup,sourceTemplate, transformationTemplate
        //依次返回 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
        $translator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['sourceUnit']['data']],
                 [$relationships['itemsData']['data']],
                 [$relationships['template']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $sourceUnitInfo,
                $itemsDataInfo,
                $templateInfo
            ));
  
        
   
        $itemsDataRestfulTranslator = $this->prophesize(ItemsDataRestfulTranslator::class);
        $itemsDataRestfulTranslator->arrayToObject(Argument::exact($itemsDataInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($itemsData);

        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($sourceUnitInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceUnit);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);
        
        $bjTemplateRestfulTranslator = $this->prophesize(BjTemplateRestfulTranslator::class);
        $bjTemplateRestfulTranslator->arrayToObject(Argument::exact($templateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($template);

        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());

        $translator->expects($this->exactly(1))
            ->method('getBjTemplateRestfulTranslator')
            ->willReturn($bjTemplateRestfulTranslator->reveal());

        $translator->expects($this->exactly(1))
            ->method('getItemsDataRestfulTranslator')
            ->willReturn($itemsDataRestfulTranslator->reveal());

        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        //揭示
        $BjSearchData = $translator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\BjSearchData', $BjSearchData);
        $this->assertEquals($crew, $BjSearchData->getCrew());
        $this->assertEquals($sourceUnit, $BjSearchData->getSourceUnit());
        $this->assertEquals($itemsData, $BjSearchData->getItemsData());
        $this->assertEquals($template, $BjSearchData->getTemplate());
    }
}
