<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Utils\BjSearchData\BjSearchDataUtils;

class BjSearchDataTranslatorTest extends TestCase
{
    use BjSearchDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BjSearchDataTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\NullBjSearchData', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $bjSearchData =
            \Base\Sdk\ResourceCatalog\Utils\BjSearchData\BjSearchDataMockFactory::generateBjSearchData(1);
       
        $expression = $this->translator->objectToArray($bjSearchData);
       
        $this->compareArrayAndObjectBjSearchData($expression, $bjSearchData);
    }

    public function testObjectToArrayFail()
    {
        $bjSearchData = null;

        $expression = $this->translator->objectToArray($bjSearchData);
        $this->assertEquals(array(), $expression);
    }

    public function testGetBaseTemplateSubjectCategoryCn()
    {
        $data = array(
            'id'=> 'MA',
            'name'=> '法人及非法人组织',
        );
        $subjectCategory = 1;
        $result = $this->translator->getSubjectCategoryToArray($subjectCategory);
        
        $this->assertEquals($data, $result);
    }
}
