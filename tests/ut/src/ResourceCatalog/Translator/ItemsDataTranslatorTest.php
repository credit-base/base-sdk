<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Utils\ItemsDataRestfulUtils;

class ItemsDataTranslatorTest extends TestCase
{
    use ItemsDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ItemsDataTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\NullItemsData', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $itemsData = \Base\Sdk\ResourceCatalog\Utils\ItemsDataMockFactory::generateItemsData(1);

        $expression = $this->translator->objectToArray($itemsData);
    
        $this->compareObjectToArrayCommon($expression, $itemsData);
    }

    public function testObjectToArrayFail()
    {
        $itemsData = null;

        $expression = $this->translator->objectToArray($itemsData);
        $this->assertEquals(array(), $expression);
    }
}
