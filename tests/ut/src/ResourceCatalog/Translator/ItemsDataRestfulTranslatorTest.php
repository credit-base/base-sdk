<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Utils\ItemsDataRestfulUtils;

use Base\Sdk\Crew\Model\Crew;

class ItemsDataRestfulTranslatorTest extends TestCase
{
    use ItemsDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ItemsDataRestfulTranslator();

        Core::$container->set('crew', new Crew());
        $this->childTranslator = new class extends ItemsDataRestfulTranslator
        {
          
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $itemsData = \Base\Sdk\ResourceCatalog\Utils\ItemsDataMockFactory::generateItemsData(1);

        $expression['data']['id'] = $itemsData->getId();
        $expression['data']['attributes']['data'] = $itemsData->getData();

        $itemsDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\ItemsData', $itemsDataObject);
        $this->compareArrayAndObjectCommon($expression, $itemsDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $itemsData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\NullItemsData', $itemsData);
    }

    public function testObjectToArray()
    {
        $itemsData = null;

        $expression = $this->translator->objectToArray($itemsData);
        $this->assertEquals(array(), $expression);
    }
}
