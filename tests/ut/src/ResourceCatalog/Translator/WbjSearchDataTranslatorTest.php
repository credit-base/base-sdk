<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Utils\WbjSearchData\WbjSearchDataUtils;

class WbjSearchDataTranslatorTest extends TestCase
{
    use WbjSearchDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WbjSearchDataTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\NullWbjSearchData', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $wbjSearchData =
            \Base\Sdk\ResourceCatalog\Utils\WbjSearchData\WbjSearchDataMockFactory::generateWbjSearchData(1);

        $expression = $this->translator->objectToArray($wbjSearchData);
    
        $this->compareArrayAndObjectWbjSearchData($expression, $wbjSearchData);
    }

    public function testObjectToArrayFail()
    {
        $wbjSearchData = null;

        $expression = $this->translator->objectToArray($wbjSearchData);
        $this->assertEquals(array(), $expression);
    }
}
