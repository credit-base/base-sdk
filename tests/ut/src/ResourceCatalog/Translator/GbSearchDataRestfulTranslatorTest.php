<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\ResourceCatalog\Utils\GbSearchData\GbSearchDataRestfulUtils;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

use Base\Sdk\ResourceCatalog\Model\itemsData;
use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\Translator\WbjTemplateRestfulTranslator;
use Base\Sdk\Template\Translator\GbTemplateRestfulTranslator;
use Base\Sdk\ResourceCatalog\Translator\ItemsDataRestfulTranslator;

class GbSearchDataRestfulTranslatorTest extends TestCase
{
    use GbSearchDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new GbSearchDataRestfulTranslator();

        Core::$container->set('crew', new Crew());
        $this->childTranslator = new class extends GbSearchDataRestfulTranslator
        {
            public function getItemsDataRestfulTranslator() : ItemsDataRestfulTranslator
            {
                return parent::getItemsDataRestfulTranslator();
            }

            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }

            public function getGbTemplateRestfulTranslator() : GbTemplateRestfulTranslator
            {
                return parent::getGbTemplateRestfulTranslator();
            }

            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testGetItemsDataRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Translator\ItemsDataRestfulTranslator',
            $this->childTranslator->getItemsDataRestfulTranslator()
        );
    }

    public function testGetGbTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Translator\GbTemplateRestfulTranslator',
            $this->childTranslator->getGbTemplateRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $gbSearchData = \Base\Sdk\ResourceCatalog\Utils\GbSearchData\GbSearchDataMockFactory::generateGbSearchData(1);

        $expression['data']['id'] = $gbSearchData->getId();
        $expression['data']['attributes']['infoClassify'] = $gbSearchData->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $gbSearchData->getInfoCategory();
        $expression['data']['attributes']['subjectCategory'] = $gbSearchData->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $gbSearchData->getDimension();
        $expression['data']['attributes']['name'] = $gbSearchData->getName();
        $expression['data']['attributes']['identify'] = $gbSearchData->getIdentify();
        $expression['data']['attributes']['expirationDate'] = $gbSearchData->getExpirationDate();

        $expression['data']['attributes']['createTime'] = $gbSearchData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $gbSearchData->getUpdateTime();
        $expression['data']['attributes']['status'] = $gbSearchData->getStatus();
        $expression['data']['attributes']['statusTime'] = $gbSearchData->getStatusTime();

        $gbSearchDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\GbSearchData', $gbSearchDataObject);
        $this->compareArrayAndObjectCommon($expression, $gbSearchDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $gbSearchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\NullGbSearchData', $gbSearchData);
    }

    public function testObjectToArray()
    {
        $gbSearchData = null;

        $expression = $this->translator->objectToArray($gbSearchData);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['crew']['data'] 、
     * $relationships['userGroup']['data']、$relationships['sourceTemplate']['data']、
     * $relationships['transformationTemplate']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用4次, 分别用于处理 userGroup 、crew、sourceTemplate、transformationTemplate
     * 且返回各自的数组 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用一次, 入参是 $crewInfo, 出参是 $crew
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $userGroupInfo, 出参是 $userGroup
     * 6. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次, 入参是 $sourceTemplateInfo, 出参是 $sourceTemplate
     * 7. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次,
     * 入参是 $transformationTemplateInfo, 出参是 $transformationTemplate
     * 8. $GbSearchData->setCrew 调用一次, 入参 $crew
     * 9. $GbSearchData->setUserGroup 调用一次, 入参 $userGroup
     * 10. $GbSearchData->setSourceTemplate 调用一次, 入参 $sourceTemplate
     * 11. $GbSearchData->setTransformationTemplate 调用一次, 入参 $transformationTemplate
     */
    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1,
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'sourceUnit'=>['data'=>'mockSourceUnit'],
            'itemsData'=>['data'=>'mockItemsData'],
            'template'=>['data'=>'mockTemplate'],
        ];

        $gbTranslator = $this->getMockBuilder(GbSearchDataRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getCrewRestfulTranslator',
             'getUserGroupRestfulTranslator',
             'getItemsDataRestfulTranslator',
             'getGbTemplateRestfulTranslator',
         ])
        ->getMock();

        $crewInfo = ['mockCrewInfo'];
        $sourceUnitInfo = ['mockSourceUnitInfo'];
        $templateInfo = ['mockTemplateInfo'];
        $itemsDataInfo = ['mockItemsDataInfo'];

        $template = new GbTemplate();
        $itemsData = new ItemsData();
        $crew = new Crew();
        $sourceUnit = new UserGroup();
       
        //预言
        $gbTranslator->expects($this->exactly(2))
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用4次, 依次入参 crew, userGroup,sourceTemplate, transformationTemplate
        //依次返回 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
        $gbTranslator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['sourceUnit']['data']],
                 [$relationships['itemsData']['data']],
                 [$relationships['template']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $sourceUnitInfo,
                $itemsDataInfo,
                $templateInfo
            ));

        
        $itemsDataRestfulTranslator = $this->prophesize(ItemsDataRestfulTranslator::class);
        $gbTemplateRestfulTranslator = $this->prophesize(GbTemplateRestfulTranslator::class);
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);

        $gbTemplateRestfulTranslator->arrayToObject(Argument::exact($templateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($template);

        $userGroupRestfulTranslator->arrayToObject(Argument::exact($sourceUnitInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceUnit);

        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);

        $itemsDataRestfulTranslator->arrayToObject(Argument::exact($itemsDataInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($itemsData);

        //绑定
        $gbTranslator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());

        $gbTranslator->expects($this->exactly(1))
            ->method('getGbTemplateRestfulTranslator')
            ->willReturn($gbTemplateRestfulTranslator->reveal());

        $gbTranslator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $gbTranslator->expects($this->exactly(1))
            ->method('getItemsDataRestfulTranslator')
            ->willReturn($itemsDataRestfulTranslator->reveal());

        //揭示
        $gbSearchData = $gbTranslator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\GbSearchData', $gbSearchData);
        $this->assertEquals($crew, $gbSearchData->getCrew());
        $this->assertEquals($sourceUnit, $gbSearchData->getSourceUnit());
        $this->assertEquals($itemsData, $gbSearchData->getItemsData());
        $this->assertEquals($template, $gbSearchData->getTemplate());
    }
}
