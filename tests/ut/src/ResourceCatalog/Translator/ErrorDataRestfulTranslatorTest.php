<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Utils\ErrorData\ErrorDataRestfulUtils;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\ResourceCatalog\Model\Task\Task;
use Base\Sdk\ResourceCatalog\Translator\TaskRestfulTranslator;
use Base\Sdk\Template\Translator\WbjTemplateRestfulTranslator;

class ErrorDataRestfulTranslatorTest extends TestCase
{
    use ErrorDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ErrorDataRestfulTranslator();

        Core::$container->set('crew', new Crew());
        $this->childTranslator = new class extends ErrorDataRestfulTranslator
        {
            public function getTaskRestfulTranslator() : TaskRestfulTranslator
            {
                return parent::getTaskRestfulTranslator();
            }
        };
    }

    public function testGetTaskRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Translator\TaskRestfulTranslator',
            $this->childTranslator->getTaskRestfulTranslator()
        );
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $errorData = \Base\Sdk\ResourceCatalog\Utils\ErrorData\MockFactory::generateErrorData(1);

        $expression['data']['id'] = $errorData->getId();
        $expression['data']['attributes']['category'] = $errorData->getCategory();
        $expression['data']['attributes']['itemsData'] = $errorData->getItemsData();
        $expression['data']['attributes']['errorType'] = $errorData->getErrorType();
        $expression['data']['attributes']['errorReason'] = $errorData->getErrorReason();

        $expression['data']['attributes']['createTime'] = $errorData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $errorData->getUpdateTime();
        $expression['data']['attributes']['status'] = $errorData->getStatus();
        $expression['data']['attributes']['statusTime'] = $errorData->getStatusTime();

        $errorDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\ErrorData', $errorDataObject);
        $this->compareArrayAndObjectCommon($expression, $errorDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $errorData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\NullErrorData', $errorData);
    }

    public function testObjectToArray()
    {
        $errorData = null;

        $expression = $this->translator->objectToArray($errorData);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['crew']['data'] 、
     * $relationships['userGroup']['data']、$relationships['sourceTemplate']['data']、
     * $relationships['transformationTemplate']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用4次, 分别用于处理 userGroup 、crew、sourceTemplate、transformationTemplate
     * 且返回各自的数组 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用一次, 入参是 $crewInfo, 出参是 $crew
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $userGroupInfo, 出参是 $userGroup
     * 6. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次, 入参是 $sourceTemplateInfo, 出参是 $sourceTemplate
     * 7. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次,
     * 入参是 $transformationTemplateInfo, 出参是 $transformationTemplate
     * 8. $rule->setCrew 调用一次, 入参 $crew
     * 9. $rule->setUserGroup 调用一次, 入参 $userGroup
     * 10. $rule->setSourceTemplate 调用一次, 入参 $sourceTemplate
     * 11. $rule->setTransformationTemplate 调用一次, 入参 $transformationTemplate
     */
    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1,
                'attributes'=>[
                    'category'=> 10
                ]
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'task'=>['data'=>'mockTask'],
            'template'=>['data'=>'mockTemplate'],
        ];

        $translator = $this->getMockBuilder(ErrorDataRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getTaskRestfulTranslator',
             'getTemplateRestfulTranslatorByCategory',
         ])
        ->getMock();

        $taskInfo = ['mockTaskInfo'];
        $templateInfo = ['mockTemplateInfo'];


        $template = new WbjTemplate();
        $task = new Task();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用4次, 依次入参 crew, userGroup,sourceTemplate, transformationTemplate
        //依次返回 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
        $translator->expects($this->exactly(2))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['task']['data']],
                 [$relationships['template']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $taskInfo,
                $templateInfo
            ));
   
        $taskRestfulTranslator = $this->prophesize(TaskRestfulTranslator::class);

        $taskRestfulTranslator->arrayToObject(Argument::exact($taskInfo))
        ->shouldBeCalledTimes(1)
        ->willReturn($task);
        
        $templateRestfulTranslator = $this->prophesize(WbjTemplateRestfulTranslator::class);
        $templateRestfulTranslator->arrayToObject(Argument::exact($templateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($template);

        //绑定
        $translator->expects($this->exactly(1))
            ->method('getTemplateRestfulTranslatorByCategory')
            ->willReturn($templateRestfulTranslator->reveal());

        $translator->expects($this->exactly(1))
            ->method('getTaskRestfulTranslator')
            ->willReturn($taskRestfulTranslator->reveal());
        //揭示
        $errorData = $translator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\ErrorData', $errorData);
        $this->assertEquals($template, $errorData->getTemplate());
        $this->assertEquals($task, $errorData->getTask());
    }
}
