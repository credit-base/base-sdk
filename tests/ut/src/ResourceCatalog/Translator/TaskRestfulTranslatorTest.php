<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\ResourceCatalog\Utils\Task\TaskRestfulUtils;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

class TaskRestfulTranslatorTest extends TestCase
{
    use TaskRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new TaskRestfulTranslator();

        Core::$container->set('crew', new Crew());
        $this->childTranslator = new class extends TaskRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $task = \Base\Sdk\ResourceCatalog\Utils\Task\MockFactory::generateTask(1);

        $expression['data']['id'] = $task->getId();
        $expression['data']['attributes']['pid'] = $task->getPid();
        $expression['data']['attributes']['total'] = $task->getTotal();
        $expression['data']['attributes']['successNumber'] = $task->getSuccessNumber();
        $expression['data']['attributes']['failureNumber'] = $task->getFailureNumber();
        $expression['data']['attributes']['sourceCategory'] = $task->getSourceCategory();
        $expression['data']['attributes']['sourceTemplate'] = $task->getSourceTemplate();
        $expression['data']['attributes']['targetCategory'] = $task->getTargetCategory();
        $expression['data']['attributes']['targetTemplate'] = $task->getTargetTemplate();
        $expression['data']['attributes']['targetRule'] = $task->getTargetRule();
        $expression['data']['attributes']['fileName'] = $task->getFileName();

        $expression['data']['attributes']['createTime'] = $task->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $task->getUpdateTime();
        $expression['data']['attributes']['status'] = $task->getStatus();
        $expression['data']['attributes']['statusTime'] = $task->getStatusTime();

        $taskObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\Task\Task', $taskObject);
        $this->compareArrayAndObjectCommon($expression, $taskObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $task = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\Task\NullTask', $task);
    }

    public function testObjectToArray()
    {
        $task = null;

        $expression = $this->translator->objectToArray($task);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['crew']['data'] 、
     * $relationships['userGroup']['data']、$relationships['sourceTemplate']['data']、
     * $relationships['transformationTemplate']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用4次, 分别用于处理 userGroup 、crew、sourceTemplate、transformationTemplate
     * 且返回各自的数组 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用一次, 入参是 $crewInfo, 出参是 $crew
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $userGroupInfo, 出参是 $userGroup
     * 6. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次, 入参是 $sourceTemplateInfo, 出参是 $sourceTemplate
     * 7. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次,
     * 入参是 $transformationTemplateInfo, 出参是 $transformationTemplate
     * 8. $Task->setCrew 调用一次, 入参 $crew
     * 9. $Task->setUserGroup 调用一次, 入参 $userGroup
     * 10. $Task->setSourceTemplate 调用一次, 入参 $sourceTemplate
     * 11. $Task->setTransformationTemplate 调用一次, 入参 $transformationTemplate
     */
    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1,
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'userGroup'=>['data'=>'mockUserGroup'],
            
        ];

        $translator = $this->getMockBuilder(TaskRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getCrewRestfulTranslator',
             'getUserGroupRestfulTranslator',
         ])
        ->getMock();

        $crewInfo = ['mockCrewInfo'];
        $userGroupInfo = ['mockUserGroupInfo'];

        $crew = new Crew();
        $userGroup = new UserGroup();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用4次, 依次入参 crew, userGroup,sourceTemplate, transformationTemplate
        //依次返回 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
        $translator->expects($this->exactly(2))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['userGroup']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $userGroupInfo
            ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($userGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroup);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);
   
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());

        $translator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        //揭示
        $Task = $translator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\Task\Task', $Task);
        $this->assertEquals($crew, $Task->getCrew());
        $this->assertEquals($userGroup, $Task->getUserGroup());
    }
}
