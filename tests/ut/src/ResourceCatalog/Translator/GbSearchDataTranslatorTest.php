<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Utils\GbSearchData\GbSearchDataUtils;

class GbSearchDataTranslatorTest extends TestCase
{
    use GbSearchDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new GbSearchDataTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\NullGbSearchData', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $gbSearchData =
            \Base\Sdk\ResourceCatalog\Utils\GbSearchData\GbSearchDataMockFactory::generateGbSearchData(1);

        $expression = $this->translator->objectToArray($gbSearchData);
    
        $this->compareArrayAndObjectGbSearchData($expression, $gbSearchData);
    }

    public function testObjectToArrayFail()
    {
        $gbSearchData = null;

        $expression = $this->translator->objectToArray($gbSearchData);
        $this->assertEquals(array(), $expression);
    }
}
