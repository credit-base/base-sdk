<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Sdk\ResourceCatalog\Utils\WbjSearchData\WbjSearchDataRestfulUtils;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Translator\CrewRestfulTranslator;

use Base\Sdk\ResourceCatalog\Model\itemsData;
use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\Translator\WbjTemplateRestfulTranslator;
use Base\Sdk\Template\Translator\BjTemplateRestfulTranslator;
use Base\Sdk\ResourceCatalog\Translator\ItemsDataRestfulTranslator;

class WbjSearchDataRestfulTranslatorTest extends TestCase
{
    use WbjSearchDataRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WbjSearchDataRestfulTranslator();

        Core::$container->set('crew', new Crew());
        $this->childTranslator = new class extends WbjSearchDataRestfulTranslator
        {
            public function getItemsDataRestfulTranslator() : ItemsDataRestfulTranslator
            {
                return parent::getItemsDataRestfulTranslator();
            }

            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }

            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }

            public function getWbjTemplateRestfulTranslator() : WbjTemplateRestfulTranslator
            {
                return parent::getWbjTemplateRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childTranslator->getCrewRestfulTranslator()
        );
    }

    public function testGetItemsDataRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Translator\ItemsDataRestfulTranslator',
            $this->childTranslator->getItemsDataRestfulTranslator()
        );
    }

    public function testGetWbjTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Template\Translator\WbjTemplateRestfulTranslator',
            $this->childTranslator->getWbjTemplateRestfulTranslator()
        );
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $wbjSearchData =
            \Base\Sdk\ResourceCatalog\Utils\WbjSearchData\WbjSearchDataMockFactory::generateWbjSearchData(1);

        $expression['data']['id'] = $wbjSearchData->getId();
        $expression['data']['attributes']['infoClassify'] = $wbjSearchData->getInfoClassify();
        $expression['data']['attributes']['infoCategory'] = $wbjSearchData->getInfoCategory();
        $expression['data']['attributes']['subjectCategory'] = $wbjSearchData->getSubjectCategory();
        $expression['data']['attributes']['dimension'] = $wbjSearchData->getDimension();
        $expression['data']['attributes']['name'] = $wbjSearchData->getName();
        $expression['data']['attributes']['identify'] = $wbjSearchData->getIdentify();
        $expression['data']['attributes']['expirationDate'] = $wbjSearchData->getExpirationDate();

        $expression['data']['attributes']['createTime'] = $wbjSearchData->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $wbjSearchData->getUpdateTime();
        $expression['data']['attributes']['status'] = $wbjSearchData->getStatus();
        $expression['data']['attributes']['statusTime'] = $wbjSearchData->getStatusTime();

        $wbjSearchDataObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\WbjSearchData', $wbjSearchDataObject);
        $this->compareArrayAndObjectCommon($expression, $wbjSearchDataObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $wbjSearchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\NullWbjSearchData', $wbjSearchData);
    }

    public function testObjectToArray()
    {
        $wbjSearchData = null;

        $expression = $this->translator->objectToArray($wbjSearchData);
        $this->assertEquals(array(), $expression);
    }

    /**
     * 测试 expression 包含 included 场景
     * 1. $this->relationship 被调用一次,
     * 且传参 $expression['included'] 和 $expression['data']['relationships']
     * 2. 返回 $relationships, 且 $relationships['crew']['data'] 、
     * $relationships['userGroup']['data']、$relationships['sourceTemplate']['data']、
     * $relationships['transformationTemplate']['data'] 必须赋值
     * 3. $this->changeArrayFormat 被调用4次, 分别用于处理 userGroup 、crew、sourceTemplate、transformationTemplate
     * 且返回各自的数组 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
     * 4. $this->getCrewRestfulTranslator()->arrayToObject 被调用一次, 入参是 $crewInfo, 出参是 $crew
     * 5. $this->getUserGroupRestfulTranslator()->arrayToObject 被调用一次, 入参是 $userGroupInfo, 出参是 $userGroup
     * 6. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次, 入参是 $sourceTemplateInfo, 出参是 $sourceTemplate
     * 7. $this->getTemplateRestfulTranslator()->arrayToObject 被调用一次,
     * 入参是 $transformationTemplateInfo, 出参是 $transformationTemplate
     * 8. $WbjSearchData->setCrew 调用一次, 入参 $crew
     * 9. $WbjSearchData->setUserGroup 调用一次, 入参 $userGroup
     * 10. $WbjSearchData->setSourceTemplate 调用一次, 入参 $sourceTemplate
     * 11. $WbjSearchData->setTransformationTemplate 调用一次, 入参 $transformationTemplate
     */
    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1,
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'crew'=>['data'=>'mockCrew'],
            'sourceUnit'=>['data'=>'mockSourceUnit'],
            'itemsData'=>['data'=>'mockItemsData'],
            'template'=>['data'=>'mockTemplate'],
        ];

        $wbjTranslator = $this->getMockBuilder(WbjSearchDataRestfulTranslator::class)
        ->setMethods([
             'relationship',
             'changeArrayFormat',
             'getCrewRestfulTranslator',
             'getUserGroupRestfulTranslator',
             'getItemsDataRestfulTranslator',
             'getWbjTemplateRestfulTranslator',
         ])
        ->getMock();

        $crewInfo = ['mockCrewInfo'];
        $sourceUnitInfo = ['mockSourceUnitInfo'];
        $templateInfo = ['mockTemplateInfo'];
        $itemsDataInfo = ['mockItemsDataInfo'];

        $crew = new Crew();
        $sourceUnit = new UserGroup();
        $template = new WbjTemplate();
        $itemsData = new ItemsData();

        //预言
        $wbjTranslator->expects($this->exactly(2))
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用4次, 依次入参 crew, userGroup,sourceTemplate, transformationTemplate
        //依次返回 $crewInfo 、 $userGroupInfo、$sourceTemplateInfo、$transformationTemplateInfo
        $wbjTranslator->expects($this->exactly(4))
            ->method('changeArrayFormat')
             ->withConsecutive(
                 [$relationships['crew']['data']],
                 [$relationships['sourceUnit']['data']],
                 [$relationships['itemsData']['data']],
                 [$relationships['template']['data']]
             )
            ->will($this->onConsecutiveCalls(
                $crewInfo,
                $sourceUnitInfo,
                $itemsDataInfo,
                $templateInfo
            ));
  
        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($sourceUnitInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($sourceUnit);

        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($crewInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($crew);
   
        $itemsDataRestfulTranslator = $this->prophesize(ItemsDataRestfulTranslator::class);

        $itemsDataRestfulTranslator->arrayToObject(Argument::exact($itemsDataInfo))
        ->shouldBeCalledTimes(1)
        ->willReturn($itemsData);
        
        $wbjTemplateRestfulTranslator = $this->prophesize(WbjTemplateRestfulTranslator::class);
        $wbjTemplateRestfulTranslator->arrayToObject(Argument::exact($templateInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($template);

        //绑定
        $wbjTranslator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());

        $wbjTranslator->expects($this->exactly(1))
            ->method('getWbjTemplateRestfulTranslator')
            ->willReturn($wbjTemplateRestfulTranslator->reveal());

        $wbjTranslator->expects($this->exactly(1))
            ->method('getItemsDataRestfulTranslator')
            ->willReturn($itemsDataRestfulTranslator->reveal());

        $wbjTranslator->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());
        //揭示
        $wbjSearchData = $wbjTranslator->arrayToObject($expression);
       
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\WbjSearchData', $wbjSearchData);
        $this->assertEquals($crew, $wbjSearchData->getCrew());
        $this->assertEquals($sourceUnit, $wbjSearchData->getSourceUnit());
        $this->assertEquals($itemsData, $wbjSearchData->getItemsData());
        $this->assertEquals($template, $wbjSearchData->getTemplate());
    }
}
