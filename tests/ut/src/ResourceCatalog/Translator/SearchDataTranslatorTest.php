<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use PHPUnit\Framework\TestCase;

class SearchDataTranslatorTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = $this->getMockBuilder('Base\Sdk\ResourceCatalog\Translator\SearchDataTranslator')
        ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }
}
