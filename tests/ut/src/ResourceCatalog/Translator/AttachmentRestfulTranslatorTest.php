<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Utils\Attachment\AttachmentRestfulUtils;

use Base\Sdk\Crew\Model\Crew;

class AttachmentRestfulTranslatorTest extends TestCase
{
    use AttachmentRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new AttachmentRestfulTranslator();

        Core::$container->set('crew', new Crew());
        $this->childTranslator = new class extends AttachmentRestfulTranslator
        {
          
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testArrayToObjectWithoutIncluded()
    {
        $attachment = \Base\Sdk\ResourceCatalog\Utils\Attachment\MockFactory::generateAttachment(1);

        $expression['data']['id'] = $attachment->getId();
        $expression['data']['attributes']['name'] = $attachment->getName();
        $expression['data']['attributes']['hash'] = $attachment->getHash();

        $expression['data']['attributes']['createTime'] = $attachment->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $attachment->getUpdateTime();
        $expression['data']['attributes']['status'] = $attachment->getStatus();
        $expression['data']['attributes']['statusTime'] = $attachment->getStatusTime();

        $attachmentObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\Attachment', $attachmentObject);
        $this->compareArrayAndObjectCommon($expression, $attachmentObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $attachment = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\NullAttachment', $attachment);
    }

    public function testObjectToArray()
    {
        $attachment = null;

        $expression = $this->translator->objectToArray($attachment);
        $this->assertEquals(array(), $expression);
    }
}
