<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Utils\ErrorData\ErrorDataUtils;

class ErrorDataTranslatorTest extends TestCase
{
    use ErrorDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ErrorDataTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\NullErrorData', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $errorData = \Base\Sdk\ResourceCatalog\Utils\ErrorData\MockFactory::generateErrorData(1);

        $expression = $this->translator->objectToArray($errorData);
    
        $this->compareArrayAndObjectErrorData($expression, $errorData);
    }

    public function testObjectToArrayFail()
    {
        $errorData = null;

        $expression = $this->translator->objectToArray($errorData);
        $this->assertEquals(array(), $expression);
    }
}
