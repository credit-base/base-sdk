<?php
namespace Base\Sdk\ResourceCatalog\Translator;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Utils\Task\TaskUtils;

class TaskTranslatorTest extends TestCase
{
    use TaskUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new TaskTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObject()
    {
        $expression = array();

        $objects = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('Base\Sdk\ResourceCatalog\Model\Task\NullTask', $objects);
    }

    public function testArrayToObjects()
    {
        $expression = array();

        $objects = $this->translator->arrayToObjects($expression);

        $this->assertEquals(array(), $objects);
    }

    public function testObjectToArray()
    {
        $task = \Base\Sdk\ResourceCatalog\Utils\Task\MockFactory::generateTask(1);

        $expression = $this->translator->objectToArray($task);
    
        $this->compareArrayAndObjectTask($expression, $task);
    }

    public function testObjectToArrayFail()
    {
        $task = null;

        $expression = $this->translator->objectToArray($task);
        $this->assertEquals(array(), $expression);
    }

    public function testIsFileExitNotFileName()
    {
        $fileName = '12344';
        $result = $this->translator->isFileExit($fileName);
        $this->assertEquals('', $result);
    }
}
