<?php
namespace Base\Sdk\ResourceCatalog\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;

/**
 * @SuppressWarnings(PHPMD)
 */
class SearchDataTest extends TestCase
{
    private $searchData;

    public function setUp()
    {
        $this->searchData = new MockSearchData();
    }

    public function tearDown()
    {
        unset($this->searchData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Model\Crew',
            $this->searchData->getCrew()
        );
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Model\UserGroup',
            $this->searchData->getSourceUnit()
        );
        $this->assertEquals(SearchData::DATA_STATUS['CONFIRM'], $this->searchData->getStatus());
        $this->assertEquals(SearchData::EXPIRATION_DATE_DEFAULT, $this->searchData->getExpirationDate());
        $this->assertEquals(0, $this->searchData->getInfoClassify());
        $this->assertEquals(0, $this->searchData->getInfoCategory());
        $this->assertEquals(0, $this->searchData->getSubjectCategory());
        $this->assertEquals(0, $this->searchData->getDimension());
        $this->assertEquals('', $this->searchData->getName());
        $this->assertEquals('', $this->searchData->getIdentify());
    }

    //Crew 测试 -------------------------------------------------------- start
    /**
     * 设置 BjSearchData setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $expected = new Crew();

        $this->searchData->setCrew($expected);
        $this->assertEquals($expected, $this->searchData->getCrew());
    }

    /**
     * 设置 BjSearchData setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->searchData->setCrew('Crew');
    }
    //Crew 测试 --------------------------------------------------------   end

    //sourceUnit 测试 -------------------------------------------------------- start
    /**
     * 设置 BjSearchData setSourceUnit() 正确的传参类型,期望传值正确
     */
    public function testSetSourceUnitCorrectType()
    {
        $expected = new UserGroup();

        $this->searchData->setSourceUnit($expected);
        $this->assertEquals($expected, $this->searchData->getSourceUnit());
    }

    /**
     * 设置 BjSearchData setSourceUnit() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceUnitWrongType()
    {
        $this->searchData->setSourceUnit('string');
    }
    //sourceUnit 测试 --------------------------------------------------------   end

    //infoClassify 测试 ------------------------------------------------------ start
    /**
     * 设置 BjSearchData setInfoClassify() 正确的传参类型,期望传值正确
     */
    public function testSetInfoClassifyCorrectType()
    {
        $this->searchData->setInfoClassify(1);
        $this->assertEquals(1, $this->searchData->getInfoClassify());
    }

    /**
     * 设置 BjSearchData setInfoClassify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoClassifyWrongType()
    {
        $this->searchData->setInfoClassify(['string']);
    }
    //infoClassify 测试 ------------------------------------------------------   end

    //infoCategory 测试 ------------------------------------------------------ start
    /**
     * 设置 BjSearchData setInfoCategory() 正确的传参类型,期望传值正确
     */
    public function testSetInfoCategoryCorrectType()
    {
        $this->searchData->setInfoCategory(1);
        $this->assertEquals(1, $this->searchData->getInfoCategory());
    }

    /**
     * 设置 BjSearchData setInfoCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoCategoryWrongType()
    {
        $this->searchData->setInfoCategory(['string']);
    }
    //infoCategory 测试 ------------------------------------------------------   end

    //subjectCategory 测试 ------------------------------------------------------ start
    /**
     * 设置 BjSearchData setSubjectCategory() 正确的传参类型,期望传值正确
     */
    public function testSetSubjectCategoryCorrectType()
    {
        $this->searchData->setSubjectCategory(1);
        $this->assertEquals(1, $this->searchData->getSubjectCategory());
    }

    /**
     * 设置 BjSearchData setSubjectCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubjectCategoryWrongType()
    {
        $this->searchData->setSubjectCategory(['string']);
    }
    //subjectCategory 测试 ------------------------------------------------------   end

    //dimension 测试 ------------------------------------------------------ start
    /**
     * 设置 BjSearchData setDimension() 正确的传参类型,期望传值正确
     */
    public function testSetDimensionCorrectType()
    {
        $this->searchData->setDimension(1);
        $this->assertEquals(1, $this->searchData->getDimension());
    }

    /**
     * 设置 BjSearchData setDimension() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDimensionWrongType()
    {
        $this->searchData->setDimension(['string']);
    }
    //dimension 测试 ------------------------------------------------------   end

    //name 测试 ------------------------------------------------------ start
    /**
     * 设置 BjSearchData setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->searchData->setName('string');
        $this->assertEquals('string', $this->searchData->getName());
    }

    /**
     * 设置 BjSearchData setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->searchData->setName(array());
    }
    //name 测试 ------------------------------------------------------   end

    //identify 测试 ------------------------------------------------------ start
    /**
     * 设置 BjSearchData setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->searchData->setIdentify('string');
        $this->assertEquals('string', $this->searchData->getIdentify());
    }

    /**
     * 设置 BjSearchData setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->searchData->setIdentify([1]);
    }
    //identify 测试 ------------------------------------------------------   end

    //expirationDate 测试 ------------------------------------------------------ start
    /**
     * 设置 BjSearchData setExpirationDate() 正确的传参类型,期望传值正确
     */
    public function testSetExpirationDateCorrectType()
    {
        $this->searchData->setExpirationDate(1);
        $this->assertEquals(1, $this->searchData->getExpirationDate());
    }

    /**
     * 设置 BjSearchData setExpirationDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExpirationDateWrongType()
    {
        $this->searchData->setExpirationDate(['string']);
    }
    //expirationDate 测试 ------------------------------------------------------   end
}
