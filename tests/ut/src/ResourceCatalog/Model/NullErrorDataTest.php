<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NullErrorDataTest extends TestCase
{
    private $nullErrorData;

    public function setUp()
    {
        $this->nullErrorData = NullErrorData::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullErrorData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsErrorData()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Model\ErrorData',
            $this->nullErrorData
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullErrorData
        );
    }

    public function testResourceNotExist()
    {
        $nullErrorData = new MockNullErrorData();

        $result = $nullErrorData->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
