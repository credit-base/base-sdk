<?php
namespace Base\Sdk\ResourceCatalog\Model;

use PHPUnit\Framework\TestCase;


use Marmot\Core;

class ItemsDataTest extends TestCase
{
    private $itemsData;

    public function setUp()
    {
        $this->itemsData = new ItemsData();
    }

    public function tearDown()
    {
        unset($this->itemsData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->itemsData->getId());
        $this->assertEquals(array(), $this->itemsData->getData());
    }

    //data 测试 -------------------------------------------------------- start
    /**
     * 设置 ItemsData setData() 正确的传参类型,期望传值正确
     */
    public function testSetDataCorrectType()
    {
        $expected = array(1,2);

        $this->itemsData->setData($expected);
        $this->assertEquals($expected, $this->itemsData->getData());
    }

    /**
     * 设置 ItemsData setData() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataWrongType()
    {
        $this->itemsData->setData('data');
    }
    //data 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 ItemsData setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $expected = 0;

        $this->itemsData->setStatus($expected);
        $this->assertEquals($expected, $this->itemsData->getStatus());
    }

    /**
     * 设置 ItemsData setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->itemsData->setStatus('string');
    }
    //status 测试 --------------------------------------------------------   end
}
