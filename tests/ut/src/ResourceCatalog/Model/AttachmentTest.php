<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Sdk\Crew\Model\Crew;

use Base\Sdk\Template\Model\NullWbjTemplate;
use Base\Sdk\Template\Repository\WbjTemplateRepository;
use Base\Sdk\ResourceCatalog\Repository\AttachmentRepository;

/**
 * @SuppressWarnings(PHPMD.Superglobals)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class AttachmentTest extends TestCase
{
    private $attachment;

    public function setUp()
    {
        Core::$container->set('crew', new Crew(1));
        $this->attachment = new Attachment();
    }

    public function tearDown()
    {
        $_FILES = [];
        unset($this->attachment);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->attachment->getId());
        $this->assertEquals('', $this->attachment->getName());
        $this->assertEquals(
            Core::$container->get('attachment.upload.path'),
            $this->attachment->getPath()
        );
        $this->assertEquals(0, $this->attachment->getSize());
        $this->assertEquals('', $this->attachment->getHash());
        $this->assertEquals('', $this->attachment->getSource());
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Model\Crew',
            $this->attachment->getCrew()
        );
        $this->assertEquals(
            Core::$container->get('time'),
            $this->attachment->getCreateTime()
        );
    }
    
    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Task setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->attachment->setId(1);
        $this->assertEquals(1, $this->attachment->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //status 测试 ---------------------------------------------------------- start
    /**
     * 设置 Task setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->attachment->setStatus(1);
        $this->assertEquals(1, $this->attachment->getStatus());
    }
    //status 测试 ----------------------------------------------------------   end

    //name -- 开始
    /**
     * 设置 Task setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->attachment->setName('name');
        $this->assertEquals('name', $this->attachment->getName());
    }
    /**
     * 设置 Task setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->attachment->setName(array(1, 2, 3));
    }
    //name -- 结束

    //extension -- 开始
    /**
     * 设置 Task getExtension() 正确的传参类型,期望传值正确
     */
    public function testSetExtensionCorrectType()
    {
        $this->attachment->setExtension('xlsx');
        $this->assertEquals('xlsx', $this->attachment->getExtension());
    }
    //extension -- 结束

    //size -- 开始
    /**
     * 设置 Task setSize() 正确的传参类型,期望传值正确
     */
    public function testSetSizeCorrectType()
    {
        $size = 1;
        $this->attachment->setSize($size);
        $this->assertEquals($size, $this->attachment->getSize());
    }
    /**
     * 设置 Task setSize() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSizeWrongType()
    {
        $this->attachment->setSize('size');
    }
    //size -- 结束

    //hash -- 开始
    /**
     * 设置 Task setHash() 正确的传参类型,期望传值正确
     */
    public function testSetHashCorrectType()
    {
        $this->attachment->setHash('hash');
        $this->assertEquals('hash', $this->attachment->getHash());
    }
    /**
     * 设置 Task setHash() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHashWrongType()
    {
        $this->attachment->setHash(array(1, 2, 3));
    }
    //hash -- 结束

    //source -- 开始
    /**
     * 设置 Task setSource() 正确的传参类型,期望传值正确
     */
    public function testSetSourceCorrectType()
    {
        $this->attachment->setSource('source');
        $this->assertEquals('source', $this->attachment->getSource());
    }
    /**
     * 设置 Task setSource() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceWrongType()
    {
        $this->attachment->setHash(array(1, 2, 3));
    }
    //source -- 结束

    //upload
    public function testUploadFailUploadFile()
    {
        $attachment = $this->getMockBuilder(MockAttachment::class)
                           ->setMethods(
                               [
                               'validateUploadFile'
                               ]
                           )->getMock();

        //绑定
        $attachment->expects($this->once())
             ->method('validateUploadFile')
             ->willReturn(false);
        $templateId = 1;
        $result = $attachment->upload($templateId);
        $this->assertFalse($result);
    }
    
    // public function testUploadValidateNameFormatFile()
    // {
    //     $attachment = $this->getMockBuilder(MockAttachment::class)
    //                        ->setMethods(
    //                            [
    //                            'validateNameFormat'
    //                            ]
    //                        )->getMock();

    //     //绑定
    //     $attachment->expects($this->any())
    //          ->method('validateNameFormat')
    //          ->willReturn(false);
    //     $templateId = 1;
    //     $result = $attachment->upload($templateId);
    //     $this->assertFalse($result);
    // }

    public function testUploadFailVaidate()
    {
        $attachment = $this->getMockBuilder(MockAttachment::class)
                           ->setMethods(
                               [
                               'validateUploadFile',
                               'validateNameFormat',
                               'prepare',
                               'validate'
                               ]
                           )->getMock();

        //绑定
        $attachment->expects($this->once())
             ->method('validateUploadFile')
             ->willReturn(true);
        
        $attachment->expects($this->once())
             ->method('validateNameFormat')
             ->willReturn(true);

        $attachment->expects($this->once())
             ->method('prepare');

         $attachment->expects($this->once())
             ->method('validate')
             ->willReturn(false);
        $templateId = 1;
        $result = $attachment->upload($templateId);
        $this->assertFalse($result);
    }

    public function testUploadFailVaidateNameFormat()
    {
        $attachment = $this->getMockBuilder(MockAttachment::class)
                           ->setMethods(
                               [
                               'validateUploadFile',
                               'validateNameFormat',
                               'prepare',
                               'validate'
                               ]
                           )->getMock();

        //绑定
        $attachment->expects($this->once())
             ->method('validateUploadFile')
             ->willReturn(true);
        
        $attachment->expects($this->once())
             ->method('validateNameFormat')
             ->willReturn(false);

        $attachment->expects($this->once())
             ->method('prepare');

         $attachment->expects($this->any())
             ->method('validate')
             ->willReturn(true);
        $templateId = 1;
        $result = $attachment->upload($templateId);
        $this->assertFalse($result);
    }

    public function testUploadSuccess()
    {
        $prefix = 'prefix';
        $destination = 'destination';
        $source = 'source';
        $templateId = 1;

        $attachment = $this->getMockBuilder(MockAttachment::class)
                           ->setMethods(
                               [
                               'validateUploadFile',
                               'validateNameFormat',
                               'prepare',
                               'validate',
                               'generateFilePath',
                               'getSource',
                               'move'
                               ]
                           )->getMock();

        //绑定
        $attachment->expects($this->once())
             ->method('validateUploadFile')
             ->willReturn(true);
        
        $attachment->expects($this->once())
             ->method('validateNameFormat')
             ->with($templateId)
             ->willReturn(true);

        $attachment->expects($this->once())
             ->method('prepare')
             ->with($prefix);

        $attachment->expects($this->once())
             ->method('validate')
             ->willReturn(true);

        $attachment->expects($this->once())
             ->method('generateFilePath')
             ->willReturn($destination);

        $attachment->expects($this->once())
             ->method('getSource')
             ->willReturn($source);

        $attachment->expects($this->once())
             ->method('move')
             ->with($source, $destination)
             ->willReturn(true);

        $result = $attachment->upload($templateId, $prefix);
        $this->assertTrue($result);
    }

    //prepare
    public function testPrepare()
    {
        $source = '/var/www/html/tests/mock/ResourceCatalog/Model/MockFile';
        $size = 10;
        $name = 'name';
        $prefix = 'attachment';
        $extension = 'xlsx';

        $_FILES[$prefix]['size'] = $size;
        $_FILES[$prefix]['name'] = $name.'.'.$extension;
        $_FILES[$prefix]['tmp_name'] = $source;

        $attachment = $this->getMockBuilder(MockAttachment::class)
                           ->setMethods(
                               [
                               'setSize',
                               'setName',
                               'setExtension',
                               'setSource',
                               'generateHash'
                               ]
                           )->getMock();

        //绑定
        $attachment->expects($this->once())
             ->method('setSize')
             ->with($size);

        $attachment->expects($this->once())
             ->method('setName')
             ->with($name);
    
        $attachment->expects($this->once())
             ->method('setExtension')
             ->with($extension);

         $attachment->expects($this->once())
             ->method('setSource')
             ->with($source);

         $attachment->expects($this->once())
             ->method('generateHash');

        $attachment->prepare($prefix);
    }

    //generateHash
    public function testGenerateHash()
    {
        $source = '/var/www/html/tests/mock/ResourceCatalog/Model/MockFile';
        $size = 10;

        $attachment = new MockAttachment();
        $attachment->setSource($source);
        $attachment->setSize($size);

        $expect = md5(md5_file($source).$size);

        $attachment->generateHash();
        $this->assertEquals($expect, $attachment->getHash());
    }

    //generateFilePath
    public function testGenerateFilePath()
    {
        $id = 1;
        $path = 'path';
        $crew = new Crew($id);
        $hash = 'hash';
        $name = 'name';
        $extension = 'extension';

        $attachment = $this->getMockBuilder(MockAttachment::class)
                           ->setMethods(
                               [
                               'getPath',
                               'getCrew',
                               'getHash',
                               'getName',
                               'getExtension'
                               ]
                           )->getMock();

        //绑定
        $attachment->expects($this->once())
             ->method('getPath')
             ->willReturn($path);

        $attachment->expects($this->once())
             ->method('getCrew')
             ->willReturn($crew);

         $attachment->expects($this->once())
             ->method('getHash')
             ->willReturn($hash);

         $attachment->expects($this->once())
             ->method('getName')
             ->willReturn($name);

        $attachment->expects($this->once())
             ->method('getExtension')
             ->willReturn($extension);

        $expect = $path.$name.'_'.$id.'_'.$hash.'.'.$extension;
        $result = $attachment->generateFilePath();
        $this->assertEquals($expect, $result);
    }

    //move
    public function testMove()
    {
        $source = '/source';
        $destination = '/destination';

        $attachment = new MockAttachment();
        $result = $attachment->move($source, $destination);

        $this->assertFalse($result);
    }

    //validateNameFormat-true
    public function testValidateNameFormat()
    {
        $attachment = $this->getMockBuilder(
            MockAttachment::class
        )->setMethods(['getTemplateById', 'getName'])->getMock();

        $templateId = 1;
        $template = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate($templateId);

        $identify = $template->getIdentify();
        $id = $templateId;
        $category = $template->getCategory();

        $fileName = $identify.'_'.$id.'_'.$category;

        $attachment->expects($this->once())->method('getName')->willReturn($fileName);
        $attachment->expects($this->once())->method('getTemplateById')->willReturn($template);

        $result = $attachment->validateNameFormat($templateId);
        $this->assertTrue($result);
    }

    //validateNameFormat-identifyFalse
    public function testValidateNameFormatIdentifyFalse()
    {
        $attachment = $this->getMockBuilder(
            MockAttachment::class
        )->setMethods(['getTemplateById', 'getName'])->getMock();

        $templateId = 1;
        $template = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate($templateId);

        $identify = $template->getIdentify().'TEST';
        $id = $templateId;
        $category = $template->getCategory();

        $fileName = $identify.'_'.$id.'_'.$category;

        $attachment->expects($this->once())->method('getName')->willReturn($fileName);
        $attachment->expects($this->once())->method('getTemplateById')->willReturn($template);

        $result = $attachment->validateNameFormat($templateId);

        $this->assertEquals(Core::getLastError()->getId(), FILE_IDENTIFY_FORMAT_ERROR);
        $this->assertFalse($result);
    }

    //validateNameFormat-categoryFalse
    public function testValidateNameFormatCategoryFalse()
    {
        $attachment = $this->getMockBuilder(
            MockAttachment::class
        )->setMethods(['getTemplateById', 'getName'])->getMock();

        $templateId = 1;
        $template = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate($templateId);

        $identify = $template->getIdentify();
        $id = $templateId;
        $category = $template->getCategory().'111';

        $fileName = $identify.'_'.$id.'_'.$category;

        $attachment->expects($this->once())->method('getName')->willReturn($fileName);
        $attachment->expects($this->once())->method('getTemplateById')->willReturn($template);

        $result = $attachment->validateNameFormat($templateId);

        $this->assertEquals(Core::getLastError()->getId(), FILE_CATEGORY_FORMAT_ERROR);
        $this->assertFalse($result);
    }

    //validateNameFormat-IdFalse
    public function testValidateNameFormatIdFalse()
    {
        $attachment = $this->getMockBuilder(
            MockAttachment::class
        )->setMethods(['getTemplateById', 'getName'])->getMock();

        $templateId = 1;
        $template = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate($templateId);

        $identify = $template->getIdentify();
        $id = $templateId.'1';
        $category = $template->getCategory();

        $fileName = $identify.'_'.$id.'_'.$category;

        $attachment->expects($this->once())->method('getName')->willReturn($fileName);
        $attachment->expects($this->once())->method('getTemplateById')->willReturn($template);

        $result = $attachment->validateNameFormat($templateId);

        $this->assertEquals(Core::getLastError()->getId(), FILE_IDENTIFY_FORMAT_ERROR);
        $this->assertFalse($result);
    }


    //validateContentDuplicate
    public function testValidateContentDuplicateFalse()
    {
        $attachment = $this->getMockBuilder(MockAttachment::class)->setMethods(['isExitByHash'])->getMock();

        $attachment->expects($this->once())->method('isExitByHash')->willReturn(true);

        $result = $attachment->validateContentDuplicate();
        $this->assertEquals(Core::getLastError()->getId(), FILE_IS_UNIQUE);
        $this->assertFalse($result);
    }

    public function testValidateContentDuplicate()
    {
        $attachment = $this->getMockBuilder(MockAttachment::class)->setMethods(['isExitByHash'])->getMock();

        $attachment->expects($this->once())->method('isExitByHash')->willReturn(false);

        $result = $attachment->validateContentDuplicate();
        $this->assertTrue($result);
    }

    //getWbjTemplateRepository
    public function testGetWbjTemplateRepository()
    {
        $attachment = new MockAttachment();

        $this->assertInstanceOf(
            'Base\Sdk\Template\Repository\WbjTemplateRepository',
            $attachment->getWbjTemplateRepository()
        );
    }

    //getTemplateById
    public function testGetTemplateByIdFalse()
    {
        $attachment = $this->getMockBuilder(
            MockAttachment::class
        )->setMethods(['getWbjTemplateRepository'])->getMock();

        $templateId = 1;
        $template = NullWbjTemplate::getInstance();

        $repository = $this->prophesize(WbjTemplateRepository::class);
        $repository->scenario(
            Argument::exact(WbjTemplateRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($templateId))->shouldBeCalledTimes(1)->willReturn($template);
    
        $attachment->expects($this->exactly(1))->method(
            'getWbjTemplateRepository'
        )->willReturn($repository->reveal());

        $result = $attachment->getTemplateById($templateId);

        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_NOT_EXIST);
        $this->assertFalse($result);
    }

    public function testGetTemplateById()
    {
        $attachment = $this->getMockBuilder(
            MockAttachment::class
        )->setMethods(['getWbjTemplateRepository'])->getMock();

        $templateId = 1;
        $template = \Base\Sdk\Template\Utils\WbjTemplate\MockWbjTemplateFactory::generateWbjTemplate($templateId);

        $repository = $this->prophesize(WbjTemplateRepository::class);
        $repository->scenario(
            Argument::exact(WbjTemplateRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($templateId))->shouldBeCalledTimes(1)->willReturn($template);
    
        $attachment->expects($this->exactly(1))->method(
            'getWbjTemplateRepository'
        )->willReturn($repository->reveal());

        $result = $attachment->getTemplateById($templateId);

        $this->assertEquals($template, $result);
    }

    //getAttachmentRepository
    public function testGetAttachmentRepository()
    {
        $attachment = new MockAttachment();

        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\AttachmentRepository',
            $attachment->getAttachmentRepository()
        );
    }

    //isExitByHash
    public function testIsExitByHashFalse()
    {
        $attachment = $this->getMockBuilder(
            MockAttachment::class
        )->setMethods(['getAttachmentRepository', 'getHash'])->getMock();
        
        $hash = 'hash';
        $filter['hash'] = $hash;
        $count = Attachment::UPLOAD_DEFAULT_COUNT;
        $list = array('list');

        $attachment->expects($this->once())->method('getHash')->willReturn($hash);

        $repository = $this->prophesize(AttachmentRepository::class);
        $repository->scenario(
            Argument::exact(WbjTemplateRepository::LIST_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact(['id']),
            Argument::exact(PAGE),
            Argument::exact(PAGE)
        )->shouldBeCalledTimes(1)->willReturn([$count, $list]);
    
        $attachment->expects($this->exactly(1))->method(
            'getAttachmentRepository'
        )->willReturn($repository->reveal());

        $result = $attachment->isExitByHash();

        $this->assertFalse($result);
    }

    public function testIsExitByHash()
    {
        $attachment = $this->getMockBuilder(
            MockAttachment::class
        )->setMethods(['getAttachmentRepository', 'getHash'])->getMock();
        
        $hash = 'hashTrue';
        $filter['hash'] = $hash;
        $sort = ['id'];
        $page = $size = PAGE;
        $count = Attachment::UPLOAD_DEFAULT_COUNT.'111';
        $list = array('list');

        $attachment->expects($this->once())->method('getHash')->willReturn($hash);

        $repository = $this->prophesize(AttachmentRepository::class);
        $repository->scenario(
            Argument::exact(WbjTemplateRepository::LIST_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count, $list]);
    
        $attachment->expects($this->exactly(1))->method(
            'getAttachmentRepository'
        )->willReturn($repository->reveal());

        $result = $attachment->isExitByHash();

        $this->assertTrue($result);
    }

    //validateUploadFile
    public function testValidateUploadFileFail()
    {
        $attachment = new MockAttachment();
        $result = $attachment->validateUploadFile();
        $this->assertFalse($result);
    }

    public function testValidateUploadFileSuccess()
    {
        $attachment = new MockAttachment();
        $_FILES['fileName'] = 'name';
        $result = $attachment->validateUploadFile();
        $this->assertTrue($result);
    }

    //validate
    public function testValidate()
    {
        $attachment = $this->getMockBuilder(MockAttachment::class)
                           ->setMethods(
                               [
                               'validateSize',
                               'validateExtension',
                               'validateContentDuplicate'
                               ]
                           )->getMock();

        //绑定
        $attachment->expects($this->once())
             ->method('validateSize')
             ->willReturn(true);

        $attachment->expects($this->once())
             ->method('validateExtension')
             ->willReturn(true);

         $attachment->expects($this->once())
             ->method('validateContentDuplicate')
             ->willReturn(true);

        $result = $attachment->validate();
        $this->assertTrue($result);
    }

    //validateSize
    public function testValidateSizeFail()
    {
        $attachment = new MockAttachment();
        $size = Attachment::MAX_FILE_SIZE + 1;
        $attachment->setSize($size);
        $result = $attachment->validateSize();
        $this->assertFalse($result);
    }

    public function testValidateSizeSuccessl()
    {
        $attachment = new MockAttachment();
        $result = $attachment->validateSize();
        $this->assertTrue($result);
    }

    //validateExtension
    public function testValidateExtensionSuccess()
    {
        $attachment = new MockAttachment();
        $attachment->setExtension('xlsx');
        $result = $attachment->validateExtension();
        $this->assertTrue($result);
    }

    public function testValidateExtensionFail()
    {
        $attachment = new MockAttachment();
        $attachment->setExtension('doc');
        $result = $attachment->validateExtension();
        $this->assertFalse($result);
    }

    //validateNameFormat
    // public function testValidateNameFormatSuccess()
    // {
    //     $attachment = new MockAttachment();
    //     $templateId = 1;
    //     $result = $attachment->validateNameFormat($templateId);
    //     $this->assertTrue($result);
    // }

    //validateContentDuplicate
    public function testValidateContentDuplicateSuccess()
    {
        $attachment = new MockAttachment();
        $result = $attachment->validateContentDuplicate();
        $this->assertTrue($result);
    }
}
