<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Firebase\JWT\JWT;

use Base\Sdk\ResourceCatalog\Repository\AttachmentRepository;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NullAttachmentTest extends TestCase
{
    private $nullAttachment;

    public function setUp()
    {
        $this->nullAttachment = NullAttachment::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullAttachment);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsAttachment()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Model\Attachment',
            $this->nullAttachment
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullAttachment
        );
    }

    public function testResourceNotExist()
    {
        $nullAttachment = new MockNullAttachment();

        $result = $nullAttachment->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
