<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Firebase\JWT\JWT;

use Base\Sdk\ResourceCatalog\Repository\ItemsDataRepository;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NullItemsDataTest extends TestCase
{
    private $nullItemsData;

    public function setUp()
    {
        $this->nullItemsData = NullItemsData::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullItemsData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsItemsData()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Model\ItemsData',
            $this->nullItemsData
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullItemsData
        );
    }

    public function testResourceNotExist()
    {
        $nullItemsData = new MockNullItemsData();

        $result = $nullItemsData->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
