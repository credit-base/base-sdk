<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NullGbSearchDataTest extends TestCase
{
    private $nullGbSearchData;

    public function setUp()
    {
        $this->nullGbSearchData = NullGbSearchData::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullGbSearchData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsGbSearchData()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Model\GbSearchData',
            $this->nullGbSearchData
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullGbSearchData
        );
    }

    public function testResourceNotExist()
    {
        $nullGbSearchData = new MockNullGbSearchData();

        $result = $nullGbSearchData->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testConfirm()
    {
        $nullNews = new MockNullGbSearchData();

        $result = $nullNews->confirm();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
