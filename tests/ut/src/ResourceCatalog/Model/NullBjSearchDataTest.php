<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NullBjSearchDataTest extends TestCase
{
    private $nullBjSearchData;

    public function setUp()
    {
        $this->nullBjSearchData = NullBjSearchData::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullBjSearchData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsBjSearchData()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Model\BjSearchData',
            $this->nullBjSearchData
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullBjSearchData
        );
    }

    public function testResourceNotExist()
    {
        $nullBjSearchData = new MockNullBjSearchData();

        $result = $nullBjSearchData->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testConfirm()
    {
        $nullNews = new MockNullBjSearchData();

        $result = $nullNews->confirm();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
