<?php
namespace Base\Sdk\ResourceCatalog\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Base\Sdk\Template\Model\Template;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class ErrorDataTest extends TestCase
{
    private $errorData;

    public function setUp()
    {
        $this->errorData = new MockErrorData();
    }

    public function tearDown()
    {
        unset($this->errorData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->errorData->getId());
        $this->assertInstanceOf('Base\Sdk\Template\Model\Template', $this->errorData->getTemplate());
        $this->assertInstanceOf('Base\Sdk\ResourceCatalog\Model\Task\Task', $this->errorData->getTask());
        $this->assertEquals(ErrorData::STATUS['NORMAL'], $this->errorData->getStatus());
        $this->assertEquals(0, $this->errorData->getCategory());
        $this->assertEquals(0, $this->errorData->getErrorType());
        $this->assertEquals(array(), $this->errorData->getItemsData());
        $this->assertEquals(array(), $this->errorData->getErrorReason());
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\ErrorDataRepository',
            $this->errorData->getRepository()
        );
    }

    //template 测试 -------------------------------------------------------- start
    /**
     * 设置 ErrorData setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $expected = new Template();

        $this->errorData->setTemplate($expected);
        $this->assertEquals($expected, $this->errorData->getTemplate());
    }

    /**
     * 设置 ErrorData setTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $this->errorData->setTemplate('template');
    }
    //template 测试 --------------------------------------------------------   end

    //itemsData 测试 -------------------------------------------------------- start
    /**
     * 设置 ErrorData setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $expected = array(1,2);

        $this->errorData->setItemsData($expected);
        $this->assertEquals($expected, $this->errorData->getItemsData());
    }

    /**
     * 设置 ErrorData setItemsData() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $this->errorData->setItemsData('string');
    }
    //itemsData 测试 --------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 ErrorData setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->errorData->setStatus($actual);
        $this->assertEquals($expected, $this->errorData->getStatus());
    }

    /**
     * 循环测试 ErrorData setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(ErrorData::STATUS['NORMAL'], ErrorData::STATUS['NORMAL']),
            array(ErrorData::STATUS['PROGRAM_EXCEPTION'], ErrorData::STATUS['PROGRAM_EXCEPTION']),
            array(ErrorData::STATUS['STORAGE_EXCEPTION'], ErrorData::STATUS['STORAGE_EXCEPTION']),
        );
    }

    /**
     * 设置 ErrorData setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->errorData->setStatus(['string']);
    }
    //status 测试 ------------------------------------------------------   end

    //errorReason 测试 -------------------------------------------------------- start
    /**
     * 设置 ErrorData setErrorReason() 正确的传参类型,期望传值正确
     */
    public function testSetErrorReasonCorrectType()
    {
        $expected = array(1,2);

        $this->errorData->setErrorReason($expected);
        $this->assertEquals($expected, $this->errorData->getErrorReason());
    }

    /**
     * 设置 ErrorData setErrorReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorReasonWrongType()
    {
        $this->errorData->setErrorReason('string');
    }
    //errorReason 测试 --------------------------------------------------------   end

    //category 测试 -------------------------------------------------------- start
    /**
     * 设置 ErrorData setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $expected = 1;

        $this->errorData->setCategory($expected);
        $this->assertEquals($expected, $this->errorData->getCategory());
    }

    /**
     * 设置 ErrorData setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->errorData->setCategory('string');
    }
    //category 测试 --------------------------------------------------------   end

    //errorType 测试 -------------------------------------------------------- start
    /**
     * 设置 ErrorData setErrorType() 正确的传参类型,期望传值正确
     */
    public function testSetErrorTypeCorrectType()
    {
        $expected = 1;

        $this->errorData->setErrorType($expected);
        $this->assertEquals($expected, $this->errorData->getErrorType());
    }

    /**
     * 设置 ErrorData setErrorType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorTypeWrongType()
    {
        $this->errorData->setErrorType('string');
    }
    //errorType 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\ErrorDataRepository',
            $this->errorData->getRepository()
        );
    }
}
