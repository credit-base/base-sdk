<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NullWbjSearchDataTest extends TestCase
{
    private $nullWbjSearchData;

    public function setUp()
    {
        $this->nullWbjSearchData = NullWbjSearchData::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullWbjSearchData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsWbjSearchData()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Model\WbjSearchData',
            $this->nullWbjSearchData
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullWbjSearchData
        );
    }

    public function testResourceNotExist()
    {
        $nullWbjSearchData = new MockNullWbjSearchData();

        $result = $nullWbjSearchData->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
