<?php
namespace Base\Sdk\ResourceCatalog\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\ResourceCatalog\Model\ItemsData;
use Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class BjSearchDataTest extends TestCase
{
    private $bjSearchData;

    public function setUp()
    {
        $this->bjSearchData = new MockBjSearchData();
    }

    public function tearDown()
    {
        unset($this->bjSearchData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf('Base\Sdk\Template\Model\BjTemplate', $this->bjSearchData->getTemplate());
        $this->assertInstanceOf('Base\Sdk\ResourceCatalog\Model\ItemsData', $this->bjSearchData->getItemsData());
        $this->assertEquals(BjSearchData::DATA_STATUS['CONFIRM'], $this->bjSearchData->getStatus());
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository',
            $this->bjSearchData->getRepository()
        );
    }

    //template 测试 -------------------------------------------------------- start
    /**
     * 设置 BjSearchData setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $expected = new BjTemplate();

        $this->bjSearchData->setTemplate($expected);
        $this->assertEquals($expected, $this->bjSearchData->getTemplate());
    }

    /**
     * 设置 BjSearchData setTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $this->bjSearchData->setTemplate('template');
    }
    //template 测试 --------------------------------------------------------   end

    //itemsData 测试 -------------------------------------------------------- start
    /**
     * 设置 BjSearchData setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $expected = new ItemsData();

        $this->bjSearchData->setItemsData($expected);
        $this->assertEquals($expected, $this->bjSearchData->getItemsData());
    }

    /**
     * 设置 BjSearchData setItemsData() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $this->bjSearchData->setItemsData('itemsData');
    }
    //itemsData 测试 --------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 BjSearchData setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->bjSearchData->setStatus($actual);
        $this->assertEquals($expected, $this->bjSearchData->getStatus());
    }

    /**
     * 循环测试 BjSearchData setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(BjSearchData::DATA_STATUS['CONFIRM'], BjSearchData::DATA_STATUS['CONFIRM']),
            array(BjSearchData::DATA_STATUS['ENABLED'], BjSearchData::DATA_STATUS['ENABLED']),
            array(BjSearchData::DATA_STATUS['DISABLED'], BjSearchData::DATA_STATUS['DISABLED']),
            array(BjSearchData::DATA_STATUS['DELETED'], BjSearchData::DATA_STATUS['DELETED']),
        );
    }

    /**
     * 设置 BjSearchData setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->bjSearchData->setStatus(['string']);
    }
    //status 测试 ------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository',
            $this->bjSearchData->getRepository()
        );
    }

    public function testGetIEnableAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository',
            $this->bjSearchData->getIEnableAbleAdapter()
        );
    }

    public function testGetIModifyStatusAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository',
            $this->bjSearchData->getIModifyStatusAbleAdapter()
        );
    }

    public function testDisableSuccess()
    {
        $this->bjSearchData = $this->getMockBuilder(MockBjSearchData::class)
            ->setMethods(['getIEnableAbleAdapter','isConfirmed'])->getMock();

        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->disable(Argument::exact($this->bjSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->bjSearchData->expects($this->exactly(1))
            ->method('getIEnableAbleAdapter')
            ->willReturn($repository->reveal());

        $this->bjSearchData->expects($this->exactly(1))
            ->method('isConfirmed')
            ->willReturn(true);

        $result = $this->bjSearchData->disable();
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $this->stub = $this->getMockBuilder(MockBjSearchData::class)
            ->setMethods(['getIEnableAbleAdapter','isConfirmed'])->getMock();
        $this->stub->expects($this->exactly(1))
            ->method('isConfirmed')
            ->willReturn(false);

        $result = $this->stub->disable();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testConfirmSuccess()
    {
        $this->bjSearchData = $this->getMockBuilder(MockBjSearchData::class)
            ->setMethods(['getRepository','isConfirm'])->getMock();

        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->confirm(Argument::exact($this->bjSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->bjSearchData->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->bjSearchData->setStatus(BjSearchData::DATA_STATUS['CONFIRM']);
        $this->bjSearchData->expects($this->exactly(1))
            ->method('isConfirm')
            ->willReturn(true);

        $result = $this->bjSearchData->confirm();
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $this->stub = $this->getMockBuilder(MockBjSearchData::class)
            ->setMethods(['getRepository','isConfirm'])->getMock();

        $this->stub->setStatus(BjSearchData::DATA_STATUS['ENABLED']);
        $this->stub->expects($this->exactly(1))
            ->method('isConfirm')
            ->willReturn(false);

        $result = $this->stub->confirm();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testDeletesSuccess()
    {
        $this->bjSearchData = $this->getMockBuilder(MockBjSearchData::class)
            ->setMethods(['getIModifyStatusAbleAdapter','isConfirm'])->getMock();

        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->deletes(Argument::exact($this->bjSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->bjSearchData->expects($this->exactly(1))
            ->method('getIModifyStatusAbleAdapter')
            ->willReturn($repository->reveal());

        $this->bjSearchData->setStatus(BjSearchData::DATA_STATUS['ENABLED']);
        $this->bjSearchData->expects($this->exactly(1))
            ->method('isConfirm')
            ->willReturn(true);

        $result = $this->bjSearchData->deletes();
        $this->assertTrue($result);
    }

    public function testDeletesFail()
    {
        $this->stub = $this->getMockBuilder(MockBjSearchData::class)
            ->setMethods(['getIModifyStatusAbleAdapter','isConfirm'])->getMock();

        $this->stub->setStatus(BjSearchData::DATA_STATUS['DISABLED']);
        $this->stub->expects($this->exactly(1))
            ->method('isConfirm')
            ->willReturn(false);

        $result = $this->stub->deletes();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testIsConfirmTrue()
    {
        $stub = new BjSearchData();
        $stub->setStatus(BjSearchData::DATA_STATUS['CONFIRM']);

        $result = $stub->isConfirm();
        $this->assertTrue($result);
    }

    public function testIsConfirmFalse()
    {
        $stub = new BjSearchData();
        $stub->setStatus(BjSearchData::DATA_STATUS['ENABLED']);

        $result = $stub->isConfirm();
        $this->assertFalse($result);
    }

    public function testIsConfirmedTrue()
    {
        $stub = new BjSearchData();
        $stub->setStatus(BjSearchData::DATA_STATUS['ENABLED']);

        $result = $stub->isConfirmed();
        $this->assertTrue($result);
    }

    public function testIsConfirmedFalse()
    {
        $stub = new BjSearchData();
        $stub->setStatus(BjSearchData::DATA_STATUS['CONFIRM']);

        $result = $stub->isConfirmed();
        $this->assertFalse($result);
    }
}
