<?php
namespace Base\Sdk\ResourceCatalog\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\ResourceCatalog\Model\ItemsData;
use Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class GbSearchDataTest extends TestCase
{
    private $gbSearchData;

    public function setUp()
    {
        $this->gbSearchData = new MockGbSearchData();
    }

    public function tearDown()
    {
        unset($this->gbSearchData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf('Base\Sdk\Template\Model\GbTemplate', $this->gbSearchData->getTemplate());
        $this->assertInstanceOf('Base\Sdk\ResourceCatalog\Model\ItemsData', $this->gbSearchData->getItemsData());
        $this->assertEquals(GbSearchData::DATA_STATUS['CONFIRM'], $this->gbSearchData->getStatus());
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository',
            $this->gbSearchData->getRepository()
        );
    }

    //template 测试 -------------------------------------------------------- start
    /**
     * 设置 gbSearchData setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $expected = new GbTemplate();

        $this->gbSearchData->setTemplate($expected);
        $this->assertEquals($expected, $this->gbSearchData->getTemplate());
    }

    /**
     * 设置 gbSearchData setTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $this->gbSearchData->setTemplate('template');
    }
    //template 测试 --------------------------------------------------------   end

    //itemsData 测试 -------------------------------------------------------- start
    /**
     * 设置 gbSearchData setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $expected = new ItemsData();

        $this->gbSearchData->setItemsData($expected);
        $this->assertEquals($expected, $this->gbSearchData->getItemsData());
    }

    /**
     * 设置 gbSearchData setItemsData() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $this->gbSearchData->setItemsData('itemsData');
    }
    //itemsData 测试 --------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 gbSearchData setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->gbSearchData->setStatus($actual);
        $this->assertEquals($expected, $this->gbSearchData->getStatus());
    }

    /**
     * 循环测试 gbSearchData setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(GbSearchData::DATA_STATUS['CONFIRM'], GbSearchData::DATA_STATUS['CONFIRM']),
            array(GbSearchData::DATA_STATUS['ENABLED'], GbSearchData::DATA_STATUS['ENABLED']),
            array(GbSearchData::DATA_STATUS['DISABLED'], GbSearchData::DATA_STATUS['DISABLED']),
            array(GbSearchData::DATA_STATUS['DELETED'], GbSearchData::DATA_STATUS['DELETED']),
        );
    }

    /**
     * 设置 gbSearchData setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->gbSearchData->setStatus(['string']);
    }
    //status 测试 ------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository',
            $this->gbSearchData->getRepository()
        );
    }

    public function testGetIEnableAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository',
            $this->gbSearchData->getIEnableAbleAdapter()
        );
    }

    public function testGetIModifyStatusAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository',
            $this->gbSearchData->getIModifyStatusAbleAdapter()
        );
    }

    public function testDisableSuccess()
    {
        $this->gbSearchData = $this->getMockBuilder(MockGbSearchData::class)
            ->setMethods(['getIEnableAbleAdapter','isConfirmed'])->getMock();

        $repository = $this->prophesize(GbSearchDataRepository::class);
        $repository->disable(Argument::exact($this->gbSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->gbSearchData->expects($this->exactly(1))
            ->method('getIEnableAbleAdapter')
            ->willReturn($repository->reveal());

        $this->gbSearchData->expects($this->exactly(1))
            ->method('isConfirmed')
            ->willReturn(true);

        $result = $this->gbSearchData->disable();
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $this->stub = $this->getMockBuilder(MockGbSearchData::class)
            ->setMethods(['getIEnableAbleAdapter','isConfirmed'])->getMock();
        $this->stub->expects($this->exactly(1))
            ->method('isConfirmed')
            ->willReturn(false);

        $result = $this->stub->disable();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }


    public function testConfirmSuccess()
    {
        $this->gbSearchData = $this->getMockBuilder(MockGbSearchData::class)
            ->setMethods(['getRepository','isConfirm'])->getMock();

        $repository = $this->prophesize(GbSearchDataRepository::class);
        $repository->confirm(Argument::exact($this->gbSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->gbSearchData->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->gbSearchData->expects($this->exactly(1))
            ->method('isConfirm')
            ->willReturn(true);

        $result = $this->gbSearchData->confirm();
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $this->stub = $this->getMockBuilder(MockGbSearchData::class)
            ->setMethods(['getRepository','isConfirm'])->getMock();
        $this->stub->expects($this->exactly(1))
            ->method('isConfirm')
            ->willReturn(false);

        $result = $this->stub->confirm();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testDeletesSuccess()
    {
        $this->gbSearchData = $this->getMockBuilder(MockGbSearchData::class)
            ->setMethods(['getIModifyStatusAbleAdapter','isConfirm'])->getMock();

        $repository = $this->prophesize(GbSearchDataRepository::class);
        $repository->deletes(Argument::exact($this->gbSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->gbSearchData->expects($this->exactly(1))
            ->method('getIModifyStatusAbleAdapter')
            ->willReturn($repository->reveal());

        $this->gbSearchData->expects($this->exactly(1))
            ->method('isConfirm')
            ->willReturn(true);

        $result = $this->gbSearchData->deletes();
        $this->assertTrue($result);
    }

    public function testDeletesFail()
    {
        $this->stub = $this->getMockBuilder(MockGbSearchData::class)
            ->setMethods(['getIModifyStatusAbleAdapter','isConfirm'])->getMock();
        $this->stub->expects($this->exactly(1))
            ->method('isConfirm')
            ->willReturn(false);

        $result = $this->stub->deletes();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }
}
