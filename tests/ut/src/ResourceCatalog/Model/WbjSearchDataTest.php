<?php
namespace Base\Sdk\ResourceCatalog\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\ResourceCatalog\Model\ItemsData;
use Base\Sdk\ResourceCatalog\Repository\WbjSearchDataRepository;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class WbjSearchDataTest extends TestCase
{
    private $wbjSearchData;

    public function setUp()
    {
        $this->wbjSearchData = new MockWbjSearchData();
    }

    public function tearDown()
    {
        unset($this->wbjSearchData);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf('Base\Sdk\Template\Model\WbjTemplate', $this->wbjSearchData->getTemplate());
        $this->assertInstanceOf('Base\Sdk\ResourceCatalog\Model\ItemsData', $this->wbjSearchData->getItemsData());
        $this->assertEquals(WbjSearchData::STATUS['ENABLED'], $this->wbjSearchData->getStatus());
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\WbjSearchDataRepository',
            $this->wbjSearchData->getRepository()
        );
    }

    //template 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjSearchData setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $expected = new WbjTemplate();

        $this->wbjSearchData->setTemplate($expected);
        $this->assertEquals($expected, $this->wbjSearchData->getTemplate());
    }

    /**
     * 设置 WbjSearchData setTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $this->wbjSearchData->setTemplate('template');
    }
    //template 测试 --------------------------------------------------------   end

    //itemsData 测试 -------------------------------------------------------- start
    /**
     * 设置 WbjSearchData setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $expected = new ItemsData();

        $this->wbjSearchData->setItemsData($expected);
        $this->assertEquals($expected, $this->wbjSearchData->getItemsData());
    }

    /**
     * 设置 WbjSearchData setItemsData() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $this->wbjSearchData->setItemsData('itemsData');
    }
    //itemsData 测试 --------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 WbjSearchData setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->wbjSearchData->setStatus($actual);
        $this->assertEquals($expected, $this->wbjSearchData->getStatus());
    }

    /**
     * 循环测试 WbjSearchData setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(WbjSearchData::STATUS['ENABLED'], WbjSearchData::STATUS['ENABLED']),
            array(WbjSearchData::STATUS['DISABLED'], WbjSearchData::STATUS['DISABLED']),
        );
    }

    /**
     * 设置 WbjSearchData setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->wbjSearchData->setStatus(['string']);
    }
    //status 测试 ------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\WbjSearchDataRepository',
            $this->wbjSearchData->getRepository()
        );
    }

    public function testGetIEnableAbleAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Repository\WbjSearchDataRepository',
            $this->wbjSearchData->getIEnableAbleAdapter()
        );
    }

    public function testDisableSuccess()
    {
        $this->wbjSearchData = $this->getMockBuilder(MockWbjSearchData::class)
            ->setMethods(['getIEnableAbleAdapter','isConfirm'])->getMock();

        $repository = $this->prophesize(WbjSearchDataRepository::class);
        $repository->disable(Argument::exact($this->wbjSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $this->wbjSearchData->expects($this->exactly(1))
            ->method('getIEnableAbleAdapter')
            ->willReturn($repository->reveal());

        $this->wbjSearchData->expects($this->exactly(1))
            ->method('isConfirm')
            ->willReturn(true);

        $result = $this->wbjSearchData->disable();
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $this->stub = $this->getMockBuilder(MockWbjSearchData::class)
            ->setMethods(['getIEnableAbleAdapter','isConfirm'])->getMock();
        $this->stub->expects($this->exactly(1))
            ->method('isConfirm')
            ->willReturn(false);

        $result = $this->stub->disable();
        $this->assertFalse($result);
        $this->assertEquals(STATUS_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }
}
