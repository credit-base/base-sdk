<?php
namespace Base\Sdk\ResourceCatalog\Model\Task;

use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;

/**
 * @SuppressWarnings(PHPMD)
 */
class TaskTest extends TestCase
{
    private $task;

    public function setUp()
    {
        $this->task = new Task();
    }

    public function tearDown()
    {
        unset($this->task);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf(
            'Base\Sdk\Crew\Model\Crew',
            $this->task->getCrew()
        );
        $this->assertInstanceOf(
            'Base\Sdk\UserGroup\Model\UserGroup',
            $this->task->getUserGroup()
        );
        $this->assertEquals(0, $this->task->getId());
        $this->assertEquals(Task::STATUS['PENDING'], $this->task->getStatus());
        $this->assertEquals(0, $this->task->getPid());
        $this->assertEquals(0, $this->task->getTotal());
        $this->assertEquals(0, $this->task->getSuccessNumber());
        $this->assertEquals(0, $this->task->getFailureNumber());
        $this->assertEquals(0, $this->task->getSourceCategory());
        $this->assertEquals(0, $this->task->getSourceTemplate());
        $this->assertEquals(0, $this->task->getTargetCategory());
        $this->assertEquals(0, $this->task->getTargetTemplate());
        $this->assertEquals(0, $this->task->getTargetRule());
        $this->assertEquals('', $this->task->getFileName());
    }

    //Crew 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTask setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $expected = new Crew();

        $this->task->setCrew($expected);
        $this->assertEquals($expected, $this->task->getCrew());
    }

    /**
     * 设置 BjTask setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->task->setCrew('Crew');
    }
    //Crew 测试 --------------------------------------------------------   end

    //sourceUnit 测试 -------------------------------------------------------- start
    /**
     * 设置 BjTask setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $expected = new UserGroup();

        $this->task->setUserGroup($expected);
        $this->assertEquals($expected, $this->task->getUserGroup());
    }

    /**
     * 设置 BjTask setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->task->setUserGroup('string');
    }
    //sourceUnit 测试 --------------------------------------------------------   end

    //pid 测试 ------------------------------------------------------ start
    /**
     * 设置 BjTask setPid() 正确的传参类型,期望传值正确
     */
    public function testSetPidCorrectType()
    {
        $this->task->setPid(1);
        $this->assertEquals(1, $this->task->getPid());
    }

    /**
     * 设置 BjTask setPid() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPidWrongType()
    {
        $this->task->setPid(['string']);
    }
    //pid 测试 ------------------------------------------------------   end

    //total 测试 ------------------------------------------------------ start
    /**
     * 设置 BjTask setTotal() 正确的传参类型,期望传值正确
     */
    public function testSetTotalCorrectType()
    {
        $this->task->setTotal(1);
        $this->assertEquals(1, $this->task->getTotal());
    }

    /**
     * 设置 BjTask setTotal() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalWrongType()
    {
        $this->task->setTotal(['string']);
    }
    //total 测试 ------------------------------------------------------   end

    //successNumber 测试 ------------------------------------------------------ start
    /**
     * 设置 BjTask setSuccessNumber() 正确的传参类型,期望传值正确
     */
    public function testSetSuccessNumberCorrectType()
    {
        $this->task->setSuccessNumber(1);
        $this->assertEquals(1, $this->task->getSuccessNumber());
    }

    /**
     * 设置 BjTask setSuccessNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSuccessNumberWrongType()
    {
        $this->task->setSuccessNumber(['string']);
    }
    //successNumber 测试 ------------------------------------------------------   end

    //failureNumber 测试 ------------------------------------------------------ start
    /**
     * 设置 BjTask setFailureNumber() 正确的传参类型,期望传值正确
     */
    public function testSetFailureNumberCorrectType()
    {
        $this->task->setFailureNumber(1);
        $this->assertEquals(1, $this->task->getFailureNumber());
    }

    /**
     * 设置 BjTask setFailureNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFailureNumberWrongType()
    {
        $this->task->setFailureNumber(['string']);
    }
    //failureNumber 测试 ------------------------------------------------------   end

    //sourceCategory 测试 ------------------------------------------------------ start
    /**
     * 设置 BjTask setSourceCategory() 正确的传参类型,期望传值正确
     */
    public function testSetSourceCategoryCorrectType()
    {
        $this->task->setSourceCategory(1);
        $this->assertEquals(1, $this->task->getSourceCategory());
    }

    /**
     * 设置 BjTask setSourceCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceCategoryWrongType()
    {
        $this->task->setSourceCategory(array());
    }
    //sourceCategory 测试 ------------------------------------------------------   end

    //sourceTemplate 测试 ------------------------------------------------------ start
    /**
     * 设置 BjTask setSourceTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetSourceTemplateCorrectType()
    {
        $this->task->setSourceTemplate(1);
        $this->assertEquals(1, $this->task->getSourceTemplate());
    }

    /**
     * 设置 BjTask setSourceTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceTemplateWrongType()
    {
        $this->task->setSourceTemplate([1]);
    }
    //sourceTemplate 测试 ------------------------------------------------------   end

    //targetCategory 测试 ------------------------------------------------------ start
    /**
     * 设置 BjTask setTargetCategory() 正确的传参类型,期望传值正确
     */
    public function testSetTargetCategoryCorrectType()
    {
        $this->task->setTargetCategory(1);
        $this->assertEquals(1, $this->task->getTargetCategory());
    }

    /**
     * 设置 BjTask setTargetCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetCategoryWrongType()
    {
        $this->task->setTargetCategory(['string']);
    }
    //targetCategory 测试 ------------------------------------------------------   end

    //targetTemplate 测试 ------------------------------------------------------ start
    /**
     * 设置 BjTask setTargetTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTargetTemplateCorrectType()
    {
        $this->task->setTargetTemplate(1);
        $this->assertEquals(1, $this->task->getTargetTemplate());
    }

    /**
     * 设置 BjTask setTargetTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetTemplateWrongType()
    {
        $this->task->setTargetTemplate(['string']);
    }
    //targetTemplate 测试 ------------------------------------------------------   end

    //targetRule 测试 ------------------------------------------------------ start
    /**
     * 设置 BjTask setTargetRule() 正确的传参类型,期望传值正确
     */
    public function testSetTargetRuleCorrectType()
    {
        $this->task->setTargetRule(1);
        $this->assertEquals(1, $this->task->getTargetRule());
    }

    /**
     * 设置 BjTask setTargetRule() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetRuleWrongType()
    {
        $this->task->setTargetRule(['string']);
    }
    //targetRule 测试 ------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 设置 BjTask setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->task->setStatus(1);
        $this->assertEquals(1, $this->task->getStatus());
    }

    /**
     * 设置 BjTask setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->task->setStatus(['string']);
    }
    //status 测试 ------------------------------------------------------   end

    //fileName 测试 ------------------------------------------------------ start
    /**
     * 设置 BjTask setFileName() 正确的传参类型,期望传值正确
     */
    public function testSetFileNameCorrectType()
    {
        $this->task->setFileName(1);
        $this->assertEquals(1, $this->task->getFileName());
    }

    /**
     * 设置 BjTask setFileName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFileNameWrongType()
    {
        $this->task->setFileName(['string']);
    }
    //fileName 测试 ------------------------------------------------------   end
}
