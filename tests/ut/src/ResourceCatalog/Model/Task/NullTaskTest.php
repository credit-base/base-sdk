<?php
namespace Base\Sdk\ResourceCatalog\Model\Task;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullTaskTest extends TestCase
{
    private $nullTask;

    public function setUp()
    {
        $this->nullTask = NullTask::getInstance();
    }

    public function tearDown()
    {
        unset($this->nullTask);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsTask()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Model\Task\Task',
            $this->nullTask
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->nullTask
        );
    }

    public function testResourceNotExist()
    {
        $nullTask = new MockNullTask();

        $result = $nullTask->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
