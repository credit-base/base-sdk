<?php
namespace Base\Sdk\ResourceCatalog\Adapter\ErrorData;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\ResourceCatalog\Model\NullErrorData;

class ErrorDataRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        Core::$container->set('crew', new Crew(1));
        $this->adapter = $this->getMockBuilder(ErrorDataRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends ErrorDataRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getErrorDataMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIErrorDataAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Adapter\ErrorData\IErrorDataAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('errorDatas', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'ERROR_DATA_LIST',
                ErrorDataRestfulAdapter::SCENARIOS['ERROR_DATA_LIST']
            ],
            [
                'ERROR_DATA_FETCH_ONE',
                ErrorDataRestfulAdapter::SCENARIOS['ERROR_DATA_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    public function testFetchOne()
    {
        $id = 1;

        $errorData = \Base\Sdk\ResourceCatalog\Utils\ErrorData\MockFactory::generateErrorData($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullErrorData::getInstance())
            ->willReturn($errorData);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($errorData, $result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [];
        
        $result = $this->childAdapter->getErrorDataMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
