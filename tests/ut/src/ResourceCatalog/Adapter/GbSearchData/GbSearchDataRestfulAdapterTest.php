<?php
namespace Base\Sdk\ResourceCatalog\Adapter\GbSearchData;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Model\NullGbSearchData;

/**
  * 屏蔽类中所有PMD警告
  * @SuppressWarnings(PHPMD)
  */
class GbSearchDataRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        Core::$container->set('crew', new Crew(1));
        $this->adapter = $this->getMockBuilder(GbSearchDataRestfulAdapter::class)
                           ->setMethods([
                               'getTranslator',
                               'translateToObject',
                               'objectToArray',
                               'getResource',
                               'fetchOneAction',
                               'post',
                               'get',
                               'isSuccess',
                               'patch',
                               'commonMapErrors'
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends GbSearchDataRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
            public function getGbSearchDataMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIGbSearchDataAdapter()
    {
        $this->assertInstanceOf(
            'Base\Sdk\ResourceCatalog\Adapter\GbSearchData\IGbSearchDataAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('gbSearchData', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'GB_SEARCH_DATA_LIST',
                GbSearchDataRestfulAdapter::SCENARIOS['GB_SEARCH_DATA_LIST']
            ],
            [
                'GB_SEARCH_DATA_FETCH_ONE',
                GbSearchDataRestfulAdapter::SCENARIOS['GB_SEARCH_DATA_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }


    public function testFetchOne()
    {
        $id = 1;

        $gbSearchData = \Base\Sdk\ResourceCatalog\Utils\GbSearchData\GbSearchDataMockFactory::generateGbSearchData($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullGbSearchData::getInstance())
            ->willReturn($gbSearchData);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($gbSearchData, $result);
    }

    private function success(GbSearchData $gbSearchData)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($gbSearchData);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGbSearchDataTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行confirm（）
     * 判断 result 是否为true
     */
    public function testConfirmSuccess()
    {
        $gbSearchData = \Base\Sdk\ResourceCatalog\Utils\GbSearchData\GbSearchDataMockFactory::generateGbSearchData(1);

        $this->adapter
            ->method('patch')
            ->with('/'.$gbSearchData->getId().'/confirm');

        $this->success($gbSearchData);
        $result = $this->adapter->confirm($gbSearchData);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGbSearchDataTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行confirm（）
     * 判断 result 是否为false
     */
    public function testConfirmFailure()
    {
        $gbSearchData = \Base\Sdk\ResourceCatalog\Utils\GbSearchData\GbSearchDataMockFactory::generateGbSearchData(1);

        $this->adapter
            ->method('patch')
            ->with('/'.$gbSearchData->getId().'/confirm');
        
        $this->failure($gbSearchData);
        $result = $this->adapter->confirm($gbSearchData);
        $this->assertFalse($result);
    }

    public function testGetMapErrors()
    {
        $commonMapErrors = [
            102 => [
                'status' => STATUS_CAN_NOT_MODIFY
            ],
            10 => RESOURCE_NOT_EXIST,
            100 => PARAMETER_IS_EMPTY,
            101 => PARAMETER_FORMAT_ERROR
        ];

        $this->adapter->expects($this->any())
        ->method('commonMapErrors')
        ->willReturn($commonMapErrors);

        $mapError = [];
        
        $result = $this->childAdapter->getGbSearchDataMapErrors();
        $this->assertEquals($mapError+$commonMapErrors, $result);
    }
}
