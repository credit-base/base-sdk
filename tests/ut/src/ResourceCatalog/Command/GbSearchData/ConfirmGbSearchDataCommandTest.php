<?php
namespace Base\Sdk\ResourceCatalog\Command\GbSearchData;

use PHPUnit\Framework\TestCase;

class ConfirmGbSearchDataCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new ConfirmGbSearchDataCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testCorrectInstanceImplementsCommand()
    {
        $this->assertInstanceof('Marmot\Interfaces\ICommand', $this->stub);
    }
}
