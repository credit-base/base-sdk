<?php
namespace Base\Sdk\Crew\Add;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Model\Department;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Crew\DetailDataTrait;

/**
 * @Feature:  我是平台管理员,当我需要新增科室时,在委办局管理下的科室管理中,新增对应的科室数据
 *           根据我归集到的科室数据新增,以便于我能更好的管理科室信息
 * @Scenario: 正常新增数据
 */

class SuccessTest extends TestCase
{
    use DetailDataTrait;

    private $crew;

    private $mock;

    public function setUp()
    {
        $this->crew = new Crew();
    }

    public function tearDown()
    {
        unset($this->crew);
    }

    /**
    * @Given: 我并未新增过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getCrewDetail();
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($this->getEmptyData())
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
       
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我调用添加函数,期待返回true
    */
    protected function add()
    {
        $this->crew = new Crew();
        $this->crew->setRealName("乔文");
     
        $userGroup = new UserGroup(1);
        $this->crew->setUserGroup($userGroup);
        $department = new Department(8);
        $this->crew->setDepartment($department);
        $this->crew->setCellphone("14599888888");
        $this->crew->setPassword("Admin123$");
        $this->crew->setCardId("610428199609206187");
        $this->crew->setCategory(3);
        $this->crew->setPurview([]);
      
        return  $this->crew->add();
    }
    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        
        $this->add();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/crews';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getCrewOperationRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        
        $this->assertEquals('乔文', $this->crew->getRealName());
        $this->assertEquals(Crew::STATUS['ENABLED'], $this->crew->getStatus());
        
        $this->assertEquals("610428199609206187", $this->crew->getCardId());
       
        $this->assertEquals(
            "14599888888",
            $this->crew->getCellphone()
        );
   
        $this->assertEquals([], $this->crew->getPurview());
        $this->assertEquals(
            3,
            $this->crew->getCategory()
        );

        $this->assertEquals(
            0,
            $this->crew->getStatusTime()
        );
        $this->assertEquals(
            1597801419,
            $this->crew->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->crew->getUpdateTime()
        );
    }
}
