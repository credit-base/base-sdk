<?php


namespace Base\Sdk\Crew;

trait DetailDataTrait
{
    protected function getCrewDetail($status = 0) : array
    {
        return [
            "meta" =>[],
            "data" => [
                "type" =>"crews",
                "id" =>"29",
                "attributes" => [
                    "realName" =>"乔文",
                    "cardId" =>"610428199609206187",
                    "userName" =>"14599888888",
                    "cellphone" =>"14599888888",
                    "category" =>3,
                    "purview" =>[],
                    "status" =>$status,
                    "createTime" =>1597801419,
                    "updateTime" =>1620435148,
                    "statusTime" =>1620435148
                ],
                "relationships" => [
                    "userGroup" => [
                        "data" => [
                            "type" =>"userGroups",
                            "id" =>"1"
                        ]
                    ],
                    "department" => [
                        "data" => [
                            "type" =>"departments",
                            "id" =>"8"
                        ]
                    ]
                ],
                "links" => [
                    "self" =>"api.base.qixinyun.com/crews/29"
                ]
            ],
            "included" =>[
                [
                    "type" =>"userGroups",
                    "id" =>"1",
                    "attributes" => [
                        "name" =>"发展和改革委员会",
                        "shortName" =>"发改委",
                        "status" =>0,
                        "createTime" =>1558345495,
                        "updateTime" =>1610539122,
                        "statusTime" =>0
                    ]
                ],
                [
                    "type" =>"departments",
                    "id" =>"8",
                    "attributes" => [
                        "name" =>"管理科办公室",
                        "status" =>0,
                        "createTime" =>1597799090,
                        "updateTime" =>1597799173,
                        "statusTime" =>0
                    ],
                    "relationships" => [
                        "userGroup" => [
                            "data" => [
                                "type" =>"userGroups",
                                "id" =>"1"
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getEmptyData() : array
    {
        return [
            "errors" => [
                [
                    "id" => "10",
                    "links" => [
                        "about" => ""
                    ],
                    "status" => "404",
                    "code" => "RESOURCE_NOT_EXIST",
                    "title" => "Resource not exists",
                    "detail" => "Server can not find resources",
                    "source" => [],
                    "meta" => []
                ]
            ]
        ];
    }

    protected function getCrewOperationRequest():array
    {
        return [
            "data" => [
                "type" =>"crews",
                "attributes" => [
                    "category" =>3,
                    "purview" =>[],
                    "cellphone" =>"14599888888",
                    "realName" =>"乔文",
                    "password"=>"Admin123$",
                    "cardId" =>"610428199609206187",
                ],
                "relationships" => [
                    "userGroup" => [
                        "data" => [
                            [
                                "type" =>"userGroups",
                                "id" =>1
                            ]
                        ]
                            
                    ],
                    "department" => [
                        "data" =>[
                            [
                                "type" =>"departments",
                                "id" =>8
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
