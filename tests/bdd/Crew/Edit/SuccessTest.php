<?php
namespace Base\Sdk\Crew\Edit;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Adapter\Crew\CrewRestfulAdapter;

use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Model\Department;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Crew\DetailDataTrait;

/**
 * @Feature: 我是平台管理员,当我需要编辑员工时,在委办局管理下的员工管理中,编辑对应的员工数据
 *           根据我归集到的员工数据编辑,以便于我能更好的管理员工信息
 * @Scenario: 正常编辑数据
 */

class SuccessTest extends TestCase
{
    use DetailDataTrait;

    private $editCrewObject;

    private $mock;

    public function setUp()
    {
        $this->editCrewObject = new Crew();
    }

    public function tearDown()
    {
        unset($this->editCrewObject);
    }

    /**
    * @Given: 我并未编辑过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getCrewDetail();

        $jsonData = json_encode($data);
        

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($this->getCrewDetail())
               )
            ]
        );
      
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要编辑的员工数据
    */
    protected function fetchCrew($id)
    {
        $adapter = new CrewRestfulAdapter();
        $this->editCrewObject = $adapter->fetchOne($id);
        
        return $this->editCrewObject;
    }

    /**
    * @When:当我调用编辑函数,期待编辑成功
    */
    protected function edit()
    {
        $this->editCrewObject->setRealName("乔文");
     
        $userGroup = new UserGroup(1);
        $this->editCrewObject->setUserGroup($userGroup);
        $department = new Department(8);
        $this->editCrewObject->setDepartment($department);
        $this->editCrewObject->setCardId("610428199609206187");
        $this->editCrewObject->setCategory(3);
        $this->editCrewObject->setPurview([]);
       
        return $this->editCrewObject->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchCrew($id);

        $this->edit();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/crews/29';

        $request = $this->mock->getLastRequest();

        $requestContents = $this->getCrewOperationRequest();
        unset($requestContents['data']['attributes']['cellphone']);
        unset($requestContents['data']['attributes']['password']);

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($requestContents), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('乔文', $this->editCrewObject->getRealName());
        $this->assertEquals(Crew::STATUS['ENABLED'], $this->editCrewObject->getStatus());
        
        $this->assertEquals("610428199609206187", $this->editCrewObject->getCardId());
       
        $this->assertEquals(
            "14599888888",
            $this->editCrewObject->getCellphone()
        );
   
        $this->assertEquals([], $this->editCrewObject->getPurview());
        $this->assertEquals(
            3,
            $this->editCrewObject->getCategory()
        );

        $this->assertEquals(
            1620435148,
            $this->editCrewObject->getStatusTime()
        );
        $this->assertEquals(
            1620435148,
            $this->editCrewObject->getUpdateTime()
        );
    }
}
