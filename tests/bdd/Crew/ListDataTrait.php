<?php


namespace Base\Sdk\Crew;

use Base\Sdk\Crew\Adapter\Crew\CrewRestfulAdapter;

trait ListDataTrait
{
    protected function getCrewList($filter = []) : array
    {
        $adapter = new CrewRestfulAdapter();

        list($count, $crewList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $crewList;
    }

    /**
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    public function getCrewListData() : array
    {
        return [
            "meta" => [
                "count" =>53,
                "links" => [
                    "first" =>1,
                    "last" =>27,
                    "prev" =>null,
                    "next" =>2
                ]
            ],
            "links" => [
                "first" =>"api.base.qixinyun.com/crews/?include=userGroup,department&page[number]=1&page[size]=2",
                "last" =>"api.base.qixinyun.com/crews/?include=userGroup,department&page[number]=27&page[size]=2",
                "prev" =>null,
                "next" =>"api.base.qixinyun.com/crews/?include=userGroup,department&page[number]=2&page[size]=2"
            ],
            "data" =>[
                [
                    "type" =>"crews",
                    "id" =>"414",
                    "attributes" => [
                        "realName" =>"测试信用刊物审核",
                        "cardId" =>"",
                        "userName" =>"12345678910",
                        "cellphone" =>"12345678910",
                        "category" =>4,
                        "purview" =>[
                            "15"
                        ],
                        "status" =>0,
                        "createTime" =>1622453869,
                        "updateTime" =>1622453973,
                        "statusTime" =>0
                    ],
                    "relationships" => [
                        "userGroup" => [
                            "data" => [
                                "type" =>"userGroups",
                                "id" =>"1"
                            ]
                        ],
                        "department" => [
                            "data" => [
                                "type" =>"departments",
                                "id" =>"0"
                            ]
                        ]
                    ],
                    "links" => [
                        "self" =>"api.base.qixinyun.com/crews/414"
                    ]
                ],
                [
                    "type" =>"crews",
                    "id" =>"421",
                    "attributes" => [
                        "realName" =>"测试信用刊物管理",
                        "cardId" =>"",
                        "userName" =>"12345678911",
                        "cellphone" =>"12345678911",
                        "category" =>4,
                        "purview" =>[
                            "14"
                        ],
                        "status" =>0,
                        "createTime" =>1622454219,
                        "updateTime" =>1622454219,
                        "statusTime" =>0
                    ],
                    "relationships" => [
                        "userGroup" => [
                            "data" => [
                                "type" =>"userGroups",
                                "id" =>"1"
                            ]
                        ],
                        "department" => [
                            "data" => [
                                "type" =>"departments",
                                "id" =>"0"
                            ]
                        ]
                    ],
                    "links" => [
                        "self" =>"api.base.qixinyun.com/crews/421"
                    ]
                ]
            ],
            "included" =>[
                [
                    "type" =>"userGroups",
                    "id" =>"1",
                    "attributes" => [
                        "name" =>"发展和改革委员会",
                        "shortName" =>"发改委",
                        "status" =>0,
                        "createTime" =>1558345495,
                        "updateTime" =>1610539122,
                        "statusTime" =>0
                    ]
                ],
                [
                    "type" =>"departments",
                    "id" =>"0",
                    "attributes" => [
                        "name" =>"",
                        "status" =>0,
                        "createTime" =>1634201708,
                        "updateTime" =>1634201708,
                        "statusTime" =>0
                    ],
                    "relationships" => [
                        "userGroup" => [
                            "data" => [
                                "type" =>"userGroups",
                                "id" =>"0"
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
