<?php
namespace Base\Sdk\Crew\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Adapter\Crew\CrewRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Crew\DetailDataTrait;

/**
 * @Feature: 我是委办局工作人员,当我需要将禁用掉的员工启用时,在员工子系统下的本级数据管理,启用对应的员工
 *           根据我核实需要启用的数据启用,以便于我实现平台员工的呈现
 * @Scenario: 启用员工
 */

class EnableTest extends TestCase
{
    use DetailDataTrait;

    private $enableCrewObject;

    private $mock;

    public function setUp()
    {
        $this->enableCrewObject = new Crew();
    }

    public function tearDown()
    {
        unset($this->enableCrewObject);
    }

    /**
    * @Given: 存在需要启用的员工
    */
    protected function prepareData()
    {
        $disableData = $this->getCrewDetail(Crew::STATUS['DISABLED']);
        $jsonData = json_encode($disableData);
        
        $enableData = $this->getCrewDetail(Crew::STATUS['ENABLED']);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($enableData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要启用的员工
    */
    protected function fetchCrew($id)
    {
        $adapter = new CrewRestfulAdapter();
        $this->enableCrewObject = $adapter->fetchOne($id);
        
        return $this->enableCrewObject;
    }

    /**
    * @And:当我调用启用函数,期待启用成功
    */
    protected function enable()
    {
        return $this->enableCrewObject->enable();
    }
    /**
     * @Then  我可以查到该条员工已被启用
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchCrew($id);
        $this->enable();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/crews/29/enable';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals('[]', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Crew::STATUS['ENABLED'], $this->enableCrewObject->getStatus());

        $this->assertEquals(
            1620435148,
            $this->enableCrewObject->getStatusTime()
        );
        $this->assertEquals(
            1620435148,
            $this->enableCrewObject->getUpdateTime()
        );
    }
}
