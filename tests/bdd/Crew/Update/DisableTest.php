<?php
namespace Base\Sdk\Crew\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Adapter\Crew\CrewRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Crew\DetailDataTrait;

/**
 * @Feature: 我是委办局工作人员,当我需要禁用员工数据时,在员工数据子系统下的本级数据管理,禁用对应的员工数据
 *           根据我核实需要禁用的数据禁用,以便于我可以更好的管理平台
 * @Scenario: 禁用员工
 */

class DisableTest extends TestCase
{
    use DetailDataTrait;

    private $crew;

    private $mock;

    public function setUp()
    {
        $this->crew = new Crew();
    }

    public function tearDown()
    {
        unset($this->crew);
    }

    /**
    * @Given: 存在需要禁用的员工
    */
    protected function prepareData()
    {
        $enableData = $this->getCrewDetail(Crew::STATUS['ENABLED']);
        $jsonData = json_encode($enableData);
        
        $disableData = $this->getCrewDetail(Crew::STATUS['DISABLED']);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($disableData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要禁用的员工
    */
    protected function fetchCrew($id)
    {
        $adapter = new CrewRestfulAdapter();
        $this->crew = $adapter->fetchOne($id);
        
        return $this->crew;
    }

    /**
    * @And:当我调用禁用函数,期待禁用成功
    */
    protected function disable()
    {
        return  $this->crew->disable();
    }
    /**
     * @Then  我可以查到该条员工已被禁用
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchCrew($id);
        $this->disable();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/crews/29/disable';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals('[]', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Crew::STATUS['DISABLED'], $this->crew->getStatus());

        $this->assertEquals(
            1620435148,
            $this->crew->getStatusTime()
        );
        $this->assertEquals(
            1620435148,
            $this->crew->getUpdateTime()
        );
    }
}
