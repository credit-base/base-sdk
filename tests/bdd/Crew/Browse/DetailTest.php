<?php
namespace Base\Sdk\Crew\Browse;

use Base\Sdk\Crew\Adapter\Crew\CrewRestfulAdapter;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\DetailDataTrait;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DetailTest extends TestCase
{
    use  DetailDataTrait;

    private $crew;

    private $mock;

    public function setUp()
    {
        $this->crew = new Crew();
    }

    public function tearDown()
    {
        unset($this->crew);
    }

    /**
     * @Given: 存在一条用户数据
     */
    protected function prepareData()
    {
        $data = $this->getCrewDetail();

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
                new Response(
                    200,
                    ['Content-Type' => 'application/vnd.api+json'],
                    $jsonData
                ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看该条用户数据详情时
     */
    protected function fetchCrew($id)
    {

        $adapter = new CrewRestfulAdapter();

        $this->crew = $adapter->fetchOne($id);

        return $this->crew;
    }

    /**
     * @Then 我可以看见用户详情, 名字为发展和该和委员会, 简称为发改委, 创建时间, 更新时间.
     */
    public function testValidate()
    {
        $id = 29;
        $this->prepareData();
        $this->fetchCrew($id);

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'crews/29';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('14599888888', $this->crew->getUserName());
        $this->assertEquals('乔文', $this->crew->getRealName());
        $this->assertEquals(Crew::STATUS['ENABLED'], $this->crew->getStatus());
        $this->assertEquals(1620435148, $this->crew->getStatusTime());
        $this->assertEquals(1597801419, $this->crew->getCreateTime());
        $this->assertEquals(1620435148, $this->crew->getUpdateTime());
    }
}
