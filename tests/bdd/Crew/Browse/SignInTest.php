<?php
namespace Base\Sdk\Crew\Browse;

use Base\Sdk\Crew\DetailDataTrait;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Model\Guest;
use Base\Sdk\Member\Model\Member;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class SignInTest extends TestCase
{
    use DetailDataTrait;

    private $crew;

    private $guest;

    private $mock;

    public function setUp()
    {
        $this->crew = new Crew();
        $this->guest = new Guest();
    }

    public function tearDown()
    {
        unset($this->crew);
        unset($this->guest);
    }

    /**
     * @todo 没有实现登录成功
     */
    protected function prepareData()
    {
//        $data = $this->getCrewDetail();
//        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], json_encode($this->getEmptyData())),
//            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
//            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
//            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    protected function signIn()
    {
        $this->guest = new Guest();
        $this->guest->setUserName('18800000000');
        $this->guest->setPassword('Admin123$');

        return $this->guest->signIn();
    }

    public function testValidate()
    {
        $this->prepareData();

        $this->signIn();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/crews/signIn';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getGuestSignInRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('18800000000', $this->guest->getUserName());
        $this->assertEquals(0, $this->guest->getStatusTime());
        $this->assertEquals(0, $this->guest->getCreateTime());
        $this->assertEquals(0, $this->guest->getUpdateTime());
    }

    protected function getGuestSignInRequest()
    {
        return array(
            "data"=>array(
                "type"=>"crews",
                "attributes"=>array(
                    "userName"=>'18800000000',
                    "password"=>'Admin123$'
                )
            )
        );
    }
}
