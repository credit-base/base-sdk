<?php
namespace Base\Sdk\Interaction;

trait ComplaintArrayDataTrait
{
    use ArrayDataTrait;

    protected function getComplaintDetailData(
        string $type,
        int $acceptStatus = 0
    ) {
        $data = $this->getDetailData($type, $acceptStatus);
        $data['data']['attributes']['subject'] = '被投诉主体';

        unset($data['data']['attributes']['certificates']);

        return $data;
    }

    protected function getComplaintListData($type)
    {
        $data = $this->getListData($type);
        $data['data'][0]['attributes']['subject'] = '被投诉主体';

        unset($data['data'][0]['attributes']['certificates']);

        return $data;
    }

    protected function getComplaintAddRequestData(string $type)
    {
        $data = $this->getAddRequestData($type);
        unset($data['data']['attributes']['certificates']);
        unset($data['data']['attributes']['contact']);
        unset($data['data']['attributes']['content']);

        $data['data']['attributes']['subject'] = '被投诉主体';
        $data['data']['attributes']['contact'] = '联系方式';
        $data['data']['attributes']['content'] = $this->getContent();

        return $data;
    }
}
