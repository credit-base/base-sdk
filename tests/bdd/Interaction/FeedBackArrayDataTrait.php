<?php
namespace Base\Sdk\Interaction;

trait FeedBackArrayDataTrait
{
    use ArrayDataTrait;

    protected function getFeedBackDetailData(
        string $type,
        int $acceptStatus = 0
    ) {
        $data = $this->getDetailData($type, $acceptStatus);

        unset($data['data']['attributes']['name']);
        unset($data['data']['attributes']['type']);
        unset($data['data']['attributes']['contact']);
        unset($data['data']['attributes']['identify']);
        unset($data['data']['attributes']['images']);
        unset($data['data']['attributes']['certificates']);

        return $data;
    }

    protected function getFeedBackListData($type)
    {
        $data = $this->getListData($type);

        unset($data['data'][0]['attributes']['name']);
        unset($data['data'][0]['attributes']['type']);
        unset($data['data'][0]['attributes']['contact']);
        unset($data['data'][0]['attributes']['identify']);
        unset($data['data'][0]['attributes']['images']);
        unset($data['data'][0]['attributes']['certificates']);

        return $data;
    }

    protected function getFeedBackAddRequestData(string $type)
    {
        $data = $this->getAddRequestData($type);

        unset($data['data']['attributes']['name']);
        unset($data['data']['attributes']['type']);
        unset($data['data']['attributes']['contact']);
        unset($data['data']['attributes']['identify']);
        unset($data['data']['attributes']['images']);
        unset($data['data']['attributes']['certificates']);

        return $data;
    }
}
