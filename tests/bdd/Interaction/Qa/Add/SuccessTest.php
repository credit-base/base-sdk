<?php
namespace Base\Sdk\Interaction\Qa\Add;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Qa;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\UserGroup\Model\UserGroup;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是门户网用户,当我需要想要对某个信用数据进行投诉时,在信用公示栏目-信用反馈中,新增一个信用问答数据
 *           通过新增信用问答界面，并根据我所提交的投诉信息数据进行新增,以便于我维护信用问答信息
 * @Scenario: 新增信用问答
 */

class SuccessTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $qaObject;

    private $mock;

    public function setUp()
    {
        $this->qaObject = new Qa();
    }

    public function tearDown()
    {
        unset($this->qaObject);
    }

    /**
    * @Given: 我并未新增过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("qas");
        
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;
        $data['data']['attributes']['createTime'] = 1635498202;

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
       
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我调用添加函数,期待返回true
    */
    protected function add()
    {
        $addData = $this->getAttributesData();
        $this->qaObject = new Qa();
        $this->qaObject->setTitle($addData['title']);
     
        $acceptUserGroup = new UserGroup(1);
        $this->qaObject->setAcceptUserGroup($acceptUserGroup);
        $this->qaObject->setContent($addData['content']);
        
        $member = new Member(1);
        $this->qaObject->setMember($member);
      
        return  $this->qaObject->add();
    }
    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        
        $this->add();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/qas';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getFeedBackAddRequestData("qas")), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getFeedBackDetailData("qas")['data'];

        $this->assertEquals($data['attributes']['title'], $this->qaObject->getTitle());
        $this->assertEquals($data['attributes']['content'], $this->qaObject->getContent());
        
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->qaObject->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->qaObject->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(Qa::STATUS['NORMAL'], $this->qaObject->getStatus());

        $this->assertEquals(
            0,
            $this->qaObject->getStatusTime()
        );
        $this->assertEquals(
            1635498202,
            $this->qaObject->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->qaObject->getUpdateTime()
        );
    }
}
