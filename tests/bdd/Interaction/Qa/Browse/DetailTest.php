<?php
namespace Base\Sdk\Interaction\Qa\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Adapter\Qa\QaRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答管理权限,在信用问答模块中
 *           查看前台用户提交的已审核通过的信用问答信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用问答信息
 * @Scenario: 查看信用问答数据详情
 */

class DetailTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $qaObject;

    private $mock;

    public function setUp()
    {
        $this->qaObject = new Qa();
    }

    public function tearDown()
    {
        unset($this->qaObject);
    }

    /**
    * @Given: 存在一条信用问答数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("qas");

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条信用问答数据详情时
    */
    protected function fetchQa($id)
    {

        $adapter = new QaRestfulAdapter();

        $this->qaObject = $adapter->fetchOne($id);
      
        return $this->qaObject;
    }

    /**
     * @Then  我可以看见该条信用问答数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchQa($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'qas/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getFeedBackDetailData("qas")['data'];

        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->qaObject->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->qaObject->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(
            $data['attributes']['title'],
            $this->qaObject->getTitle()
        );
        $this->assertEquals(
            $data['attributes']['content'],
            $this->qaObject->getContent()
        );
       
        $this->assertEquals(Qa::STATUS['NORMAL'], $this->qaObject->getStatus());

        $this->assertEquals(
            0,
            $this->qaObject->getStatusTime()
        );
        $this->assertEquals(
            0,
            $this->qaObject->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->qaObject->getUpdateTime()
        );
    }
}
