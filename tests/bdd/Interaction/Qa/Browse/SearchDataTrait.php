<?php
namespace Base\Sdk\Interaction\Qa\Browse;

use Base\Sdk\Interaction\Adapter\Qa\QaRestfulAdapter;

trait SearchDataTrait
{
    protected function getQaList(array $filter = [])
    {
        $adapter = new QaRestfulAdapter();

        list($count, $qaList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
   
        return $qaList;
    }
}
