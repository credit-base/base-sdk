<?php
namespace Base\Sdk\Interaction\Qa\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Adapter\Qa\QaRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是门户网用户,我想要撤销我之前提交上去的信用问答时,在用户中心-信用问答中,我可以管理我提交的信用问答数据
 *           可以撤销受理情况为未受理状态的信用问答,以便于我维护我的信用问答信息
 * @Scenario: 撤销
 */

class RevokeTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $revokeQaObject;

    private $mock;

    public function setUp()
    {
        $this->revokeQaObject = new Qa();
    }

    public function tearDown()
    {
        unset($this->revokeQaObject);
    }

    /**
    * @Given: 存在需要撤销的信用问答
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("qas");
        $jsonData = json_encode($data);
        
        $revokeData = $this->getFeedBackDetailData("qas");
        $revokeData['data']['attributes']['status'] = Qa::STATUS['REVOKED'];

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($revokeData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要撤销的信用问答
    */
    protected function fetchQa($id)
    {
        $adapter = new QaRestfulAdapter();
        $this->revokeQaObject = $adapter->fetchOne($id);
        
        return $this->revokeQaObject;
    }

    /**
    * @And:当我调用撤销函数,期待撤销成功
    */
    protected function revoke()
    {
        return $this->revokeQaObject->revoke();
    }
    /**
     * @Then  我可以查到该条信用问答已被撤销
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchQa($id);
        $this->revoke();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/qas/1/revoke';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals('[]', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Qa::STATUS['REVOKED'], $this->revokeQaObject->getStatus());
    }
}
