<?php
namespace Base\Sdk\Interaction\Qa\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Adapter\Qa\QaRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答管理权限,当我需要想要对某个待审核的信用问答数据进行公示/取消公示时,
 *           在业务管理-信用问答表中,对已经审核通过的信用问答进行公示或取消公示的操作,以便于我维护信用问答信息
 * @Scenario: 公示信用问答
 */

class PublishTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $qaObject;

    private $mock;

    public function setUp()
    {
        $this->qaObject = new Qa();
    }

    public function tearDown()
    {
        unset($this->qaObject);
    }

    /**
    * @Given: 存在需要公示的信用问答
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("qas", Qa::ACCEPT_STATUS['COMPLETE']);
        $jsonData = json_encode($data);
        
        $publishData = $this->getFeedBackDetailData("qas", Qa::ACCEPT_STATUS['COMPLETE']);
        $publishData['data']['attributes']['status'] = Qa::STATUS['PUBLISH'];

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($publishData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要公示的信用问答
    */
    protected function fetchQa($id)
    {
        $adapter = new QaRestfulAdapter();
        $this->qaObject = $adapter->fetchOne($id);
        
        return $this->qaObject;
    }

    /**
    * @And:当我调用公示函数,期待公示成功
    */
    protected function publish()
    {
        return $this->qaObject->publish();
    }
    /**
     * @Then  我可以查到该条信用问答已被公示
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchQa($id);
        $this->publish();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/qas/1/publish';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals('[]', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            Qa::ACCEPT_STATUS['COMPLETE'],
            $this->qaObject->getAcceptStatus()
        );

        $this->assertEquals(Qa::STATUS['PUBLISH'], $this->qaObject->getStatus());
    }
}
