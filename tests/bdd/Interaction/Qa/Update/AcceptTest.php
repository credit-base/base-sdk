<?php
namespace Base\Sdk\Interaction\Qa\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\Interaction\Adapter\Qa\QaRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答管理权限,当我需要想要对某个信用问答数据进行受理处理时,在业务管理-信用问答中
 *           对前台用户提交上来的信用问答进行受理处理，并提交我填写的受理依据,以便于我维护信用问答信息
 * @Scenario: 受理信用问答
 */

class AcceptTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $qaObject;

    private $mock;

    public function setUp()
    {
        $this->qaObject = new Qa();
    }

    public function tearDown()
    {
        unset($this->qaObject);
    }

    /**
    * @Given: 我并未受理过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("qas");

        $jsonData = json_encode($data);
        
        $acceptData = $this->getFeedBackDetailData("qas", Qa::ACCEPT_STATUS['ACCEPTING']);
        $acceptData['data']['attributes']['statusTime'] = 1621997357;
        $acceptData['data']['attributes']['updateTime'] = 1621997357;
  
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($acceptData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要受理的信用问答数据
    */
    protected function fetchQa($id)
    {
        $adapter = new QaRestfulAdapter();
        $this->qaObject = $adapter->fetchOne($id);
        
        return $this->qaObject;
    }

    /**
    * @And:当我调用受理函数,期待返回true
    */
    protected function accept()
    {
        $reply = $this->qaObject->getReply();
        $reply->setContent('予以受理');
        $reply->setImages($this->getImages());
        $reply->setAdmissibility(1);

        $crew = new Crew(1);
        $reply->setCrew($crew);
       
        $this->qaObject->setReply($reply);
        
        return  $this->qaObject->accept();
    }
    /**
     * @Then  可以查到受理的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchQa($id);
        $this->accept();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/qas/1/accept';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getReplyData('qas')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            Qa::ACCEPT_STATUS['ACCEPTING'],
            $this->qaObject->getAcceptStatus()
        );
       
        $this->assertEquals(
            1,
            $this->qaObject->getReply()->getAdmissibility()
        );

        $this->assertEquals(
            '予以受理',
            $this->qaObject->getReply()->getContent()
        );

        $this->assertEquals(
            $this->getImages(),
            $this->qaObject->getReply()->getImages()
        );

        $this->assertEquals(
            1621997357,
            $this->qaObject->getStatusTime()
        );
        $this->assertEquals(
            1621997357,
            $this->qaObject->getUpdateTime()
        );
    }
}
