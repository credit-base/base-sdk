<?php
namespace Base\Sdk\Interaction\UnAuditComplaint\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditComplaint;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature:  我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉管理权限,在信用投诉模块中
 *           查看前台用户提交的待审核和已驳回的信用投诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的待审核和已驳回的信用投诉信息
 * @Scenario: 查看查看信用投诉数据列表列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ComplaintArrayDataTrait;

    private $unAuditComplaint;

    private $mock;

    public function setUp()
    {
        $this->unAuditComplaint = new UnAuditComplaint();
    }

    public function tearDown()
    {
        unset($this->unAuditComplaint);
    }

    /**
    * @Given: 存在查看信用投诉数据审核列表数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintListData('unAuditedComplaints');
        $data['data'][0]['attributes']['applyStatus'] = UnAuditComplaint::APPLY_STATUS['PENDING'];

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看查看信用投诉审核数据列表时
     */
    public function fetchUnAuditComplaintList()
    {
        $filter = [];
        $list =$this->getUnAuditComplaintList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见查看信用投诉审核数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditComplaintList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedComplaints';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $unAuditComplaintList = $this->fetchUnAuditComplaintList();

        $unAuditComplaintArray = $this->getComplaintListData('unAuditedComplaints')['data'];
        
        foreach ($unAuditComplaintList as $unAuditComplaint) {
            foreach ($unAuditComplaintArray as $item) {
                if ($unAuditComplaint->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['title'], $unAuditComplaint->getTitle());
                    $this->assertEquals($item['attributes']['content'], $unAuditComplaint->getContent());
                    $this->assertEquals(
                        $item['attributes']['type'],
                        $unAuditComplaint->getType()
                    );
                    $this->assertEquals(
                        $item['attributes']['contact'],
                        $unAuditComplaint->getContact()
                    );
                    $this->assertEquals(
                        $item['attributes']['identify'],
                        $unAuditComplaint->getIdentify()
                    );

                    $this->assertEquals(
                        $item['attributes']['images'],
                        $unAuditComplaint->getImages()
                    );
            
                    $this->assertEquals(
                        $item['attributes']['subject'],
                        $unAuditComplaint->getSubject()
                    );

                    $this->assertEquals($item['attributes']['status'], $unAuditComplaint->getStatus());

                    $this->assertEquals(
                        UnAuditComplaint::APPLY_STATUS['PENDING'],
                        $unAuditComplaint->getApplyStatus()
                    );
        
                    $this->assertEquals(
                        $item['attributes']['acceptStatus'],
                        $unAuditComplaint->getAcceptStatus()
                    );

                    $this->assertEquals(
                        $item['relationships']['acceptUserGroup']['data']['id'],
                        $unAuditComplaint->getAcceptUserGroup()->getId()
                    );

                    $this->assertEquals(
                        $item['relationships']['member']['data']['id'],
                        $unAuditComplaint->getMember()->getId()
                    );

                    $this->assertEquals(
                        0,
                        $unAuditComplaint->getStatusTime()
                    );
                    $this->assertEquals(
                        0,
                        $unAuditComplaint->getCreateTime()
                    );
                    $this->assertEquals(
                        0,
                        $unAuditComplaint->getUpdateTime()
                    );
                }
            }
        }
    }
}
