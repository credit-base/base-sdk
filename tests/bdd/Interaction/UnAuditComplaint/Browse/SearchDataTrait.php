<?php
namespace Base\Sdk\Interaction\UnAuditComplaint\Browse;

use Base\Sdk\Interaction\Adapter\UnAuditComplaint\UnAuditComplaintRestfulAdapter;

trait SearchDataTrait
{
    protected function getUnAuditComplaintList(array $filter = [])
    {
        $adapter = new UnAuditComplaintRestfulAdapter();

        list($count, $appealList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
   
        return $appealList;
    }
}
