<?php
namespace Base\Sdk\Interaction\UnAuditComplaint\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditComplaint;
use Base\Sdk\Interaction\Adapter\UnAuditComplaint\UnAuditComplaintRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉审核权限,在信用投诉模块中
 *           查看前台用户提交的信用投诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用投诉信息
 * @Scenario: 查看信用投诉审核数据详情
 */

class DetailTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $unAuditComplaint;

    private $mock;

    public function setUp()
    {
        $this->unAuditComplaint = new UnAuditComplaint();
    }

    public function tearDown()
    {
        unset($this->unAuditComplaint);
    }

    /**
    * @Given: 存在一条信用投诉审核数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("unAuditedComplaints", UnAuditComplaint::ACCEPT_STATUS['ACCEPTING']);
        $data['data']['attributes']['applyStatus'] = UnAuditComplaint::APPLY_STATUS['PENDING'];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条信用投诉审核数据详情时
    */
    protected function fetchUnAuditComplaint($id)
    {

        $adapter = new UnAuditComplaintRestfulAdapter();

        $this->unAuditComplaint = $adapter->fetchOne($id);
      
        return $this->unAuditComplaint;
    }

    /**
     * @Then 我可以看见该条信用投诉审核数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditComplaint($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedComplaints/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getComplaintDetailData("unAuditedComplaints")['data'];

        $this->assertEquals($data['attributes']['name'], $this->unAuditComplaint->getName());
        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->unAuditComplaint->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->unAuditComplaint->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(
            $data['attributes']['title'],
            $this->unAuditComplaint->getTitle()
        );
        $this->assertEquals(
            $data['attributes']['content'],
            $this->unAuditComplaint->getContent()
        );
        $this->assertEquals(
            $data['attributes']['type'],
            $this->unAuditComplaint->getType()
        );
        $this->assertEquals(
            $data['attributes']['contact'],
            $this->unAuditComplaint->getContact()
        );
        $this->assertEquals(
            $data['attributes']['identify'],
            $this->unAuditComplaint->getIdentify()
        );

        $this->assertEquals(
            $data['attributes']['images'],
            $this->unAuditComplaint->getImages()
        );
   
        $this->assertEquals(
            $data['attributes']['subject'],
            $this->unAuditComplaint->getSubject()
        );
       
        $this->assertEquals(
            UnAuditComplaint::STATUS['NORMAL'],
            $this->unAuditComplaint->getStatus()
        );
        $this->assertEquals(
            UnAuditComplaint::APPLY_STATUS['PENDING'],
            $this->unAuditComplaint->getApplyStatus()
        );

        $this->assertEquals(
            0,
            $this->unAuditComplaint->getStatusTime()
        );
        $this->assertEquals(
            0,
            $this->unAuditComplaint->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->unAuditComplaint->getUpdateTime()
        );
    }
}
