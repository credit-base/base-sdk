<?php
namespace Base\Sdk\Interaction\UnAuditComplaint\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditComplaint;
use Base\Sdk\Interaction\Adapter\UnAuditComplaint\UnAuditComplaintRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉审核权限,在信用投诉模块中
 *           查看前台用户提交的信用投诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用投诉信息
 * @Scenario: 查看信用投诉数据审核列表-异常不存在数据
 */

class FailEmptyTest extends TestCase
{
    private $unAuditUnAuditComplaint;

    private $mock;

    public function setUp()
    {
        $this->unAuditUnAuditComplaint = new UnAuditComplaint();
    }

    public function tearDown()
    {
        unset($this->unAuditUnAuditComplaint);
    }

    /**
    * @Given: 不存在信用投诉审核数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看信用投诉审核列表时
     */
    public function fetchUnAuditComplaintList()
    {
        $adapter = new UnAuditComplaintRestfulAdapter();

        list($count, $this->unAuditUnAuditComplaint) = $adapter
        ->search();
        unset($count);

        return $this->unAuditUnAuditComplaint;
    }

    /**
     * @Then: 获取不到审核数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchUnAuditComplaintList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedComplaints';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->unAuditUnAuditComplaint);
    }
}
