<?php
namespace Base\Sdk\Interaction\UnAuditComplaint\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditComplaint;
use Base\Sdk\Interaction\Adapter\UnAuditComplaint\UnAuditComplaintRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉审核权限,当我需要想要对某个待审核的信用投诉数据进行审核时,在业务审核-审核表中
 *           对前台用户提交上来的信用投诉进行审核通过或审核驳回操作,以便于我维护信用投诉审核信息
 * @Scenario: 审核驳回
 */

class RejectTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $unAuditComplaintObject;

    private $mock;

    public function setUp()
    {
        $this->unAuditComplaintObject = new UnAuditComplaint();
    }

    public function tearDown()
    {
        unset($this->unAuditComplaintObject);
    }

    /**
    * @Given: 存在一条待审核的信用投诉审核数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData(
            "unAuditedComplaints",
            UnAuditComplaint::ACCEPT_STATUS['ACCEPTING']
        );
        $data['data']['attributes']['applyStatus'] = UnAuditComplaint::APPLY_STATUS['PENDING'];
        $jsonData = json_encode($data);
        
        $rejectData = $this->getComplaintDetailData(
            "unAuditedComplaints",
            UnAuditComplaint::ACCEPT_STATUS['ACCEPTING']
        );
        $rejectData['data']['attributes']['applyStatus'] = UnAuditComplaint::APPLY_STATUS['REJECT'];
        $rejectData['data']['attributes']['rejectReason'] = '驳回原因';

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($rejectData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要审核驳回的信用投诉
    */
    protected function fetchUnAuditComplaint($id)
    {
        $adapter = new UnAuditComplaintRestfulAdapter();
        $this->unAuditComplaintObject = $adapter->fetchOne($id);
        
        return $this->unAuditComplaintObject;
    }

    /**
    * @And:当我调用审核驳回函数,期待返回true
    */
    protected function reject()
    {
        $crew = new Crew(1);
        $this->unAuditComplaintObject->setApplyCrew($crew);
        $this->unAuditComplaintObject->setRejectReason('驳回原因');

        return $this->unAuditComplaintObject->reject();
    }
    /**
     * @Then 数据已经被审核驳回
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditComplaint($id);
        $this->reject();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedComplaints/1/reject';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getRejectRequestData('unAuditedComplaints')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            UnAuditComplaint::APPLY_STATUS['REJECT'],
            $this->unAuditComplaintObject->getApplyStatus()
        );

        $this->assertEquals(
            UnAuditComplaint::ACCEPT_STATUS['ACCEPTING'],
            $this->unAuditComplaintObject->getAcceptStatus()
        );

        $this->assertEquals(
            '驳回原因',
            $this->unAuditComplaintObject->getRejectReason()
        );

        $this->assertEquals(
            1,
            $this->unAuditComplaintObject->getApplyCrew()->getId()
        );
    }
}
