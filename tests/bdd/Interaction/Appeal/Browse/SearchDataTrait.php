<?php
namespace Base\Sdk\Interaction\Appeal\Browse;

use Base\Sdk\Interaction\Adapter\Appeal\AppealRestfulAdapter;

trait SearchDataTrait
{
    protected function getAppealList(array $filter = [])
    {
        $adapter = new AppealRestfulAdapter();

        list($count, $appealList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
   
        return $appealList;
    }
}
