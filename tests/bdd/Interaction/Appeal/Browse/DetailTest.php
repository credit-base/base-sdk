<?php
namespace Base\Sdk\Interaction\Appeal\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\Interaction\Adapter\Appeal\AppealRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉管理权限,在异议申诉模块中
 *           查看前台用户提交的已审核通过的异议申诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的异议申诉信息
 * @Scenario: 查看异议申诉数据详情
 */

class DetailTest extends TestCase
{
    use ArrayDataTrait;

    private $appeal;

    private $mock;

    public function setUp()
    {
        $this->appeal = new Appeal();
    }

    public function tearDown()
    {
        unset($this->appeal);
    }

    /**
    * @Given: 存在一条异议申诉数据
    */
    protected function prepareData()
    {
        $data = $this->getDetailData("appeals");

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条异议申诉数据详情时
    */
    protected function fetchAppeal($id)
    {

        $adapter = new AppealRestfulAdapter();

        $this->appeal = $adapter->fetchOne($id);
      
        return $this->appeal;
    }

    /**
     * @Then  我可以看见该条异议申诉数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchAppeal($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'appeals/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getDetailData("appeals")['data'];

        $this->assertEquals($data['attributes']['name'], $this->appeal->getName());
        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->appeal->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->appeal->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(
            $data['attributes']['title'],
            $this->appeal->getTitle()
        );
        $this->assertEquals(
            $data['attributes']['content'],
            $this->appeal->getContent()
        );
        $this->assertEquals(
            $data['attributes']['type'],
            $this->appeal->getType()
        );
        $this->assertEquals(
            $data['attributes']['contact'],
            $this->appeal->getContact()
        );
        $this->assertEquals(
            $data['attributes']['identify'],
            $this->appeal->getIdentify()
        );

        $this->assertEquals(
            $data['attributes']['images'],
            $this->appeal->getImages()
        );
   
        $this->assertEquals(
            $data['attributes']['certificates'],
            $this->appeal->getCertificates()
        );
       
        $this->assertEquals(Appeal::STATUS['NORMAL'], $this->appeal->getStatus());

        $this->assertEquals(
            0,
            $this->appeal->getStatusTime()
        );
        $this->assertEquals(
            0,
            $this->appeal->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->appeal->getUpdateTime()
        );
    }
}
