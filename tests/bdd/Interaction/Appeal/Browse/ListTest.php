<?php
namespace Base\Sdk\Interaction\Appeal\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Appeal;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ArrayDataTrait;

/**
 * @Feature:  我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉管理权限,在异议申诉模块中
 *           查看前台用户提交的已审核通过的异议申诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的异议申诉信息
 * @Scenario: 查看查看异议申诉数据列表列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ArrayDataTrait;

    private $appeal;

    private $mock;

    public function setUp()
    {
        $this->appeal = new Appeal();
    }

    public function tearDown()
    {
        unset($this->appeal);
    }

    /**
    * @Given: 存在查看异议申诉数据列表数据
    */
    protected function prepareData()
    {
        $data = $this->getListData('appeals');
        
        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看查看异议申诉数据列表时
     */
    public function fetchAppealList()
    {
        $filter = [];
        $list =$this->getAppealList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见查看异议申诉数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchAppealList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'appeals';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $appealList = $this->fetchAppealList();

        $appealArray = $this->getListData('appeals')['data'];
        
        foreach ($appealList as $appealObject) {
            foreach ($appealArray as $item) {
                if ($appealObject->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['title'], $appealObject->getTitle());
                    $this->assertEquals($item['attributes']['content'], $appealObject->getContent());
                    $this->assertEquals(
                        $item['attributes']['type'],
                        $appealObject->getType()
                    );
                    $this->assertEquals(
                        $item['attributes']['contact'],
                        $appealObject->getContact()
                    );
                    $this->assertEquals(
                        $item['attributes']['identify'],
                        $appealObject->getIdentify()
                    );

                    $this->assertEquals(
                        $item['attributes']['images'],
                        $appealObject->getImages()
                    );
            
                    $this->assertEquals(
                        $item['attributes']['certificates'],
                        $appealObject->getCertificates()
                    );

                    $this->assertEquals($item['attributes']['status'], $appealObject->getStatus());
                    $this->assertEquals(
                        $item['relationships']['acceptUserGroup']['data']['id'],
                        $appealObject->getAcceptUserGroup()->getId()
                    );

                    $this->assertEquals(
                        $item['relationships']['member']['data']['id'],
                        $appealObject->getMember()->getId()
                    );

                    $this->assertEquals(
                        0,
                        $appealObject->getStatusTime()
                    );
                    $this->assertEquals(
                        0,
                        $appealObject->getCreateTime()
                    );
                    $this->assertEquals(
                        0,
                        $appealObject->getUpdateTime()
                    );
                }
            }
        }
    }
}
