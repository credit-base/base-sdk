<?php
namespace Base\Sdk\Interaction\Appeal\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\Interaction\Adapter\Appeal\AppealRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉管理权限,当我需要想要对某个异议申诉数据进行受理处理时,在业务管理-异议申诉中
 *           对前台用户提交上来的异议申诉进行受理处理，并提交我填写的受理依据,以便于我维护异议申诉信息
 * @Scenario: 受理异议申诉
 */

class AcceptTest extends TestCase
{
    use ArrayDataTrait;

    private $appeal;

    private $mock;

    public function setUp()
    {
        $this->appeal = new Appeal();
    }

    public function tearDown()
    {
        unset($this->appeal);
    }

    /**
    * @Given: 我并未受理过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getDetailData("appeals");

        $jsonData = json_encode($data);
        
        $acceptData = $this->getDetailData("appeals", Appeal::ACCEPT_STATUS['ACCEPTING']);
        $acceptData['data']['attributes']['statusTime'] = 1621997357;
        $acceptData['data']['attributes']['updateTime'] = 1621997357;
  
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($acceptData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要受理的异议申诉数据
    */
    protected function fetchAppeal($id)
    {
        $adapter = new AppealRestfulAdapter();
        $this->appeal = $adapter->fetchOne($id);
        
        return $this->appeal;
    }

    /**
    * @And:当我调用受理函数,期待返回true
    */
    protected function accept()
    {
        $reply = $this->appeal->getReply();
        $reply->setContent('予以受理');
        $reply->setImages($this->getImages());
        $reply->setAdmissibility(1);

        $crew = new Crew(1);
        $reply->setCrew($crew);
       
        $this->appeal->setReply($reply);
        
        return  $this->appeal->accept();
    }
    /**
     * @Then  可以查到受理的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchAppeal($id);
        $this->accept();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/appeals/1/accept';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getReplyData('appeals')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            Appeal::ACCEPT_STATUS['ACCEPTING'],
            $this->appeal->getAcceptStatus()
        );
       
        $this->assertEquals(
            1,
            $this->appeal->getReply()->getAdmissibility()
        );

        $this->assertEquals(
            '予以受理',
            $this->appeal->getReply()->getContent()
        );

        $this->assertEquals(
            $this->getImages(),
            $this->appeal->getReply()->getImages()
        );

        $this->assertEquals(
            1621997357,
            $this->appeal->getStatusTime()
        );
        $this->assertEquals(
            1621997357,
            $this->appeal->getUpdateTime()
        );
    }
}
