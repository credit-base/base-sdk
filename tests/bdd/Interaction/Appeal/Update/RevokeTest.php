<?php
namespace Base\Sdk\Interaction\Appeal\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\Interaction\Adapter\Appeal\AppealRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ArrayDataTrait;

/**
 * @Feature: 我是门户网用户,我想要撤销我之前提交上去的异议申诉时,在用户中心-异议申诉中,我可以管理我提交的异议申诉数据
 *           可以撤销受理情况为未受理状态的异议申诉,以便于我维护我的异议申诉信息
 * @Scenario: 撤销异议申诉
 */

class RevokeTest extends TestCase
{
    use ArrayDataTrait;

    private $revokeAppealObject;

    private $mock;

    public function setUp()
    {
        $this->revokeAppealObject = new Appeal();
    }

    public function tearDown()
    {
        unset($this->revokeAppealObject);
    }

    /**
    * @Given: 存在需要撤销的异议申诉
    */
    protected function prepareData()
    {
        $data = $this->getDetailData("appeals");
        $jsonData = json_encode($data);
        
        $revokeData = $this->getDetailData("appeals");
        $revokeData['data']['attributes']['status'] = Appeal::STATUS['REVOKED'];

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($revokeData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要撤销的异议申诉
    */
    protected function fetchAppeal($id)
    {
        $adapter = new AppealRestfulAdapter();
        $this->revokeAppealObject = $adapter->fetchOne($id);
        
        return $this->revokeAppealObject;
    }

    /**
    * @And:当我调用撤销函数,期待撤销成功
    */
    protected function revoke()
    {
        return $this->revokeAppealObject->revoke();
    }
    /**
     * @Then  我可以查到该条异议申诉已被撤销
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchAppeal($id);
        $this->revoke();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/appeals/1/revoke';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals('[]', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Appeal::STATUS['REVOKED'], $this->revokeAppealObject->getStatus());
    }
}
