<?php
namespace Base\Sdk\Interaction\Appeal\Add;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Appeal;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\UserGroup\Model\UserGroup;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ArrayDataTrait;

/**
 * @Feature: 我是门户网用户,当我需要想要对某个信用数据进行投诉时,在信用公示栏目-信用反馈中,新增一个异议申诉数据
 *           通过新增异议申诉界面，并根据我所提交的投诉信息数据进行新增,以便于我维护异议申诉信息
 * @Scenario: 新增异议申诉
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $appeal;

    private $mock;

    public function setUp()
    {
        $this->appeal = new Appeal();
    }

    public function tearDown()
    {
        unset($this->appeal);
    }

    /**
    * @Given: 我并未新增过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getDetailData("appeals");
        
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;
        $data['data']['attributes']['createTime'] = 1635498202;

        $jsonData = json_encode($data);
       
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
       
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我调用添加函数,期待返回true
    */
    protected function add()
    {
        $addData = $this->getAttributesData();
        $this->appeal = new Appeal();
        $this->appeal->setTitle($addData['title']);
     
        $acceptUserGroup = new UserGroup(1);
        $this->appeal->setAcceptUserGroup($acceptUserGroup);
        $this->appeal->setContent($addData['content']);
        $this->appeal->setIdentify($addData['identify']);
        $this->appeal->setName($addData['name']);
        $this->appeal->setImages($addData['images']);
        $this->appeal->setCertificates($addData['certificates']);
        $member = new Member(1);
        $this->appeal->setMember($member);
        $this->appeal->setType($addData['type']);
        $this->appeal->setContact($addData['contact']);
      
        return  $this->appeal->add();
    }
    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        
        $this->add();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/appeals';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getAddRequestData("appeals")), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getDetailData("appeals")['data'];

        $this->assertEquals($data['attributes']['title'], $this->appeal->getTitle());
        $this->assertEquals($data['attributes']['content'], $this->appeal->getContent());
        $this->assertEquals($data['attributes']['identify'], $this->appeal->getIdentify());

        $this->assertEquals($data['attributes']['name'], $this->appeal->getName());
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->appeal->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->appeal->getAcceptUserGroup()->getId()
        );
        $this->assertEquals(
            $data['attributes']['images'],
            $this->appeal->getImages()
        );
   
        $this->assertEquals(
            $data['attributes']['certificates'],
            $this->appeal->getCertificates()
        );
        $this->assertEquals(
            $data['attributes']['type'],
            $this->appeal->getType()
        );
        $this->assertEquals(
            $data['attributes']['contact'],
            $this->appeal->getContact()
        );

        $this->assertEquals(Appeal::STATUS['NORMAL'], $this->appeal->getStatus());

        $this->assertEquals(
            0,
            $this->appeal->getStatusTime()
        );
        $this->assertEquals(
            1635498202,
            $this->appeal->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->appeal->getUpdateTime()
        );
    }
}
