<?php
namespace Base\Sdk\Interaction\UnAuditAppeal\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditAppeal;
use Base\Sdk\Interaction\Adapter\UnAuditAppeal\UnAuditAppealRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉管理权限,
 *           当我需要想要对某个受理情况为已受理且审核状态为已驳回的异议申诉数据进行重新受理时,在业务管理-异议申诉的受理表中
 *           对前台用户提交上来的异议申诉进行重新受理，并提交我填写的受理依据,以便于我维护异议申诉信息
 * @Scenario: 重新受理异议申诉审核数据
 */

class ResubmitAcceptTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditAppeal;

    private $mock;

    public function setUp()
    {
        $this->unAuditAppeal = new UnAuditAppeal();
    }

    public function tearDown()
    {
        unset($this->unAuditAppeal);
    }

    /**
    * @Given: 我并未重新受理过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getDetailData("unAuditedAppeals", UnAuditAppeal::ACCEPT_STATUS['ACCEPTING']);

        $jsonData = json_encode($data);
        
        $acceptData = $this->getDetailData("unAuditedAppeals", UnAuditAppeal::ACCEPT_STATUS['ACCEPTING']);
        $acceptData['data']['attributes']['applyStatus'] = UnAuditAppeal::APPLY_STATUS['REJECT'];
        $acceptData['data']['attributes']['statusTime'] = 1621997357;
        $acceptData['data']['attributes']['updateTime'] = 1621997357;
  
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($acceptData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要重新受理的异议申诉数据
    */
    protected function fetchUnAuditAppeal($id)
    {
        $adapter = new UnAuditAppealRestfulAdapter();
        $this->unAuditAppeal = $adapter->fetchOne($id);
        
        return $this->unAuditAppeal;
    }

    /**
    * @And:当我调用重新受理函数,期待返回true
    */
    protected function resubmit()
    {
        $reply = $this->unAuditAppeal->getReply();
        $reply->setContent('予以受理');
        $reply->setImages($this->getImages());
        $reply->setAdmissibility(1);

        $crew = new Crew(1);
        $reply->setCrew($crew);
       
        $this->unAuditAppeal->setReply($reply);
        
        return  $this->unAuditAppeal->resubmit();
    }
    /**
     * @Then  可以查到重新受理的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditAppeal($id);
        $this->resubmit();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedAppeals/1/resubmit';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getReplyData('unAuditedAppeals')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            UnAuditAppeal::ACCEPT_STATUS['ACCEPTING'],
            $this->unAuditAppeal->getAcceptStatus()
        );

        $this->assertEquals(
            UnAuditAppeal::APPLY_STATUS['REJECT'],
            $this->unAuditAppeal->getApplyStatus()
        );
       
        $this->assertEquals(
            1,
            $this->unAuditAppeal->getReply()->getAdmissibility()
        );

        $this->assertEquals(
            '予以受理',
            $this->unAuditAppeal->getReply()->getContent()
        );

        $this->assertEquals(
            $this->getImages(),
            $this->unAuditAppeal->getReply()->getImages()
        );

        $this->assertEquals(
            1621997357,
            $this->unAuditAppeal->getStatusTime()
        );
        $this->assertEquals(
            1621997357,
            $this->unAuditAppeal->getUpdateTime()
        );
    }
}
