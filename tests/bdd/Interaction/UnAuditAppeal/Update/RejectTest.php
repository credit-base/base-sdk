<?php
namespace Base\Sdk\Interaction\UnAuditAppeal\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditAppeal;
use Base\Sdk\Interaction\Adapter\UnAuditAppeal\UnAuditAppealRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉审核权限,当我需要想要对某个待审核的异议申诉数据进行审核时,在业务审核-审核表中
 *           对前台用户提交上来的异议申诉进行审核通过或审核驳回操作,以便于我维护异议申诉审核信息
 * @Scenario: 审核驳回
 */

class RejectTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditAppealObject;

    private $mock;

    public function setUp()
    {
        $this->unAuditAppealObject = new UnAuditAppeal();
    }

    public function tearDown()
    {
        unset($this->unAuditAppealObject);
    }

    /**
    * @Given: 存在一条待审核的异议申诉审核数据
    */
    protected function prepareData()
    {
        $data = $this->getDetailData("unAuditedAppeals", UnAuditAppeal::ACCEPT_STATUS['ACCEPTING']);
        $data['data']['attributes']['applyStatus'] = UnAuditAppeal::APPLY_STATUS['PENDING'];
        $jsonData = json_encode($data);
        
        $rejectData = $this->getDetailData("unAuditedAppeals", UnAuditAppeal::ACCEPT_STATUS['ACCEPTING']);
        $rejectData['data']['attributes']['applyStatus'] = UnAuditAppeal::APPLY_STATUS['REJECT'];
        $rejectData['data']['attributes']['rejectReason'] = '驳回原因';

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($rejectData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要审核驳回的异议申诉
    */
    protected function fetchUnAuditAppeal($id)
    {
        $adapter = new UnAuditAppealRestfulAdapter();
        $this->unAuditAppealObject = $adapter->fetchOne($id);
        
        return $this->unAuditAppealObject;
    }

    /**
    * @And:当我调用审核驳回函数,期待返回true
    */
    protected function reject()
    {
        $crew = new Crew(1);
        $this->unAuditAppealObject->setApplyCrew($crew);
        $this->unAuditAppealObject->setRejectReason('驳回原因');

        return $this->unAuditAppealObject->reject();
    }
    /**
     * @Then 数据已经被审核驳回
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditAppeal($id);
        $this->reject();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedAppeals/1/reject';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);

        $this->assertEquals(json_encode($this->getRejectRequestData('unAuditedAppeals')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            UnAuditAppeal::APPLY_STATUS['REJECT'],
            $this->unAuditAppealObject->getApplyStatus()
        );

        $this->assertEquals(
            UnAuditAppeal::ACCEPT_STATUS['ACCEPTING'],
            $this->unAuditAppealObject->getAcceptStatus()
        );

        $this->assertEquals(
            '驳回原因',
            $this->unAuditAppealObject->getRejectReason()
        );

        $this->assertEquals(
            1,
            $this->unAuditAppealObject->getApplyCrew()->getId()
        );
    }
}
