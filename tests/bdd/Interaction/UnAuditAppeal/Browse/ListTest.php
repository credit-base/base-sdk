<?php
namespace Base\Sdk\Interaction\UnAuditAppeal\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditAppeal;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ArrayDataTrait;

/**
 * @Feature:  我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉管理权限,在异议申诉模块中
 *           查看前台用户提交的待审核和已驳回的异议申诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的待审核和已驳回的异议申诉信息
 * @Scenario: 查看查看异议申诉数据列表列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ArrayDataTrait;

    private $unAuditAppeal;

    private $mock;

    public function setUp()
    {
        $this->unAuditAppeal = new UnAuditAppeal();
    }

    public function tearDown()
    {
        unset($this->unAuditAppeal);
    }

    /**
    * @Given: 存在查看异议申诉数据审核列表数据
    */
    protected function prepareData()
    {
        $data = $this->getListData('unAuditedAppeals');
        $data['data'][0]['attributes']['applyStatus'] = UnAuditAppeal::APPLY_STATUS['PENDING'];

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看查看异议申诉审核数据列表时
     */
    public function fetchUnAuditAppealList()
    {
        $filter = [];
        $list =$this->getUnAuditAppealList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见查看异议申诉审核数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditAppealList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedAppeals';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $unAuditAppealList = $this->fetchUnAuditAppealList();

        $unAuditAppealArray = $this->getListData('unAuditedAppeals')['data'];
        
        foreach ($unAuditAppealList as $object) {
            foreach ($unAuditAppealArray as $item) {
                if ($object->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['title'], $object->getTitle());
                    $this->assertEquals($item['attributes']['content'], $object->getContent());
                    $this->assertEquals(
                        $item['attributes']['type'],
                        $object->getType()
                    );
                    $this->assertEquals(
                        $item['attributes']['contact'],
                        $object->getContact()
                    );
                    $this->assertEquals(
                        $item['attributes']['identify'],
                        $object->getIdentify()
                    );

                    $this->assertEquals(
                        $item['attributes']['images'],
                        $object->getImages()
                    );
            
                    $this->assertEquals(
                        $item['attributes']['certificates'],
                        $object->getCertificates()
                    );

                    $this->assertEquals($item['attributes']['status'], $object->getStatus());

                    $this->assertEquals(
                        UnAuditAppeal::APPLY_STATUS['PENDING'],
                        $object->getApplyStatus()
                    );
        
                    $this->assertEquals(
                        $item['attributes']['acceptStatus'],
                        $object->getAcceptStatus()
                    );

                    $this->assertEquals(
                        $item['relationships']['acceptUserGroup']['data']['id'],
                        $object->getAcceptUserGroup()->getId()
                    );

                    $this->assertEquals(
                        $item['relationships']['member']['data']['id'],
                        $object->getMember()->getId()
                    );

                    $this->assertEquals(
                        0,
                        $object->getStatusTime()
                    );
                    $this->assertEquals(
                        0,
                        $object->getCreateTime()
                    );
                    $this->assertEquals(
                        0,
                        $object->getUpdateTime()
                    );
                }
            }
        }
    }
}
