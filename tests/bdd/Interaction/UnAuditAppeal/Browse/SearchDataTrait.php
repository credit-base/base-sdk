<?php
namespace Base\Sdk\Interaction\UnAuditAppeal\Browse;

use Base\Sdk\Interaction\Adapter\UnAuditAppeal\UnAuditAppealRestfulAdapter;

trait SearchDataTrait
{
    protected function getUnAuditAppealList(array $filter = [])
    {
        $adapter = new UnAuditAppealRestfulAdapter();

        list($count, $appealList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
   
        return $appealList;
    }
}
