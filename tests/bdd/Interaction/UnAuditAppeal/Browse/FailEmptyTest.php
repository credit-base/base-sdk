<?php
namespace Base\Sdk\Interaction\UnAuditAppeal\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditAppeal;
use Base\Sdk\Interaction\Adapter\UnAuditAppeal\UnAuditAppealRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉审核权限,在异议申诉模块中
 *           查看前台用户提交的异议申诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的异议申诉信息
 * @Scenario: 查看异议申诉数据审核列表-异常不存在数据
 */

class FailEmptyTest extends TestCase
{
    private $unAuditUnAuditAppeal;

    private $mock;

    public function setUp()
    {
        $this->unAuditUnAuditAppeal = new UnAuditAppeal();
    }

    public function tearDown()
    {
        unset($this->unAuditUnAuditAppeal);
    }

    /**
    * @Given: 不存在异议申诉审核数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看异议申诉审核列表时
     */
    public function fetchUnAuditAppealList()
    {
        $adapter = new UnAuditAppealRestfulAdapter();

        list($count, $this->unAuditUnAuditAppeal) = $adapter
        ->search();
        unset($count);

        return $this->unAuditUnAuditAppeal;
    }

    /**
     * @Then: 获取不到审核数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchUnAuditAppealList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedAppeals';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->unAuditUnAuditAppeal);
    }
}
