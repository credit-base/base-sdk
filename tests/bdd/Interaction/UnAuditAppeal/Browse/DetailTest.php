<?php
namespace Base\Sdk\Interaction\UnAuditAppeal\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditAppeal;
use Base\Sdk\Interaction\Adapter\UnAuditAppeal\UnAuditAppealRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有异议申诉审核权限,在异议申诉模块中
 *           查看前台用户提交的异议申诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的异议申诉信息
 * @Scenario: 查看异议申诉审核数据详情
 */

class DetailTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditAppeal;

    private $mock;

    public function setUp()
    {
        $this->unAuditAppeal = new UnAuditAppeal();
    }

    public function tearDown()
    {
        unset($this->unAuditAppeal);
    }

    /**
    * @Given: 存在一条异议申诉审核数据
    */
    protected function prepareData()
    {
        $data = $this->getDetailData("unAuditedAppeals", UnAuditAppeal::ACCEPT_STATUS['ACCEPTING']);
        $data['data']['attributes']['applyStatus'] = UnAuditAppeal::APPLY_STATUS['PENDING'];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条异议申诉审核数据详情时
    */
    protected function fetchUnAuditAppeal($id)
    {

        $adapter = new UnAuditAppealRestfulAdapter();

        $this->unAuditAppeal = $adapter->fetchOne($id);
      
        return $this->unAuditAppeal;
    }

    /**
     * @Then 我可以看见该条异议申诉审核数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditAppeal($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedAppeals/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getDetailData("unAuditedAppeals")['data'];

        $this->assertEquals($data['attributes']['name'], $this->unAuditAppeal->getName());
        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->unAuditAppeal->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->unAuditAppeal->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(
            $data['attributes']['title'],
            $this->unAuditAppeal->getTitle()
        );
        $this->assertEquals(
            $data['attributes']['content'],
            $this->unAuditAppeal->getContent()
        );
        $this->assertEquals(
            $data['attributes']['type'],
            $this->unAuditAppeal->getType()
        );
        $this->assertEquals(
            $data['attributes']['contact'],
            $this->unAuditAppeal->getContact()
        );
        $this->assertEquals(
            $data['attributes']['identify'],
            $this->unAuditAppeal->getIdentify()
        );

        $this->assertEquals(
            $data['attributes']['images'],
            $this->unAuditAppeal->getImages()
        );
   
        $this->assertEquals(
            $data['attributes']['certificates'],
            $this->unAuditAppeal->getCertificates()
        );
       
        $this->assertEquals(
            UnAuditAppeal::STATUS['NORMAL'],
            $this->unAuditAppeal->getStatus()
        );
        $this->assertEquals(
            UnAuditAppeal::APPLY_STATUS['PENDING'],
            $this->unAuditAppeal->getApplyStatus()
        );

        $this->assertEquals(
            0,
            $this->unAuditAppeal->getStatusTime()
        );
        $this->assertEquals(
            0,
            $this->unAuditAppeal->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->unAuditAppeal->getUpdateTime()
        );
    }
}
