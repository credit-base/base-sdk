<?php
namespace Base\Sdk\Interaction\UnAuditQa\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditQa;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature:  我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答管理权限,在信用问答模块中
 *           查看前台用户提交的待审核和已驳回的信用问答信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的待审核和已驳回的信用问答信息
 * @Scenario: 查看查看信用问答数据列表列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, FeedBackArrayDataTrait;

    private $unAuditQa;

    private $mock;

    public function setUp()
    {
        $this->unAuditQa = new UnAuditQa();
    }

    public function tearDown()
    {
        unset($this->unAuditQa);
    }

    /**
    * @Given: 存在查看信用问答数据审核列表数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackListData('unAuditedQas');
        $data['data'][0]['attributes']['applyStatus'] = UnAuditQa::APPLY_STATUS['PENDING'];

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看查看信用问答审核数据列表时
     */
    public function fetchUnAuditQaList()
    {
        $filter = [];
        $list =$this->getUnAuditQaList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见查看信用问答审核数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditQaList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedQas';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $unAuditQaList = $this->fetchUnAuditQaList();

        $unAuditQaArray = $this->getFeedBackListData('unAuditedQas')['data'];
        
        foreach ($unAuditQaList as $unAuditQa) {
            foreach ($unAuditQaArray as $item) {
                if ($unAuditQa->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['title'], $unAuditQa->getTitle());
                    $this->assertEquals($item['attributes']['content'], $unAuditQa->getContent());

                    $this->assertEquals($item['attributes']['status'], $unAuditQa->getStatus());

                    $this->assertEquals(
                        UnAuditQa::APPLY_STATUS['PENDING'],
                        $unAuditQa->getApplyStatus()
                    );
        
                    $this->assertEquals(
                        $item['attributes']['acceptStatus'],
                        $unAuditQa->getAcceptStatus()
                    );

                    $this->assertEquals(
                        $item['relationships']['acceptUserGroup']['data']['id'],
                        $unAuditQa->getAcceptUserGroup()->getId()
                    );

                    $this->assertEquals(
                        $item['relationships']['member']['data']['id'],
                        $unAuditQa->getMember()->getId()
                    );

                    $this->assertEquals(
                        0,
                        $unAuditQa->getStatusTime()
                    );
                    $this->assertEquals(
                        0,
                        $unAuditQa->getCreateTime()
                    );
                    $this->assertEquals(
                        0,
                        $unAuditQa->getUpdateTime()
                    );
                }
            }
        }
    }
}
