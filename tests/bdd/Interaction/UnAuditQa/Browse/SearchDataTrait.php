<?php
namespace Base\Sdk\Interaction\UnAuditQa\Browse;

use Base\Sdk\Interaction\Adapter\UnAuditQa\UnAuditQaRestfulAdapter;

trait SearchDataTrait
{
    protected function getUnAuditQaList(array $filter = [])
    {
        $adapter = new UnAuditQaRestfulAdapter();

        list($count, $list) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
   
        return $list;
    }
}
