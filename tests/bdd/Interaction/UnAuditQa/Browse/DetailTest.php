<?php
namespace Base\Sdk\Interaction\UnAuditQa\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditQa;
use Base\Sdk\Interaction\Adapter\UnAuditQa\UnAuditQaRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答审核权限,在信用问答模块中
 *           查看前台用户提交的信用问答信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用问答信息
 * @Scenario: 查看信用问答审核数据详情
 */

class DetailTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $unAuditQa;

    private $mock;

    public function setUp()
    {
        $this->unAuditQa = new UnAuditQa();
    }

    public function tearDown()
    {
        unset($this->unAuditQa);
    }

    /**
    * @Given: 存在一条信用问答审核数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("unAuditedQas", UnAuditQa::ACCEPT_STATUS['ACCEPTING']);
        $data['data']['attributes']['applyStatus'] = UnAuditQa::APPLY_STATUS['PENDING'];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条信用问答审核数据详情时
    */
    protected function fetchUnAuditQa($id)
    {

        $adapter = new UnAuditQaRestfulAdapter();

        $this->unAuditQa = $adapter->fetchOne($id);
      
        return $this->unAuditQa;
    }

    /**
     * @Then 我可以看见该条信用问答审核数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditQa($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedQas/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getFeedBackDetailData("unAuditedQas")['data'];

        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->unAuditQa->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->unAuditQa->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(
            $data['attributes']['title'],
            $this->unAuditQa->getTitle()
        );
        $this->assertEquals(
            $data['attributes']['content'],
            $this->unAuditQa->getContent()
        );
       
        $this->assertEquals(
            UnAuditQa::STATUS['NORMAL'],
            $this->unAuditQa->getStatus()
        );
        $this->assertEquals(
            UnAuditQa::APPLY_STATUS['PENDING'],
            $this->unAuditQa->getApplyStatus()
        );

        $this->assertEquals(
            0,
            $this->unAuditQa->getStatusTime()
        );
        $this->assertEquals(
            0,
            $this->unAuditQa->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->unAuditQa->getUpdateTime()
        );
    }
}
