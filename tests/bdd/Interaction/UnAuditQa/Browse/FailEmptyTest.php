<?php
namespace Base\Sdk\Interaction\UnAuditQa\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditQa;
use Base\Sdk\Interaction\Adapter\UnAuditQa\UnAuditQaRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用问答审核权限,在信用问答模块中
 *           查看前台用户提交的信用问答信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用问答信息
 * @Scenario: 查看信用问答数据审核列表-异常不存在数据
 */

class FailEmptyTest extends TestCase
{
    private $unAuditUnAuditQa;

    private $mock;

    public function setUp()
    {
        $this->unAuditUnAuditQa = new UnAuditQa();
    }

    public function tearDown()
    {
        unset($this->unAuditUnAuditQa);
    }

    /**
    * @Given: 不存在信用问答审核数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看信用问答审核列表时
     */
    public function fetchUnAuditQaList()
    {
        $adapter = new UnAuditQaRestfulAdapter();

        list($count, $this->unAuditUnAuditQa) = $adapter
        ->search();
        unset($count);

        return $this->unAuditUnAuditQa;
    }

    /**
     * @Then: 获取不到审核数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchUnAuditQaList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedQas';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->unAuditUnAuditQa);
    }
}
