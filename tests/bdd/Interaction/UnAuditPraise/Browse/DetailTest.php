<?php
namespace Base\Sdk\Interaction\UnAuditPraise\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditPraise;
use Base\Sdk\Interaction\Adapter\UnAuditPraise\UnAuditPraiseRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬审核权限,在信用表扬模块中
 *           查看前台用户提交的信用表扬信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的信用表扬信息
 * @Scenario: 查看信用表扬审核数据详情
 */

class DetailTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $unAuditPraise;

    private $mock;

    public function setUp()
    {
        $this->unAuditPraise = new UnAuditPraise();
    }

    public function tearDown()
    {
        unset($this->unAuditPraise);
    }

    /**
    * @Given: 存在一条信用表扬审核数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("unAuditedPraises", UnAuditPraise::ACCEPT_STATUS['ACCEPTING']);
        $data['data']['attributes']['applyStatus'] = UnAuditPraise::APPLY_STATUS['PENDING'];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条信用表扬审核数据详情时
    */
    protected function fetchUnAuditPraise($id)
    {

        $adapter = new UnAuditPraiseRestfulAdapter();

        $this->unAuditPraise = $adapter->fetchOne($id);
      
        return $this->unAuditPraise;
    }

    /**
     * @Then 我可以看见该条信用表扬审核数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditPraise($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedPraises/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getComplaintDetailData("unAuditedPraises")['data'];

        $this->assertEquals($data['attributes']['name'], $this->unAuditPraise->getName());
        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->unAuditPraise->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->unAuditPraise->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(
            $data['attributes']['title'],
            $this->unAuditPraise->getTitle()
        );
        $this->assertEquals(
            $data['attributes']['content'],
            $this->unAuditPraise->getContent()
        );
        $this->assertEquals(
            $data['attributes']['type'],
            $this->unAuditPraise->getType()
        );
        $this->assertEquals(
            $data['attributes']['contact'],
            $this->unAuditPraise->getContact()
        );
        $this->assertEquals(
            $data['attributes']['identify'],
            $this->unAuditPraise->getIdentify()
        );

        $this->assertEquals(
            $data['attributes']['images'],
            $this->unAuditPraise->getImages()
        );
   
        $this->assertEquals(
            $data['attributes']['subject'],
            $this->unAuditPraise->getSubject()
        );
       
        $this->assertEquals(
            UnAuditPraise::STATUS['NORMAL'],
            $this->unAuditPraise->getStatus()
        );
        $this->assertEquals(
            UnAuditPraise::APPLY_STATUS['PENDING'],
            $this->unAuditPraise->getApplyStatus()
        );

        $this->assertEquals(
            0,
            $this->unAuditPraise->getStatusTime()
        );
        $this->assertEquals(
            0,
            $this->unAuditPraise->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->unAuditPraise->getUpdateTime()
        );
    }
}
