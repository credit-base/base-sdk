<?php
namespace Base\Sdk\Interaction\UnAuditPraise\Browse;

use Base\Sdk\Interaction\Adapter\UnAuditPraise\UnAuditPraiseRestfulAdapter;

trait SearchDataTrait
{
    protected function getUnAuditPraiseList(array $filter = [])
    {
        $adapter = new UnAuditPraiseRestfulAdapter();

        list($count, $list) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
   
        return $list;
    }
}
