<?php
namespace Base\Sdk\Interaction\UnAuditPraise\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditPraise;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature:  我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,在信用表扬模块中
 *           查看前台用户提交的待审核和已驳回的信用表扬信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的待审核和已驳回的信用表扬信息
 * @Scenario: 查看查看信用表扬数据列表列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ComplaintArrayDataTrait;

    private $unAuditPraise;

    private $mock;

    public function setUp()
    {
        $this->unAuditPraise = new UnAuditPraise();
    }

    public function tearDown()
    {
        unset($this->unAuditPraise);
    }

    /**
    * @Given: 存在查看信用表扬数据审核列表数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintListData('unAuditedPraises');
        $data['data'][0]['attributes']['applyStatus'] = UnAuditPraise::APPLY_STATUS['PENDING'];

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看查看信用表扬审核数据列表时
     */
    public function fetchUnAuditPraiseList()
    {
        $filter = [];
        $list =$this->getUnAuditPraiseList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见查看信用表扬审核数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditPraiseList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedPraises';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $unAuditPraiseList = $this->fetchUnAuditPraiseList();

        $unAuditPraiseArray = $this->getComplaintListData('unAuditedPraises')['data'];
        
        foreach ($unAuditPraiseList as $unAuditPraise) {
            foreach ($unAuditPraiseArray as $item) {
                if ($unAuditPraise->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['title'], $unAuditPraise->getTitle());
                    $this->assertEquals($item['attributes']['content'], $unAuditPraise->getContent());
                    $this->assertEquals(
                        $item['attributes']['type'],
                        $unAuditPraise->getType()
                    );
                    $this->assertEquals(
                        $item['attributes']['contact'],
                        $unAuditPraise->getContact()
                    );
                    $this->assertEquals(
                        $item['attributes']['identify'],
                        $unAuditPraise->getIdentify()
                    );

                    $this->assertEquals(
                        $item['attributes']['images'],
                        $unAuditPraise->getImages()
                    );
            
                    $this->assertEquals(
                        $item['attributes']['subject'],
                        $unAuditPraise->getSubject()
                    );

                    $this->assertEquals($item['attributes']['status'], $unAuditPraise->getStatus());

                    $this->assertEquals(
                        UnAuditPraise::APPLY_STATUS['PENDING'],
                        $unAuditPraise->getApplyStatus()
                    );
        
                    $this->assertEquals(
                        $item['attributes']['acceptStatus'],
                        $unAuditPraise->getAcceptStatus()
                    );

                    $this->assertEquals(
                        $item['relationships']['acceptUserGroup']['data']['id'],
                        $unAuditPraise->getAcceptUserGroup()->getId()
                    );

                    $this->assertEquals(
                        $item['relationships']['member']['data']['id'],
                        $unAuditPraise->getMember()->getId()
                    );

                    $this->assertEquals(
                        0,
                        $unAuditPraise->getStatusTime()
                    );
                    $this->assertEquals(
                        0,
                        $unAuditPraise->getCreateTime()
                    );
                    $this->assertEquals(
                        0,
                        $unAuditPraise->getUpdateTime()
                    );
                }
            }
        }
    }
}
