<?php
namespace Base\Sdk\Interaction\UnAuditPraise\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditPraise;
use Base\Sdk\Interaction\Adapter\UnAuditPraise\UnAuditPraiseRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬审核权限,当我需要想要对某个待审核的信用表扬数据进行审核时,在业务审核-审核表中
 *           对前台用户提交上来的信用表扬进行审核通过或审核驳回操作,以便于我维护信用表扬审核信息
 * @Scenario: 审核通过
 */

class ApproveTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $unAuditPraiseObject;

    private $mock;

    public function setUp()
    {
        $this->unAuditPraiseObject = new UnAuditPraise();
    }

    public function tearDown()
    {
        unset($this->unAuditPraiseObject);
    }

    /**
    * @Given: 存在一条待审核的信用表扬审核数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("unAuditedPraises", UnAuditPraise::ACCEPT_STATUS['ACCEPTING']);
        $data['data']['attributes']['applyStatus'] = UnAuditPraise::APPLY_STATUS['PENDING'];
        $jsonData = json_encode($data);
        
        $approveData = $this->getComplaintDetailData("unAuditedPraises", UnAuditPraise::ACCEPT_STATUS['ACCEPTING']);
        $approveData['data']['attributes']['applyStatus'] = UnAuditPraise::APPLY_STATUS['APPROVE'];

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($approveData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要审核通过的信用表扬
    */
    protected function fetchUnAuditPraise($id)
    {
        $adapter = new UnAuditPraiseRestfulAdapter();
        $this->unAuditPraiseObject = $adapter->fetchOne($id);

        
        return $this->unAuditPraiseObject;
    }

    /**
    * @And:当我调用审核通过函数,期待返回true
    */
    protected function approve()
    {
        $crew = new Crew(1);
        $this->unAuditPraiseObject->setApplyCrew($crew);

        return $this->unAuditPraiseObject->approve();
    }
    /**
     * @Then 数据已经被审核通过
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditPraise($id);
        $this->approve();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedPraises/1/approve';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getUnAuditStatusRequest('unAuditedPraises')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            UnAuditPraise::APPLY_STATUS['APPROVE'],
            $this->unAuditPraiseObject->getApplyStatus()
        );

        $this->assertEquals(
            UnAuditPraise::ACCEPT_STATUS['ACCEPTING'],
            $this->unAuditPraiseObject->getAcceptStatus()
        );
        
        $this->assertEquals(
            1,
            $this->unAuditPraiseObject->getApplyCrew()->getId()
        );
    }
}
