<?php
namespace Base\Sdk\Interaction\UnAuditPraise\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditPraise;
use Base\Sdk\Interaction\Adapter\UnAuditPraise\UnAuditPraiseRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,
 *           当我需要想要对某个受理情况为已受理且审核状态为已驳回的信用表扬数据进行重新受理时,在业务管理-信用表扬的受理表中
 *           对前台用户提交上来的信用表扬进行重新受理，并提交我填写的受理依据,以便于我维护信用表扬信息
 * @Scenario: 重新受理信用表扬审核数据
 */

class ResubmitAcceptTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $unAuditPraise;

    private $mock;

    public function setUp()
    {
        $this->unAuditPraise = new UnAuditPraise();
    }

    public function tearDown()
    {
        unset($this->unAuditPraise);
    }

    /**
    * @Given: 我并未重新受理过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("unAuditedPraises", UnAuditPraise::ACCEPT_STATUS['ACCEPTING']);

        $jsonData = json_encode($data);
        
        $acceptData = $this->getComplaintDetailData("unAuditedPraises", UnAuditPraise::ACCEPT_STATUS['ACCEPTING']);
        $acceptData['data']['attributes']['applyStatus'] = UnAuditPraise::APPLY_STATUS['REJECT'];
        $acceptData['data']['attributes']['statusTime'] = 1621997357;
        $acceptData['data']['attributes']['updateTime'] = 1621997357;
  
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($acceptData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要重新受理的信用表扬数据
    */
    protected function fetchUnAuditPraise($id)
    {
        $adapter = new UnAuditPraiseRestfulAdapter();
        $this->unAuditPraise = $adapter->fetchOne($id);
        
        return $this->unAuditPraise;
    }

    /**
    * @And:当我调用重新受理函数,期待返回true
    */
    protected function resubmit()
    {
        $reply = $this->unAuditPraise->getReply();
        $reply->setContent('予以受理');
        $reply->setImages($this->getImages());
        $reply->setAdmissibility(1);

        $crew = new Crew(1);
        $reply->setCrew($crew);
       
        $this->unAuditPraise->setReply($reply);
        
        return  $this->unAuditPraise->resubmit();
    }
    /**
     * @Then  可以查到重新受理的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditPraise($id);
        $this->resubmit();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedPraises/1/resubmit';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getReplyData('unAuditedPraises')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            UnAuditPraise::ACCEPT_STATUS['ACCEPTING'],
            $this->unAuditPraise->getAcceptStatus()
        );

        $this->assertEquals(
            UnAuditPraise::APPLY_STATUS['REJECT'],
            $this->unAuditPraise->getApplyStatus()
        );
       
        $this->assertEquals(
            1,
            $this->unAuditPraise->getReply()->getAdmissibility()
        );

        $this->assertEquals(
            '予以受理',
            $this->unAuditPraise->getReply()->getContent()
        );

        $this->assertEquals(
            $this->getImages(),
            $this->unAuditPraise->getReply()->getImages()
        );

        $this->assertEquals(
            1621997357,
            $this->unAuditPraise->getStatusTime()
        );
        $this->assertEquals(
            1621997357,
            $this->unAuditPraise->getUpdateTime()
        );
    }
}
