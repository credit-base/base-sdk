<?php
namespace Base\Sdk\Interaction\Complaint\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Complaint;
use Base\Sdk\Interaction\Adapter\Complaint\ComplaintRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是门户网用户,我想要撤销我之前提交上去的信用投诉时,在用户中心-信用投诉中,我可以管理我提交的信用投诉数据
 *           可以撤销受理情况为未受理状态的信用投诉,以便于我维护我的信用投诉信息
 * @Scenario: 撤销信用投诉
 */

class RevokeTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $revokeComplaintObject;

    private $mock;

    public function setUp()
    {
        $this->revokeComplaintObject = new Complaint();
    }

    public function tearDown()
    {
        unset($this->revokeComplaintObject);
    }

    /**
    * @Given: 存在需要撤销的信用投诉
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("complaints");
        $jsonData = json_encode($data);
        
        $revokeData = $this->getComplaintDetailData("complaints");
        $revokeData['data']['attributes']['status'] = Complaint::STATUS['REVOKED'];

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($revokeData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要撤销的信用投诉
    */
    protected function fetchComplaint($id)
    {
        $adapter = new ComplaintRestfulAdapter();
        $this->revokeComplaintObject = $adapter->fetchOne($id);
        
        return $this->revokeComplaintObject;
    }

    /**
    * @And:当我调用撤销函数,期待撤销成功
    */
    protected function revoke()
    {
        return $this->revokeComplaintObject->revoke();
    }
    /**
     * @Then  我可以查到该条信用投诉已被撤销
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchComplaint($id);
        $this->revoke();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/complaints/1/revoke';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals('[]', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Complaint::STATUS['REVOKED'], $this->revokeComplaintObject->getStatus());
    }
}
