<?php
namespace Base\Sdk\Interaction\Complaint\Add;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Complaint;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\UserGroup\Model\UserGroup;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是门户网用户,当我需要想要对某个信用数据进行投诉时,在信用公示栏目-信用反馈中,新增一个信用投诉数据
 *           通过新增信用投诉界面，并根据我所提交的投诉信息数据进行新增,以便于我维护信用投诉信息
 * @Scenario: 新增信用投诉
 */

class SuccessTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $complaint;

    private $mock;

    public function setUp()
    {
        $this->complaint = new Complaint();
    }

    public function tearDown()
    {
        unset($this->complaint);
    }

    /**
    * @Given: 我并未新增过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("complaints");
        
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;
        $data['data']['attributes']['createTime'] = 1635498202;

        $jsonData = json_encode($data);
       
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
       
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我调用添加函数,期待返回true
    */
    protected function add()
    {
        $addData = $this->getAttributesData();
        $this->complaint = new Complaint();
        $this->complaint->setTitle($addData['title']);
     
        $acceptUserGroup = new UserGroup(1);
        $this->complaint->setAcceptUserGroup($acceptUserGroup);
        $this->complaint->setContent($addData['content']);
        $this->complaint->setIdentify($addData['identify']);
        $this->complaint->setName($addData['name']);
        $this->complaint->setImages($addData['images']);
        $this->complaint->setSubject('被投诉主体');
        $member = new Member(1);
        $this->complaint->setMember($member);
        $this->complaint->setType($addData['type']);
        $this->complaint->setContact($addData['contact']);
      
        return  $this->complaint->add();
    }
    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        
        $this->add();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/complaints';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getComplaintAddRequestData("complaints")), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getComplaintDetailData("complaints")['data'];

        $this->assertEquals($data['attributes']['title'], $this->complaint->getTitle());
        $this->assertEquals($data['attributes']['content'], $this->complaint->getContent());
        $this->assertEquals($data['attributes']['identify'], $this->complaint->getIdentify());

        $this->assertEquals($data['attributes']['name'], $this->complaint->getName());
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->complaint->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->complaint->getAcceptUserGroup()->getId()
        );
        $this->assertEquals(
            $data['attributes']['images'],
            $this->complaint->getImages()
        );
   
        $this->assertEquals(
            $data['attributes']['subject'],
            $this->complaint->getSubject()
        );
        $this->assertEquals(
            $data['attributes']['type'],
            $this->complaint->getType()
        );
        $this->assertEquals(
            $data['attributes']['contact'],
            $this->complaint->getContact()
        );

        $this->assertEquals(Complaint::STATUS['NORMAL'], $this->complaint->getStatus());

        $this->assertEquals(
            0,
            $this->complaint->getStatusTime()
        );
        $this->assertEquals(
            1635498202,
            $this->complaint->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->complaint->getUpdateTime()
        );
    }
}
