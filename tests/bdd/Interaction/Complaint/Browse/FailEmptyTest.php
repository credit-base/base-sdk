<?php
namespace Base\Sdk\Interaction\Complaint\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Complaint;
use Base\Sdk\Interaction\Adapter\Complaint\ComplaintRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉管理权限,在信用投诉模块中
 *           查看前台用户提交的已审核通过的信用投诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用投诉信息
 * @Scenario: 查看信用投诉数据列表-异常不存在数据
 */

class FailEmptyTest extends TestCase
{
    private $complaint;

    private $mock;

    public function setUp()
    {
        $this->complaint = new Complaint();
    }

    public function tearDown()
    {
        unset($this->complaint);
    }

    /**
    * @Given: 不存在信用投诉数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看信用投诉列表时
     */
    public function fetchComplaintList()
    {
        $adapter = new ComplaintRestfulAdapter();

        list($count, $this->complaint) = $adapter
        ->search();
        unset($count);

        return $this->complaint;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchComplaintList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'complaints';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->complaint);
    }
}
