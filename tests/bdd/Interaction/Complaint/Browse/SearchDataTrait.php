<?php
namespace Base\Sdk\Interaction\Complaint\Browse;

use Base\Sdk\Interaction\Adapter\Complaint\ComplaintRestfulAdapter;

trait SearchDataTrait
{
    protected function getComplaintList(array $filter = [])
    {
        $adapter = new ComplaintRestfulAdapter();

        list($count, $complaintList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
   
        return $complaintList;
    }
}
