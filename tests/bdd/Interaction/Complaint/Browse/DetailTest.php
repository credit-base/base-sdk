<?php
namespace Base\Sdk\Interaction\Complaint\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Complaint;
use Base\Sdk\Interaction\Adapter\Complaint\ComplaintRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉管理权限,在信用投诉模块中
 *           查看前台用户提交的已审核通过的信用投诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用投诉信息
 * @Scenario: 查看信用投诉数据详情
 */

class DetailTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $complaint;

    private $mock;

    public function setUp()
    {
        $this->complaint = new Complaint();
    }

    public function tearDown()
    {
        unset($this->complaint);
    }

    /**
    * @Given: 存在一条信用投诉数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("complaints");

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条信用投诉数据详情时
    */
    protected function fetchComplaint($id)
    {

        $adapter = new ComplaintRestfulAdapter();

        $this->complaint = $adapter->fetchOne($id);
      
        return $this->complaint;
    }

    /**
     * @Then  我可以看见该条信用投诉数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchComplaint($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'complaints/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getComplaintDetailData("complaints")['data'];

        $this->assertEquals($data['attributes']['name'], $this->complaint->getName());
        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->complaint->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->complaint->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(
            $data['attributes']['title'],
            $this->complaint->getTitle()
        );
        $this->assertEquals(
            $data['attributes']['content'],
            $this->complaint->getContent()
        );
        $this->assertEquals(
            $data['attributes']['type'],
            $this->complaint->getType()
        );
        $this->assertEquals(
            $data['attributes']['contact'],
            $this->complaint->getContact()
        );
        $this->assertEquals(
            $data['attributes']['identify'],
            $this->complaint->getIdentify()
        );

        $this->assertEquals(
            $data['attributes']['images'],
            $this->complaint->getImages()
        );
   
        $this->assertEquals(
            $data['attributes']['subject'],
            $this->complaint->getSubject()
        );
       
        $this->assertEquals(Complaint::STATUS['NORMAL'], $this->complaint->getStatus());

        $this->assertEquals(
            0,
            $this->complaint->getStatusTime()
        );
        $this->assertEquals(
            0,
            $this->complaint->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->complaint->getUpdateTime()
        );
    }
}
