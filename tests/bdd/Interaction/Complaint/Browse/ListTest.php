<?php
namespace Base\Sdk\Interaction\Complaint\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Complaint;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature:  我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用投诉管理权限,在信用投诉模块中
 *           查看前台用户提交的已审核通过的信用投诉信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用投诉信息
 * @Scenario: 查看查看信用投诉数据列表列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ComplaintArrayDataTrait;

    private $complaint;

    private $mock;

    public function setUp()
    {
        $this->complaint = new Complaint();
    }

    public function tearDown()
    {
        unset($this->complaint);
    }

    /**
    * @Given: 存在查看信用投诉数据列表数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintListData('complaints');
        
        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看查看信用投诉数据列表时
     */
    public function fetchComplaintList()
    {
        $filter = [];
        $list =$this->getComplaintList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见查看信用投诉数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchComplaintList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'complaints';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $complaintList = $this->fetchComplaintList();

        $complaintArray = $this->getComplaintListData('complaints')['data'];
        
        foreach ($complaintList as $object) {
            foreach ($complaintArray as $item) {
                if ($object->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['title'], $object->getTitle());
                    $this->assertEquals($item['attributes']['content'], $object->getContent());
                    $this->assertEquals(
                        $item['attributes']['type'],
                        $object->getType()
                    );
                    $this->assertEquals(
                        $item['attributes']['contact'],
                        $object->getContact()
                    );
                    $this->assertEquals(
                        $item['attributes']['identify'],
                        $object->getIdentify()
                    );

                    $this->assertEquals(
                        $item['attributes']['images'],
                        $object->getImages()
                    );
            
                    $this->assertEquals(
                        $item['attributes']['subject'],
                        $object->getSubject()
                    );

                    $this->assertEquals($item['attributes']['status'], $object->getStatus());
                    $this->assertEquals(
                        $item['relationships']['acceptUserGroup']['data']['id'],
                        $object->getAcceptUserGroup()->getId()
                    );

                    $this->assertEquals(
                        $item['relationships']['member']['data']['id'],
                        $object->getMember()->getId()
                    );

                    $this->assertEquals(
                        0,
                        $object->getStatusTime()
                    );
                    $this->assertEquals(
                        0,
                        $object->getCreateTime()
                    );
                    $this->assertEquals(
                        0,
                        $object->getUpdateTime()
                    );
                }
            }
        }
    }
}
