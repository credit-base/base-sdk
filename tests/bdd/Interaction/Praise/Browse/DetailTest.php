<?php
namespace Base\Sdk\Interaction\Praise\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Adapter\Praise\PraiseRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,在信用表扬模块中
 *           查看前台用户提交的已审核通过的信用表扬信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用表扬信息
 * @Scenario: 查看信用表扬数据详情
 */

class DetailTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $praise;

    private $mock;

    public function setUp()
    {
        $this->praise = new Praise();
    }

    public function tearDown()
    {
        unset($this->praise);
    }

    /**
    * @Given: 存在一条信用表扬数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("praises");

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条信用表扬数据详情时
    */
    protected function fetchPraise($id)
    {

        $adapter = new PraiseRestfulAdapter();

        $this->praise = $adapter->fetchOne($id);
      
        return $this->praise;
    }

    /**
     * @Then  我可以看见该条信用表扬数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchPraise($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'praises/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getComplaintDetailData("praises")['data'];

        $this->assertEquals($data['attributes']['name'], $this->praise->getName());
        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->praise->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->praise->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(
            $data['attributes']['title'],
            $this->praise->getTitle()
        );
        $this->assertEquals(
            $data['attributes']['content'],
            $this->praise->getContent()
        );
        $this->assertEquals(
            $data['attributes']['type'],
            $this->praise->getType()
        );
        $this->assertEquals(
            $data['attributes']['contact'],
            $this->praise->getContact()
        );
        $this->assertEquals(
            $data['attributes']['identify'],
            $this->praise->getIdentify()
        );

        $this->assertEquals(
            $data['attributes']['images'],
            $this->praise->getImages()
        );
   
        $this->assertEquals(
            $data['attributes']['subject'],
            $this->praise->getSubject()
        );
       
        $this->assertEquals(Praise::STATUS['NORMAL'], $this->praise->getStatus());

        $this->assertEquals(
            0,
            $this->praise->getStatusTime()
        );
        $this->assertEquals(
            0,
            $this->praise->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->praise->getUpdateTime()
        );
    }
}
