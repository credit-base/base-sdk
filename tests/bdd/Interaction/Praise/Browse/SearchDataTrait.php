<?php
namespace Base\Sdk\Interaction\Praise\Browse;

use Base\Sdk\Interaction\Adapter\Praise\PraiseRestfulAdapter;

trait SearchDataTrait
{
    protected function getPraiseList(array $filter = [])
    {
        $adapter = new PraiseRestfulAdapter();

        list($count, $list) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
   
        return $list;
    }
}
