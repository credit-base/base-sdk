<?php
namespace Base\Sdk\Interaction\Praise\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Praise;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature:  我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,在信用表扬模块中
 *           查看前台用户提交的已审核通过的信用表扬信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用表扬信息
 * @Scenario: 查看查看信用表扬数据列表列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ComplaintArrayDataTrait;

    private $praise;

    private $mock;

    public function setUp()
    {
        $this->praise = new Praise();
    }

    public function tearDown()
    {
        unset($this->praise);
    }

    /**
    * @Given: 存在查看信用表扬数据列表数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintListData('praises');
        
        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看查看信用表扬数据列表时
     */
    public function fetchPraiseList()
    {
        $filter = [];
        $list =$this->getPraiseList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见查看信用表扬数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchPraiseList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'praises';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $praiseList = $this->fetchPraiseList();

        $praiseArray = $this->getComplaintListData('praises')['data'];
        
        foreach ($praiseList as $praiseObject) {
            foreach ($praiseArray as $item) {
                if ($praiseObject->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['title'], $praiseObject->getTitle());
                    $this->assertEquals($item['attributes']['content'], $praiseObject->getContent());
                    $this->assertEquals(
                        $item['attributes']['type'],
                        $praiseObject->getType()
                    );
                    $this->assertEquals(
                        $item['attributes']['contact'],
                        $praiseObject->getContact()
                    );
                    $this->assertEquals(
                        $item['attributes']['identify'],
                        $praiseObject->getIdentify()
                    );

                    $this->assertEquals(
                        $item['attributes']['images'],
                        $praiseObject->getImages()
                    );
            
                    $this->assertEquals(
                        $item['attributes']['subject'],
                        $praiseObject->getSubject()
                    );

                    $this->assertEquals($item['attributes']['status'], $praiseObject->getStatus());
                    $this->assertEquals(
                        $item['relationships']['acceptUserGroup']['data']['id'],
                        $praiseObject->getAcceptUserGroup()->getId()
                    );

                    $this->assertEquals(
                        $item['relationships']['member']['data']['id'],
                        $praiseObject->getMember()->getId()
                    );

                    $this->assertEquals(
                        0,
                        $praiseObject->getStatusTime()
                    );
                    $this->assertEquals(
                        0,
                        $praiseObject->getCreateTime()
                    );
                    $this->assertEquals(
                        0,
                        $praiseObject->getUpdateTime()
                    );
                }
            }
        }
    }
}
