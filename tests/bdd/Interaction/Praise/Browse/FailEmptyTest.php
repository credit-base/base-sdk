<?php
namespace Base\Sdk\Interaction\Praise\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Adapter\Praise\PraiseRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,在信用表扬模块中
 *           查看前台用户提交的已审核通过的信用表扬信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的信用表扬信息
 * @Scenario: 查看信用表扬数据列表-异常不存在数据
 */

class FailEmptyTest extends TestCase
{
    private $praise;

    private $mock;

    public function setUp()
    {
        $this->praise = new Praise();
    }

    public function tearDown()
    {
        unset($this->praise);
    }

    /**
    * @Given: 不存在信用表扬数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看信用表扬列表时
     */
    public function fetchPraiseList()
    {
        $adapter = new PraiseRestfulAdapter();

        list($count, $this->praise) = $adapter
        ->search();
        unset($count);

        return $this->praise;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchPraiseList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'praises';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->praise);
    }
}
