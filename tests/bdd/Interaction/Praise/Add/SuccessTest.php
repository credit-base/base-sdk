<?php
namespace Base\Sdk\Interaction\Praise\Add;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Praise;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\UserGroup\Model\UserGroup;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是门户网用户,当我需要想要对某个信用数据进行投诉时,在信用公示栏目-信用反馈中,新增一个信用表扬数据
 *           通过新增信用表扬界面，并根据我所提交的投诉信息数据进行新增,以便于我维护信用表扬信息
 * @Scenario: 新增信用表扬
 */

class SuccessTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $praise;

    private $mock;

    public function setUp()
    {
        $this->praise = new Praise();
    }

    public function tearDown()
    {
        unset($this->praise);
    }

    /**
    * @Given: 我并未新增过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("praises");
        
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;
        $data['data']['attributes']['createTime'] = 1635498202;

        $jsonData = json_encode($data);
       
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
       
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我调用添加函数,期待返回true
    */
    protected function add()
    {
        $addData = $this->getAttributesData();
        $this->praise = new Praise();
        $this->praise->setTitle($addData['title']);
     
        $acceptUserGroup = new UserGroup(1);
        $this->praise->setAcceptUserGroup($acceptUserGroup);
        $this->praise->setContent($addData['content']);
        $this->praise->setIdentify($addData['identify']);
        $this->praise->setName($addData['name']);
        $this->praise->setImages($addData['images']);
        $this->praise->setSubject('被投诉主体');
        $member = new Member(1);
        $this->praise->setMember($member);
        $this->praise->setType($addData['type']);
        $this->praise->setContact($addData['contact']);
      
        return  $this->praise->add();
    }
    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        
        $this->add();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/praises';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getComplaintAddRequestData("praises")), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getComplaintDetailData("praises")['data'];

        $this->assertEquals($data['attributes']['title'], $this->praise->getTitle());
        $this->assertEquals($data['attributes']['content'], $this->praise->getContent());
        $this->assertEquals($data['attributes']['identify'], $this->praise->getIdentify());

        $this->assertEquals($data['attributes']['name'], $this->praise->getName());
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->praise->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->praise->getAcceptUserGroup()->getId()
        );
        $this->assertEquals(
            $data['attributes']['images'],
            $this->praise->getImages()
        );
   
        $this->assertEquals(
            $data['attributes']['subject'],
            $this->praise->getSubject()
        );
        $this->assertEquals(
            $data['attributes']['type'],
            $this->praise->getType()
        );
        $this->assertEquals(
            $data['attributes']['contact'],
            $this->praise->getContact()
        );

        $this->assertEquals(Praise::STATUS['NORMAL'], $this->praise->getStatus());

        $this->assertEquals(
            0,
            $this->praise->getStatusTime()
        );
        $this->assertEquals(
            1635498202,
            $this->praise->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->praise->getUpdateTime()
        );
    }
}
