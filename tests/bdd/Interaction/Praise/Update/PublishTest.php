<?php
namespace Base\Sdk\Interaction\Praise\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Adapter\Praise\PraiseRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,当我需要想要对某个待审核的信用表扬数据进行公示/取消公示时,
 *           在业务管理-信用表扬表中,对已经审核通过的信用表扬进行公示或取消公示的操作,以便于我维护信用表扬信息
 * @Scenario: 公示信用表扬
 */

class PublishTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $praiseObject;

    private $mock;

    public function setUp()
    {
        $this->praiseObject = new Praise();
    }

    public function tearDown()
    {
        unset($this->praiseObject);
    }

    /**
    * @Given: 存在需要公示的信用表扬
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("praises", Praise::ACCEPT_STATUS['COMPLETE']);
        $jsonData = json_encode($data);
        
        $publishData = $this->getComplaintDetailData("praises", Praise::ACCEPT_STATUS['COMPLETE']);
        $publishData['data']['attributes']['status'] = Praise::STATUS['PUBLISH'];

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($publishData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要公示的信用表扬
    */
    protected function fetchPraise($id)
    {
        $adapter = new PraiseRestfulAdapter();
        $this->praiseObject = $adapter->fetchOne($id);
        
        return $this->praiseObject;
    }

    /**
    * @And:当我调用公示函数,期待公示成功
    */
    protected function publish()
    {
        return $this->praiseObject->publish();
    }
    /**
     * @Then  我可以查到该条信用表扬已被公示
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchPraise($id);
        $this->publish();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/praises/1/publish';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals('[]', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            Praise::ACCEPT_STATUS['COMPLETE'],
            $this->praiseObject->getAcceptStatus()
        );

        $this->assertEquals(Praise::STATUS['PUBLISH'], $this->praiseObject->getStatus());
    }
}
