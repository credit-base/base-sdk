<?php
namespace Base\Sdk\Interaction\Praise\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Adapter\Praise\PraiseRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有信用表扬管理权限,当我需要想要对某个信用表扬数据进行受理处理时,在业务管理-信用表扬中
 *           对前台用户提交上来的信用表扬进行受理处理，并提交我填写的受理依据,以便于我维护信用表扬信息
 * @Scenario: 受理信用表扬
 */

class AcceptTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $praise;

    private $mock;

    public function setUp()
    {
        $this->praise = new Praise();
    }

    public function tearDown()
    {
        unset($this->praise);
    }

    /**
    * @Given: 我并未受理过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("praises");

        $jsonData = json_encode($data);
        
        $acceptData = $this->getComplaintDetailData("praises", Praise::ACCEPT_STATUS['ACCEPTING']);
        $acceptData['data']['attributes']['statusTime'] = 1621997357;
        $acceptData['data']['attributes']['updateTime'] = 1621997357;
  
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($acceptData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要受理的信用表扬数据
    */
    protected function fetchPraise($id)
    {
        $adapter = new PraiseRestfulAdapter();
        $this->praise = $adapter->fetchOne($id);
        
        return $this->praise;
    }

    /**
    * @And:当我调用受理函数,期待返回true
    */
    protected function accept()
    {
        $reply = $this->praise->getReply();
        $reply->setContent('予以受理');
        $reply->setImages($this->getImages());
        $reply->setAdmissibility(1);

        $crew = new Crew(1);
        $reply->setCrew($crew);
       
        $this->praise->setReply($reply);
        
        return  $this->praise->accept();
    }
    /**
     * @Then  可以查到受理的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchPraise($id);
        $this->accept();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/praises/1/accept';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getReplyData('praises')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            Praise::ACCEPT_STATUS['ACCEPTING'],
            $this->praise->getAcceptStatus()
        );
       
        $this->assertEquals(
            1,
            $this->praise->getReply()->getAdmissibility()
        );

        $this->assertEquals(
            '予以受理',
            $this->praise->getReply()->getContent()
        );

        $this->assertEquals(
            $this->getImages(),
            $this->praise->getReply()->getImages()
        );

        $this->assertEquals(
            1621997357,
            $this->praise->getStatusTime()
        );
        $this->assertEquals(
            1621997357,
            $this->praise->getUpdateTime()
        );
    }
}
