<?php
namespace Base\Sdk\Interaction\Praise\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\Interaction\Adapter\Praise\PraiseRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\ComplaintArrayDataTrait;

/**
 * @Feature: 我是门户网用户,我想要撤销我之前提交上去的信用表扬时,在用户中心-信用表扬中,我可以管理我提交的信用表扬数据
 *           可以撤销受理情况为未受理状态的信用表扬,以便于我维护我的信用表扬信息
 * @Scenario: 撤销信用表扬
 */

class RevokeTest extends TestCase
{
    use ComplaintArrayDataTrait;

    private $revokePraiseObject;

    private $mock;

    public function setUp()
    {
        $this->revokePraiseObject = new Praise();
    }

    public function tearDown()
    {
        unset($this->revokePraiseObject);
    }

    /**
    * @Given: 存在需要撤销的信用表扬
    */
    protected function prepareData()
    {
        $data = $this->getComplaintDetailData("praises");
        $jsonData = json_encode($data);
        
        $revokeData = $this->getComplaintDetailData("praises");
        $revokeData['data']['attributes']['status'] = Praise::STATUS['REVOKED'];

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($revokeData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要撤销的信用表扬
    */
    protected function fetchPraise($id)
    {
        $adapter = new PraiseRestfulAdapter();
        $this->revokePraiseObject = $adapter->fetchOne($id);
        
        return $this->revokePraiseObject;
    }

    /**
    * @And:当我调用撤销函数,期待撤销成功
    */
    protected function revoke()
    {
        return $this->revokePraiseObject->revoke();
    }
    /**
     * @Then  我可以查到该条信用表扬已被撤销
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchPraise($id);
        $this->revoke();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/praises/1/revoke';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals('[]', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Praise::STATUS['REVOKED'], $this->revokePraiseObject->getStatus());
    }
}
