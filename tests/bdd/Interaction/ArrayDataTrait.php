<?php
namespace Base\Sdk\Interaction;

trait ArrayDataTrait
{
    protected function getImages():array
    {
        $images= array(
            array('name' => '图片名称1', 'identify' => 'identify.jpg'),
            array('name' => '图片名称2', 'identify' => 'identify.jpg'),
            array('name' => '图片名称3', 'identify' => 'identify.jpg')
        );

        return $images;
    }

    protected function getContent():string
    {
        $content = '回复内容';
        return $content;
    }

    protected function getAttributesData():array
    {
        $attributes = [
            "type"=>1,
            "title"=>"标题",
            "images"=>$this->getImages(),
            "name"=>"反馈人真实姓名/企业名称",
            "identify"=>"统一社会信用代码/反馈人身份证号",
            "contact"=>"联系方式",
            "content"=> $this->getContent(),
            "certificates"=>array(
                array('name' => '上传身份证/营业执照', 'identify' => 'identify.jpg')
            ),
        ];

        return $attributes;
    }

    protected function getAddRequestData(string $type):array
    {
        return [
            'data' => array(
                "attributes"=>$this->getAttributesData(),
                "relationships"=>array(
                    "member"=>array(
                        "data"=>array(
                            array("type"=>"members","id"=>1)
                        )
                    ),
                    "acceptUserGroup"=>array(
                        "data"=>array(
                            array("type"=>"userGroups","id"=>1)
                        )
                    )
                ),
                "type"=>$type,
            )
        ];
    }

    protected function getUserGroups():array
    {
        return json_decode(' {
            "type": "userGroups",
            "id": "1",
            "attributes": {
                "name": "萍乡市发展和改革委员会",
                "shortName": "发改委",
                "status": 0,
                "createTime": 1516168970,
                "updateTime": 1516168970,
                "statusTime": 0
            }
        }', true);
    }

    protected function getMember():array
    {
        return json_decode('{
            "type": "members",
            "id": "1",
            "attributes": {
                "userName": "用户名",
                "realName": "姓名",
                "cellphone": "18800000000",
                "email": "997809098@qq.com",
                "cardId": "412825199009094567",
                "contactAddress": "雁塔区长延堡街道",
                "securityQuestion": 1,
                "gender": 1,
                "status": 0,
                "createTime": 1621434208,
                "updateTime": 1621435225,
                "statusTime": 1621434736
            }
        }', true);
    }

    protected function getCrew():array
    {
        return json_decode('{
            "type": "crews",
            "id": "1",
            "attributes": {
                "realName": "张科",
                "cardId": "412825199009094532",
                "userName": "18800000000",
                "cellphone": "18800000000",
                "category": 1,
                "purview": [
                    "1",
                    "2",
                    "3"
                ],
                "status": 0,
                "createTime": 1618284031,
                "updateTime": 1619578455,
                "statusTime": 0
            },
            "relationships": {
                "userGroup": {
                    "data": {
                        "type": "userGroups",
                        "id": "1"
                    }
                },
                "department": {
                    "data": {
                        "type": "departments",
                        "id": "3"
                    }
                }
            }
        }', true);
    }

    protected function getReplies():array
    {
        return json_decode('{
            "type": "replies",
            "id": "1",
            "attributes": {
                "content": "予以受理",
                "images": [
                    {
                        "name": "图片名称1",
                        "identify": "identify.jpg"
                    },
                    {
                        "name": "图片名称2",
                        "identify": "identify.jpg"
                    },
                    {
                        "name": "图片名称3",
                        "identify": "identify.jpg"
                    }
                ],
                "admissibility": 1,
                "status": 0,
                "createTime": 1635498739,
                "updateTime": 1635498739,
                "statusTime": 0
            },
            "relationships": {
                "acceptUserGroup": {
                    "data": {
                        "type": "userGroups",
                        "id": "1"
                    }
                },
                "crew": {
                    "data": {
                        "type": "crews",
                        "id": "1"
                    }
                }
            }
        }', true);
    }

    protected function getIncludedData():array
    {
        return [
            $this->getUserGroups(),
            $this->getMember(),
            $this->getCrew(),
            $this->getReplies()
        ];
    }

    protected function getDetailData(string $type, int $acceptStatus = 0):array
    {
        $attributes = $this->getAttributesData();
        $attributes['status'] = 0;
        $attributes['acceptStatus'] = $acceptStatus;
        $data = [
            "type"=> $type,
            "id"=> 1,
            "attributes"=>$attributes,
            "relationships"=> [
                "acceptUserGroup"=> [
                    "data"=> [
                        "type"=> "userGroups",
                        "id"=> "1"
                    ]
                ],
                "member"=> [
                    "data"=> [
                        "type"=> "members",
                        "id"=> "1"
                    ]
                ],
                "reply"=> [
                    "data"=> [
                        "type"=> "replies",
                        "id"=> "1"
                    ]
                ]
            ],
        ];

        $result['data'] = $data;
        $result['included'] = $this->getIncludedData();
        return $result;
    }

    protected function getReplyData(string $type)
    {
        $reply = [
            'data'=>[
                [
                    'type'=>'replies',
                    'attributes'=>[
                        'content' =>'予以受理',
                        'admissibility'=> 1,
                        'images' => $this->getImages(),
                    ],
                    "relationships"=>array(
                        "crew"=>array(
                            "data"=>array(
                                array("type"=>"crews","id"=>1)
                            )
                        )
                    ),
                ]
            ]
        ];

        return [
            'data'=>[
                'attributes'=>[],
                'relationships'=>[
                    'reply'=>$reply
                ],
                'type'=>$type,
            ]
        ];
    }

    protected function getListData(string $type):array
    {
        return [
            "meta"=> [
                "count"=> 1,
                "links"=> [
                    "first"=> 1,
                    "last"=> 1,
                    "prev"=> null,
                    "next"=> 1
                ]
            ],
            'data'=>[
                 $this->getDetailData($type)['data'],
            ],
            'included' =>$this->getIncludedData(),
        ];
    }

    protected function getUnAuditStatusRequest(string $type):array
    {
        return [
            "data"=>[
                "attributes"=>[],
                "relationships"=> [
                    "applyCrew"=> [
                        "data"=>[
                            [
                                "type"=> "crews",
                                "id"=> 1
                            ]
                        ]
                    ]
                ],
                "type"=>$type,
            ]
        ];
    }

    protected function getRejectRequestData(string $type):array
    {
        $data = $this->getUnAuditStatusRequest($type);
        $data['data']['attributes']['rejectReason'] = '驳回原因';

        return $data;
    }
}
