<?php
namespace Base\Sdk\Interaction\UnAuditFeedback\Browse;

use Base\Sdk\Interaction\Adapter\UnAuditFeedback\UnAuditFeedbackRestfulAdapter;

trait SearchDataTrait
{
    protected function getUnAuditFeedbackList(array $filter = [])
    {
        $adapter = new UnAuditFeedbackRestfulAdapter();

        list($count, $list) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
   
        return $list;
    }
}
