<?php
namespace Base\Sdk\Interaction\UnAuditFeedback\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditFeedback;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature:  我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈管理权限,在问题反馈模块中
 *           查看前台用户提交的待审核和已驳回的问题反馈信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的待审核和已驳回的问题反馈信息
 * @Scenario: 查看查看问题反馈数据列表列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, FeedBackArrayDataTrait;

    private $unAuditFeedback;

    private $mock;

    public function setUp()
    {
        $this->unAuditFeedback = new UnAuditFeedback();
    }

    public function tearDown()
    {
        unset($this->unAuditFeedback);
    }

    /**
    * @Given: 存在查看问题反馈数据审核列表数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackListData('unAuditedFeedbacks');
        $data['data'][0]['attributes']['applyStatus'] = UnAuditFeedback::APPLY_STATUS['PENDING'];

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看查看问题反馈审核数据列表时
     */
    public function fetchUnAuditFeedbackList()
    {
        $filter = [];
        $list =$this->getUnAuditFeedbackList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见查看问题反馈审核数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUnAuditFeedbackList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedFeedbacks';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $unAuditFeedbackList = $this->fetchUnAuditFeedbackList();

        $unAuditFeedbackArray = $this->getFeedBackListData('unAuditedFeedbacks')['data'];
        
        foreach ($unAuditFeedbackList as $object) {
            foreach ($unAuditFeedbackArray as $item) {
                if ($object->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['title'], $object->getTitle());
                    $this->assertEquals($item['attributes']['content'], $object->getContent());

                    $this->assertEquals($item['attributes']['status'], $object->getStatus());

                    $this->assertEquals(
                        UnAuditFeedback::APPLY_STATUS['PENDING'],
                        $object->getApplyStatus()
                    );
        
                    $this->assertEquals(
                        $item['attributes']['acceptStatus'],
                        $object->getAcceptStatus()
                    );

                    $this->assertEquals(
                        $item['relationships']['acceptUserGroup']['data']['id'],
                        $object->getAcceptUserGroup()->getId()
                    );

                    $this->assertEquals(
                        $item['relationships']['member']['data']['id'],
                        $object->getMember()->getId()
                    );

                    $this->assertEquals(
                        0,
                        $object->getStatusTime()
                    );
                    $this->assertEquals(
                        0,
                        $object->getCreateTime()
                    );
                    $this->assertEquals(
                        0,
                        $object->getUpdateTime()
                    );
                }
            }
        }
    }
}
