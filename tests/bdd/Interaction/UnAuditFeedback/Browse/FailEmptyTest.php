<?php
namespace Base\Sdk\Interaction\UnAuditFeedback\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditFeedback;
use Base\Sdk\Interaction\Adapter\UnAuditFeedback\UnAuditFeedbackRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈审核权限,在问题反馈模块中
 *           查看前台用户提交的问题反馈信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的问题反馈信息
 * @Scenario: 查看问题反馈数据审核列表-异常不存在数据
 */

class FailEmptyTest extends TestCase
{
    private $unAuditUnAuditFeedback;

    private $mock;

    public function setUp()
    {
        $this->unAuditUnAuditFeedback = new UnAuditFeedback();
    }

    public function tearDown()
    {
        unset($this->unAuditUnAuditFeedback);
    }

    /**
    * @Given: 不存在问题反馈审核数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看问题反馈审核列表时
     */
    public function fetchUnAuditFeedbackList()
    {
        $adapter = new UnAuditFeedbackRestfulAdapter();

        list($count, $this->unAuditUnAuditFeedback) = $adapter
        ->search();
        unset($count);

        return $this->unAuditUnAuditFeedback;
    }

    /**
     * @Then: 获取不到审核数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchUnAuditFeedbackList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedFeedbacks';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->unAuditUnAuditFeedback);
    }
}
