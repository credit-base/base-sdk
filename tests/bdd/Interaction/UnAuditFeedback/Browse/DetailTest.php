<?php
namespace Base\Sdk\Interaction\UnAuditFeedback\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditFeedback;
use Base\Sdk\Interaction\Adapter\UnAuditFeedback\UnAuditFeedbackRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈审核权限,在问题反馈模块中
 *           查看前台用户提交的问题反馈信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的问题反馈信息
 * @Scenario: 查看问题反馈审核数据详情
 */

class DetailTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $unAuditFeedback;

    private $mock;

    public function setUp()
    {
        $this->unAuditFeedback = new UnAuditFeedback();
    }

    public function tearDown()
    {
        unset($this->unAuditFeedback);
    }

    /**
    * @Given: 存在一条问题反馈审核数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("unAuditedFeedbacks", UnAuditFeedback::ACCEPT_STATUS['ACCEPTING']);
        $data['data']['attributes']['applyStatus'] = UnAuditFeedback::APPLY_STATUS['PENDING'];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条问题反馈审核数据详情时
    */
    protected function fetchUnAuditFeedback($id)
    {

        $adapter = new UnAuditFeedbackRestfulAdapter();

        $this->unAuditFeedback = $adapter->fetchOne($id);
      
        return $this->unAuditFeedback;
    }

    /**
     * @Then 我可以看见该条问题反馈审核数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditFeedback($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedFeedbacks/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getFeedBackDetailData("unAuditedFeedbacks")['data'];

        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->unAuditFeedback->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->unAuditFeedback->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(
            $data['attributes']['title'],
            $this->unAuditFeedback->getTitle()
        );
        $this->assertEquals(
            $data['attributes']['content'],
            $this->unAuditFeedback->getContent()
        );
       
        $this->assertEquals(
            UnAuditFeedback::STATUS['NORMAL'],
            $this->unAuditFeedback->getStatus()
        );
        $this->assertEquals(
            UnAuditFeedback::APPLY_STATUS['PENDING'],
            $this->unAuditFeedback->getApplyStatus()
        );

        $this->assertEquals(
            0,
            $this->unAuditFeedback->getStatusTime()
        );
        $this->assertEquals(
            0,
            $this->unAuditFeedback->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->unAuditFeedback->getUpdateTime()
        );
    }
}
