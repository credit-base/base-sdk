<?php
namespace Base\Sdk\Interaction\UnAuditFeedback\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditFeedback;
use Base\Sdk\Interaction\Adapter\UnAuditFeedback\UnAuditFeedbackRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈管理权限,
 *           当我需要想要对某个受理情况为已受理且审核状态为已驳回的问题反馈数据进行重新受理时,在业务管理-问题反馈的受理表中
 *           对前台用户提交上来的问题反馈进行重新受理，并提交我填写的受理依据,以便于我维护问题反馈信息
 * @Scenario: 重新受理问题反馈审核数据
 */

class ResubmitAcceptTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $unAuditFeedback;

    private $mock;

    public function setUp()
    {
        $this->unAuditFeedback = new UnAuditFeedback();
    }

    public function tearDown()
    {
        unset($this->unAuditFeedback);
    }

    /**
    * @Given: 我并未重新受理过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("unAuditedFeedbacks", UnAuditFeedback::ACCEPT_STATUS['ACCEPTING']);

        $jsonData = json_encode($data);
        
        $acceptData = $this->getFeedBackDetailData("unAuditedFeedbacks", UnAuditFeedback::ACCEPT_STATUS['ACCEPTING']);
        $acceptData['data']['attributes']['applyStatus'] = UnAuditFeedback::APPLY_STATUS['REJECT'];
        $acceptData['data']['attributes']['statusTime'] = 1621997357;
        $acceptData['data']['attributes']['updateTime'] = 1621997357;
  
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($acceptData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要重新受理的问题反馈数据
    */
    protected function fetchUnAuditFeedback($id)
    {
        $adapter = new UnAuditFeedbackRestfulAdapter();
        $this->unAuditFeedback = $adapter->fetchOne($id);
        
        return $this->unAuditFeedback;
    }

    /**
    * @And:当我调用重新受理函数,期待返回true
    */
    protected function resubmit()
    {
        $reply = $this->unAuditFeedback->getReply();
        $reply->setContent('予以受理');
        $reply->setImages($this->getImages());
        $reply->setAdmissibility(1);

        $crew = new Crew(1);
        $reply->setCrew($crew);
       
        $this->unAuditFeedback->setReply($reply);
        
        return  $this->unAuditFeedback->resubmit();
    }
    /**
     * @Then  可以查到重新受理的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditFeedback($id);
        $this->resubmit();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedFeedbacks/1/resubmit';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getReplyData('unAuditedFeedbacks')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            UnAuditFeedback::ACCEPT_STATUS['ACCEPTING'],
            $this->unAuditFeedback->getAcceptStatus()
        );

        $this->assertEquals(
            UnAuditFeedback::APPLY_STATUS['REJECT'],
            $this->unAuditFeedback->getApplyStatus()
        );
       
        $this->assertEquals(
            1,
            $this->unAuditFeedback->getReply()->getAdmissibility()
        );

        $this->assertEquals(
            '予以受理',
            $this->unAuditFeedback->getReply()->getContent()
        );

        $this->assertEquals(
            $this->getImages(),
            $this->unAuditFeedback->getReply()->getImages()
        );

        $this->assertEquals(
            1621997357,
            $this->unAuditFeedback->getStatusTime()
        );
        $this->assertEquals(
            1621997357,
            $this->unAuditFeedback->getUpdateTime()
        );
    }
}
