<?php
namespace Base\Sdk\Interaction\UnAuditFeedback\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\UnAuditFeedback;
use Base\Sdk\Interaction\Adapter\UnAuditFeedback\UnAuditFeedbackRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈审核权限,当我需要想要对某个待审核的问题反馈数据进行审核时,在业务审核-审核表中
 *           对前台用户提交上来的问题反馈进行审核通过或审核驳回操作,以便于我维护问题反馈审核信息
 * @Scenario: 审核通过
 */

class ApproveTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $unAuditFeedbackObject;

    private $mock;

    public function setUp()
    {
        $this->unAuditFeedbackObject = new UnAuditFeedback();
    }

    public function tearDown()
    {
        unset($this->unAuditFeedbackObject);
    }

    /**
    * @Given: 存在一条待审核的问题反馈审核数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("unAuditedFeedbacks", UnAuditFeedback::ACCEPT_STATUS['ACCEPTING']);
        $data['data']['attributes']['applyStatus'] = UnAuditFeedback::APPLY_STATUS['PENDING'];
        $jsonData = json_encode($data);
        
        $approveData = $this->getFeedBackDetailData("unAuditedFeedbacks", UnAuditFeedback::ACCEPT_STATUS['ACCEPTING']);
        $approveData['data']['attributes']['applyStatus'] = UnAuditFeedback::APPLY_STATUS['APPROVE'];

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($approveData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要审核通过的问题反馈
    */
    protected function fetchUnAuditFeedback($id)
    {
        $adapter = new UnAuditFeedbackRestfulAdapter();
        $this->unAuditFeedbackObject = $adapter->fetchOne($id);

        
        return $this->unAuditFeedbackObject;
    }

    /**
    * @And:当我调用审核通过函数,期待返回true
    */
    protected function approve()
    {
        $crew = new Crew(1);
        $this->unAuditFeedbackObject->setApplyCrew($crew);

        return $this->unAuditFeedbackObject->approve();
    }
    /**
     * @Then 数据已经被审核通过
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditFeedback($id);
        $this->approve();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedFeedbacks/1/approve';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getUnAuditStatusRequest('unAuditedFeedbacks')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            UnAuditFeedback::APPLY_STATUS['APPROVE'],
            $this->unAuditFeedbackObject->getApplyStatus()
        );

        $this->assertEquals(
            UnAuditFeedback::ACCEPT_STATUS['ACCEPTING'],
            $this->unAuditFeedbackObject->getAcceptStatus()
        );
        
        $this->assertEquals(
            1,
            $this->unAuditFeedbackObject->getApplyCrew()->getId()
        );
    }
}
