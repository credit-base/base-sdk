<?php
namespace Base\Sdk\Interaction\Feedback\Browse;

use Base\Sdk\Interaction\Adapter\Feedback\FeedbackRestfulAdapter;

trait SearchDataTrait
{
    protected function getFeedbackList(array $filter = [])
    {
        $adapter = new FeedbackRestfulAdapter();

        list($count, $feedbackList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
   
        return $feedbackList;
    }
}
