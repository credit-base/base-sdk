<?php
namespace Base\Sdk\Interaction\Feedback\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Feedback;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature:  我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈管理权限,在问题反馈模块中
 *           查看前台用户提交的已审核通过的问题反馈信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的问题反馈信息
 * @Scenario: 查看查看问题反馈数据列表列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, FeedBackArrayDataTrait;

    private $feedback;

    private $mock;

    public function setUp()
    {
        $this->feedback = new Feedback();
    }

    public function tearDown()
    {
        unset($this->feedback);
    }

    /**
    * @Given: 存在查看问题反馈数据列表数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackListData('feedbacks');
        
        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看查看问题反馈数据列表时
     */
    public function fetchFeedbackList()
    {
        $filter = [];
        $list =$this->getFeedbackList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见查看问题反馈数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchFeedbackList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'feedbacks';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $feedbackList = $this->fetchFeedbackList();

        $feedbackArray = $this->getFeedBackListData('feedbacks')['data'];
        
        foreach ($feedbackList as $feedback) {
            foreach ($feedbackArray as $item) {
                if ($feedback->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['title'], $feedback->getTitle());
                    $this->assertEquals($item['attributes']['content'], $feedback->getContent());

                    $this->assertEquals($item['attributes']['status'], $feedback->getStatus());
                    $this->assertEquals(
                        $item['relationships']['acceptUserGroup']['data']['id'],
                        $feedback->getAcceptUserGroup()->getId()
                    );

                    $this->assertEquals(
                        $item['relationships']['member']['data']['id'],
                        $feedback->getMember()->getId()
                    );

                    $this->assertEquals(
                        0,
                        $feedback->getStatusTime()
                    );
                    $this->assertEquals(
                        0,
                        $feedback->getCreateTime()
                    );
                    $this->assertEquals(
                        0,
                        $feedback->getUpdateTime()
                    );
                }
            }
        }
    }
}
