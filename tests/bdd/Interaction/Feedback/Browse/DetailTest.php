<?php
namespace Base\Sdk\Interaction\Feedback\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\Interaction\Adapter\Feedback\FeedbackRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈管理权限,在问题反馈模块中
 *           查看前台用户提交的已审核通过的问题反馈信息,通过列表和详情的展示效果,以便于我清晰的了解用户所提交的已审核通过的问题反馈信息
 * @Scenario: 查看问题反馈数据详情
 */

class DetailTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $feedback;

    private $mock;

    public function setUp()
    {
        $this->feedback = new Feedback();
    }

    public function tearDown()
    {
        unset($this->feedback);
    }

    /**
    * @Given: 存在一条问题反馈数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("feedbacks");

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条问题反馈数据详情时
    */
    protected function fetchFeedback($id)
    {

        $adapter = new FeedbackRestfulAdapter();

        $this->feedback = $adapter->fetchOne($id);
      
        return $this->feedback;
    }

    /**
     * @Then  我可以看见该条问题反馈数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchFeedback($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'feedbacks/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getFeedBackDetailData("feedbacks")['data'];

        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->feedback->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->feedback->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(
            $data['attributes']['title'],
            $this->feedback->getTitle()
        );
        $this->assertEquals(
            $data['attributes']['content'],
            $this->feedback->getContent()
        );
        
        $this->assertEquals(Feedback::STATUS['NORMAL'], $this->feedback->getStatus());

        $this->assertEquals(
            0,
            $this->feedback->getStatusTime()
        );
        $this->assertEquals(
            0,
            $this->feedback->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->feedback->getUpdateTime()
        );
    }
}
