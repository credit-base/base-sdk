<?php
namespace Base\Sdk\Interaction\Feedback\Add;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Feedback;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\UserGroup\Model\UserGroup;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是门户网用户,当我需要想要对某个信用数据进行投诉时,在信用公示栏目-信用反馈中,新增一个问题反馈数据
 *           通过新增问题反馈界面，并根据我所提交的投诉信息数据进行新增,以便于我维护问题反馈信息
 * @Scenario: 新增问题反馈
 */

class SuccessTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $feedback;

    private $mock;

    public function setUp()
    {
        $this->feedback = new Feedback();
    }

    public function tearDown()
    {
        unset($this->feedback);
    }

    /**
    * @Given: 我并未新增过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("feedbacks");
        
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;
        $data['data']['attributes']['createTime'] = 1635498202;

        $jsonData = json_encode($data);
       
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
       
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我调用添加函数,期待返回true
    */
    protected function add()
    {
        $addData = $this->getAttributesData();
        $this->feedback = new Feedback();
        $this->feedback->setTitle($addData['title']);
     
        $acceptUserGroup = new UserGroup(1);
        $this->feedback->setAcceptUserGroup($acceptUserGroup);
        $this->feedback->setContent($addData['content']);
        $member = new Member(1);
        $this->feedback->setMember($member);
      
        return  $this->feedback->add();
    }
    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        
        $this->add();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/feedbacks';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getFeedBackAddRequestData("feedbacks")), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $data = $this->getFeedBackDetailData("feedbacks")['data'];

        $this->assertEquals($data['attributes']['title'], $this->feedback->getTitle());
        $this->assertEquals($data['attributes']['content'], $this->feedback->getContent());
        $this->assertEquals(
            $data['relationships']['acceptUserGroup']['data']['id'],
            $this->feedback->getMember()->getId()
        );
        $this->assertEquals(
            $data['relationships']['member']['data']['id'],
            $this->feedback->getAcceptUserGroup()->getId()
        );

        $this->assertEquals(Feedback::STATUS['NORMAL'], $this->feedback->getStatus());

        $this->assertEquals(
            0,
            $this->feedback->getStatusTime()
        );
        $this->assertEquals(
            1635498202,
            $this->feedback->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->feedback->getUpdateTime()
        );
    }
}
