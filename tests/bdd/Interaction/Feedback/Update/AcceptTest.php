<?php
namespace Base\Sdk\Interaction\Feedback\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\Interaction\Adapter\Feedback\FeedbackRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有问题反馈管理权限,当我需要想要对某个问题反馈数据进行受理处理时,在业务管理-问题反馈中
 *           对前台用户提交上来的问题反馈进行受理处理，并提交我填写的受理依据,以便于我维护问题反馈信息
 * @Scenario: 受理问题反馈
 */

class AcceptTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $feedback;

    private $mock;

    public function setUp()
    {
        $this->feedback = new Feedback();
    }

    public function tearDown()
    {
        unset($this->feedback);
    }

    /**
    * @Given: 我并未受理过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("feedbacks");

        $jsonData = json_encode($data);
        
        $acceptData = $this->getFeedBackDetailData("feedbacks", Feedback::ACCEPT_STATUS['ACCEPTING']);
        $acceptData['data']['attributes']['statusTime'] = 1621997357;
        $acceptData['data']['attributes']['updateTime'] = 1621997357;
  
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($acceptData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要受理的问题反馈数据
    */
    protected function fetchFeedback($id)
    {
        $adapter = new FeedbackRestfulAdapter();
        $this->feedback = $adapter->fetchOne($id);
        
        return $this->feedback;
    }

    /**
    * @And:当我调用受理函数,期待返回true
    */
    protected function accept()
    {
        $reply = $this->feedback->getReply();
        $reply->setContent('予以受理');
        $reply->setImages($this->getImages());
        $reply->setAdmissibility(1);

        $crew = new Crew(1);
        $reply->setCrew($crew);
       
        $this->feedback->setReply($reply);
        
        return  $this->feedback->accept();
    }
    /**
     * @Then  可以查到受理的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchFeedback($id);
        $this->accept();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/feedbacks/1/accept';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getReplyData('feedbacks')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            Feedback::ACCEPT_STATUS['ACCEPTING'],
            $this->feedback->getAcceptStatus()
        );
       
        $this->assertEquals(
            1,
            $this->feedback->getReply()->getAdmissibility()
        );

        $this->assertEquals(
            '予以受理',
            $this->feedback->getReply()->getContent()
        );

        $this->assertEquals(
            $this->getImages(),
            $this->feedback->getReply()->getImages()
        );

        $this->assertEquals(
            1621997357,
            $this->feedback->getStatusTime()
        );
        $this->assertEquals(
            1621997357,
            $this->feedback->getUpdateTime()
        );
    }
}
