<?php
namespace Base\Sdk\Interaction\Feedback\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\Interaction\Adapter\Feedback\FeedbackRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Interaction\FeedBackArrayDataTrait;

/**
 * @Feature: 我是门户网用户,我想要撤销我之前提交上去的问题反馈时,在用户中心-问题反馈中,我可以管理我提交的问题反馈数据
 *           可以撤销受理情况为未受理状态的问题反馈,以便于我维护我的问题反馈信息
 * @Scenario: 撤销问题反馈
 */

class RevokeTest extends TestCase
{
    use FeedBackArrayDataTrait;

    private $revokeFeedbackObject;

    private $mock;

    public function setUp()
    {
        $this->revokeFeedbackObject = new Feedback();
    }

    public function tearDown()
    {
        unset($this->revokeFeedbackObject);
    }

    /**
    * @Given: 存在需要撤销的问题反馈
    */
    protected function prepareData()
    {
        $data = $this->getFeedBackDetailData("feedbacks");
        $jsonData = json_encode($data);
        
        $revokeData = $this->getFeedBackDetailData("feedbacks");
        $revokeData['data']['attributes']['status'] = Feedback::STATUS['REVOKED'];

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($revokeData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要撤销的问题反馈
    */
    protected function fetchFeedback($id)
    {
        $adapter = new FeedbackRestfulAdapter();
        $this->revokeFeedbackObject = $adapter->fetchOne($id);
        
        return $this->revokeFeedbackObject;
    }

    /**
    * @And:当我调用撤销函数,期待撤销成功
    */
    protected function revoke()
    {
        return $this->revokeFeedbackObject->revoke();
    }
    /**
     * @Then  我可以查到该条问题反馈已被撤销
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchFeedback($id);
        $this->revoke();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/feedbacks/1/revoke';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals('[]', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Feedback::STATUS['REVOKED'], $this->revokeFeedbackObject->getStatus());
    }
}
