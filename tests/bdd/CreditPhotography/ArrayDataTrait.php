<?php
namespace Base\Sdk\CreditPhotography;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;

trait ArrayDataTrait
{
    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function getCreditPhotographyDetailData(
        int $id,
        int $applyStatus = IApplyAble::APPLY_STATUS['APPROVE'],
        int $status = CreditPhotography::STATUS['NOMAL']
    ):array {
        return [
            "meta"=> [],
            "data"=> [
                "type"=> "creditPhotography",
                "id"=> $id,
                "attributes"=> [
                    "description"=> "人无信不立,业无信不兴,国无信不宁",
                    "attachments"=> [
                        [
                            "name"=> "name",
                            "identify"=> "identify.jpg"
                        ],
                        [
                            "name"=> "name",
                            "identify"=> "identify.jpg"
                        ],
                        [
                            "name"=> "name",
                            "identify"=> "identify.jpg"
                        ]
                    ],
                    "applyStatus"=> $applyStatus,
                    "rejectReason"=> "驳回原因",
                    "status"=> $status,
                    "createTime"=> 1622285937,
                    "updateTime"=> 1622286017,
                    "statusTime"=> 1622286017
                ],
                "relationships"=> [
                    "member"=> [
                        "data"=> [
                            "type"=> "members",
                            "id"=> "2"
                        ]
                    ],
                    "applyCrew"=> [
                        "data"=> [
                            "type"=> "crews",
                            "id"=> "1"
                        ]
                    ]
                ],
                "links"=> [
                    "self"=> "127.0.0.1:8089/creditPhotography/2"
                ]
            ],
            "included"=> [
                [
                    "type"=> "members",
                    "id"=> "2",
                    "attributes"=> [
                        "userName"=> "测试用户名",
                        "realName"=> "姓名",
                        "cellphone"=> "18800000001",
                        "email"=> "997809099@qq.com",
                        "cardId"=> "412825199009094567",
                        "contactAddress"=> "雁塔区长延堡街道",
                        "gender"=> 0,
                        "status"=> 0,
                        "createTime"=> 1621434329,
                        "updateTime"=> 1621434329,
                        "statusTime"=> 0
                    ]
                ],
                [
                    "type"=> "crews",
                    "id"=> "1",
                    "attributes"=> [
                        "realName"=> "张科",
                        "cardId"=> "412825199009094532",
                        "userName"=> "18800000000",
                        "cellphone"=> "18800000000",
                        "category"=> 1,
                        "purview"=> [
                            "1",
                            "2",
                            "3"
                        ],
                        "status"=> 0,
                        "createTime"=> 1618284031,
                        "updateTime"=> 1619578455,
                        "statusTime"=> 0
                    ],
                    "relationships"=> [
                        "userGroup"=> [
                            "data"=> [
                                "type"=> "userGroups",
                                "id"=> "1"
                            ]
                        ],
                        "department"=> [
                            "data"=> [
                                "type"=> "departments",
                                "id"=> "3"
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function getCreditPhotographyListData():array
    {
        return [
            "meta"=> [
                "count"=> 5,
                "links"=> [
                    "first"=> null,
                    "last"=> null,
                    "prev"=> null,
                    "next"=> null
                ]
            ],
            "links"=> [
                "first"=> null,
                "last"=> null,
                "prev"=> null,
                "next"=> null
            ],
            "data"=> [
                [
                    "type"=> "creditPhotography",
                    "id"=> "4",
                    "attributes"=> [
                        "description"=> "诚信是一种软实力,更是一种竞争力",
                        "attachments"=> [
                            [
                                "name"=> "附件1",
                                "identify"=> "附件1identify.jpg"
                            ],
                            [
                                "name"=> "附件2",
                                "identify"=> "附件2identify.jpg"
                            ],
                            [
                                "name"=> "附件3",
                                "identify"=> "附件1identify.jpg"
                            ]
                        ],
                        "applyStatus"=> -2,
                        "rejectReason"=> "数据重复",
                        "status"=> 0,
                        "createTime"=> 1622286210,
                        "updateTime"=> 1622286244,
                        "statusTime"=> 1622286244
                    ],
                    "relationships"=> [
                        "member"=> [
                            "data"=> [
                                "type"=> "members",
                                "id"=> "4"
                            ]
                        ],
                        "applyCrew"=> [
                            "data"=> [
                                "type"=> "crews",
                                "id"=> "2"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/creditPhotography/4"
                    ]
                ],
                [
                    "type"=> "creditPhotography",
                    "id"=> "1",
                    "attributes"=> [
                        "description"=> "描述",
                        "attachments"=> [
                            [
                                "name"=> "凭证1",
                                "identify"=> "凭证1identify.jpg"
                            ],
                            [
                                "name"=> "凭证2",
                                "identify"=> "凭证2identify.jpg"
                            ],
                            [
                                "name"=> "凭证3",
                                "identify"=> "凭证3identify.jpg"
                            ]
                        ],
                        "applyStatus"=> 0,
                        "rejectReason"=> "",
                        "status"=> -2,
                        "createTime"=> 1622285898,
                        "updateTime"=> 1622285977,
                        "statusTime"=> 1622285977
                    ],
                    "relationships"=> [
                        "member"=> [
                            "data"=> [
                                "type"=> "members",
                                "id"=> "1"
                            ]
                        ],
                        "applyCrew"=> [
                            "data"=> [
                                "type"=> "crews",
                                "id"=> "3"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/creditPhotography/1"
                    ]
                ],
                [
                    "type"=> "creditPhotography",
                    "id"=> "5",
                    "attributes"=> [
                        "description"=> "信用随手拍，拍拍更随心",
                        "attachments"=> [
                            [
                                "name"=> "图片1",
                                "identify"=> "图片1identify.jpg"
                            ],
                            [
                                "name"=> "图片2",
                                "identify"=> "图片2identify.jpg"
                            ],
                            [
                                "name"=> "图片3",
                                "identify"=> "图片3identify.jpg"
                            ]
                        ],
                        "applyStatus"=> 0,
                        "rejectReason"=> "",
                        "status"=> 0,
                        "createTime"=> 1622286212,
                        "updateTime"=> 1622286212,
                        "statusTime"=> 0
                    ],
                    "relationships"=> [
                        "member"=> [
                            "data"=> [
                                "type"=> "members",
                                "id"=> "4"
                            ]
                        ],
                        "applyCrew"=> [
                            "data"=> [
                                "type"=> "crews",
                                "id"=> "3"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/creditPhotography/5"
                    ]
                ],
                [
                    "type"=> "creditPhotography",
                    "id"=> "2",
                    "attributes"=> [
                        "description"=> "人无信不立,业无信不兴,国无信不宁",
                        "attachments"=> [
                            [
                                "name"=> "特照1",
                                "identify"=> "特照1identify.jpg"
                            ],
                            [
                                "name"=> "特照2",
                                "identify"=> "特照2identify.jpg"
                            ],
                            [
                                "name"=> "特照3",
                                "identify"=> "特照3identify.jpg"
                            ]
                        ],
                        "applyStatus"=> 2,
                        "rejectReason"=> "",
                        "status"=> 0,
                        "createTime"=> 1622285937,
                        "updateTime"=> 1622286017,
                        "statusTime"=> 1622286017
                    ],
                    "relationships"=> [
                        "member"=> [
                            "data"=> [
                                "type"=> "members",
                                "id"=> "2"
                            ]
                        ],
                        "applyCrew"=> [
                            "data"=> [
                                "type"=> "crews",
                                "id"=> "1"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/creditPhotography/2"
                    ]
                ],
                [
                    "type"=> "creditPhotography",
                    "id"=> "3",
                    "attributes"=> [
                        "description"=> "严于利己，宽于待人",
                        "attachments"=> [
                            [
                                "name"=> "特色1",
                                "identify"=> "特色1identify.jpg"
                            ],
                            [
                                "name"=> "特色2",
                                "identify"=> "特色2identify.jpg"
                            ],
                            [
                                "name"=> "特色3",
                                "identify"=> "特色3identify.jpg"
                            ]
                        ],
                        "applyStatus"=> 2,
                        "rejectReason"=> "",
                        "status"=> 0,
                        "createTime"=> 1622285954,
                        "updateTime"=> 1622286190,
                        "statusTime"=> 1622286190
                    ],
                    "relationships"=> [
                        "member"=> [
                            "data"=> [
                                "type"=> "members",
                                "id"=> "2"
                            ]
                        ],
                        "applyCrew"=> [
                            "data"=> [
                                "type"=> "crews",
                                "id"=> "1"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/creditPhotography/3"
                    ]
                ]
            ],
            "included"=> [
                [
                    "type"=> "members",
                    "id"=> "4",
                    "attributes"=> [
                        "userName"=> "18800000001",
                        "realName"=> "白玉",
                        "cellphone"=> "18800000011",
                        "email"=> "997934302@qq.com",
                        "cardId"=> "412825199009094531",
                        "contactAddress"=> "长安区",
                        "gender"=> 2,
                        "status"=> 0,
                        "createTime"=> 1516174523,
                        "updateTime"=> 1516174523,
                        "statusTime"=> 0
                    ]
                ],
                [
                    "type"=> "crews",
                    "id"=> "2",
                    "attributes"=> [
                        "realName"=> "王文",
                        "cardId"=> "412825199009094533",
                        "userName"=> "18800000001",
                        "cellphone"=> "18800000001",
                        "category"=> 3,
                        "purview"=> [],
                        "status"=> -2,
                        "createTime"=> 1618284059,
                        "updateTime"=> 1619578659,
                        "statusTime"=> 1619578659
                    ],
                    "relationships"=> [
                        "userGroup"=> [
                            "data"=> [
                                "type"=> "userGroups",
                                "id"=> "1"
                            ]
                        ],
                        "department"=> [
                            "data"=> [
                                "type"=> "departments",
                                "id"=> "1"
                            ]
                        ]
                    ]
                ],
                [
                    "type"=> "members",
                    "id"=> "1",
                    "attributes"=> [
                        "userName"=> "用户名",
                        "realName"=> "姓名",
                        "cellphone"=> "18800000000",
                        "email"=> "997809098@qq.com",
                        "cardId"=> "412825199009094567",
                        "contactAddress"=> "雁塔区长延堡街道",
                        "gender"=> 1,
                        "status"=> 0,
                        "createTime"=> 1621434208,
                        "updateTime"=> 1621435225,
                        "statusTime"=> 1621434736
                    ]
                ],
                [
                    "type"=> "crews",
                    "id"=> "3",
                    "attributes"=> [
                        "realName"=> "李一一",
                        "cardId"=> "412825199009095679",
                        "userName"=> "18892037640",
                        "cellphone"=> "18892037640",
                        "category"=> 4,
                        "purview"=> [],
                        "status"=> 0,
                        "createTime"=> 1622286731,
                        "updateTime"=> 1622286731,
                        "statusTime"=> 0
                    ],
                    "relationships"=> [
                        "userGroup"=> [
                            "data"=> [
                                "type"=> "userGroups",
                                "id"=> "2"
                            ]
                        ],
                        "department"=> [
                            "data"=> [
                                "type"=> "departments",
                                "id"=> "2"
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getCreditPhotographyStatusRequest() : array
    {
        return [
            "data"=>[
                "type"=>"creditPhotography",
                "attributes"=>[],
                "relationships"=> [
                    "applyCrew"=> [
                        "data"=>[
                            [
                                "type"=> "crews",
                                "id"=> 1
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getCreditPhotographyAddRequest() : array
    {
        return [
            "data"=>[
                "type"=>"creditPhotography",
                "attributes"=> [
                    "description"=> "人无信不立,业无信不兴,国无信不宁",
                    "attachments"=> [
                        [
                            "name"=> "name",
                            "identify"=> "identify.jpg"
                        ],
                        [
                            "name"=> "name",
                            "identify"=> "identify.jpg"
                        ],
                        [
                            "name"=> "name",
                            "identify"=> "identify.jpg"
                        ]
                    ],
                ],
                "relationships"=> [
                    "member"=> [
                        "data"=>[
                            [
                                "type"=> "members",
                                "id"=> 2
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
