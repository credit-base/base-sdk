<?php
namespace Base\Sdk\CreditPhotography\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\CreditPhotography\ArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,当有审核状态为已通过的信用随手拍数据时,我可以在OA的信用随手拍审核的发布表查看该条数据
 *           我可以查看其内容,通过列表中已展现的数据内容, 以便于我了解详情
 * @Scenario: 查看信用随手拍列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ArrayDataTrait;

    private $creditPhotography;

    private $mock;

    public function setUp()
    {
        $this->creditPhotography = new CreditPhotography();
    }

    public function tearDown()
    {
        unset($this->creditPhotography);
    }

    /**
    * @Given: 存在信用随手拍数据
    */
    protected function prepareData()
    {
        $data = $this->getCreditPhotographyListData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看信用随手拍列表时
     */
    public function fetchCreditPhotographyList()
    {
        $filter = [];
        $list =$this->getCreditPhotographyList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见信用随手拍数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchCreditPhotographyList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'creditPhotography';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $creditPhotographyList = $this->fetchCreditPhotographyList();

        $creditPhotographyArray = $this->getCreditPhotographyListData()['data'];
        
        foreach ($creditPhotographyList as $object) {
            foreach ($creditPhotographyArray as $item) {
                if ($item['id'] == $object->getId()) {
                    $this->assertEquals($item['attributes']['description'], $object->getDescription());
                    $this->assertEquals($item['attributes']['attachments'], $object->getAttachments());
                    $this->assertEquals($item['attributes']['applyStatus'], $object->getApplyStatus());
                    $this->assertEquals($item['attributes']['rejectReason'], $object->getRejectReason());
                    $this->assertEquals($item['attributes']['status'], $object->getStatus());
                
                    $this->assertEquals(
                        $item['attributes']['statusTime'],
                        $object->getStatusTime()
                    );
                    $this->assertEquals(
                        $item['attributes']['createTime'],
                        $object->getCreateTime()
                    );
                    $this->assertEquals(
                        $item['attributes']['updateTime'],
                        $object->getUpdateTime()
                    );
                }
            }
        }
    }
}
