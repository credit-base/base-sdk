<?php
namespace Base\Sdk\CreditPhotography\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Adapter\CreditPhotography\CreditPhotographyRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,当有审核状态为已通过的信用随手拍数据时,我可以在OA的信用随手拍审核的发布表查看该条数据
 *           我可以查看其内容,通过列表中已展现的数据内容, 以便于我了解详情
 * @Scenario: 查看信用随手拍列表-异常数据不存在
 */

class FailEmptyTest extends TestCase
{
    private $creditPhotography;

    private $mock;

    public function setUp()
    {
        $this->creditPhotography = new CreditPhotography();
    }

    public function tearDown()
    {
        unset($this->creditPhotography);
    }

    /**
    * @Given: 不存在新闻数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看新闻列表时
     */
    public function fetchCreditPhotographyList()
    {
        $adapter = new CreditPhotographyRestfulAdapter();

        list($count, $this->creditPhotography) = $adapter
        ->search();
        unset($count);

        return $this->creditPhotography;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchCreditPhotographyList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'creditPhotography';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->creditPhotography);
    }
}
