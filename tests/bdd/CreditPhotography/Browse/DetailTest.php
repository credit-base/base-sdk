<?php
namespace Base\Sdk\CreditPhotography\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Adapter\CreditPhotography\CreditPhotographyRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\CreditPhotography\ArrayDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,当有审核状态为已通过的信用随手拍数据时,
 *           我可以在OA的信用随手拍审核的发布表查看该条数据
 *           我可以查看其内容,通过列表中已展现的数据内容, 以便于我了解详情
 * @Scenario: 查看≈≈数据详情
 */

class DetailTest extends TestCase
{
    use ArrayDataTrait;

    private $creditPhotography;

    private $mock;

    public function setUp()
    {
        $this->creditPhotography = new CreditPhotography();
    }

    public function tearDown()
    {
        unset($this->creditPhotography);
    }

    /**
    * @Given: 存在一条新闻数据
    */
    protected function prepareData()
    {
        $data = $this->getCreditPhotographyDetailData(1);

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条新闻数据详情时
    */
    protected function fetchCreditPhotography($id)
    {

        $adapter = new CreditPhotographyRestfulAdapter();

        $this->creditPhotography = $adapter->fetchOne($id);
      
        return $this->creditPhotography;
    }

    /**
     * @Then  我可以看见该条新闻数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchCreditPhotography($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'creditPhotography/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('人无信不立,业无信不兴,国无信不宁', $this->creditPhotography->getDescription());
        $this->assertEquals(
            [
                [
                    "name"=> "name",
                    "identify"=> "identify.jpg"
                ],
                [
                    "name"=> "name",
                    "identify"=> "identify.jpg"
                ],
                [
                    "name"=> "name",
                    "identify"=> "identify.jpg"
                ]
            ],
            $this->creditPhotography->getAttachments()
        );
        $this->assertEquals(2, $this->creditPhotography->getApplyStatus());
        $this->assertEquals(0, $this->creditPhotography->getStatus());
    
        $this->assertEquals(
            1622286017,
            $this->creditPhotography->getStatusTime()
        );
        $this->assertEquals(
            1622285937,
            $this->creditPhotography->getCreateTime()
        );
        $this->assertEquals(
            1622286017,
            $this->creditPhotography->getUpdateTime()
        );
    }
}
