<?php
namespace Base\Sdk\CreditPhotography\Browse;

use Base\Sdk\CreditPhotography\Adapter\CreditPhotography\CreditPhotographyRestfulAdapter;

trait SearchDataTrait
{
    protected function getCreditPhotographyList(array $filter = [])
    {
        $adapter = new CreditPhotographyRestfulAdapter();

        list($count, $creditPhotographyList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
        
        return $creditPhotographyList;
    }
}
