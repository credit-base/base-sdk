<?php
namespace Base\Sdk\CreditPhotography\Add;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;

use Base\Sdk\Member\Model\Member;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\CreditPhotography\ArrayDataTrait;

use Base\Sdk\Common\Model\IApplyAble;

/**
 * @Feature: 我是前台用户,当我需要新增一个信用随手拍时,在门户信用随手拍栏目中,新增对应的信用随手拍数据
 *           通过新增信用随手拍界面，并根据我所采集的信用随手拍数据进行新增,以便于我维护信用随手拍列表
 * @Scenario: 正常新增数据数据
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $creditPhotography;

    private $mock;

    public function setUp()
    {
        $this->creditPhotography = new CreditPhotography();
    }

    public function tearDown()
    {
        unset($this->creditPhotography);
    }

    /**
    * @Given: 我并未新增过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getCreditPhotographyDetailData(1, IApplyAble::APPLY_STATUS['PENDING']);
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
       
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我调用添加函数,期待返回true
    */
    protected function add()
    {
        $this->creditPhotography = new CreditPhotography();
        $this->creditPhotography->setDescription('人无信不立,业无信不兴,国无信不宁');
     
        $member = new Member(2);
        $this->creditPhotography->setMember($member);
        $this->creditPhotography->setAttachments(
            [
                [
                    "name"=> "name",
                    "identify"=> "identify.jpg"
                ],
                [
                    "name"=> "name",
                    "identify"=> "identify.jpg"
                ],
                [
                    "name"=> "name",
                    "identify"=> "identify.jpg"
                ]
            ]
        );
      
        return $this->creditPhotography->add();
    }
    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        
        $this->add();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/creditPhotography';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getCreditPhotographyAddRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('人无信不立,业无信不兴,国无信不宁', $this->creditPhotography->getDescription());
        $this->assertEquals(
            [
                [
                    "name"=> "name",
                    "identify"=> "identify.jpg"
                ],
                [
                    "name"=> "name",
                    "identify"=> "identify.jpg"
                ],
                [
                    "name"=> "name",
                    "identify"=> "identify.jpg"
                ]
            ],
            $this->creditPhotography->getAttachments()
        );
        $this->assertEquals(0, $this->creditPhotography->getApplyStatus());
        $this->assertEquals(0, $this->creditPhotography->getStatus());
    
        $this->assertEquals(
            0,
            $this->creditPhotography->getStatusTime()
        );
        $this->assertEquals(
            1622285937,
            $this->creditPhotography->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->creditPhotography->getUpdateTime()
        );
    }
}
