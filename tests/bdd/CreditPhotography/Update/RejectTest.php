<?php
namespace Base\Sdk\CreditPhotography\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Adapter\CreditPhotography\CreditPhotographyRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\CreditPhotography\ArrayDataTrait;

/**
 * @Feature: 我是拥有信用随手拍审核权限的平台管理员/委办局管理员/操作用户,当我需要审核一个信用随手拍时,在审核表中,审核待审核的信用随手拍数据
 *           通过信用随手拍详情页面的审核通过与审核驳回操作,以便于我维护信用随手拍列表
 * @Scenario: 审核驳回
 */

class RejectTest extends TestCase
{
    use ArrayDataTrait;

    private $creditPhotography;

    private $mock;

    public function setUp()
    {
        $this->creditPhotography = new CreditPhotography();
    }

    public function tearDown()
    {
        unset($this->creditPhotography);
    }

    /**
    * @Given: 存在需要审核的新闻
    */
    protected function prepareData()
    {
        $data = $this->getCreditPhotographyDetailData(1, IApplyAble::APPLY_STATUS['PENDING']);
        $jsonData = json_encode($data);
        
        $rejectData = $this->getCreditPhotographyDetailData(
            1,
            IApplyAble::APPLY_STATUS['REJECT']
        );

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($rejectData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要审核驳回的新闻
    */
    protected function fetchCreditPhotography($id)
    {
        $adapter = new CreditPhotographyRestfulAdapter();
        $this->creditPhotography = $adapter->fetchOne($id);
        
        return $this->creditPhotography;
    }

    /**
    * @And:当我调用审核驳回函数,期待审核驳回成功
    */
    protected function reject()
    {
       
        $crew = new Crew(1);
        $this->creditPhotography->setApplyCrew($crew);
        $this->creditPhotography->setRejectReason('驳回原因');
     
        return $this->creditPhotography->reject();
    }
    /**
     * @Then  我可以查到该条新闻状态已被审核驳回，并能看到驳回原因
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchCreditPhotography($id);
        $this->reject();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/creditPhotography/1/reject';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $rejectRequestData = $this->getCreditPhotographyStatusRequest();

        $rejectRequestData['data']['attributes']['rejectReason'] = '驳回原因';

        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($rejectRequestData), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            IApplyAble::APPLY_STATUS['REJECT'],
            $this->creditPhotography->getApplyStatus()
        );
        $this->assertEquals(
            1,
            $this->creditPhotography->getApplyCrew()->getId()
        );

        $this->assertEquals(
            1622286017,
            $this->creditPhotography->getStatusTime()
        );
        $this->assertEquals(
            1622286017,
            $this->creditPhotography->getUpdateTime()
        );
    }
}
