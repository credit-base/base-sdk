<?php
namespace Base\Sdk\CreditPhotography\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Adapter\CreditPhotography\CreditPhotographyRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\CreditPhotography\ArrayDataTrait;

use Base\Sdk\Common\Model\IApplyAble;

/**
 * @Feature: 我是前台用户,当我需要删除信用随手拍数据时,在个人中心信用随手拍类别,删除对应的信用随手拍数据
 *           根据我核实需要删除的数据删除,以便于我可以更好的管理数据
 * @Scenario: 删除信用随手拍数据
 */

class DeleteTest extends TestCase
{
    use ArrayDataTrait;

    private $creditPhotography;

    private $mock;

    public function setUp()
    {
        $this->creditPhotography = new CreditPhotography();
    }

    public function tearDown()
    {
        unset($this->creditPhotography);
    }

    /**
    * @Given: 存在需要删除的信用随手拍数据
    */
    protected function prepareData()
    {
        $data = $this->getCreditPhotographyDetailData(
            1,
            IApplyAble::APPLY_STATUS['APPROVE'],
            CreditPhotography::STATUS['NOMAL']
        );
        $jsonData = json_encode($data);
        
        $deleteData = $this->getCreditPhotographyDetailData(
            1,
            IApplyAble::APPLY_STATUS['APPROVE'],
            CreditPhotography::STATUS['DELETE']
        );
        
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($deleteData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要删除的信用随手拍数据
    */
    protected function fetchCreditPhotography($id)
    {
        $adapter = new CreditPhotographyRestfulAdapter();
        $this->creditPhotography = $adapter->fetchOne($id);
        
        return $this->creditPhotography;
    }

    /**
    * @And:当我调用删除函数,期待删除成功
    */
    protected function delete()
    {
        return $this->creditPhotography->delete();
    }
    /**
     * @Then  数据已经被删除
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchCreditPhotography($id);
        $this->delete();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/creditPhotography/1/delete';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('DELETE', $method);
        $this->assertEquals('[]', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(CreditPhotography::STATUS['DELETE'], $this->creditPhotography->getStatus());

        $this->assertEquals(
            1622286017,
            $this->creditPhotography->getStatusTime()
        );
        $this->assertEquals(
            1622286017,
            $this->creditPhotography->getUpdateTime()
        );
    }
}
