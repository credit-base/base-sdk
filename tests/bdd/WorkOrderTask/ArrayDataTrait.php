<?php
namespace Base\Sdk\WorkOrderTask;

trait ArrayDataTrait
{
    public function getWorkOrderTasksDetailData($id = 1, $status = 0, $reason = ''):array
    {
        return [
            "meta"=> [],
            "data"=> [
                "type"=> "workOrderTasks",
                "id"=> $id,
                "attributes"=> [
                    "reason"=> $reason,
                    "feedbackRecords"=> $this->getCommonFeedbackRecords(),
                    "status"=> $status,
                    "createTime"=> 1631685765,
                    "updateTime"=> 1631686932,
                    "statusTime"=> 1531897790
                ],
                "relationships"=> [
                    "parentTask"=> [
                        "data"=> [
                            "type"=> "parentTasks",
                            "id"=> "2"
                        ]
                    ],
                    "assignObject"=> [
                        "data"=> [
                            "type"=> "userGroups",
                            "id"=> "3"
                        ]
                    ],
                    "template"=> [
                        "data"=> [
                            "type"=> "gbTemplates",
                            "id"=> "2"
                        ]
                    ]
                ],
                "links"=> [
                    "self"=> "127.0.0.1:8080/workOrderTasks/3"
                ]
            ],
            "included"=> [
                [
                    "type"=> "parentTasks",
                    "id"=> "2",
                    "attributes"=> [
                        "templateType"=> 1,
                        "title"=> "归集地方性黑名单信息",
                        "description"=> "依据国家要求，现需要按要求归集地方性黑名单信息",
                        "endTime"=> "2020-01-01",
                        "attachment"=> [
                            "name"=> "国203号文",
                            "identify"=> "b900638af896a26de13bc89ce4f64124.pdf"
                        ],
                        "ratio"=> 50,
                        "reason"=> "",
                        "status"=> 0,
                        "createTime"=> 1531897790,
                        "updateTime"=> 1531897790,
                        "statusTime"=> 1531897790
                    ]
                ],
                $this->getUserGroup(3),
                $this->getTemplates(2)
            ]
        ];
    }

    /**
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    protected function getWorkOrderTaskListData():array
    {
        return [
            "meta"=> [
                "count"=> 3,
                "links"=> [
                    "first"=> null,
                    "last"=> null,
                    "prev"=> null,
                    "next"=> null
                ]
            ],
            "links"=> [
                "first"=> null,
                "last"=> null,
                "prev"=> null,
                "next"=> null
            ],
            "data"=> [
                [
                    "type"=> "workOrderTasks",
                    "id"=> "1",
                    "attributes"=> [
                        "reason"=> "撤销原因",
                        "feedbackRecords"=> [],
                        "status"=> 1,
                        "createTime"=> 1531897790,
                        "updateTime"=> 1531897790,
                        "statusTime"=> 0
                    ],
                    "relationships"=> [
                        "parentTask"=> [
                            "data"=> [
                                "type"=> "parentTasks",
                                "id"=> "1"
                            ]
                        ],
                        "assignObject"=> [
                            "data"=> [
                                "type"=> "userGroups",
                                "id"=> "2"
                            ]
                        ],
                        "template"=> [
                            "data"=> [
                                "type"=> "gbTemplates",
                                "id"=> "1"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8080/workOrderTasks/1"
                    ]
                ],
                [
                    "type"=> "workOrderTasks",
                    "id"=> "2",
                    "attributes"=> [
                        "reason"=> "终结原因",
                        "feedbackRecords"=> [],
                        "status"=> 4,
                        "createTime"=> 1531897790,
                        "updateTime"=> 1531897790,
                        "statusTime"=> 0
                    ],
                    "relationships"=> [
                        "parentTask"=> [
                            "data"=> [
                                "type"=> "parentTasks",
                                "id"=> "2"
                            ]
                        ],
                        "assignObject"=> [
                            "data"=> [
                                "type"=> "userGroups",
                                "id"=> "2"
                            ]
                        ],
                        "template"=> [
                            "data"=> [
                                "type"=> "bjTemplates",
                                "id"=> "2"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8080/workOrderTasks/2"
                    ]
                ],
                [
                    "type"=> "workOrderTasks",
                    "id"=> "3",
                    "attributes"=> [
                        "reason"=> "",
                        "feedbackRecords"=> $this->getCommonFeedbackRecords(),
                        "status"=> 0,
                        "createTime"=> 1531897790,
                        "updateTime"=> 1531897790,
                        "statusTime"=> 0
                    ],
                    "relationships"=> [
                        "parentTask"=> [
                            "data"=> [
                                "type"=> "parentTasks",
                                "id"=> "2"
                            ]
                        ],
                        "assignObject"=> [
                            "data"=> [
                                "type"=> "userGroups",
                                "id"=> "3"
                            ]
                        ],
                        "template"=> [
                            "data"=> [
                                "type"=> "bjTemplates",
                                "id"=> "2"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8080/workOrderTasks/2"
                    ]
                ]
            ],
            "included"=> [
                [
                    "type"=> "gbTemplates",
                    "id"=> "1",
                    "attributes"=> [
                        "name"=> "地方性红名单"
                    ]
                ],
                [
                    "type"=> "bjTemplates",
                    "id"=> "2",
                    "attributes"=> [
                        "name"=> "地方性黑名单"
                    ]
                ],
                [
                    "type"=> "userGroups",
                    "id"=> "2",
                    "attributes"=> [
                        "name"=> "市场监督管理局"
                    ]
                ],
                [
                    "type"=> "userGroups",
                    "id"=> "3",
                    "attributes"=> [
                        "name"=> "人力资源和社会保障局"
                    ]
                ],
                [
                    "type"=> "parentTasks",
                    "id"=> "1",
                    "attributes"=> [
                        "templateType"=> 1,
                        "title"=> "归集地方性红名单信息",
                        "endTime"=> "2020-01-01"
                    ]
                ],
                [
                    "type"=> "parentTasks",
                    "id"=> "2",
                    "attributes"=> [
                        "templateType"=> 2,
                        "title"=> "归集地方性黑名单信息",
                        "endTime"=> "2020-01-01"
                    ]
                ]
            ]
        ];
    }

    /**
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    public function getCommonFeedbackRecords():array
    {
        return [
            [
                "crew"=> 2,
                "crewName"=> "人社局管理员",
                "userGroup"=> 3,
                "userGroupName"=> "人力和社会资源保障局",
                "isExistedTemplate"=> 1,
                "templateId"=> 1,
                "items"=> $this->getCommonItemsData(),
                "reason"=> "人力和社会资源保障局的反馈原因",
                "feedbackTime"=> 1614422868
            ],
            [
                "crew"=> 1,
                "crewName"=> "发改委管理员",
                "userGroup"=> 1,
                "userGroupName"=> "发展和改革委员会",
                "isExistedTemplate"=> 1,
                "templateId"=> 1,
                "items"=> $this->getCommonItemsData(),
                "reason"=> "发展和改革委员会的反馈原因",
                "feedbackTime"=> 1614509268
            ]
        ];
    }

    protected function getCommonItemsData():array
    {
        return [
            [
                "name"=> "主体名称",
                "identify"=> "ZTMC",
                "type"=> 1,
                "length"=> 200,
                "options"=> [],
                "dimension"=> 1,
                "isNecessary"=> 1,
                "isMasked"=> 0,
                "maskRule"=> [],
                "remarks"=> "信用主体名称"
            ],
            [
                "name"=> "统一社会信用代码",
                "identify"=> "TYSHXYDM",
                "type"=> 1,
                "length"=> 50,
                "options"=> [],
                "dimension"=> 1,
                "isNecessary"=> 1,
                "isMasked"=> 1,
                "maskRule"=> [
                    3,
                    4
                ],
                "remarks"=> "信用主体代码"
            ],
            [
                "name"=> "信息类别",
                "identify"=> "XXLB",
                "type"=> 5,
                "length"=> 50,
                "options"=> [
                    "基础信息",
                    "守信信息",
                    "失信信息",
                    "其他信息"
                ],
                "dimension"=> 2,
                "isNecessary"=> 1,
                "isMasked"=> 1,
                "maskRule"=> [
                    1,
                    2
                ],
                "remarks"=> "信息性质类型，支持单选"
            ]
        ];
    }

    protected function getParentTaskAddData():array
    {
        return [
            "data"=> [
                "type"=> "parentTasks",
                "attributes"=> [
                    "title"=> "归集地方性红名单信息",
                    "description"=> "依据国家要求，现需要按要求归集地方性红名单信息",
                    "attachment"=> [
                        "name"=> "国203号文",
                        "identify"=> "b900638af896a26de13bc89ce4f64124.pdf"
                    ],
                    "templateType"=> 1,
                    "endTime"=> "2020-01-01",
                ],
                "relationships"=> [
                    "template"=> [
                        "data"=> [
                            [
                                "type"=> "gbTemplates",
                                "id"=> 1
                            ]
                        ]
                    ],
                    "assignObjects"=> [
                        "data"=> [
                            [
                                "type"=> "userGroups",
                                "id"=> 1
                            ],
                            [
                                "type"=> "userGroups",
                                "id"=> 2
                            ],
                        ]
                    ],
                ]
            ]
        ];
    }

    protected function getParentTaskDetail(
        $id = 1,
        $status = 0,
        $reason = ''
    ):array {
        return [
            "meta"=> [],
            "data"=> [
                "type"=> "parentTasks",
                "id"=> $id,
                "attributes"=> [
                    "templateType"=> 1,
                    "title"=> "归集地方性红名单信息",
                    "description"=> "依据国家要求，现需要按要求归集地方性红名单信息",
                    "endTime"=> "2020-01-01",
                    "attachment"=> [
                        "name"=> "国203号文",
                        "identify"=> "b900638af896a26de13bc89ce4f64124.pdf"
                    ],
                    "ratio"=> 100,
                    "reason"=> $reason,
                    "status"=> $status,
                    "createTime"=> 1531897790,
                    "updateTime"=> 1531897790,
                    "statusTime"=> 1531897790
                ],
                "relationships"=> [
                    "assignObject"=> [
                        "data"=> [
                            [
                                "type"=> "userGroups",
                                "id"=> "1"
                            ],
                            [
                                "type"=> "userGroups",
                                "id"=> "2"
                            ],
                        ]
                    ],
                    "template"=> [
                        "data"=> [
                            "type"=> "gbTemplates",
                            "id"=> "1"
                        ]
                    ]
                ],
                "links"=> [
                    "self"=> "127.0.0.1:8080/workOrderTasks/3"
                ]
            ],
            "included"=> [
                $this->getTemplates(1),
                $this->getUserGroup(1),
                $this->getUserGroup(2)
            ]
        ];
    }

    protected function getUserGroup($id = 1):array
    {
        return [
            "type"=> "userGroups",
            "id"=> $id,
            "attributes"=> [
                "name"=> "人力资源和社会保障局",
                "shortName"=> "人社局",
                "purview"=> [],
                "status"=> 0,
                "createTime"=> 1531897790,
                "updateTime"=> 1531897790,
                "statusTime"=> 0
            ]
        ];
    }

    protected function getTemplates($id = 1):array
    {
        return [
            "type"=> "gbTemplates",
            "id"=> $id,
            "attributes"=> [
                "name"=> "地方性黑名单",
                "identify"=> "DFXHMD",
                "subjectCategory"=> [
                    1,
                    3
                ],
                "dimension"=> 1,
                "exchangeFrequency"=> 1,
                "infoClassify"=> 1,
                "infoCategory"=> 1,
                "description"=> "目录描述信息",
                "items"=> $this->getCommonItemsData(),
                "status"=> 0,
                "createTime"=> 1531897790,
                "updateTime"=> 1531897790,
                "statusTime"=> 0
            ]
        ];
    }

    protected function getStatusRequest($type = 'parentTasks', $reason = '撤销原因'):array
    {
        return [
            "data"=> [
                "type"=> $type,
                "attributes"=> [
                    "reason"=> $reason,
                ]
            ]
        ];
    }

    /**
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    protected function getParentTaskListData():array
    {
        return [
            "meta"=> [
                "count"=> 2,
                "links"=> [
                    "first"=> null,
                    "last"=> null,
                    "prev"=> null,
                    "next"=> null
                ]
            ],
            "links"=> [
                "first"=> null,
                "last"=> null,
                "prev"=> null,
                "next"=> null
            ],
            "data"=> [
                [
                    "type"=> "parentTasks",
                    "id"=> "1",
                    "attributes"=> [
                        "templateType"=> 1,
                        "title"=> "红名单信息",
                        "description"=> "依据国家要求，现需要归集地方性红名单信息",
                        "endTime"=> "2020-01-10",
                        "attachment"=> [
                            "name"=> "国209号文",
                            "identify"=> "b900638af896a26de45bc89ce4f64124.pdf"
                        ],
                        "ratio"=> 100,
                        "reason"=> "撤销原因",
                        "status"=> 0,
                        "createTime"=> 1531897850,
                        "updateTime"=> 1531897943,
                        "statusTime"=> 1531897943
                    ],
                    "relationships"=> [
                        "template"=> [
                            "data"=> [
                                "type"=> "gbTemplates",
                                "id"=> "1"
                            ]
                        ],
                        "assignObjects"=> [
                            "data"=> [
                                [
                                    "type"=> "userGroups",
                                    "id"=> "1"
                                ],
                                [
                                    "type"=> "userGroups",
                                    "id"=> "2"
                                ],
                                [
                                    "type"=> "userGroups",
                                    "id"=> "3"
                                ]
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8080/parentTasks/1"
                    ]
                ],
                [
                    "type"=> "parentTasks",
                    "id"=> "2",
                    "attributes"=> [
                        "templateType"=> 2,
                        "title"=> "归集地方性黑名单信息",
                        "description"=> "依据省平台要求，现需要按要求归集地方性黑名单信息",
                        "endTime"=> "2020-01-01",
                        "attachment"=> [
                            "name"=> "省203号文",
                            "identify"=> "b900638af896a26de13bc89ce4f64127.pdf"
                        ],
                        "ratio"=> 50,
                        "reason"=> "",
                        "status"=> 0,
                        "createTime"=> 1531896309,
                        "updateTime"=> 1531898755,
                        "statusTime"=> 1531898755
                    ],
                    "relationships"=> [
                        "template"=> [
                            "data"=> [
                                "type"=> "gbTemplates",
                                "id"=> "2"
                            ]
                        ],
                        "assignObjects"=> [
                            "data"=> [
                                [
                                    "type"=> "userGroups",
                                    "id"=> "3"
                                ],
                                [
                                    "type"=> "userGroups",
                                    "id"=> "4"
                                ],
                                [
                                    "type"=> "userGroups",
                                    "id"=> "5"
                                ]
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8080/parentTasks/2"
                    ]
                ]
            ],
            "included"=> [
                $this->getTemplates(1),
                $this->getTemplates(2),
                $this->getUserGroup(1),
                $this->getUserGroup(2),
                $this->getUserGroup(3),
                $this->getUserGroup(4),
                $this->getUserGroup(5),
            ]
        ];
    }
}
