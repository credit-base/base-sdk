<?php
namespace Base\Sdk\WorkOrderTask\WorkOrderTask\Browse;

use Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskRestfulAdapter;

trait SearchDataTrait
{
    protected function getWorkOrderTaskList(array $filter = [])
    {
        $adapter = new WorkOrderTaskRestfulAdapter();

        list($count, $workOrderTaskList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
        
        return $workOrderTaskList;
    }
}
