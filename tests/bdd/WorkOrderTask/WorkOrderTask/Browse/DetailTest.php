<?php
namespace Base\Sdk\WorkOrderTask\WorkOrderTask\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;
use Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\WorkOrderTask\ArrayDataTrait;

/**
 * @Feature: 我是委办局领导,我登录的账号可以管理指派给我的任务,
 *           在政务网OA中,我可以管理给我指派的工单任务,
 *           通过列表形式呈现,以便于我可以把我的数据通过发改委指派的资源目录上报上去
 * @Scenario: 查看工单任务数据详情
 */

class DetailTest extends TestCase
{
    use ArrayDataTrait;

    private $workOrderTask;

    private $mock;

    public function setUp()
    {
        $this->workOrderTask = new WorkOrderTask();
    }

    public function tearDown()
    {
        unset($this->workOrderTask);
    }

    /**
    * @Given: 存在一条工单任务数据
    */
    protected function prepareData()
    {
        $data = $this->getWorkOrderTasksDetailData(1);

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条工单任务数据详情时
    */
    protected function fetchWorkOrderTask($id)
    {

        $adapter = new WorkOrderTaskRestfulAdapter();

        $this->workOrderTask = $adapter->fetchOne($id);
      
        return $this->workOrderTask;
    }

    /**
     * @Then  我可以看见该条工单任务数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchWorkOrderTask($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'workOrderTasks/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            $this->getCommonFeedbackRecords(),
            $this->workOrderTask->getFeedbackRecords()
        );
   
        $this->assertEquals(WorkOrderTask::STATUS['DQR'], $this->workOrderTask->getStatus());

        $this->assertEquals(
            1631685765,
            $this->workOrderTask->getCreateTime()
        );
        $this->assertEquals(
            1631686932,
            $this->workOrderTask->getUpdateTime()
        );
    }
}
