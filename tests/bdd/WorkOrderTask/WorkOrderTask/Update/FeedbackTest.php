<?php
namespace Base\Sdk\WorkOrderTask\WorkOrderTask\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;
use Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\WorkOrderTask\ArrayDataTrait;

/**
 * @Feature: 我是委办局领导,当我们有数据但没有对应的资源目录可以上报时,在政务网OA中,
 *           可以通过接受发改委领导指派的工单任务生成对应的资源目录
 *           当任务我确认无误时可以同意使用任务中的资源目录上报数据，
 *           当任务有问题时我可以进行反馈,以便于我可以把我的数据通过发改委指派的资源目录上报上去
 * @Scenario: 反馈工单任务
 */

class FeedbackTest extends TestCase
{
    use ArrayDataTrait;

    private $workOrderTask;

    private $mock;

    public function setUp()
    {
        $this->workOrderTask = new WorkOrderTask();
    }

    public function tearDown()
    {
        unset($this->workOrderTask);
    }

    /**
    * @Given: 存在需要反馈的工单任务
    */
    protected function prepareData()
    {
        $data = $this->getWorkOrderTasksDetailData(1, 4);
        $jsonData = json_encode($data);
        
        $revokeData = $this->getWorkOrderTasksDetailData(1, 4);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($revokeData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要反馈的工单任务
    */
    protected function fetchWorkOrderTask($id)
    {
        $adapter = new WorkOrderTaskRestfulAdapter();
        $this->workOrderTask = $adapter->fetchOne($id);
        
        return $this->workOrderTask;
    }

    /**
    * @And:当我调用反馈函数,期待反馈成功
    */
    protected function feedback()
    {
        $this->workOrderTask->setFeedbackRecords($this->getCommonFeedbackRecords());
        return $this->workOrderTask->feedback();
    }
    /**
     * @Then  数据已经被反馈
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchWorkOrderTask($id);
        $this->feedback();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/workOrderTasks/1/feedback';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();

        $requestContents = $this->getStatusRequest('workOrderTasks');
        unset($requestContents['data']['attributes']['reason']);
        $requestContents['data']['attributes']['feedbackRecords'] = $this->getCommonFeedbackRecords();

        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($requestContents), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals($this->getCommonFeedbackRecords(), $this->workOrderTask->getFeedbackRecords());

        $this->assertEquals(
            1631686932,
            $this->workOrderTask->getUpdateTime()
        );
    }
}
