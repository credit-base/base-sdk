<?php
namespace Base\Sdk\WorkOrderTask\WorkOrderTask\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\WorkOrderTask\Model\WorkOrderTask;
use Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\WorkOrderTask\ArrayDataTrait;

/**
 * @Feature: 我是委办局领导,当我们有数据但没有对应的资源目录可以上报时,在政务网OA中,
 *           可以通过接受发改委领导指派的工单任务生成对应的资源目录
 *           当任务我确认无误时可以同意使用任务中的资源目录上报数据，当任务有问题时我可以进行反馈,
 *           以便于我可以把我的数据通过发改委指派的资源目录上报上去
 * @Scenario: 确认工单任务
 */

class ConfirmTest extends TestCase
{
    use ArrayDataTrait;

    private $workOrderTask;

    private $mock;

    public function setUp()
    {
        $this->workOrderTask = new WorkOrderTask();
    }

    public function tearDown()
    {
        unset($this->workOrderTask);
    }

    /**
    * @Given: 存在需要确认的工单任务
    */
    protected function prepareData()
    {
        $data = $this->getWorkOrderTasksDetailData(1);
        $jsonData = json_encode($data);
        
        $revokeData = $this->getWorkOrderTasksDetailData(1, 2);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($revokeData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要确认的工单任务
    */
    protected function fetchWorkOrderTask($id)
    {
        $adapter = new WorkOrderTaskRestfulAdapter();
        $this->workOrderTask = $adapter->fetchOne($id);
        
        return $this->workOrderTask;
    }

    /**
    * @And:当我调用确认函数,期待确认成功
    */
    protected function confirm()
    {
        return $this->workOrderTask->confirm();
    }
    /**
     * @Then  数据已经被确认
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchWorkOrderTask($id);
        $this->confirm();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/workOrderTasks/1/confirm';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();

        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals('[]', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(2, $this->workOrderTask->getStatus());

        $this->assertEquals(
            1631686932,
            $this->workOrderTask->getUpdateTime()
        );
    }
}
