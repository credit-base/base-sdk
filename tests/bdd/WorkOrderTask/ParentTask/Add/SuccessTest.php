<?php
namespace Base\Sdk\WorkOrderTask\ParentTask\Add;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\WorkOrderTask\Model\ParentTask;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\UserGroup\Model\UserGroup;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\WorkOrderTask\ArrayDataTrait;

/**
 * @Feature: 我是发改委领导,当我需要我指定的委办局根据我的资源目录模板上传数据时,在政务网OA中,
 *           可以给我指定的委办局指派我指定的资源目录模板,
 *           资源目录模板以任务的形式指派委办局确认无误后会生成我指派的模板,
 *           以便于委办局可以按照我的模板来填报数据
 * @Scenario: 新增父任务
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $parentTask;

    private $mock;

    public function setUp()
    {
        $this->parentTask = new ParentTask();
    }

    public function tearDown()
    {
        unset($this->parentTask);
    }

    /**
    * @Given: 我并未新增过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getParentTaskDetail(1);

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
       
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我调用添加函数,期待返回true
    */
    protected function assign()
    {
        $this->parentTask = new ParentTask();
        $this->parentTask->setTitle('归集地方性红名单信息');
     
        $gbTemplates = new GbTemplate(1);
        $this->parentTask->setTemplate($gbTemplates);
        $this->parentTask->setDescription("依据国家要求，现需要按要求归集地方性红名单信息");
        $this->parentTask->setTemplateType(1);
        $this->parentTask->setAttachment(
            [
                "name"=> "国203号文",
                "identify"=> "b900638af896a26de13bc89ce4f64124.pdf"
            ]
        );
        $this->parentTask->setEndTime("2020-01-01");

        $assignObjects = [2,[new UserGroup(1),new UserGroup(2)]];
        $this->parentTask->setAssignObjects($assignObjects);
      
        return  $this->parentTask->assign();
    }
    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        
        $this->assign();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/parentTasks';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getParentTaskAddData()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('归集地方性红名单信息', $this->parentTask->getTitle());
        $this->assertEquals(1, $this->parentTask->getTemplateType());
        $this->assertEquals('依据国家要求，现需要按要求归集地方性红名单信息', $this->parentTask->getDescription());
   
        $this->assertEquals(
            [
                "name"=> "国203号文",
                "identify"=> "b900638af896a26de13bc89ce4f64124.pdf"
            ],
            $this->parentTask->getAttachment()
        );
        $this->assertEquals(
            "2020-01-01",
            $this->parentTask->getEndTime()
        );

        $this->assertEquals(
            1531897790,
            $this->parentTask->getUpdateTime()
        );
    }
}
