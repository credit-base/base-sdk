<?php
namespace Base\Sdk\WorkOrderTask\ParentTask\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\WorkOrderTask\Adapter\ParentTask\ParentTaskRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\WorkOrderTask\ArrayDataTrait;

/**
 * @Feature: 我是发改委领导,当我有指派后的工单任务时,在政务网OA中,我可以管理我指派的工单任务
 *           可以对整个任务作出管理，也可以对单独的指派给委办局的任务作出管理,以便于我指派的任务可以有结果
 * @Scenario: 撤销父任务
 */

class RevokeTest extends TestCase
{
    use ArrayDataTrait;

    private $parentTask;

    private $mock;

    public function setUp()
    {
        $this->parentTask = new ParentTask();
    }

    public function tearDown()
    {
        unset($this->parentTask);
    }

    /**
    * @Given: 存在需要撤销的父任务
    */
    protected function prepareData()
    {
        $data = $this->getParentTaskDetail(1);
        $jsonData = json_encode($data);
        
        $revokeData = $this->getParentTaskDetail(1, 1, '撤销原因');
        
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($revokeData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要撤销的父任务
    */
    protected function fetchParentTask($id)
    {
        $adapter = new ParentTaskRestfulAdapter();
        $this->parentTask = $adapter->fetchOne($id);
        
        return $this->parentTask;
    }

    /**
    * @And:当我调用撤销函数,期待撤销成功
    */
    protected function revoke()
    {
        $this->parentTask->setReason('撤销原因');
        return $this->parentTask->revoke();
    }
    /**
     * @Then  数据已经被撤销
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchParentTask($id);
        $this->revoke();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/parentTasks/1/revoke';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();

        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getStatusRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(1, $this->parentTask->getStatus());

        $this->assertEquals(
            1531897790,
            $this->parentTask->getUpdateTime()
        );
    }
}
