<?php
namespace Base\Sdk\WorkOrderTask\ParentTask\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\WorkOrderTask\Model\ParentTask;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\WorkOrderTask\ArrayDataTrait;

/**
 * @Feature:  我是发改委领导,当我有指派后的工单任务时,在政务网OA中,我可以管理我指派的工单任务,
 *           可以对整个任务作出管理，也可以对单独的指派给委办局的任务作出管理,以便于我指派的任务可以有结果
 * @Scenario: 查看工单任务列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ArrayDataTrait;

    private $parentTask;

    private $mock;

    public function setUp()
    {
        $this->parentTask = new ParentTask();
    }

    public function tearDown()
    {
        unset($this->parentTask);
    }

    /**
    * @Given: 存在工单任务数据
    */
    protected function prepareData()
    {
        $data = $this->getParentTaskListData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看工单任务列表时
     */
    public function fetchParentTaskList()
    {
        $filter = [];
        $list =$this->getParentTaskList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见工单任务数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchParentTaskList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'parentTasks';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $parentTasksList = $this->fetchParentTaskList();

        $parentTasksArray = $this->getParentTaskListData()['data'];
        
        foreach ($parentTasksList as $object) {
            foreach ($parentTasksArray as $item) {
                if ($object->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['title'], $object->getTitle());
                    $this->assertEquals($item['attributes']['description'], $object->getDescription());

                    $this->assertEquals($item['attributes']['endTime'], $object->getEndTime());
                    $this->assertEquals(
                        $item['attributes']['templateType'],
                        $object->getTemplateType()
                    );
            
                    $this->assertEquals(
                        $item['attributes']['attachment'],
                        $object->getAttachment()
                    );
                    $this->assertEquals(
                        $item['attributes']['updateTime'],
                        $object->getUpdateTime()
                    );
                }
            }
        }
    }
}
