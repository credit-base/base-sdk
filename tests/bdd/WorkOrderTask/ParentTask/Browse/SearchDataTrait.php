<?php
namespace Base\Sdk\WorkOrderTask\ParentTask\Browse;

use Base\Sdk\WorkOrderTask\Adapter\ParentTask\ParentTaskRestfulAdapter;

trait SearchDataTrait
{
    protected function getParentTaskList(array $filter = [])
    {
        $adapter = new ParentTaskRestfulAdapter();

        list($count, $parentTaskList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
        
        return $parentTaskList;
    }
}
