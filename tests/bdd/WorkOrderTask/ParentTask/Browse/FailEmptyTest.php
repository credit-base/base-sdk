<?php
namespace Base\Sdk\WorkOrderTask\ParentTask\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Base\Sdk\WorkOrderTask\Adapter\ParentTask\ParentTaskRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是发改委领导,当我有指派后的工单任务时,在政务网OA中,我可以管理我指派的工单任务,
 *           可以对整个任务作出管理，也可以对单独的指派给委办局的任务作出管理,以便于我指派的任务可以有结果
 * @Scenario: 查看工单任务数据列表-异常不存在数据
 */

class FailEmptyTest extends TestCase
{
    private $parentTask;

    private $mock;

    public function setUp()
    {
        $this->parentTask = new ParentTask();
    }

    public function tearDown()
    {
        unset($this->parentTask);
    }

    /**
    * @Given: 不存在工单任务数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看工单任务列表时
     */
    public function fetchParentTaskList()
    {
        $adapter = new ParentTaskRestfulAdapter();

        list($count, $this->parentTask) = $adapter
        ->search();
        unset($count);

        return $this->parentTask;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchParentTaskList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'parentTasks';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->parentTask);
    }
}
