<?php
namespace Base\Sdk\Template\WbjTemplate\Edit;

use Base\Sdk\Template\AddDataTrait;
use Base\Sdk\Template\EditDataTrait;
use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\Adapter\WbjTemplate\WbjTemplateRestfulAdapter;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是拥有信用刊物管理权限的平台管理员/委办局管理员/操作用户,当我需要编辑信用刊物信息时,在发布表中,编辑对应信用刊物数据
 *           通过编辑信用刊物界面，并根据我所采集的信用刊物数据进行编辑,以便于我可以更好的维护信用刊物管理列表
 * @Scenario: 正常编辑数据
 */

class SuccessTest extends TestCase
{
    use EditDataTrait;

    private $editWbjTemplateObject;

    private $mock;
    private $id;

    public function setUp()
    {
        $this->editWbjTemplateObject = new WbjTemplate();
        $this->id = 1;
    }

    public function tearDown()
    {
        unset($this->editWbjTemplateObject);
        unset($this->id);
    }

    /**
    * @Given: 我并未编辑过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getResponseData(1);
        $formatJsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(200, ['Content-Type' => 'application/vnd.api+json'], $formatJsonData),
               new Response(201, ['Content-Type' => 'application/vnd.api+json'], $formatJsonData)
            ]
        );
      
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要编辑的信用刊物数据
    */
    protected function fetchWbjTemplate()
    {
        $adapter = new WbjTemplateRestfulAdapter();
        $this->editWbjTemplateObject = $adapter->fetchOne($this->id);
        return $this->editWbjTemplateObject;
    }

    /**
    * @When:当我调用编辑函数,期待编辑成功
    */
    protected function edit()
    {
        $data = $this->getResponseData($this->id, 'templates');
        $this->editWbjTemplateObject->setName($data['data']['attributes']['name']);
        $this->editWbjTemplateObject->setIdentify($data['data']['attributes']['identify']);
        $this->editWbjTemplateObject->setDescription($data['data']['attributes']['description']);
        $this->editWbjTemplateObject->setSubjectCategory($data['data']['attributes']['subjectCategory']);
        $this->editWbjTemplateObject->setItems($data['data']['attributes']['items']);
        $this->editWbjTemplateObject->setDimension($data['data']['attributes']['dimension']);
        $this->editWbjTemplateObject->setExchangeFrequency($data['data']['attributes']['exchangeFrequency']);
        $this->editWbjTemplateObject->setInfoClassify($data['data']['attributes']['infoClassify']);
        $this->editWbjTemplateObject->setInfoCategory($data['data']['attributes']['infoCategory']);
        $this->editWbjTemplateObject->setSourceUnit($this->getUserGroup(1));

        return $this->editWbjTemplateObject->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchWbjTemplate();
        $this->edit();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/templates/'.$this->id;

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getWbjTemplateOperationRequest($this->id)), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            1615279726,
            $this->editWbjTemplateObject->getCreateTime()
        );
        $this->assertEquals(
            1615279726,
            $this->editWbjTemplateObject->getUpdateTime()
        );
    }
}
