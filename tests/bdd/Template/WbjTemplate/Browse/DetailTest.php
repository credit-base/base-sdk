<?php
namespace Base\Sdk\Template\WbjTemplate\Browse;

use Base\Sdk\Template\Adapter\WbjTemplate\WbjTemplateRestfulAdapter;
use Base\Sdk\Template\DetailDataTrait;
use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\Template\ArrayDataTrait;
use Base\Sdk\Template\TemplateFactory;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DetailTest extends TestCase
{
    use  DetailDataTrait;

    private $wbjTemplate;

    private $mock;

    public function setUp()
    {
        $this->wbjTemplate = new WbjTemplate();
    }

    public function tearDown()
    {
        unset($this->wbjTemplate);
    }

    /**
     * @Given: 存在一条数据
     */
    protected function prepareData()
    {
        $id = 1;
        $data = $this->getTemplateDetailData($id, 'wbjTemplates');

        $this->mock = new MockHandler(
            [
                new Response(
                    200,
                    ['Content-Type' => 'application/vnd.api+json'],
                    json_encode($data)
                ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看该条数据详情时
     */
    protected function fetchWbjTemplate($id)
    {

        $adapter = TemplateFactory::getAdapter('wbjTemplates');

        $this->wbjTemplate = $adapter->fetchOne($id);

        return $this->wbjTemplate;
    }

    /**
     * @Then 我可以看见详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchWbjTemplate($id);

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'templates/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(3, $this->wbjTemplate->getCategory());
        $this->assertEquals(1, $this->wbjTemplate->getId());
        $this->assertEquals(1626504022, $this->wbjTemplate->getCreateTime());
        $this->assertEquals(1631862848, $this->wbjTemplate->getUpdateTime());
    }
}
