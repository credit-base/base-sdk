<?php
namespace Base\Sdk\Template\WbjTemplate\Browse;

use Base\Sdk\Template\ListDataTrait;
use Base\Sdk\Template\Model\WbjTemplate;
use Marmot\Core;

use PHPUnit\Framework\TestCase;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是委办局工作人员,当我需要查看企业数据时,在信用信息查询应用系统下的企业管理中
 *           可以查看到所有的企业数据,通过列表和详情的形式查看我所有的企业, 以便于我可以精确的查看到不同的企业信息
 * @Scenario: 查看模板列表
 */

class ListTest extends TestCase
{
    use ListDataTrait;

    private $wbjTemplate;
    private $wbjTemplateType;

    private $mock;

    public function setUp()
    {
        $this->wbjTemplate = new WbjTemplate();
        $this->wbjTemplateType = 'wbjTemplates';
    }

    public function tearDown()
    {
        unset($this->wbjTemplate);
        unset($this->wbjTemplateType);
    }

    /**
    * @Given: 存在企业数据
    */
    protected function prepareData()
    {
        $data = $this->getTemplateListData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
               new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看企业列表时
     */
    public function fetchTemplateList()
    {
        $filter = [];
        return $this->getTemplateList($filter, $this->wbjTemplateType);
    }

    /**
     * @Then  我可以看见企业数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchTemplateList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'templates';

        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals($expectedPath, $request->getUri()->getPath());
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function response()
    {
        $memberObject = $this->fetchTemplateList();
        $memberArray = $this->getTemplateListData()['data'];
        
        foreach ($memberObject as $item) {
            foreach ($memberArray as $val) {
                if ($val['id'] == $item->getId()) {
                    $this->assertEquals($val['attributes']['dimension'], $item->getDimension());
                    $this->assertEquals($val['attributes']['name'], $item->getName());
                    $this->assertEquals($val['attributes']['identify'], $item->getIdentify());
                    $this->assertEquals($val['attributes']['subjectCategory'], $item->getSubjectCategory());
                }
            }
        }
    }
}
