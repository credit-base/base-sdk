<?php


namespace Base\Sdk\Template\WbjTemplate\Add;

use Base\Sdk\Template\AddDataTrait;
use Base\Sdk\Template\Model\WbjTemplate;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class SuccessTest extends TestCase
{
    use AddDataTrait;

    private $wbjTemplate;

    private $mock;

    public function setUp()
    {
        $this->wbjTemplate = new WbjTemplate();
    }

    public function tearDown()
    {
        unset($this->wbjTemplate);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $id = 1;
        $data = $this->getResponseData($id, 'templates');

        $this->mock = new MockHandler([
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], json_encode($data)),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $this->wbjTemplate = new WbjTemplate();

        $data = $this->getResponseData(0, 'templates');

        $this->wbjTemplate->setName($data['data']['attributes']['name']);
        $this->wbjTemplate->setIdentify($data['data']['attributes']['identify']);
        $this->wbjTemplate->setDescription($data['data']['attributes']['description']);
        $this->wbjTemplate->setSubjectCategory($data['data']['attributes']['subjectCategory']);
        $this->wbjTemplate->setItems($data['data']['attributes']['items']);
        $this->wbjTemplate->setDimension($data['data']['attributes']['dimension']);
        $this->wbjTemplate->setExchangeFrequency($data['data']['attributes']['exchangeFrequency']);
        $this->wbjTemplate->setInfoClassify($data['data']['attributes']['infoClassify']);
        $this->wbjTemplate->setInfoCategory($data['data']['attributes']['infoCategory']);

        return $this->wbjTemplate->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->add();
        $this->request();
        $this->response();
    }


    private function request()
    {
        $expectedPath = '/templates';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $requestData = $this->getAddWbjRequestData('templates');

        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($requestData), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(array(), $this->wbjTemplate->getItems());
        $this->assertEquals(1, $this->wbjTemplate->getCategory());
        $this->assertEquals('企业年报信息目录描述', $this->wbjTemplate->getDescription());
        $this->assertEquals(1615279726, $this->wbjTemplate->getCreateTime());
        $this->assertEquals(1615279726, $this->wbjTemplate->getUpdateTime());
    }
}
