<?php


namespace Base\Sdk\Template;

use Base\Sdk\Template\Model\Template;

trait DetailDataTrait
{
    use ArrayDataTrait;

    protected function getTemplateDetailData(
        int $id = 0,
        string $type = 'baseTemplates'
    ): array {
        return [
            "meta" =>[],
            "data" => [
                "type" =>$type,
                "id" =>$id,
                "attributes" => [
                    "name" =>"企业基本信息",
                    "identify" =>"QYJBXX",
                    "subjectCategory" =>[
                        "1",
                        "3"
                    ],
                    "dimension" =>1,
                    "exchangeFrequency" =>4,
                    "infoClassify" =>"5",
                    "infoCategory" =>"1",
                    "description" =>"企业基本信息",
                    "category" =>3,
                    "items" => $this->getItems(),
                    "status" =>0,
                    "createTime" =>1626504022,
                    "updateTime" =>1631862848,
                    "statusTime" =>0
                ],
                "links" => [
                    "self" =>"api.base.qixinyun.com/".$type."/1"
                ]
            ]
        ];
    }
}
