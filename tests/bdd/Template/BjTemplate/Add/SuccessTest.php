<?php


namespace Base\Sdk\Template\BjTemplate\Add;

use Base\Sdk\Template\AddDataTrait;
use Base\Sdk\Template\Model\BjTemplate;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class SuccessTest extends TestCase
{
    use AddDataTrait;

    private $bjTemplate;

    private $mock;

    public function setUp()
    {
        $this->bjTemplate = new BjTemplate();
    }

    public function tearDown()
    {
        unset($this->bjTemplate);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $id = 1;
        $data = $this->getResponseData($id, 'bjTemplates');

        $this->mock = new MockHandler([
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], json_encode($data)),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $this->bjTemplate = new BjTemplate();

        $data = $this->getResponseData(0, 'bjTemplates');

        $this->bjTemplate->setName($data['data']['attributes']['name']);
        $this->bjTemplate->setIdentify($data['data']['attributes']['identify']);
        $this->bjTemplate->setDescription($data['data']['attributes']['description']);
        $this->bjTemplate->setSubjectCategory($data['data']['attributes']['subjectCategory']);
        $this->bjTemplate->setItems($data['data']['attributes']['items']);
        $this->bjTemplate->setDimension($data['data']['attributes']['dimension']);
        $this->bjTemplate->setExchangeFrequency($data['data']['attributes']['exchangeFrequency']);
        $this->bjTemplate->setInfoClassify($data['data']['attributes']['infoClassify']);
        $this->bjTemplate->setInfoCategory($data['data']['attributes']['infoCategory']);
        $this->bjTemplate->setSourceUnit($this->getUserGroup());
        $this->bjTemplate->setGbTemplate($this->getGbTemplate());

        return $this->bjTemplate->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->add();
        $this->request();
        $this->response();
    }


    private function request()
    {
        $expectedPath = '/bjTemplates';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getAddBjRequestData('bjTemplates')), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(1, $this->bjTemplate->getCategory());
        $this->assertEquals(1615279726, $this->bjTemplate->getCreateTime());
        $this->assertEquals(1615279726, $this->bjTemplate->getUpdateTime());
        $this->assertEquals('企业年报信息目录描述', $this->bjTemplate->getDescription());
        $this->assertEquals(array(), $this->bjTemplate->getItems());
    }
}
