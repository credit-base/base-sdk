<?php
namespace Base\Sdk\Template\BjTemplate\Edit;

use Base\Sdk\Template\AddDataTrait;
use Base\Sdk\Template\EditDataTrait;
use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\Template\Adapter\BjTemplate\BjTemplateRestfulAdapter;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是拥有信用刊物管理权限的平台管理员/委办局管理员/操作用户,当我需要编辑信用刊物信息时,在发布表中,编辑对应信用刊物数据
 *           通过编辑信用刊物界面，并根据我所采集的信用刊物数据进行编辑,以便于我可以更好的维护信用刊物管理列表
 * @Scenario: 正常编辑数据
 */

class SuccessTest extends TestCase
{
    use EditDataTrait;

    private $editBjTemplateObject;

    private $mock;
    private $id;

    public function setUp()
    {
        $this->editBjTemplateObject = new BjTemplate();
        $this->id = 1;
    }

    public function tearDown()
    {
        unset($this->editBjTemplateObject);
        unset($this->id);
    }

    /**
    * @Given: 我并未编辑过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getResponseData(1);
        $formatJsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(200, ['Content-Type' => 'application/vnd.api+json'], $formatJsonData),
               new Response(201, ['Content-Type' => 'application/vnd.api+json'], $formatJsonData)
            ]
        );
      
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要编辑的信用刊物数据
    */
    protected function fetchBjTemplate()
    {
        $adapter = new BjTemplateRestfulAdapter();
        $this->editBjTemplateObject = $adapter->fetchOne($this->id);
        return $this->editBjTemplateObject;
    }

    /**
    * @When:当我调用编辑函数,期待编辑成功
    */
    protected function edit()
    {
        $data = $this->getResponseData($this->id, 'bjTemplates');
        $this->editBjTemplateObject->setName($data['data']['attributes']['name']);
        $this->editBjTemplateObject->setIdentify($data['data']['attributes']['identify']);
        $this->editBjTemplateObject->setDescription($data['data']['attributes']['description']);
        $this->editBjTemplateObject->setSubjectCategory($data['data']['attributes']['subjectCategory']);
        $this->editBjTemplateObject->setItems($data['data']['attributes']['items']);
        $this->editBjTemplateObject->setDimension($data['data']['attributes']['dimension']);
        $this->editBjTemplateObject->setExchangeFrequency($data['data']['attributes']['exchangeFrequency']);
        $this->editBjTemplateObject->setInfoClassify($data['data']['attributes']['infoClassify']);
        $this->editBjTemplateObject->setInfoCategory($data['data']['attributes']['infoCategory']);
        $this->editBjTemplateObject->setSourceUnit($this->getUserGroup());
        $this->editBjTemplateObject->setGbTemplate($this->getGbTemplate());
       
        return $this->editBjTemplateObject->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchBjTemplate();
        $this->edit();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/bjTemplates/'.$this->id;

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getBjTemplateOperationRequest($this->id)), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            1615279726,
            $this->editBjTemplateObject->getCreateTime()
        );
        $this->assertEquals(
            1615279726,
            $this->editBjTemplateObject->getUpdateTime()
        );
    }
}
