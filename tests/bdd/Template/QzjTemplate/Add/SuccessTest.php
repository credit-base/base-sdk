<?php


namespace Base\Sdk\Template\QzjTemplate\Add;

use Base\Sdk\Template\AddDataTrait;
use Base\Sdk\Template\Model\QzjTemplate;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @Feature: 我是拥有前置机目录权限的平台管理员/委办局管理员/操作用户,当我需要向前置机报送数据时,
 *           我需要相关的资源目录对应,以便于我生成规则进行数据的导入,在目录管理子系统下的前置机资源目录管理中,
 *           新增前置机资源目录,根据我需要报送的资源目录数据新增对应的前置机资源目录,以便于实现资源目录数据的报送
 * @Scenario: 正常新增数据数据
 */
class SuccessTest extends TestCase
{
    use AddDataTrait;

    private $qzjTemplate;

    private $mock;

    public function setUp()
    {
        $this->qzjTemplate = new QzjTemplate();
    }

    public function tearDown()
    {
        unset($this->qzjTemplate);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $id = 1;
        $data = $this->getResponseData($id, 'templates');
        $data['data']['attributes']['category'] = 20;

        $this->mock = new MockHandler([
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], json_encode($data)),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $this->qzjTemplate = new QzjTemplate();

        $data = $this->getResponseData(1, 'qzjTemplates');

        $this->qzjTemplate->setName($data['data']['attributes']['name']);
        $this->qzjTemplate->setIdentify($data['data']['attributes']['identify']);
        $this->qzjTemplate->setDescription($data['data']['attributes']['description']);
        $this->qzjTemplate->setSubjectCategory($data['data']['attributes']['subjectCategory']);
        $this->qzjTemplate->setItems($data['data']['attributes']['items']);
        $this->qzjTemplate->setDimension($data['data']['attributes']['dimension']);
        $this->qzjTemplate->setExchangeFrequency($data['data']['attributes']['exchangeFrequency']);
        $this->qzjTemplate->setInfoClassify($data['data']['attributes']['infoClassify']);
        $this->qzjTemplate->setInfoCategory($data['data']['attributes']['infoCategory']);
        $this->qzjTemplate->setCategory(20);

        return $this->qzjTemplate->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->add();
        $this->request();
        $this->response();
    }


    private function request()
    {
        $expectedPath = '/qzjTemplates';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $requestData = $this->getAddWbjRequestData('qzjTemplates');
        unset($requestData['data']['id']);
        $requestData['data']['attributes']['category'] = 20;

        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($requestData), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(array(), $this->qzjTemplate->getItems());
        $this->assertEquals(20, $this->qzjTemplate->getCategory());
        $this->assertEquals('企业年报信息目录描述', $this->qzjTemplate->getDescription());
        $this->assertEquals(1615279726, $this->qzjTemplate->getCreateTime());
        $this->assertEquals(1615279726, $this->qzjTemplate->getUpdateTime());
    }
}
