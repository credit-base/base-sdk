<?php
namespace Base\Sdk\Template\QzjTemplate\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Model\QzjTemplate;
use Base\Sdk\Template\Adapter\QzjTemplate\QzjTemplateRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是拥有前置机目录权限的平台管理员/委办局管理员/操作用户,当我需要查看我的资源目录时,
 *           在目录管理子系统下的前置机资源目录管理中,可以查看到我所属委办局下的所有资源目录,
 *           通过列表和详情的形式呈现,以便于我可以更好的管理资源目录
 * @Scenario: 查看前置机资源目录列表
 */

class FailEmptyTest extends TestCase
{
    private $qzjTemplate;

    private $mock;

    public function setUp()
    {
        $this->qzjTemplate = new QzjTemplate();
    }

    public function tearDown()
    {
        unset($this->qzjTemplate);
    }

    /**
    * @Given: 不存在前置机资源目录数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看前置机资源目录列表时
     */
    public function fetchQzjTemplateList()
    {
        $adapter = new QzjTemplateRestfulAdapter();

        list($count, $this->qzjTemplate) = $adapter
        ->search();
        unset($count);

        return $this->qzjTemplate;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchQzjTemplateList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'qzjTemplates';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->qzjTemplate);
    }
}
