<?php
namespace Base\Sdk\Template\QzjTemplate\Browse;

use Base\Sdk\Template\ListDataTrait;
use Base\Sdk\Template\Model\QzjTemplate;
use Marmot\Core;

use PHPUnit\Framework\TestCase;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是拥有前置机目录权限的平台管理员/委办局管理员/操作用户,当我需要查看我的资源目录时,
 *           在目录管理子系统下的前置机资源目录管理中,可以查看到我所属委办局下的所有资源目录,
 *           通过列表和详情的形式呈现,以便于我可以更好的管理资源目录
 * @Scenario: 查看前置机资源目录列表
 */
class ListTest extends TestCase
{
    use ListDataTrait;

    private $qzjTemplate;
    private $qzjTemplateType;

    private $mock;

    public function setUp()
    {
        $this->qzjTemplate = new QzjTemplate();
        $this->qzjTemplateType = 'qzjTemplates';
    }

    public function tearDown()
    {
        unset($this->qzjTemplate);
        unset($this->qzjTemplateType);
    }

    /**
    * @Given: 存在前置机资源目录数据
    */
    protected function prepareData()
    {
        $data = $this->getTemplateListData($this->qzjTemplateType);
        $data['data'][0]['attributes']['category'] = 21;
        
        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
               new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看前置机资源目录列表时
     */
    public function fetchTemplateList()
    {
        $filter = [];
     
        return $this->getTemplateList($filter, $this->qzjTemplateType);
    }

    /**
     * @Then  我可以看见前置机资源目录数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchTemplateList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'qzjTemplates';

        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals($expectedPath, $request->getUri()->getPath());
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function response()
    {
        $qzjTemplateObject = $this->fetchTemplateList();
        
        $qzjTemplateArray = $this->getTemplateListData('qzjTemplates')['data'];
        $qzjTemplateArray[0]['attributes']['category'] = 21;
        
        foreach ($qzjTemplateObject as $item) {
            foreach ($qzjTemplateArray as $val) {
                if ($val['id'] == $item->getId()) {
                    $this->assertEquals($val['attributes']['dimension'], $item->getDimension());
                    $this->assertEquals($val['attributes']['name'], $item->getName());
                    $this->assertEquals($val['attributes']['identify'], $item->getIdentify());
                    $this->assertEquals($val['attributes']['category'], $item->getCategory());
                    $this->assertEquals($val['attributes']['subjectCategory'], $item->getSubjectCategory());
                }
            }
        }
    }
}
