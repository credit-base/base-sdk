<?php
namespace Base\Sdk\Template\QzjTemplate\Browse;

use Base\Sdk\Template\DetailDataTrait;
use Base\Sdk\Template\Model\QzjTemplate;
use Base\Sdk\Template\TemplateFactory;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @Feature: 我是拥有前置机目录权限的平台管理员/委办局管理员/操作用户,当我需要查看我的资源目录时,
 *           在目录管理子系统下的前置机资源目录管理中,可以查看到我所属委办局下的所有资源目录,
 *           通过列表和详情的形式呈现,以便于我可以更好的管理资源目录
 * @Scenario: 查看前置机资源目录详情数据
 */
class DetailTest extends TestCase
{
    use  DetailDataTrait;

    private $qjTemplate;

    private $mock;

    public function setUp()
    {
        $this->qjTemplate = new QzjTemplate();
    }

    public function tearDown()
    {
        unset($this->qjTemplate);
    }

    /**
     * @Given: 存在一条数据
     */
    protected function prepareData()
    {
        $id = 1;
        $data = $this->getTemplateDetailData($id, 'qzjTemplates');
        $data['data']['attributes']['category'] = 20;

        $this->mock = new MockHandler(
            [
                new Response(
                    200,
                    ['Content-Type' => 'application/vnd.api+json'],
                    json_encode($data)
                ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看该条数据详情时
     */
    protected function fetchQzjTemplate($id)
    {

        $adapter = TemplateFactory::getAdapter('qzjTemplates');

        $this->qjTemplate = $adapter->fetchOne($id);

        return $this->qjTemplate;
    }

    /**
     * @Then 我可以看见前置机资源目录详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchQzjTemplate($id);

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'qzjTemplates/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(20, $this->qjTemplate->getCategory());
        $this->assertEquals("企业基本信息", $this->qjTemplate->getName());
        $this->assertEquals("QYJBXX", $this->qjTemplate->getIdentify());
        $this->assertEquals(4, $this->qjTemplate->getExchangeFrequency());
        $this->assertEquals(["1","3"], $this->qjTemplate->getSubjectCategory());
        $this->assertEquals(1, $this->qjTemplate->getDimension());
        $this->assertEquals("5", $this->qjTemplate->getInfoClassify());
        $this->assertEquals("1", $this->qjTemplate->getInfoCategory());
        $this->assertEquals("企业基本信息", $this->qjTemplate->getDescription());
        $this->assertEquals(1, $this->qjTemplate->getId());
        $this->assertEquals(1626504022, $this->qjTemplate->getCreateTime());
        $this->assertEquals(1631862848, $this->qjTemplate->getUpdateTime());
    }
}
