<?php
namespace Base\Sdk\Template\QzjTemplate\Edit;

use Base\Sdk\Template\EditDataTrait;
use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Model\QzjTemplate;
use Base\Sdk\Template\Adapter\QzjTemplate\QzjTemplateRestfulAdapter;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是拥有前置机目录权限的平台管理员/委办局管理员/操作用户,当我需要向前置机报送数据时,
 *           我需要相关的资源目录对应,以便于我生成规则进行数据的导入,在目录管理子系统下的前置机资源目录管理中,
 *           编辑前置机资源目录,根据我需要报送的资源目录数据新增对应的前置机资源目录,以便于实现资源目录数据的报送
 * @Scenario: 正常编辑数据
 */
class SuccessTest extends TestCase
{
    use EditDataTrait;

    private $editQzjTemplateObject;

    private $mock;
    private $id;

    public function setUp()
    {
        $this->editQzjTemplateObject = new QzjTemplate();
        $this->id = 1;
    }

    public function tearDown()
    {
        unset($this->editQzjTemplateObject);
        unset($this->id);
    }

    /**
    * @Given: 我并未编辑过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getResponseData(1);
        $data['data']['attributes']['category'] = 21;
        $formatJsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(200, ['Content-Type' => 'application/vnd.api+json'], $formatJsonData),
               new Response(201, ['Content-Type' => 'application/vnd.api+json'], $formatJsonData)
            ]
        );
      
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要编辑的前置机资源目录数据
    */
    protected function fetchQzjTemplate()
    {
        $adapter = new QzjTemplateRestfulAdapter();
        $this->editQzjTemplateObject = $adapter->fetchOne($this->id);
        return $this->editQzjTemplateObject;
    }

    /**
    * @When:当我调用编辑函数,期待编辑成功
    */
    protected function edit()
    {
        $data = $this->getResponseData($this->id, 'qzjTemplates');
        $this->editQzjTemplateObject->setName($data['data']['attributes']['name']);
        $this->editQzjTemplateObject->setIdentify($data['data']['attributes']['identify']);
        $this->editQzjTemplateObject->setDescription($data['data']['attributes']['description']);
        $this->editQzjTemplateObject->setSubjectCategory($data['data']['attributes']['subjectCategory']);
        $this->editQzjTemplateObject->setItems($data['data']['attributes']['items']);
        $this->editQzjTemplateObject->setDimension($data['data']['attributes']['dimension']);
        $this->editQzjTemplateObject->setExchangeFrequency($data['data']['attributes']['exchangeFrequency']);
        $this->editQzjTemplateObject->setInfoClassify($data['data']['attributes']['infoClassify']);
        $this->editQzjTemplateObject->setInfoCategory($data['data']['attributes']['infoCategory']);
        $this->editQzjTemplateObject->setCategory(21);
        $this->editQzjTemplateObject->setSourceUnit($this->getUserGroup(1));

        return $this->editQzjTemplateObject->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchQzjTemplate();
        $this->edit();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/qzjTemplates/'.$this->id;

        $request = $this->mock->getLastRequest();

        $requestContents = $this->getWbjTemplateOperationRequest($this->id, 'qzjTemplates');
        $requestContents['data']['attributes']['category'] = 21;
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($requestContents), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            1615279726,
            $this->editQzjTemplateObject->getCreateTime()
        );
        $this->assertEquals(
            1615279726,
            $this->editQzjTemplateObject->getUpdateTime()
        );
    }
}
