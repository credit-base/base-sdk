<?php


namespace Base\Sdk\Template;

trait ListDataTrait
{
    use ArrayDataTrait;

    protected function getTemplateList(array $filter = [], $type = 'baseTemplates')
    {
        $adapter = TemplateFactory::getAdapter($type);

        list($count, $list) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $list;
    }

    protected function getTemplateListData($type = 'baseTemplates') : array
    {
        return [
            "meta" => [
                "count" =>1,
                "links" => [
                    "first" =>null,
                    "last" =>null,
                    "prev" =>null,
                    "next" =>null
                ]
            ],
            "links" => [
                "first" =>null,
                "last" =>null,
                "prev" =>null,
                "next" =>null
            ],
            "data" =>[
                [
                    "type" =>$type,
                    "id" =>"1",
                    "attributes" => [
                        "name" =>"企业基本信息",
                        "identify" =>"QYJBXX",
                        "subjectCategory" =>[
                            "1",
                            "3"
                        ],
                        "dimension" =>1,
                        "exchangeFrequency" =>4,
                        "infoClassify" =>"5",
                        "infoCategory" =>"1",
                        "description" =>"企业基本信息",
                        "category" =>3,
                        "items" => $this->getItems(),
                        "status" =>0,
                        "createTime" =>1626504022,
                        "updateTime" =>1631862848,
                        "statusTime" =>0
                    ],
                    "links" => [
                        "self" =>"api.base.qixinyun.com/".$type."/1"
                    ]
                ]
            ]
        ];
    }
}
