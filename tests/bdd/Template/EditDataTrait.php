<?php


namespace Base\Sdk\Template;

trait EditDataTrait
{
    use ArrayDataTrait;

    protected function getBjTemplateOperationRequest($id = 0, $type = 'bjTemplates') : array
    {
        return array(
            "data" => array(
                "type" => $type,
                "id" =>$id,
                "attributes" => array(
                    "name" =>"社保缴费信息",
                    "identify" =>"SBJFXX",
                    "subjectCategory" =>[
                        "2"
                    ],
                    "dimension" =>1,
                    "items" =>array(),
                    "infoClassify" =>5,
                    "infoCategory" =>4,
                    "exchangeFrequency" =>7,
                    "description" =>"企业年报信息目录描述",
                ),
                "relationships" => array(
                    "sourceUnit" => array(
                        "data" => array(
                            array("type" => "userGroups", "id" => 1)
                        )
                    ),
                    "gbTemplate" => array(
                        "data" => array(
                            array("type" => "gbTemplates", "id" => 1)
                        )
                    )
                )
            )
        );
    }

    protected function getGbTemplateOperationRequest($id = 0, $type = 'gbTemplates') : array
    {
        return array(
            "data" => array(
                "type" => $type,
                "id" =>$id,
                "attributes" => array(
                    "name" =>"社保缴费信息",
                    "identify" =>"SBJFXX",
                    "subjectCategory" =>[
                        "2"
                    ],
                    "dimension" =>1,
                    "exchangeFrequency" =>7,
                    "infoClassify" =>5,
                    "infoCategory" =>4,
                    "description" =>"企业年报信息目录描述",
                    "items" =>array(),
                ),

            )
        );
    }

    protected function getWbjTemplateOperationRequest($id = 0, $type = 'templates') : array
    {
        return array(
            "data" => array(
                "type" => $type,
                "id" => $id,
                "attributes" => array(
                    "name" =>"社保缴费信息",
                    "identify" =>"SBJFXX",
                    "subjectCategory" =>["2"],
                    "dimension" =>1,
                    "exchangeFrequency" =>7,
                    "infoClassify" =>5,
                    "infoCategory" =>4,
                    "description" =>"企业年报信息目录描述",
                    "items" =>array(),
                ),
                "relationships" => array(
                    "sourceUnit" => array(
                        "data" => array(
                            array("type" => "userGroups", "id" => 1)
                        )
                    )
                )
            )
        );
    }

    protected function getBaseTemplateOperationRequest($id = 0, $type = 'baseTemplates') : array
    {
        return array(
            "data" => array(
                "type" => $type,
                "id" =>$id,
                "attributes" => array(
                    "name" =>"社保缴费信息",
                    "identify" =>"SBJFXX",
                    "subjectCategory" =>["2"],
                    "dimension" =>1,
                    "items" =>array(),
                    "infoClassify" =>5,
                    "infoCategory" =>4,
                    "exchangeFrequency" =>7,
                    "description" =>"企业年报信息目录描述",
                ),

            )
        );
    }
}
