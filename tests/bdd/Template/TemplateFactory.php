<?php


namespace Base\Sdk\Template;

class TemplateFactory
{
    const ADAPTER_MAPS = [
        'baseTemplates' => 'Base\Sdk\Template\Adapter\BaseTemplate\BaseTemplateRestfulAdapter',
        'bjTemplates' => 'Base\Sdk\Template\Adapter\BjTemplate\BjTemplateRestfulAdapter',
        'gbTemplates' => 'Base\Sdk\Template\Adapter\GbTemplate\GbTemplateRestfulAdapter',
        'wbjTemplates' => 'Base\Sdk\Template\Adapter\WbjTemplate\WbjTemplateRestfulAdapter',
        'qzjTemplates' => 'Base\Sdk\Template\Adapter\QzjTemplate\QzjTemplateRestfulAdapter',
    ];

    public static function getAdapter($type)
    {
        $adapter = self::ADAPTER_MAPS[$type];
        return new $adapter;
    }
}
