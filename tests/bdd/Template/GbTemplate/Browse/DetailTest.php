<?php
namespace Base\Sdk\Template\GbTemplate\Browse;

use Base\Sdk\Template\Adapter\GbTemplate\GbTemplateRestfulAdapter;
use Base\Sdk\Template\DetailDataTrait;
use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\ArrayDataTrait;
use Base\Sdk\Template\TemplateFactory;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DetailTest extends TestCase
{
    use  DetailDataTrait;

    private $gbTemplate;

    private $mock;

    public function setUp()
    {
        $this->gbTemplate = new GbTemplate();
    }

    public function tearDown()
    {
        unset($this->gbTemplate);
    }

    /**
     * @Given: 存在一条数据
     */
    protected function prepareData()
    {
        $id = 1;
        $data = $this->getTemplateDetailData($id, 'gbTemplates');

        $this->mock = new MockHandler(
            [
                new Response(
                    200,
                    ['Content-Type' => 'application/vnd.api+json'],
                    json_encode($data)
                ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看该条数据详情时
     */
    protected function fetchGbTemplate($id)
    {

        $adapter = TemplateFactory::getAdapter('gbTemplates');

        $this->gbTemplate = $adapter->fetchOne($id);

        return $this->gbTemplate;
    }

    /**
     * @Then 我可以看见详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchGbTemplate($id);

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'gbTemplates/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(1626504022, $this->gbTemplate->getCreateTime());
        $this->assertEquals(1631862848, $this->gbTemplate->getUpdateTime());
        $this->assertEquals(3, $this->gbTemplate->getCategory());
        $this->assertEquals(1, $this->gbTemplate->getId());
    }
}
