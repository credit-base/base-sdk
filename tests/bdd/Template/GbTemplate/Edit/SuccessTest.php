<?php
namespace Base\Sdk\Template\GbTemplate\Edit;

use Base\Sdk\Template\AddDataTrait;
use Base\Sdk\Template\EditDataTrait;
use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\Template\Adapter\GbTemplate\GbTemplateRestfulAdapter;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是拥有信用刊物管理权限的平台管理员/委办局管理员/操作用户,当我需要编辑信用刊物信息时,在发布表中,编辑对应信用刊物数据
 *           通过编辑信用刊物界面，并根据我所采集的信用刊物数据进行编辑,以便于我可以更好的维护信用刊物管理列表
 * @Scenario: 正常编辑数据
 */

class SuccessTest extends TestCase
{
    use EditDataTrait;

    private $editGbTemplateObject;

    private $mock;
    private $id;

    public function setUp()
    {
        $this->editGbTemplateObject = new GbTemplate();
        $this->id = 1;
    }

    public function tearDown()
    {
        unset($this->editGbTemplateObject);
        unset($this->id);
    }

    /**
    * @Given: 我并未编辑过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getResponseData(1);
        $formatJsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(200, ['Content-Type' => 'application/vnd.api+json'], $formatJsonData),
               new Response(201, ['Content-Type' => 'application/vnd.api+json'], $formatJsonData)
            ]
        );
      
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要编辑的信用刊物数据
    */
    protected function fetchGbTemplate()
    {
        $adapter = new GbTemplateRestfulAdapter();
        $this->editGbTemplateObject = $adapter->fetchOne($this->id);
        return $this->editGbTemplateObject;
    }

    /**
    * @When:当我调用编辑函数,期待编辑成功
    */
    protected function edit()
    {
        $data = $this->getResponseData($this->id, 'gbTemplates');
        $this->editGbTemplateObject->setName($data['data']['attributes']['name']);
        $this->editGbTemplateObject->setIdentify($data['data']['attributes']['identify']);
        $this->editGbTemplateObject->setDescription($data['data']['attributes']['description']);
        $this->editGbTemplateObject->setSubjectCategory($data['data']['attributes']['subjectCategory']);
        $this->editGbTemplateObject->setItems($data['data']['attributes']['items']);
        $this->editGbTemplateObject->setDimension($data['data']['attributes']['dimension']);
        $this->editGbTemplateObject->setExchangeFrequency($data['data']['attributes']['exchangeFrequency']);
        $this->editGbTemplateObject->setInfoClassify($data['data']['attributes']['infoClassify']);
        $this->editGbTemplateObject->setInfoCategory($data['data']['attributes']['infoCategory']);
       
        return $this->editGbTemplateObject->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchGbTemplate();
        $this->edit();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/gbTemplates/'.$this->id;

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getGbTemplateOperationRequest($this->id)), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            1615279726,
            $this->editGbTemplateObject->getCreateTime()
        );
        $this->assertEquals(
            1615279726,
            $this->editGbTemplateObject->getUpdateTime()
        );
    }
}
