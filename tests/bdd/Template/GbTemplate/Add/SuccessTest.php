<?php


namespace Base\Sdk\Template\GbTemplate\Add;

use Base\Sdk\Template\AddDataTrait;
use Base\Sdk\Template\Model\GbTemplate;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class SuccessTest extends TestCase
{
    use AddDataTrait;

    private $gbTemplate;

    private $mock;

    public function setUp()
    {
        $this->gbTemplate = new GbTemplate();
    }

    public function tearDown()
    {
        unset($this->gbTemplate);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $id = 1;
        $data = $this->getResponseData($id, 'gbTemplates');

        $this->mock = new MockHandler([
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], json_encode($data)),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $this->gbTemplate = new GbTemplate();

        $data = $this->getResponseData(0, 'gbTemplates');

        $this->gbTemplate->setName($data['data']['attributes']['name']);
        $this->gbTemplate->setIdentify($data['data']['attributes']['identify']);
        $this->gbTemplate->setDescription($data['data']['attributes']['description']);
        $this->gbTemplate->setSubjectCategory($data['data']['attributes']['subjectCategory']);
        $this->gbTemplate->setItems($data['data']['attributes']['items']);
        $this->gbTemplate->setDimension($data['data']['attributes']['dimension']);
        $this->gbTemplate->setExchangeFrequency($data['data']['attributes']['exchangeFrequency']);
        $this->gbTemplate->setInfoClassify($data['data']['attributes']['infoClassify']);
        $this->gbTemplate->setInfoCategory($data['data']['attributes']['infoCategory']);

        return $this->gbTemplate->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->add();
        $this->request();
        $this->response();
    }


    private function request()
    {
        $expectedPath = '/gbTemplates';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $requestData = $this->getAddGbRequestData('gbTemplates');
        unset($requestData['data']['relationships']);

        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($requestData), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(1, $this->gbTemplate->getCategory());
        $this->assertEquals('企业年报信息目录描述', $this->gbTemplate->getDescription());
        $this->assertEquals(array(), $this->gbTemplate->getItems());
        $this->assertEquals(1615279726, $this->gbTemplate->getCreateTime());
        $this->assertEquals(1615279726, $this->gbTemplate->getUpdateTime());
    }
}
