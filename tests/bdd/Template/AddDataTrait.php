<?php
namespace Base\Sdk\Template;

trait AddDataTrait
{
    use ArrayDataTrait;

    protected function getAddBjRequestData($type = 'bjTemplates') : array
    {
        return array(
            "data" => array(
                "type" => $type,
                "id" =>0,
                "attributes" => array(
                    "name" =>"社保缴费信息",
                    "identify" =>"SBJFXX",
                    "subjectCategory" =>[
                        "2"
                    ],
                    "dimension" =>1,
                    "items" =>array(),//$this->getItems(),
                    "infoClassify" =>5,
                    "infoCategory" =>4,
                    "exchangeFrequency" =>7,
                    "description" =>"企业年报信息目录描述",
                ),
                "relationships" => array(
                    "sourceUnit" => array(
                        "data" => array(
                            array("type" => "userGroups", "id" => 1)
                        )
                    ),
                    "gbTemplate" => array(
                        "data" => array(
                            array("type" => "gbTemplates", "id" => 1)
                        )
                    )
                )
            )
        );
    }

    protected function getAddGbRequestData($type = 'gbTemplates') : array
    {
        return array(
            "data" => array(
                "type" => $type,
                "id" =>0,
                "attributes" => array(
                    "name" =>"社保缴费信息",
                    "identify" =>"SBJFXX",
                    "subjectCategory" =>[
                        "2"
                    ],
                    "dimension" =>1,
                    "exchangeFrequency" =>7,
                    "infoClassify" =>5,
                    "infoCategory" =>4,
                    "description" =>"企业年报信息目录描述",
                    "items" =>array(),//$this->getItems()
                ),

            )
        );
    }

    protected function getAddWbjRequestData($type = 'templates') : array
    {
        return array(
            "data" => array(
                "type" => $type,
                "id" =>0,
                "attributes" => array(
                    "name" =>"社保缴费信息",
                    "identify" =>"SBJFXX",
                    "subjectCategory" =>["2"],
                    "dimension" =>1,
                    "exchangeFrequency" =>7,
                    "infoClassify" =>5,
                    "infoCategory" =>4,
                    "description" =>"企业年报信息目录描述",
                    "items" =>array(),
                ),
                "relationships" => array(
                    "sourceUnit" => array(
                        "data" => array(
                            array("type" => "userGroups", "id" => 0)
                        )
                    )
                )
            )
        );
    }
}
