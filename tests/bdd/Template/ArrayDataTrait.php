<?php


namespace Base\Sdk\Template;

use Base\Sdk\Template\Model\GbTemplate;
use Base\Sdk\UserGroup\Model\UserGroup;

trait ArrayDataTrait
{
    /**
     * @SuppressWarnings(PHPMD)
     */
    private function getItems() : array
    {
        return [
            [
                "name" =>"主体名称",
                "type" =>"1",
                "length" =>"200",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"ZTMC",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"统一社会信用代码",
                "type" =>"1",
                "length" =>"18",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"TYSHXYDM",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"成立日期",
                "type" =>"2",
                "length" =>"8",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"CLRQ",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"核准日期",
                "type" =>"2",
                "length" =>"8",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"HZRQ",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"经营场所",
                "type" =>"1",
                "length" =>"255",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"JYCS",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"注册资本（万）",
                "type" =>"1",
                "length" =>"255",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"ZCZB",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"营业期限自",
                "type" =>"2",
                "length" =>"8",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"YYQXQ",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"营业期限至",
                "type" =>"2",
                "length" =>"8",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"YYQXZ",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"经营范围",
                "type" =>"1",
                "length" =>"2000",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"JYFW",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"登记机关",
                "type" =>"1",
                "length" =>"255",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"DJJG",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"法定代表人",
                "type" =>"1",
                "length" =>"255",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"FDDBR",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"法人身份证号",
                "type" =>"1",
                "length" =>"200",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"FRZJHM",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"登记状态",
                "type" =>"1",
                "length" =>"200",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"DJZT",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"企业类型",
                "type" =>"1",
                "length" =>"200",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"QYLX",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"企业类型代码",
                "type" =>"1",
                "length" =>"200",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"QYLXDM",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"行业门类",
                "type" =>"1",
                "length" =>"4",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"HYML",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"行业代码",
                "type" =>"1",
                "length" =>"200",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"HYDM",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"所属区域",
                "type" =>"1",
                "length" =>"50",
                "options" =>[],
                "remarks" =>"",
                "identify" =>"SSQY",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"主体类别",
                "type" =>"6",
                "length" =>"50",
                "options" =>[
                    "法人及非法人组织",
                    "个体工商户"
                ],
                "remarks" =>"法人及非法人组织;自然人;个体工商户，支持多选",
                "identify" =>"ZTLB",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"公开范围",
                "type" =>"5",
                "length" =>"20",
                "options" =>[
                    "社会公开"
                ],
                "remarks" =>"支持单选",
                "identify" =>"GKFW",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"更新频率",
                "type" =>"5",
                "length" =>"20",
                "options" =>[
                    "每月"
                ],
                "remarks" =>"支持单选",
                "identify" =>"GXPL",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"信息分类",
                "type" =>"5",
                "length" =>"50",
                "options" =>[
                    "其他"
                ],
                "remarks" =>"支持单选",
                "identify" =>"XXFL",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ],
            [
                "name" =>"信息类别",
                "type" =>"5",
                "length" =>"50",
                "options" =>[
                    "基础信息"
                ],
                "remarks" =>"信息性质类型，支持单选",
                "identify" =>"XXLB",
                "isMasked" =>"0",
                "maskRule" =>[],
                "dimension" =>"2",
                "isNecessary" =>"0"
            ]
        ];
    }

    private function getInclude() : array
    {
        return [
            [
                "type" =>"userGroups",
                "id" =>"1",
                "attributes" => [
                    "name" =>"人力和资源社会保障局",
                    "shortName" =>"人社局",
                    "status" =>0,
                    "createTime" =>1558345495,
                    "updateTime" =>1598015256,
                    "statusTime" =>0
                ]
            ],
            [
                "type" =>"gbTemplates",
                "id" =>"1",
                "attributes" => [
                    "name" =>"企业年报信息",
                    "identify" =>"QYNBXX",
                    "subjectCategory" =>[
                        "1"
                    ],
                    "dimension" =>1,
                    "exchangeFrequency" =>7,
                    "infoClassify" =>"5",
                    "infoCategory" =>"4",
                    "description" =>"企业年报信息目录描述",
                    "category" =>2,
                    "items" =>$this->getItems(),
                    "status" =>0,
                    "createTime" =>1615279079,
                    "updateTime" =>1626076726,
                    "statusTime" =>0
                ]
            ]
        ];
    }

    private function getRelationships() : array
    {
        return [
            "sourceUnit" => [
                "data" => [
                    "type" =>"userGroups",
                    "id" =>"1"
                ]
            ],
            "gbTemplate" => [
                "data" => [
                    "type" =>"gbTemplates",
                    "id" =>"1"
                ]
            ]
        ];
    }

    protected function getResponseData($id = 0, $type = 'bjTemplates') : array
    {
        return [
            "meta" =>[],
            "data" => [
                "type" =>$type,
                "id" =>$id,
                "attributes" => [
                    "name" =>"社保缴费信息",
                    "identify" =>"SBJFXX",
                    "subjectCategory" =>[
                        "2"
                    ],
                    "dimension" =>1,
                    "exchangeFrequency" =>7,
                    "infoClassify" =>5,
                    "infoCategory" =>4,
                    "description" =>"企业年报信息目录描述",
                    "category" =>1,
                    "items" => array(),//$this->getItems(),
                    "status" =>0,
                    "createTime" =>1615279726,
                    "updateTime" =>1615279726,
                    "statusTime" =>0
                ],
                "relationships" => $this->getRelationships(),
                "links" => [
                    "self" =>"api.base.qixinyun.com/".$type."/".$id
                ]
            ],
            "included" => $this->getInclude()
        ];
    }

    protected function getUserGroup($id = 1) : UserGroup
    {
        return new UserGroup($id);
    }

    protected function getGbTemplate($id = 1) : GbTemplate
    {
        return new GbTemplate($id);
    }
}
