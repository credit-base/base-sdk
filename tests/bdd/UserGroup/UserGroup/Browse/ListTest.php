<?php


namespace Base\Sdk\UserGroup\UserGroup\Browse;

use Base\Sdk\UserGroup\UserGroup\ListDataTrait;
use Base\Sdk\UserGroup\Model\UserGroup;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ListTest extends TestCase
{
    use ListDataTrait;

    private $userGroup;

    private $mock;

    public function setUp()
    {
        $this->userGroup = new UserGroup();
    }

    public function tearDown()
    {
        unset($this->userGroup);
    }

    /**
     * @Given: 存在委办局数据
     */
    protected function prepareData()
    {
        $data = $this->getUserGroupListData();

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看委办局列表时
     */
    public function fetchUserGroupList() : array
    {
        $filter = [];
        return $this->getUserGroupList($filter);
    }

    /**
     * @Then  我可以看见企业数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUserGroupList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('userGroups', $request->getUri()->getPath());
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function response()
    {
        $memberObject = $this->fetchUserGroupList();
        $memberArray = $this->getUserGroupListData()['data'];

        foreach ($memberObject as $item) {
            foreach ($memberArray as $val) {
                if ($val['id'] == $item->getId()) {
                    $this->assertEquals($val['attributes']['name'], $item->getName());
                    $this->assertEquals($val['attributes']['updateTime'], $item->getUpdateTime());
                }
            }
        }
    }
}
