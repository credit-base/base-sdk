<?php


namespace Base\Sdk\UserGroup\UserGroup;

use Base\Sdk\UserGroup\Adapter\UserGroupRestfulAdapter;

trait ListDataTrait
{
    protected function getUserGroupList(array $filter = []) : array
    {
        $adapter = new UserGroupRestfulAdapter();

        list($count, $userGroupList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $userGroupList;
    }

    protected function getUserGroupListData(): array
    {
        return [
            "meta" => [
                "count" => 4,
                "links" => [
                    "first" => null,
                    "last" => null,
                    "prev" => null,
                    "next" => null
                ]
            ],
            "links" => [
                "first" => null,
                "last" => null,
                "prev" => null,
                "next" => null
            ],
            "data" => [
                [
                    "type" => "userGroups",
                    "id" => "15",
                    "attributes" => [
                        "name" => "人力和资源社会保障局",
                        "shortName" => "人社局",
                        "status" => 0,
                        "createTime" => 1558345495,
                        "updateTime" => 1598015256,
                        "statusTime" => 0
                    ],
                    "links" => [
                        "self" => "api.base.qixinyun.com/userGroups/15"
                    ]
                ],
                [
                    "type" => "userGroups",
                    "id" => "1",
                    "attributes" => [
                        "name" => "发展和改革委员会",
                        "shortName" => "发改委",
                        "status" => 0,
                        "createTime" => 1558345495,
                        "updateTime" => 1610539122,
                        "statusTime" => 0
                    ],
                    "links" => [
                        "self" => "api.base.qixinyun.com/userGroups/1"
                    ]
                ]
            ]
        ];
    }
}
