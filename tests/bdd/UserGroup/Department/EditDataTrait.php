<?php


namespace Base\Sdk\UserGroup\Department;

trait EditDataTrait
{
    protected function getDepartmentOperationRequest() : array
    {
        return array(
            "data" => array(
                "type" => "departments",
                "attributes" => array(
                    "name" => '金融办'
                )
            )
        );
    }

    protected function getDepartmentEditDetail() : array
    {
        return [
            "meta" =>[],
            "data" => [
                "type" =>"departments",
                "id" =>"29",
                "attributes" => [
                    "name" =>"财务综合科",
                    "status" =>0,
                    "createTime" =>1597808826,
                    "updateTime" =>1619491552,
                    "statusTime" =>0
                ],
                "relationships" => [
                    "userGroup" => [
                        "data" => [
                            "type" =>"userGroups",
                            "id" =>"8"
                        ]
                    ]
                ],
                "links" => [
                    "self" =>"api.base.qixinyun.com/departments/29"
                ]
            ],
            "included" =>[
                [
                    "type" =>"userGroups",
                    "id" =>"8",
                    "attributes" => [
                        "name" =>"市场监督管理局",
                        "shortName" =>"市监局",
                        "status" =>0,
                        "createTime" =>1558345495,
                        "updateTime" =>1598015256,
                        "statusTime" =>0
                    ]
                ]
            ]
        ];
    }
}
