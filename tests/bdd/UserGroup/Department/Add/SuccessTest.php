<?php
namespace Base\Sdk\UserGroup\Department\Add;

use Base\Sdk\UserGroup\Department\DetailTrait;
use Base\Sdk\UserGroup\Model\Department;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Department\AddDataTrait;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class SuccessTest extends TestCase
{
    use DetailTrait, AddDataTrait;

    private $department;

    private $mock;

    public function setUp()
    {
        $this->department = new Department();
    }

    public function tearDown()
    {
        unset($this->department);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getDepartmentDetail();
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;

        $jsonData = json_encode($data);

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], json_encode($this->getEmpty())),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $this->department = new Department();
        $this->department->setUserGroup(new UserGroup(8));
        $this->department->setName('主任办公室');

        return $this->department->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->add();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/departments';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getDepartmentAddRequest('主任办公室', 8)), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('主任办公室', $this->department->getName());
        $this->assertEquals(0, $this->department->getStatusTime());
        $this->assertEquals(1597808772, $this->department->getCreateTime());
        $this->assertEquals(0, $this->department->getUpdateTime());
    }
}
