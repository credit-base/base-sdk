<?php


namespace Base\Sdk\UserGroup\Department;

trait DetailTrait
{
    protected function getDepartmentDetail() : array
    {
        return [
            "meta" =>[],
            "data" => [
                "type" =>"departments",
                "id" =>"22",
                "attributes" => [
                    "name" =>"主任办公室",
                    "status" =>0,
                    "createTime" =>1597808772,
                    "updateTime" =>1597808772,
                    "statusTime" =>0
                ],
                "relationships" => [
                    "userGroup" => [
                        "data" => [
                            "type" =>"userGroups",
                            "id" =>"8"
                        ]
                    ]
                ],
                "links" => [
                    "self" =>"api.base.qixinyun.com/departments/22"
                ]
            ],
            "included" =>[
                [
                    "type" =>"userGroups",
                    "id" =>"8",
                    "attributes" => [
                        "name" =>"市场监督管理局",
                        "shortName" =>"市监局",
                        "status" =>0,
                        "createTime" =>1558345495,
                        "updateTime" =>1598015256,
                        "statusTime" =>0
                    ]
                ]
            ]
        ];
    }

    protected function getEmpty() : array
    {
        return [
            "errors" => [
                [
                    "id" => "10",
                    "links" => [
                        "about" => ""
                    ],
                    "status" => "404",
                    "code" => "RESOURCE_NOT_EXIST",
                    "title" => "Resource not exist",
                    "detail" => "Server can not find resource",
                    "source" => [],
                    "meta" => []
                ]
            ]
        ];
    }
}
