<?php

namespace Base\Sdk\UserGroup\Department;

trait AddDataTrait
{
    protected function getDepartmentAddRequest($name = '', $userGroupId = 1) : array
    {
        return array(
            "data" => array(
                "type" => "departments",
                "attributes" => array(
                    "name" => $name
                ),
                "relationships" => array(
                    "userGroup" => array(
                        "data" => array(
                            array("type" => "userGroups", "id" => $userGroupId)
                        )
                    )
                )
            )
        );
    }
}
