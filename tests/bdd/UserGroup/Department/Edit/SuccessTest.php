<?php
namespace Base\Sdk\UserGroup\Department\Edit;

use Base\Sdk\UserGroup\Adapter\DepartmentRestfulAdapter;
use Base\Sdk\UserGroup\Department\EditDataTrait;
use Base\Sdk\UserGroup\Model\Department;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class SuccessTest extends TestCase
{
    use EditDataTrait;

    private $editDepartment;

    private $mock;
    private $id;

    public function setUp()
    {
        $this->editDepartment = new Department();
        $this->id = 29;
    }

    public function tearDown()
    {
        unset($this->editDepartment);
        unset($this->id);
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function prepareData()
    {
        $data  = $this->getDepartmentEditDetail();
        $formatJsonData = json_encode($data);
        $empty = [
            "errors" => [
                [
                    "id" => "10",
                    "links" => [
                        "about" => ""
                    ],
                    "status" => "404",
                    "code" => "RESOURCE_NOT_EXIST",
                    "title" => "Resource not exist",
                    "detail" => "Server can not find resource",
                    "source" => [],
                    "meta" => []
                ]
            ]
        ];

        $this->mock = new MockHandler(
            [
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], json_encode($data)),
                new Response(404, ['Content-Type' => 'application/vnd.api+json'], json_encode($empty)),
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], $formatJsonData),
            ]
        );

        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:获取需要编辑的科室数据
     */
    protected function fetchDepartment() : Department
    {
        $adapter = new DepartmentRestfulAdapter();
        $this->editDepartment = $adapter->fetchOne($this->id);
        return $this->editDepartment;
    }

    /**
     * @When:当我调用编辑函数,期待编辑成功
     */
    protected function edit() : bool
    {
        $this->editDepartment->setName('金融办');
        return $this->editDepartment->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchDepartment();
        $this->edit();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/departments/'.$this->id;

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();

        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getDepartmentOperationRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('财务综合科', $this->editDepartment->getName());
        $this->assertEquals(1597808826, $this->editDepartment->getCreateTime());
        $this->assertEquals(1619491552, $this->editDepartment->getUpdateTime());
    }
}
