<?php


namespace Base\Sdk\UserGroup\Department;

use Base\Sdk\UserGroup\Adapter\DepartmentRestfulAdapter;

trait ListDataTrait
{
    protected function getDepartmentList(array $filter = []) : array
    {
        $adapter = new DepartmentRestfulAdapter();

        list($count, $userGroupList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $userGroupList;
    }

    protected function getDepartmentListData(): array
    {
        return [
            "meta" => [
                "count" =>25,
                "links" => [
                    "first" =>1,
                    "last" =>2,
                    "prev" =>null,
                    "next" =>2
                ]
            ],
            "links" => [
                "first" =>"api.base.qixinyun.com/departments/?include=userGroup&page[number]=1&page[size]=20",
                "last" =>"api.base.qixinyun.com/departments/?include=userGroup&page[number]=2&page[size]=20",
                "prev" =>null,
                "next" =>"api.base.qixinyun.com/departments/?include=userGroup&page[number]=2&page[size]=20"
            ],
            "data" =>[
                [
                    "type" =>"departments",
                    "id" =>"1",
                    "attributes" => [
                        "name" =>"主任办公室",
                        "status" =>0,
                        "createTime" =>1597798915,
                        "updateTime" =>1597798915,
                        "statusTime" =>0
                    ],
                    "relationships" => [
                        "userGroup" => [
                            "data" => [
                                "type" =>"userGroups",
                                "id" =>"1"
                            ]
                        ]
                    ],
                    "links" => [
                        "self" =>"api.base.qixinyun.com/departments/1"
                    ]
                ]
            ],
            "included" =>[
                [
                    "type" =>"userGroups",
                    "id" =>"1",
                    "attributes" => [
                        "name" =>"发展和改革委员会",
                        "shortName" =>"发改委",
                        "status" =>0,
                        "createTime" =>1558345495,
                        "updateTime" =>1610539122,
                        "statusTime" =>0
                    ]
                ]
            ]
        ];
    }
}
