<?php
namespace Base\UserGroup\Department\Browse;

use Base\Sdk\UserGroup\Department\DetailTrait;
use Base\Sdk\UserGroup\Model\UserGroup;
use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\UserGroup\Model\Department;
use Base\Sdk\UserGroup\Adapter\DepartmentRestfulAdapter;
use Marmot\Framework\Classes\Request;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是一名分公司员工（平管/超管）,当我需要查看我的委办局数据时,在委办局管理中,可以查看到我的所有委办局数据,
 *           通过列表和详情的形式查看到我所有的委办局信息,以便于我可以了解委办局的情况
 * @Scenario: 查看委办局数据详情
 */

class DetailTest extends TestCase
{
    use DetailTrait;
    
    private $department;

    private $mock;

    public function setUp()
    {
        $this->department = new Department();
    }

    public function tearDown()
    {
        unset($this->department);
    }

    /**
    * @Given: 存在一条委办局数据
    */
    protected function prepareData()
    {
        $jsonData = json_encode($this->getDepartmentDetail());

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看该条委办局数据详情时
     * @param int $id
     * @return UserGroup
     */
    protected function fetchDepartment(int $id) : Department
    {

        $adapter = new DepartmentRestfulAdapter();

        $this->department = $adapter->fetchOne($id);

        return $this->department;
    }

    /**
     * @Then 我可以看见委办局详情, 名字为发展和该和委员会, 简称为发改委, 创建时间, 更新时间.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchDepartment($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals('departments/1', $path);
    }

    private function response()
    {
        $this->assertEquals('主任办公室', $this->department->getName());
        $this->assertEquals(Department::STATUS_NORMAL, $this->department->getStatus());
        $this->assertEquals(
            0,
            $this->department->getStatusTime()
        );
        $this->assertEquals(
            1597808772,
            $this->department->getCreateTime()
        );
        $this->assertEquals(
            1597808772,
            $this->department->getUpdateTime()
        );
    }
}
