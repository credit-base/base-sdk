<?php
namespace Base\Sdk\Enterprise;

trait ArrayDataTrait
{
    protected function getEnterpriseDetailData(int $id) : array
    {
        return [
            "meta"=> [],
            "data"=> [
                "type"=> "enterprises",
                "id"=> $id,
                "attributes"=> [
                    "name"=> "宏远园林工程有限公司",
                    "unifiedSocialCreditCode"=> "610527199403165500",
                    "establishmentDate"=> 19980323,
                    "approvalDate"=> 20180504,
                    "address"=> "黑牛城道298号",
                    "registrationCapital"=> "100",
                    "businessTermStart"=> 19980323,
                    "businessTermTo"=> 20180504,
                    "businessScope"=> "劳务服务(不含劳务派遣);房屋租赁。",
                    "registrationAuthority"=> "市场和质量监督管理局",
                    "principal"=> "王新华",
                    "principalCardId"=> "610526199703036719",
                    "registrationStatus"=> "存续（在营、开业、在册)",
                    "enterpriseTypeCode"=> "150",
                    "enterpriseType"=> "有限责任公司",
                    "industryCategory"=> "A",
                    "industryCode"=> "11",
                    "administrativeArea"=> 0,
                    "status"=> 0,
                    "createTime"=> 1516174523,
                    "updateTime"=> 1516174523,
                    "statusTime"=> 0
                ],
                "links"=> [
                    "self"=> "127.0.0.1:8089/enterprises/4"
                ]
            ]
        ];
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function getEnterpriseListData():array
    {
        return [
            "meta"=> [
                "count"=> 4,
                "links"=> [
                    "first"=> null,
                    "last"=> null,
                    "prev"=> null,
                    "next"=> null
                ]
            ],
            "links"=> [
                "first"=> null,
                "last"=> null,
                "prev"=> null,
                "next"=> null
            ],
            "data"=> [
                [
                    "type"=> "enterprises",
                    "id"=> "1",
                    "attributes"=> [
                        "name"=> "百瑞泰管业股份有限公司",
                        "unifiedSocialCreditCode"=> "21120101600530263M",
                        "establishmentDate"=> 20000110,
                        "approvalDate"=> 20000110,
                        "address"=> "南阳",
                        "registrationCapital"=> "100",
                        "businessTermStart"=> 20000110,
                        "businessTermTo"=> 20300110,
                        "businessScope"=> "装饰类",
                        "registrationAuthority"=> "发展和改革委员会",
                        "principal"=> "百泰",
                        "principalCardId"=> "610526199703036713",
                        "registrationStatus"=> "营业",
                        "enterpriseTypeCode"=> "3661",
                        "enterpriseType"=> "有限公司",
                        "industryCategory"=> "H",
                        "industryCode"=> "2374",
                        "administrativeArea"=> 0,
                        "status"=> 0,
                        "createTime"=> 1516174523,
                        "updateTime"=> 1516174523,
                        "statusTime"=> 0
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/enterprises/1"
                    ]
                ],
                [
                    "type"=> "enterprises",
                    "id"=> "2",
                    "attributes"=> [
                        "name"=> "德恒科技有限公司",
                        "unifiedSocialCreditCode"=> "31120101600530264M",
                        "establishmentDate"=> 20000101,
                        "approvalDate"=> 20000109,
                        "address"=> "南阳",
                        "registrationCapital"=> "18",
                        "businessTermStart"=> 20000109,
                        "businessTermTo"=> 20300109,
                        "businessScope"=> "科技类",
                        "registrationAuthority"=> "发展和改革委员会",
                        "principal"=> "赵楚",
                        "principalCardId"=> "111525198503245845",
                        "registrationStatus"=> "营业",
                        "enterpriseTypeCode"=> "3661",
                        "enterpriseType"=> "有限公司",
                        "industryCategory"=> "G",
                        "industryCode"=> "2373",
                        "administrativeArea"=> 0,
                        "status"=> 0,
                        "createTime"=> 1516174523,
                        "updateTime"=> 1516174523,
                        "statusTime"=> 0
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/enterprises/2"
                    ]
                ],
                [
                    "type"=> "enterprises",
                    "id"=> "3",
                    "attributes"=> [
                        "name"=> "青曲烟酒有限公司",
                        "unifiedSocialCreditCode"=> "323434323234545001",
                        "establishmentDate"=> 20200411,
                        "approvalDate"=> 20200411,
                        "address"=> "南阳",
                        "registrationCapital"=> "100",
                        "businessTermStart"=> 20200411,
                        "businessTermTo"=> 20500411,
                        "businessScope"=> "便利店",
                        "registrationAuthority"=> "发展和改革委员会",
                        "principal"=> "李而",
                        "principalCardId"=> "610526199603036713",
                        "registrationStatus"=> "登记状态",
                        "enterpriseTypeCode"=> "3661",
                        "enterpriseType"=> "有限公司",
                        "industryCategory"=> "A",
                        "industryCode"=> "11",
                        "administrativeArea"=> 0,
                        "status"=> 0,
                        "createTime"=> 1516174523,
                        "updateTime"=> 1516174523,
                        "statusTime"=> 0
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/enterprises/3"
                    ]
                ]
            ]
        ];
    }
}
