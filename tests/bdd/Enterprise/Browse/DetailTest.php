<?php
namespace Base\Sdk\Enterprise\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Enterprise\Model\Enterprise;
use Base\Sdk\Enterprise\Adapter\Enterprise\EnterpriseRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Enterprise\ArrayDataTrait;

/**
 * @Feature: 我是委办局工作人员,当我需要查看企业数据时,在信用信息查询应用系统下的企业管理中
 *           可以查看到所有的企业数据,通过列表和详情的形式查看我所有的企业, 以便于我可以精确的查看到不同的企业信息
 * @Scenario: 查看企业数据详情
 */

class DetailTest extends TestCase
{
    use ArrayDataTrait;

    private $enterprise;

    private $mock;

    public function setUp()
    {
        $this->enterprise = new Enterprise();
    }

    public function tearDown()
    {
        unset($this->enterprise);
    }

    /**
    * @Given: 存在一条企业数据
    */
    protected function prepareData()
    {
        $data = $this->getEnterpriseDetailData(1);

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条企业数据详情时
    */
    protected function fetchEnterprise($id)
    {

        $adapter = new EnterpriseRestfulAdapter();

        $this->enterprise = $adapter->fetchOne($id);
      
        return $this->enterprise;
    }

    /**
     * @Then  我可以看见该条企业数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchEnterprise($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'enterprises/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('宏远园林工程有限公司', $this->enterprise->getName());
        $this->assertEquals('610527199403165500', $this->enterprise->getUnifiedSocialCreditCode());
        $this->assertEquals('19980323', $this->enterprise->getEstablishmentDate());
        $this->assertEquals('20180504', $this->enterprise->getApprovalDate());
        
        $this->assertEquals(
            "黑牛城道298号",
            $this->enterprise->getAddress()
        );
        $this->assertEquals("100", $this->enterprise->getRegistrationCapital());
        $this->assertEquals(
            19980323,
            $this->enterprise->getBusinessTermStart()
        );
        $this->assertEquals(
            20180504,
            $this->enterprise->getBusinessTermTo()
        );
   
        $this->assertEquals(
            "劳务服务(不含劳务派遣);房屋租赁。",
            $this->enterprise->getBusinessScope()
        );
        $this->assertEquals(
            "市场和质量监督管理局",
            $this->enterprise->getRegistrationAuthority()
        );

        $this->assertEquals("王新华", $this->enterprise->getPrincipal());
        $this->assertEquals("610526199703036719", $this->enterprise->getPrincipalCardId());
        $this->assertEquals("存续（在营、开业、在册)", $this->enterprise->getRegistrationStatus());
        $this->assertEquals("150", $this->enterprise->getEnterpriseTypeCode());
        $this->assertEquals("有限责任公司", $this->enterprise->getEnterpriseType());
        $this->assertEquals("A", $this->enterprise->getIndustryCategory());
        $this->assertEquals("11", $this->enterprise->getIndustryCode());
        $this->assertEquals(0, $this->enterprise->getAdministrativeArea());
        $this->assertEquals(0, $this->enterprise->getStatus());

        $this->assertEquals(
            0,
            $this->enterprise->getStatusTime()
        );
        $this->assertEquals(
            1516174523,
            $this->enterprise->getCreateTime()
        );
        $this->assertEquals(
            1516174523,
            $this->enterprise->getUpdateTime()
        );
    }
}
