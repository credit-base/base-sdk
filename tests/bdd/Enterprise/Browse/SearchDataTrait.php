<?php
namespace Base\Sdk\Enterprise\Browse;

use Base\Sdk\Enterprise\Adapter\Enterprise\EnterpriseRestfulAdapter;

trait SearchDataTrait
{
    protected function getEnterpriseList(array $filter = [])
    {
        $adapter = new EnterpriseRestfulAdapter();

        list($count, $enterpriseList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
        
        return $enterpriseList;
    }
}
