<?php
namespace Base\Sdk\Enterprise\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Enterprise\Model\Enterprise;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Enterprise\ArrayDataTrait;

/**
 * @Feature: 我是委办局工作人员,当我需要查看企业数据时,在信用信息查询应用系统下的企业管理中
 *           可以查看到所有的企业数据,通过列表和详情的形式查看我所有的企业, 以便于我可以精确的查看到不同的企业信息
 * @Scenario: 查看企业列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ArrayDataTrait;

    private $enterprise;

    private $mock;

    public function setUp()
    {
        $this->enterprise = new Enterprise();
    }

    public function tearDown()
    {
        unset($this->enterprise);
    }

    /**
    * @Given: 存在企业数据
    */
    protected function prepareData()
    {
        $data = $this->getEnterpriseListData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看企业列表时
     */
    public function fetchEnterpriseList()
    {
        $filter = [];
        $list =$this->getEnterpriseList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见企业数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchEnterpriseList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'enterprises';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function response()
    {
        $enterpriseObject = $this->fetchEnterpriseList();

        $enterpriseArray = $this->getEnterpriseListData()['data'];
        
        foreach ($enterpriseObject as $item) {
            foreach ($enterpriseArray as $val) {
                if ($val['id'] == $item->getId()) {
                    $this->assertEquals($val['attributes']['name'], $item->getName());
                    $this->assertEquals(
                        $val['attributes']['unifiedSocialCreditCode'],
                        $item->getUnifiedSocialCreditCode()
                    );
                    $this->assertEquals(
                        $val['attributes']['establishmentDate'],
                        $item->getEstablishmentDate()
                    );
                    $this->assertEquals(
                        $val['attributes']['approvalDate'],
                        $item->getApprovalDate()
                    );
                    
                    $this->assertEquals(
                        $val['attributes']['address'],
                        $item->getAddress()
                    );
                    $this->assertEquals(
                        $val['attributes']['registrationCapital'],
                        $item->getRegistrationCapital()
                    );
                    $this->assertEquals(
                        $val['attributes']['businessTermStart'],
                        $item->getBusinessTermStart()
                    );
                    $this->assertEquals(
                        $val['attributes']['businessTermTo'],
                        $item->getBusinessTermTo()
                    );
            
                    $this->assertEquals(
                        $val['attributes']['businessScope'],
                        $item->getBusinessScope()
                    );
                    $this->assertEquals(
                        $val['attributes']['registrationAuthority'],
                        $item->getRegistrationAuthority()
                    );
    
                    $this->assertEquals($val['attributes']['principal'], $item->getPrincipal());
                    $this->assertEquals(
                        $val['attributes']['principalCardId'],
                        $item->getPrincipalCardId()
                    )
                        ;
                    $this->assertEquals(
                        $val['attributes']['registrationStatus'],
                        $item->getRegistrationStatus()
                    );
                    $this->assertEquals(
                        $val['attributes']['enterpriseTypeCode'],
                        $item->getEnterpriseTypeCode()
                    );
                    $this->assertEquals(
                        $val['attributes']['enterpriseType'],
                        $item->getEnterpriseType()
                    );
                    $this->assertEquals(
                        $val['attributes']['industryCategory'],
                        $item->getIndustryCategory()
                    );
                    $this->assertEquals(
                        $val['attributes']['industryCode'],
                        $item->getIndustryCode()
                    );
                    $this->assertEquals(
                        $val['attributes']['administrativeArea'],
                        $item->getAdministrativeArea()
                    );
                    $this->assertEquals(
                        $val['attributes']['status'],
                        $item->getStatus()
                    );
    
                    $this->assertEquals(
                        $val['attributes']['statusTime'],
                        $item->getStatusTime()
                    );
                    $this->assertEquals(
                        $val['attributes']['createTime'],
                        $item->getCreateTime()
                    );
                    $this->assertEquals(
                        $val['attributes']['updateTime'],
                        $item->getUpdateTime()
                    );
                }
            }
        }
    }
}
