<?php
namespace Base\Sdk\Enterprise\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Enterprise\Model\Enterprise;
use Base\Sdk\Enterprise\Adapter\Enterprise\EnterpriseRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是委办局工作人员,当我需要查看企业数据时,在信用信息查询应用系统下的企业管理中
 *           可以查看到所有的企业数据,通过列表和详情的形式查看我所有的企业, 以便于我可以精确的查看到不同的企业信息
 * @Scenario: 查看企业列表
 */

class FailEmptyTest extends TestCase
{
    private $enterprise;

    private $mock;

    public function setUp()
    {
        $this->enterprise = new Enterprise();
    }

    public function tearDown()
    {
        unset($this->enterprise);
    }

    /**
    * @Given: 不存在企业数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看企业列表时
     */
    public function fetchEnterpriseList()
    {
        $adapter = new EnterpriseRestfulAdapter();

        list($count, $this->enterprise) = $adapter
        ->search();
        unset($count);

        return $this->enterprise;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchEnterpriseList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'enterprises';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->enterprise);
    }
}
