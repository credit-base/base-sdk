<?php
namespace Base\Sdk\ResourceCatalog;

use Base\Sdk\ResourceCatalog\Adapter\BjSearchData\BjSearchDataRestfulAdapter;
use Base\Sdk\ResourceCatalog\Adapter\GbSearchData\GbSearchDataRestfulAdapter;
use Base\Sdk\ResourceCatalog\Adapter\WbjSearchData\WbjSearchDataRestfulAdapter;
use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Model\WbjSearchData;
use Base\Sdk\ResourceCatalog\Model\SearchData;

trait DetailDataTrait
{

    protected function fetchBjSearchData($id) : BjSearchData
    {
        $adapter = new BjSearchDataRestfulAdapter();

        $this->bjSearchData = $adapter->fetchOne($id);

        return $this->bjSearchData;
    }

    protected function fetchGbSearchData($id) : GbSearchData
    {
        $adapter = new GbSearchDataRestfulAdapter();

        $this->gbSearchData = $adapter->fetchOne($id);

        return $this->gbSearchData;
    }

    protected function fetchWbjSearchData($id) : WbjSearchData
    {
        $adapter = new WbjSearchDataRestfulAdapter();

        $this->wbjSearchData = $adapter->fetchOne($id);

        return $this->wbjSearchData;
    }

    /**
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    protected function getDetailData() : array
    {
        return [
            "meta" =>[],
            "data" => [
                "type" =>"gbSearchData",
                "id" =>"309",
                "attributes" => [
                    "infoClassify" =>1,
                    "infoCategory" =>1,
                    "subjectCategory" =>2,
                    "dimension" =>3,
                    "name" =>"市衡粤旅游发展有限公司",
                    "identify" =>"91410900MA46184E9Q",
                    "expirationDate" =>4102324600,
                    "description" =>"",
                    "status" =>0,
                    "frontEndProcessorStatus" =>1,
                    "createTime" =>1629200269,
                    "updateTime" =>1629200269,
                    "statusTime" =>0
                ],
                "relationships" => [
                    "crew" => [
                        "data" => [
                            "type" =>"crews",
                            "id" =>"2"
                        ]
                    ],
                    "sourceUnit" => [
                        "data" => [
                            "type" =>"userGroups",
                            "id" =>"2"
                        ]
                    ],
                    "itemsData" => [
                        "data" => [
                            "type" =>"bjItemsData",
                            "id" =>"309"
                        ]
                    ],
                    "template" => [
                        "data" => [
                            "type" =>"bjTemplates",
                            "id" =>"52"
                        ]
                    ]
                ],
                "links" => [
                    "self" =>"api.base.qixinyun.com/gbSearchData/309"
                ]
            ],
            "included" =>[
                [
                    "type" =>"crews",
                    "id" =>"2",
                    "attributes" => [
                        "realName" =>"张科",
                        "cardId" =>"610428199609205018",
                        "userName" =>"18800000001",
                        "cellphone" =>"18800000001",
                        "category" =>1,
                        "purview" =>[
                            9
                        ],
                        "status" =>0,
                        "createTime" =>1597622740,
                        "updateTime" =>1620425256,
                        "statusTime" =>1620425256
                    ],
                    "relationships" => [
                        "userGroup" => [
                            "data" => [
                                "type" =>"userGroups",
                                "id" =>"1"
                            ]
                        ],
                        "department" => [
                            "data" => [
                                "type" =>"departments",
                                "id" =>"1"
                            ]
                        ]
                    ]
                ],
                [
                    "type" =>"userGroups",
                    "id" =>"2",
                    "attributes" => [
                        "name" =>"发展和改革委员会",
                        "shortName" =>"发改委",
                        "status" =>0,
                        "createTime" =>1558325495,
                        "updateTime" =>1610532122,
                        "statusTime" =>0
                    ]
                ],
                [
                    "type" =>"bjItemsData",
                    "id" =>"309",
                    "attributes" => [
                        "data" => [
                            "ZTMC" =>"市衡粤旅游发展有限公司",
                            "TYSHXYDM" =>"91410900MA26184E9Q",
                            "FDDBR" =>"张晓苹",
                            "XKRQ" =>"20210219",
                            "XKJDWSH" =>"豫高速公路许 字第222号",
                            "ZTLB" =>"法人及非法人组织",
                            "GKFW" =>"社会公开",
                            "GXPL" =>"每2年",
                            "XXFL" =>"行政许可",
                            "XXLB" =>"基础信息"
                        ]
                    ]
                ],
                [
                    "type" =>"bjTemplates",
                    "id" =>"52",
                    "attributes" => [
                        "name" =>"演示本级资源目录",
                        "identify" =>"YSBJZYML",
                        "subjectCategory" =>[
                            "2"
                        ],
                        "dimension" =>1,
                        "exchangeFrequency" =>7,
                        "infoClassify" =>"1",
                        "infoCategory" =>"1",
                        "description" =>"",
                        "category" =>2,
                        "items" =>[
                            [
                                "name" =>"信息类别",
                                "type" =>"5",
                                "length" =>"52",
                                "options" =>[
                                    "基础信息"
                                ],
                                "remarks" =>"信息性质类型，支持单选",
                                "identify" =>"XXLB",
                                "isMasked" =>"1",
                                "maskRule" =>[],
                                "dimension" =>"2",
                                "isNecessary" =>"0"
                            ]
                        ],
                        "status" =>0,
                        "createTime" =>1629298839,
                        "updateTime" =>1629234567,
                        "statusTime" =>0
                    ],
                    "relationships" => [
                        "sourceUnit" => [
                            "data" => [
                                "type" =>"userGroups",
                                "id" =>"1"
                            ]
                        ],
                        "gbTemplate" => [
                            "data" => [
                                "type" =>"gbTemplates",
                                "id" =>"68"
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    //启用禁用
    protected function getSearchDataStatusResponse($status = SearchData::DATA_STATUS['ENABLED']):array
    {
        $data = $this->getDetailData();
        $data['data']['attributes']['status'] = $status;
        $data['data']['attributes']['createTime'] = 0;
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;

        return $data;
    }
}
