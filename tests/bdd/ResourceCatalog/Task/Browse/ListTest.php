<?php
namespace Base\Sdk\ResourceCatalog\Task\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Model\Task\Task;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature:  我是委办局工作人员,当我需要查看我的任务数据时,在资源目录数据子系统下的委办局数据中,可以查看到我的所有任务数据,
 * 通过列表形式查看我所有的任务数据,以便于我可以更快的了解资源目录数据的导入情况
 * @Scenario: 查看任务列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait;

    private $task;

    private $mock;

    public function setUp()
    {
        $this->task = new Task();
    }

    public function tearDown()
    {
        unset($this->task);
    }

    /**
    * @Given: 存在任务数据
    */
    protected function prepareData()
    {
        $data = $this->getTaskListArrayData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看任务列表时
     */
    public function fetchTaskList()
    {
        $filter = [];
        $list =$this->getTaskList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见任务数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchTaskList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'tasks';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $tasksList = $this->fetchTaskList();

        $tasksArray = $this->getTaskListArrayData()['data'];
        
        foreach ($tasksList as $object) {
            foreach ($tasksArray as $item) {
                if ($object->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['total'], $object->getTotal());
                    $this->assertEquals($item['attributes']['successNumber'], $object->getSuccessNumber());

                    $this->assertEquals($item['attributes']['failureNumber'], $object->getFailureNumber());
                    $this->assertEquals(
                        $item['attributes']['sourceCategory'],
                        $object->getSourceCategory()
                    );
            
                    $this->assertEquals(
                        $item['attributes']['sourceTemplate'],
                        $object->getSourceTemplate()
                    );
                    $this->assertEquals(
                        $item['attributes']['targetCategory'],
                        $object->getTargetCategory()
                    );
                    $this->assertEquals(
                        $item['attributes']['targetTemplate'],
                        $object->getTargetTemplate()
                    );
                    $this->assertEquals(
                        $item['attributes']['targetRule'],
                        $object->getTargetRule()
                    );
    
                    $this->assertEquals(
                        $item['attributes']['fileName'],
                        $object->getFileName()
                    );
                    $this->assertEquals(
                        $item['attributes']['status'],
                        $object->getStatus()
                    );
        
                    $this->assertEquals(
                        $item['attributes']['updateTime'],
                        $object->getUpdateTime()
                    );
                }
            }
        }
    }
}
