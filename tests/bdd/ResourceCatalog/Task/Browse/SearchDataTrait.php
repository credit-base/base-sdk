<?php
namespace Base\Sdk\ResourceCatalog\Task\Browse;

use Base\Sdk\ResourceCatalog\Adapter\Task\TaskRestfulAdapter;

trait SearchDataTrait
{
    protected function getTaskList(array $filter = [])
    {
        $adapter = new TaskRestfulAdapter();

        list($count, $taskList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
        
        return $taskList;
    }

    /**
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    protected function getTaskListArrayData():array
    {
        return [
            "meta"=> [
                "count"=> 3,
                "links"=> [
                    "first"=> null,
                    "last"=> null,
                    "prev"=> null,
                    "next"=> null
                ]
            ],
            "links"=> [
                "first"=> null,
                "last"=> null,
                "prev"=> null,
                "next"=> null
            ],
            "data"=> [
                [
                    "type"=> "tasks",
                    "id"=> "1",
                    "attributes"=> [
                        "pid"=> 0,
                        "total"=> 4,
                        "successNumber"=> 3,
                        "failureNumber"=> 1,
                        "sourceCategory"=> 10,
                        "sourceTemplate"=> 1,
                        "targetCategory"=> 10,
                        "targetTemplate"=> 1,
                        "targetRule"=> 1,
                        "scheduleTask"=> 3416,
                        "errorNumber"=> 0,
                        "fileName"=> "FAILURE_1_10_HZXKXX_1_10_1_234551212.xlsx",
                        "status"=> -3,
                        "createTime"=> 1628066697,
                        "updateTime"=> 1628066697,
                        "statusTime"=> 1628066699
                    ],
                    "relationships"=> [
                        "crew"=> [
                            "data"=> [
                                "type"=> "crews",
                                "id"=> "1"
                            ]
                        ],
                        "userGroup"=> [
                            "data"=> [
                                "type"=> "userGroups",
                                "id"=> "1"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/tasks/1"
                    ]
                ],
                [
                    "type"=> "tasks",
                    "id"=> "2",
                    "attributes"=> [
                        "pid"=> 1,
                        "total"=> 3,
                        "successNumber"=> 2,
                        "failureNumber"=> 1,
                        "sourceCategory"=> 10,
                        "sourceTemplate"=> 1,
                        "targetCategory"=> 2,
                        "targetTemplate"=> 1,
                        "targetRule"=> 4,
                        "scheduleTask"=> 3418,
                        "errorNumber"=> 0,
                        "fileName"=> "FAILURE_1_2_HZXKXX_1_10_1_234551212.xlsx",
                        "status"=> -3,
                        "createTime"=> 1628066699,
                        "updateTime"=> 1628066699,
                        "statusTime"=> 1628066701
                    ],
                    "relationships"=> [
                        "crew"=> [
                            "data"=> [
                                "type"=> "crews",
                                "id"=> "1"
                            ]
                        ],
                        "userGroup"=> [
                            "data"=> [
                                "type"=> "userGroups",
                                "id"=> "1"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/tasks/2"
                    ]
                ],
                [
                    "type"=> "tasks",
                    "id"=> "3",
                    "attributes"=> [
                        "pid"=> 1,
                        "total"=> 3,
                        "successNumber"=> 2,
                        "failureNumber"=> 1,
                        "sourceCategory"=> 10,
                        "sourceTemplate"=> 1,
                        "targetCategory"=> 1,
                        "targetTemplate"=> 5,
                        "targetRule"=> 6,
                        "scheduleTask"=> 3419,
                        "errorNumber"=> 0,
                        "fileName"=> "FAILURE_5_1_HZXKXX_1_10_1_234551212.xlsx",
                        "status"=> -3,
                        "createTime"=> 1628066699,
                        "updateTime"=> 1628066699,
                        "statusTime"=> 1628066701
                    ],
                    "relationships"=> [
                        "crew"=> [
                            "data"=> [
                                "type"=> "crews",
                                "id"=> "1"
                            ]
                        ],
                        "userGroup"=> [
                            "data"=> [
                                "type"=> "userGroups",
                                "id"=> "1"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/tasks/3"
                    ]
                ]
            ],
            "included"=> [
                [
                    "type"=> "crews",
                    "id"=> "1",
                    "attributes"=> [
                        "realName"=> "张科",
                        "cardId"=> "412825199009094532",
                        "userName"=> "18800000000",
                        "cellphone"=> "18800000000",
                        "category"=> 1,
                        "purview"=> [
                            "1",
                            "2",
                            "3"
                        ],
                        "status"=> 0,
                        "createTime"=> 1618284031,
                        "updateTime"=> 1619578455,
                        "statusTime"=> 0
                    ],
                    "relationships"=> [
                        "userGroup"=> [
                            "data"=> [
                                "type"=> "userGroups",
                                "id"=> "1"
                            ]
                        ],
                        "department"=> [
                            "data"=> [
                                "type"=> "departments",
                                "id"=> "3"
                            ]
                        ]
                    ]
                ],
                [
                    "type"=> "userGroups",
                    "id"=> "1",
                    "attributes"=> [
                        "name"=> "萍乡市发展和改革委员会",
                        "shortName"=> "发改委",
                        "status"=> 0,
                        "createTime"=> 1516168970,
                        "updateTime"=> 1516168970,
                        "statusTime"=> 0
                    ]
                ]
            ]
        ];
    }
}
