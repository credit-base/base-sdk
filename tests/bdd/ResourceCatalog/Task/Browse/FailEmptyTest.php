<?php
namespace Base\Sdk\ResourceCatalog\Task\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Model\Task\Task;
use Base\Sdk\ResourceCatalog\Adapter\Task\TaskRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的任务数据时,
 *           在资源目录数据子系统下的委办局数据中,可以查看到我的所有任务数据,
 *           通过列表形式查看我所有的任务数据,以便于我可以更快的了解资源目录数据的导入情况
 * @Scenario: 查看任务数据列表-异常不存在数据
 */

class FailEmptyTest extends TestCase
{
    private $task;

    private $mock;

    public function setUp()
    {
        $this->task = new Task();
    }

    public function tearDown()
    {
        unset($this->task);
    }

    /**
    * @Given: 不存在任务数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看任务列表时
     */
    public function fetchTaskList()
    {
        $adapter = new TaskRestfulAdapter();

        list($count, $this->task) = $adapter
        ->search();
        unset($count);

        return $this->task;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchTaskList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'tasks';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->task);
    }
}
