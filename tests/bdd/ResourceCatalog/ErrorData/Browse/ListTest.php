<?php
namespace Base\Sdk\ResourceCatalog\ErrorData\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Model\ErrorData;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature:  我是委办局工作人员,当我需要了解我数据上传数据的原因时,在资源目录数据子系统下的上传任务列表中,点击失败数据可以查看到我的所有上传失败的数据,
 *           通过列表形式呈现,以便于我可以更快的修复错误数据并重新上传
 * @Scenario: 查看失败资源目录数据列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait;

    private $errorData;

    private $mock;

    public function setUp()
    {
        $this->errorData = new ErrorData();
    }

    public function tearDown()
    {
        unset($this->errorData);
    }

    /**
    * @Given: 存在失败资源目录数据数据
    */
    protected function prepareData()
    {
        $data = $this->getErrorDataListArrayData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看失败资源目录数据列表时
     */
    public function fetchErrorDataList()
    {
        $filter = [];
        $list =$this->getErrorDataList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见失败资源目录数据数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchErrorDataList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'errorDatas';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $tasksList = $this->fetchErrorDataList();

        $tasksArray = $this->getErrorDataListArrayData()['data'];
        
        foreach ($tasksList as $object) {
            foreach ($tasksArray as $item) {
                if ($object->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['category'], $object->getCategory());
                    $this->assertEquals($item['attributes']['itemsData'], $object->getItemsData());

                    $this->assertEquals($item['attributes']['errorType'], $object->getErrorType());
                    $this->assertEquals(
                        $item['attributes']['errorReason'],
                        $object->getErrorReason()
                    );
                    $this->assertEquals(
                        $item['attributes']['status'],
                        $object->getStatus()
                    );
        
                    $this->assertEquals(
                        $item['attributes']['updateTime'],
                        $object->getUpdateTime()
                    );
                }
            }
        }
    }
}
