<?php
namespace Base\Sdk\ResourceCatalog\ErrorData\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Model\ErrorData;
use Base\Sdk\ResourceCatalog\Adapter\ErrorData\ErrorDataRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是委办局工作人员,当我需要了解我数据上传数据的原因时,在资源目录数据子系统下的上传任务列表中,点击失败数据可以查看到我的所有上传失败的数据,
 *           通过列表形式呈现,以便于我可以更快的修复错误数据并重新上传
 * @Scenario: 查看失败资源目录数据详情
 */

class DetailTest extends TestCase
{
    use SearchDataTrait;

    private $errorData;

    private $mock;

    public function setUp()
    {
        $this->errorData = new ErrorData();
    }

    public function tearDown()
    {
        unset($this->errorData);
    }

    /**
    * @Given: 存在一条失败资源目录数据
    */
    protected function prepareData()
    {
        $data = $this->getErrorDataDetailArrayData();

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条失败资源目录数据详情时
    */
    protected function fetchErrorData($id)
    {

        $adapter = new ErrorDataRestfulAdapter();

        $this->errorData = $adapter->fetchOne($id);
      
        return $this->errorData;
    }

    /**
     * @Then  我可以看见该条失败资源目录数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchErrorData($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'errorDatas/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            10,
            $this->errorData->getCategory()
        );
        $this->assertEquals(
            $this->getCommonItemsData(),
            $this->errorData->getItemsData()
        );
        $this->assertEquals(
            $this->getCommonErrorReason(),
            $this->errorData->getErrorReason()
        );
        $this->assertEquals(
            8,
            $this->errorData->getErrorType()
        );

        $this->assertEquals(
            1629871256,
            $this->errorData->getCreateTime()
        );
        $this->assertEquals(
            1629871256,
            $this->errorData->getUpdateTime()
        );
    }
}
