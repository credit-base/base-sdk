<?php
namespace Base\Sdk\ResourceCatalog\ErrorData\Browse;

use Base\Sdk\ResourceCatalog\Adapter\ErrorData\ErrorDataRestfulAdapter;

trait SearchDataTrait
{
    protected function getErrorDataList(array $filter = [])
    {
        $adapter = new ErrorDataRestfulAdapter();

        list($count, $taskList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
        
        return $taskList;
    }

    protected function getErrorDataListArrayData():array
    {
        return [
            "meta"=> [
                "count"=> 2,
                "links"=> [
                    "first"=> null,
                    "last"=> null,
                    "prev"=> null,
                    "next"=> null
                ]
            ],
            "links"=> [
                "first"=> null,
                "last"=> null,
                "prev"=> null,
                "next"=> null
            ],
            "data"=> [
                [
                    "type"=> "errorDatas",
                    "id"=> "1",
                    "attributes"=> [
                        "category"=> 10,
                        "itemsData"=> $this->getCommonItemsData(),
                        "errorType"=> 8,
                        "errorReason"=> $this->getCommonErrorReason(),
                        "status"=> 0,
                        "createTime"=> 1628066697,
                        "updateTime"=> 1628066697,
                        "statusTime"=> 1628066699
                    ],
                    "relationships"=> [
                        "crew"=> [
                            "data"=> [
                                "type"=> "tasks",
                                "id"=> "1"
                            ]
                        ],
                        "userGroup"=> [
                            "data"=> [
                                "type"=> "templates",
                                "id"=> "1"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/errorDatas/1"
                    ]
                ],
                [
                    "type"=> "errorDatas",
                    "id"=> "2",
                    "attributes"=> [
                        "category"=> 10,
                        "itemsData"=> $this->getCommonItemsData(),
                        "errorType"=> 8,
                        "errorReason"=> $this->getCommonErrorReason(),
                        "status"=> 0,
                        "createTime"=> 1628066699,
                        "updateTime"=> 1628066699,
                        "statusTime"=> 1628066701
                    ],
                    "relationships"=> [
                        "crew"=> [
                            "data"=> [
                                "type"=> "tasks",
                                "id"=> "1"
                            ]
                        ],
                        "userGroup"=> [
                            "data"=> [
                                "type"=> "templates",
                                "id"=> "1"
                            ]
                        ]
                    ],
                    "links"=> [
                        "self"=> "127.0.0.1:8089/errorDatas/2"
                    ]
                ]
            ],
            "included"=> $this->getCommonIncluded()
        ];
    }

    protected function getErrorDataDetailArrayData():array
    {
        return [
            "meta"=> [],
            "data"=> [
                "type"=> "errorDatas",
                "id"=> "1",
                "attributes"=> [
                    "category"=> 10,
                    "itemsData"=> $this->getCommonItemsData(),
                    "errorType"=> 8,
                    "errorReason"=> $this->getCommonErrorReason(),
                    "status"=> 0,
                    "createTime"=> 1629871256,
                    "updateTime"=> 1629871256,
                    "statusTime"=> 0
                ],
                "relationships"=> [
                    "task"=> [
                        "data"=> [
                            "type"=> "tasks",
                            "id"=> "1"
                        ]
                    ],
                    "template"=> [
                        "data"=> [
                            "type"=> "templates",
                            "id"=> "1"
                        ]
                    ]
                ],
                "links"=> [
                    "self"=> "127.0.0.1:8089/errorDatas/1"
                ]
            ],
            "included"=>$this->getCommonIncluded()
        ];
    }

    protected function getCommonItemsData():array
    {
        return [
            "ZTMC"=> "四川瑞煜建筑有限公司信阳分公司",
            "XK_XDR_LB"=> "法人及非法人组织",
            "TYSHXYDM"=> "91411500MA9F86JG96",
            "XK_XDR_GSZC"=> "",
            "XK_XDR_ZZJG"=> "91292",
            "XK_XDR_SWDJ"=> "",
            "XK_XDR_SYDW"=> "",
            "XK_XDR_SHZZ"=> "MA9F86JG-9",
            "XK_FRDB"=> "王灿国",
            "XK_FR_ZJLX"=> "",
            "XK_FR_ZJHM"=> "",
            "XK_XDR_ZJLX"=> "",
            "XK_XDR_ZJHM"=> "",
            "XK_XKWS"=> "企业注册登记",
            "XK_WSH"=> "411596000077514",
            "XK_XKLB"=> "登记",
            "XK_XKZS"=> "企业注册登记",
            "XK_XKBH"=> "410000017210705A000092",
            "XK_NR"=> "企业注册登记",
            "XK_JDRQ"=> "20210715",
            "XK_YXQZ"=> "2020-06-04",
            "XK_YXQZI"=> "73050",
            "XK_XKJG"=> "信阳市市场监督管理局专业分局",
            "XK_XKJGDM"=> "11411500747429737F",
            "XK_ZT"=> "1",
            "XK_LYDW"=> "信阳市发展和改革委员会1",
            "XK_LYDWDM"=> "11411500006067580U",
            "BZ"=> "河南省高速公路联网管理中心",
            "GQSJ"=> "20210816",
            "ZTLB"=> "法人及非法人组织",
            "GKFW"=> "社会公开",
            "GXPL"=> "实时",
            "XXFL"=> "行政许可",
            "XXLB"=> "基础信息"
        ];
    }

    protected function getCommonErrorReason():array
    {
        return [
            "XK_YXQZ"=> [
                "8"
            ],
            "XK_YXQZI"=> [
                "8"
            ],
            "XK_FR_ZJHM"=> [
                "8"
            ],
            "XK_FR_ZJLX"=> [
                "8"
            ],
            "XK_XDR_GSZC"=> [
                "8"
            ],
            "XK_XDR_SWDJ"=> [
                "8"
            ],
            "XK_XDR_SYDW"=> [
                "8"
            ],
            "XK_XDR_ZJHM"=> [
                "8"
            ],
            "XK_XDR_ZJLX"=> [
                "8"
            ]
        ];
    }

    /**
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    protected function getCommonIncluded():array
    {
        return [
            [
                "type"=> "tasks",
                "id"=> "1",
                "attributes"=> [
                    "pid"=> 0,
                    "total"=> 5,
                    "successNumber"=> 3,
                    "failureNumber"=> 2,
                    "sourceCategory"=> 10,
                    "sourceTemplate"=> 1,
                    "targetCategory"=> 10,
                    "targetTemplate"=> 1,
                    "targetRule"=> 1,
                    "scheduleErrorData"=> 1920,
                    "errorNumber"=> 0,
                    "fileName"=> "FAILURE_10_1_10_HZXKXX_1_10_1_2323fff.xlsx",
                    "status"=> -3,
                    "createTime"=> 1629871256,
                    "updateTime"=> 1629871256,
                    "statusTime"=> 1629871273
                ],
                "relationships"=> [
                    "crew"=> [
                        "data"=> [
                            "type"=> "crews",
                            "id"=> "1"
                        ]
                    ],
                    "userGroup"=> [
                        "data"=> [
                            "type"=> "userGroups",
                            "id"=> "1"
                        ]
                    ]
                ]
            ],
            [
                "type"=> "templates",
                "id"=> "1",
                "attributes"=> [
                    "name"=> "行政许可信息",
                    "identify"=> "HZXKXX",
                    "subjectCategory"=> [
                        "1",
                        "3"
                    ],
                    "dimension"=> 1,
                    "exchangeFrequency"=> 1,
                    "infoClassify"=> "1",
                    "infoCategory"=> "1",
                    "description"=> "行政许可信息",
                    "category"=> 10,
                    "items"=> [
                        [
                            "name"=> "行政相对人名称",
                            "type"=> "1",
                            "length"=> "200",
                            "options"=> [],
                            "remarks"=> "信用主体的法定名称",
                            "identify"=> "ZTMC",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "行政相对人类别",
                            "type"=> "1",
                            "length"=> "16",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XDR_LB",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "行政相对人代码_1(统一社会信用代码)",
                            "type"=> "1",
                            "length"=> "18",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "TYSHXYDM",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "行政相对人代码_2 (工商注册号)",
                            "type"=> "1",
                            "length"=> "50",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XDR_GSZC",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "行政相对人代码_3(组织机构代码)",
                            "type"=> "1",
                            "length"=> "9",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XDR_ZZJG",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "行政相对人代码_4(税务登记号)",
                            "type"=> "1",
                            "length"=> "15",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XDR_SWDJ",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "行政相对人代码_5(事业单位证书号)",
                            "type"=> "1",
                            "length"=> "12",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XDR_SYDW",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "行政相对人代码_6(社会组织登记证号)",
                            "type"=> "1",
                            "length"=> "50",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XDR_SHZZ",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "法定代表人",
                            "type"=> "1",
                            "length"=> "50",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_FRDB",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "法定代表人证件类型",
                            "type"=> "1",
                            "length"=> "64",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_FR_ZJLX",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "法定代表人证件号码",
                            "type"=> "1",
                            "length"=> "64",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_FR_ZJHM",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "证件类型",
                            "type"=> "1",
                            "length"=> "64",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XDR_ZJLX",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "证件号码",
                            "type"=> "1",
                            "length"=> "64",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XDR_ZJHM",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "行政许可决定文书名称",
                            "type"=> "1",
                            "length"=> "64",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XKWS",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "行政许可决定文书号",
                            "type"=> "1",
                            "length"=> "64",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_WSH",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "许可类别",
                            "type"=> "1",
                            "length"=> "256",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XKLB",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "许可证书名称",
                            "type"=> "1",
                            "length"=> "64",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XKZS",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "许可编号",
                            "type"=> "1",
                            "length"=> "64",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XKBH",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "许可内容",
                            "type"=> "1",
                            "length"=> "4000",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_NR",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "许可决定日期",
                            "type"=> "2",
                            "length"=> "8",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_JDRQ",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "有效期自",
                            "type"=> "2",
                            "length"=> "8",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_YXQZ",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "有效期至",
                            "type"=> "2",
                            "length"=> "8",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_YXQZI",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "许可机关",
                            "type"=> "1",
                            "length"=> "200",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XKJG",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "许可机关统一社会信用代码",
                            "type"=> "1",
                            "length"=> "18",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_XKJGDM",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "当前状态",
                            "type"=> "3",
                            "length"=> "1",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_ZT",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "数据来源单位",
                            "type"=> "1",
                            "length"=> "200",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_LYDW",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "数据来源单位统一社会信用代码",
                            "type"=> "1",
                            "length"=> "18",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "XK_LYDWDM",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "备注",
                            "type"=> "1",
                            "length"=> "4000",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "BZ",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "过期时间",
                            "type"=> "2",
                            "length"=> "8",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "GQSJ",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "信息分类",
                            "type"=> "5",
                            "length"=> "50",
                            "options"=> [
                                "行政许可"
                            ],
                            "remarks"=> "支持单选",
                            "identify"=> "XXFL",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "2",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "信息类别",
                            "type"=> "5",
                            "length"=> "50",
                            "options"=> [
                                "基础信息"
                            ],
                            "remarks"=> "信息性质类型，支持单选",
                            "identify"=> "XXLB",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "2",
                            "isNecessary"=> "1"
                        ]
                    ],
                    "status"=> 0,
                    "createTime"=> 1626504022,
                    "updateTime"=> 1629172736,
                    "statusTime"=> 0
                ],
                "relationships"=> [
                    "sourceUnit"=> [
                        "data"=> [
                            "type"=> "userGroups",
                            "id"=> "1"
                        ]
                    ]
                ]
            ]
        ];
    }
}
