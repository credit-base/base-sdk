<?php
namespace Base\Sdk\ResourceCatalog\ErrorData\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\ResourceCatalog\Model\ErrorData;
use Base\Sdk\ResourceCatalog\Adapter\ErrorData\ErrorDataRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是委办局工作人员,当我需要了解我数据上传数据的原因时,在资源目录数据子系统下的上传任务列表中,点击失败数据可以查看到我的所有上传失败的数据,
 *           通过列表形式呈现,以便于我可以更快的修复错误数据并重新上传
 * @Scenario: 查看错误数据列表-异常不存在数据
 */

class FailEmptyTest extends TestCase
{
    private $errorData;

    private $mock;

    public function setUp()
    {
        $this->errorData = new ErrorData();
    }

    public function tearDown()
    {
        unset($this->errorData);
    }

    /**
    * @Given: 不存在错误数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看错误数据列表时
     */
    public function fetchErrorDataList()
    {
        $adapter = new ErrorDataRestfulAdapter();

        list($count, $this->errorData) = $adapter
        ->search();
        unset($count);

        return $this->errorData;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchErrorDataList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'errorDatas';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->errorData);
    }
}
