<?php


namespace Base\Sdk\ResourceCatalog\BjSearchData\Update;

use Base\Sdk\ResourceCatalog\DetailDataTrait;
use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use Base\Sdk\ResourceCatalog\Model\SearchData;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ConfirmTest extends TestCase
{
    use DetailDataTrait;

    private $bjSearchData;

    private $mock;

    public function setUp()
    {
        $this->bjSearchData = new BjSearchData();
    }

    public function tearDown()
    {
        unset($this->bjSearchData);
    }

    protected function prepareData()
    {
        $enableData = $this->getSearchDataStatusResponse(SearchData::DATA_STATUS['CONFIRM']);
        $jsonData = json_encode($enableData);

        $disableData = $this->getSearchDataStatusResponse(SearchData::DATA_STATUS['ENABLED']);

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], json_encode($disableData))
            ]
        );

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    protected function confirm()
    {
        return $this->bjSearchData->confirm();
    }

    public function testValidate()
    {
        $id = 309;
        $this->prepareData();

        $this->fetchBjSearchData($id);
        $this->confirm();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();

        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode(array()), $contents);
        $this->assertEquals('/bjSearchData/309/confirm', $path);
    }

    private function response()
    {
        $this->assertEquals(SearchData::DATA_STATUS['ENABLED'], $this->bjSearchData->getStatus());
    }
}
