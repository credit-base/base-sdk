<?php
namespace Base\Sdk\ResourceCatalog\BjSearchData\Browse;

use Base\Sdk\ResourceCatalog\ListDataTrait;
use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ListTest extends TestCase
{
    use ListDataTrait;

    private $userGroup;

    private $mock;

    public function setUp()
    {
        $this->userGroup = new BjSearchData();
    }

    public function tearDown()
    {
        unset($this->userGroup);
    }

    protected function prepareData()
    {
        $data = $this->getListData();

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看委办局列表时
     */
    public function fetchUserGroupList() : array
    {
        $filter = [];
        return $this->getBjSearchDataList($filter);
    }

    /**
     * @Then  我可以看见企业数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUserGroupList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('bjSearchData', $request->getUri()->getPath());
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function response()
    {
        $bjSearchDataObject = $this->fetchUserGroupList();
        $bjSearchDataArray = $this->getListData()['data'];

        foreach ($bjSearchDataObject as $item) {
            foreach ($bjSearchDataArray as $val) {
                if ($val['id'] == $item->getId()) {
                    $this->assertEquals($val['attributes']['name'], $item->getName());
                    $this->assertEquals($val['attributes']['updateTime'], $item->getUpdateTime());
                }
            }
        }
    }
}
