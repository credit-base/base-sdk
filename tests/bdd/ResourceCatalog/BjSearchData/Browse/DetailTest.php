<?php
namespace Base\Sdk\ResourceCatalog\BjSearchData\Browse;

use Base\Sdk\ResourceCatalog\Adapter\BjSearchData\BjSearchDataRestfulAdapter;
use Base\Sdk\ResourceCatalog\DetailDataTrait;
use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DetailTest extends TestCase
{
    use DetailDataTrait;
    
    private $bjSearchData;

    private $mock;

    public function setUp()
    {
        $this->bjSearchData = new BjSearchData();
    }

    public function tearDown()
    {
        unset($this->bjSearchData);
    }

    protected function prepareData()
    {
        $data = $this->getDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
                new Response(
                    200,
                    ['Content-Type' => 'application/vnd.api+json'],
                    $jsonData
                ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    public function testValidate()
    {
        $id = 309;
        $this->prepareData();
        $this->fetchBjSearchData($id);

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $contents);
        $this->assertEquals('bjSearchData/309', $path);
    }

    private function response()
    {
        $this->assertEquals('市衡粤旅游发展有限公司', $this->bjSearchData->getName());
        $this->assertEquals(BjSearchData::STATUS['ENABLED'], $this->bjSearchData->getStatus());
        $this->assertEquals(
            0,
            $this->bjSearchData->getStatusTime()
        );
        $this->assertEquals(
            1629200269,
            $this->bjSearchData->getCreateTime()
        );
        $this->assertEquals(
            1629200269,
            $this->bjSearchData->getUpdateTime()
        );
    }
}
