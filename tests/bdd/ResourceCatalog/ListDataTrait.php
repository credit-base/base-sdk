<?php
namespace Base\Sdk\ResourceCatalog;

use Base\Sdk\ResourceCatalog\Adapter\GbSearchData\GbSearchDataRestfulAdapter;
use Base\Sdk\ResourceCatalog\Adapter\BjSearchData\BjSearchDataRestfulAdapter;
use Base\Sdk\ResourceCatalog\Adapter\WbjSearchData\WbjSearchDataRestfulAdapter;

trait ListDataTrait
{
    protected function getGbSearchDataList($filter = []) : array
    {
        $adapter = new GbSearchDataRestfulAdapter();

        list($count, $gbSearchDataList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $gbSearchDataList;
    }

    protected function getBjSearchDataList($filter = []) : array
    {
        $adapter = new BjSearchDataRestfulAdapter();

        list($count, $bjSearchDataList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $bjSearchDataList;
    }

    protected function getWbjSearchDataList($filter = []) : array
    {
        $adapter = new WbjSearchDataRestfulAdapter();

        list($count, $wbjSearchDataList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $wbjSearchDataList;
    }

    /**
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    protected function getListData() : array
    {
        return [
            "meta" => [
                "count" =>44,
                "links" => [
                    "first" =>1,
                    "last" =>44,
                    "prev" =>null,
                    "next" =>2
                ]
            ],
            "links" => [
                "first" =>"api.base.qixinyun.com/gbSearchData/?include=crew,sourceUnit,itemsData,template&sort=-id&page[number]=1&page[size]=1",//phpcs:ignore
                "last" =>"api.base.qixinyun.com/gbSearchData/?include=crew,sourceUnit,itemsData,template&sort=-id&page[number]=44&page[size]=1",//phpcs:ignore
                "prev" =>null,
                "next" =>"api.base.qixinyun.com/gbSearchData/?include=crew,sourceUnit,itemsData,template&sort=-id&page[number]=2&page[size]=1"//phpcs:ignore
            ],
            "data" =>[
                [
                    "type" =>"gbSearchData",
                    "id" =>"176",
                    "attributes" => [
                        "infoClassify" =>1,
                        "infoCategory" =>1,
                        "subjectCategory" =>1,
                        "dimension" =>1,
                        "name" =>"市衡粤旅游发展有限公司",
                        "identify" =>"91410900MA46184E9Q",
                        "expirationDate" =>4102329600,
                        "description" =>"",
                        "status" =>0,
                        "frontEndProcessorStatus" =>0,
                        "createTime" =>1630650192,
                        "updateTime" =>1630650192,
                        "statusTime" =>0
                    ],
                    "relationships" => [
                        "crew" => [
                            "data" => [
                                "type" =>"crews",
                                "id" =>"1"
                            ]
                        ],
                        "sourceUnit" => [
                            "data" => [
                                "type" =>"userGroups",
                                "id" =>"1"
                            ]
                        ],
                        "itemsData" => [
                            "data" => [
                                "type" =>"gbItemsData",
                                "id" =>"176"
                            ]
                        ],
                        "template" => [
                            "data" => [
                                "type" =>"gbTemplates",
                                "id" =>"72"
                            ]
                        ]
                    ],
                    "links" => [
                        "self" =>"api.base.qixinyun.com/gbSearchData/176"
                    ]
                ]
            ],
            "included" =>[
                [
                    "type" =>"crews",
                    "id" =>"1",
                    "attributes" => [
                        "realName" =>"张科",
                        "cardId" =>"610428199609205018",
                        "userName" =>"18800000000",
                        "cellphone" =>"18800000000",
                        "category" =>1,
                        "purview" =>[8, 9],
                        "status" =>0,
                        "createTime" =>1597699740,
                        "updateTime" =>1620485256,
                        "statusTime" =>1620475256
                    ],
                    "relationships" => [
                        "userGroup" => [
                            "data" => [
                                "type" =>"userGroups",
                                "id" =>"1"
                            ]
                        ],
                        "department" => [
                            "data" => [
                                "type" =>"departments",
                                "id" =>"1"
                            ]
                        ]
                    ]
                ],
                [
                    "type" =>"userGroups",
                    "id" =>"1",
                    "attributes" => [
                        "name" =>"发展和改革委员会",
                        "shortName" =>"发改委",
                        "status" =>0,
                        "createTime" =>1558375495,
                        "updateTime" =>1610539122,
                        "statusTime" =>0
                    ]
                ],
                [
                    "type" =>"gbItemsData",
                    "id" =>"176",
                    "attributes" => [
                        "data" => [
                            "ZTMC" =>"市衡粤旅游发展有限公司",
                            "XK_XDR_LB" =>"法人及非法人组织",
                            "TYSHXYDM" =>"91410900MA40184E9Q",
                            "XK_XDR_GSZC" =>"914109006686666666",
                            "XK_XDR_ZZJG" =>"91291",
                            "XK_XDR_SWDJ" =>"237890014",
                            "XK_XDR_SYDW" =>"91410901M",
                            "XK_XDR_SHZZ" =>"MA46184E-7",
                            "XK_FRDB" =>"张晓苹",
                            "XK_FR_ZJLX" =>"身份证",
                            "XK_FR_ZJHM" =>"412825199408025671",
                            "XK_XDR_ZJLX" =>"身份证",
                            "XK_XDR_ZJHM" =>"412825199408025671",
                            "XK_XKWS" =>"高速公路联网管理中心交通行政许可决定4",
                            "XK_WSH" =>"豫高速公路许 字第20210705000090号",
                            "XK_XKLB" =>"普通",
                            "XK_XKZS" =>"高速公路超限运输车辆通行证1",
                            "XK_XKBH" =>"410000017210705A000092",
                            "XK_NR" =>"高速公路超限运输车辆通行证1",
                            "XK_JDRQ" =>"20210717",
                            "XK_YXQZ" =>"20210720",
                            "XK_YXQZI" =>"20210808",
                            "XK_XKJG" =>"河南省高速公路联网管理中心3",
                            "XK_XKJGDM" =>"12410000MB15527980",
                            "XK_ZT" =>"2",
                            "XK_LYDW" =>"河南省高速公路联网管理中心1",
                            "XK_LYDWDM" =>"12410000MB14727980",
                            "BZ" =>""
                        ]
                    ]
                ],
                [
                    "type" =>"gbTemplates",
                    "id" =>"72",
                    "attributes" => [
                        "name" =>"行政许可信息",
                        "identify" =>"HZXKXX",
                        "subjectCategory" =>["1"],
                        "dimension" =>1,
                        "exchangeFrequency" =>1,
                        "infoClassify" =>"1",
                        "infoCategory" =>"1",
                        "description" =>"行政许可信息",
                        "category" =>2,
                        "items" =>[
                            [
                                "name" =>"许可决定日期",
                                "type" =>"2",
                                "length" =>"8",
                                "options" =>[],
                                "remarks" =>"",
                                "identify" =>"XK_JDRQ",
                                "isMasked" =>"1",
                                "maskRule" =>[],
                                "dimension" =>"1",
                                "isNecessary" =>"1"
                            ],
                        ],
                        "status" =>0,
                        "createTime" =>1626746026,
                        "updateTime" =>1628746037,
                        "statusTime" =>0
                    ]
                ]
            ]
        ];
    }
}
