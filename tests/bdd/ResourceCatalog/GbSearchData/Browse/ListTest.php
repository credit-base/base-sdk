<?php
namespace Base\Sdk\ResourceCatalog\GbSearchData\Browse;

use Base\Sdk\ResourceCatalog\ListDataTrait;
use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ListTest extends TestCase
{
    use ListDataTrait;

    private $userGroup;

    private $mock;

    public function setUp()
    {
        $this->userGroup = new GbSearchData();
    }

    public function tearDown()
    {
        unset($this->userGroup);
    }

    protected function prepareData()
    {
        $data = $this->getListData();

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看委办局列表时
     */
    public function fetchUserGroupList() : array
    {
        $filter = [];
        return $this->getGbSearchDataList($filter);
    }

    /**
     * @Then  我可以看见企业数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchUserGroupList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals('gbSearchData', $request->getUri()->getPath());
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function response()
    {
        $gbSearchDataObject = $this->fetchUserGroupList();
        $gbSearchDataArray = $this->getListData()['data'];

        foreach ($gbSearchDataObject as $item) {
            foreach ($gbSearchDataArray as $val) {
                if ($val['id'] == $item->getId()) {
                    $this->assertEquals($val['attributes']['name'], $item->getName());
                    $this->assertEquals($val['attributes']['updateTime'], $item->getUpdateTime());
                }
            }
        }
    }
}
