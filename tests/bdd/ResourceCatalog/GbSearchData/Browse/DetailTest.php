<?php
namespace Base\Sdk\ResourceCatalog\GbSearchData\Browse;

use Base\Sdk\ResourceCatalog\Adapter\GbSearchData\GbSearchDataRestfulAdapter;
use Base\Sdk\ResourceCatalog\DetailDataTrait;
use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DetailTest extends TestCase
{
    use DetailDataTrait;
    
    private $gbSearchData;

    private $mock;

    public function setUp()
    {
        $this->gbSearchData = new GbSearchData();
    }

    public function tearDown()
    {
        unset($this->gbSearchData);
    }

    protected function prepareData()
    {
        $data = $this->getDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
                new Response(
                    200,
                    ['Content-Type' => 'application/vnd.api+json'],
                    $jsonData
                ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    protected function fetchGbSearchData($id) : GbSearchData
    {
        $adapter = new GbSearchDataRestfulAdapter();

        $this->gbSearchData = $adapter->fetchOne($id);

        return $this->gbSearchData;
    }

    public function testValidate()
    {
        $id = 309;
        $this->prepareData();
        $this->fetchGbSearchData($id);

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $contents);
        $this->assertEquals('gbSearchData/309', $path);
    }

    private function response()
    {
        $this->assertEquals('市衡粤旅游发展有限公司', $this->gbSearchData->getName());
        $this->assertEquals(GbSearchData::STATUS['ENABLED'], $this->gbSearchData->getStatus());
        $this->assertEquals(
            0,
            $this->gbSearchData->getStatusTime()
        );
        $this->assertEquals(
            1629200269,
            $this->gbSearchData->getCreateTime()
        );
        $this->assertEquals(
            1629200269,
            $this->gbSearchData->getUpdateTime()
        );
    }
}
