<?php


namespace Base\Sdk\ResourceCatalog\GbSearchData\Update;

use Base\Sdk\ResourceCatalog\DetailDataTrait;
use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Model\SearchData;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ConfirmTest extends TestCase
{
    use DetailDataTrait;

    private $gbSearchData;

    private $mock;

    public function setUp()
    {
        $this->gbSearchData = new GbSearchData();
    }

    public function tearDown()
    {
        unset($this->gbSearchData);
    }

    protected function prepareData()
    {
        $enableData = $this->getSearchDataStatusResponse(SearchData::DATA_STATUS['CONFIRM']);
        $jsonData = json_encode($enableData);

        $disableData = $this->getSearchDataStatusResponse(SearchData::DATA_STATUS['ENABLED']);

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], json_encode($disableData))
            ]
        );

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    protected function confirm()
    {
        return $this->gbSearchData->confirm();
    }

    public function testValidate()
    {
        $id = 309;
        $this->prepareData();

        $this->fetchGbSearchData($id);
        $this->confirm();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();

        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode(array()), $contents);
        $this->assertEquals('/gbSearchData/309/confirm', $path);
    }

    private function response()
    {
        $this->assertEquals(SearchData::DATA_STATUS['ENABLED'], $this->gbSearchData->getStatus());
    }
}
