<?php
namespace Base\Sdk\ResourceCatalog\GbSearchData\Update;

use Base\Sdk\ResourceCatalog\DetailDataTrait;
use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Model\SearchData;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DisableTest extends TestCase
{
    use DetailDataTrait;

    private $gbSearchData;

    private $mock;

    public function setUp()
    {
        $this->gbSearchData = new GbSearchData();
    }

    public function tearDown()
    {
        unset($this->gbSearchData);
    }

    /**
     * @Given: 存在需要禁用的用户
     */
    protected function prepareData()
    {
        $enableData = $this->getSearchDataStatusResponse(SearchData::DATA_STATUS['ENABLED']);
        $jsonData = json_encode($enableData);

        $disableData = $this->getSearchDataStatusResponse(SearchData::DATA_STATUS['DISABLED']);

        $this->mock = new MockHandler(
            [
                new Response(
                    200,
                    ['Content-Type' => 'application/vnd.api+json'],
                    $jsonData
                ),
                new Response(
                    201,
                    ['Content-Type' => 'application/vnd.api+json'],
                    json_encode($disableData)
                )
            ]
        );

        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @And:当我调用禁用函数,期待禁用成功
     */
    protected function disable()
    {
        return $this->gbSearchData->disable();
    }
    /**
     * @Then  我可以查到该条用户已被禁用
     */
    public function testValidate()
    {
        $id = 309;
        $this->prepareData();

        $this->fetchGbSearchData($id);
        $this->disable();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/gbSearchData/309/disable';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();

        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode(array()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(SearchData::DATA_STATUS['DISABLED'], $this->gbSearchData->getStatus());

        $statusTime = $updateTime = 0;
        $this->assertEquals($statusTime, $this->gbSearchData->getStatusTime());
        $this->assertEquals($updateTime, $this->gbSearchData->getUpdateTime());
    }
}
