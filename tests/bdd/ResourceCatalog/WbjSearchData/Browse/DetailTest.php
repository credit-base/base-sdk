<?php
namespace Base\Sdk\ResourceCatalog\WbjSearchData\Browse;

use Base\Sdk\ResourceCatalog\Adapter\WbjSearchData\WbjSearchDataRestfulAdapter;
use Base\Sdk\ResourceCatalog\DetailDataTrait;
use Base\Sdk\ResourceCatalog\Model\WbjSearchData;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DetailTest extends TestCase
{
    use DetailDataTrait;
    
    private $wbjSearchData;

    private $mock;

    public function setUp()
    {
        $this->wbjSearchData = new WbjSearchData();
    }

    public function tearDown()
    {
        unset($this->wbjSearchData);
    }

    protected function prepareData()
    {
        $data = $this->getDetailData();
        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
                new Response(
                    200,
                    ['Content-Type' => 'application/vnd.api+json'],
                    $jsonData
                ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    protected function fetchWbjSearchData($id) : WbjSearchData
    {
        $adapter = new WbjSearchDataRestfulAdapter();

        $this->wbjSearchData = $adapter->fetchOne($id);

        return $this->wbjSearchData;
    }

    public function testValidate()
    {
        $id = 309;
        $this->prepareData();
        $this->fetchWbjSearchData($id);

        $this->request();
        $this->response();
    }

    private function request()
    {
        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $contents);
        $this->assertEquals('wbjSearchData/309', $path);
    }

    private function response()
    {
        $this->assertEquals('市衡粤旅游发展有限公司', $this->wbjSearchData->getName());
        $this->assertEquals(WbjSearchData::STATUS['ENABLED'], $this->wbjSearchData->getStatus());
        $this->assertEquals(
            0,
            $this->wbjSearchData->getStatusTime()
        );
        $this->assertEquals(
            1629200269,
            $this->wbjSearchData->getCreateTime()
        );
        $this->assertEquals(
            1629200269,
            $this->wbjSearchData->getUpdateTime()
        );
    }
}
