<?php
namespace Base\Sdk\Rule\Rule\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Adapter\Rule\RuleRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Rule\ArrayDataTrait;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 *           我拥有规则管理权限、且当我需要查看通过审核的规则的列表时,
 *           在正式表中,管理正式表中的规则,通过列表与详情的形式查看到已审核通过的规则信息, 以便于我维护规则模块
 * @Scenario: 查看规则数据详情
 */

class DetailTest extends TestCase
{
    use ArrayDataTrait;

    private $rule;

    private $mock;

    public function setUp()
    {
        $this->rule = new Rule();
    }

    public function tearDown()
    {
        unset($this->rule);
    }

    /**
    * @Given: 存在一条规则数据
    */
    protected function prepareData()
    {
        $data = $this->getRuleDetailData(1);

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条规则数据详情时
    */
    protected function fetchRule($id)
    {

        $adapter = new RuleRestfulAdapter();

        $this->rule = $adapter->fetchOne($id);
      
        return $this->rule;
    }

    /**
     * @Then  我可以看见该条规则数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchRule($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'rules/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            $this->getCommonRulesData(),
            $this->rule->getRules()
        );
        $this->assertEquals(2, $this->rule->getTransformationCategory());
        $this->assertEquals(10, $this->rule->getSourceCategory());

        $this->assertEquals(1631685765, $this->rule->getVersion());
        $this->assertEquals(
            10,
            $this->rule->getDataTotal()
        );
   
        $this->assertEquals(Rule::STATUS['NORMAL'], $this->rule->getStatus());
        $this->assertEquals(1, $this->rule->getCrew()->getId());
        $this->assertEquals(1, $this->rule->getUserGroup()->getId());

        $this->assertEquals(
            1631686932,
            $this->rule->getStatusTime()
        );
        $this->assertEquals(
            1631685765,
            $this->rule->getCreateTime()
        );
        $this->assertEquals(
            1631686932,
            $this->rule->getUpdateTime()
        );
    }
}
