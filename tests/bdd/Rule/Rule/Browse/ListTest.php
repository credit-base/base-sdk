<?php
namespace Base\Sdk\Rule\Rule\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Model\Rule;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Rule\ArrayDataTrait;

/**
 * @Feature:  我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 *            我拥有规则管理权限、且当我需要查看通过审核的规则的列表时,
 *            在正式表中,管理正式表中的规则,通过列表与详情的形式查看到已审核通过的规则信息, 以便于我维护规则模块
 * @Scenario: 查看规则列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ArrayDataTrait;

    private $rule;

    private $mock;

    public function setUp()
    {
        $this->rule = new Rule();
    }

    public function tearDown()
    {
        unset($this->rule);
    }

    /**
    * @Given: 存在规则数据
    */
    protected function prepareData()
    {
        $data = $this->getRuleListData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看规则列表时
     */
    public function fetchRuleList()
    {
        $filter = [];
        $list =$this->getRuleList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见规则数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchRuleList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'rules';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $ruleList = $this->fetchRuleList();

        $ruleArray = $this->getRuleListData()['data'];
        
        foreach ($ruleList as $ruleObject) {
            foreach ($ruleArray as $rule) {
                if ($ruleObject->getId() == $rule['id']) {
                    $this->assertEquals($rule['attributes']['rules'], $ruleObject->getRules());
                    $this->assertEquals(
                        $rule['attributes']['transformationCategory'],
                        $ruleObject->getTransformationCategory()
                    );
                    $this->assertEquals(
                        $rule['attributes']['sourceCategory'],
                        $ruleObject->getSourceCategory()
                    );

                    $this->assertEquals(
                        $rule['attributes']['version'],
                        $ruleObject->getVersion()
                    );
                    $this->assertEquals(
                        $rule['attributes']['dataTotal'],
                        $ruleObject->getDataTotal()
                    );

                    $this->assertEquals($rule['attributes']['status'], $ruleObject->getStatus());
                    $this->assertEquals(
                        $rule['relationships']['crew']['data']['id'],
                        $ruleObject->getCrew()->getId()
                    );
                    $this->assertEquals(
                        $rule['relationships']['userGroup']['data']['id'],
                        $ruleObject->getUserGroup()->getId()
                    );

                    $this->assertEquals(
                        $rule['attributes']['statusTime'],
                        $ruleObject->getStatusTime()
                    );
                    $this->assertEquals(
                        $rule['attributes']['createTime'],
                        $ruleObject->getCreateTime()
                    );
                    $this->assertEquals(
                        $rule['attributes']['updateTime'],
                        $ruleObject->getUpdateTime()
                    );
                }
            }
        }
    }
}
