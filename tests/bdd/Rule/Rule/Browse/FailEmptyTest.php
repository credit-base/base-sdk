<?php
namespace Base\Sdk\Rule\Rule\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Adapter\Rule\RuleRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 *           我拥有规则管理权限、且当我需要查看通过审核的规则的列表时,
 *           在正式表中,管理正式表中的规则,通过列表与详情的形式查看到已审核通过的规则信息, 以便于我维护规则模块
 * @Scenario: 查看规则数据列表-异常不存在数据
 */

class FailEmptyTest extends TestCase
{
    private $rule;

    private $mock;

    public function setUp()
    {
        $this->rule = new Rule();
    }

    public function tearDown()
    {
        unset($this->rule);
    }

    /**
    * @Given: 不存在规则数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看规则列表时
     */
    public function fetchRuleList()
    {
        $adapter = new RuleRestfulAdapter();

        list($count, $this->rule) = $adapter
        ->search();
        unset($count);

        return $this->rule;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchRuleList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'rules';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->rule);
    }
}
