<?php
namespace Base\Sdk\Rule\Rule\Browse;

use Base\Sdk\Rule\Adapter\Rule\RuleRestfulAdapter;

trait SearchDataTrait
{
    protected function getRuleList(array $filter = [])
    {
        $adapter = new RuleRestfulAdapter();

        list($count, $ruleList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
        
        return $ruleList;
    }
}
