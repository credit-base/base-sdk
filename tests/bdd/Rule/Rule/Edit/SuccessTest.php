<?php
namespace Base\Sdk\Rule\Rule\Edit;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Adapter\Rule\RuleRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Rule\ArrayDataTrait;

/**
 * @Feature: 我是拥有规则管理权限的平台管理员/委办局管理员/操作用户,当我需要编辑规则信息时,
 *           在正式表中,编辑对应规则数据
 *           通过编辑规则界面，并根据我所采集的规则数据进行编辑,以便于我可以更好的维护规则管理列表
 * @Scenario: 正常编辑数据
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $editRuleObject;

    private $mock;

    public function setUp()
    {
        $this->editRuleObject = new Rule();
    }

    public function tearDown()
    {
        unset($this->editRuleObject);
    }

    /**
    * @Given: 我并未编辑过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getRuleOtherDetailData(1);

        $jsonData = json_encode($data);
        

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($this->getRuleDetailData(1))
               )
            ]
        );
      
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要编辑的规则数据
    */
    protected function fetchRule($id)
    {
        $adapter = new RuleRestfulAdapter();
        $this->editRuleObject = $adapter->fetchOne($id);
        
        return $this->editRuleObject;
    }

    /**
    * @When:当我调用编辑函数,期待编辑成功
    */
    protected function edit()
    {
        $this->editRuleObject->setRules(
            $this->getCommonRulesData()
        );
     
        $crew = new Crew(1);
        $this->editRuleObject->setCrew($crew);
       
        return $this->editRuleObject->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchRule($id);

        $this->edit();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/rules/1';

        $request = $this->mock->getLastRequest();

        $requestContents = $this->getRuleOperationRequest();
        unset($requestContents['data']['attributes']['transformationCategory']);
        unset($requestContents['data']['attributes']['sourceCategory']);
        unset($requestContents['data']['relationships']['transformationTemplate']);
        unset($requestContents['data']['relationships']['sourceTemplate']);

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($requestContents), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            $this->getCommonRulesData(),
            $this->editRuleObject->getRules()
        );
        $this->assertEquals(1, $this->editRuleObject->getCrew()->getId());
        $this->assertEquals(1, $this->editRuleObject->getUserGroup()->getId());

        $this->assertEquals(
            1631686932,
            $this->editRuleObject->getStatusTime()
        );
        $this->assertEquals(
            1631686932,
            $this->editRuleObject->getUpdateTime()
        );
    }
}
