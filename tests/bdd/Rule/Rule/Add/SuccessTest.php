<?php
namespace Base\Sdk\Rule\Rule\Add;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Model\Rule;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Rule\ArrayDataTrait;
use Base\Sdk\Template\Model\Template;

/**
 * @Feature: 我是拥有规则管理权限的平台管理员/委办局管理员/操作用户,
 *           当我需要新增一个规则时,在正式表中,新增对应的规则数据
 *           通过新增规则界面，并根据我所采集的规则数据进行新增,以便于我维护规则列表
 * @Scenario: 正常新增数据
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $rule;

    private $mock;

    public function setUp()
    {
        $this->rule = new Rule();
    }

    public function tearDown()
    {
        unset($this->rule);
    }

    /**
    * @Given: 我并未新增过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getRuleDetailData(1);
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
       
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我调用添加函数,期待返回true
    */
    protected function add()
    {
        $this->rule = new Rule();
        $this->rule->setTransformationCategory(2);
     
        $crew = new Crew(1);
        $this->rule->setCrew($crew);
        $transformationTemplate = new Template(1);
        $this->rule->setTransformationTemplate($transformationTemplate);
        $sourceTemplate = new Template(1);
        $this->rule->setSourceTemplate($sourceTemplate);
        $this->rule->setSourceCategory(10);
        $this->rule->setRules($this->getCommonRulesData());
      
        return  $this->rule->add();
    }
    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        
        $this->add();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/rules';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getRuleOperationRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        
        $this->assertEquals(2, $this->rule->getTransformationCategory());
        $this->assertEquals(Rule::STATUS['NORMAL'], $this->rule->getStatus());
        
        $this->assertEquals(10, $this->rule->getSourceCategory());
       
        $this->assertEquals(
            10,
            $this->rule->getDataTotal()
        );
   
        $this->assertEquals(1631685765, $this->rule->getVersion());
        $this->assertEquals(
            $this->getCommonRulesData(),
            $this->rule->getRules()
        );
        $this->assertEquals(1, $this->rule->getCrew()->getId());
        $this->assertEquals(1, $this->rule->getUserGroup()->getId());

        $this->assertEquals(
            0,
            $this->rule->getStatusTime()
        );
        $this->assertEquals(
            1631685765,
            $this->rule->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->rule->getUpdateTime()
        );
    }
}
