<?php
namespace Base\Sdk\Rule\Rule\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Adapter\Rule\RuleRestfulAdapter;


use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Rule\ArrayDataTrait;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 *           我拥有规则管理权限、且当我需要删除正式表的规则时,
 *           在正式表中,删除正式表中的规则, 以便于我维护规则模块
 * @Scenario: 删除规则数据
 */

class DeleteTest extends TestCase
{
    use ArrayDataTrait;

    private $rule;

    private $mock;

    public function setUp()
    {
        $this->rule = new Rule();
    }

    public function tearDown()
    {
        unset($this->rule);
    }

    /**
    * @Given: 存在需要删除的规则数据
    */
    protected function prepareData()
    {
        $data = $this->getRuleDetailData(1);
        $jsonData = json_encode($data);
        
        $deleteData = $this->getRuleDetailData(
            1,
            Rule::STATUS['DELETED']
        );
        
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($deleteData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要删除的规则数据
    */
    protected function fetchRule($id)
    {
        $adapter = new RuleRestfulAdapter();
        $this->rule = $adapter->fetchOne($id);
        
        return $this->rule;
    }

    /**
    * @And:当我调用删除函数,期待删除成功
    */
    protected function delete()
    {
        return $this->rule->deletes();
    }
    /**
     * @Then  数据已经被删除
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchRule($id);
        $this->delete();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/rules/1/delete';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getRuleStatusRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Rule::STATUS['DELETED'], $this->rule->getStatus());
        $this->assertEquals(1, $this->rule->getCrew()->getId());
        $this->assertEquals(
            1631686932,
            $this->rule->getStatusTime()
        );
        $this->assertEquals(
            1631686932,
            $this->rule->getUpdateTime()
        );
    }
}
