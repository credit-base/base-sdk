<?php
namespace Base\Sdk\Rule\UnAuditRule\Edit;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Model\UnAuditRule;
use Base\Sdk\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use Base\Sdk\Common\Model\IApplyAble;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Rule\ArrayDataTrait;

/**
 * @Feature: 我是拥有规则管理权限的平台管理员/委办局管理员/操作用户,当我需要编辑规则信息时,在审核表中,编辑已驳回规则数据
 *           通过编辑规则界面，并根据我所采集的规则数据进行编辑,以便于我可以更好的管理规则
 * @Scenario: 正常编辑审核数据
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditRule;

    private $mock;

    public function setUp()
    {
        $this->unAuditRule = new UnAuditRule();
    }

    public function tearDown()
    {
        unset($this->unAuditRule);
    }

    /**
    * @Given: 我并未编辑过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getUnAuditRuleOtherDetailData(1, IApplyAble::APPLY_STATUS['REJECT']);

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($this->getUnAuditRuleDetailData(1))
               )
            ]
        );
      
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要编辑的规则数据
    */
    protected function fetchUnAuditRule($id)
    {
        $adapter = new UnAuditRuleRestfulAdapter();
        $this->unAuditRule = $adapter->fetchOne($id);
        
        return $this->unAuditRule;
    }

    /**
    * @When:当我调用编辑函数,期待编辑成功
    */
    protected function edit()
    {
        $this->unAuditRule->setRules(
            $this->getCommonRulesData()
        );
     
        $crew = new Crew(1);
        $this->unAuditRule->setCrew($crew);
       
        return $this->unAuditRule->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditRule($id);

        $this->edit();
    
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedRules/1/resubmit';

        $request = $this->mock->getLastRequest();

        $requestEditData = $this->getRuleOperationRequest();
        $requestEditData['data']['type'] = "unAuditedRules";
        unset($requestEditData['data']['attributes']['transformationCategory']);
        unset($requestEditData['data']['attributes']['sourceCategory']);
        unset($requestEditData['data']['relationships']['transformationTemplate']);
        unset($requestEditData['data']['relationships']['sourceTemplate']);

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($requestEditData), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            $this->getCommonRulesData(),
            $this->unAuditRule->getRules()
        );
        
        $this->assertEquals(1, $this->unAuditRule->getApplyCrew()->getId());
        $this->assertEquals(1, $this->unAuditRule->getApplyUserGroup()->getId());
        $this->assertEquals(
            IApplyAble::APPLY_STATUS['PENDING'],
            $this->unAuditRule->getApplyStatus()
        );
        $this->assertEquals(1, $this->unAuditRule->getCrew()->getId());
        
        $this->assertEquals(
            1631686932,
            $this->unAuditRule->getStatusTime()
        );
    
        $this->assertEquals(
            1631686932,
            $this->unAuditRule->getUpdateTime()
        );
    }
}
