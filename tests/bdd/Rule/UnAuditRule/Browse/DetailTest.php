<?php
namespace Base\Sdk\Rule\UnAuditRule\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Model\UnAuditRule;
use Base\Sdk\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Rule\ArrayDataTrait;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 *           我拥有规则管理权限、且当我需要查看未审核或已驳回的规则列表时,
 *           在审核表中,查看审核表中的已驳回或是未审核的信息,通过列表与详情的形式查看到未审核或已驳回的规则信息,以便于我维护规则模块
 * @Scenario: 查看规则审核数据详情
 */

class DetailTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditRule;

    private $mock;

    public function setUp()
    {
        $this->unAuditRule = new UnAuditRule();
    }

    public function tearDown()
    {
        unset($this->unAuditRule);
    }

    /**
    * @Given: 存在一条规则审核数据
    */
    protected function prepareData()
    {
        $data = $this->getUnAuditRuleDetailData(1);

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条规则数据审核详情时
    */
    protected function fetchUnAuditRule($id)
    {

        $adapter = new UnAuditRuleRestfulAdapter();

        $this->unAuditRule = $adapter->fetchOne($id);
      
        return $this->unAuditRule;
    }

    /**
     * @Then  我可以看见该条规则数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditRule($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedRules/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            $this->getCommonRulesData(),
            $this->unAuditRule->getRules()
        );
        $this->assertEquals(2, $this->unAuditRule->getTransformationCategory());
        $this->assertEquals(10, $this->unAuditRule->getSourceCategory());

        $this->assertEquals(1631685765, $this->unAuditRule->getVersion());
        $this->assertEquals(
            10,
            $this->unAuditRule->getDataTotal()
        );
   
        $this->assertEquals(Rule::STATUS['NORMAL'], $this->unAuditRule->getStatus());
        $this->assertEquals(1, $this->unAuditRule->getCrew()->getId());
        $this->assertEquals(1, $this->unAuditRule->getUserGroup()->getId());

        $this->assertEquals(
            IApplyAble::APPLY_STATUS['PENDING'],
            $this->unAuditRule->getApplyStatus()
        );
        $this->assertEquals(2, $this->unAuditRule->getOperationType());

        $this->assertEquals(Rule::STATUS['NORMAL'], $this->unAuditRule->getStatus());
        $this->assertEquals(1, $this->unAuditRule->getCrew()->getId());
        $this->assertEquals(1, $this->unAuditRule->getUserGroup()->getId());

        $this->assertEquals(
            1631686932,
            $this->unAuditRule->getStatusTime()
        );
        $this->assertEquals(
            1631685765,
            $this->unAuditRule->getCreateTime()
        );
        $this->assertEquals(
            1631686932,
            $this->unAuditRule->getUpdateTime()
        );
    }
}
