<?php
namespace Base\Sdk\Rule\UnAuditRule\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Rule\Model\UnAuditRule;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;


use Base\Sdk\Rule\ArrayDataTrait;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 *           我拥有规则管理权限、且当我需要查看未审核或已驳回的规则列表时,
 *           在审核表中,查看审核表中的已驳回或是未审核的信息,
 *           通过列表与详情的形式查看到未审核或已驳回的规则信息,以便于我维护规则模块
 * @Scenario: 查看规则审核列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ArrayDataTrait;

    private $unAuditRule;

    private $mock;

    public function setUp()
    {
        $this->unAuditRule = new UnAuditRule();
    }

    public function tearDown()
    {
        unset($this->unAuditRule);
    }

    /**
    * @Given: 存在规则数据
    */
    protected function prepareData()
    {
        $data = $this->getUnAuditRuleListData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看规则列表时
     */
    public function fetchUnAuditRuleList()
    {
        $filter = [];

        $unAuditRule = $this->getUnAuditRuleList($filter);

        return $unAuditRule;
    }

    /**
     * @Then  我可以看见规则数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchUnAuditRuleList();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedRules';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $list = $this->fetchUnAuditRuleList();

        $array = $this->getUnAuditRuleListData()['data'];

        foreach ($list as $unAuditRuleObject) {
            foreach ($array as $unAuditRuleData) {
                if ($unAuditRuleObject->getId() == $unAuditRuleData['id']) {
                    $this->assertEquals($unAuditRuleData['attributes']['rules'], $unAuditRuleObject->getRules());
                    $this->assertEquals(
                        $unAuditRuleData['attributes']['transformationCategory'],
                        $unAuditRuleObject->getTransformationCategory()
                    );
                    $this->assertEquals(
                        $unAuditRuleData['attributes']['sourceCategory'],
                        $unAuditRuleObject->getSourceCategory()
                    );

                    $this->assertEquals(
                        $unAuditRuleData['attributes']['version'],
                        $unAuditRuleObject->getVersion()
                    );
                    $this->assertEquals(
                        $unAuditRuleData['attributes']['dataTotal'],
                        $unAuditRuleObject->getDataTotal()
                    );

                    $this->assertEquals(
                        $unAuditRuleData['attributes']['status'],
                        $unAuditRuleObject->getStatus()
                    );

                    $this->assertEquals(
                        $unAuditRuleData['relationships']['applyCrew']['data']['id'],
                        $unAuditRuleObject->getApplyCrew()->getId()
                    );
                    $this->assertEquals(
                        $unAuditRuleData['relationships']['applyUserGroup']['data']['id'],
                        $unAuditRuleObject->getApplyUserGroup()->getId()
                    );
                    $this->assertEquals(
                        $unAuditRuleData['attributes']['applyStatus'],
                        $unAuditRuleObject->getApplyStatus()
                    );

                    $this->assertEquals(
                        $unAuditRuleData['attributes']['statusTime'],
                        $unAuditRuleObject->getStatusTime()
                    );
                    $this->assertEquals(
                        $unAuditRuleData['attributes']['createTime'],
                        $unAuditRuleObject->getCreateTime()
                    );
                    $this->assertEquals(
                        $unAuditRuleData['attributes']['updateTime'],
                        $unAuditRuleObject->getUpdateTime()
                    );
                }
            }
        }
    }
}
