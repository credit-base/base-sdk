<?php
namespace Base\Sdk\Rule\UnAuditRule\Browse;

use Base\Sdk\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter;

trait SearchDataTrait
{
    protected function getUnAuditRuleList(array $filter)
    {
        $adapter = new UnAuditRuleRestfulAdapter();

        list($count, $unAuditRuleList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $unAuditRuleList;
    }
}
