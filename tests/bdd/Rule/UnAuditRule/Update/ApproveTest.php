<?php
namespace Base\Sdk\Rule\UnAuditRule\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Rule\Model\UnAuditRule;
use Base\Sdk\Rule\Adapter\UnAuditRule\UnAuditRuleRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Rule\ArrayDataTrait;

/**
 * @Feature: 我是拥有规则审核权限的平台管理员/委办局管理员/操作用户,当我需要审核一个规则时,在审核表中,审核待审核的规则数据
 *           通过规则详情页面的审核通过与审核驳回操作,以便于我维护规则列表
 * @Scenario: 审核通过
 */

class ApproveTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditRule;

    private $mock;

    public function setUp()
    {
        $this->unAuditRule = new UnAuditRule();
    }

    public function tearDown()
    {
        unset($this->unAuditRule);
    }

    /**
    * @Given: 存在需要审核的规则
    */
    protected function prepareData()
    {
        $data = $this->getUnAuditRuleDetailData(1);
        $jsonData = json_encode($data);
        
        $approveData = $this->getUnAuditRuleDetailData(
            1,
            IApplyAble::APPLY_STATUS['APPROVE']
        );

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($approveData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要审核的规则
    */
    protected function fetchUnAuditRule($id)
    {
        $adapter = new UnAuditRuleRestfulAdapter();
        $this->unAuditRule = $adapter->fetchOne($id);
        
        return $this->unAuditRule;
    }

    /**
    * @And:当我调用审核函数,期待审核成功
    */
    protected function approve()
    {
       
        $crew = new Crew(1);
        $this->unAuditRule->setApplyCrew($crew);
     
        return $this->unAuditRule->approve();
    }
    /**
     * @Then  我可以查到该条规则已被审核
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditRule($id);
        $this->approve();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedRules/1/approve';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getUnAuditRuleStatusRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            IApplyAble::APPLY_STATUS['APPROVE'],
            $this->unAuditRule->getApplyStatus()
        );
        $this->assertEquals(
            1,
            $this->unAuditRule->getApplyCrew()->getId()
        );

        $this->assertEquals(
            1631686932,
            $this->unAuditRule->getStatusTime()
        );
    
        $this->assertEquals(
            1631686932,
            $this->unAuditRule->getUpdateTime()
        );
    }
}
