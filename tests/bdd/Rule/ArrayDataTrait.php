<?php
namespace Base\Sdk\Rule;

use Base\Sdk\Rule\Model\Rule;

use Base\Sdk\Common\Model\IApplyAble;

trait ArrayDataTrait
{
    public function getRuleDetailData(
        int $id,
        int $status = Rule::STATUS['NORMAL']
    ):array {
        $data = $this->getRuleOperationRequest();
        $data['data']['id'] = $id;
        $data['data']['links'] = [
            "self"=> "127.0.0.1:8089/rules/2"
        ];
        $data['data']['attributes']['status'] = $status;
        $data['data']['attributes']['version'] = 1631685765;
        $data['data']['attributes']['dataTotal'] = 10;
        $data['data']['attributes']['createTime'] = 1631685765;
        $data['data']['attributes']['updateTime'] = 1631686932;
        $data['data']['attributes']['statusTime'] = 1631686932;
        $data['data']['relationships'] = $this->getCommonRelationshipsData();
        $data['included'] = $this->getCommonIncludedData();
        return $data;
    }

    protected function getRuleListData():array
    {
        $list = [
            "meta"=> [
                "count"=> 2,
                "links"=> [
                    "first"=> null,
                    "last"=> null,
                    "prev"=> null,
                    "next"=> null
                ]
            ],
            "links"=> [
                "first"=> null,
                "last"=> null,
                "prev"=> null,
                "next"=> null
            ]
        ];
        $list['data'][0] = $this->getRuleDetailData(1)['data'];
        $list['data'][1] = $this->getRuleOtherDetailData(2)['data'];

        $list['included'] = $this->getCommonIncludedData();

        return $list;
    }

    protected function getRuleStatusRequest() : array
    {
        return [
            "data"=>[
                "type"=>"rules",
                "attributes"=>[],
                "relationships"=> [
                    "crew"=> [
                        "data"=>[
                            [
                                "type"=> "crews",
                                "id"=> 1
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getRuleOperationRequest():array
    {
        $rules = $this->getCommonRulesData();
        $data = [
            "data"=> [
                "type"=> "rules",
                "attributes"=> [
                    "rules"=> $rules,
                    "transformationCategory" => 2,
                    "sourceCategory" => 10
                ],
                "relationships"=> [
                    "crew"=> [
                        "data"=> [
                            [
                                "type"=> "crews",
                                "id"=> 1
                            ]
                        ]
                    ],
                    "transformationTemplate"=> [
                        "data"=> [
                            [
                                "type"=> "transformationTemplates",
                                "id"=> 1
                            ]
                        ]
                    ],
                    "sourceTemplate"=> [
                        "data"=> [
                            [
                                "type"=> "sourceTemplates",
                                "id"=> 1
                            ]
                        ]
                    ]
                ]
            ]
        ];
    
        return $data;
    }

    protected function getCommonRulesData():array
    {
        return array(
            'transformationRule' => array(
                "ZTMC" => "ZTMC",
                "XXLB" => "XXLB"
            ),
            'completionRule' =>  array(
                'ZTMC' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                ),
                'TYSHXYDM' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                ),
            ),
            'comparisonRule' =>  array(
                'ZTMC' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                ),
                'TYSHXYDM' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                ),
            ),
            'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC","TYSHXYDM"))
        );
    }

    protected function getCommonRelationshipsData():array
    {
        return [
            "userGroup"=> [
                "data"=> [
                    "type"=> "userGroups",
                    "id"=> "1"
                ]
            ],
            "crew"=> [
                "data"=> [
                    "type"=> "crews",
                    "id"=> "1"
                ]
            ],
            "transformationTemplate"=>array(
                "data"=>array(
                    "type"=>"gbTemplates",
                    "id"=>1
                )
            ),
            "sourceTemplate"=>array(
                "data"=>array(
                    "type"=>"templates",
                    "id"=>1
                )
            ),
        ];
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function getCommonIncludedData():array
    {
        return [
            [
                "type"=> "userGroups",
                "id"=> "1",
                "attributes"=> [
                    "name"=> "萍乡市发展和改革委员会",
                    "shortName"=> "发改委",
                    "status"=> 0,
                    "createTime"=> 1516168970,
                    "updateTime"=> 1516168970,
                    "statusTime"=> 0
                ]
            ],
            [
                "type"=> "crews",
                "id"=> "1",
                "attributes"=> [
                    "realName"=> "张科",
                    "cardId"=> "412825199009094532",
                    "userName"=> "18800000000",
                    "cellphone"=> "18800000000",
                    "category"=> 1,
                    "purview"=> [
                        "1",
                        "2",
                        "3"
                    ],
                    "status"=> 0,
                    "createTime"=> 1618284031,
                    "updateTime"=> 1619578455,
                    "statusTime"=> 0
                ],
                "relationships"=> [
                    "userGroup"=> [
                        "data"=> [
                            "type"=> "userGroups",
                            "id"=> "1"
                        ]
                    ],
                    "department"=> [
                        "data"=> [
                            "type"=> "departments",
                            "id"=> "3"
                        ]
                    ]
                ]
            ],
            [
                "type"=> "baseTemplates",
                "id"=> "1",
                "attributes"=> [
                    "name"=> "企业基本信息",
                    "identify"=> "QYJBXX",
                    "subjectCategory"=> [
                        "1",
                        "3"
                    ],
                    "dimension"=> 1,
                    "exchangeFrequency"=> 4,
                    "infoClassify"=> "5",
                    "infoCategory"=> "1",
                    "description"=> "企业基本信息",
                    "category"=> 3,
                    "items"=> [
                        [
                            "name"=> "主体名称",
                            "type"=> "1",
                            "length"=> "40",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "ZTMC",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "2",
                            "isNecessary"=> "0"
                        ]
                    ],
                    "status"=> 0,
                    "createTime"=> 1626504022,
                    "updateTime"=> 1631585037,
                    "statusTime"=> 0
                ]
            ],
            [
                "type"=> "bjTemplates",
                "id"=> "6",
                "attributes"=> [
                    "name"=> "企业基本信息",
                    "identify"=> "QYJBXX",
                    "subjectCategory"=> [
                        "1",
                        "3"
                    ],
                    "dimension"=> 1,
                    "exchangeFrequency"=> 4,
                    "infoClassify"=> "5",
                    "infoCategory"=> "1",
                    "description"=> "企业基本信息",
                    "category"=> 1,
                    "items"=> [
                        [
                            "name"=> "主体名称",
                            "type"=> "1",
                            "length"=> "200",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "ZTMC",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "统一社会信用代码",
                            "type"=> "1",
                            "length"=> "18",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "TYSHXYDM",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "成立日期",
                            "type"=> "2",
                            "length"=> "8",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "CLRQ",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "核准日期",
                            "type"=> "2",
                            "length"=> "8",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "HZRQ",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "经营场所",
                            "type"=> "1",
                            "length"=> "255",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "JYCS",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "注册资本（万）",
                            "type"=> "1",
                            "length"=> "255",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "ZCZB",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "营业期限自",
                            "type"=> "2",
                            "length"=> "8",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "YYQXQ",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "营业期限至",
                            "type"=> "2",
                            "length"=> "8",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "YYQXZ",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "经营范围",
                            "type"=> "1",
                            "length"=> "2000",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "JYFW",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "登记机关",
                            "type"=> "1",
                            "length"=> "255",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "DJJG",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "法定代表人",
                            "type"=> "1",
                            "length"=> "255",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "FDDBR",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "法人身份证号",
                            "type"=> "1",
                            "length"=> "200",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "FRZJHM",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "登记状态",
                            "type"=> "1",
                            "length"=> "200",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "DJZT",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "企业类型",
                            "type"=> "1",
                            "length"=> "200",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "QYLX",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "企业类型代码",
                            "type"=> "1",
                            "length"=> "200",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "QYLXDM",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "行业门类",
                            "type"=> "1",
                            "length"=> "4",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "HYML",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "行业代码",
                            "type"=> "1",
                            "length"=> "200",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "HYDM",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "所属区域",
                            "type"=> "1",
                            "length"=> "50",
                            "options"=> [],
                            "remarks"=> "",
                            "identify"=> "SSQY",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "1",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "主体类别",
                            "type"=> "6",
                            "length"=> "50",
                            "options"=> [
                                "法人及非法人组织",
                                "个体工商户"
                            ],
                            "remarks"=> "法人及非法人组织;自然人;个体工商户，支持多选",
                            "identify"=> "ZTLB",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "2",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "公开范围",
                            "type"=> "5",
                            "length"=> "20",
                            "options"=> [
                                "社会公开"
                            ],
                            "remarks"=> "支持单选",
                            "identify"=> "GKFW",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "2",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "更新频率",
                            "type"=> "5",
                            "length"=> "20",
                            "options"=> [
                                "每月"
                            ],
                            "remarks"=> "支持单选",
                            "identify"=> "GXPL",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "2",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "信息分类",
                            "type"=> "5",
                            "length"=> "50",
                            "options"=> [
                                "其他"
                            ],
                            "remarks"=> "支持单选",
                            "identify"=> "XXFL",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "2",
                            "isNecessary"=> "1"
                        ],
                        [
                            "name"=> "信息类别",
                            "type"=> "5",
                            "length"=> "50",
                            "options"=> [
                                "基础信息"
                            ],
                            "remarks"=> "信息性质类型，支持单选",
                            "identify"=> "XXLB",
                            "isMasked"=> "0",
                            "maskRule"=> [],
                            "dimension"=> "2",
                            "isNecessary"=> "1"
                        ]
                    ],
                    "status"=> 0,
                    "createTime"=> 1631264706,
                    "updateTime"=> 1631584898,
                    "statusTime"=> 0
                ],
                "relationships"=> [
                    "sourceUnit"=> [
                        "data"=> [
                            "type"=> "userGroups",
                            "id"=> "1"
                        ]
                    ],
                    "gbTemplate"=> [
                        "data"=> [
                            "type"=> "gbTemplates",
                            "id"=> "4"
                        ]
                    ]
                ]
            ],
        ];
    }

    protected function getRuleOtherDetailData($id = 1):array
    {
        $data = [
            "data"=>[
                "type"=> "rules",
                "id"=> $id,
                "attributes"=> [
                    "rules"=>array(
                        'transformationRule' => array(
                            "ZTMC" => "ZTMC",
                            "SFZH" => "SFZH"
                        ),
                        'completionRule' =>  array(
                            'TYSHXYDM' => array(
                                array('id'=>4, 'base'=> array(2), 'item'=>'TYSHXYDM'),
                                array('id'=>7, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                            ),
                        ),
                        'comparisonRule' =>  array(
                            'ZTMC' => array(
                                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                                array('id'=>2, 'base'=> array(2), 'item'=>'ZTMC'),
                            )
                        ),
                        'deDuplicationRule' => array("result"=>1, "items"=>array("ZTMC","TYSHXYDM"))
                    ),
                    "transformationCategory" => 1,
                    "sourceCategory" => 20,
                    "version"=> 1631687786,
                    "dataTotal"=> 0,
                    "status"=> 0,
                    "createTime"=> 1631687786,
                    "updateTime"=> 1631689242,
                    "statusTime"=> 1631689242
                ],
                "links"=> [
                    "self"=> "127.0.0.1=>8089/rules/1"
                ]
            ]
        ];
        $data['data']['relationships'] = $this->getCommonRelationshipsData();
        $data['included'] = $this->getCommonIncludedData();

        return $data;
    }

    protected function getUnAuditRuleDetailData(
        $id,
        $applyStatus = IApplyAble::APPLY_STATUS['PENDING']
    ):array {
        $data = $this->getRuleDetailData($id);
        $data = $this->getUnAuditRuleCommonDetailData($data, $applyStatus);
     
        return $data;
    }

    protected function getUnAuditRuleCommonDetailData(
        $data,
        $applyStatus = IApplyAble::APPLY_STATUS['PENDING']
    ):array {
        $data['data']['type'] = "unAuditedRules";
        $data['data']['attributes']['applyStatus'] = $applyStatus;
        $data['data']['attributes']['rejectReason'] = '驳回原因';
        $data['data']['attributes']['operationType'] = 2;
        $data['data']['attributes']['relationId'] = 2;
        $data['data']['links'] = [
            "self"=> "127.0.0.1:8089/unAuditedRules/1"
        ];
        $relationships = $this->getUnAuditRuleRelationshipsData();
        $data['data']['relationships'] = array_merge($this->getCommonRelationshipsData(), $relationships);

        return $data;
    }

    protected function getUnAuditRuleRelationshipsData():array
    {
        return [
            "applyUserGroup"=> [
                "data"=> [
                    "type"=> "userGroups",
                    "id"=> "1"
                ]
            ],
            "applyCrew"=> [
                "data"=> [
                    "type"=> "crews",
                    "id"=> "1"
                ]
            ],
            "publishCrew"=> [
                "data"=> [
                    "type"=> "crews",
                    "id"=> "1"
                ]
            ]
        ];
    }

    protected function getUnAuditRuleOtherDetailData(
        $id = 1,
        $applyStatus = IApplyAble::APPLY_STATUS['PENDING']
    ):array {
        $data = $this->getRuleOtherDetailData($id);
        $data = $this->getUnAuditRuleCommonDetailData($data, $applyStatus);

        return $data;
    }

    protected function getUnAuditRuleStatusRequest():array
    {
        return [
            "data"=>[
                "type"=>"unAuditedRules",
                "attributes"=>[],
                "relationships"=> [
                    "applyCrew"=> [
                        "data"=>[
                            [
                                "type"=> "crews",
                                "id"=> 1
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getUnAuditRuleListData():array
    {
        $data = $this->getRuleListData();

        $data['data'][0] = $this->getUnAuditRuleListCommonData($data['data'][0]);

        $data['data'][1] = $this->getUnAuditRuleListCommonData($data['data'][1]);

        $data['included'] = $this->getCommonIncludedData();

        return $data;
    }

    protected function getUnAuditRuleListCommonData(
        $data,
        $applyStatus = IApplyAble::APPLY_STATUS['PENDING']
    ):array {
        $data['type'] = "unAuditedRules";
        $data['attributes']['applyStatus'] = $applyStatus;
        $data['attributes']['rejectReason'] = '驳回原因';
        $data['attributes']['operationType'] = 2;
        $data['attributes']['relationId'] = 2;
        $data['links'] = [
            "self"=> "127.0.0.1:8089/unAuditedRules/1"
        ];
        $relationships = $this->getUnAuditRuleRelationshipsData();
        $data['relationships'] = array_merge($this->getCommonRelationshipsData(), $relationships);

        return $data;
    }
}
