<?php


namespace Base\Sdk\Member\Add;

use Base\Sdk\Member\Model\SecurityQA;
use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\ArrayDataTrait;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new Member();
    }

    public function tearDown()
    {
        unset($this->member);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getMemberDetailData(1);
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;

        $jsonData = json_encode($data);

        $empty = [
            "errors" => [
                [
                    "id" => "10",
                    "links" => [
                        "about" => ""
                    ],
                    "status" => "404",
                    "code" => "RESOURCE_NOT_EXIST",
                    "title" => "Resource not exist",
                    "detail" => "Server can not find resource",
                    "source" => [],
                    "meta" => []
                ]
            ]
        ];

        $this->mock = new MockHandler([
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], json_encode($empty)),
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], json_encode($empty)),
            new Response(404, ['Content-Type' => 'application/vnd.api+json'], json_encode($empty)),
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $this->member = new Member();
        
        $this->member->setUserName('用户名名');
        $this->member->setRealName('用户名名');
        $this->member->setCellphone('13233334444');
        $this->member->setEmail('email');
        $this->member->setCardId('12233334444');
        $this->member->setContactAddress('12233334444');
        $this->member->setPassword('12233334444');

        $securityQa = new SecurityQA();
        $securityQa->setId(0);
        $securityQa->setAnswer('12233334444');
        $this->member->setSecurityQa($securityQa);

        return $this->member->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }


    private function request()
    {
        $expectedPath = '/members';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getMemberAddRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('用户名名', $this->member->getRealName());
        $this->assertEquals(0, $this->member->getStatusTime());
        $this->assertEquals(1516174523, $this->member->getCreateTime());
        $this->assertEquals(0, $this->member->getUpdateTime());
    }
}
