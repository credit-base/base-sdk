<?php
namespace Base\Sdk\Member\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Adapter\Member\MemberRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Member\ArrayDataTrait;

/**
 * @Feature: 我是拥有用户发布权限的平台管理员/委办局管理员/操作用户,
 *           当我需要修改某个特定用户禁用时,在发布表的操作栏中,将用户修改为禁用状态
 *           通过发布表的操作栏中的禁用操作,以便于我可以更好的维护用户发布管理列表
 * @Scenario: 禁用用户
 */

class DisableTest extends TestCase
{
    use ArrayDataTrait;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new Member();
    }

    public function tearDown()
    {
        unset($this->member);
    }

    /**
    * @Given: 存在需要禁用的用户
    */
    protected function prepareData()
    {
        $enableData = $this->getMemberStatusResponse(Member::STATUS['ENABLED']);
        $jsonData = json_encode($enableData);
        
        $disableData = $this->getMemberStatusResponse(Member::STATUS['DISABLED']);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($disableData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要禁用的用户
    */
    protected function fetchMember($id)
    {
        $adapter = new MemberRestfulAdapter();
        $this->member = $adapter->fetchOne($id);
        
        return $this->member;
    }

    /**
    * @And:当我调用禁用函数,期待禁用成功
    */
    protected function disable()
    {
        return $this->member->disable();
    }
    /**
     * @Then  我可以查到该条用户已被禁用
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchMember($id);
        $this->disable();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/members/3/disable';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getMemberStatusRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Member::STATUS['DISABLED'], $this->member->getStatus());

        $statusTime = $updateTime = 1621847560;
        $this->assertEquals($statusTime, $this->member->getStatusTime());
        $this->assertEquals($updateTime, $this->member->getUpdateTime());
    }
}
