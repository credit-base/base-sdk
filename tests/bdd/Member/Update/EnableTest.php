<?php


namespace Base\Sdk\Member\Update;

use Base\Sdk\Member\Adapter\Member\MemberRestfulAdapter;
use Base\Sdk\Member\ArrayDataTrait;
use Base\Sdk\Member\Model\Member;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class EnableTest extends TestCase
{
    use ArrayDataTrait;

    private $enableMembersObject;

    private $mock;

    public function setUp()
    {
        $this->enableMembersObject = new Member();
    }

    public function tearDown()
    {
        unset($this->enableMembersObject);
    }

    /**
     * @Given: 存在需要启用的用户
     */
    protected function prepareData()
    {
        $disableData = $this->getMemberStatusResponse(Member::STATUS['DISABLED']);
        $jsonData = json_encode($disableData);

        $enableData = $this->getMemberStatusResponse(Member::STATUS['ENABLED']);

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], json_encode($enableData))
            ]
        );

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要启用的用户
     */
    protected function fetchMember($id)
    {
        $adapter = new MemberRestfulAdapter();
        $this->enableMembersObject = $adapter->fetchOne($id);

        return $this->enableMembersObject;
    }

    /**
     * @And:当我调用启用函数,期待启用成功
     */
    protected function enable()
    {
        return $this->enableMembersObject->enable();
    }

    /**
     * @Then  我可以查到该条新闻已被启用
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();

        $this->fetchMember($id);
        $this->enable();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/members/3/enable';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();

        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getMemberStatusRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Member::STATUS['ENABLED'], $this->enableMembersObject->getStatus());

        $this->assertEquals(
            1621847560,
            $this->enableMembersObject->getStatusTime()
        );
        $this->assertEquals(
            1621847560,
            $this->enableMembersObject->getUpdateTime()
        );
    }
}
