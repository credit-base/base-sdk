<?php
namespace Base\Sdk\Member\Browse;

use Base\Sdk\Member\Adapter\Member\MemberRestfulAdapter;
use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\ArrayDataTrait;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DetailTest extends TestCase
{
    use  ArrayDataTrait;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new Member();
    }

    public function tearDown()
    {
        unset($this->member);
    }

    /**
     * @Given: 存在一条用户数据
     */
    protected function prepareData()
    {
        $data = $this->getMembersCommonData();

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
                new Response(
                    200,
                    ['Content-Type' => 'application/vnd.api+json'],
                    $jsonData
                ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看该条用户数据详情时
     */
    protected function fetchMember($id)
    {

        $adapter = new MemberRestfulAdapter();

        $this->member = $adapter->fetchOne($id);

        return $this->member;
    }

    /**
     * @Then 我可以看见用户详情, 名字为发展和该和委员会, 简称为发改委, 创建时间, 更新时间.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchMember($id);

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'members/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('18800000000', $this->member->getUserName());
        $this->assertEquals('张科', $this->member->getRealName());
        $this->assertEquals(Member::STATUS['ENABLED'], $this->member->getStatus());
        $this->assertEquals(1621847602, $this->member->getStatusTime());
        $this->assertEquals(1516174523, $this->member->getCreateTime());
        $this->assertEquals(1623134985, $this->member->getUpdateTime());
    }
}
