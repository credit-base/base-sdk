<?php


namespace Base\Sdk\Member;

use Base\Sdk\Common\Model\IApplyCategory;
use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Member\Adapter\Member\MemberRestfulAdapter;
use Base\Sdk\Member\Model\Member;

trait ArrayDataTrait
{
    protected function getMemberList(array $filter = [])
    {
        $adapter = new MemberRestfulAdapter();

        list($count, $memberList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $memberList;
    }

    protected function getMembersCommonData(): array
    {
        return [
            "meta" => [],
            "data" => [
                "type" => "members",
                "id" => "1",
                "attributes" => [
                    "userName" => "18800000000",
                    "realName" => "张科",
                    "cellphone" => "18800000000",
                    "email" => "997934301@qq.com",
                    "cardId" => "412825199009094538",
                    "contactAddress" => "雁塔区",
                    "securityQuestion" => 1,
                    "gender" => 2,
                    "status" => 0,
                    "createTime" => 1516174523,
                    "updateTime" => 1623134985,
                    "statusTime" => 1621847602
                ],
                "links" => [
                    "self" => "api.base.qixinyun.com/members/1"
                ]
            ]
        ];
    }

    protected function getMemberListData(): array
    {
        return [
            "meta" => [
                "count" => 30,
                "links" => [
                    "first" => 1,
                    "last" => 15,
                    "prev" => null,
                    "next" => 2
                ]
            ],
            "links" => [
                "first" => "api.base.qixinyun.com/members/?sort=-updateTime&page[number]=1&page[size]=2",
                "last" => "api.base.qixinyun.com/members/?sort=-updateTime&page[number]=15&page[size]=2",
                "prev" => null,
                "next" => "api.base.qixinyun.com/members/?sort=-updateTime&page[number]=2&page[size]=2"
            ],
            "data" => [
                [
                    "type" => "members",
                    "id" => "135",
                    "attributes" => [
                        "userName" => "测试",
                        "realName" => "刘欣",
                        "cellphone" => "15002980525",
                        "email" => "102510@qq.com",
                        "cardId" => "612301199511180525",
                        "contactAddress" => "延平门",
                        "securityQuestion" => 5,
                        "gender" => 0,
                        "status" => 0,
                        "createTime" => 1626274306,
                        "updateTime" => 1626274306,
                        "statusTime" => 0
                    ],
                    "links" => [
                        "self" => "api.base.qixinyun.com/members/135"
                    ]
                ],
                [
                    "type" => "members",
                    "id" => "4",
                    "attributes" => [
                        "userName" => "18800000003",
                        "realName" => "钱森森",
                        "cellphone" => "18800000003",
                        "email" => "997934337@qq.com",
                        "cardId" => "412825199009094533",
                        "contactAddress" => "莲湖区",
                        "securityQuestion" => 3,
                        "gender" => 2,
                        "status" => 0,
                        "createTime" => 1516174523,
                        "updateTime" => 1625034475,
                        "statusTime" => 1625034475
                    ],
                    "links" => [
                        "self" => "api.base.qixinyun.com/members/4"
                    ]
                ]
            ]
        ];
    }

    protected function getMemberDetailData(
        int $id = 0,
        int $status = Member::STATUS['ENABLED'],
        int $gender = Member::GENDER['GENDER_MALE']
    ): array {
        return [
            "meta" => [],
            "data" => [
                "type" => "members",
                "id" => $id,
                "attributes" => [
                    "userName" => "用户名名",
                    "realName" => "用户名名",
                    "cardId" => "12233334444",
                    "cellphone" => "13233334444",
                    "email" => "email",
                    "contactAddress" => "12233334444",
                    "securityQuestion" => 0,
                    "securityAnswer" => "12233334444",
                    "gender" => $gender,
                    "status" => $status,
                    "createTime" => 1516174523,
                    "updateTime" => 1623134985,
                    "statusTime" => 1621847602
                ],
                "links" => [
                    "self" => "api.base.qixinyun.com/members/1"
                ]
            ]
        ];
    }

    protected function getMemberAddRequest() : array
    {
        return array(
            'data' => array(
                "type" => "members",
                "attributes" => array(
                    "userName" => "用户名名",
                    "realName" => "用户名名",
                    "cellphone" => "13233334444",
                    "email" => "email",
                    "cardId" => "12233334444",
                    "contactAddress" => "12233334444",
                    "securityQuestion" => 0,
                    "securityAnswer" => "12233334444",
                    "password" => "12233334444"
                )
            )
        );
    }

    //启用禁用
    protected function getMemberStatusResponse($status = Member::STATUS['ENABLED']):array
    {
        $data = $this->getMemberCommonResponse();
        $data['data']['attributes']['status'] = $status;

        return $data;
    }

    protected function getMemberCommonResponse() : array
    {
        return [
            "meta" =>[],
            "data" => [
                "type" =>"members",
                "id" =>"3",
                "attributes" => [
                    "userName" =>"18800000002",
                    "realName" =>"赵伟",
                    "cellphone" =>"18800000002",
                    "email" =>"997934303@qq.com",
                    "cardId" =>"412825199009094532",
                    "contactAddress" =>"长安区",
                    "securityQuestion" =>2,
                    "gender" =>1,
                    "status" =>0,
                    "createTime" =>1516174523,
                    "updateTime" =>1621847560,
                    "statusTime" =>1621847560
                ],
                "links" => [
                    "self" =>"api.base.qixinyun.com/members/3"
                ]
            ]
        ];
    }

    protected function getMemberStatusRequest() : array
    {
        return array();
        return [
            "data"=>[
                "type"=>"members",
                "attributes"=>[]
            ]
        ];
    }
}
