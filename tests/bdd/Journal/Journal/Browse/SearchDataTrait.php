<?php
namespace Base\Sdk\Journal\Journal\Browse;

use Base\Sdk\Journal\Adapter\Journal\JournalRestfulAdapter;

trait SearchDataTrait
{
    protected function getJournalList(array $filter = [])
    {
        $adapter = new JournalRestfulAdapter();

        list($count, $journalList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
        
        return $journalList;
    }
}
