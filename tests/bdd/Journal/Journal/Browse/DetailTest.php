<?php
namespace Base\Sdk\Journal\Journal\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Adapter\Journal\JournalRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Journal\ArrayDataTrait;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 *           我拥有信用刊物管理权限、且当我需要查看通过审核的信用刊物的列表时,
 *           在发布表中,管理发布表中的信用刊物,通过列表与详情的形式查看到已发布的信用刊物信息, 以便于我维护信用刊物模块
 * @Scenario: 查看信用刊物数据详情
 */

class DetailTest extends TestCase
{
    use ArrayDataTrait;

    private $journal;

    private $mock;

    public function setUp()
    {
        $this->journal = new Journal();
    }

    public function tearDown()
    {
        unset($this->journal);
    }

    /**
    * @Given: 存在一条信用刊物数据
    */
    protected function prepareData()
    {
        $data = $this->getJournalDetailData(1);

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条信用刊物数据详情时
    */
    protected function fetchJournal($id)
    {

        $adapter = new JournalRestfulAdapter();

        $this->journal = $adapter->fetchOne($id);
      
        return $this->journal;
    }

    /**
     * @Then  我可以看见该条信用刊物数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchJournal($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'journals/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            [
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.jpg"
            ],
            [
                "name"=> "诚信中国年2021刊物1",
                "identify"=> "诚信中国年2021刊物1.jpg"
            ]
            ],
            $this->journal->getAuthImages()
        );
        $this->assertEquals(Journal::STATUS['ENABLED'], $this->journal->getStatus());

        $this->assertEquals('诚信中国年2021刊物', $this->journal->getTitle());
        $this->assertEquals(
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.jpg"
            ],
            $this->journal->getCover()
        );
        $this->assertEquals('信用中国官网', $this->journal->getSource());

        $this->assertEquals(2021, $this->journal->getYear());
        $this->assertEquals(
            '诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介',
            $this->journal->getDescription()
        );
      
        $this->assertEquals(
            [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.pdf"
            ],
            $this->journal->getAttachment()
        );

        $this->assertEquals(
            1621997357,
            $this->journal->getStatusTime()
        );
        $this->assertEquals(
            1621997069,
            $this->journal->getCreateTime()
        );
        $this->assertEquals(
            1621997553,
            $this->journal->getUpdateTime()
        );
    }
}
