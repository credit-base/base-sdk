<?php
namespace Base\Sdk\Journal\Journal\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\Journal;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Journal\ArrayDataTrait;

/**
 * @Feature:  我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,我拥有信用刊物管理权限、且当我需要查看通过审核的信用刊物的列表时,
 *           在发布表中,管理发布表中的信用刊物,通过列表与详情的形式查看到已发布的信用刊物信息, 以便于我维护信用刊物模块
 * @Scenario: 查看信用刊物列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ArrayDataTrait;

    private $journal;

    private $mock;

    public function setUp()
    {
        $this->journal = new Journal();
    }

    public function tearDown()
    {
        unset($this->journal);
    }

    /**
    * @Given: 存在信用刊物数据
    */
    protected function prepareData()
    {
        $data = $this->getJournalListData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看信用刊物列表时
     */
    public function fetchJournalList()
    {
        $filter = [];
        $list =$this->getJournalList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见信用刊物数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchJournalList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'journals';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $journalList = $this->fetchJournalList();

        $journalArray = $this->getJournalListData()['data'];
        
        foreach ($journalList as $object) {
            foreach ($journalArray as $item) {
                if ($object->getId() == $item['id']) {
                    $this->assertEquals($item['attributes']['title'], $object->getTitle());
                    $this->assertEquals($item['attributes']['source'], $object->getSource());
                    $this->assertEquals($item['attributes']['description'], $object->getDescription());

                    $this->assertEquals($item['attributes']['year'], $object->getYear());
                    $this->assertEquals(
                        $item['attributes']['authImages'],
                        $object->getAuthImages()
                    );
            
                    $this->assertEquals(
                        $item['attributes']['attachment'],
                        $object->getAttachment()
                    );
                    $this->assertEquals(
                        $item['attributes']['cover'],
                        $object->getCover()
                    );

                    $this->assertEquals($item['attributes']['status'], $object->getStatus());
                    $this->assertEquals(
                        $item['relationships']['crew']['data']['id'],
                        $object->getCrew()->getId()
                    );

                    $this->assertEquals(
                        $item['attributes']['statusTime'],
                        $object->getStatusTime()
                    );
                    $this->assertEquals(
                        $item['attributes']['createTime'],
                        $object->getCreateTime()
                    );
                    $this->assertEquals(
                        $item['attributes']['updateTime'],
                        $object->getUpdateTime()
                    );
                }
            }
        }
    }
}
