<?php
namespace Base\Sdk\Journal\Journal\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Adapter\Journal\JournalRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 *           我拥有信用刊物管理权限、且当我需要查看通过审核的信用刊物的列表时,
 *           在发布表中,管理发布表中的信用刊物,通过列表与详情的形式查看到已发布的信用刊物信息, 以便于我维护信用刊物模块
 * @Scenario: 查看信用刊物数据列表-异常不存在数据
 */

class FailEmptyTest extends TestCase
{
    private $journal;

    private $mock;

    public function setUp()
    {
        $this->journal = new Journal();
    }

    public function tearDown()
    {
        unset($this->journal);
    }

    /**
    * @Given: 不存在新闻数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看新闻列表时
     */
    public function fetchJournalList()
    {
        $adapter = new JournalRestfulAdapter();

        list($count, $this->journal) = $adapter
        ->search();
        unset($count);

        return $this->journal;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchJournalList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'journals';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->journal);
    }
}
