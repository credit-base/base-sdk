<?php
namespace Base\Sdk\Journal\Journal\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Adapter\Journal\JournalRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Journal\ArrayDataTrait;

/**
 * @Feature: 我是拥有信用刊物发布权限的平台管理员/委办局管理员/操作用户,
 *           当我需要修改某个特定信用刊物禁用时,在发布表的操作栏中,将信用刊物修改为禁用状态
 *           通过发布表的操作栏中的禁用操作,以便于我可以更好的维护信用刊物发布管理列表
 * @Scenario: 禁用信用刊物
 */

class DisableTest extends TestCase
{
    use ArrayDataTrait;

    private $journal;

    private $mock;

    public function setUp()
    {
        $this->journal = new Journal();
    }

    public function tearDown()
    {
        unset($this->journal);
    }

    /**
    * @Given: 存在需要禁用的信用刊物
    */
    protected function prepareData()
    {
        $enableData = $this->getJournalDetailData(1, Journal::STATUS['ENABLED']);
        $jsonData = json_encode($enableData);
        
        $disableData = $this->getJournalDetailData(1, Journal::STATUS['DISABLED']);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($disableData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要禁用的信用刊物
    */
    protected function fetchJournal($id)
    {
        $adapter = new JournalRestfulAdapter();
        $this->journal = $adapter->fetchOne($id);
        
        return $this->journal;
    }

    /**
    * @And:当我调用禁用函数,期待禁用成功
    */
    protected function disable()
    {
       
        $crew = new Crew(1);
         $this->journal->setCrew($crew);
     
        return  $this->journal->disable();
    }
    /**
     * @Then  我可以查到该条信用刊物已被禁用
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchJournal($id);
        $this->disable();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/journals/1/disable';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getJournalStatusRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Journal::STATUS['DISABLED'], $this->journal->getStatus());

        $this->assertEquals(
            1621997357,
            $this->journal->getStatusTime()
        );
        $this->assertEquals(
            1621997553,
            $this->journal->getUpdateTime()
        );
    }
}
