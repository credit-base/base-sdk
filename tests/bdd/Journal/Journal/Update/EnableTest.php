<?php
namespace Base\Sdk\Journal\Journal\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Adapter\Journal\JournalRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Journal\ArrayDataTrait;

/**
 * @Feature: 我是拥有信用刊物发布权限的平台管理员/委办局管理员/操作用户,当我需要修改某个特定信用刊物启用时,在发布表的操作栏中,将信用刊物修改为启用状态
 *           通过发布表的操作栏中的启用操作,以便于我可以更好的维护信用刊物发布管理列表
 * @Scenario: 启用信用刊物
 */

class EnableTest extends TestCase
{
    use ArrayDataTrait;

    private $enableJournalObject;

    private $mock;

    public function setUp()
    {
        $this->enableJournalObject = new Journal();
    }

    public function tearDown()
    {
        unset($this->enableJournalObject);
    }

    /**
    * @Given: 存在需要启用的信用刊物
    */
    protected function prepareData()
    {
        $disableData = $this->getJournalDetailData(1, Journal::STATUS['DISABLED']);
        $jsonData = json_encode($disableData);
        
        $enableData = $this->getJournalDetailData(1, Journal::STATUS['ENABLED']);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($enableData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要启用的信用刊物
    */
    protected function fetchJournal($id)
    {
        $adapter = new JournalRestfulAdapter();
        $this->enableJournalObject = $adapter->fetchOne($id);
        
        return $this->enableJournalObject;
    }

    /**
    * @And:当我调用启用函数,期待启用成功
    */
    protected function enable()
    {
       
        $crew = new Crew(1);
        $this->enableJournalObject->setCrew($crew);
     
        return $this->enableJournalObject->enable();
    }
    /**
     * @Then  我可以查到该条信用刊物已被启用
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchJournal($id);
        $this->enable();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/journals/1/enable';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getJournalStatusRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(Journal::STATUS['ENABLED'], $this->enableJournalObject->getStatus());

        $this->assertEquals(
            1621997357,
            $this->enableJournalObject->getStatusTime()
        );
        $this->assertEquals(
            1621997553,
            $this->enableJournalObject->getUpdateTime()
        );
    }
}
