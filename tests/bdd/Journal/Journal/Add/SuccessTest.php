<?php
namespace Base\Sdk\Journal\Journal\Add;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\Journal;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Journal\ArrayDataTrait;

/**
 * @Feature: 我是拥有信用刊物管理权限的平台管理员/委办局管理员/操作用户,当我需要新增一个信用刊物时,在发布表中,新增对应的信用刊物数据
 *           通过新增信用刊物界面，并根据我所采集的信用刊物数据进行新增,以便于我维护信用刊物列表
 * @Scenario: 正常新增数据
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $journal;

    private $mock;

    public function setUp()
    {
        $this->journal = new Journal();
    }

    public function tearDown()
    {
        unset($this->journal);
    }

    /**
    * @Given: 我并未新增过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getJournalDetailData(1);
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
       
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我调用添加函数,期待返回true
    */
    protected function add()
    {
        $this->journal = new Journal();
        $this->journal->setTitle('诚信中国年2021刊物');
     
        $crew = new Crew(1);
        $this->journal->setCrew($crew);
        $this->journal->setDescription("诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介");
        $this->journal->setYear(2021);
        $this->journal->setAuthImages(
            [
                [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.jpg"
                ],
                [
                    "name"=> "诚信中国年2021刊物1",
                    "identify"=> "诚信中国年2021刊物1.jpg"
                ]
            ]
        );
        $this->journal->setAttachment(
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.pdf"
            ]
        );
        $this->journal->setCover(
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.jpg"
            ]
        );
        $this->journal->setSource('信用中国官网');
        $this->journal->setStatus(0);
      
        return  $this->journal->add();
    }
    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        
        $this->add();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/journals';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getJournalOperationRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('诚信中国年2021刊物', $this->journal->getTitle());
        $this->assertEquals('信用中国官网', $this->journal->getSource());
        $this->assertEquals('诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介', $this->journal->getDescription());

        $this->assertEquals(2021, $this->journal->getYear());
        $this->assertEquals(
            [
                [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.jpg"
                ],
                [
                    "name"=> "诚信中国年2021刊物1",
                    "identify"=> "诚信中国年2021刊物1.jpg"
                ]
            ],
            $this->journal->getAuthImages()
        );
   
        $this->assertEquals(
            [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.pdf"
            ],
            $this->journal->getAttachment()
        );
        $this->assertEquals(
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.jpg"
            ],
            $this->journal->getCover()
        );

        $this->assertEquals(Journal::STATUS['ENABLED'], $this->journal->getStatus());

        $this->assertEquals(
            0,
            $this->journal->getStatusTime()
        );
        $this->assertEquals(
            1621997069,
            $this->journal->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->journal->getUpdateTime()
        );
    }
}
