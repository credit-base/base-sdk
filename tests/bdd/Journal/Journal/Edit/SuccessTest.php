<?php
namespace Base\Sdk\Journal\Journal\Edit;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Adapter\Journal\JournalRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Journal\ArrayDataTrait;

/**
 * @Feature: 我是拥有信用刊物管理权限的平台管理员/委办局管理员/操作用户,当我需要编辑信用刊物信息时,在发布表中,编辑对应信用刊物数据
 *           通过编辑信用刊物界面，并根据我所采集的信用刊物数据进行编辑,以便于我可以更好的维护信用刊物管理列表
 * @Scenario: 正常编辑数据
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $editJournalObject;

    private $mock;

    public function setUp()
    {
        $this->editJournalObject = new Journal();
    }

    public function tearDown()
    {
        unset($this->editJournalObject);
    }

    /**
    * @Given: 我并未编辑过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getJournalOtherDetailData(1);

        $jsonData = json_encode($data);
        

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($this->getJournalDetailData(1))
               )
            ]
        );
      
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要编辑的信用刊物数据
    */
    protected function fetchJournal($id)
    {
        $adapter = new JournalRestfulAdapter();
        $this->editJournalObject = $adapter->fetchOne($id);
        
        return $this->editJournalObject;
    }

    /**
    * @When:当我调用编辑函数,期待编辑成功
    */
    protected function edit()
    {
        $this->editJournalObject->setTitle('诚信中国年2021刊物');
     
        $crew = new Crew(1);
        $this->editJournalObject->setCrew($crew);
        $this->editJournalObject->setDescription("诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介");
        $this->editJournalObject->setYear(2021);
        $this->editJournalObject->setAuthImages(
            [
                [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.jpg"
                ],
                [
                    "name"=> "诚信中国年2021刊物1",
                    "identify"=> "诚信中国年2021刊物1.jpg"
                ]
            ]
        );
        $this->editJournalObject->setAttachment(
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.pdf"
            ]
        );
        $this->editJournalObject->setCover(
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.jpg"
            ]
        );
        $this->editJournalObject->setSource('信用中国官网');
        $this->editJournalObject->setStatus(0);
       
        return $this->editJournalObject->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchJournal($id);

        $this->edit();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/journals/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getJournalOperationRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('诚信中国年2021刊物', $this->editJournalObject->getTitle());
        $this->assertEquals('信用中国官网', $this->editJournalObject->getSource());
        $this->assertEquals(
            '诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介',
            $this->editJournalObject->getDescription()
        );

        $this->assertEquals(2021, $this->editJournalObject->getYear());
        $this->assertEquals(
            [
                [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.jpg"
                ],
                [
                    "name"=> "诚信中国年2021刊物1",
                    "identify"=> "诚信中国年2021刊物1.jpg"
                ]
            ],
            $this->editJournalObject->getAuthImages()
        );
   
        $this->assertEquals(
            [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.pdf"
            ],
            $this->editJournalObject->getAttachment()
        );
        $this->assertEquals(
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.jpg"
            ],
            $this->editJournalObject->getCover()
        );

        $this->assertEquals(Journal::STATUS['ENABLED'], $this->editJournalObject->getStatus());
        $this->assertEquals(1, $this->editJournalObject->getCrew()->getId());
        $this->assertEquals(1, $this->editJournalObject->getUserGroup()->getId());

        $this->assertEquals(
            1621997357,
            $this->editJournalObject->getStatusTime()
        );
        $this->assertEquals(
            1621997069,
            $this->editJournalObject->getCreateTime()
        );
        $this->assertEquals(
            1621997553,
            $this->editJournalObject->getUpdateTime()
        );
    }
}
