<?php
namespace Base\Sdk\Journal;

use Base\Sdk\Journal\Model\Journal;

use Base\Sdk\Common\Model\IApplyAble;

trait ArrayDataTrait
{
    public function getJournalDetailData(
        int $id,
        int $status = Journal::STATUS['ENABLED']
    ):array {
        $data = $this->getJournalOperationRequest($status);
        $data['data']['id'] = $id;
        $data['data']['links'] = [
            "self"=> "127.0.0.1:8089/journals/1"
        ];
        $data['data']['attributes']['createTime'] = 1621997069;
        $data['data']['attributes']['updateTime'] = 1621997553;
        $data['data']['attributes']['statusTime'] = 1621997357;
        $data['data']['relationships'] = $this->getCommonRelationshipsData();
        $data['included'] = $this->getCommonIncludedData();
        return $data;
    }

    protected function getJournalListData():array
    {
        $list = [
            "meta"=> [
                "count"=> 2,
                "links"=> [
                    "first"=> null,
                    "last"=> null,
                    "prev"=> null,
                    "next"=> null
                ]
            ],
            "links"=> [
                "first"=> null,
                "last"=> null,
                "prev"=> null,
                "next"=> null
            ]
        ];
        $list['data'][0] = $this->getJournalDetailData(1)['data'];
        $list['data'][1] = $this->getJournalOtherDetailData(2)['data'];

        $list['included'] = $this->getCommonIncludedData();
       
        return $list;
    }

    protected function getJournalStatusRequest() : array
    {
        return [
            "data"=>[
                "type"=>"journals",
                "attributes"=>[],
                "relationships"=> [
                    "crew"=> [
                        "data"=>[
                            [
                                "type"=> "crews",
                                "id"=> 1
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getJournalOperationRequest(int $status = Journal::STATUS['ENABLED']):array
    {
        return [
            "data"=> [
                "type"=> "journals",
                "attributes"=> [
                    "title"=> "诚信中国年2021刊物",
                    "source"=> "信用中国官网",
                    "attachment"=> [
                        "name"=> "诚信中国年2021刊物",
                        "identify"=> "诚信中国年2021刊物.pdf"
                    ],
                    "status"=> $status,
                    "year"=> 2021,
                    "description"=> "诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介",
                    "authImages"=> [
                        [
                            "name"=> "诚信中国年2021刊物",
                            "identify"=> "诚信中国年2021刊物.jpg"
                        ],
                        [
                            "name"=> "诚信中国年2021刊物1",
                            "identify"=> "诚信中国年2021刊物1.jpg"
                        ]
                    ],
                    "cover"=> [
                        "name"=> "诚信中国年2021刊物",
                        "identify"=> "诚信中国年2021刊物.jpg"
                    ]
                ],
                "relationships"=> [
                    "crew"=> [
                        "data"=> [
                            [
                                "type"=> "crews",
                                "id"=> 1
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getCommonRelationshipsData():array
    {
        return [
            "publishUserGroup"=> [
                "data"=> [
                    "type"=> "userGroups",
                    "id"=> "1"
                ]
            ],
            "crew"=> [
                "data"=> [
                    "type"=> "crews",
                    "id"=> "1"
                ]
            ]
        ];
    }

    protected function getCommonIncludedData():array
    {
        return [
            [
                "type"=> "userGroups",
                "id"=> "2",
                "attributes"=> [
                    "name"=> "宜昌市发展和改革委员会",
                    "shortName"=> "发改委",
                    "status"=> 0,
                    "createTime"=> 1516165692,
                    "updateTime"=> 1516164782,
                    "statusTime"=> 0
                ]
            ],
            [
                "type"=> "crews",
                "id"=> "1",
                "attributes"=> [
                    "realName"=> "张小南",
                    "cardId"=> "623485199009094532",
                    "userName"=> "18800009964",
                    "cellphone"=> "1880009964",
                    "category"=> 1,
                    "purview"=> [
                        "1",
                        "2",
                        "3"
                    ],
                    "status"=> 0,
                    "createTime"=> 1618246776,
                    "updateTime"=> 1616594799,
                    "statusTime"=> 0
                ],
                "relationships"=> [
                    "userGroup"=> [
                        "data"=> [
                            "type"=> "userGroups",
                            "id"=> "2"
                        ]
                    ],
                    "department"=> [
                        "data"=> [
                            "type"=> "departments",
                            "id"=> "5"
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getJournalOtherDetailData($id = 1):array
    {
        return [
            "data"=>[
                "type"=> "journals",
                "id"=> $id,
                "attributes"=> [
                    "title"=> "信用刊物标题",
                    "source"=> "来源",
                    "description"=> "简介",
                    "cover"=> [
                        "name"=> "封面名称",
                        "identify"=> "封面地址.jpg"
                    ],
                    "attachment"=> [
                        "name"=> "附件名称",
                        "identify"=> "附件地址.pdf"
                    ],
                    "authImages"=> [
                        [
                            "name"=> "name",
                            "identify"=> "identify.jpg"
                        ],
                        [
                            "name"=> "name",
                            "identify"=> "identify.jpg"
                        ],
                        [
                            "name"=> "name",
                            "identify"=> "identify.jpg"
                        ]
                    ],
                    "year"=> 2021,
                    "status"=> -2,
                    "createTime"=> 1621996436,
                    "updateTime"=> 1621997649,
                    "statusTime"=> 1621997649
                ],
                "relationships"=> [
                    "publishUserGroup"=> [
                        "data"=> [
                            "type"=> "userGroups",
                            "id"=> "1"
                        ]
                    ],
                    "crew"=> [
                        "data"=> [
                            "type"=> "crews",
                            "id"=> "1"
                        ]
                    ]
                ],
                "links"=> [
                    "self"=> "127.0.0.1:8089/journals/1"
                ]
            ]
        ];
    }

    protected function getUnAuditJournalDetailData(
        $id,
        $applyStatus = IApplyAble::APPLY_STATUS['PENDING']
    ):array {
        $data = $this->getJournalDetailData($id);
        $data = $this->getUnAuditJournalCommonDetailData($data, $applyStatus);

        return $data;
    }

    protected function getUnAuditJournalCommonDetailData(
        $data,
        $applyStatus = IApplyAble::APPLY_STATUS['PENDING']
    ):array {
        $data['data']['type'] = "unAuditedJournals";
        $data['data']['attributes']['applyInfoCategory'] = 2;
        $data['data']['attributes']['applyInfoType'] = 1;
        $data['data']['attributes']['applyStatus'] = $applyStatus;
        $data['data']['attributes']['rejectReason'] = '驳回原因';
        $data['data']['attributes']['operationType'] = 2;
        $data['data']['attributes']['journalId'] = 2;
        $data['data']['links'] = [
            "self"=> "127.0.0.1:8089/unAuditedJournals/1"
        ];
        $relationships = $this->getUnAuditJournalRelationshipsData();
        $data['data']['relationships'] = array_merge($this->getCommonRelationshipsData(), $relationships);

        return $data;
    }

    protected function getUnAuditJournalRelationshipsData():array
    {
        return [
            "applyUserGroup"=> [
                "data"=> [
                    "type"=> "userGroups",
                    "id"=> "1"
                ]
            ],
            "applyCrew"=> [
                "data"=> [
                    "type"=> "crews",
                    "id"=> "1"
                ]
            ],
            "relation"=> [
                "data"=> [
                    "type"=> "crews",
                    "id"=> "1"
                ]
            ]
        ];
    }

    protected function getUnAuditJournalOtherDetailData(
        $id = 1,
        $applyStatus = IApplyAble::APPLY_STATUS['PENDING']
    ):array {
        $data = $this->getJournalOtherDetailData($id);
        $data = $this->getUnAuditJournalCommonDetailData($data, $applyStatus);

        return $data;
    }

    protected function getUnAuditJournalStatusRequest():array
    {
        return [
            "data"=>[
                "type"=>"unAuditedJournals",
                "attributes"=>[],
                "relationships"=> [
                    "applyCrew"=> [
                        "data"=>[
                            [
                                "type"=> "crews",
                                "id"=> 1
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getUnAuditJournalListData():array
    {
        $data = $this->getJournalListData();
     
        $data['data'][0] = $this->getUnAuditJournalCommonData($data['data'][0]);

        $data['data'][1] = $this->getUnAuditJournalCommonData($data['data'][1]);

        $data['included'] = $this->getCommonIncludedData();

        return $data;
    }

    protected function getUnAuditJournalCommonData(
        $data,
        $applyStatus = IApplyAble::APPLY_STATUS['PENDING']
    ):array {
        $data['type'] = "unAuditedJournals";
        $data['attributes']['applyInfoCategory'] = 2;
        $data['attributes']['applyInfoType'] = 1;
        $data['attributes']['applyStatus'] = $applyStatus;
        $data['attributes']['rejectReason'] = '驳回原因';
        $data['attributes']['operationType'] = 2;
        $data['attributes']['journalId'] = 2;
        $data['links'] = [
            "self"=> "127.0.0.1:8089/unAuditedJournals/1"
        ];
        $relationships = $this->getUnAuditJournalRelationshipsData();
        $data['relationships'] = array_merge($this->getCommonRelationshipsData(), $relationships);

        return $data;
    }
}
