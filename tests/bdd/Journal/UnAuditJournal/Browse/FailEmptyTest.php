<?php
namespace Base\Sdk\Journal\UnAuditJournal\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Adapter\UnAuditJournal\UnAuditJournalRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 *           我拥有信用刊物管理权限、且当我需要查看未审核或已驳回的信用刊物列表时,
 *           在审核表中,查看审核表中的已驳回或是未审核的信息,
 *           通过列表与详情的形式查看到未审核或已驳回的信用刊物信息,以便于我维护信用刊物模块
 * @Scenario: 查看信用刊物审核数据列表-异常数据不存在
 */

class FailEmptyTest extends TestCase
{
    private $unAuditJournal;

    private $mock;

    public function setUp()
    {
        $this->unAuditJournal = new UnAuditJournal();
    }

    public function tearDown()
    {
        unset($this->unAuditJournal);
    }

    /**
    * @Given: 不存在信用刊物审核数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看信用刊物审核列表时
     */
    public function fetchUnAuditJournalList()
    {
        $adapter = new UnAuditJournalRestfulAdapter();

        list($count, $this->unAuditJournal) = $adapter
        ->search();
        unset($count);

        return $this->unAuditJournal;
    }

    /**
     * @Then: 获取不到审核数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchUnAuditJournalList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedJournals';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->unAuditJournal);
    }
}
