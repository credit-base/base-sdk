<?php
namespace Base\Sdk\Journal\UnAuditJournal\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\UnAuditJournal;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;


use Base\Sdk\Journal\ArrayDataTrait;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 *           我拥有信用刊物管理权限、且当我需要查看未审核或已驳回的信用刊物列表时,
 *           在审核表中,查看审核表中的已驳回或是未审核的信息,
 *           通过列表与详情的形式查看到未审核或已驳回的信用刊物信息,以便于我维护信用刊物模块
 * @Scenario: 查看信用刊物审核列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ArrayDataTrait;

    private $unAuditJournal;

    private $mock;

    public function setUp()
    {
        $this->unAuditJournal = new UnAuditJournal();
    }

    public function tearDown()
    {
        unset($this->unAuditJournal);
    }

    /**
    * @Given: 存在信用刊物数据
    */
    protected function prepareData()
    {
        $data = $this->getUnAuditJournalListData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看信用刊物列表时
     */
    public function fetchUnAuditJournalList()
    {
        $filter = [];

        $unAuditJournal = $this->getUnAuditJournalList($filter);

        return $unAuditJournal;
    }

    /**
     * @Then  我可以看见信用刊物数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchUnAuditJournalList();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedJournals';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $list = $this->fetchUnAuditJournalList();

        $array = $this->getUnAuditJournalListData()['data'];
       
        foreach ($list as $object) {
            foreach ($array as $data) {
                if ($object->getId() == $data['id']) {
                    $this->assertEquals($data['attributes']['title'], $object->getTitle());
                    $this->assertEquals($data['attributes']['source'], $object->getSource());
                    $this->assertEquals($data['attributes']['description'], $object->getDescription());

                    $this->assertEquals($data['attributes']['year'], $object->getYear());
                    $this->assertEquals(
                        $data['attributes']['authImages'],
                        $object->getAuthImages()
                    );
            
                    $this->assertEquals(
                        $data['attributes']['attachment'],
                        $object->getAttachment()
                    );
                    $this->assertEquals(
                        $data['attributes']['cover'],
                        $object->getCover()
                    );

                    $this->assertEquals($data['attributes']['status'], $object->getStatus());

                    $this->assertEquals(
                        $data['attributes']['applyInfoType'],
                        $object->getApplyInfoType()
                    );
                    $this->assertEquals(
                        $data['attributes']['applyStatus'],
                        $object->getApplyStatus()
                    );

                    $this->assertEquals(
                        $data['attributes']['statusTime'],
                        $object->getStatusTime()
                    );
                    $this->assertEquals(
                        $data['attributes']['createTime'],
                        $object->getCreateTime()
                    );
                    $this->assertEquals(
                        $data['attributes']['updateTime'],
                        $object->getUpdateTime()
                    );
                }
            }
        }
    }
}
