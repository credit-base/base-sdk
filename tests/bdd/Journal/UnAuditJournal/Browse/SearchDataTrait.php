<?php
namespace Base\Sdk\Journal\UnAuditJournal\Browse;

use Base\Sdk\Journal\Adapter\UnAuditJournal\UnAuditJournalRestfulAdapter;

trait SearchDataTrait
{
    protected function getUnAuditJournalList(array $filter)
    {
        $adapter = new UnAuditJournalRestfulAdapter();

        list($count, $unAuditJournalList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $unAuditJournalList;
    }
}
