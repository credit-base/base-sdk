<?php
namespace Base\Sdk\Journal\UnAuditJournal\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Adapter\UnAuditJournal\UnAuditJournalRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Journal\ArrayDataTrait;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户）,
 *           我拥有信用刊物管理权限、且当我需要查看未审核或已驳回的信用刊物列表时,
 *           在审核表中,查看审核表中的已驳回或是未审核的信息,
 *           通过列表与详情的形式查看到未审核或已驳回的信用刊物信息,以便于我维护信用刊物模块
 * @Scenario: 查看信用刊物审核数据详情
 */

class DetailTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditJournal;

    private $mock;

    public function setUp()
    {
        $this->unAuditJournal = new UnAuditJournal();
    }

    public function tearDown()
    {
        unset($this->unAuditJournal);
    }

    /**
    * @Given: 存在一条信用刊物审核数据
    */
    protected function prepareData()
    {
        $data = $this->getUnAuditJournalDetailData(1);

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条信用刊物数据审核详情时
    */
    protected function fetchUnAuditJournal($id)
    {

        $adapter = new UnAuditJournalRestfulAdapter();

        $this->unAuditJournal = $adapter->fetchOne($id);
      
        return $this->unAuditJournal;
    }

    /**
     * @Then  我可以看见该条信用刊物数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditJournal($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedJournals/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('诚信中国年2021刊物', $this->unAuditJournal->getTitle());
        $this->assertEquals('信用中国官网', $this->unAuditJournal->getSource());
        $this->assertEquals(1, $this->unAuditJournal->getApplyInfoType());
        $this->assertEquals(
            '诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介',
            $this->unAuditJournal->getDescription()
        );

        $this->assertEquals(2021, $this->unAuditJournal->getYear());
        $this->assertEquals(
            [
                [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.jpg"
                ],
                [
                    "name"=> "诚信中国年2021刊物1",
                    "identify"=> "诚信中国年2021刊物1.jpg"
                ]
            ],
            $this->unAuditJournal->getAuthImages()
        );
        $this->assertEquals(
            IApplyAble::APPLY_STATUS['PENDING'],
            $this->unAuditJournal->getApplyStatus()
        );
        $this->assertEquals(
            [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.pdf"
            ],
            $this->unAuditJournal->getAttachment()
        );
        $this->assertEquals(2, $this->unAuditJournal->getOperationType());
        $this->assertEquals(
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.jpg"
            ],
            $this->unAuditJournal->getCover()
        );

        $this->assertEquals(Journal::STATUS['ENABLED'], $this->unAuditJournal->getStatus());

        $this->assertEquals(
            1621997357,
            $this->unAuditJournal->getStatusTime()
        );
        $this->assertEquals(
            1621997069,
            $this->unAuditJournal->getCreateTime()
        );
        $this->assertEquals(
            1621997553,
            $this->unAuditJournal->getUpdateTime()
        );
    }
}
