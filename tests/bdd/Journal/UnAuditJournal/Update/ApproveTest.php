<?php
namespace Base\Sdk\Journal\UnAuditJournal\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Adapter\UnAuditJournal\UnAuditJournalRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Journal\ArrayDataTrait;

/**
 * @Feature: 我是拥有信用刊物审核权限的平台管理员/委办局管理员/操作用户,当我需要审核一个信用刊物时,在审核表中,审核待审核的信用刊物数据
 *           通过信用刊物详情页面的审核通过与审核驳回操作,以便于我维护信用刊物列表
 * @Scenario: 审核通过
 */

class ApproveTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditJournal;

    private $mock;

    public function setUp()
    {
        $this->unAuditJournal = new UnAuditJournal();
    }

    public function tearDown()
    {
        unset($this->unAuditJournal);
    }

    /**
    * @Given: 存在需要审核的信用刊物
    */
    protected function prepareData()
    {
        $data = $this->getUnAuditJournalDetailData(1);
        $jsonData = json_encode($data);
        
        $approveData = $this->getUnAuditJournalDetailData(
            1,
            IApplyAble::APPLY_STATUS['APPROVE']
        );

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($approveData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要审核的信用刊物
    */
    protected function fetchUnAuditJournal($id)
    {
        $adapter = new UnAuditJournalRestfulAdapter();
        $this->unAuditJournal = $adapter->fetchOne($id);
        
        return $this->unAuditJournal;
    }

    /**
    * @And:当我调用审核函数,期待审核成功
    */
    protected function approve()
    {
       
        $crew = new Crew(1);
        $this->unAuditJournal->setApplyCrew($crew);
     
        return $this->unAuditJournal->approve();
    }
    /**
     * @Then  我可以查到该条信用刊物已被审核
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditJournal($id);
        $this->approve();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedJournals/1/approve';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getUnAuditJournalStatusRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            IApplyAble::APPLY_STATUS['APPROVE'],
            $this->unAuditJournal->getApplyStatus()
        );
        $this->assertEquals(
            1,
            $this->unAuditJournal->getApplyCrew()->getId()
        );

        $this->assertEquals(
            1621997357,
            $this->unAuditJournal->getStatusTime()
        );
        $this->assertEquals(
            1621997553,
            $this->unAuditJournal->getUpdateTime()
        );
    }
}
