<?php
namespace Base\Sdk\Journal\UnAuditJournal\Edit;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Adapter\UnAuditJournal\UnAuditJournalRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use Base\Sdk\Common\Model\IApplyAble;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Journal\ArrayDataTrait;

/**
 * @Feature: 我是拥有信用刊物管理权限的平台管理员/委办局管理员/操作用户,当我需要编辑信用刊物信息时,在审核表中,编辑已驳回信用刊物数据
 *           通过编辑信用刊物界面，并根据我所采集的信用刊物数据进行编辑,以便于我可以更好的管理信用刊物
 * @Scenario: 正常编辑审核数据
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditJournal;

    private $mock;

    public function setUp()
    {
        $this->unAuditJournal = new UnAuditJournal();
    }

    public function tearDown()
    {
        unset($this->unAuditJournal);
    }

    /**
    * @Given: 我并未编辑过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getUnAuditJournalOtherDetailData(1, IApplyAble::APPLY_STATUS['REJECT']);

        $jsonData = json_encode($data);
       
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($this->getUnAuditJournalDetailData(1))
               )
            ]
        );
      
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要编辑的信用刊物数据
    */
    protected function fetchUnAuditJournal($id)
    {
        $adapter = new UnAuditJournalRestfulAdapter();
        $this->unAuditJournal = $adapter->fetchOne($id);
        
        return $this->unAuditJournal;
    }

    /**
    * @When:当我调用编辑函数,期待编辑成功
    */
    protected function edit()
    {
        $this->unAuditJournal->setTitle('诚信中国年2021刊物');
     
        $crew = new Crew(1);
        $this->unAuditJournal->setCrew($crew);
        $this->unAuditJournal->setYear(2021);
        $this->unAuditJournal->setDescription("诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介");
        $this->unAuditJournal->setAuthImages(
            [
                [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.jpg"
                ],
                [
                    "name"=> "诚信中国年2021刊物1",
                    "identify"=> "诚信中国年2021刊物1.jpg"
                ]
            ]
        );
        $this->unAuditJournal->setAttachment(
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.pdf"
            ]
        );
        $this->unAuditJournal->setCover(
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.jpg"
            ]
        );
        $this->unAuditJournal->setSource('信用中国官网');
        $this->unAuditJournal->setStatus(0);
       
        return $this->unAuditJournal->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditJournal($id);

        $this->edit();
    
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedJournals/1/resubmit';

        $request = $this->mock->getLastRequest();

        $requestEditData = $this->getJournalOperationRequest();
        $requestEditData['data']['type'] = "unAuditedJournals";

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($requestEditData), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('诚信中国年2021刊物', $this->unAuditJournal->getTitle());
        $this->assertEquals('信用中国官网', $this->unAuditJournal->getSource());
        
        $this->assertEquals(
            '诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物诚信中国年2021刊物简介',
            $this->unAuditJournal->getDescription()
        );

        $this->assertEquals(2021, $this->unAuditJournal->getYear());
        $this->assertEquals(
            [
                [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.jpg"
                ],
                [
                    "name"=> "诚信中国年2021刊物1",
                    "identify"=> "诚信中国年2021刊物1.jpg"
                ]
            ],
            $this->unAuditJournal->getAuthImages()
        );
 
        $this->assertEquals(
            [
                    "name"=> "诚信中国年2021刊物",
                    "identify"=> "诚信中国年2021刊物.pdf"
            ],
            $this->unAuditJournal->getAttachment()
        );
        $this->assertEquals(2, $this->unAuditJournal->getOperationType());
        $this->assertEquals(
            [
                "name"=> "诚信中国年2021刊物",
                "identify"=> "诚信中国年2021刊物.jpg"
            ],
            $this->unAuditJournal->getCover()
        );
        
        $this->assertEquals(1, $this->unAuditJournal->getApplyCrew()->getId());
        $this->assertEquals(1, $this->unAuditJournal->getApplyUserGroup()->getId());
        $this->assertEquals(1, $this->unAuditJournal->getApplyInfoType());
        $this->assertEquals(
            IApplyAble::APPLY_STATUS['PENDING'],
            $this->unAuditJournal->getApplyStatus()
        );
        $this->assertEquals(Journal::STATUS['ENABLED'], $this->unAuditJournal->getStatus());
        $this->assertEquals(
            1621997357,
            $this->unAuditJournal->getStatusTime()
        );
        $this->assertEquals(
            1621997069,
            $this->unAuditJournal->getCreateTime()
        );
        $this->assertEquals(
            1621997553,
            $this->unAuditJournal->getUpdateTime()
        );
    }
}
