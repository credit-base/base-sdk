<?php
namespace Base\Sdk\News\News\Add;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Translator\INews;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\News\ArrayDataTrait;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要新增一个新闻时,在发布表中,新增对应的新闻数据
 *           通过新增新闻界面，并根据我所采集的新闻数据进行新增,以便于我维护新闻列表
 * @Scenario: 正常新增数据数据
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $news;

    private $mock;

    public function setUp()
    {
        $this->news = new News();
    }

    public function tearDown()
    {
        unset($this->news);
    }

    /**
    * @Given: 我并未新增过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getNewsCommonResponse();
        $data['data']['attributes']['updateTime'] = 0;
        $data['data']['attributes']['statusTime'] = 0;

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
       
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我调用添加函数,期待返回true
    */
    protected function add()
    {
        $this->news = new News();
        $this->news->setTitle('新闻标题');
     
        $crew = new Crew(1);
        $this->news->setCrew($crew);
        $this->news->setDimension(2);
        $this->news->setNewsType(151);
        $this->news->setBannerImage(
            [
                "name"=> "轮播图图片名称",
                "identify"=> "轮播图图片地址.jpg"
            ]
        );
        $this->news->setContent('新闻内容');
        $this->news->setStick(2);
        $this->news->setAttachments(
            [
                [
                    "name"=> "附件名称",
                    "identify"=> "附件地址.doc"
                ]
            ]
        );
        $this->news->setCover(
            [
                "name"=> "封面名称",
                "identify"=> "封面地址.jpg"
            ]
        );
        $this->news->setSource('新闻来源');
        $this->news->setBannerStatus(2);
        $this->news->setHomePageShowStatus(2);
        $this->news->setStatus(0);
      
        return $this->news->add();
    }
    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        
        $this->add();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/news';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getNewsOperationRequestData()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('新闻标题', $this->news->getTitle());
        $this->assertEquals('新闻来源', $this->news->getSource());
        $this->assertEquals(News::STATUS['ENABLED'], $this->news->getStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->news->getBannerStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->news->getHomePageShowStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->news->getStick());

        $this->assertEquals(News::DIMENSION['GOVERNMENT_AFFAIRS'], $this->news->getDimension());
        $this->assertEquals(NEWS_TYPE['LEADING_GROUP'], $this->news->getNewsType());
        $this->assertEquals('新闻描述', $this->news->getDescription());
        $this->assertEquals(
            [
                "name"=> "轮播图图片名称",
                "identify"=> "轮播图图片地址.jpg"
            ],
            $this->news->getBannerImage()
        );
        $this->assertEquals('新闻内容', $this->news->getContent());
        $this->assertEquals(
            [
                [
                    "name"=> "附件名称",
                    "identify"=> "附件地址.doc"
                ]
            ],
            $this->news->getAttachments()
        );
        $this->assertEquals(
            [
                "name"=> "封面名称",
                "identify"=> "封面地址.jpg"
            ],
            $this->news->getCover()
        );
        $this->assertEquals(
            0,
            $this->news->getStatusTime()
        );
        $this->assertEquals(
            1620831782,
            $this->news->getCreateTime()
        );
        $this->assertEquals(
            0,
            $this->news->getUpdateTime()
        );
    }
}
