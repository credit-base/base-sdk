<?php
namespace Base\Sdk\News\News\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Adapter\News\NewsRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\News\ArrayDataTrait;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,
 *           当我需要修改某个特定新闻禁用时,在发布表的操作栏中,将新闻修改为禁用状态
 *           通过发布表的操作栏中的禁用操作,以便于我可以更好的维护新闻发布管理列表
 * @Scenario: 禁用新闻
 */

class DisableTest extends TestCase
{
    use ArrayDataTrait;

    private $news;

    private $mock;

    public function setUp()
    {
        $this->news = new News();
    }

    public function tearDown()
    {
        unset($this->news);
    }

    /**
    * @Given: 存在需要禁用的新闻
    */
    protected function prepareData()
    {
        $enableData = $this->getNewsStatusResponse(News::STATUS['ENABLED']);
        $jsonData = json_encode($enableData);
        
        $disableData = $this->getNewsStatusResponse(News::STATUS['DISABLED']);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($disableData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要禁用的新闻
    */
    protected function fetchNews($id)
    {
        $adapter = new NewsRestfulAdapter();
        $this->news = $adapter->fetchOne($id);
        
        return $this->news;
    }

    /**
    * @And:当我调用禁用函数,期待禁用成功
    */
    protected function disable()
    {
       
        $crew = new Crew(1);
        $this->news->setCrew($crew);
     
        return $this->news->disable();
    }
    /**
     * @Then  我可以查到该条新闻已被禁用
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchNews($id);
        $this->disable();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/news/1/disable';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getNewsStatusRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(News::STATUS['DISABLED'], $this->news->getStatus());

        $this->assertEquals(
            1620869215,
            $this->news->getStatusTime()
        );
        $this->assertEquals(
            1620872174,
            $this->news->getUpdateTime()
        );
    }
}
