<?php
namespace Base\Sdk\News\News\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Adapter\News\NewsRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\News\ArrayDataTrait;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,
 *           当我需要修改某个特定新闻取消置顶时,在发布表的操作栏中,将新闻修改为取消置顶状态
 *           通过发布表的操作栏中的取消置顶操作,以便于我可以更好的维护新闻发布管理列表
 * @Scenario: 取消置顶新闻
 */

class CancelTopTest extends TestCase
{
    use ArrayDataTrait;

    private $news;

    private $mock;

    public function setUp()
    {
        $this->news = new News();
    }

    public function tearDown()
    {
        unset($this->news);
    }

    /**
    * @Given: 存在需要取消置顶的新闻
    */
    protected function prepareData()
    {
        $topData = $this->getNewsStatusResponse(
            News::STATUS['ENABLED'],
            News::STICK['ENABLED']
        );
        
        $jsonData = json_encode($topData);
        
        $cancelTopData = $this->getNewsStatusResponse(
            News::STATUS['ENABLED'],
            News::STICK['DISABLED']
        );

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($cancelTopData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要取消置顶的新闻
    */
    protected function fetchNews($id)
    {
        $adapter = new NewsRestfulAdapter();
        $this->news = $adapter->fetchOne($id);
        
        return $this->news;
    }

    /**
    * @And:当我调用取消置顶函数,期待取消置顶成功
    */
    protected function cancelTop()
    {
       
        $crew = new Crew(1);
        $this->news->setCrew($crew);
     
        return $this->news->cancelTop();
    }
    /**
     * @Then  我可以查到该条新闻已被取消置顶
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchNews($id);
        $this->cancelTop();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/news/1/cancelTop';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getNewsStatusRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(News::STATUS['ENABLED'], $this->news->getStatus());
        $this->assertEquals(News::STICK['DISABLED'], $this->news->getStick());

        $this->assertEquals(
            1620869215,
            $this->news->getStatusTime()
        );
        $this->assertEquals(
            1620872174,
            $this->news->getUpdateTime()
        );
    }
}
