<?php
namespace Base\Sdk\News\News\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Adapter\News\NewsRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\News\ArrayDataTrait;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,
 *           当我需要修改某个特定新闻移动时,在发布表的操作栏中,将新闻修改为移动状态
 *           通过发布表的操作栏中的移动操作,以便于我可以更好的维护新闻发布管理列表
 * @Scenario: 移动新闻
 */

class MoveTest extends TestCase
{
    use ArrayDataTrait;

    private $news;

    private $mock;

    public function setUp()
    {
        $this->news = new News();
    }

    public function tearDown()
    {
        unset($this->news);
    }

    /**
    * @Given: 存在需要移动的新闻
    */
    protected function prepareData()
    {
        $oldData = $this->getNewsCommonResponse(1);
        
        $jsonData = json_encode($oldData);
        
        $newData = $this->getNewsCommonResponse(1);
        $newData['data']['attributes']['newsType'] = NEWS_TYPE['CREDIT_OFFICE'];

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($newData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要移动的新闻
    */
    protected function fetchNews($id)
    {
        $adapter = new NewsRestfulAdapter();
        $this->news = $adapter->fetchOne($id);
        
        return $this->news;
    }

    /**
    * @And:当我调用移动函数,期待移动成功
    */
    protected function move()
    {
       
        $crew = new Crew(1);
        $this->news->setCrew($crew);
        $this->news->setNewsType(NEWS_TYPE['CREDIT_OFFICE']);
     
        return $this->news->move();
    }
    /**
     * @Then  我可以查到该条新闻已被移动
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchNews($id);
        $this->move();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/news/1/move/'.$this->news->getNewsType();
        
        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
       
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getNewsStatusRequest()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(NEWS_TYPE['CREDIT_OFFICE'], $this->news->getNewsType());

        $this->assertEquals(
            1620869215,
            $this->news->getStatusTime()
        );
        $this->assertEquals(
            1620872174,
            $this->news->getUpdateTime()
        );
    }
}
