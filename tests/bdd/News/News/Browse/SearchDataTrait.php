<?php
namespace Base\Sdk\News\News\Browse;

use Base\Sdk\News\Adapter\News\NewsRestfulAdapter;

trait SearchDataTrait
{
    protected function getNewsList(array $filter = [])
    {
        $adapter = new NewsRestfulAdapter();

        list($count, $newsList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);
        
        return $newsList;
    }
}
