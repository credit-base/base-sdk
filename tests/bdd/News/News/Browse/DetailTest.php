<?php
namespace Base\Sdk\News\News\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Adapter\News\NewsRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\News\ArrayDataTrait;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),
 *           我拥有新闻发布权限、且当我需要查看通过审核的新闻的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻数据详情
 */

class DetailTest extends TestCase
{
    use ArrayDataTrait;

    private $news;

    private $mock;

    public function setUp()
    {
        $this->news = new News();
    }

    public function tearDown()
    {
        unset($this->news);
    }

    /**
    * @Given: 存在一条新闻数据
    */
    protected function prepareData()
    {
        $data = $this->getNewsCommonResponse(1);

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条新闻数据详情时
    */
    protected function fetchNews($id)
    {

        $adapter = new NewsRestfulAdapter();

        $this->news = $adapter->fetchOne($id);
      
        return $this->news;
    }

    /**
     * @Then  我可以看见该条新闻数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchNews($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'news/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $requestContents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $requestContents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('新闻标题', $this->news->getTitle());
        $this->assertEquals('新闻内容', $this->news->getContent());
        $this->assertEquals('新闻来源', $this->news->getSource());
        $this->assertEquals('新闻描述', $this->news->getDescription());
        
        $this->assertEquals(
            News::DIMENSION['GOVERNMENT_AFFAIRS'],
            $this->news->getDimension()
        );
        $this->assertEquals(NEWS_TYPE['LEADING_GROUP'], $this->news->getNewsType());
        $this->assertEquals(
            [
                "name"=> "轮播图图片名称",
                "identify"=> "轮播图图片地址.jpg"
            ],
            $this->news->getBannerImage()
        );
   
        $this->assertEquals(
            [
                [
                    "name"=> "附件名称",
                    "identify"=> "附件地址.doc"
                ]
            ],
            $this->news->getAttachments()
        );
        $this->assertEquals(
            [
                "name"=> "封面名称",
                "identify"=> "封面地址.jpg"
            ],
            $this->news->getCover()
        );

        $this->assertEquals(News::STATUS['ENABLED'], $this->news->getStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->news->getBannerStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->news->getHomePageShowStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->news->getStick());

        $this->assertEquals(
            1620869215,
            $this->news->getStatusTime()
        );
        $this->assertEquals(
            1620831782,
            $this->news->getCreateTime()
        );
        $this->assertEquals(
            1620872174,
            $this->news->getUpdateTime()
        );
    }
}
