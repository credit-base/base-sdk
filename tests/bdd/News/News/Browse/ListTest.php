<?php
namespace Base\Sdk\News\News\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\News\ArrayDataTrait;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有新闻发布权限、且当我需要查看通过审核的新闻的列表时,
 *           在发布表中,通过列表与详情的形式查看到已发布的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ArrayDataTrait;

    private $news;

    private $mock;

    public function setUp()
    {
        $this->news = new News();
    }

    public function tearDown()
    {
        unset($this->news);
    }

    /**
    * @Given: 存在新闻数据
    */
    protected function prepareData()
    {
        $newDetail = $this->getNewsOldData();

        $data = [
            "meta"=>[
                "count"=> 4,
                "links"=> [
                    "first"=> null,
                    "last"=> null,
                    "prev"=> null,
                    "next"=> null
                ]
            ],
            "links"=> [
                "first"=> null,
                "last"=> null,
                "prev"=> null,
                "next"=> null
            ]
        ];

        $data['data'] = [$newDetail['data']];

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看新闻列表时
     */
    public function fetchNewsList()
    {
        $filter = [];
        $list =$this->getNewsList($filter);
        
        return $list;
    }

    /**
     * @Then  我可以看见新闻数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchNewsList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'news';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $newsList = $this->fetchNewsList();
        
        foreach ($newsList as $val) {
            $this->assertEquals('测试新闻标题123', $val->getTitle());
            $this->assertEquals('测试新闻来源123', $val->getSource());
            $this->assertEquals(News::STATUS['ENABLED'], $val->getStatus());
            $this->assertEquals(News::STICK['ENABLED'], $val->getBannerStatus());
            $this->assertEquals(News::STICK['ENABLED'], $val->getHomePageShowStatus());
            $this->assertEquals(News::STICK['ENABLED'], $val->getStick());
    
            $this->assertEquals(News::DIMENSION['GOVERNMENT_AFFAIRS'], $val->getDimension());
            $this->assertEquals(NEWS_TYPE['LEADING_GROUP'], $val->getNewsType());
            $this->assertEquals('测试内容123', $val->getDescription());
            $this->assertEquals(
                [
                    "name"=> "测试轮播图图片名称123",
                    "identify"=> "测试轮播图图片地址123.jpg"
                ],
                $val->getBannerImage()
            );
            $this->assertEquals('测试内容123', $val->getContent());
            $this->assertEquals(
                [
                    [
                        "name"=> "测试name13",
                        "identify"=> "测试identify123.doc"
                    ]
                ],
                $val->getAttachments()
            );
            $this->assertEquals(
                [
                    "name"=> "测试封面名称123",
                    "identify"=> "测试封面地址123.jpg"
                ],
                $val->getCover()
            );
    
            $this->assertEquals(
                1620869215,
                $val->getStatusTime()
            );
            $this->assertEquals(
                1620831782,
                $val->getCreateTime()
            );
            $this->assertEquals(
                1620872174,
                $val->getUpdateTime()
            );
        }
    }
}
