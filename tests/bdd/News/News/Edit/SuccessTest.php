<?php
namespace Base\Sdk\News\News\Edit;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Adapter\News\NewsRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\News\ArrayDataTrait;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要编辑一个新闻时,在发布表中,编辑对应的新闻数据
 *           通过新增新闻界面，并根据我所采集的新闻数据进行新增,以便于我维护新闻列表
 * @Scenario: 正常编辑数据
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $editNewsObject;

    private $mock;

    public function setUp()
    {
        $this->editNewsObject = new News();
    }

    public function tearDown()
    {
        unset($this->editNewsObject);
    }

    /**
    * @Given: 我并未编辑过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getNewsOldData();

        $jsonData = json_encode($data);
        

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($this->getNewsCommonResponse())
               )
            ]
        );
      
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要编辑的新闻数据
    */
    protected function fetchNews($id)
    {
        $adapter = new NewsRestfulAdapter();
        $this->editNewsObject = $adapter->fetchOne($id);
        
        return $this->editNewsObject;
    }

    /**
    * @When:当我调用编辑函数,期待编辑成功
    */
    protected function edit()
    {
        $this->editNewsObject->setTitle('新闻标题');
        $this->editNewsObject->setDimension(2);
        $this->editNewsObject->setNewsType(151);
        $crew = new Crew(1);
        $this->editNewsObject->setCrew($crew);
        $this->editNewsObject->setBannerImage(
            [
                "name"=> "轮播图图片名称",
                "identify"=> "轮播图图片地址.jpg"
            ]
        );
        $this->editNewsObject->setContent('新闻内容');
        $this->editNewsObject->setStick(2);
        $this->editNewsObject->setAttachments(
            [
                [
                    "name"=> "附件名称",
                    "identify"=> "附件地址.doc"
                ]
            ]
        );
        $this->editNewsObject->setCover(
            [
                "name"=> "封面名称",
                "identify"=> "封面地址.jpg"
            ]
        );
        $this->editNewsObject->setSource('新闻来源');
        $this->editNewsObject->setBannerStatus(2);
        $this->editNewsObject->setHomePageShowStatus(2);
        $this->editNewsObject->setStatus(0);
       
        return $this->editNewsObject->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchNews($id);

        $this->edit();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/news/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($this->getNewsOperationRequestData()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('新闻标题', $this->editNewsObject->getTitle());
        $this->assertEquals('新闻来源', $this->editNewsObject->getSource());
        $this->assertEquals(News::STATUS['ENABLED'], $this->editNewsObject->getStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->editNewsObject->getBannerStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->editNewsObject->getHomePageShowStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->editNewsObject->getStick());

        $this->assertEquals(News::DIMENSION['GOVERNMENT_AFFAIRS'], $this->editNewsObject->getDimension());
        $this->assertEquals(NEWS_TYPE['LEADING_GROUP'], $this->editNewsObject->getNewsType());
        $this->assertEquals('新闻描述', $this->editNewsObject->getDescription());
        $this->assertEquals(
            [
                "name"=> "轮播图图片名称",
                "identify"=> "轮播图图片地址.jpg"
            ],
            $this->editNewsObject->getBannerImage()
        );
        $this->assertEquals('新闻内容', $this->editNewsObject->getContent());
        $this->assertEquals(
            [
                [
                    "name"=> "附件名称",
                    "identify"=> "附件地址.doc"
                ]
            ],
            $this->editNewsObject->getAttachments()
        );
        $this->assertEquals(
            [
                "name"=> "封面名称",
                "identify"=> "封面地址.jpg"
            ],
            $this->editNewsObject->getCover()
        );

        $this->assertEquals(
            1620869215,
            $this->editNewsObject->getStatusTime()
        );
        $this->assertEquals(
            1620831782,
            $this->editNewsObject->getCreateTime()
        );
        $this->assertEquals(
            1620872174,
            $this->editNewsObject->getUpdateTime()
        );
    }
}
