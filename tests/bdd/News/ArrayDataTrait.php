<?php
namespace Base\Sdk\News;

use Base\Sdk\News\Model\News;

use Base\Sdk\Common\Model\IApplyCategory;

use Base\Sdk\Common\Model\IApplyAble;

trait ArrayDataTrait
{
    protected function getNewsCommonData():array
    {
        return [
            "data"=>[
                "type"=>"news",
                "attributes"=>[
                    "title"=> "新闻标题",
                    "source"=> "新闻来源",
                    "attachments"=> [
                        [
                            "name"=> "附件名称",
                            "identify"=> "附件地址.doc"
                        ]
                    ],
                    "status"=> 0,
                    "dimension"=> 2,
                    "newsType"=> 151,
                    "bannerImage"=> [
                        "name"=> "轮播图图片名称",
                        "identify"=> "轮播图图片地址.jpg"
                    ],
                    "content"=> "新闻内容",
                    "description"=> "新闻描述",
                    "stick"=> 2,
                    "cover"=> [
                        "name"=> "封面名称",
                        "identify"=> "封面地址.jpg"
                    ],
                    "bannerStatus"=> 2,
                    "homePageShowStatus"=> 2,
                ],
                "relationships"=> [
                    "crew"=> [
                        "data"=>[
                            [
                                "type"=> "crews",
                                "id"=> 1
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getNewsOperationRequestData():array
    {
        $data = $this->getNewsCommonData();
        unset($data['data']['attributes']['description']);

        return $data;
    }

    protected function getNewsCommonResponse($id = 1):array
    {
        $data = $this->getNewsCommonData();

        $data['data']['id'] = $id;
        $data['data']['links'] = [
            "self"=>"127.0.0.1:8080\/news\/1"
        ];
        $data['data']['attributes']['createTime'] = 1620831782;
        $data['data']['attributes']['updateTime'] = 1620872174;
        $data['data']['attributes']['statusTime'] = 1620869215;
        $data['data']['relationships'] = [
            "publishUserGroup"=> [
                "data"=> [
                    "type"=> "userGroups",
                    "id"=>1
                ]
            ],
            "crew"=> [
                "data"=> [
                    "type"=> "crews",
                    "id"=> 1
                ]
            ]
        ];
        $data['included'] = $this->getNewsCommonIncluded();
        return $data;
    }

    protected function getNewsStatusRequest() : array
    {
        return [
            "data"=>[
                "type"=>"news",
                "attributes"=>[],
                "relationships"=> [
                    "crew"=> [
                        "data"=>[
                            [
                                "type"=> "crews",
                                "id"=> 1
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getNewsStatusResponse(
        $status = News::STATUS['ENABLED'],
        $stick = News::STICK['DISABLED']
    ):array {
        $data = $this->getNewsCommonResponse();
        $data['data']['attributes']['status'] = $status;
        $data['data']['attributes']['stick'] = $stick;
        
        return $data;
    }
    
    protected function getUnAuditNewsCommonResponse(
        $id = 1,
        $applyStatus = IApplyAble::APPLY_STATUS['PENDING'],
        $rejectReason = ''
    ):array {
        $data = $this->getNewsCommonResponse($id);
        $data['data']['type'] = "unAuditedNews";
        $data['data']['links'] = [
            "self"=>"127.0.0.1:8080\/unAuditedNews\/1"
        ];
        $data['data']['attributes']['operationType'] = IApplyCategory::OPERATION_TYPE['EDIT'];
        $data['data']['attributes']['applyStatus'] = $applyStatus;
        $data['data']['attributes']['applyInfoType'] = 1;
        $data['data']['attributes']['rejectReason'] = $rejectReason;
        
        return $data;
    }

    protected function getUnAuditNewsStatusResponse(
        $id = 1,
        $applyStatus = IApplyAble::APPLY_STATUS['PENDING'],
        $rejectReason = '驳回原因'
    ):array {
        $applyStatusData = $this->getUnAuditNewsCommonResponse(
            $id,
            $applyStatus,
            $rejectReason
        );

        $relationships = $this->getUnAuditNewsRelationships();
        $applyStatusData['data']['relationships'] =
            array_merge($applyStatusData['data']['relationships'], $relationships);

        $included = $this->getUnAuditNewsIncluded();
        $applyStatusData['included'] =
            array_merge($applyStatusData['included'], $included);
            
        return $applyStatusData;
    }

    protected function getUnAuditNewsStatusRequest() : array
    {
        return [
            "data"=>[
                "type"=>"unAuditedNews",
                "attributes"=>[],
                "relationships"=> [
                    "applyCrew"=> [
                        "data"=>[
                            [
                                "type"=> "crews",
                                "id"=> 2
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function getNewsOldData():array
    {
        $data = [
            "meta"=>[],
            "data"=>[
                "type"=>"news",
                "id"=>"1",
                "attributes"=>[
                     "title"=> "测试新闻标题123",
                     "source"=> "测试新闻来源123",
                     "cover"=> [
                         "name"=> "测试封面名称123",
                         "identify"=> "测试封面地址123.jpg"
                     ],
                     "attachments"=> [
                         [
                             "name"=> "测试name13",
                             "identify"=> "测试identify123.doc"
                         ]
                     ],
                     "content"=> "测试内容123",
                     "description"=> "测试内容123",
                     "newsType"=> 151,
                     "category"=> 1,
                     "dimension"=> 2,
                     "bannerImage"=> [
                         "name"=> "测试轮播图图片名称123",
                         "identify"=> "测试轮播图图片地址123.jpg"
                     ],
                     "bannerStatus"=> 2,
                     "homePageShowStatus"=> 2,
                     "stick"=> 2,
                     "status"=> 0,
                     "createTime"=> 1620831782,
                     "updateTime"=> 1620872174,
                     "statusTime"=> 1620869215
                 ],
                 "relationships"=> [
                     "publishUserGroup"=> [
                         "data"=> [
                             "type"=> "userGroups",
                             "id"=>1
                         ]
                     ],
                     "crew"=> [
                         "data"=> [
                             "type"=> "crews",
                             "id"=> 1
                         ]
                     ]
                 ],
                "links"=>[
                    "self"=>"127.0.0.1:8080\/news\/1"
                 ]
            ]
         ];

         $data['included'] = $this->getNewsCommonIncluded();

         return $data;
    }

    protected function getUnAuditNewsOldData():array
    {
        $unAuditNews = $this->getNewsOldData();

        $unAuditNews['data']['type'] = "unAuditedNews";
        $unAuditNews['data']['attributes']['applyStatus'] = -2;
        $unAuditNews['data']['attributes']['rejectReason'] = "驳回原因";
        $unAuditNews['data']['attributes']['operationType'] = 1;

        $relationships = $this->getUnAuditNewsRelationships();

        $unAuditNews['data']['relationships'] = array_merge($unAuditNews['data']['relationships'], $relationships);

        $included = $this->getUnAuditNewsIncluded();

        $unAuditNews['included'] = array_merge($unAuditNews['included'], $included);

        return $unAuditNews;
    }

    protected function getUnAuditNewsRelationships():array
    {
        return [
            "applyCrew"=> [
                "data"=> [
                    "type"=> "crews",
                    "id"=> "2"
                ]
            ],
            "applyUserGroup"=> [
                "data"=> [
                    "type"=> "userGroups",
                    "id"=> "2"
                ]
            ]
        ];
    }

    protected function getNewsCommonIncluded():array
    {
        return [
            [
                "type"=> "userGroups",
                "id"=> 1,
                "attributes"=> [
                    "name"=> "萍乡市发展和改革委员会",
                    "shortName"=> "发改委",
                    "status"=> 0,
                    "createTime"=> 1516168970,
                    "updateTime"=> 1516168970,
                    "statusTime"=> 0
                ]
            ],
            [
                "type"=> "crews",
                "id"=> 1,
                "attributes"=> [
                    "realName"=> "张科",
                    "cardId"=> "412825199009094532",
                    "userName"=> "18800000000",
                    "cellphone"=> "18800000000",
                    "category"=> 1,
                    "purview"=> [
                        "1",
                        "2",
                        "3"
                    ],
                    "status"=> 0,
                    "createTime"=> 1618284031,
                    "updateTime"=> 1619578455,
                    "statusTime"=> 0
                ],
                "relationships"=> [
                    "userGroup"=> [
                        "data"=> [
                            "type"=> "userGroups",
                            "id"=>1
                        ]
                    ],
                    "department"=> [
                        "data"=> [
                            "type"=> "departments",
                            "id"=> 3
                        ]
                    ]
                ],
            ]
        ];
    }

    protected function getUnAuditNewsIncluded():array
    {
        return [
            [
                "type"=> "applyUserGroup",
                "id"=> 2,
                "attributes"=> [
                    "name"=> "宜昌市发展和改革委员会",
                    "shortName"=> "发改委",
                    "status"=> 0,
                    "createTime"=> 1516168970,
                    "updateTime"=> 1516168970,
                    "statusTime"=> 0
                ]
            ],
            [
                "type"=> "applyCrew",
                "id"=> 2,
                "attributes"=> [
                    "realName"=> "宴乐",
                    "cardId"=> "412825199009094555",
                    "userName"=> "18800000080",
                    "cellphone"=> "18800000080",
                    "category"=> 1,
                    "purview"=> [
                        "1",
                        "2",
                        "3"
                    ],
                    "status"=> 0,
                    "createTime"=> 1618284031,
                    "updateTime"=> 1619578455,
                    "statusTime"=> 0
                ],
                "relationships"=> [
                    "userGroup"=> [
                        "data"=> [
                            "type"=> "userGroups",
                            "id"=>2
                        ]
                    ],
                    "department"=> [
                        "data"=> [
                            "type"=> "departments",
                            "id"=> 3
                        ]
                    ]
                ],
            ]
        ];
    }
}
