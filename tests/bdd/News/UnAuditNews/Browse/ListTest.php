<?php
namespace Base\Sdk\News\UnAuditNews\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Model\UnAuditNews;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Common\Model\IApplyCategory;
use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\News\ArrayDataTrait;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有新闻发布权限、且当我需要查看待审核和已驳回的新闻列表时,
 *           在审核表中,通过列表与详情的形式查看到待审核和已驳回的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻审核列表
 */

class ListTest extends TestCase
{
    use SearchDataTrait, ArrayDataTrait;

    private $unAuditNews;

    private $mock;

    public function setUp()
    {
        $this->unAuditNews = new UnAuditNews();
    }

    public function tearDown()
    {
        unset($this->unAuditNews);
    }

    /**
    * @Given: 存在新闻数据
    */
    protected function prepareData()
    {
        $detail = $this->getUnAuditNewsOldData();
        $data = [
            "meta"=>[
                "count"=> 1,
                "links"=> [
                    "first"=> null,
                    "last"=> null,
                    "prev"=> null,
                    "next"=> null
                ]
            ],
            "links"=> [
                "first"=> null,
                "last"=> null,
                "prev"=> null,
                "next"=> null
            ]
        ];
        $data['data'] = [$detail['data']];
        $data['included'] = $detail['included'];

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看新闻列表时
     */
    public function fetchUnAuditNewsList()
    {
        $filter = [];

        $unAuditNews = $this->getUnAuditNewsList($filter);

        return $unAuditNews;
    }

    /**
     * @Then  我可以看见新闻数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchUnAuditNewsList();
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedNews';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $list = $this->fetchUnAuditNewsList();

        foreach ($list as $data) {
            $this->assertEquals('测试新闻标题123', $data->getTitle());
            $this->assertEquals('测试新闻来源123', $data->getSource());
            $this->assertEquals(News::STATUS['ENABLED'], $data->getStatus());
            $this->assertEquals(News::STICK['ENABLED'], $data->getBannerStatus());
            $this->assertEquals(News::STICK['ENABLED'], $data->getHomePageShowStatus());
            $this->assertEquals(News::STICK['ENABLED'], $data->getStick());
    
            $this->assertEquals(News::DIMENSION['GOVERNMENT_AFFAIRS'], $data->getDimension());
            $this->assertEquals(NEWS_TYPE['LEADING_GROUP'], $data->getNewsType());
            $this->assertEquals('测试内容123', $data->getDescription());
            $this->assertEquals(
                [
                    "name"=> "测试轮播图图片名称123",
                    "identify"=> "测试轮播图图片地址123.jpg"
                ],
                $data->getBannerImage()
            );
            $this->assertEquals('测试内容123', $data->getContent());
            $this->assertEquals(
                [
                    [
                        "name"=> "测试name13",
                        "identify"=> "测试identify123.doc"
                    ]
                ],
                $data->getAttachments()
            );
            $this->assertEquals(
                [
                    "name"=> "测试封面名称123",
                    "identify"=> "测试封面地址123.jpg"
                ],
                $data->getCover()
            );
            $this->assertEquals(
                IApplyCategory::OPERATION_TYPE['ADD'],
                $data->getOperationType()
            );
            $this->assertEquals(
                IApplyAble::APPLY_STATUS['REJECT'],
                $data->getApplyStatus()
            );
    
            $this->assertEquals(
                1620869215,
                $data->getStatusTime()
            );
            $this->assertEquals(
                1620831782,
                $data->getCreateTime()
            );
            $this->assertEquals(
                1620872174,
                $data->getUpdateTime()
            );
        }
    }
}
