<?php
namespace Base\Sdk\News\UnAuditNews\Browse;

use Base\Sdk\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;

trait SearchDataTrait
{
    protected function getUnAuditNewsList(array $filter)
    {
        $adapter = new UnAuditNewsRestfulAdapter();

        list($count, $newsList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $newsList;
    }
}
