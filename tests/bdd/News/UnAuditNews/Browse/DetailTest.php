<?php
namespace Base\Sdk\News\UnAuditNews\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\Common\Model\IApplyCategory;
use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\News\ArrayDataTrait;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),
 *           我拥有新闻发布权限、且当我需要查看未审核或已驳回的新闻列表时,
 *           在审核表中,通过列表与详情的形式查看到未审核或已驳回的新闻信息,
 *           以便于我维护新闻模块
 * @Scenario: 查看新闻数据审核详情
 */

class DetailTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditNews;

    private $mock;

    public function setUp()
    {
        $this->unAuditNews = new UnAuditNews();
    }

    public function tearDown()
    {
        unset($this->unAuditNews);
    }

    /**
    * @Given: 存在一条新闻审核数据
    */
    protected function prepareData()
    {
        $data = $this->getUnAuditNewsCommonResponse(1);

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:当我查看该条新闻数据审核详情时
    */
    protected function fetchUnAuditNews($id)
    {

        $adapter = new UnAuditNewsRestfulAdapter();

        $this->unAuditNews = $adapter->fetchOne($id);
      
        return $this->unAuditNews;
    }

    /**
     * @Then  我可以看见该条新闻数据的全部信息
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditNews($id);
        
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedNews/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(News::DIMENSION['GOVERNMENT_AFFAIRS'], $this->unAuditNews->getDimension());
        $this->assertEquals(NEWS_TYPE['LEADING_GROUP'], $this->unAuditNews->getNewsType());

        $this->assertEquals('新闻标题', $this->unAuditNews->getTitle());
        $this->assertEquals('新闻来源', $this->unAuditNews->getSource());
        $this->assertEquals('新闻描述', $this->unAuditNews->getDescription());

        $this->assertEquals(News::STATUS['ENABLED'], $this->unAuditNews->getStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->unAuditNews->getBannerStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->unAuditNews->getHomePageShowStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->unAuditNews->getStick());
        
        $this->assertEquals('新闻内容', $this->unAuditNews->getContent());
        $this->assertEquals(
            [
                [
                    "name"=> "附件名称",
                    "identify"=> "附件地址.doc"
                ]
            ],
            $this->unAuditNews->getAttachments()
        );

        $this->assertEquals(
            [
                "name"=> "轮播图图片名称",
                "identify"=> "轮播图图片地址.jpg"
            ],
            $this->unAuditNews->getBannerImage()
        );

        $this->assertEquals(
            [
                "name"=> "封面名称",
                "identify"=> "封面地址.jpg"
            ],
            $this->unAuditNews->getCover()
        );

        $this->assertEquals(
            1620869215,
            $this->unAuditNews->getStatusTime()
        );
        $this->assertEquals(
            1620831782,
            $this->unAuditNews->getCreateTime()
        );
        $this->assertEquals(
            1620872174,
            $this->unAuditNews->getUpdateTime()
        );

        $this->assertEquals(
            IApplyCategory::OPERATION_TYPE['EDIT'],
            $this->unAuditNews->getOperationType()
        );
        $this->assertEquals(
            IApplyAble::APPLY_STATUS['PENDING'],
            $this->unAuditNews->getApplyStatus()
        );
    }
}
