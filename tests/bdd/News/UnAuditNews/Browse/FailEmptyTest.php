<?php
namespace Base\Sdk\News\UnAuditNews\Browse;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是分公司同事（平台管理员）/委办局领导（委办局管理员）/委办局员工（操作用户),我拥有新闻发布权限、且当我需要查看待审核和已驳回的新闻的列表时,
 *           在审核表中,通过列表与详情的形式查看到已发布的新闻信息,以便于我维护新闻模块
 * @Scenario: 查看新闻审核列表
 */

class FailEmptyTest extends TestCase
{
    private $unAuditNews;

    private $mock;

    public function setUp()
    {
        $this->unAuditNews = new UnAuditNews();
    }

    public function tearDown()
    {
        unset($this->unAuditNews);
    }

    /**
    * @Given: 不存在新闻审核数据
    */
    protected function prepareData()
    {
        $data = [
            'id'=>100,
            'code'=>PARAMETER_IS_EMPTY,
            'title'=>'数据为空',
            'detail'=>''
        ];

        $jsonData = json_encode($data);

        $this->mock = new MockHandler(
            [
               new Response(
                   404,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
            ]
        );
      
        $handler = HandlerStack::create($this->mock);
       
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看新闻审核列表时
     */
    public function fetchUnAuditNewsList()
    {
        $adapter = new UnAuditNewsRestfulAdapter();

        list($count, $this->unAuditNews) = $adapter
        ->search();
        unset($count);

        return $this->unAuditNews;
    }

    /**
     * @Then: 获取不到审核数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $this->fetchUnAuditNewsList();
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'unAuditedNews';

        $request = $this->mock->getLastRequest();
       
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        
        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEmpty($this->unAuditNews);
    }
}
