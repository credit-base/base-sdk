<?php
namespace Base\Sdk\News\UnAuditNews\Update;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\Common\Model\IApplyCategory;
use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\News\ArrayDataTrait;

/**
 * @Feature: 我是拥有新闻审核权限的平台管理员/委办局管理员/操作用户,
 *           当我需要审核一个新闻时,在审核表中,审核待审核的新闻数据
 *           通过新闻详情页面的审核通过与审核驳回操作,以便于我维护新闻列表
 * @Scenario: 审核驳回
 */

class RejectTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditNews;

    private $mock;

    public function setUp()
    {
        $this->unAuditNews = new UnAuditNews();
    }

    public function tearDown()
    {
        unset($this->unAuditNews);
    }

    /**
    * @Given: 存在需要审核的新闻
    */
    protected function prepareData()
    {
        $data = $this->getUnAuditNewsStatusResponse();
        $jsonData = json_encode($data);
        
        $rejectData = $this->getUnAuditNewsStatusResponse(
            1,
            IApplyAble::APPLY_STATUS['REJECT'],
            '驳回原因'
        );

        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($rejectData)
               )
            ]
        );
    
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:存在需要审核驳回的新闻
    */
    protected function fetchUnAuditNews($id)
    {
        $adapter = new UnAuditNewsRestfulAdapter();
        $this->unAuditNews = $adapter->fetchOne($id);
        
        return $this->unAuditNews;
    }

    /**
    * @And:当我调用审核驳回函数,期待审核驳回成功
    */
    protected function reject()
    {
       
        $crew = new Crew(2);
        $this->unAuditNews->setApplyCrew($crew);
        $this->unAuditNews->setRejectReason('驳回原因');
     
        return $this->unAuditNews->reject();
    }
    /**
     * @Then  我可以查到该条新闻状态已被审核驳回，并能看到驳回原因
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditNews($id);
        $this->reject();
     
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedNews/1/reject';

        $request = $this->mock->getLastRequest();
        
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $rejectRequestData = $this->getUnAuditNewsStatusRequest();

        $rejectRequestData['data']['attributes']['rejectReason'] = '驳回原因';

        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($rejectRequestData), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(
            IApplyAble::APPLY_STATUS['REJECT'],
            $this->unAuditNews->getApplyStatus()
        );
        $this->assertEquals(
            2,
            $this->unAuditNews->getApplyCrew()->getId()
        );

        $this->assertEquals(
            1620869215,
            $this->unAuditNews->getStatusTime()
        );
        $this->assertEquals(
            1620872174,
            $this->unAuditNews->getUpdateTime()
        );
    }
}
