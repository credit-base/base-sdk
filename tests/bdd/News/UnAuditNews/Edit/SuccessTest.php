<?php
namespace Base\Sdk\News\UnAuditNews\Edit;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Model\News;
use Base\Sdk\News\Adapter\UnAuditNews\UnAuditNewsRestfulAdapter;

use Base\Sdk\Crew\Model\Crew;

use Base\Sdk\Common\Model\IApplyCategory;
use Base\Sdk\Common\Model\IApplyAble;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use Base\Sdk\News\ArrayDataTrait;

/**
 * @Feature: 我是拥有新闻发布权限的平台管理员/委办局管理员/操作用户,当我需要编辑一个新闻时,在发布表中,编辑已驳回新闻数据
 *           通过编辑新闻界面，并根据我所采集的新闻数据进行编辑,以便于我可以更好的维护新闻管理列表
 * @Scenario: 正常编辑审核数据
 */

class SuccessTest extends TestCase
{
    use ArrayDataTrait;

    private $unAuditNews;

    private $mock;

    public function setUp()
    {
        $this->unAuditNews = new UnAuditNews();
    }

    public function tearDown()
    {
        unset($this->unAuditNews);
    }

    /**
    * @Given: 我并未编辑过该条数据
    */
    protected function prepareData()
    {
        $data = $this->getUnAuditNewsOldData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(
                   200,
                   ['Content-Type' => 'application/vnd.api+json'],
                   $jsonData
               ),
               new Response(
                   201,
                   ['Content-Type' => 'application/vnd.api+json'],
                   json_encode($this->getUnAuditNewsCommonResponse())
               )
            ]
        );
      
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
    * @When:获取需要编辑的新闻数据
    */
    protected function fetchUnAuditNews($id)
    {
        $adapter = new UnAuditNewsRestfulAdapter();
        $this->unAuditNews = $adapter->fetchOne($id);
        
        return $this->unAuditNews;
    }

    /**
    * @When:当我调用编辑函数,期待编辑成功
    */
    protected function edit()
    {
        $this->unAuditNews->setTitle('新闻标题');
        $this->unAuditNews->setDimension(2);
        $this->unAuditNews->setNewsType(151);
        $crew = new Crew(1);
        $this->unAuditNews->setCrew($crew);
        $this->unAuditNews->setBannerImage(
            [
                "name"=> "轮播图图片名称",
                "identify"=> "轮播图图片地址.jpg"
            ]
        );
        $this->unAuditNews->setContent('新闻内容');
        $this->unAuditNews->setStick(2);
        $this->unAuditNews->setAttachments(
            [
                [
                    "name"=> "附件名称",
                    "identify"=> "附件地址.doc"
                ]
            ]
        );
        $this->unAuditNews->setCover(
            [
                "name"=> "封面名称",
                "identify"=> "封面地址.jpg"
            ]
        );
        $this->unAuditNews->setSource('新闻来源');
        $this->unAuditNews->setBannerStatus(2);
        $this->unAuditNews->setHomePageShowStatus(2);
        $this->unAuditNews->setStatus(0);
       
        return $this->unAuditNews->edit();
    }
    /**
     * @Then  我可以查到编辑的数据
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        
        $this->fetchUnAuditNews($id);

        $this->edit();
    
        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/unAuditedNews/1/resubmit';

        $request = $this->mock->getLastRequest();

        $requestEditData = $this->getNewsOperationRequestData();
        $requestEditData['data']['type'] = "unAuditedNews";

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode($requestEditData), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals('新闻标题', $this->unAuditNews->getTitle());
        $this->assertEquals('新闻来源', $this->unAuditNews->getSource());
        $this->assertEquals(News::STATUS['ENABLED'], $this->unAuditNews->getStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->unAuditNews->getBannerStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->unAuditNews->getHomePageShowStatus());
        $this->assertEquals(News::STICK['ENABLED'], $this->unAuditNews->getStick());

        $this->assertEquals(News::DIMENSION['GOVERNMENT_AFFAIRS'], $this->unAuditNews->getDimension());
        $this->assertEquals(NEWS_TYPE['LEADING_GROUP'], $this->unAuditNews->getNewsType());
        $this->assertEquals('新闻描述', $this->unAuditNews->getDescription());
        $this->assertEquals(
            [
                "name"=> "轮播图图片名称",
                "identify"=> "轮播图图片地址.jpg"
            ],
            $this->unAuditNews->getBannerImage()
        );
        $this->assertEquals('新闻内容', $this->unAuditNews->getContent());
        $this->assertEquals(
            [
                [
                    "name"=> "附件名称",
                    "identify"=> "附件地址.doc"
                ]
            ],
            $this->unAuditNews->getAttachments()
        );
        $this->assertEquals(
            [
                "name"=> "封面名称",
                "identify"=> "封面地址.jpg"
            ],
            $this->unAuditNews->getCover()
        );

        $this->assertEquals(
            IApplyCategory::OPERATION_TYPE['EDIT'],
            $this->unAuditNews->getOperationType()
        );
        $this->assertEquals(
            IApplyAble::APPLY_STATUS['PENDING'],
            $this->unAuditNews->getApplyStatus()
        );

        $this->assertEquals(
            1620869215,
            $this->unAuditNews->getStatusTime()
        );
        $this->assertEquals(
            1620831782,
            $this->unAuditNews->getCreateTime()
        );
        $this->assertEquals(
            1620872174,
            $this->unAuditNews->getUpdateTime()
        );
    }
}
