<?php


namespace Base\Sdk\WebsiteCustomize;

use Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize\WebsiteCustomizeRestfulAdapter;
use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;

trait ListDataTrait
{
    use ArrayDataTrait;

    protected function getWebsiteCustomizeList(array $filter = [])
    {
        $adapter = new WebsiteCustomizeRestfulAdapter();

        list($count, $memberList) = $adapter->search($filter, ['-updateTime'], 1, 10);
        unset($count);

        return $memberList;
    }

    protected function getWebsiteCustomizeListData(): array
    {
        return [
            'meta' => [
                'count' =>12,
                'links' => [
                    'first' =>1,
                    'last' =>12,
                    'prev' =>null,
                    'next' =>2
                ]
            ],
            'links' => [
                'first' =>'api.base.qixinyun.com/websiteCustomizes/?sort=-id&page[number]=1&page[size]=1',
                'last' =>'api.base.qixinyun.com/websiteCustomizes/?sort=-id&page[number]=12&page[size]=1',
                'prev' =>null,
                'next' =>'api.base.qixinyun.com/websiteCustomizes/?sort=-id&page[number]=2&page[size]=1'
            ],
            'data' =>[
                [
                    'type' =>'websiteCustomizes',
                    'id' => 78,
                    'attributes' => [
                        'category' =>1,
                        'content' => $this->getContent(),
                        'version' =>2108232515,
                        'status' =>2,
                        'createTime' =>1629719232,
                        'updateTime' =>1629719232,
                        'statusTime' =>0
                    ],
                    'relationships' => $this->getRelationships(),
                    'links' => [
                        'self' =>'api.base.qixinyun.com/websiteCustomizes/78'
                    ],
                ]
            ],
            'included' => $this->getInclude()
        ];
    }
}
