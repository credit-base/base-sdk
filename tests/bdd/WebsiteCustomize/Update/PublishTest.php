<?php


namespace Base\Sdk\WebsiteCustomize\Update;

use Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize\WebsiteCustomizeRestfulAdapter;
use Base\Sdk\WebsiteCustomize\ArrayDataTrait;
use Base\Sdk\WebsiteCustomize\DetailDataTrait;
use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class PublishTest extends TestCase
{
    use DetailDataTrait;

    private $enableWebsiteCustomizesObject;

    private $mock;

    public function setUp()
    {
        $this->enableWebsiteCustomizesObject = new WebsiteCustomize();
    }

    public function tearDown()
    {
        unset($this->enableWebsiteCustomizesObject);
    }

    /**
     * @Given: 存在需要启用的用户
     */
    protected function prepareData()
    {
        $id = 1;
        $disableData = $this->getWebsiteCustomizeDetailData($id, WebsiteCustomize::STATUS['UNPUBLISHED']);
        $jsonData = json_encode($disableData);

        $enableData = $this->getWebsiteCustomizeDetailData($id, WebsiteCustomize::STATUS['PUBLISHED']);

        $this->mock = new MockHandler(
            [
                new Response(200, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
                new Response(201, ['Content-Type' => 'application/vnd.api+json'], json_encode($enableData))
            ]
        );

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:存在需要启用的用户
     */
    protected function fetchWebsiteCustomize($id)
    {
        $adapter = new WebsiteCustomizeRestfulAdapter();
        $this->enableWebsiteCustomizesObject = $adapter->fetchOne($id);

        return $this->enableWebsiteCustomizesObject;
    }

    /**
     * @And:当我调用启用函数,期待启用成功
     */
    protected function publish()
    {
        return $this->enableWebsiteCustomizesObject->publish();
    }

    /**
     * @Then  我可以查到该条新闻已被启用
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();

        $this->fetchWebsiteCustomize($id);
        $this->publish();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = '/websiteCustomizes/1/publish';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();

        $contents = $request->getBody()->getContents();
        $this->assertEquals('PATCH', $method);
        $this->assertEquals(json_encode(array()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(WebsiteCustomize::STATUS['PUBLISHED'], $this->enableWebsiteCustomizesObject->getStatus());

        $this->assertEquals(
            1629689141,
            $this->enableWebsiteCustomizesObject->getStatusTime()
        );
        $this->assertEquals(
            1629689141,
            $this->enableWebsiteCustomizesObject->getUpdateTime()
        );
    }
}
