<?php


namespace Base\Sdk\WebsiteCustomize;

use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;

trait DetailDataTrait
{
    use ArrayDataTrait;
    protected function getWebsiteCustomizeDetailData(
        int $id = 0,
        int $status = WebsiteCustomize::STATUS['PUBLISHED']
    ): array {
        return [
            'meta' =>[],
            'data' => [
                'type' =>'websiteCustomizes',
                'id' => $id,
                'attributes' => [
                    'category' =>1,
                    'content' => $this->getContent(),
                    'version' =>2108120000,
                    'status' => $status,
                    'createTime' =>1516174523,
                    'updateTime' =>1629689141,
                    'statusTime' =>1629689141
                ],
                'relationships' => $this->getRelationships(),
                'links' => [
                    'self' =>'api.base.qixinyun.com/websiteCustomizes/1'
                ],
                'included' => $this->getInclude()
            ]
        ];
    }
}
