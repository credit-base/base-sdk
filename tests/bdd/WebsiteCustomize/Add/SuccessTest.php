<?php


namespace Base\Sdk\WebsiteCustomize\Add;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\WebsiteCustomize\DetailDataTrait;
use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Base\Sdk\WebsiteCustomize\ArrayDataTrait;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class SuccessTest extends TestCase
{
    use DetailDataTrait;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new WebsiteCustomize();
    }

    public function tearDown()
    {
        unset($this->member);
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function prepareData()
    {
        $data = $this->getWebsiteCustomizeDetailData(1);
        $data['data']['attributes']['content'] = array();

        $this->mock = new MockHandler([
            new Response(201, ['Content-Type' => 'application/vnd.api+json'], json_encode($data)),

        ]);

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When:当我调用添加函数,期待返回true
     */
    protected function add()
    {
        $this->member = new WebsiteCustomize();
        $this->member->setCategory(1);
        $this->member->setStatus(WebsiteCustomize::STATUS['PUBLISHED']);
//        $this->member->setContent($this->getContent());
        $this->member->setContent(array());

        return $this->member->add();
    }

    /**
     * @Then  我可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->add();

        $this->request();
        $this->response();
    }


    private function request()
    {
        $expectedPath = '/websiteCustomizes';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('POST', $method);
        $this->assertEquals(json_encode($this->getWebsiteCustomizeResponseData()), $contents);
        $this->assertEquals($expectedPath, $path);
    }

    protected function getWebsiteCustomizeResponseData() : array
    {
        return [
            'data' => [
                'type' => 'websiteCustomizes',
                'attributes' => [
                    'category' => 1,
                    'content' => [],//$this->getContent(),
                    'status' => 2,
                ],
                'relationships' => [
                    "crew" => [
                        "data" =>[
                            [
                                "type" =>"crews",
                                "id" =>0
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    private function response()
    {
        $this->assertEquals(1, $this->member->getCategory());
        $this->assertEquals(1516174523, $this->member->getCreateTime());
        $this->assertEquals(1629689141, $this->member->getStatusTime());
        $this->assertEquals(1629689141, $this->member->getUpdateTime());
    }
}
