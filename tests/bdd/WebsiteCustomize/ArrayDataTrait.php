<?php


namespace Base\Sdk\WebsiteCustomize;

trait ArrayDataTrait
{
    protected function getContent() : array
    {
        return [
            'nav' => $this->getNav(),
            'logo' => [
                'name' =>'信用中国（四川宜宾）',
                'identify' =>'logo.png'
            ],
            'theme' => [
                'color' =>'#41245',
                'image' => [
                    'name' =>'headerBg',
                    'identify' =>'bg-header.png'
                ]
            ],
            'headerBg' => [
                'name' =>'headerBg',
                'identify' =>'bg-header.png'
            ],
            'footerNav' => $this->getFootNav(),
            'footerTwo' => $this->getFooterTwo(),
            'silhouette' => [
                'image' => [
                    'name' =>'',
                    'identify' =>'yibin-portal-bottom_silhouette.png'
                ],
                'status' =>'0',
                'description' =>'历史文化'
            ],
            'footerThree' => $this->getFooterThree(),
            'frameWindow' => [
                'url' =>'http://www.yibin.gov.cn/qt_297/test/test1/index.html',
                'status' =>'0'
            ],
            'centerDialog' => [
                'url' =>'javascript:;',
                'image' => [
                    'name' =>'',
                    'identify' =>'http://portal.ny.qixinyun.com/images/party-100-year.png'
                ],
                'status' =>'0'
            ],
            'footerBanner' => $this->getFooterBanner(),
            'headerSearch' => $this->getHeaderSearch(),
            'relatedLinks' => $this->getRelatedLinks(),
            'rightToolBar' => $this->getRightToolBar(),
            'animateWindow' => $this->getAnimateWindow(),
            'headerBarLeft' => $this->getHeaderBarLeft(),
            'leftFloatCard' => $this->getLeftFloatCard(),
            'specialColumn' => $this->getSpecialColumn(),
            'headerBarRight' => $this->getHeaderBarRight(),
            'memorialStatus' => 0,
            'organizationGroup' => $this->getOrganizationGroup(),
            'partyAndGovernmentOrgans' => [
                'url' =>'https://bszs.conac.cn/sitename?method=show&amp;id=A259C6A7DC646119E05310291AACAE96',
                'image' => [
                    'name' =>'',
                    'identify' =>'https://dcs.conac.cn/image/red_error.png'
                ],
                'status' =>'0'
            ],
            'governmentErrorCorrection' => [
                'url' =>'https://bszs.conac.cn/sitename?method=show&amp;id=A259C6A7DC646119E05310291AACAE96',
                'image' => [
                    'name' =>'',
                    'identify' =>'https://zfwzgl.www.gov.cn/exposure/images/jiucuo.png'
                ],
                'status' =>'0'
            ],
            'version' =>2108120000,
            'status' =>0,
            'createTime' =>1516174523,
            'updateTime' =>1629689141,
            'statusTime' =>1629689141
        ];
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function getNav() : array
    {
        return [
            [
                'nav' =>'1',
                'url' =>'/',
                'name' =>'首页',
                'image' => [
                    'name' =>'首页',
                    'identify' =>'首页.png'
                ],
                'status' =>'0'
            ],
            [
                'nav' =>'2',
                'url' =>'/creditDynamics/index',
                'name' =>'信用动态',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'3',
                'url' =>'/news/index?category=NC0',
                'name' =>'政策法规',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'4',
                'url' =>'/news/index?category=NC4',
                'name' =>'标准规范',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'5',
                'url' =>'/creditPublicities/index',
                'name' =>'信息公示',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'6',
                'url' =>'/disciplinaries/index',
                'name' =>'联合奖惩',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'7',
                'url' =>'/news?type=MDMy',
                'name' =>'信用承诺',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'8',
                'url' =>'/news?type=MDQp',
                'name' =>'典型案例',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'9',
                'url' =>'/news/index?category=NDM',
                'name' =>'信用研究',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'10',
                'url' =>'/journals',
                'name' =>'信用刊物',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'11',
                'url' =>'/creditPhotographys',
                'name' =>'信用随手拍',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'0',
                'url' =>'/',
                'name' =>'办事服务',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'0',
                'url' =>'/',
                'name' =>'政府信息公开',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'0',
                'url' =>'/',
                'name' =>'信易+',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'0',
                'url' =>'/',
                'name' =>'信用地图',
                'image' =>[],
                'status' =>'0'
            ],
            [
                'nav' =>'0',
                'url' =>'/',
                'name' =>'工程信用库',
                'image' =>[],
                'status' =>'0'
            ]
        ];
    }

    protected function getFootNav() : array
    {
        return [
            [
                'url' =>'/about',
                'name' =>'帮助说明',
                'status' =>'0'
            ],
            [
                'url' =>'/about',
                'name' =>'技术支持',
                'status' =>'0'
            ],
            [
                'url' =>'/about',
                'name' =>'链接我站',
                'status' =>'0'
            ],
            [
                'url' =>'/about',
                'name' =>'隐私保护',
                'status' =>'0'
            ],
            [
                'url' =>'/about',
                'name' =>'版权声明',
                'status' =>'0'
            ],
            [
                'url' =>'/about',
                'name' =>'关于我们',
                'status' =>'0'
            ],
            [
                'url' =>'/navigations/index',
                'name' =>'网站地图',
                'status' =>'0'
            ]
        ];
    }

    protected function getFooterTwo() : array
    {
        return [
            [
                'url' =>'',
                'name' =>'主办单位：',
                'type' =>'0',
                'status' =>'0',
                'description' =>'宜宾市发展和改革委员会'
            ],
            [
                'url' =>'',
                'name' =>'联系电话：',
                'type' =>'0',
                'status' =>'0',
                'description' =>'0831-2339249'
            ]
        ];
    }

    protected function getFooterThree() : array
    {
        return [
            [
                'url' =>'https://beian.miit.gov.cn/#/Integrated/index',
                'name' =>'备案号',
                'type' =>'7',
                'status' =>'0',
                'description' =>'蜀ICP备2021016944号-1'
            ],
            [
                'url' =>'http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=51150402000015',
                'name' =>'公网安备',
                'type' =>'8',
                'status' =>'0',
                'description' =>'51150402000015号'
            ],
            [
                'url' =>'',
                'name' =>'网站标识码',
                'type' =>'9',
                'status' =>'0',
                'description' =>'5115000065'
            ]
        ];
    }

    protected function getFooterBanner() : array
    {
        return [
            [
                'url' =>'http://www.xinhuanet.com/politics/fdbnlqhxzc/',
                'image' => [
                    'name' =>'',
                    'identify' =>'https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/ossProduction/banner-fd.png'
                ],
                'status' =>'0'
            ],
            [
                'url' =>'http://www.sc.gov.cn/10462/sctfjkt/sctfjkt.shtml',
                'image' => [
                    'name' =>'',
                    'identify' =>'https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/ossProduction/banner-jkt.png'
                ],
                'status' =>'0'
            ]
        ];
    }

    protected function getHeaderSearch() : array
    {
        return [
            'newsTitle' => [
                'name' =>'站内文章',
                'status' =>'0'
            ],
            'legalPerson' => [
                'name' =>'法人',
                'status' =>'0'
            ],
            'creditInformation' => [
                'name' =>'信用信息',
                'status' =>'0'
            ],
            'unifiedSocialCreditCode' => [
                'name' =>'统一社会信用代码',
                'status' =>'0'
            ],
            'personalCreditInformation' => [
                'name' =>'个人信用信息',
                'status' =>'0'
            ]
        ];
    }

    protected function getRelatedLinks() : array
    {
        return [
            [
                'name' =>'信用体系建设共建单位',
                'items' =>[
                    [
                        'url' =>'http://fg.yibin.gov.cn/',
                        'name' =>'市发展改革委',
                        'status' =>'0'
                    ],
                    [
                        'url' =>'javascript:void(0)',
                        'name' =>'人行宜宾市中支',
                        'status' =>'0'
                    ],
                    [
                        'url' =>'http://fg.yibin.gov.cn/',
                        'name' =>'市委宣传部',
                        'status' =>'0'
                    ],
                    [
                        'url' =>'javascript:void(0)',
                        'name' =>'市网信办',
                        'status' =>'0'
                    ]
                ],
                'status' =>'0'
            ],
            [
                'name' =>'电子政务网站',
                'items' =>[
                    [
                        'url' =>'http://fg.yibin.gov.cn/',
                        'name' =>'市发展改革委',
                        'status' =>'0'
                    ],
                    [
                        'url' =>'javascript:void(0)',
                        'name' =>'人行宜宾市中支',
                        'status' =>'0'
                    ],
                    [
                        'url' =>'http://fg.yibin.gov.cn/',
                        'name' =>'市委宣传部',
                        'status' =>'0'
                    ],
                    [
                        'url' =>'javascript:void(0)',
                        'name' =>'市网信办',
                        'status' =>'0'
                    ]
                ],
                'status' =>'0'
            ]
        ];
    }

    protected function getRightToolBar() : array
    {
        return [
            [
                'url' =>'/',
                'name' =>'调查',
                'images' =>[],
                'status' =>'0',
                'category' =>'2'
            ],
            [
                'name' =>'微信',
                'images' =>[
                    [
                        'image' => [
                            'name' =>'微信公众号',
                            'identify' =>'weixin.jpg'
                        ],
                        'title' =>'安卓版APP'
                    ]
                ],
                'status' =>'0',
                'category' =>'1'
            ],
            [
                'name' =>'App',
                'images' =>[
                    [
                        'image' => [
                            'name' =>'安卓版APP',
                            'identify' =>'weixin.jpg'
                        ],
                        'title' =>'安卓版APP'
                    ],
                    [
                        'image' => [
                            'name' =>'IOS版APP',
                            'identify' =>'weixin.jpg'
                        ],
                        'title' =>'IOS版APP'
                    ]
                ],
                'status' =>'0',
                'category' =>'1'
            ],
            [
                'url' =>'/',
                'name' =>'表扬',
                'images' =>[],
                'status' =>'0',
                'category' =>'2'
            ],
            [
                'name' =>'邮箱',
                'images' =>[],
                'status' =>'0',
                'category' =>'3',
                'description' =>'（宜宾市发展改革委）scybxytxbgs@163.com'
            ]
        ];
    }

    protected function getAnimateWindow() : array
    {
        return [
            [
                'url' =>'javascript:;',
                'image' => [
                    'name' =>'信易贷申请入口',
                    'identify' =>'nanyang-portal-index-aside.png'
                ],
                'status' =>'0'
            ],
            [
                'url' =>'http://portal.px.qixinyun.com/redBlacks?scene=MA',
                'image' => [
                    'name' =>'红黑榜名单查询',
                    'identify' =>'pxiang-portal-index-aside.png'
                ],
                'status' =>'0'
            ]
        ];
    }

    protected function getHeaderBarLeft() : array
    {
        return [
            [
                'url' =>'',
                'name' =>'访问量统计',
                'type' =>'1',
                'status' =>'0'
            ]
        ];
    }

    protected function getLeftFloatCard() : array
    {
        return [
            [
                'url' =>'http://portal.ny.qixinyun.com/outbreakColumn/index',
                'image' => [
                    'name' =>'',
                    'identify' =>'portal-yiqing-image.png'
                ],
                'status' =>'0'
            ],
            [
                'url' =>'http://portal.ny.qixinyun.com/creditPhotographies?scene=MA',
                'image' => [
                    'name' =>'',
                    'identify' =>'portal-suishoupai-image.png'
                ],
                'status' =>'0'
            ],
            [
                'url' =>'https://www.celoan.cn/?city=Pingxiang',
                'image' => [
                    'name' =>'融资',
                    'identify' =>'home-left-img.png'
                ],
                'status' =>'0'
            ]
        ];
    }

    protected function getSpecialColumn() : array
    {
        return [
            [
                'url' =>'http://portal.yb.qixinyun.com/honestyConstructions/index',
                'image' => [
                    'name' =>'',
                    'identify' =>'yibin/yibin-portal-topic-banner.jpg'
                ],
                'status' =>'0'
            ],
            [
                'url' =>'http://portal.yb.qixinyun.com/news?type=My8',
                'image' => [
                    'name' =>'',
                    'identify' =>'yibin/xinyi-plus-banner.jpg'
                ],
                'status' =>'0'
            ]
        ];
    }

    protected function getHeaderBarRight() : array
    {
        return [
            [
                'url' =>'',
                'name' =>'简体/繁体',
                'type' =>'2',
                'status' =>'0'
            ],
            [
                'url' =>'',
                'name' =>'无障碍阅读',
                'type' =>'3',
                'status' =>'0'
            ],
            [
                'url' =>'',
                'name' =>'用户中心',
                'type' =>'4',
                'status' =>'0'
            ],
            [
                'url' =>'navigations/index',
                'name' =>'网站声明',
                'type' =>'0',
                'status' =>'0'
            ]
        ];
    }

    protected function getOrganizationGroup() : array
    {
        return [
            [
                'url' =>'http://www.creditchina.gov.cn/',
                'image' => [
                    'name' =>'',
                    'identify' =>'https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link1.png'
                ],
                'status' =>'0'
            ],
            [
                'url' =>'http://zxgk.court.gov.cn/',
                'image' => [
                    'name' =>'',
                    'identify' =>'https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link2.png'
                ],
                'status' =>'0'
            ],
            [
                'url' =>'http://www.gsxt.gov.cn/index.htm',
                'image' => [
                    'name' =>'',
                    'identify' =>'https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link3.png'
                ],
                'status' =>'0'
            ],
            [
                'url' =>'http://hd.chinatax.gov.cn/xxk/',
                'image' => [
                    'name' =>'',
                    'identify' =>'https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link4.png'
                ],
                'status' =>'0'
            ],
            [
                'url' =>'http://www.creditsc.gov.cn/',
                'image' => [
                    'name' =>'',
                    'identify' =>'https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link5.png'
                ],
                'status' =>'0'
            ],
            [
                'url' =>'http://www.yibin.gov.cn/',
                'image' => [
                    'name' =>'',
                    'identify' =>'https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link6.png'
                ],
                'status' =>'0'
            ],
            [
                'url' =>'http://fg.yibin.gov.cn/',
                'image' => [
                    'name' =>'',
                    'identify' =>'https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/portal/image-yb-link7.png'
                ],
                'status' =>'0'
            ]
        ];
    }


    protected function getRelationships() : array
    {
        return [
            'crew' => [
                'data' => [
                    'type' =>'crews',
                    'id' =>'1'
                ]
            ]
        ];
    }

    protected function getInclude() : array
    {
        return [
            [
                'type' =>'crews',
                'id' =>'1',
                'attributes' => [
                    'realName' =>'张科',
                    'cardId' =>'610428199609205018',
                    'userName' =>'18800000000',
                    'cellphone' =>'18800000000',
                    'category' =>1,
                    'purview' =>[
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9
                    ],
                    'status' =>0,
                    'createTime' =>1597629740,
                    'updateTime' =>1620435256,
                    'statusTime' =>1620435256
                ],
                'relationships' => [
                    'userGroup' => [
                        'data' => [
                            'type' =>'userGroups',
                            'id' =>'1'
                        ]
                    ],
                    'department' => [
                        'data' => [
                            'type' =>'departments',
                            'id' =>'1'
                        ]
                    ]
                ]
            ]
        ];
    }
}
