<?php
namespace Base\Sdk\WebsiteCustomize\Browse;

use Base\Sdk\WebsiteCustomize\Adapter\WebsiteCustomize\WebsiteCustomizeRestfulAdapter;
use Base\Sdk\WebsiteCustomize\DetailDataTrait;
use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Base\Sdk\WebsiteCustomize\ArrayDataTrait;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DetailTest extends TestCase
{
    use  DetailDataTrait;

    private $websiteCustomize;

    private $mock;

    public function setUp()
    {
        $this->websiteCustomize = new WebsiteCustomize();
    }

    public function tearDown()
    {
        unset($this->websiteCustomize);
    }

    /**
     * @Given: 存在一条数据
     */
    protected function prepareData()
    {
        $id = 1;
        $data = $this->getWebsiteCustomizeDetailData($id);

        //print_r($data);die();
        $jsonData = json_encode($data);



        $this->mock = new MockHandler(
            [
                new Response(
                    200,
                    ['Content-Type' => 'application/vnd.api+json'],
                    $jsonData
                ),
            ]
        );
        $handler = HandlerStack::create($this->mock);

        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看该条数据详情时
     */
    protected function fetchWebsiteCustomize($id)
    {

        $adapter = new WebsiteCustomizeRestfulAdapter();

        $this->websiteCustomize = $adapter->fetchOne($id);

        return $this->websiteCustomize;
    }

    /**
     * @Then 我可以看见详情.
     */
    public function testValidate()
    {
        $id = 1;
        $this->prepareData();
        $this->fetchWebsiteCustomize($id);

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'websiteCustomizes/1';

        $request = $this->mock->getLastRequest();

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();
        $contents = $request->getBody()->getContents();

        $this->assertEquals('GET', $method);
        $this->assertEquals('', $contents);
        $this->assertEquals($expectedPath, $path);
    }

    private function response()
    {
        $this->assertEquals(1, $this->websiteCustomize->getCategory());
        $this->assertEquals(1, $this->websiteCustomize->getId());
        $this->assertEquals(WebsiteCustomize::STATUS['PUBLISHED'], $this->websiteCustomize->getStatus());
        $this->assertEquals(1629689141, $this->websiteCustomize->getStatusTime());
        $this->assertEquals(1516174523, $this->websiteCustomize->getCreateTime());
        $this->assertEquals(1629689141, $this->websiteCustomize->getUpdateTime());
    }
}
