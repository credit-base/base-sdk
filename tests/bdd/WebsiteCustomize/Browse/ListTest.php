<?php
namespace Base\Sdk\WebsiteCustomize\Browse;

use Base\Sdk\WebsiteCustomize\ListDataTrait;
use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Marmot\Core;

use PHPUnit\Framework\TestCase;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @Feature: 我是委办局工作人员,当我需要查看企业数据时,在信用信息查询应用系统下的企业管理中
 *           可以查看到所有的企业数据,通过列表和详情的形式查看我所有的企业, 以便于我可以精确的查看到不同的企业信息
 * @Scenario: 查看企业列表
 */

class ListTest extends TestCase
{
    use ListDataTrait;

    private $member;

    private $mock;

    public function setUp()
    {
        $this->member = new WebsiteCustomize();
    }

    public function tearDown()
    {
        unset($this->member);
    }

    /**
    * @Given: 存在企业数据
    */
    protected function prepareData()
    {
        $data = $this->getWebsiteCustomizeListData();

        $jsonData = json_encode($data);
        
        $this->mock = new MockHandler(
            [
               new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
               new Response(201, ['Content-Type' => 'application/vnd.api+json'], $jsonData),
            ]
        );

        $handler = HandlerStack::create($this->mock);
        Core::$container->set('guzzle.handler', $handler);
    }

    /**
     * @When: 当我查看企业列表时
     */
    public function fetchWebsiteCustomizeList()
    {
        $filter = [];
        return $this->getWebsiteCustomizeList($filter);
    }

    /**
     * @Then  我可以看见企业数据的列表信息
     */
    public function testValidate()
    {
        $this->prepareData();

        $this->fetchWebsiteCustomizeList();

        $this->request();
        $this->response();
    }

    private function request()
    {
        $expectedPath = 'websiteCustomizes';

        $request = $this->mock->getLastRequest();

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('', $request->getBody()->getContents());
        $this->assertEquals($expectedPath, $request->getUri()->getPath());
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    private function response()
    {
        $memberObject = $this->fetchWebsiteCustomizeList();
        $memberArray = $this->getWebsiteCustomizeListData()['data'];
        
        foreach ($memberObject as $item) {
            foreach ($memberArray as $val) {
                if ($val['id'] == $item->getId()) {
                    $this->assertEquals($val['attributes']['category'], $item->getCategory());
                    $this->assertEquals($val['attributes']['content'], $item->getContent());
                }
            }
        }
    }
}
