<?php
namespace Base\Sdk\Member\Adapter\Member;

use Base\Sdk\Member\Translator\MemberSessionTranslator;
use Base\Sdk\Member\Adapter\Member\Query\MemberSessionDataCacheQuery;

class MockMemberSessionAdapter extends MemberSessionAdapter
{
    public function getSession() : MemberSessionDataCacheQuery
    {
        return parent::getSession();
    }

    public function getTranslator() : MemberSessionTranslator
    {
        return parent::getTranslator();
    }

    public function getTTL() : int
    {
        return parent::getTTL();
    }
}
