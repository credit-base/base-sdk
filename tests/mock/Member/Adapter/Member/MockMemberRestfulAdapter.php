<?php
namespace Base\Sdk\Member\Adapter\Member;

use Marmot\Interfaces\IRestfulTranslator;

use Base\Sdk\Member\Model\Member;

class MockMemberRestfulAdapter extends MemberRestfulAdapter
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function addAction(Member $member) : bool
    {
        return parent::addAction($member);
    }

    public function editAction(Member $member) : bool
    {
        return parent::editAction($member);
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }
}
