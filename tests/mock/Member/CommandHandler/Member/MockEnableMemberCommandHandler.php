<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Base\Sdk\Common\Model\IEnableAble;

class MockEnableMemberCommandHandler extends EnableMemberCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
