<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Base\Sdk\Member\Repository\MemberRepository;
use Base\Sdk\Member\Repository\MemberSessionRepository;

class MockAuthMemberCommandHandler extends AuthMemberCommandHandler
{
    public function getMemberSessionRepository() : MemberSessionRepository
    {
        return parent::getMemberSessionRepository();
    }

    public function getMemberRepository() : MemberRepository
    {
        return parent::getMemberRepository();
    }
}
