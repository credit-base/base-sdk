<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Base\Sdk\Common\Model\IEnableAble;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Repository\MemberRepository;

class MockDisableMemberCommandHandler extends DisableMemberCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
