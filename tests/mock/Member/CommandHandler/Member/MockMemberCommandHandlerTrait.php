<?php
namespace Base\Sdk\Member\CommandHandler\Member;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ITranslator;

use Base\Sdk\Member\Model\Member;
use Base\Sdk\Member\Model\SecurityQA;
use Base\Sdk\Member\Repository\MemberRepository;

class MockMemberCommandHandlerTrait
{
    use MemberCommandHandlerTrait;

    public function publicGetRepository() : MemberRepository
    {
        return $this->getRepository();
    }

    public function publicFetchMember(int $id) : Member
    {
        return $this->fetchMember($id);
    }

    public function publicGetMember() : Member
    {
        return $this->getMember();
    }

    public function publicGetSecurityQA() : SecurityQA
    {
        return $this->getSecurityQA();
    }
}
