<?php
namespace Base\Sdk\Rule\Model;

use Base\Sdk\Common\Adapter\IApplyAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

use Base\Sdk\Rule\Repository\UnAuditRuleRepository;

class MockUnAuditRule extends UnAuditRule
{
    public function getUnAuditRuleRepository(): UnAuditRuleRepository
    {
        return parent::getUnAuditRuleRepository();
    }

    public function getIModifyStatusAbleAdapter(): IModifyStatusAbleAdapter
    {
        return parent::getIModifyStatusAbleAdapter();
    }

    public function getIOperateAbleAdapter(): IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function getIApplyAbleAdapter(): IApplyAbleAdapter
    {
        return parent::getIApplyAbleAdapter();
    }
}
