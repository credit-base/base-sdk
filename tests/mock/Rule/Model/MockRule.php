<?php
namespace Base\Sdk\Rule\Model;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;
use Base\Sdk\Rule\Repository\RuleRepository;

class MockRule extends Rule
{
    public function getRepository(): RuleRepository
    {
        return parent::getRepository();
    }

    public function getIModifyStatusAbleAdapter(): IModifyStatusAbleAdapter
    {
        return parent::getIModifyStatusAbleAdapter();
    }

    public function getIOperateAbleAdapter(): IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }
}
