<?php
namespace Base\Sdk\Rule\WidgetRules;

class MockRuleWidgetRules extends RuleWidgetRules
{
    public function validateTransformationRule(array $rules) : bool
    {
        return parent::validateTransformationRule($rules);
    }

    public function validateCompletionRules(array $rules) : bool
    {
        return parent::validateCompletionRules($rules);
    }

    public function validateComparisonRules(array $rules) : bool
    {
        return parent::validateComparisonRules($rules);
    }

    public function validateDeDuplicationRules(array $rules) : bool
    {
        return parent::validateDeDuplicationRules($rules);
    }

    public function ruleName(string $name) : bool
    {
        return parent::ruleName($name);
    }

    public function ruleFormat($rule) : bool
    {
        return parent::ruleFormat($rule);
    }
    
    public function validateRule(string $name, array $rule) : bool
    {
        return parent::validateRule($name, $rule);
    }
}
