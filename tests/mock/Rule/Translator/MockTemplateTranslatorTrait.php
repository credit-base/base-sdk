<?php
namespace Base\Sdk\Rule\Translator;

class MockTemplateTranslatorTrait
{
    use TemplateTranslatorTrait;

    public function publicGetTemplateRestfulTranslatorByCategory(int $type)
    {
        return $this->getTemplateRestfulTranslatorByCategory($type);
    }

    public function publicGetTemplateTranslatorByCategory(int $type)
    {
        return $this->getTemplateTranslatorByCategory($type);
    }
}
