<?php
namespace Base\Sdk\Rule\CommandHandler\Rule;

use Base\Sdk\Rule\Model\Rule;
use Base\Sdk\Rule\Repository\RuleRepository;

trait MockRuleCommandHandlerTrait
{
    use RuleCommandHandlerTrait;

    public function getPublicRule() : Rule
    {
        return $this->getRule();
    }

    public function getPublicRepository() : RuleRepository
    {
        return $this->getRepository();
    }

    public function publicFetchRule(int $id) : Rule
    {
        return $this->fetchRule($id);
    }
}
