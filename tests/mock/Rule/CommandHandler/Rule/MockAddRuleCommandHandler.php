<?php
namespace Base\Sdk\Rule\CommandHandler\Rule;

use Base\Sdk\Template\Model\Template;

class MockAddRuleCommandHandler extends AddRuleCommandHandler
{
    use MockRuleCommandHandlerTrait;
    
    public function getTemplateRepositoryByCategory(int $category)
    {
        return parent::getTemplateRepositoryByCategory($category);
    }

    public function fetchTemplate(int $id, int $category) : Template
    {
        return parent::fetchTemplate($id, $category);
    }

    public function executeAction($command)
    {
        return parent::executeAction($command);
    }
}
