<?php
namespace Base\Sdk\Rule\CommandHandler\Rule;

use Base\Sdk\Common\Model\IModifyStatusAble;

class MockDeleteRuleCommandHandler extends DeleteRuleCommandHandler
{
    public function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return parent::fetchIModifyStatusObject($id);
    }
}
