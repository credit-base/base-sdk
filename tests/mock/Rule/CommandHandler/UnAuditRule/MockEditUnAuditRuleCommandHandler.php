<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

class MockEditUnAuditRuleCommandHandler extends EditUnAuditRuleCommandHandler
{
    use MockUnAuditRuleCommandHandlerTrait;

    public function executeAction($command)
    {
        return parent::executeAction($command);
    }
}
