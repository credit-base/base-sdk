<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use Base\Sdk\Common\Model\IApplyAble;

class MockApproveUnAuditRuleCommandHandler extends ApproveUnAuditRuleCommandHandler
{
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
