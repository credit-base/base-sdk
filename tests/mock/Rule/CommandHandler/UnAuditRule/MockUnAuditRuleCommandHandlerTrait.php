<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use Base\Sdk\Rule\Model\UnAuditRule;
use Base\Sdk\Rule\Repository\UnAuditRuleRepository;

trait MockUnAuditRuleCommandHandlerTrait
{
    use UnAuditRuleCommandHandlerTrait;

    public function getPublicRepository() : UnAuditRuleRepository
    {
        return $this->getRepository();
    }

    public function publicFetchUnAuditRule(int $id) : UnAuditRule
    {
        return $this->fetchUnAuditRule($id);
    }
}
