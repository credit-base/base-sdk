<?php
namespace Base\Sdk\Rule\CommandHandler\UnAuditRule;

use Base\Sdk\Common\Model\IModifyStatusAble;

class MockRevokeUnAuditRuleCommandHandler extends RevokeUnAuditRuleCommandHandler
{
    public function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return parent::fetchIModifyStatusObject($id);
    }
}
