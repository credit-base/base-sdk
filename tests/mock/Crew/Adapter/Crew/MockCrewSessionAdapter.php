<?php
namespace Base\Sdk\Crew\Adapter\Crew;

use Base\Sdk\Crew\Translator\CrewSessionTranslator;
use Base\Sdk\Crew\Adapter\Crew\Query\CrewSessionDataCacheQuery;

class MockCrewSessionAdapter extends CrewSessionAdapter
{
    public function getSession() : CrewSessionDataCacheQuery
    {
        return parent::getSession();
    }

    public function getTranslator() : CrewSessionTranslator
    {
        return parent::getTranslator();
    }

    public function getTTL() : int
    {
        return parent::getTTL();
    }
}
