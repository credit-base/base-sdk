<?php
namespace Base\Sdk\Crew\CommandHandler\Crew;

use Base\Sdk\Common\Model\IEnableAble;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

class MockDisableCrewCommandHandler extends DisableCrewCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }

    public function getRepository() : CrewRepository
    {
        return parent::getRepository();
    }

    public function fetchCrew(int $id) : Crew
    {
        return parent::fetchCrew($id);
    }
}
