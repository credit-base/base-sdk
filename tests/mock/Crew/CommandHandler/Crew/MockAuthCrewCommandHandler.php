<?php
namespace Base\Sdk\Crew\CommandHandler\Crew;

use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\Crew\Repository\CrewSessionRepository;

class MockAuthCrewCommandHandler extends AuthCrewCommandHandler
{
    public function getCrewSessionRepository() : CrewSessionRepository
    {
        return parent::getCrewSessionRepository();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }
}
