<?php
namespace Base\Sdk\Crew\CommandHandler\Crew;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\UserGroup\Repository\DepartmentRepository;

class MockAddCrewCommandHandler extends AddCrewCommandHandler
{
    public function getCrew() : Crew
    {
        return parent::getCrew();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getDepartmentRepository() : DepartmentRepository
    {
        return parent::getDepartmentRepository();
    }
}
