<?php
namespace Base\Sdk\Crew\CommandHandler\Crew;

use Base\Sdk\Common\Model\IEnableAble;

class MockEnableCrewCommandHandler extends EnableCrewCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
