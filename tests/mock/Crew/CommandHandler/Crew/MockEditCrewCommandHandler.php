<?php
namespace Base\Sdk\Crew\CommandHandler\Crew;

use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\UserGroup\Repository\DepartmentRepository;

class MockEditCrewCommandHandler extends EditCrewCommandHandler
{
    public function getRepository() : CrewRepository
    {
        return parent::getRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getDepartmentRepository() : DepartmentRepository
    {
        return parent::getDepartmentRepository();
    }
}
