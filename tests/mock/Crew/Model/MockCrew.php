<?php
namespace Base\Sdk\Crew\Model;

use Base\Sdk\Crew\Repository\CrewRepository;
use Base\Sdk\Crew\Repository\CrewSessionRepository;

class MockCrew extends Crew
{
    public function getRepository() : CrewRepository
    {
        return parent::getRepository();
    }

    public function getCrewSessionRepository() : CrewSessionRepository
    {
        return parent::getCrewSessionRepository();
    }

    public function clearSession() : bool
    {
        return parent::clearSession();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function isCellphoneExist() : bool
    {
        return parent::isCellphoneExist();
    }
}
