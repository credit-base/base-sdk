<?php
namespace Base\Sdk\Crew\Model;

class MockGuest extends Guest
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }

    public function saveIdentify()
    {
        return parent::saveIdentify();
    }

    public function registerGlobalCrew() : bool
    {
        return parent::registerGlobalCrew();
    }
}
