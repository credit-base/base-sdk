<?php
namespace Base\Sdk\UserGroup\CommandHandler;

use Base\Sdk\UserGroup\Model\Department;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class MockAddDepartmentCommandHandler extends AddDepartmentCommandHandler
{
    public function getDepartment() : Department
    {
        return parent::getDepartment();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }
}
