<?php
namespace Base\Sdk\UserGroup\CommandHandler;

use Base\Sdk\UserGroup\Repository\DepartmentRepository;

class MockEditDepartmentCommandHandler extends EditDepartmentCommandHandler
{
    public function getRepository() : DepartmentRepository
    {
        return parent::getRepository();
    }
}
