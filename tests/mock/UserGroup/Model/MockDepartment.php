<?php
namespace Base\Sdk\UserGroup\Model;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

use Base\Sdk\UserGroup\Repository\DepartmentRepository;

class MockDepartment extends Department
{
    public function getRepository() : DepartmentRepository
    {
        return parent::getRepository();
    }

    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }
    
    public function nameIsExist() : bool
    {
        return parent::nameIsExist();
    }
}
