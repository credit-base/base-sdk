<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData;

class MockConfirmGbSearchDataCommandHandler extends ConfirmGbSearchDataCommandHandler
{
    use MockGbSearchDataCommandHandlerTrait;
}
