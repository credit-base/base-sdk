<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use Base\Sdk\ResourceCatalog\Model\GbSearchData;
use Base\Sdk\ResourceCatalog\Repository\GbSearchDataRepository;

trait MockGbSearchDataCommandHandlerTrait
{
    use GbSearchDataCommandHandlerTrait;

    public function getPublicRepository() : GbSearchDataRepository
    {
        return $this->getRepository();
    }

    public function publicFetchGbSearchData(int $id) : GbSearchData
    {
        return $this->fetchGbSearchData($id);
    }
}
