<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use Base\Sdk\Common\Model\IModifyStatusAble;

class MockDeleteGbSearchDataCommandHandler extends DeleteGbSearchDataCommandHandler
{
    use MockGbSearchDataCommandHandlerTrait;

    public function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return parent::fetchIModifyStatusObject($id);
    }
}
