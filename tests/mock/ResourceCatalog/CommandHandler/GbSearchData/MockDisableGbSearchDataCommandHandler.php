<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use Base\Sdk\Common\Model\IEnableAble;

class MockDisableGbSearchDataCommandHandler extends DisableGbSearchDataCommandHandler
{
    use MockGbSearchDataCommandHandlerTrait;

    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
