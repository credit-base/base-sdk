<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\WbjSearchData;

use Base\Sdk\Common\Model\IEnableAble;

use Base\Sdk\ResourceCatalog\Model\WbjSearchData;
use Base\Sdk\ResourceCatalog\Repository\WbjSearchDataRepository;

class MockDisableWbjSearchDataCommandHandler extends DisableWbjSearchDataCommandHandler
{
    public function getRepository() : WbjSearchDataRepository
    {
        return parent::getRepository();
    }

    public function fetchWbjSearchData(int $id) : WbjSearchData
    {
        return parent::fetchWbjSearchData($id);
    }

    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
