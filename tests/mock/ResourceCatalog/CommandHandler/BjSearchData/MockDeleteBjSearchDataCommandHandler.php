<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Base\Sdk\Common\Model\IModifyStatusAble;

class MockDeleteBjSearchDataCommandHandler extends DeleteBjSearchDataCommandHandler
{
    use MockBjSearchDataCommandHandlerTrait;

    public function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return parent::fetchIModifyStatusObject($id);
    }
}
