<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Base\Sdk\ResourceCatalog\Model\BjSearchData;
use Base\Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

trait MockBjSearchDataCommandHandlerTrait
{
    use BjSearchDataCommandHandlerTrait;

    public function getPublicRepository() : BjSearchDataRepository
    {
        return $this->getRepository();
    }

    public function publicFetchBjSearchData(int $id) : BjSearchData
    {
        return $this->fetchBjSearchData($id);
    }
}
