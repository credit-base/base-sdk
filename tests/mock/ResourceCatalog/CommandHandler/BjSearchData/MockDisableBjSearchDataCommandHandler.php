<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Base\Sdk\Common\Model\IEnableAble;

class MockDisableBjSearchDataCommandHandler extends DisableBjSearchDataCommandHandler
{
    use MockBjSearchDataCommandHandlerTrait;

    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}
