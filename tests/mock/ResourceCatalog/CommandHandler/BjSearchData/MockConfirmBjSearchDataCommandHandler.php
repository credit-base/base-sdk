<?php
namespace Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData;

class MockConfirmBjSearchDataCommandHandler extends ConfirmBjSearchDataCommandHandler
{
    use MockBjSearchDataCommandHandlerTrait;
}
