<?php
namespace Base\Sdk\ResourceCatalog\Model;

class MockNullErrorData extends NullErrorData
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
