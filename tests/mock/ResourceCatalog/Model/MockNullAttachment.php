<?php
namespace Base\Sdk\ResourceCatalog\Model;

class MockNullAttachment extends NullAttachment
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
