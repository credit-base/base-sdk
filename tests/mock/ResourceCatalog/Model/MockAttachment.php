<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Base\Sdk\Template\Repository\WbjTemplateRepository;

use Base\Sdk\ResourceCatalog\Repository\AttachmentRepository;

/**
 * @SuppressWarnings(PHPMD)
 */
class MockAttachment extends Attachment
{
    public function prepare(string $prefix) : void
    {
        parent::prepare($prefix);
    }

    public function generateHash() : void
    {
        parent::generateHash();
    }

    public function generateFilePath() : string
    {
        return parent::generateFilePath();
    }

    public function move(string $source, string $destination)
    {
        return parent::move($source, $destination);
    }

    public function validateUploadFile() : bool
    {
        return parent::validateUploadFile();
    }

    public function validate() : bool
    {
        return parent::validate();
    }

    public function validateSize() : bool
    {
        return parent::validateSize();
    }

    public function validateExtension() : bool
    {
        return parent::validateExtension();
    }

    public function validateNameFormat(int $templateId) : bool
    {
        return parent::validateNameFormat($templateId);
    }

    public function validateContentDuplicate() : bool
    {
        return parent::validateContentDuplicate();
    }

    public function getWbjTemplateRepository() : WbjTemplateRepository
    {
        return parent::getWbjTemplateRepository();
    }

    public function getTemplateById(int $templateId)
    {
        return parent::getTemplateById($templateId);
    }

    public function getAttachmentRepository() : AttachmentRepository
    {
        return parent::getAttachmentRepository();
    }

    public function isExitByHash() : bool
    {
        return parent::isExitByHash();
    }
}
