<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Base\Sdk\ResourceCatalog\Adapter\GbSearchData\IGbSearchDataAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;

class MockGbSearchData extends GbSearchData
{
    public function getRepository() : IGbSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter
    {
        return parent::getIModifyStatusAbleAdapter();
    }

    public function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return parent::getIEnableAbleAdapter();
    }
}
