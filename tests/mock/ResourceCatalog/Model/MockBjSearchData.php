<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Base\Sdk\ResourceCatalog\Adapter\BjSearchData\IBjSearchDataAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;

class MockBjSearchData extends BjSearchData
{
    public function getRepository() : IBjSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter
    {
        return parent::getIModifyStatusAbleAdapter();
    }

    public function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return parent::getIEnableAbleAdapter();
    }
}
