<?php
namespace Base\Sdk\ResourceCatalog\Model;

class MockNullWbjSearchData extends NullWbjSearchData
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
