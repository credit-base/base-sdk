<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Base\Sdk\ResourceCatalog\Adapter\WbjSearchData\IWbjSearchDataAdapter;

use Base\Sdk\Common\Adapter\IEnableAbleAdapter;

class MockWbjSearchData extends WbjSearchData
{
    public function getRepository() : IWbjSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return parent::getIEnableAbleAdapter();
    }
}
