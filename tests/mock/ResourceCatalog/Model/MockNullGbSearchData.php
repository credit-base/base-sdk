<?php
namespace Base\Sdk\ResourceCatalog\Model;

class MockNullGbSearchData extends NullGbSearchData
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
