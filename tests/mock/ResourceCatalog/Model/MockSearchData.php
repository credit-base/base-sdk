<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Base\Sdk\Common\Adapter\IEnableAbleAdapter;

use Base\Sdk\ResourceCatalog\Repository\WbjSearchDataRepository;
use Base\Sdk\Template\Model\WbjTemplate;

class MockSearchData extends SearchData
{
    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->itemsData = new ItemsData();
        $this->template = new WbjTemplate();
        $this->repository = new WbjSearchDataRepository();
    }

    public function setStatus(int $status) : void
    {
        unset($status);
    }

    public function getItemsData()
    {
        return $this->itemsData;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->repository;
    }
}
