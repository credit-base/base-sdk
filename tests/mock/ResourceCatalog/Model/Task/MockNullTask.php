<?php
namespace Base\Sdk\ResourceCatalog\Model\Task;

class MockNullTask extends NullTask
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
