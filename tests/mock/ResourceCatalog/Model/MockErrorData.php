<?php
namespace Base\Sdk\ResourceCatalog\Model;

use Base\Sdk\ResourceCatalog\Repository\ErrorDataRepository;

class MockErrorData extends ErrorData
{
    public function getRepository() : ErrorDataRepository
    {
        return parent::getRepository();
    }
}
