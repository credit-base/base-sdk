<?php
namespace Base\Sdk\Template\CommandHandler\QzjTemplate;

use Base\Sdk\Template\Model\QzjTemplate;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class MockAddQzjTemplateCommandHandler extends AddQzjTemplateCommandHandler
{
    public function getQzjTemplate() : QzjTemplate
    {
        return parent::getQzjTemplate();
    }
    
    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }
}
