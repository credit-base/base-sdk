<?php
namespace Base\Sdk\Template\CommandHandler\QzjTemplate;

use Base\Sdk\Template\Repository\QzjTemplateRepository;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class MockEditQzjTemplateCommandHandler extends EditQzjTemplateCommandHandler
{
    public function getRepository() : QzjTemplateRepository
    {
        return parent::getRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }
}
