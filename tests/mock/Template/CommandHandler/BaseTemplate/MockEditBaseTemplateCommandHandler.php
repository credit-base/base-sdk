<?php
namespace Base\Sdk\Template\CommandHandler\BaseTemplate;

use Base\Sdk\Template\Repository\BaseTemplateRepository;

class MockEditBaseTemplateCommandHandler extends EditBaseTemplateCommandHandler
{
    public function getRepository() : BaseTemplateRepository
    {
        return parent::getRepository();
    }
}
