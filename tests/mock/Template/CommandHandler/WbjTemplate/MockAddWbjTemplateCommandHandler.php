<?php
namespace Base\Sdk\Template\CommandHandler\WbjTemplate;

use Base\Sdk\Template\Model\WbjTemplate;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class MockAddWbjTemplateCommandHandler extends AddWbjTemplateCommandHandler
{
    public function getWbjTemplate() : WbjTemplate
    {
        return parent::getWbjTemplate();
    }
    
    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }
}
