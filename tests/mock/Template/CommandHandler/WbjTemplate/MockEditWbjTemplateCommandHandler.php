<?php
namespace Base\Sdk\Template\CommandHandler\WbjTemplate;

use Base\Sdk\Template\Repository\WbjTemplateRepository;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class MockEditWbjTemplateCommandHandler extends EditWbjTemplateCommandHandler
{
    public function getRepository() : WbjTemplateRepository
    {
        return parent::getRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }
}
