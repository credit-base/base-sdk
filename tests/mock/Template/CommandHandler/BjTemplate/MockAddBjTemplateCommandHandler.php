<?php
namespace Base\Sdk\Template\CommandHandler\BjTemplate;

use Base\Sdk\Template\Model\BjTemplate;
use Base\Sdk\Template\Repository\GbTemplateRepository;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class MockAddBjTemplateCommandHandler extends AddBjTemplateCommandHandler
{
    public function getBjTemplate() : BjTemplate
    {
        return parent::getBjTemplate();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getGbTemplateRepository() : GbTemplateRepository
    {
        return parent::getGbTemplateRepository();
    }
}
