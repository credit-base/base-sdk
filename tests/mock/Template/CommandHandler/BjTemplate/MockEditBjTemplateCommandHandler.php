<?php
namespace Base\Sdk\Template\CommandHandler\BjTemplate;

use Base\Sdk\Template\Repository\BjTemplateRepository;
use Base\Sdk\Template\Repository\GbTemplateRepository;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;

class MockEditBjTemplateCommandHandler extends EditBjTemplateCommandHandler
{
    public function getRepository() : BjTemplateRepository
    {
        return parent::getRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getGbTemplateRepository() : GbTemplateRepository
    {
        return parent::getGbTemplateRepository();
    }
}
