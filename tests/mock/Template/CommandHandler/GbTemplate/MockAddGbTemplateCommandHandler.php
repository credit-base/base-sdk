<?php
namespace Base\Sdk\Template\CommandHandler\GbTemplate;

use Base\Sdk\Template\Model\GbTemplate;

class MockAddGbTemplateCommandHandler extends AddGbTemplateCommandHandler
{
    public function getGbTemplate() : GbTemplate
    {
        return parent::getGbTemplate();
    }
}
