<?php
namespace Base\Sdk\Template\CommandHandler\GbTemplate;

use Base\Sdk\Template\Repository\GbTemplateRepository;

class MockEditGbTemplateCommandHandler extends EditGbTemplateCommandHandler
{
    public function getRepository() : GbTemplateRepository
    {
        return parent::getRepository();
    }
}
