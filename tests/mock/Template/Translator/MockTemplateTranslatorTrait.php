<?php
namespace Base\Sdk\Template\Translator;

trait MockTemplateTranslatorTrait
{
    public function getSubjectCategoryCn($subjectCategory)
    {
        return parent::getSubjectCategoryCn($subjectCategory);
    }

    public function getIsNecessaryCn($isNecessary)
    {
        return parent::getIsNecessaryCn($isNecessary);
    }

    public function getItemCn($items)
    {
        return parent::getItemCn($items);
    }

    public function getDimensionCn($dimension)
    {
        return parent::getDimensionCn($dimension);
    }

    public function getTypeCn($type)
    {
        return parent::getTypeCn($type);
    }

    public function getIsMaskedCn($isMasked)
    {
        return parent::getIsMaskedCn($isMasked);
    }
}
