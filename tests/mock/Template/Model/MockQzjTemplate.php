<?php
namespace Base\Sdk\Template\Model;

use Base\Sdk\Common\Adapter\IEnableAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\ITopAbleAdapter;

class MockQzjTemplate extends QzjTemplate
{
    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }
}
