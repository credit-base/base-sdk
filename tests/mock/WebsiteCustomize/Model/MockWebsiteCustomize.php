<?php
namespace Base\Sdk\WebsiteCustomize\Model;

use Base\Sdk\Common\Adapter\IEnableAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

class MockWebsiteCustomize extends WebsiteCustomize
{
    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }
}
