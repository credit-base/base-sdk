<?php
namespace Base\Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Base\Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Base\Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

trait MockWebsiteCustomizeCommandHandlerTrait
{
    public function getWebsiteCustomize() : WebsiteCustomize
    {
        return parent::getWebsiteCustomize();
    }

    public function getRepository() : WebsiteCustomizeRepository
    {
        return parent::getRepository();
    }

    public function fetchWebsiteCustomize(int $id) : WebsiteCustomize
    {
        return parent::fetchWebsiteCustomize($id);
    }
}
