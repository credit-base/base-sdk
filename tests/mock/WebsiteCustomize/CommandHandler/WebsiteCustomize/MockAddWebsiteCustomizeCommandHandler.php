<?php
namespace Base\Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize;

class MockAddWebsiteCustomizeCommandHandler extends AddWebsiteCustomizeCommandHandler
{
    use MockWebsiteCustomizeCommandHandlerTrait;
}
