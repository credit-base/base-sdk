<?php
namespace Base\Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize;

class MockPublishWebsiteCustomizeCommandHandler extends PublishWebsiteCustomizeCommandHandler
{
    use MockWebsiteCustomizeCommandHandlerTrait;
}
