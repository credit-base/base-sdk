<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Command\RejectCommand;

class MockRejectCreditPhotographyCommandHandler extends RejectCreditPhotographyCommandHandler
{
    use MockCreditPhotographyCommandHandlerTrait;
    
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
