<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

class MockDeleteCreditPhotographyCommandHandler extends DeleteCreditPhotographyCommandHandler
{
    use MockCreditPhotographyCommandHandlerTrait;
}
