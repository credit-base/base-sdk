<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository;

trait MockCreditPhotographyCommandHandlerTrait
{
    public function getCreditPhotography() : CreditPhotography
    {
        return parent::getCreditPhotography();
    }

    public function getRepository() : CreditPhotographyRepository
    {
        return parent::getRepository();
    }

    public function fetchCreditPhotography(int $id) : CreditPhotography
    {
        return parent::fetchCreditPhotography($id);
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function fetchCrew(int $id) : Crew
    {
        return parent::fetchCrew($id);
    }
}
