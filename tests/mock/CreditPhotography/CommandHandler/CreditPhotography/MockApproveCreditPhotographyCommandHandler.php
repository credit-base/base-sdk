<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Command\ApproveCommand;

class MockApproveCreditPhotographyCommandHandler extends ApproveCreditPhotographyCommandHandler
{
    use MockCreditPhotographyCommandHandlerTrait;
    
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
