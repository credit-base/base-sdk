<?php
namespace Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;

class MockAddCreditPhotographyCommandHandler extends AddCreditPhotographyCommandHandler
{
    public function getCreditPhotography() : CreditPhotography
    {
        return parent::getCreditPhotography();
    }
}
