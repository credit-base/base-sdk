<?php
namespace Base\Sdk\CreditPhotography\Model;

use Base\Sdk\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

class MockCreditPhotography extends CreditPhotography
{
    public function getRepository() : CreditPhotographyRepository
    {
        return parent::getRepository();
    }

    public function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return parent::getIApplyAbleAdapter();
    }

    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }
}
