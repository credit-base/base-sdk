<?php
namespace Base\Sdk\Journal\CommandHandler\UnAuditJournal;

use Base\Sdk\Journal\Repository\UnAuditJournalRepository;
use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\Journal\Model\UnAuditJournal;

class MockEditUnAuditJournalCommandHandler extends EditUnAuditJournalCommandHandler
{
    use MockUnAuditJournalCommandHandlerTrait;
}
