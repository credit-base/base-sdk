<?php
namespace Base\Sdk\Journal\CommandHandler\UnAuditJournal;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Command\RejectCommand;

class MockRejectUnAuditJournalCommandHandler extends RejectUnAuditJournalCommandHandler
{
    use MockUnAuditJournalCommandHandlerTrait;
    
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }

    public function executeAction(RejectCommand $command)
    {
        return parent::executeAction($command);
    }
}
