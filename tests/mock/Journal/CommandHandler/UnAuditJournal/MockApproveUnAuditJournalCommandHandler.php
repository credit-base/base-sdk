<?php
namespace Base\Sdk\Journal\CommandHandler\UnAuditJournal;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Command\ApproveCommand;

class MockApproveUnAuditJournalCommandHandler extends ApproveUnAuditJournalCommandHandler
{
    use MockUnAuditJournalCommandHandlerTrait;
    
    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }

    public function executeAction(ApproveCommand $command)
    {
        return parent::executeAction($command);
    }
}
