<?php
namespace Base\Sdk\Journal\CommandHandler\UnAuditJournal;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\Journal\Model\UnAuditJournal;
use Base\Sdk\Journal\Repository\UnAuditJournalRepository;

trait MockUnAuditJournalCommandHandlerTrait
{
    public function getUnAuditJournal() : UnAuditJournal
    {
        return parent::getUnAuditJournal();
    }

    public function getRepository() : UnAuditJournalRepository
    {
        return parent::getRepository();
    }

    public function fetchUnAuditJournal(int $id) : UnAuditJournal
    {
        return parent::fetchUnAuditJournal($id);
    }

    // public function getCrewRepository() : CrewRepository
    // {
    //     return parent::getCrewRepository();
    // }

    // public function fetchCrew(int $id) : Crew
    // {
    //     return parent::fetchCrew($id);
    // }
}
