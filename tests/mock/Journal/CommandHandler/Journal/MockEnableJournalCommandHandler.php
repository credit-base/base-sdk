<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Base\Sdk\Common\Model\IEnableAble;

use Base\Sdk\Common\Command\EnableCommand;

class MockEnableJournalCommandHandler extends EnableJournalCommandHandler
{
    use MockJournalCommandHandlerTrait;
    
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
    
    public function executeAction(EnableCommand $command)
    {
        return parent::executeAction($command);
    }
}
