<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Base\Sdk\Common\Model\IEnableAble;

use Base\Sdk\Common\Command\DisableCommand;

class MockDisableJournalCommandHandler extends DisableJournalCommandHandler
{
    use MockJournalCommandHandlerTrait;

    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }

    public function executeAction(DisableCommand $command)
    {
        return parent::executeAction($command);
    }
}
