<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\Journal\Repository\JournalRepository;

trait MockJournalCommandHandlerTrait
{
    public function getJournal() : Journal
    {
        return parent::getJournal();
    }

    public function getRepository() : JournalRepository
    {
        return parent::getRepository();
    }

    public function fetchJournal(int $id) : Journal
    {
        return parent::fetchJournal($id);
    }

    // public function getCrewRepository() : CrewRepository
    // {
    //     return parent::getCrewRepository();
    // }

    // public function fetchCrew(int $id) : Crew
    // {
    //     return parent::fetchCrew($id);
    // }
}
