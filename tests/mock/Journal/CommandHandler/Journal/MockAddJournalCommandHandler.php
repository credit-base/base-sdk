<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Base\Sdk\Journal\Model\Journal;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\UserGroup\Repository\DepartmentRepository;

class MockAddJournalCommandHandler extends AddJournalCommandHandler
{
    use MockJournalCommandHandlerTrait;
}
