<?php
namespace Base\Sdk\Journal\CommandHandler\Journal;

use Base\Sdk\Journal\Repository\JournalRepository;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\UserGroup\Repository\DepartmentRepository;

class MockEditJournalCommandHandler extends EditJournalCommandHandler
{
    use MockJournalCommandHandlerTrait;
}
