<?php
namespace Base\Sdk\News\CommandHandler\UnAuditNews;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\News\Model\UnAuditNews;
use Base\Sdk\News\Repository\UnAuditNewsRepository;

trait MockUnAuditNewsCommandHandlerTrait
{
    public function getUnAuditNews() : UnAuditNews
    {
        return parent::getUnAuditNews();
    }

    public function getRepository() : UnAuditNewsRepository
    {
        return parent::getRepository();
    }

    public function fetchUnAuditNews(int $id) : UnAuditNews
    {
        return parent::fetchUnAuditNews($id);
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function fetchCrew(int $id) : Crew
    {
        return parent::fetchCrew($id);
    }
}
