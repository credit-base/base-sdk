<?php
namespace Base\Sdk\News\CommandHandler\UnAuditNews;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Command\RejectCommand;

class MockRejectUnAuditNewsCommandHandler extends RejectUnAuditNewsCommandHandler
{
    use MockUnAuditNewsCommandHandlerTrait;

    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }

    public function executeAction(RejectCommand $command)
    {
        return parent::executeAction($command);
    }
}
