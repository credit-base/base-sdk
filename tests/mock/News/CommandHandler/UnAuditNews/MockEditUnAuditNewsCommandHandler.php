<?php
namespace Base\Sdk\News\CommandHandler\UnAuditNews;

class MockEditUnAuditNewsCommandHandler extends EditUnAuditNewsCommandHandler
{
    use MockUnAuditNewsCommandHandlerTrait;
}
