<?php
namespace Base\Sdk\News\CommandHandler\UnAuditNews;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Command\ApproveCommand;

class MockApproveUnAuditNewsCommandHandler extends ApproveUnAuditNewsCommandHandler
{
    use MockUnAuditNewsCommandHandlerTrait;

    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }

    public function executeAction(ApproveCommand $command)
    {
        return parent::executeAction($command);
    }
}
