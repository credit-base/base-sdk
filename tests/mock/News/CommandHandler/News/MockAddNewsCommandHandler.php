<?php
namespace Base\Sdk\News\CommandHandler\News;

class MockAddNewsCommandHandler extends AddNewsCommandHandler
{
    use MockNewsCommandHandlerTrait;
}
