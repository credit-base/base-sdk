<?php
namespace Base\Sdk\News\CommandHandler\News;

use Base\Sdk\Common\Model\ITopAble;
use Base\Sdk\Common\Command\CancelTopCommand;

class MockCancelTopNewsCommandHandler extends CancelTopNewsCommandHandler
{
    use MockNewsCommandHandlerTrait;

    public function fetchITopObject($id) : ITopAble
    {
        return parent::fetchITopObject($id);
    }

    public function executeAction(CancelTopCommand $command)
    {
        return parent::executeAction($command);
    }
}
