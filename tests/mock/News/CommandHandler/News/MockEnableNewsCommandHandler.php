<?php
namespace Base\Sdk\News\CommandHandler\News;

use Base\Sdk\Common\Model\IEnableAble;

use Base\Sdk\Common\Command\EnableCommand;

class MockEnableNewsCommandHandler extends EnableNewsCommandHandler
{
    use MockNewsCommandHandlerTrait;

    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
    
    public function executeAction(EnableCommand $command)
    {
        return parent::executeAction($command);
    }
}
