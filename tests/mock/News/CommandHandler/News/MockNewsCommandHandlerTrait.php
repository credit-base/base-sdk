<?php
namespace Base\Sdk\News\CommandHandler\News;

use Base\Sdk\Crew\Model\Crew;
use Base\Sdk\Crew\Repository\CrewRepository;

use Base\Sdk\News\Model\News;
use Base\Sdk\News\Repository\NewsRepository;

trait MockNewsCommandHandlerTrait
{
    public function getNews() : News
    {
        return parent::getNews();
    }

    public function getRepository() : NewsRepository
    {
        return parent::getRepository();
    }

    public function fetchNews(int $id) : News
    {
        return parent::fetchNews($id);
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function fetchCrew(int $id) : Crew
    {
        return parent::fetchCrew($id);
    }
}
