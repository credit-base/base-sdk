<?php
namespace Base\Sdk\News\CommandHandler\News;

class MockEditNewsCommandHandler extends EditNewsCommandHandler
{
    use MockNewsCommandHandlerTrait;
}
