<?php
namespace Base\Sdk\News\CommandHandler\News;

use Base\Sdk\Common\Model\ITopAble;
use Base\Sdk\Common\Command\TopCommand;

class MockTopNewsCommandHandler extends TopNewsCommandHandler
{
    use MockNewsCommandHandlerTrait;

    public function fetchITopObject($id) : ITopAble
    {
        return parent::fetchITopObject($id);
    }

    public function executeAction(TopCommand $command)
    {
        return parent::executeAction($command);
    }
}
