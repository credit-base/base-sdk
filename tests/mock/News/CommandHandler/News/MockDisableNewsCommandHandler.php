<?php
namespace Base\Sdk\News\CommandHandler\News;

use Base\Sdk\Common\Model\IEnableAble;

use Base\Sdk\Common\Command\DisableCommand;

class MockDisableNewsCommandHandler extends DisableNewsCommandHandler
{
    use MockNewsCommandHandlerTrait;

    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }

    public function executeAction(DisableCommand $command)
    {
        return parent::executeAction($command);
    }
}
