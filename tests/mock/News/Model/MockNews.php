<?php
namespace Base\Sdk\News\Model;

use Base\Sdk\Common\Adapter\IEnableAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\ITopAbleAdapter;

class MockNews extends News
{
    public function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return parent::getIEnableAbleAdapter();
    }

    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function getITopAbleAdapter() : ITopAbleAdapter
    {
        return parent::getITopAbleAdapter();
    }
}
