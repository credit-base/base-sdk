<?php
namespace Base\Sdk\News\Model;

use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;

use Base\Sdk\News\Repository\UnAuditNewsRepository;

class MockUnAuditNews extends UnAuditNews
{
    public function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return parent::getIApplyAbleAdapter();
    }

    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function getUnAuditNewsRepository() : UnAuditNewsRepository
    {
        return parent::getUnAuditNewsRepository();
    }
}
