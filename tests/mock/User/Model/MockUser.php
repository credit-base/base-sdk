<?php
namespace Base\Sdk\User\Model;

use Base\Sdk\Common\Adapter\IEnableAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

use Base\Sdk\Crew\Repository\CrewRepository;

class MockUser extends User
{
    public function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return parent::getIEnableAbleAdapter();
    }

    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function getRepository()
    {
        return new CrewRepository();
    }
}
