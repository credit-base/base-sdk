<?php
namespace Base\Sdk\Statistical\Translator;

class MockStatisticalRestfulTranslator extends StatisticalRestfulTranslator
{
    public function translateToObject(array $expression, $statistical = null)
    {
        return parent::translateToObject($expression, $statistical);
    }
}
