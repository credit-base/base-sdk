<?php

namespace Base\Sdk\Statistical\Adapter\Statistical;

use Marmot\Interfaces\INull;

class MockStatisticalRestfulAdapter extends StatisticalRestfulAdapter
{
    use MockStaticsAdapterTrait;

    public function getScenario() : array
    {
        return parent::getScenario();
    }

    protected function fetchOneAction(int $id, INull $null)
    {
        return parent::fetchOneAction($id, $null);
    }
}
