<?php
namespace Base\Sdk\Statistical\Adapter\Statistical;

class MockStaticsEnterpriseRelationInformationCountAdapter extends StaticsEnterpriseRelationInformationCountAdapter
{
    use MockStaticsAdapterTrait;
}
