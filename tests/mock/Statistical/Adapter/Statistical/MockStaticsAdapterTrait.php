<?php
namespace Base\Sdk\Statistical\Adapter\Statistical;

use Marmot\Interfaces\IRestfulTranslator;

trait MockStaticsAdapterTrait
{
    public function getResource() : string
    {
        return parent::getResource();
    }

    public function get(string $url, array $query = array(), array $requestHeaders = array())
    {
        parent::get($url, $query, $requestHeaders);
    }

    public function getAsync(string $url, array $query = array(), array $requestHeaders = array())
    {
        parent::getAsync($url, $query, $requestHeaders);
    }

    public function isSuccess() : bool
    {
        return parent::isSuccess();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }
}
