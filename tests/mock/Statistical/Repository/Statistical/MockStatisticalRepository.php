<?php


namespace Base\Sdk\Statistical\Repository\Statistical;

use Base\Sdk\Statistical\Adapter\Statistical\IAdapterFactory;
use Base\Sdk\Statistical\Adapter\Statistical\IStatisticalAdapter;

class MockStatisticalRepository extends StatisticalRepository
{
    public function __construct()
    {
        $adapter = IAdapterFactory::MAPS['enterpriseRelationInformationCount'];
        $adapter = new $adapter;
        parent::__construct($adapter);
    }
}
