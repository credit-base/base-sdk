<?php
namespace Base\Sdk\Interaction\CommandHandler\Complaint;

class MockAcceptComplaintCommandHandler extends AcceptComplaintCommandHandler
{
    use MockComplaintCommandHandlerTrait;

    public function getReplyObject($interaction, array $reply)
    {
        return parent::getReplyObject($interaction, $reply);
    }
}
