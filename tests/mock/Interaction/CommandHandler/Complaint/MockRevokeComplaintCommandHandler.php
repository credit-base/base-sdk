<?php
namespace Base\Sdk\Interaction\CommandHandler\Complaint;

use Base\Sdk\Common\Model\IModifyStatusAble;

class MockRevokeComplaintCommandHandler extends RevokeComplaintCommandHandler
{
    use MockComplaintCommandHandlerTrait;

    public function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return parent::fetchIModifyStatusObject($id);
    }
}
