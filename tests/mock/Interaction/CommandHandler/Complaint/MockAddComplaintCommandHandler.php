<?php
namespace Base\Sdk\Interaction\CommandHandler\Complaint;

class MockAddComplaintCommandHandler extends AddComplaintCommandHandler
{
    use MockComplaintCommandHandlerTrait;
}
