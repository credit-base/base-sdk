<?php
namespace Base\Sdk\Interaction\CommandHandler\Complaint;

use Base\Sdk\Interaction\Model\Complaint;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Interaction\Repository\ComplaintRepository;

trait MockComplaintCommandHandlerTrait
{
    use ComplaintCommandHandlerTrait;

    public function getPublicRepository() : ComplaintRepository
    {
        return $this->getRepository();
    }

    public function getPublicUserGroupRepository() : UserGroupRepository
    {
        return $this->getUserGroupRepository();
    }

    public function publicFetchComplaint(int $id) : Complaint
    {
        return $this->fetchComplaint($id);
    }

    public function getPublicComplaint():Complaint
    {
        return $this->getComplaint();
    }

    public function publicFetchUserGroup(int $id) : UserGroup
    {
        return $this->fetchUserGroup($id);
    }
}
