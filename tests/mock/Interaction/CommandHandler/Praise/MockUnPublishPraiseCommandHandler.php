<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

class MockUnPublishPraiseCommandHandler extends UnPublishPraiseCommandHandler
{
    use MockPraiseCommandHandlerTrait;

    public function executeAction($command)
    {
        return parent::executeAction($command);
    }
}
