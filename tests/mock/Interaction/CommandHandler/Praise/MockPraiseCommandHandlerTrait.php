<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Base\Sdk\Interaction\Model\Praise;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Interaction\Repository\PraiseRepository;

trait MockPraiseCommandHandlerTrait
{
    use PraiseCommandHandlerTrait;

    public function getPublicRepository() : PraiseRepository
    {
        return $this->getRepository();
    }

    public function getPublicUserGroupRepository() : UserGroupRepository
    {
        return $this->getUserGroupRepository();
    }

    public function publicFetchPraise(int $id) : Praise
    {
        return $this->fetchPraise($id);
    }

    public function getPublicPraise():Praise
    {
        return $this->getPraise();
    }

    public function publicFetchUserGroup(int $id) : UserGroup
    {
        return $this->fetchUserGroup($id);
    }
}
