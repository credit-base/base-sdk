<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

class MockPublishPraiseCommandHandler extends PublishPraiseCommandHandler
{
    use MockPraiseCommandHandlerTrait;

    public function executeAction($command)
    {
        return parent::executeAction($command);
    }
}
