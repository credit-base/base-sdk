<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

use Base\Sdk\Common\Model\IModifyStatusAble;

class MockRevokePraiseCommandHandler extends RevokePraiseCommandHandler
{
    use MockPraiseCommandHandlerTrait;

    public function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return parent::fetchIModifyStatusObject($id);
    }
}
