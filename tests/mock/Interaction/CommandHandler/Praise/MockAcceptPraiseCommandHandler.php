<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

class MockAcceptPraiseCommandHandler extends AcceptPraiseCommandHandler
{
    use MockPraiseCommandHandlerTrait;

    public function getReplyObject($interaction, array $reply)
    {
        return parent::getReplyObject($interaction, $reply);
    }
}
