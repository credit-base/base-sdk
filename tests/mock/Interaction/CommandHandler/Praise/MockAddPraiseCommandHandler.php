<?php
namespace Base\Sdk\Interaction\CommandHandler\Praise;

class MockAddPraiseCommandHandler extends AddPraiseCommandHandler
{
    use MockPraiseCommandHandlerTrait;
}
