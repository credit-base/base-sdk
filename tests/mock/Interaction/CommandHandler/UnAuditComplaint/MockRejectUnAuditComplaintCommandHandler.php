<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditComplaint;

class MockRejectUnAuditComplaintCommandHandler extends RejectUnAuditComplaintCommandHandler
{
    use MockUnAuditComplaintCommandHandlerTrait;
}
