<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditComplaint;

class MockResubmitUnAuditComplaintCommandHandler extends ResubmitUnAuditComplaintCommandHandler
{
    use MockUnAuditComplaintCommandHandlerTrait;

    public function getReplyObject($interaction, array $reply)
    {
        return parent::getReplyObject($interaction, $reply);
    }
}
