<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditComplaint;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Interaction\Model\UnAuditComplaint;
use Base\Sdk\Interaction\Repository\UnAuditComplaintRepository;

trait MockUnAuditComplaintCommandHandlerTrait
{
    use UnAuditComplaintCommandHandlerTrait;

    public function getPublicRepository() : UnAuditComplaintRepository
    {
        return $this->getRepository();
    }

    public function publicFetchUnAuditComplaint(int $id) : UnAuditComplaint
    {
        return $this->fetchUnAuditComplaint($id);
    }

    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
