<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditComplaint;

class MockApproveUnAuditComplaintCommandHandler extends ApproveUnAuditComplaintCommandHandler
{
    use MockUnAuditComplaintCommandHandlerTrait;
}
