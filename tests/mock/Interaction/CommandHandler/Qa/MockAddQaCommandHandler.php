<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

class MockAddQaCommandHandler extends AddQaCommandHandler
{
    use MockQaCommandHandlerTrait;
}
