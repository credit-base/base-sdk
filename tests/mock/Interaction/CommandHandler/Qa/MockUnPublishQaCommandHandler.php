<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

class MockUnPublishQaCommandHandler extends UnPublishQaCommandHandler
{
    use MockQaCommandHandlerTrait;

    public function executeAction($command)
    {
        return parent::executeAction($command);
    }
}
