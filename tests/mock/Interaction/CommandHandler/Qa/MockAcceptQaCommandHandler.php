<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

class MockAcceptQaCommandHandler extends AcceptQaCommandHandler
{
    use MockQaCommandHandlerTrait;

    public function getReplyObject($interaction, array $reply)
    {
        return parent::getReplyObject($interaction, $reply);
    }
}
