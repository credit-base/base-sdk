<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Base\Sdk\Common\Model\IModifyStatusAble;

class MockRevokeQaCommandHandler extends RevokeQaCommandHandler
{
    use MockQaCommandHandlerTrait;

    public function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return parent::fetchIModifyStatusObject($id);
    }
}
