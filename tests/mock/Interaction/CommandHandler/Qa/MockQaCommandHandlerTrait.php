<?php
namespace Base\Sdk\Interaction\CommandHandler\Qa;

use Base\Sdk\Interaction\Model\Qa;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Interaction\Repository\QaRepository;

trait MockQaCommandHandlerTrait
{
    use QaCommandHandlerTrait;

    public function getPublicRepository() : QaRepository
    {
        return $this->getRepository();
    }

    public function getPublicUserGroupRepository() : UserGroupRepository
    {
        return $this->getUserGroupRepository();
    }

    public function publicFetchQa(int $id) : Qa
    {
        return $this->fetchQa($id);
    }

    public function getPublicQa():Qa
    {
        return $this->getQa();
    }

    public function publicFetchUserGroup(int $id) : UserGroup
    {
        return $this->fetchUserGroup($id);
    }
}
