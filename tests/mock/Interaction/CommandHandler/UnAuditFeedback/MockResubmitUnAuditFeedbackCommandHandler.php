<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditFeedback;

class MockResubmitUnAuditFeedbackCommandHandler extends ResubmitUnAuditFeedbackCommandHandler
{
    use MockUnAuditFeedbackCommandHandlerTrait;

    public function getReplyObject($interaction, array $reply)
    {
        return parent::getReplyObject($interaction, $reply);
    }
}
