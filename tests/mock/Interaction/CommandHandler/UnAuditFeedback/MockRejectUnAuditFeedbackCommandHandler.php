<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditFeedback;

class MockRejectUnAuditFeedbackCommandHandler extends RejectUnAuditFeedbackCommandHandler
{
    use MockUnAuditFeedbackCommandHandlerTrait;
}
