<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditFeedback;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Interaction\Model\UnAuditFeedback;
use Base\Sdk\Interaction\Repository\UnAuditFeedbackRepository;

trait MockUnAuditFeedbackCommandHandlerTrait
{
    use UnAuditFeedbackCommandHandlerTrait;

    public function getPublicRepository() : UnAuditFeedbackRepository
    {
        return $this->getRepository();
    }

    public function publicFetchUnAuditFeedback(int $id) : UnAuditFeedback
    {
        return $this->fetchUnAuditFeedback($id);
    }

    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
