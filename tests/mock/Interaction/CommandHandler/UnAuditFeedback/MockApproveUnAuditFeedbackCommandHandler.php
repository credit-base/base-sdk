<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditFeedback;

class MockApproveUnAuditFeedbackCommandHandler extends ApproveUnAuditFeedbackCommandHandler
{
    use MockUnAuditFeedbackCommandHandlerTrait;
}
