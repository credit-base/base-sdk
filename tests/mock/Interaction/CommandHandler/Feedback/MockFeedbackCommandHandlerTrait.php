<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

use Base\Sdk\Interaction\Model\Feedback;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Interaction\Repository\FeedbackRepository;

trait MockFeedbackCommandHandlerTrait
{
    use FeedbackCommandHandlerTrait;

    public function getPublicRepository() : FeedbackRepository
    {
        return $this->getRepository();
    }

    public function getPublicUserGroupRepository() : UserGroupRepository
    {
        return $this->getUserGroupRepository();
    }

    public function publicFetchFeedback(int $id) : Feedback
    {
        return $this->fetchFeedback($id);
    }

    public function getPublicFeedback():Feedback
    {
        return $this->getFeedback();
    }

    public function publicFetchUserGroup(int $id) : UserGroup
    {
        return $this->fetchUserGroup($id);
    }
}
