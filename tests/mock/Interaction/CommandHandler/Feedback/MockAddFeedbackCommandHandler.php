<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

class MockAddFeedbackCommandHandler extends AddFeedbackCommandHandler
{
    use MockFeedbackCommandHandlerTrait;
}
