<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

use Base\Sdk\Common\Model\IModifyStatusAble;

class MockRevokeFeedbackCommandHandler extends RevokeFeedbackCommandHandler
{
    use MockFeedbackCommandHandlerTrait;

    public function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return parent::fetchIModifyStatusObject($id);
    }
}
