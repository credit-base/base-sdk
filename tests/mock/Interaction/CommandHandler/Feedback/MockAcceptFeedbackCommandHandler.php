<?php
namespace Base\Sdk\Interaction\CommandHandler\Feedback;

class MockAcceptFeedbackCommandHandler extends AcceptFeedbackCommandHandler
{
    use MockFeedbackCommandHandlerTrait;

    public function getReplyObject($interaction, array $reply)
    {
        return parent::getReplyObject($interaction, $reply);
    }
}
