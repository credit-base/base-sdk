<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditQa;

class MockRejectUnAuditQaCommandHandler extends RejectUnAuditQaCommandHandler
{
    use MockUnAuditQaCommandHandlerTrait;
}
