<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditQa;

class MockResubmitUnAuditQaCommandHandler extends ResubmitUnAuditQaCommandHandler
{
    use MockUnAuditQaCommandHandlerTrait;

    public function getReplyObject($interaction, array $reply)
    {
        return parent::getReplyObject($interaction, $reply);
    }
}
