<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditQa;

class MockApproveUnAuditQaCommandHandler extends ApproveUnAuditQaCommandHandler
{
    use MockUnAuditQaCommandHandlerTrait;
}
