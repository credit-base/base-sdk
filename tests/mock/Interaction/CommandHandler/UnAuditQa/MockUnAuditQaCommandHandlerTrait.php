<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditQa;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Interaction\Model\UnAuditQa;
use Base\Sdk\Interaction\Repository\UnAuditQaRepository;

trait MockUnAuditQaCommandHandlerTrait
{
    use UnAuditQaCommandHandlerTrait;

    public function getPublicRepository() : UnAuditQaRepository
    {
        return $this->getRepository();
    }

    public function publicFetchUnAuditQa(int $id) : UnAuditQa
    {
        return $this->fetchUnAuditQa($id);
    }

    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
