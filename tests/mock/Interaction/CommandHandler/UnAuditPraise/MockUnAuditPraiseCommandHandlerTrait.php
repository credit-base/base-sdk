<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditPraise;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Interaction\Model\UnAuditPraise;
use Base\Sdk\Interaction\Repository\UnAuditPraiseRepository;

trait MockUnAuditPraiseCommandHandlerTrait
{
    use UnAuditPraiseCommandHandlerTrait;

    public function getPublicRepository() : UnAuditPraiseRepository
    {
        return $this->getRepository();
    }

    public function publicFetchUnAuditPraise(int $id) : UnAuditPraise
    {
        return $this->fetchUnAuditPraise($id);
    }

    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
