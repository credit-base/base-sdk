<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditPraise;

class MockRejectUnAuditPraiseCommandHandler extends RejectUnAuditPraiseCommandHandler
{
    use MockUnAuditPraiseCommandHandlerTrait;
}
