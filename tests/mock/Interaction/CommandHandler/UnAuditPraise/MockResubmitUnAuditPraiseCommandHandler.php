<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditPraise;

class MockResubmitUnAuditPraiseCommandHandler extends ResubmitUnAuditPraiseCommandHandler
{
    use MockUnAuditPraiseCommandHandlerTrait;

    public function getReplyObject($interaction, array $reply)
    {
        return parent::getReplyObject($interaction, $reply);
    }
}
