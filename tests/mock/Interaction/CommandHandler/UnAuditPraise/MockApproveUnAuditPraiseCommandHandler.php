<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditPraise;

class MockApproveUnAuditPraiseCommandHandler extends ApproveUnAuditPraiseCommandHandler
{
    use MockUnAuditPraiseCommandHandlerTrait;
}
