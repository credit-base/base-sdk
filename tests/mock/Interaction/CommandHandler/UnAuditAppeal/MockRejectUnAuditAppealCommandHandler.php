<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditAppeal;

class MockRejectUnAuditAppealCommandHandler extends RejectUnAuditAppealCommandHandler
{
    use MockUnAuditAppealCommandHandlerTrait;
}
