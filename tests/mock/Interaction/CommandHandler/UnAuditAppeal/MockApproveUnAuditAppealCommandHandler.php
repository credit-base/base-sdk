<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditAppeal;

class MockApproveUnAuditAppealCommandHandler extends ApproveUnAuditAppealCommandHandler
{
    use MockUnAuditAppealCommandHandlerTrait;
}
