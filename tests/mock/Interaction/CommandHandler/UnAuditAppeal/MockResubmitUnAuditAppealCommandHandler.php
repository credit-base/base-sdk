<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditAppeal;

class MockResubmitUnAuditAppealCommandHandler extends ResubmitUnAuditAppealCommandHandler
{
    use MockUnAuditAppealCommandHandlerTrait;

    public function getReplyObject($interaction, array $reply)
    {
        return parent::getReplyObject($interaction, $reply);
    }
}
