<?php
namespace Base\Sdk\Interaction\CommandHandler\UnAuditAppeal;

use Base\Sdk\Common\Model\IApplyAble;

use Base\Sdk\Interaction\Model\UnAuditAppeal;
use Base\Sdk\Interaction\Repository\UnAuditAppealRepository;

trait MockUnAuditAppealCommandHandlerTrait
{
    use UnAuditAppealCommandHandlerTrait;

    public function getPublicRepository() : UnAuditAppealRepository
    {
        return $this->getRepository();
    }

    public function publicFetchUnAuditAppeal(int $id) : UnAuditAppeal
    {
        return $this->fetchUnAuditAppeal($id);
    }

    public function fetchIApplyObject($id) : IApplyAble
    {
        return parent::fetchIApplyObject($id);
    }
}
