<?php
namespace Base\Sdk\Interaction\CommandHandler\Appeal;

use Base\Sdk\Common\Model\IModifyStatusAble;

class MockRevokeAppealCommandHandler extends RevokeAppealCommandHandler
{
    use MockAppealCommandHandlerTrait;

    public function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return parent::fetchIModifyStatusObject($id);
    }
}
