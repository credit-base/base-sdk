<?php
namespace Base\Sdk\Interaction\CommandHandler\Appeal;

class MockAddAppealCommandHandler extends AddAppealCommandHandler
{
    use MockAppealCommandHandlerTrait;
}
