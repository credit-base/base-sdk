<?php
namespace Base\Sdk\Interaction\CommandHandler\Appeal;

use Base\Sdk\Interaction\Model\Appeal;
use Base\Sdk\UserGroup\Model\UserGroup;
use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\Interaction\Repository\AppealRepository;

trait MockAppealCommandHandlerTrait
{
    use AppealCommandHandlerTrait;

    public function getPublicRepository() : AppealRepository
    {
        return $this->getRepository();
    }

    public function getPublicUserGroupRepository() : UserGroupRepository
    {
        return $this->getUserGroupRepository();
    }

    public function publicFetchAppeal(int $id) : Appeal
    {
        return $this->fetchAppeal($id);
    }

    public function getPublicAppeal():Appeal
    {
        return $this->getAppeal();
    }

    public function publicFetchUserGroup(int $id) : UserGroup
    {
        return $this->fetchUserGroup($id);
    }
}
