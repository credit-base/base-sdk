<?php
namespace Base\Sdk\Interaction\CommandHandler\Appeal;

class MockAcceptAppealCommandHandler extends AcceptAppealCommandHandler
{
    use MockAppealCommandHandlerTrait;

    public function getReplyObject($interaction, array $reply)
    {
        return parent::getReplyObject($interaction, $reply);
    }
}
