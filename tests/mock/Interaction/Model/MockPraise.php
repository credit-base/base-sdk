<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;

class MockPraise extends Praise
{
    public function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter
    {
        return parent::getIModifyStatusAbleAdapter();
    }

    public function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return parent::getIOperateAbleAdapter();
    }

    public function isAccept():bool
    {
        return parent::isAccept();
    }

    public function isPending():bool
    {
        return parent::isPending();
    }

    public function isPublish() : bool
    {
        return parent::isPublish();
    }
}
