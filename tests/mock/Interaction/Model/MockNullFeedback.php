<?php
namespace Base\Sdk\Interaction\Model;

class MockNullFeedback extends NullFeedback
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
