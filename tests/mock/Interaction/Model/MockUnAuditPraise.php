<?php
namespace Base\Sdk\Interaction\Model;

use Base\Sdk\Common\Adapter\IApplyAbleAdapter;
use Base\Sdk\Common\Adapter\IResubmitAbleAdapter;

class MockUnAuditPraise extends UnAuditPraise
{
    public function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return parent::getIApplyAbleAdapter();
    }

    public function getIResubmitAbleAdapter() : IResubmitAbleAdapter
    {
        return parent::getIResubmitAbleAdapter();
    }
}
