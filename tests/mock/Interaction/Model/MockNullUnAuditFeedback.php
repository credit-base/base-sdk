<?php
namespace Base\Sdk\Interaction\Model;

class MockNullUnAuditFeedback extends NullUnAuditFeedback
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
