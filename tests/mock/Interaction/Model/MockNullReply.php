<?php
namespace Base\Sdk\Interaction\Model;

class MockNullReply extends NullReply
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
