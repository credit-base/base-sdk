<?php
namespace Base\Sdk\Enterprise\Model;

use Base\Sdk\Enterprise\Repository\EnterpriseRepository;

class MockEnterprise extends Enterprise
{
    public function getRepository() : EnterpriseRepository
    {
        return parent::getRepository();
    }
}
