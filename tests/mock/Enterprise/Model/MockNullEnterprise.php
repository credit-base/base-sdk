<?php
namespace Base\Sdk\Enterprise\Model;

class MockNullEnterprise extends NullEnterprise
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
