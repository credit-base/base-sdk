<?php
namespace Base\Sdk\WorkOrderTask\Model;

class MockNullParentTask extends NullParentTask
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}
