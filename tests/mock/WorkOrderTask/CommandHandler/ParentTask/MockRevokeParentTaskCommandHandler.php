<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\ParentTask;

use Base\Sdk\WorkOrderTask\Repository\ParentTaskRepository;

class MockRevokeParentTaskCommandHandler extends RevokeParentTaskCommandHandler
{
    public function getRepository() : ParentTaskRepository
    {
        return parent::getRepository();
    }
}
