<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\ParentTask;

use Base\Sdk\UserGroup\Repository\UserGroupRepository;
use Base\Sdk\WorkOrderTask\Model\ParentTask;
use Marmot\Interfaces\ICommand;

class MockAssignParentTaskCommandHandler extends AssignParentTaskCommandHandler
{
    public function getParentTask() : ParentTask
    {
        return parent::getParentTask();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getTemplateRepository($templateType)
    {
        return parent::getTemplateRepository($templateType);
    }

    public function executeAction(ICommand $command)
    {
        return parent::executeAction($command);
    }
}
