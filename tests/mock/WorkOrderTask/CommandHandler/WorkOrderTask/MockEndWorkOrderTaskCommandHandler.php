<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

class MockEndWorkOrderTaskCommandHandler extends EndWorkOrderTaskCommandHandler
{
    public function getRepository() : WorkOrderTaskRepository
    {
        return parent::getRepository();
    }
}
