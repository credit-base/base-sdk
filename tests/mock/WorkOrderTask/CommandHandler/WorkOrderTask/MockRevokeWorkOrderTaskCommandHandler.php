<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

class MockRevokeWorkOrderTaskCommandHandler extends RevokeWorkOrderTaskCommandHandler
{
    public function getRepository() : WorkOrderTaskRepository
    {
        return parent::getRepository();
    }
}
