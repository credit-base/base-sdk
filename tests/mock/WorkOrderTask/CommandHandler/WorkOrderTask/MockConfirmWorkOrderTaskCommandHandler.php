<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

class MockConfirmWorkOrderTaskCommandHandler extends ConfirmWorkOrderTaskCommandHandler
{
    public function getRepository() : WorkOrderTaskRepository
    {
        return parent::getRepository();
    }
}
