<?php
namespace Base\Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use Base\Sdk\WorkOrderTask\Repository\WorkOrderTaskRepository;

class MockFeedbackWorkOrderTaskCommandHandler extends FeedbackWorkOrderTaskCommandHandler
{
    public function getRepository() : WorkOrderTaskRepository
    {
        return parent::getRepository();
    }
}
