<?php


namespace Base\Sdk\WorkOrderTask\Adapter\ParentTask;

class MockParentTaskRestfulAdapter extends ParentTaskRestfulAdapter
{
    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }
}
