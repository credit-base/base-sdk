<?php


namespace Base\Sdk\WorkOrderTask\Adapter\WorkOrderTask;

use Marmot\Interfaces\IRestfulTranslator;

class MockWorkOrderTaskRestfulAdapter extends WorkOrderTaskRestfulAdapter
{
    public function getMapErrors() : array
    {
        return parent::getMapErrors();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function isSuccess() : bool
    {
        return parent::isSuccess();
    }

    public function patch(string $url, array $data = array(), array $requestHeaders = array())
    {
        parent::patch($url, $data, $requestHeaders);
    }

    public function getResource() : string
    {
        return parent::getResource();
    }

    public function translateToObject($object = null)
    {
        return parent::translateToObject($object);
    }
}
