<?php
namespace Base\Sdk\WorkOrderTask\Translator;

class MockWorkOrderTaskTranslator extends WorkOrderTaskTranslator
{
    public function getParentTaskTranslator() : ParentTaskTranslator
    {
        return parent::getParentTaskTranslator();
    }

    public function getItemCn($items)
    {
        return parent::getItemCn($items);
    }

    public function getIsMaskedCn($isMasked)
    {
        return parent::getIsMaskedCn($isMasked);
    }

    public function getDimensionCn($dimension)
    {
        return parent::getDimensionCn($dimension);
    }

    public function getTypeCn($type)
    {
        return parent::getTypeCn($type);
    }

    public function feedbackRecords($feedbackRecords)
    {
        return parent::feedbackRecords($feedbackRecords);
    }
}
