<?php


namespace Base\Sdk\WorkOrderTask\Translator;

use Base\Sdk\UserGroup\Translator\UserGroupRestfulTranslator;

class MockParentTaskRestfulTranslator extends ParentTaskRestfulTranslator
{
    public function getUserGroupRestfulTranslator(): UserGroupRestfulTranslator
    {
        return parent::getUserGroupRestfulTranslator();
    }

    public function translateToObject(array $expression, $parentTask = null)
    {
        return parent::translateToObject($expression, $parentTask);
    }

    public function getAssignObjectsArray(array $assignObjects)
    {
        return parent::getAssignObjectsArray($assignObjects);
    }
}
