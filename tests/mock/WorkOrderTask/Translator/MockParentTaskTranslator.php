<?php

namespace Base\Sdk\WorkOrderTask\Translator;

use Base\Sdk\UserGroup\Translator\UserGroupTranslator;

class MockParentTaskTranslator extends ParentTaskTranslator
{
    public function getUserGroupTranslator() : UserGroupTranslator
    {
        return parent::getUserGroupTranslator();
    }
}
