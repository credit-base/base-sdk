<?php
namespace Base\Sdk\WorkOrderTask\Translator;

class MockWorkOrderTaskRestfulTranslator extends WorkOrderTaskRestfulTranslator
{
    public function translateToObject(array $expression, $workOrderTask = null)
    {
        return parent::translateToObject($expression, $workOrderTask);
    }
}
