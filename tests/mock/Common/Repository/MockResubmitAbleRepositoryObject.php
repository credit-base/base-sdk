<?php
namespace Base\Sdk\Common\Repository;

class MockResubmitAbleRepositoryObject
{
    use ResubmitAbleRepositoryTrait;
}
