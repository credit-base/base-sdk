<?php
namespace Base\Sdk\Common\Repository;

class MockModifyStatusAbleRepositoryObject
{
    use ModifyStatusAbleRepositoryTrait;
}
