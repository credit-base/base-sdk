<?php
namespace Base\Sdk\Common\Translator;

use Marmot\Interfaces\IRestfulTranslator;

class MockRestfulTranslator implements IRestfulTranslator
{
    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function arrayToObject(array $expression, $object = null)
    {
        unset($expression);
        return $object;
    }

    public function objectToArray($object, array $keys = array())
    {
        unset($object);
        unset($keys);
        return array();
    }
}
