<?php
namespace Base\Sdk\Common\Translator;

class MockRestfulTranslatorTrait
{
    use RestfulTranslatorTrait;

    public function publicIncludedAttributes($included)
    {
        return $this->includedAttributes($included);
    }

    public function publicIncludedRelationships($included)
    {
        return $this->includedRelationships($included);
    }

    public function publicRelationship($included, $relationships)
    {
        return $this->relationship($included, $relationships);
    }

    public function publicChangeArrayFormat($relationships, $included = array())
    {
        return $this->changeArrayFormat($relationships, $included);
    }
}
