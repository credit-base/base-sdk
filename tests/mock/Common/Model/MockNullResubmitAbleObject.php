<?php
namespace Base\Sdk\Common\Model;

class MockNullResubmitAbleObject
{
    use NullResubmitAbleTrait;

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
