<?php
namespace Base\Sdk\Common\Model;

use Base\Sdk\Common\Model\IResubmitAble;
use Base\Sdk\Common\Adapter\IResubmitAbleAdapter;
use Base\Sdk\Common\Adapter\MockResubmitAbleAdapter;

class MockResubmitAbleObject implements IResubmitAble
{
    use ResubmitAbleTrait;

    public function resubmitActionPublic(): bool
    {
        return $this->resubmitAction();
    }

    protected function getIResubmitAbleAdapter() : IResubmitAbleAdapter
    {
        return new MockResubmitAbleAdapter();
    }
}
