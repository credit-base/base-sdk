<?php
namespace Base\Sdk\Common\Model;

use Base\Sdk\Common\Model\IModifyStatusAble;
use Base\Sdk\Common\Adapter\MockModifyStatusAbleAdapter;
use Base\Sdk\Common\Adapter\IModifyStatusAbleAdapter;

class MockModifyStatusAbleObject implements IModifyStatusAble
{
    use ModifyStatusAbleTrait;

    private $status;

    private $applyStatus;

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function setApplyStatus(int $applyStatus): void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }

    // public function revoke() : bool
    // {
    //     return true;
    // }

    // public function close() : bool
    // {
    //     return true;
    // }

    // public function deletes() : bool
    // {
    //     return true;
    // }

    public function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter
    {
        return new MockModifyStatusAbleAdapter();
    }
}
