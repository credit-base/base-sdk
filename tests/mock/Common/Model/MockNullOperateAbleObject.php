<?php
namespace Base\Sdk\Common\Model;

class MockNullOperateAbleObject
{
    use NullOperateAbleTrait;

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
