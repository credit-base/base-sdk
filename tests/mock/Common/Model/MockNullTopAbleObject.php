<?php
namespace Base\Sdk\Common\Model;

class MockNullTopAbleObject
{
    use NullTopAbleTrait;

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
