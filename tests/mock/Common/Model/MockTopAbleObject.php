<?php
namespace Base\Sdk\Common\Model;

use Base\Sdk\Common\Model\ITopAble;
use Base\Sdk\Common\Adapter\ITopAbleAdapter;
use Base\Sdk\Common\Adapter\MockTopAbleAdapter;

class MockTopAbleObject implements ITopAble
{
    use TopAbleTrait;
    
    private $stick;

    public function getStick() : int
    {
        return $this->stick;
    }
    
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
    
    protected function getITopAbleAdapter() : ITopAbleAdapter
    {
        return new MockTopAbleAdapter();
    }
}
