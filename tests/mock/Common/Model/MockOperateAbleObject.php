<?php
namespace Base\Sdk\Common\Model;

use Base\Sdk\Common\Model\IOperateAble;
use Base\Sdk\Common\Adapter\IOperateAbleAdapter;
use Base\Sdk\Common\Adapter\MockOperateAbleAdapter;

class MockOperateAbleObject implements IOperateAble
{
    use OperateAbleTrait;

    public function addActionPublic(): bool
    {
        return $this->addAction();
    }

    public function editActionPublic(): bool
    {
        return $this->editAction();
    }

    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return new MockOperateAbleAdapter();
    }
}
