<?php
namespace Base\Sdk\Common\Model;

class MockNullApplyAbleObject
{
    use NullApplyAbleTrait;

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
