<?php
namespace Base\Sdk\Common\Model;

class MockNullModifyStatusAbleObject
{
    use NullModifyStatusAbleTrait;

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
