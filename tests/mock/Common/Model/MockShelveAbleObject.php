<?php
namespace Base\Sdk\Common\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Base\Sdk\Common\Model\IShelveAble;

class MockShelveAbleObject implements IShelveAble, IObject
{
    use Object;

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function shelve() : bool
    {
        return true;
    }

    public function offShelve() : bool
    {
        return true;
    }
}
