<?php
namespace Base\Sdk\Common\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;
use Base\Sdk\Common\Adapter\MockApplyAbleAdapter;

class MockApplyAbleObject implements IApplyAble, IObject
{
    use Object, ApplyAbleTrait;

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function approveActionPublic() : bool
    {
        return $this->approveAction();
    }

    public function rejectActionPublic() : bool
    {
        return $this->rejectAction();
    }

    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return new MockApplyAbleAdapter();
    }
}
