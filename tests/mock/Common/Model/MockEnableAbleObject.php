<?php
namespace Base\Sdk\Common\Model;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Sdk\Common\Adapter\IEnableAbleAdapter;
use Base\Sdk\Common\Adapter\MockEnableAbleAdapter;

class MockEnableAbleObject implements IEnableAble
{
    use EnableAbleTrait;
    
    private $status;

    public function getStatus() : int
    {
        return $this->status;
    }
    
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
    
    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return new MockEnableAbleAdapter();
    }
}
