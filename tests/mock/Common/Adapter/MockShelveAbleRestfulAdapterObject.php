<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IShelveAble;

class MockShelveAbleRestfulAdapterObject
{
    use ShelveAbleRestfulAdapterTrait;

    protected function getResource() : string
    {
        return '';
    }

    public function shelveActionPublic(IShelveAble $shelveAbleObject) : bool
    {
        return $this->shelveAction($shelveAbleObject);
    }

    public function offShelveActionPublic(IShelveAble $shelveAbleObject) : bool
    {
        return $this->offShelveAction($shelveAbleObject);
    }
}
