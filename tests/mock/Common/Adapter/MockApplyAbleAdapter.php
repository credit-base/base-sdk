<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\Common\Adapter\IApplyAbleAdapter;

class MockApplyAbleAdapter implements IApplyAbleAdapter
{
    public function approve(IApplyAble $applyAbleObject) : bool
    {
        unset($applyAbleObject);
        return true;
    }

    public function reject(IApplyAble $applyAbleObject) : bool
    {
        unset($applyAbleObject);
        return true;
    }
}
