<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IEnableAble;

class MockEnableAbleAdapter implements IEnableAbleAdapter
{
    public function enable(IEnableAble $enableAbleObject) : bool
    {
        unset($enableAbleObject);
        return true;
    }

    public function disable(IEnableAble $enableAbleObject) : bool
    {
        unset($enableAbleObject);
        return true;
    }
}
