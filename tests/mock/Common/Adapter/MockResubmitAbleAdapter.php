<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IResubmitAble;

class MockResubmitAbleAdapter implements IResubmitAbleAdapter
{
    public function resubmit(IResubmitAble $resubmitAbleObject) : bool
    {
        unset($resubmitAbleObject);
        return true;
    }
}
