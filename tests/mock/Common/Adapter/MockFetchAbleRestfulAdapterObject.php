<?php
namespace Base\Sdk\Common\Adapter;

use Marmot\Interfaces\INull;

class MockFetchAbleRestfulAdapterObject
{
    use FetchAbleRestfulAdapterTrait;

    protected function getResource() : string
    {
        return '';
    }

    public function fetchOneActionPublic(int $id, INull $null)
    {
        return $this->fetchOneAction($id, $null);
    }

    public function fetchListActionPublic(array $ids)
    {
        return $this->fetchListAction($ids);
    }

    public function searchActionPublic(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->searchAction($filter, $sort, $number, $size);
    }
}
