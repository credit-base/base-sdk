<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IModifyStatusAble;

class MockModifyStatusAbleAdapter implements IModifyStatusAbleAdapter
{
    public function close(IModifyStatusAble $modifyStatusAbleObject) : bool
    {
        unset($modifyStatusAbleObject);
        return true;
    }

    public function revoke(IModifyStatusAble $modifyStatusAbleObject) : bool
    {
        unset($modifyStatusAbleObject);
        return true;
    }

    public function deletes(IModifyStatusAble $modifyStatusAbleObject) : bool
    {
        unset($modifyStatusAbleObject);
        return true;
    }
}
