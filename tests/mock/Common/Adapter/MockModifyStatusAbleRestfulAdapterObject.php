<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IModifyStatusAble;

class MockModifyStatusAbleRestfulAdapterObject
{
    use ModifyStatusAbleRestfulAdapterTrait;

    protected function getResource() : string
    {
        return '';
    }

    public function revokeActionPublic(IModifyStatusAble $modifyStatusAbleObject) : bool
    {
        return $this->revokeAction($modifyStatusAbleObject);
    }

    public function closeActionPublic(IModifyStatusAble $modifyStatusAbleObject) : bool
    {
        return $this->closeAction($modifyStatusAbleObject);
    }

    public function deletesActionPublic(IModifyStatusAble $modifyStatusAbleObject) : bool
    {
        return $this->deletesAction($modifyStatusAbleObject);
    }
}
