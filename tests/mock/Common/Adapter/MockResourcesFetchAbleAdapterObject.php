<?php
namespace Base\Sdk\Common\Adapter;

use Marmot\Interfaces\INull;

class MockResourcesFetchAbleAdapterObject
{
    use ResourcesFetchAbleAdapterTrait;

    protected function getResource() : string
    {
        return '';
    }

    public function fetchOneActionPublic(int $id, string $resources, INull $null)
    {
        return $this->fetchOneAction($id, $resources, $null);
    }

    public function searchActionPublic(
        string $resources,
        array $filter = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->searchAction($resources, $filter, $number, $size);
    }
}
