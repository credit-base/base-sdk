<?php
namespace Base\Sdk\Common\Adapter;

use Base\Sdk\Common\Model\IEnableAble;

class MockEnableAbleRestfulAdapterObject
{
    use EnableAbleRestfulAdapterTrait;

    protected function getResource() : string
    {
        return '';
    }

    public function enableActionPublic(IEnableAble $enableAbleObject) : bool
    {
        return $this->enableAction($enableAbleObject);
    }

    public function disableActionPublic(IEnableAble $enableAbleObject) : bool
    {
        return $this->disableAction($enableAbleObject);
    }
}
